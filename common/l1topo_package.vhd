

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.ALL;

package l1topo_package is

	function ld(m : positive) return natural;
	
	--arrays of std_logic_vectors
--	type arraySLV is array (natural range <>) of std_logic_vector;
    type arraySLV256 is array (natural range <>) of std_logic_vector(256 - 1 downto 0);
    type arraySLV128 is array (natural range <>) of std_logic_vector(128 - 1 downto 0);
    type arraySLV96 is array (natural range <>) of std_logic_vector(96 - 1 downto 0);
    type arraySLV80 is array (natural range <>) of std_logic_vector(79 - 1 downto 0);
    type arraySLV38 is array (natural range <>) of std_logic_vector(38 - 1 downto 0);
    type arraySLV37 is array (natural range <>) of std_logic_vector(37 - 1 downto 0);
    type arraySLV36 is array (natural range <>) of std_logic_vector(36 - 1 downto 0);
    type arraySLV32 is array (natural range <>) of std_logic_vector(32 - 1 downto 0);
    type arraySLV20 is array (natural range <>) of std_logic_vector(20 - 1 downto 0);
    type arraySLV19 is array (natural range <>) of std_logic_vector(19 - 1 downto 0);
    type arraySLV16 is array (natural range <>) of std_logic_vector(16 - 1 downto 0);
    type arraySLV12 is array (natural range <>) of std_logic_vector(12 - 1 downto 0);
    type arraySLV11 is array (natural range <>) of std_logic_vector(11 - 1 downto 0);
    type arraySLV10 is array (natural range <>) of std_logic_vector(10 - 1 downto 0);
    type arraySLV9 is array (natural range <>) of std_logic_vector(9 - 1 downto 0);
    type arraySLV8 is array (natural range <>) of std_logic_vector(8 - 1 downto 0);
    type arraySLV7 is array (natural range <>) of std_logic_vector(7 - 1 downto 0);
    type arraySLV6 is array (natural range <>) of std_logic_vector(6 - 1 downto 0);
    type arraySLV5 is array (natural range <>) of std_logic_vector(5 - 1 downto 0);
    type arraySLV4 is array (natural range <>) of std_logic_vector(4 - 1 downto 0);
    type arraySLV3 is array (natural range <>) of std_logic_vector(3 - 1 downto 0);
    type arraySLV2 is array (natural range <>) of std_logic_vector(2 - 1 downto 0);
    type arraySLV1 is array (natural range <>) of std_logic_vector(1 - 1 downto 0);
    type arraySL is array (natural range <>) of std_logic;

    type arrayUNS8 is array (natural range <>) of unsigned(8 - 1 downto 0);
    type arrayUNS7 is array (natural range <>) of unsigned(7 - 1 downto 0);
    type arrayUNS6 is array (natural range <>) of unsigned(6 - 1 downto 0);
    type arrayUNS5 is array (natural range <>) of unsigned(5 - 1 downto 0);
    type arrayUNS4 is array (natural range <>) of unsigned(4 - 1 downto 0);
    type arrayUNS3 is array (natural range <>) of unsigned(3 - 1 downto 0);
    type arrayUNS2 is array (natural range <>) of unsigned(2 - 1 downto 0);
    type arrayUNS1 is array (natural range <>) of unsigned(1 - 1 downto 0);

    
    type arrayOfArray80xSLV128 is array (natural range <>) of arraySLV128(79 downto 0);


end l1topo_package;

package body l1topo_package is

	-- log_2 function
	function ld(m : positive) return natural is
	begin
		for n in 0 to integer'high loop
			if (2 ** n >= m) then
				return n;
			end if;
		end loop;
	end function ld;

end l1topo_package;
