library IEEE;
--use IEEE.STD_LOGIC_UNSIGNED.ALL;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


package rod_l1_topo_types_const is
  ------------------------------------------------------------------------------
  -- kintex & virtex
  ------------------------------------------------------------------------------
  constant NUMBER_OF_ROS_OUTPUT_BUSES : integer := 8;
  constant ROS_OUTPUT_GRANURALITY     : integer := 8;
  constant OUTPUT_DATA_WIDTH          : integer := NUMBER_OF_ROS_OUTPUT_BUSES*ROS_OUTPUT_GRANURALITY;
  constant NUMBER_OF_ROS_ROI_INPUT_BUSES : integer := 81;  --§§M 83 for delayed
                                                           --muons§§D - no !! ??
  constant MAX_NUMBER_OF_ROS_ROI_INPUT_BUSES : integer := 81; --§§M 83 for delayed
                                                              --muons§§D - no
                                                              --!! ??
  constant ROS_INPUT_GRANURALITY         : integer := 128;
  constant SIZE_OF_SLICE_VALUE           : integer := 4;
  constant MAX_NUMBER_OF_TOTAL_SLICES    : integer := NUMBER_OF_ROS_ROI_INPUT_BUSES*2**(SIZE_OF_SLICE_VALUE);
  constant NUMBER_OF_PARSERS             : integer := 16;
  function log2_int (x : natural) return integer;
  --DDR 
  constant LINKS_NUMBER : integer := 8;
  type rod_control_registers_array is array (0 to 78) of std_logic_vector(31 downto 0);
  type rod_status_registers_array is array (0 to 74) of std_logic_vector(31 downto 0);
  
  -----------------------------------------------------------------------------
  -- virtex
  -----------------------------------------------------------------------------
  
  
  subtype  in_memory_address_range is natural range 9 downto 0; --: increased from 8bit to 10bit as we're using ring_buffer_128b_1024W now. $%^
  --subtype  in_memory_address_range is natural range 8 downto 0; --: increased from 8bit to 10bit as we're using ring_buffer_128b_1024W now. $%^
  subtype  memory_selector_range is natural range log2_int(NUMBER_OF_ROS_ROI_INPUT_BUSES) downto 0;
  subtype  slice_values_range is natural range SIZE_OF_SLICE_VALUE-1 downto 0;
  type     in_data_array is array (0 to NUMBER_OF_ROS_ROI_INPUT_BUSES-1) of std_logic_vector(ROS_INPUT_GRANURALITY-1 downto 0);
  type     memory_array is array (0 to NUMBER_OF_ROS_ROI_INPUT_BUSES) of std_logic_vector(ROS_INPUT_GRANURALITY-1 downto 0);
  type     slice_parameters_array_u is array (0 to NUMBER_OF_ROS_ROI_INPUT_BUSES) of unsigned(slice_values_range);
  type     err_in_array is array (15 downto 0) of std_logic_vector(NUMBER_OF_ROS_ROI_INPUT_BUSES-1 downto 0);

  -----------------------------------------------------------------------------
  -- kintex
  -----------------------------------------------------------------------------
  constant VERSION_NUMBER : std_logic_vector(3 downto 0) :=x"1";
  constant NUMBER_OF_OUTPUT_LINKS : integer := 2;
  constant all_data_sent_const_ones : std_logic_vector(NUMBER_OF_OUTPUT_LINKS-1 downto 0) := (others => '1');
  type in_ddr_data_array is array (0 to NUMBER_OF_ROS_OUTPUT_BUSES-1) of std_logic_vector(7 downto 0);
  type out_data_array is array (0 to NUMBER_OF_OUTPUT_LINKS-1) of std_logic_vector(ROS_INPUT_GRANURALITY-1 downto 0);
  type in_cntrl_array is array (0 to NUMBER_OF_ROS_ROI_INPUT_BUSES-1) of std_logic_vector(log2_int(NUMBER_OF_OUTPUT_LINKS)+1 downto 0);
  type kintex_memory_array is array (0 to NUMBER_OF_ROS_ROI_INPUT_BUSES-1) of std_logic_vector(ROS_INPUT_GRANURALITY downto 0);
  type bus_number_array is array (0 to NUMBER_OF_OUTPUT_LINKS-1) of unsigned(log2_int(NUMBER_OF_ROS_ROI_INPUT_BUSES) downto 0);
  type fifo_address_array is array (0 to NUMBER_OF_ROS_ROI_INPUT_BUSES) of unsigned(log2_int(NUMBER_OF_ROS_ROI_INPUT_BUSES) downto 0);
  type link_pointer_array is array (0 to NUMBER_OF_OUTPUT_LINKS - 1) of fifo_address_array;
  type input_ros_roi_data_cntr_array is array (0 to NUMBER_OF_ROS_ROI_INPUT_BUSES-1) of unsigned(SIZE_OF_SLICE_VALUE-1 downto 0);
  type output_link_data_cntr_array is array (0 to NUMBER_OF_OUTPUT_LINKS-1) of unsigned(log2_int(MAX_NUMBER_OF_TOTAL_SLICES) downto 0);

  type links_number_1d_array is array (0 to NUMBER_OF_PARSERS-1) of integer range 0 to NUMBER_OF_ROS_ROI_INPUT_BUSES;
  type types_number_1d_array is array (0 to NUMBER_OF_PARSERS-1) of integer range 0 to NUMBER_OF_PARSERS;
  type types_number_array is array (0 to NUMBER_OF_OUTPUT_LINKS-1) of integer range 0 to NUMBER_OF_ROS_ROI_INPUT_BUSES;

  type pointers_array is array (0 to NUMBER_OF_ROS_ROI_INPUT_BUSES-1) of integer range 0 to NUMBER_OF_ROS_ROI_INPUT_BUSES;
  type crate_number_array is array (0 to NUMBER_OF_ROS_ROI_INPUT_BUSES -1) of std_logic_vector(3 downto 0);
  
  type roib_input_links_numbers_array is array (0 to 3) of std_logic_vector(6 downto 0);
  type proc_roib_input_links_numbers_array is array (0 to 1) of roib_input_links_numbers_array;
  type cntr_debug_array is array (0 to 31) of unsigned(31 downto 0);
  -----------------------------------------------------------------------------
  -- TOB parsers
  -----------------------------------------------------------------------------
  constant EM_TOB_TYPE      : std_logic_vector(3 downto 0) := x"0";
  constant TAU_TOB_TYPE     : std_logic_vector(3 downto 0) := x"1";
  constant MUON_TOB_TYPE    : std_logic_vector(3 downto 0) := x"2";
  constant MUON_TOB_TYPE_D  : std_logic_vector(3 downto 0) := x"3";  --delayed_muons
  constant JET_TOB_TYPE     : std_logic_vector(3 downto 0) := x"4";
  constant ENERGY_TOB_TYPE  : std_logic_vector(3 downto 0) := x"6";
  constant TRIGGER_TOB_TYPE : std_logic_vector(3 downto 0) := x"8";
  constant BLOCK_WORD_TYPE  : std_logic_vector(3 downto 0) := x"c";
  constant FIBER_WORD_TYPE  : std_logic_vector(3 downto 0) := x"d";
  constant STATUS_WORD_TYPE : std_logic_vector(3 downto 0) := x"e";
  constant NOT_USED_TOB_TYPE  : std_logic_vector(3 downto 0) := x"f";
  constant MAX_TOB_NUMBER : integer := 8;
  type tob_map_array is array (MAX_NUMBER_OF_ROS_ROI_INPUT_BUSES downto 0) of std_logic_vector(3 downto 0);
  type tob_map_array_2d is array (0 to 1) of tob_map_array;
  
  constant TOB_MAP : tob_map_array_2d :=
    (
    (--FPGA 0
     80 => TRIGGER_TOB_TYPE,
     79 => MUON_TOB_TYPE,               --C side
     78 => MUON_TOB_TYPE_D,             --C side delayed
     77 => MUON_TOB_TYPE_D,             --A side delayed
     76 => MUON_TOB_TYPE,               --A side
     71 downto 56 => JET_TOB_TYPE,
     54 => ENERGY_TOB_TYPE,
     52 => ENERGY_TOB_TYPE,
     51 downto 46 => EM_TOB_TYPE,
     45 downto 40 => TAU_TOB_TYPE,
     35 downto 30 => EM_TOB_TYPE,
     29 downto 24 => TAU_TOB_TYPE,
     23 downto 18 => EM_TOB_TYPE,
     17 downto 12 => TAU_TOB_TYPE,
     11 downto 6 => EM_TOB_TYPE,
     5 downto 0 => TAU_TOB_TYPE,
     others => NOT_USED_TOB_TYPE),
    (--FPGA 1
     80 => TRIGGER_TOB_TYPE,
     79 => MUON_TOB_TYPE,               --C side
     78 => MUON_TOB_TYPE_D,             --C side delayed
     77 => MUON_TOB_TYPE_D,             --A side delayed
     76 => MUON_TOB_TYPE,               --A side
     71 downto 56 => JET_TOB_TYPE,
     54 => ENERGY_TOB_TYPE,
     52 => ENERGY_TOB_TYPE,
     51 downto 46 => TAU_TOB_TYPE,
     45 downto 40 => EM_TOB_TYPE,
     35 downto 30 => TAU_TOB_TYPE,
     29 downto 24 => EM_TOB_TYPE,
     23 downto 18 => TAU_TOB_TYPE,
     17 downto 12 => EM_TOB_TYPE,
     11 downto 6 => TAU_TOB_TYPE,
     5 downto 0 => EM_TOB_TYPE,
     others => NOT_USED_TOB_TYPE)    
    );
  
  type start_array is array (MAX_TOB_NUMBER-1 downto 0) of integer range 0 to 127;
  type length_array is array (MAX_TOB_NUMBER-1 downto 0) of integer range 0 to 127;
  type overflow_array is array (MAX_TOB_NUMBER-1 downto 0) of integer range 0 to 127;
  
  type tob_definition is record         -- used for individual types definition
    tob_start          : start_array;
    tob_length         : length_array;
    overflow_elements  : overflow_array;
    crc_start          : integer;
    crc_length         : integer;
    max_tob_number     : integer;
  end record;
  type all_tob_def_array is array (0 to 15) of tob_definition;
   
  constant ALL_TOB_ARRAY : all_tob_def_array :=
    (--tob start  , tob length  ,       OV     , crc start, end, max
    ((0,0,0,105,82,59,36,13),(0,0,0,23,23,23,23,23),(0,0,0,0,0,0,0,12),0,11,5),  --EM_TOB_TYPE_0
    ((0,0,0,105,82,59,36,13),(0,0,0,23,23,23,23,23),(0,0,0,0,0,0,0,12),0,11,5),  --TAU_TOB_TYPE_1
    ((112,96,80,64,48,32,16,0),(16,16,16,16,16,16,16,16),(0,0,0,0,0,0,0,0),0,0,8),   --MUON_TOB_TYPE_2 
    ((112,96,80,64,48,32,16,0),(16,16,16,16,16,16,16,16),(0,0,0,0,0,0,0,0),0,0,8),   --MUON_TOB_TYPE_3_DELAYED 
    ((0,0,0,0,100,72,44,16)   ,(0,0,0,0,28,28,28,28),(0,0,0,0,0,0,0,15),0,11,4),        --JET_TOB_TYPE_4/5
    ((0,0,0,0,100,72,44,16)   ,(0,0,0,0,28,28,28,28),(0,0,0,0,0,0,0,15),0,11,4),        --JET_TOB_TYPE_4/5
    ((0,112,96,80,72,44,28,12),(0,16,16,16,8,8,16,16),(0,0,127,111,95,79,43,27),0,11,6),--ENERGY_TOB_TYPE_6,44-72hasto be marged
    ((others => 0),(others => 0), (others => 0),    0     , 0  ,  0 ),    --NOT_USED_TOB_TYPE_7
    ((0,0,0,0,49,33,17,1),(0,0,0,0,16,16,16,16),(others => 0),    0     , 0  ,  4),   --TRIGGER_TOB_TYPE_8
    ((others => 0),(others => 0), (others => 0),    0     , 0  ,  0 ),    --NOT_USED_TOB_TYPE_9
    ((others => 0),(others => 0), (others => 0),    0     , 0  ,  0 ),    --NOT_USED_TOB_TYPE_10
    ((others => 0),(others => 0), (others => 0),    0     , 0  ,  0 ),    --NOT_USED_TOB_TYPE_11
    ((others => 0),(others => 0), (others => 0),    0     , 0  ,  0 ),    --NOT_USED_TOB_TYPE_12
    ((others => 0),(others => 0), (others => 0),    0     , 0  ,  0 ),    --NOT_USED_TOB_TYPE_13
    ((others => 0),(others => 0), (others => 0),    0     , 0  ,  0 ),    --NOT_USED_TOB_TYPE_14
    ((others => 0),(others => 0), (others => 0),    0     , 0  ,  0 )    --NOT_USED_TOB_TYPE_15
    );

      
--  function calculate_pointers (link_assignment_in : in_cntrl_array; type_assignment_in: slice_parameters_array_u) return pointers_array;
  function tot_number_of_links(in_out_assigned_links : in_cntrl_array ; link_number : integer) return integer;
  --function set_number_of_type(link_assignment_in : in_cntrl_array) return types_number_array;
  --function link_number_of_parsers(assigned_ind_parsers_amount : types_number_array) return integer;
  function set_active_links (in_out_assigned_links : in_cntrl_array ; link_number : integer) return pointers_array;

  function std_int (X : std_logic_vector) return integer;

  function max_offset(Y : slice_parameters_array_u; X : slice_parameters_array_u) return std_logic_vector;
  
end rod_l1_topo_types_const;

package body rod_l1_topo_types_const is

  function log2_int (x : natural) return integer is
    variable temp : integer := x;
    variable n    : integer := 0;
  begin
    while temp > 1 loop
      temp := temp / 2;
      n    := n + 1;
    end loop;
    return n;
  end function log2_int;

  
  --function set_number_of_type (link_assignment_in : in_cntrl_array; type_assignment_in: slice_parameters_array_u) return types_number_array is
  --  variable types_array : types_number_array := (others => (others => '0'));
  --  variable i : integer := 0;
  --begin
  --  while i < link_assignment_in'length loop
  --    if unsigned(link_assignment_in(i)) < NUMBER_OF_PARSERS and
  --      type_assignment_in(i) > 0 and type_assignment_in < NUMBER_OF_PARSERS then
  --      types_array(to_integer(unsigned(link_assignment_in(i))))(type_assignment_in(i)) := types_array(to_integer(unsigned(link_assignment_in(i))))(type_assignment_in(i)) + 1;
  --    end if;
  --    i := i + 1;
  --  end loop;
  --  return types_array;
  --end function set_number_of_type;
  

  function tot_number_of_links(in_out_assigned_links : in_cntrl_array ; link_number : integer) return integer is
    variable i : integer := 0;
    variable tot_number : integer := 0;
    begin
      while i < NUMBER_OF_ROS_ROI_INPUT_BUSES  loop
        if unsigned(in_out_assigned_links(i)) = link_number then
          tot_number := tot_number + 1;
        end if;
        i := i + 1;
      end loop;
    return tot_number;
  end function tot_number_of_links;
  
  --function calculate_pointers (link_assignment_in : in_cntrl_array; type_assignment_in: slice_parameters_array_u) return pointers_array is
  
  --variable calc_array_a, calc_array_b : pointers_array := (others => 0 );
    
  --variable i,j : integer := 0;
  --begin
  --  while i < NUMBER_OF_ROS_ROI_INPUT_BUSES-1 loop
  --    if unsigned(link_assignment_in(i)) = link_number then
  --      calc_array_a(j) := calc_array_b(to_integer(type_assignment_in(i)));
  --      calc_array_b(to_integer(type_assignment_in(i))) := calc_array_b(to_integer(type_assignment_in(i))) + 1;
  --    end if;
      
  --    i:= i + 1;
  --  end loop;
  --  return calc_array_a;
  --end function calculate_pointers;

  function set_active_links (in_out_assigned_links : in_cntrl_array ; link_number : integer) return pointers_array is
    variable i,j : integer := 0;
    variable active_links : pointers_array := (others => 0);
    begin
      while i < NUMBER_OF_ROS_ROI_INPUT_BUSES  loop
        if unsigned(in_out_assigned_links(i)) = link_number then
          active_links(j) := i;
          j := j + 1;
        end if;
        i := i + 1;
      end loop;
    return active_links;
  end function set_active_links;

  function std_int (X : std_logic_vector) return integer is
  variable tmp : integer := 0;
  begin
	 tmp := to_integer(unsigned(X));
    return tmp;
  end std_int;

  function max_offset(Y : slice_parameters_array_u; X : slice_parameters_array_u) return std_logic_vector is
    variable max : std_logic_vector(3 downto 0):=(others => '0');
    variable i : integer :=0;
    begin
      while i < NUMBER_OF_ROS_ROI_INPUT_BUSES  loop
        if X(i) > unsigned(max) then
          max := std_logic_vector(X(i));
          i := i + 1;
        else
          max := max;
          i := i + 1;
        end if;
      end loop;     
      while i < NUMBER_OF_ROS_ROI_INPUT_BUSES  loop
        if Y(i) > unsigned(max) then
          max := std_logic_vector(Y(i));
          i := i + 1;
        else
          max := max;
          i := i + 1;
        end if;
      end loop;
      
    return max;
  end function max_offset;
        
end rod_l1_topo_types_const;
   

