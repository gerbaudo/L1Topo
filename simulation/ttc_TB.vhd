----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    22:08:33 11/27/2014 
-- Design Name: 
-- Module Name:    ttc_TB - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
USE ieee.math_real.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ttc_TB is
GENERIC (
	TRIGGER_PERIOD : time  := 10 us
	
);
PORT(
	CLK_P, CLK_N : IN std_logic;
        BUSY_IN  : in std_logic;
	TTC_EVT_H_STR_OUT : OUT std_logic;
	TTC_EVT_L_STR_OUT : OUT std_logic;
	TTC_L1A_OUT : OUT std_logic;
	TTC_BCNT_STR_OUT : OUT std_logic;
	TTC_BCNT_OUT : OUT std_logic_vector(11 downto 0);
	DOUT_OUT : OUT std_logic_vector(7 downto 0);
	SUBADDR_OUT : OUT std_logic_vector(7 downto 0);
	DOUTSTR_OUT : OUT std_logic
	);
end ttc_TB;

architecture Behavioral of ttc_TB is

signal clk_p_sgn, clk_n_sgn : std_logic  := '0';
signal l1a_out_sgn  : std_logic  := '0';
signal evt_h_str : std_logic  := '0';
signal evt_l_str : std_logic  := '0';
signal bcnt_str : std_logic   := '0';

signal bcn : unsigned (11 downto 0)  := (others => '0');
signal ttc_bcnt_sgn : unsigned (11 downto 0)  := (others => '0');
signal evt_cnt : unsigned (23 downto 0) := (others => '0');

signal tt_rdy : std_logic  := '0';
signal wait_time : time := 1 us;
signal ov_l1a_beg : std_logic;
signal ov_busy : std_logic;
shared   variable interval : time := 0 sec;
signal dout_l : unsigned(7 downto 0) := (others => '0');

begin
PROCESS(tt_rdy)
  VARIABLE seed1,seed2: positive;       -- Seed values for random generator
  VARIABLE rand: real;                  -- Random real-number value in range 0 to 1.0
BEGIN
    UNIFORM(seed1,seed2, rand);         -- generate random number
    interval  :=1050 ns + (rand*8048.0)*1 ns;
END PROCESS;

--clock_gen_proc : process
--begin
--	clk_p_sgn  <= '1';
--	clk_n_sgn  <= '0';
--	wait for 12.5 ns;
--	clk_p_sgn  <= '0';
--	clk_n_sgn  <= '1';
--	wait for 12.5 ns;
--end process;

wait_time <= (100/((((to_integer(evt_cnt)) mod 10)+1)*(((to_integer(evt_cnt)) mod 10)+1)))*TRIGGER_PERIOD;
clk_p_sgn <= CLK_P;
clk_n_sgn <= CLK_N;

process
  begin
    ov_l1a_beg <= '0';
    wait for 16 us;
    ov_l1a_beg <= '1';
    wait for 1700 us;
    ov_l1a_beg <= '0';
    wait for 14540 us;
  end process;

  ov_busy  <=  ov_l1a_beg and (not(BUSY_IN) and clk_p_sgn);
  
ttc_pulses_gen : process
begin
  for i in 1 to 9 loop
        tt_rdy      <= '0';
	l1a_out_sgn <= '0';
	evt_l_str   <= '0';
	evt_h_str   <= '0';
        wait until ov_busy = '1';
	-- wait for TRIGGER_PERIOD -12.5 ns;--wait_time;
--	wait until rising_edge(clk_p_sgn);

        tt_rdy      <= '0' ;
	l1a_out_sgn  <= '1';
	bcnt_str     <= '1';
	evt_l_str   <= '0';
	wait for 25 ns;
        tt_rdy      <= '0';
	l1a_out_sgn  <= '0';
	bcnt_str    <= '0';
	evt_l_str   <= '1';
	wait for 25 ns;
        tt_rdy      <= '0';
	l1a_out_sgn  <= '0';
	evt_l_str    <= '0';
	bcnt_str    <=  '0';
	evt_h_str    <= '1';
        wait for 25 ns;
        tt_rdy      <= '0';
	l1a_out_sgn <= '0';
	evt_l_str   <= '0';
	evt_h_str   <= '0';
	wait for 125 ns;
        tt_rdy      <= '1';
        l1a_out_sgn <= '0';
	evt_l_str   <= '0';
	evt_h_str   <= '0';
        wait for 25 ns;
        dout_l <= dout_l + 1;
   end loop;
        tt_rdy      <= '0';
	l1a_out_sgn <= '0';
	evt_l_str   <= '0';
	evt_h_str   <= '0';
        wait until ov_busy = '1';
	-- wait for TRIGGER_PERIOD -12.5 ns;--wait_time;
--	wait until rising_edge(clk_p_sgn);

        tt_rdy      <= '0' ;
	l1a_out_sgn  <= '1';
	bcnt_str     <= '1';
	evt_l_str   <= '0';
	wait for 25 ns;
        tt_rdy      <= '0';
	l1a_out_sgn  <= '0';
	bcnt_str    <= '0';
	evt_l_str   <= '1';
	wait for 25 ns;
        tt_rdy      <= '0';
	l1a_out_sgn  <= '0';
	evt_l_str    <= '0';
	bcnt_str    <=  '0';
	evt_h_str    <= '1';
        wait for 25 ns;
        tt_rdy      <= '0';
	l1a_out_sgn <= '0';
	evt_l_str   <= '0';
	evt_h_str   <= '0';
	wait for 49000 ns;
        tt_rdy      <= '1'; --late trigger type
        l1a_out_sgn <= '0';
	evt_l_str   <= '0';
	evt_h_str   <= '0';
        wait for 25 ns;
        
        tt_rdy      <= '0';
	l1a_out_sgn <= '0';
	evt_l_str   <= '0';
	evt_h_str   <= '0';
   wait for 31000 ns;     
end process;

bcnt_inc_proc : process (clk_p_sgn)
begin
	if rising_edge(clk_p_sgn) then
		bcn <= bcn +1;
	end if;
end process;

evt_cnt_proc : process
begin
	wait until l1a_out_sgn='1';
	evt_cnt  <= evt_cnt +1;
end process;

BCnt_MUX : process (l1a_out_sgn,evt_l_str,evt_h_str)
begin 
	if (l1a_out_sgn = '1') then
	ttc_bcnt_sgn  <= bcn;
	elsif(evt_l_str= '1') then
	ttc_bcnt_sgn <= evt_cnt(11 downto 0);
	elsif(evt_h_str = '1') then
	ttc_bcnt_sgn  <= evt_cnt(23 downto 12);
	else
	ttc_bcnt_sgn <= ttc_bcnt_sgn;
	end if;
end process;


--triggerType_proc : process
--begin
--tt_rdy  <= '0';
--wait until rising_edge(l1a_out_sgn);
--wait for 1075 ns;-- interval;--wait_time + 100 ns;--interval;
--tt_rdy  <= '1';
--wait for 25 ns;
--end process;

TTC_BCNT_OUT <= std_logic_vector(ttc_bcnt_sgn);
--CLK_P  <= clk_p_sgn;
--CLK_N  <= clk_n_sgn;
TTC_EVT_H_STR_OUT <= evt_h_str;
TTC_EVT_L_STR_OUT  <= evt_l_str;
TTC_L1A_OUT  <= l1a_out_sgn;
TTC_BCNT_STR_OUT  <= bcnt_str;
DOUT_OUT  <= std_logic_vector(dout_l'delayed(19 us));--x"aa";
SUBADDR_OUT <= x"00";
DOUTSTR_OUT  <= tt_rdy'delayed(19 us);


end Behavioral;

