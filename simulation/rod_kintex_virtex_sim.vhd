library IEEE;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.rod_l1_topo_types_const.all;
use work.L1TopoGTConfiguration.all;


entity rod_kintex_virtex_sim is
end rod_kintex_virtex_sim;

architecture rod_kintex_virtex_sim of rod_kintex_virtex_sim is

-- processor

  signal GCK1_P, GCK1_N             : std_logic;
  signal GCK2_P, GCK2_N             : std_logic;
  signal MGT2_CLK_P, MGT2_CLK_N     : std_logic_vector(11 downto 1);
  signal MGT4_CLK_P, MGT4_CLK_N     : std_logic_vector(11 downto 1);
   
  signal U1_RxP                          : std_logic_vector(79 downto 0);
  signal U1_RxN                          : std_logic_vector(79 downto 0);
  signal U1_TxP                          : std_logic_vector(3 downto 0);
  signal U1_TxN                          : std_logic_vector(3 downto 0);                 
 
  signal U2_RxP                          : std_logic_vector(79 downto 0);
  signal U2_RxN                          : std_logic_vector(79 downto 0);
  signal U2_TxP                          : std_logic_vector(3 downto 0);                 
  signal U2_TxN                          : std_logic_vector(3 downto 0);                 

--controller 

  signal MGT5_CLK_P, MGT5_CLK_N                   : std_logic;
  signal MGT7_CLK_P, MGT7_CLK_N                   : std_logic;
  signal K7_MGTRX_3_P, K7_MGTRX_3_N               : std_logic;
  signal K7_MGTTX_3_P, K7_MGTTX_3_N               : std_logic;
  signal OPTO_KT1_P, OPTO_KT1_N                   : std_logic_vector(11 downto 0);
  signal OPTO_KR1_P, OPTO_KR1_N                   : std_logic_vector(11 downto 0);
  signal TTC_RESET_OUT                            : std_logic;
  signal TTC_EVT_H_STR_IN                         : std_logic;
  signal TTC_L1A_IN                               : std_logic;
  signal TTC_BCNT_STR_IN                          : std_logic;
  signal TTC_EVT_L_STR_IN                         : std_logic;
  signal TTC_BCNT_IN                              : std_logic_vector(11 downto 0);
  signal TTC_EVTCNTRRST_IN                        : std_logic;
  signal TTC_BCNRST_IN                            : std_logic;
  signal TTC_BRCST_IN                             : std_logic_vector(5 downto 0);
  signal TTC_BCSTR1_IN                            : std_logic;
  signal TTC_BCSTR2_IN                            : std_logic;
  signal TTC_DOUT_STR_IN                          : std_logic;
  signal TTC_DOUT_IN                              : std_logic_vector(7 downto 0);
  signal TTC_SUBADDR_IN                           : std_logic_vector(7 downto 0);
  signal TTC_CTRL_22                              : std_logic;
  signal TTC_CTRL_23                              : std_logic;
  signal TTC_CTRL_24                              : std_logic;
  signal TTC_CTRL_25                              : std_logic;
  signal TTC_CTRL_26                              : std_logic;
  signal TTC_CTRL_27                              : std_logic;
  signal ROD_GTX_RXP                              : std_logic_vector(1 downto 0);
  signal ROD_GTX_RXN                              : std_logic_vector(1 downto 0);
  signal roib_busy                            : std_logic :='0';
  signal daq_busy                             : std_logic :='0';

--interconnection
  signal proc1_busy_p, proc1_busy_n, proc1_ttc_L1a_p, proc1_ttc_L1a_n : std_logic := '0';
  signal proc2_busy_p, proc2_busy_n, proc2_ttc_L1a_p, proc2_ttc_L1a_n : std_logic := '0';
  signal proc_clk_p, proc_clk_n : std_logic:= '0';
  signal busy_from_topo : std_logic := '0';
  signal ctrlbus_u1_in_p, ctrlbus_u1_in_n : std_logic_vector(20 downto 9);
  
begin
--processors
  TOP_L1TOPOPROCESSOR_U1: entity work.top_L1TopoProcessor_1
    generic map (
      PROCESSOR_NUMBER => 1,
      SIMULATION       => TRUE)
    port map (
      GCK1_P       => GCK1_P,
      GCK1_N       => GCK1_N,
      GCK2_P       => GCK2_P,
      GCK2_N       => GCK2_N,
      MGT2_CLK_P   => MGT2_CLK_P,
      MGT2_CLK_N   => MGT2_CLK_N,
      MGT4_CLK_P   => MGT4_CLK_P,
      MGT4_CLK_N   => MGT4_CLK_N,
      RxP          => U1_RxP,
      RxN          => U1_RxN,
      TxP          => U1_TxP,
      TxN          => U1_TxN,
      CTRLBUS_0_P  => '0',
      CTRLBUS_0_N  => '1',
      CTRLBUS_1_P  => '0',
      CTRLBUS_1_N  => '1',
      CTRLBUS_2_P  => '0',
      CTRLBUS_2_N  => '1',
      CTRLBUS_3_P  => '0',
      CTRLBUS_3_N  => '1',
      CTRLBUS_4_P  => '0',
      CTRLBUS_4_N  => '1',
      CTRLBUS_5_P  => '0',
      CTRLBUS_5_N  => '1',
      CTRLBUS_6_P  => proc1_busy_p,
      CTRLBUS_6_N  => proc1_busy_n,
      CTRLBUS_7_P  => open,
      CTRLBUS_7_N  => open,
      CTRLBUS_8_P  => proc1_ttc_L1a_p,
      CTRLBUS_8_N  => proc1_ttc_L1a_n,
      CTRLBUS_9_P  => open,
      CTRLBUS_9_N  => open,
      CTRLBUS_10_P => open,
      CTRLBUS_10_N => open,
      CTRLBUS_11_P => open,
      CTRLBUS_11_N => open,
      CTRLBUS_12_P => proc_clk_p,
      CTRLBUS_12_N => proc_clk_n,
      CTRLBUS_13_P => open,
      CTRLBUS_13_N => open,
      CTRLBUS_14_P => open,
      CTRLBUS_14_N => open,
      CTRLBUS_15_P => open,
      CTRLBUS_15_N => open,
      CTRLBUS_16_P => open,
      CTRLBUS_16_N => open,
      CTRLBUS_17_P => open,
      CTRLBUS_17_N => open,
      CTRLBUS_18_P => open,
      CTRLBUS_18_N => open,
      CTRLBUS_19_P => open,
      CTRLBUS_19_N => open,
      CTRLBUS_20_P => open,
      CTRLBUS_20_N => open,
      EXT_V7_P     => open,
      EXT_V7_N     => open);


  TOP_L1TOPOPROCESSOR_U2: entity work.top_L1TopoProcessor_1
    generic map (
      PROCESSOR_NUMBER => 2,
      SIMULATION       => TRUE)
    port map (
      GCK1_P       => GCK1_P,
      GCK1_N       => GCK1_N,
      GCK2_P       => GCK2_P,
      GCK2_N       => GCK2_N,
      MGT2_CLK_P   => MGT2_CLK_P,
      MGT2_CLK_N   => MGT2_CLK_N,
      MGT4_CLK_P   => MGT4_CLK_P,
      MGT4_CLK_N   => MGT4_CLK_N,
      RxP          => U2_RxP,
      RxN          => U2_RxN,
      TxP          => U2_TxP,
      TxN          => U2_TxN,
      CTRLBUS_0_P  => '0',
      CTRLBUS_0_N  => '1',
      CTRLBUS_1_P  => '0',
      CTRLBUS_1_N  => '1',
      CTRLBUS_2_P  => '0',
      CTRLBUS_2_N  => '1',
      CTRLBUS_3_P  => '0',
      CTRLBUS_3_N  => '1',
      CTRLBUS_4_P  => '0',
      CTRLBUS_4_N  => '1',
      CTRLBUS_5_P  => '0',
      CTRLBUS_5_N  => '1',
      CTRLBUS_6_P  => proc2_busy_p,   
      CTRLBUS_6_N  => proc2_busy_n,   
      CTRLBUS_7_P  => open,   
      CTRLBUS_7_N  => open,   
      CTRLBUS_8_P  => proc2_ttc_L1a_p,   
      CTRLBUS_8_N  => proc2_ttc_L1a_n,   
      CTRLBUS_9_P  => open,
      CTRLBUS_9_N  => open,
      CTRLBUS_10_P => open,
      CTRLBUS_10_N => open,
      CTRLBUS_11_P => open,
      CTRLBUS_11_N => open,
      CTRLBUS_12_P => open,   
      CTRLBUS_12_N => open,   
      CTRLBUS_13_P => open,
      CTRLBUS_13_N => open,
      CTRLBUS_14_P => open,
      CTRLBUS_14_N => open,
      CTRLBUS_15_P => open,
      CTRLBUS_15_N => open,
      CTRLBUS_16_P => open,
      CTRLBUS_16_N => open,
      CTRLBUS_17_P => open,
      CTRLBUS_17_N => open,
      CTRLBUS_18_P => open,
      CTRLBUS_18_N => open,
      CTRLBUS_19_P => open,
      CTRLBUS_19_N => open,
      CTRLBUS_20_P => open,
      CTRLBUS_20_N => open,
      EXT_V7_P     => open,
      EXT_V7_N     => open);
--controller
  ctrlbus_u1_in_p <= x"00" & proc_clk_p & "000";
  ctrlbus_u1_in_n <= x"ff" & proc_clk_n & "111";
  ROD_GTX_RXP(0) <= U1_TxP(0);
  ROD_GTX_RXN(0) <= U1_TxN(0);
  ROD_GTX_RXP(1) <= U2_TxP(0);
  ROD_GTX_RXN(1) <= U2_TxN(0);
    
  L1TOPO_CONTROLFPGA_TOP_INST: entity work.L1Topo_ControlFPGA_top
    generic map (
      SIMULATION           => TRUE,
      VIVADO               => TRUE,
      DEBUG                => FALSE)
    port map (
      GCK1_P              => GCK1_P,
      GCK1_N              => GCK1_N,
      GCK2_P              => GCK2_P,
      GCK2_N              => GCK2_N,
      MGT5_CLK_P          => MGT5_CLK_P,
      MGT5_CLK_N          => MGT5_CLK_N,
      MGT7_CLK_P          => MGT7_CLK_P,
      MGT7_CLK_N          => MGT7_CLK_N,
      MGT2_CLK_P          => MGT2_CLK_P(1),
      MGT2_CLK_N          => MGT2_CLK_N(1),
      K7_MGTRX_3_P        => K7_MGTRX_3_P,
      K7_MGTRX_3_N        => K7_MGTRX_3_N, 
      K7_MGTTX_3_P        => K7_MGTTX_3_P,
      K7_MGTTX_3_N        => K7_MGTTX_3_N,
      OPTO_KT1_P          => OPTO_KT1_P,
      OPTO_KT1_N          => OPTO_KT1_N,
      OPTO_KR1_P          => OPTO_KR1_P,
      OPTO_KR1_N          => OPTO_KR1_N,
      TTC_RESET_OUT       => TTC_RESET_OUT,
      TTC_EVT_H_STR_IN    => TTC_EVT_H_STR_IN,
      TTC_L1A_IN          => TTC_L1A_IN,
      TTC_BCNT_STR_IN     => TTC_BCNT_STR_IN,
      TTC_EVT_L_STR_IN    => TTC_EVT_L_STR_IN,
      TTC_BCNT_IN         => TTC_BCNT_IN,
      TTC_EVTCNTRRST_IN   => TTC_EVTCNTRRST_IN,
      TTC_BCNRST_IN       => TTC_BCNRST_IN,
      TTC_BRCST_IN        => TTC_BRCST_IN,
      TTC_BCSTR1_IN       => TTC_BCSTR1_IN,
      TTC_BCSTR2_IN       => TTC_BCSTR2_IN,
      TTC_DOUT_STR_IN     => TTC_DOUT_STR_IN,
      TTC_DOUT_IN         => TTC_DOUT_IN,
      TTC_SUBADDR_IN      => TTC_SUBADDR_IN,
      TTC_CTRL_22         => TTC_CTRL_22,
      TTC_CTRL_23         => TTC_CTRL_23,
      TTC_CTRL_24         => TTC_CTRL_24,
      TTC_CTRL_25         => TTC_CTRL_25,
      TTC_CTRL_26         => TTC_CTRL_26,
      TTC_CTRL_27         => TTC_CTRL_27,
      ROD_GTX_RXP         => ROD_GTX_RXP,
      ROD_GTX_RXN         => ROD_GTX_RXN,
      --AVAGO_SCL           => open,
      --AVAGO_SDA           => (others => 'Z'),
      --POWER_SCL           => open,
      --POWER_SDA           => 'Z',
      --DS2411              => 'Z',
      CTRLBUS_U1_OUT_P    => open,
      CTRLBUS_U1_OUT_N    => open,
      CTRLBUS_U1_OUT_P_8  => proc1_ttc_L1a_p,
      CTRLBUS_U1_OUT_N_8  => proc1_ttc_L1a_n,
      
      CTRLBUS_U1_IN_P     => ctrlbus_u1_in_p,
      CTRLBUS_U1_IN_N     => ctrlbus_u1_in_n,
      
      CTRLBUS_U1_IN_P_6   => proc1_busy_n,--iverted due to PCB design
      CTRLBUS_U1_IN_N_6   => proc1_busy_p,
      CTRLBUS_U2_OUT_P    => open,
      CTRLBUS_U2_OUT_N    => open,
      CTRLBUS_U2_OUT_P_29 => proc2_ttc_L1a_p,
      CTRLBUS_U2_OUT_N_29 => proc2_ttc_L1a_n,
      CTRLBUS_U2_IN_P     => (others => '0'),
      CTRLBUS_U2_IN_N     => (others => '1'),
      CTRLBUS_U2_IN_P_27  => proc2_busy_p,
      CTRLBUS_U2_IN_N_27  => proc2_busy_n,
      CTRLBUS_U2_IN_P_28  => '0',
      CTRLBUS_U2_IN_N_28  => '1',
      EXT_K7_0            => open,
      EXT_K7_1            => open,
      EXT_K7_2            => open,
      ROD_ROIB_BUSY       => roib_busy,
      ROD_DAQ_BUSY        => daq_busy);
  

-------------------------------------------------------------------------------
-- clocks
-------------------------------------------------------------------------------
  
  GT_CLOCK: process
  begin  -- process CLOCK
    gck1_p <= '1';
    gck1_n <= '0';
    wait for 12476 ps;
    gck1_p <= '0';
    gck1_n <= '1';
    wait for 12476 ps;
  end process GT_CLOCK;

  GC_CLOCK: process
  begin  -- process CLOCK
    gck2_P <= '1';
    gck2_N <= '0';
    wait for 12476 ps;
    gck2_P <= '0';
    gck2_N <= '1';
    wait for 12476 ps;
  end process GC_CLOCK;

  MG_CLOCK: process
  begin  -- process CLOCK
     MGT7_CLK_P<= '1';
     MGT7_CLK_N<= '0';
    wait for 4 ns;
     MGT7_CLK_P<= '0';
     MGT7_CLK_N<= '1';
    wait for 4 ns;
  end process MG_CLOCK;

  GEN_MGT_CLOCKS: for i in 1 to 11 generate
    MGT_CLOCK: process
    begin  -- process CLOCK
      MGT2_CLK_P(i)<= '1';
      MGT2_CLK_N(i)<= '0';
      MGT4_CLK_P(i)<= '1';
      MGT4_CLK_N(i)<= '0';
      wait for 3119 ps;
      MGT2_CLK_P(i)<= '0';
      MGT2_CLK_N(i)<= '1';
      MGT4_CLK_P(i)<= '0';
      MGT4_CLK_N(i)<= '1';
      wait for 3119 ps;
    end process MGT_CLOCK;
  end generate GEN_MGT_CLOCKS;
  
  ttc_TB_1: entity work.ttc_TB
    generic map (
      TRIGGER_PERIOD => 25 ns)
    port map (
      CLK_P             => gck1_p,
      CLK_N             => gck1_n,
      BUSY_IN           => busy_from_topo,
      TTC_EVT_H_STR_OUT => TTC_EVT_H_STR_IN,
      TTC_EVT_L_STR_OUT => TTC_EVT_L_STR_IN,
      TTC_L1A_OUT       => TTC_L1A_IN,
      TTC_BCNT_STR_OUT  => TTC_BCNT_STR_IN,
      TTC_BCNT_OUT      => TTC_BCNT_IN,
      DOUT_OUT          => TTC_DOUT_IN,
      SUBADDR_OUT       => TTC_SUBADDR_IN,
      DOUTSTR_OUT       => TTC_DOUT_STR_IN);
  busy_from_topo <= daq_busy or roib_busy;
  
  TTC_EVTCNTRRST_IN <= '0';
  TTC_BCNRST_IN <= '0';
  TTC_BRCST_IN <= (others => '0');
  TTC_BCSTR2_IN <= '0';
  TTC_BCSTR1_IN <= '0';
end rod_kintex_virtex_sim;
