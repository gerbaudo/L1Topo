----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:12:53 12/16/2014 
-- Design Name: 
-- Module Name:    mux_cpld - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;


entity L1TopoCPLD is port(
	KC5032A_20MHZ:		in  std_logic;
	KINTEX_CPLD:		in  std_logic_vector(0 downto 0);
	ATCA_CTRL:			in  std_logic_vector(3 downto 0);
	TTC_CTRL_5:			out std_logic;
	TTC_CTRL_6:			out std_logic;
	TTC_CTRL_19:		out std_logic;
	TTC_CTRL_20:		out std_logic;
	TTC_CTRL_21:		out std_logic;
	MUX_SEL_CPLD:		out std_logic_vector(9 downto 0);
	KINTEX7_CTRL:		in	 std_logic_vector(0 downto 0);
	VIRTEX7_CTRL_0:	in	 std_logic;
	VIRTEX7_CTRL_5:	in	 std_logic;
	ACE_CTRL: 			out std_logic_vector(5 downto 0)
);
end L1TopoCPLD;




architecture Behavioral of L1TopoCPLD is

signal cpldclk20:				std_logic;
signal ttc_reset_counter:	std_logic_vector(11 downto 0);
signal ace_cfginit_b:		std_logic;

begin



CPLDCLK20_BUFG: BUFG port map(
	I => KC5032A_20MHZ,
	O => cpldclk20
);


-- TTC

-- TTC_RESET_B min pulse width:
-- t_min = 10*R*C    (http://ttc.web.cern.ch/TTC/TTCrx_manual3.9.pdf, page 32)
-- pull-down/up resistors R430-R441 = 20kOhm, assuming C = 50pF
-- => t_min = 1mu s
process(cpldclk20) begin
if rising_edge(cpldclk20) then
	if KINTEX_CPLD(0)='0' then 					--KINTEX_CPLD(0) is constrainted to use PULLUP
		ttc_reset_counter <= (others => '0');
	elsif ttc_reset_counter(11)='0' then 		--saturate after 100mu s (100*t_min)
		ttc_reset_counter <= std_logic_vector(unsigned(ttc_reset_counter)+1);
	end if;
end if;
end process;

TTC_CTRL_21 <= ttc_reset_counter(11);	--TTC_RESET_B	(reset TTC until counter saturates)



--TTC chip ID(13 downto 0) <= TTC_SUBADDR(5 downto 0) & TTC_DOUT(7 downto 0)
--
--TTC_SUBADDR(5 downto 2)  <= "0000"	(pull-down resistors R434-R437)
--TTC_SUBADDR(1 downto 0) & TTC_DOUT(7 downto 6) <= L1Topo-Crate slot number, see below
--TTC_DOUT(5 downto 0)		<= "001010"	(pull-down/-up resistors R430-R433, R440-R441)
TTC_CTRL_19 <= ATCA_CTRL(0);	--TTC_DOUT6
TTC_CTRL_20 <= ATCA_CTRL(1);	--TTC_DOUT7
TTC_CTRL_5  <= ATCA_CTRL(2);	--TTC_SUBADDR0
TTC_CTRL_6  <= ATCA_CTRL(3);	--TTC_SUBADDR1





-- MGT1
MUX_SEL_CPLD(0) <= '0'; -- SEL0
MUX_SEL_CPLD(1) <= '0'; -- SEL1

-- MGT2
MUX_SEL_CPLD(2) <= '0'; -- SEL1	!! regard the different wiring
MUX_SEL_CPLD(3) <= '0'; -- SEL0

-- MGT3
MUX_SEL_CPLD(4) <= '1'; -- SEL0
MUX_SEL_CPLD(5) <= '0'; -- SEL1

-- MGT4
MUX_SEL_CPLD(6) <= '1'; -- SEL1	!!
MUX_SEL_CPLD(7) <= '0'; -- SEL0

-- GCK1
MUX_SEL_CPLD(8) <= '0'; -- SEL1	!!
MUX_SEL_CPLD(9) <= '0'; -- SEL0


-- SysACE
--KINTEX7_CTRL(0), VIRTEX7_CTRL_0, VIRTEX7_CTRL_5 are the INIT_B pins of U3, U1 and U2
ace_cfginit_b <= KINTEX7_CTRL(0) and VIRTEX7_CTRL_0 and VIRTEX7_CTRL_5;

ACE_CTRL(0) <= '1';				-- ACE_RESET_B		(ACE_DAUGHTERBOARD ST5 connector 22)
ACE_CTRL(1) <= '0';				-- ACE_CFGADDR0	(ST5.23)
ACE_CTRL(2) <= '0';				-- ACE_CFGADDR1	(ST5.24)
ACE_CTRL(3) <= '0';				-- ACE_CFGADDR2	(ST5.25)
ACE_CTRL(4) <= '1';				-- ACE_CFGMODEPIN	(ST5.26)
ACE_CTRL(5) <= ace_cfginit_b;	-- ACE_CFGINIT_B	(ST5.27)

end Behavioral;

