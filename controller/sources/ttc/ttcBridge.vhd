----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    19:32:50 02/12/2014 
-- Design Name: 
-- Module Name:    ctrlbus_ttc - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity ttcBridge is port(
		sysclk40: in std_logic;
		sysclk80: in std_logic;
		ttc_L1Accept: in std_logic;
		ttc_EventCounterReset: in std_logic;
		ttc_BunchCounterReset: in std_logic;
		ttc_BroadcastStrobe1, ttc_BroadcastStrobe2: in std_logic;
		ttc_Broadcast: in std_logic_vector(5 downto 0);
		ttcBridgeControl: in std_logic_vector(2 downto 0);
		ttcBridge_u1, ttcBridge_u2: out std_logic_vector(1 downto 0);
		
		debug: out std_logic
	);
end ttcBridge;




architecture Behavioral of ttcBridge is
	
	attribute s: string;
	
	signal ttc_L1Accept_mux: std_logic;
	signal ttc_broadcast_01xxxx_received: std_logic;
	signal ttc_BunchCounterReset_mux: std_logic;
	

	signal toggle, toggle_reg: std_logic_vector(1 downto 0);
	attribute s of toggle: signal is "true";
	attribute s of toggle_reg: signal is "true";
	
	signal paralleldata: std_logic_vector(3 downto 0);
	attribute s of paralleldata: signal is "true";
	

begin
	
	ttc_L1Accept_mux <= ttc_L1Accept or  ttcBridgeControl(0);
	
	ttc_broadcast_01xxxx_received <= '1' when ( (ttc_Broadcast(5 downto 4)="01" and ttc_BroadcastStrobe1='1') 
												or (ttcBridgeControl(1)='1') ) 
												else '0';
												
	ttc_BunchCounterReset_mux <= ttc_BunchCounterReset or ttcBridgeControl(2);

	

	paralleldata <= ttc_L1Accept & ttc_broadcast_01xxxx_received & ttc_BunchCounterReset & '1';

	process(sysclk40) begin
		if rising_edge(sysclk40) then toggle <= not toggle;
		end if;
	end process;

	process(sysclk80) begin
		if rising_edge(sysclk80) then
			toggle_reg <= toggle;
			if toggle_reg /= toggle then
				ttcBridge_u1 <= paralleldata(1 downto 0);
				ttcBridge_u2 <= paralleldata(1 downto 0);
			else
				ttcBridge_u1 <= paralleldata(3 downto 2);
				ttcBridge_u2 <= paralleldata(3 downto 2);
			end if;
		end if;
	end process;

	

	
	
	debug <= ttc_broadcast_01xxxx_received;
	
end Behavioral;

