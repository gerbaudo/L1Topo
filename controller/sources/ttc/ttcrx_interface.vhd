library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_UNSIGNED.all;
use IEEE.NUMERIC_STD.all;
library UNISIM;
use UNISIM.VComponents.all;

entity ttcrx_interface is
  generic (
    SIMULATION : boolean := false;
    DEBUG : integer := 0
    );
  port (
    CLK_40MHZ     : in  std_logic;
    CLK_160MHZ    : in  std_logic;
    MMCX_OUT      : out std_logic;
    CLK_LOCKED_IN : in  std_logic;      -- resets ttc dec
    RESET_IN      : in  std_logic;      -- resets counters

    --input signals from ttcrx chip
    TTC_EVT_H_STR_IN  : in std_logic;
    TTC_EVT_L_STR_IN  : in std_logic;
    TTC_EVTCNTRRST_IN : in std_logic;
    TTC_L1A_IN        : in std_logic;
    TTC_BCNT_STR_IN   : in std_logic;
    TTC_BCNT_IN       : in std_logic_vector(11 downto 0);
    TTC_BCNRST_IN     : in std_logic;
    TTC_BRCST_IN      : in std_logic_vector(5 downto 0);
    TTC_BCSTR1_IN     : in std_logic;

    --21th Feb 2016, ck: routing registered ttc_broadcast, ttc_broadcast_strobe1 and ttc_bunchcounter_reset up to top module to forward to processors
    ttc_brcst_reg     : out std_logic_vector(5 downto 0);
    ttc_bcstr1_reg    : out std_logic;
    ttc_bcnrst_reg    : out std_logic;

    TTC_BCSTR2_IN     : in std_logic;
    TTC_DOUT_STR_IN   : in std_logic;
    TTC_DOUT_IN       : in std_logic_vector(7 downto 0);
    TTC_SUBADDR_IN    : in std_logic_vector(7 downto 0);

    -- input signals from ipbus
    IPB_ECRID_RST_IN   : in std_logic_vector(31 downto 0);
    IPB_ECRID_SET_IN   : in std_logic_vector(31 downto 0);
    IPB_ORBIT_WRAP_IN  : in std_logic_vector(31 downto 0);
    IPB_ORBIT_RST_IN   : in std_logic_vector(31 downto 0);
    IPB_L1ID_RST_IN    : in std_logic_vector(31 downto 0);
    IPB_TRG_TIMEOUT_IN : in std_logic_vector(31 downto 0);
    IPB_TRG_TYPE_IN    : in std_logic_vector(31 downto 0);

    --output signals
    TTC_RESET_OUT           : out std_logic;
    L1A_OUT                 : out std_logic;  --extended level 1 accepted
    TTC_BCID_OUT            : out std_logic_vector(11 downto 0);
    TTC_EVTID_OUT           : out std_logic_vector(23 downto 0);
    TTC_TRG_TYPE_OUT        : out std_logic_vector(7 downto 0);
    TTC_TRG_TYPE_RDY_OUT    : out std_logic;
    TTC_TRG_TIMEOUT_OUT     : out std_logic;
    LOCAL_ORBIT_CTR_OUT     : out std_logic_vector(23 downto 0);
    LOCAL_ECR_CTR_OUT       : out std_logic_vector(7 downto 0);
    LOCAL_BCID_OUT          : out std_logic_vector(11 downto 0);
    LOCAL_BCID_MISMATCH_OUT : out std_logic;

    GENERAL_L1A_CTR_OUT       : out std_logic_vector(31 downto 0);
    GENERAL_ECR_CTR_OUT       : out std_logic_vector(31 downto 0);
    GENERAL_EVT_TYPE_CNTR_OUT : out std_logic_vector(31 downto 0)
    );
end ttcrx_interface;

architecture Behavioral of ttcrx_interface is

  attribute KEEP_HIERARCHY               : string;
  attribute KEEP_HIERARCHY of Behavioral : architecture is "FALSE";

  signal bcid                                                : std_logic_vector(11 downto 0) := x"000";
  signal evtid                                               : std_logic_vector(23 downto 0) := x"000000";
  signal l1a                                                 : std_logic_vector(2 downto 0)  := "000";
  signal l1a_in_qq, l1a_in_q                                 : std_logic                     := '0';
  signal l1a_in                                              : std_logic_vector(0 downto 0);
  signal l1a_forward                                         : std_logic                     := '0';
  signal local_bcn                                           : std_logic_vector(11 downto 0) := x"000";
  signal local_ecr                                           : std_logic_vector(31 downto 0) := (others => '0');
  signal local_orbit_ctr                                     : std_logic_vector(23 downto 0) := x"000000";
  signal local_evtid                                         : std_logic_vector(23 downto 0) := x"000000";
  signal trg_timeout_ctr                                     : std_logic_vector(31 downto 0) := x"0000_0000";
  signal trigger_timeout, trigger_type_en, trigger_type_en_q : std_logic                     := '0';
  signal trigger_type                                        : std_logic_vector(7 downto 0)  := (others => '0');
  signal trg_timeout_count, trigger_type_pulse               : std_logic                     := '0';
  signal bcn_mismatch                                        : std_logic                     := '0';
  signal local_timeout_value                                 : std_logic_vector(8 downto 0) := '0' & x"00";
  signal gen_l1a_ctr, gen_ecr_ctr                            : std_logic_vector(31 downto 0) := x"0000_0000";

  signal ttc_evt_h_str_in_io                                                    : std_logic;
  signal ttc_evt_l_str_in_io                                                    : std_logic;
  signal ttc_evtcntrrst_in_io                                                   : std_logic;
  signal ttc_l1a_in_io                                                          : std_logic;
  signal ttc_bcnt_str_in_io                                                     : std_logic;
  signal ttc_bcnt_in_io                                                         : std_logic_vector(11 downto 0);
  signal ttc_bcnrst_in_io                                                       : std_logic;
  signal ttc_brcst_in_io                                                        : std_logic_vector(5 downto 0);
  signal ttc_bcstr1_in_io                                                       : std_logic;
  signal ttc_bcstr2_in_io                                                       : std_logic;
  signal ttc_dout_str_in_io, ttc_dout_str_in_io_sync1, ttc_dout_str_in_io_sync2 : std_logic;
  signal ttc_dout_in_io                                                         : std_logic_vector(7 downto 0);
  signal ttc_subaddr_in_io                                                      : std_logic_vector(7 downto 0);

  signal trigger_type_save                     : std_logic                    := '0';
  signal trigger_type_read                     : std_logic_vector(0 downto 0) := (others => '0');
  signal trigger_type_in, trigger_type_out     : std_logic_vector(7 downto 0) := (others => '0');
  signal ttc_ila_trg, ttc_ila_trg_sync         : std_logic_vector(255 downto 0);
  signal ttc_icon_control                      : std_logic_vector(35 downto 0);
  signal one_bit_ref                           : std_logic                    := '0';
  signal evt_trigge_sync                       : std_logic;
  signal evt_trigger_cntr_fast                 : unsigned(31 downto 0)        := (others => '0');
  signal trigger_type_empty, trigger_type_full : std_logic;
  signal trigger_type_cntr                     : unsigned(7 downto 0) := (others => '0');
  signal trigger_type_fifo_rst                 : std_logic;
  signal time_out_cntr                         : unsigned(8 downto 0) := (others => '1');
  signal ttc_evt_h_str_in_io_sync                   : std_logic_vector(2 downto 0);
  signal ttc_evt_l_str_in_io_sync                   : std_logic_vector(2 downto 0);
  signal ttc_bcnt_str_in_io_sync                    : std_logic_vector(2 downto 0);
    
  attribute IOB                                : string;
  attribute IOB of ttc_evt_h_str_in_io         : signal is "TRUE";
  attribute IOB of ttc_evt_l_str_in_io         : signal is "TRUE";
  attribute IOB of ttc_evtcntrrst_in_io        : signal is "TRUE";
  attribute IOB of ttc_l1a_in_io               : signal is "TRUE";
  attribute IOB of ttc_bcnt_str_in_io          : signal is "TRUE";
  attribute IOB of ttc_bcnt_in_io              : signal is "TRUE";
  attribute IOB of ttc_bcnrst_in_io            : signal is "TRUE";
  attribute IOB of ttc_brcst_in_io             : signal is "TRUE";
  attribute IOB of ttc_bcstr1_in_io            : signal is "TRUE";
  attribute IOB of ttc_bcstr2_in_io            : signal is "TRUE";
  attribute IOB of ttc_dout_str_in_io          : signal is "TRUE";
  attribute IOB of ttc_dout_in_io              : signal is "TRUE";
  attribute IOB of ttc_subaddr_in_io           : signal is "TRUE";

  component icon_ttc_debug
    port (
      control0 : inout std_logic_vector(35 downto 0)
      );
  end component;

  component ila_ttc_debug
    port (
      control : inout std_logic_vector(35 downto 0);
      clk     : in    std_logic;
      trig0   : in    std_logic_vector(255 downto 0));
  end component;

begin

  SAMPLE_TRG_TYPE : process(CLK_160MHZ)
  begin
    if rising_edge(CLK_160MHZ) then
      ttc_dout_str_in_io       <= TTC_DOUT_STR_IN;
      ttc_dout_str_in_io_sync1 <= ttc_dout_str_in_io;
      ttc_dout_str_in_io_sync2 <= ttc_dout_str_in_io_sync1;
      ttc_dout_in_io           <= TTC_DOUT_IN;
      ttc_subaddr_in_io        <= TTC_SUBADDR_IN;
      ttc_evt_h_str_in_io  <= TTC_EVT_H_STR_IN;
      ttc_evt_l_str_in_io  <= TTC_EVT_L_STR_IN;
      ttc_bcnt_str_in_io   <= TTC_BCNT_STR_IN;
      ttc_evt_h_str_in_io_sync(0)  <= ttc_evt_h_str_in_io;
      ttc_evt_l_str_in_io_sync(0)  <= ttc_evt_l_str_in_io;
      ttc_bcnt_str_in_io_sync(0)   <= ttc_bcnt_str_in_io;
      ttc_evt_h_str_in_io_sync(1)  <= ttc_evt_h_str_in_io_sync(0);
      ttc_evt_l_str_in_io_sync(1)  <= ttc_evt_l_str_in_io_sync(0); 
      ttc_bcnt_str_in_io_sync(1)   <= ttc_bcnt_str_in_io_sync(0);
      ttc_evt_h_str_in_io_sync(2)  <= ttc_evt_h_str_in_io_sync(1);
      ttc_evt_l_str_in_io_sync(2)  <= ttc_evt_l_str_in_io_sync(1); 
      ttc_bcnt_str_in_io_sync(2)   <= ttc_bcnt_str_in_io_sync(1);
      ttc_bcnt_in_io       <= TTC_BCNT_IN;
    end if;
  end process SAMPLE_TRG_TYPE;

  PLACE_IO_REG : process(CLK_40MHZ)
  begin
    if falling_edge(CLK_40MHZ) then
--    ttc_evt_h_str_in_io  <= TTC_EVT_H_STR_IN;
--    ttc_evt_l_str_in_io  <= TTC_EVT_L_STR_IN;
      ttc_evtcntrrst_in_io <= TTC_EVTCNTRRST_IN;
      ttc_l1a_in_io        <= TTC_L1A_IN;
--    ttc_bcnt_str_in_io   <= TTC_BCNT_STR_IN;
--      ttc_bcnt_in_io       <= TTC_BCNT_IN;
      ttc_bcnrst_in_io     <= TTC_BCNRST_IN;
      ttc_brcst_in_io      <= TTC_BRCST_IN;
      ttc_bcstr1_in_io     <= TTC_BCSTR1_IN;
      ttc_bcstr2_in_io     <= TTC_BCSTR2_IN;
      --ttc_dout_str_in_io   <= TTC_DOUT_STR_IN;
      --ttc_dout_str_in_io_sync   <= ttc_dout_str_in_io;
      --ttc_dout_in_io       <= TTC_DOUT_IN;      
    end if;
  end process; 

    --21th Feb 2016, ck: routing registered ttc_broadcast, ttc_broadcast_strobe1 and ttc_bunchcounter_reset up to top module to forward to processors
    ttc_brcst_reg   <= ttc_brcst_in_io;
    ttc_bcstr1_reg  <= ttc_bcstr1_in_io;
    ttc_bcnrst_reg  <= ttc_bcnrst_in_io;

  process(CLK_40MHZ)
  begin
    if rising_edge(CLK_40MHZ) then
      l1a(2 downto 0) <= l1a(1 downto 0) & ttc_l1a_in_io;
    end if;
  end process;

  process(CLK_160MHZ)
  begin
    if rising_edge(CLK_160MHZ) then
      if (ttc_l1a_in_io = '1' and ttc_bcnt_str_in_io_sync(1) = '1' and ttc_bcnt_str_in_io_sync(2) = '0' ) then
        evtid <= evtid;
        bcid <= ttc_bcnt_in_io;
      elsif (l1a(0) = '1' and ttc_evt_l_str_in_io_sync(1) = '1' and ttc_evt_l_str_in_io_sync(2) = '0') then
        evtid(23 downto 12) <= evtid(23 downto 12);
        evtid(11 downto 0)  <= ttc_bcnt_in_io;
        bcid                <= bcid;
      elsif (l1a(1) = '1' and ttc_evt_h_str_in_io_sync(1) = '1' and ttc_evt_h_str_in_io_sync(2) = '0') then
        evtid(11 downto 0)  <= evtid(11 downto 0);
        evtid(23 downto 12) <= ttc_bcnt_in_io;
        bcid                <= bcid;
      else
        bcid  <= bcid;
        evtid <= evtid;
      end if;
    end if;
  end process;

  process(CLK_40MHZ)
  begin
    if rising_edge(CLK_40MHZ) then
      if (RESET_IN = '1' or ttc_bcnrst_in_io = '1' or local_bcn = x"deb") then
        local_bcn <= x"000";
      else
        local_bcn <= local_bcn + 1;
      end if;
    end if;
  end process;

  process(CLK_40MHZ)
  begin
    if rising_edge(CLK_40MHZ) then
      if (RESET_IN = '1' or IPB_ECRID_RST_IN(0) = '1') then
        local_ecr <= (others => '0');
      elsif (IPB_ECRID_SET_IN(0) = '1') then
        local_ecr(31 downto 8) <= (others => '0');
        local_ecr(7 downto 0)  <= IPB_ECRID_SET_IN(15 downto 8);
      elsif (ttc_evtcntrrst_in_io = '1') then
        local_ecr <= local_ecr + 1;
      else
        local_ecr <= local_ecr;
      end if;
    end if;
  end process;

  process(CLK_40MHZ)
  begin
    if rising_edge(CLK_40MHZ) then
      if (RESET_IN = '1' or local_orbit_ctr = x"010000" or local_orbit_ctr(15 downto 0) = IPB_ORBIT_WRAP_IN(15 downto 0) or IPB_ORBIT_RST_IN(0) = '1') then
        local_orbit_ctr <= (others => '0');
      elsif (ttc_bcnrst_in_io = '1') then
        local_orbit_ctr <= local_orbit_ctr + 1;
      else
        local_orbit_ctr <= local_orbit_ctr;
      end if;
    end if;
  end process;

  process(CLK_40MHZ)
  begin
    if rising_edge(CLK_40MHZ) then
      if (RESET_IN = '1' or ttc_evtcntrrst_in_io = '1' or local_evtid = x"01000000" or IPB_L1ID_RST_IN(0) = '1') then
        local_evtid <= (others => '0');
      elsif (ttc_l1a_in_io = '1') then
        local_evtid <= local_evtid + 1;
      else
        local_evtid <= local_evtid;
      end if;
    end if;
  end process;

-------------------------------------------------------------------------------
-- trigger type
-------------------------------------------------------------------------------
  
  SAVE_TRIGG_TYPE : process(CLK_160MHZ)
  begin
    if rising_edge(CLK_160MHZ) then
      if (IPB_TRG_TYPE_IN(0) = '0') then  -- not manually overwritten
        if (ttc_dout_str_in_io_sync2 = '0' and ttc_dout_str_in_io_sync1 = '1' and ttc_subaddr_in_io = x"00")  then  -- trigger type packet received
          trigger_type_in   <= ttc_dout_in_io;
          trigger_type_save <= '1';
        else
          trigger_type_in   <= ttc_dout_in_io;
          trigger_type_save <= '0';
        end if;
      else
        if (ttc_dout_str_in_io_sync2 = '0' and ttc_dout_str_in_io_sync1 = '1' and ttc_subaddr_in_io = x"00")  then  -- trigger type packet received
          trigger_type_in   <= IPB_TRG_TYPE_IN(15 downto 8);
          trigger_type_save <= '1';
        else
          trigger_type_in   <= ttc_dout_in_io;
          trigger_type_save <= '0';
        end if;
      end if;
    end if;
  end process SAVE_TRIGG_TYPE;

  TRIGG_TYPE_SFIFT_FIFO : entity work.ttc_evt_type_fifo
    port map (
      wr_clk        => CLK_160MHZ,
      rd_clk        => CLK_40MHZ,
      rst           => RESET_IN,
      din           => trigger_type_in,
      wr_en         => trigger_type_save,
      rd_en         => trigger_type_read(0),
      dout          => trigger_type_out,
      rd_data_count => open,
      full          => trigger_type_full,
      empty         => trigger_type_empty
      );
  trigger_type_read(0) <= not trigger_type_empty;
  
  SEND_TRIGGER_TYPE : process (CLK_40MHZ)
  begin
    if rising_edge(CLK_40MHZ) then
        trigger_type_en   <= trigger_type_read(0);
        trigger_type_en_q <= trigger_type_en;
    end if;
  end process SEND_TRIGGER_TYPE;

  TTC_TRG_TYPE_OUT     <= trigger_type_out;
  TTC_TRG_TYPE_RDY_OUT <= trigger_type_en_q;

  -----------------------------------------------------------------------------
  -- l1a
  -----------------------------------------------------------------------------

  l1a_delay : process(CLK_40MHZ)
  begin
    if rising_edge(CLK_40MHZ) then
      l1a_in(0) <= ttc_l1a_in_io;
      l1a_in_q  <= l1a_in(0);
      l1a_in_qq <= l1a_in_q;
      
    end if;
  end process;
  l1a_forward <= l1a_in(0); -- or l1a_in_q;  -- or l1a_in_qq;

  process(CLK_40MHZ)
  begin
    if rising_edge(CLK_40MHZ) then
      if (RESET_IN = '1') then
        bcn_mismatch <= '0';
      elsif (l1a_in(0) = '1') then
        if (local_bcn - 1 /= bcid) then
          bcn_mismatch <= '1';
        else
          bcn_mismatch <= '0';
        end if;
      elsif (trigger_type_en_q = '1') then
        bcn_mismatch <= '0';
      else
        bcn_mismatch <= bcn_mismatch;
      end if;
    end if;
  end process;

  process(CLK_40MHZ)
  begin
    if rising_edge(CLK_40MHZ) then
      if (IPB_L1ID_RST_IN(0) = '1') or RESET_IN = '1' then
        gen_l1a_ctr <= (others => '0');
      elsif (ttc_l1a_in_io = '1') then
        gen_l1a_ctr <= gen_l1a_ctr + 1;
      else
        gen_l1a_ctr <= gen_l1a_ctr;
      end if;
    end if;
  end process;

  process(CLK_40MHZ)
  begin
    if rising_edge(CLK_40MHZ) then
      if (IPB_ECRID_RST_IN(0) = '1') or RESET_IN = '1' then
        gen_ecr_ctr <= (others => '0');
      elsif (ttc_evtcntrrst_in_io = '1') then
        gen_ecr_ctr <= gen_ecr_ctr + 1;
      else
        gen_ecr_ctr <= gen_ecr_ctr;
      end if;
    end if;
  end process;



  TTC_RESET_OUT <= CLK_LOCKED_IN;
  MMCX_OUT      <= ttc_l1a_in_io;
  L1A_OUT       <= l1a_forward;

  TTC_BCID_OUT         <= bcid;
  TTC_EVTID_OUT        <= evtid;


  LOCAL_ORBIT_CTR_OUT     <= local_orbit_ctr;
  LOCAL_ECR_CTR_OUT       <= local_ecr(7 downto 0);
  LOCAL_BCID_OUT          <= local_bcn;
  LOCAL_BCID_MISMATCH_OUT <= bcn_mismatch;

  GENERAL_L1A_CTR_OUT <= gen_l1a_ctr;
  GENERAL_ecr_CTR_OUT <= local_ecr;

-------------------------------------------------------------------------------
-- debug
-------------------------------------------------------------------------------
  TTC_SIM_EN : if SIMULATION = false generate

    ONE_BIT_REFERENCE : process (CLK_40MHZ)
    begin
      if rising_edge(CLK_40MHZ) then
        one_bit_ref <= not one_bit_ref;
      end if;
    end process ONE_BIT_REFERENCE;
  EN_DEBUG: if DEBUG = 1 generate
    TTC_RX_ILA_INST: entity work.ttc_rx_ila
      port map(
        clk => CLK_160MHZ,
        probe0(0) => RESET_IN,
        probe1(0) => ttc_l1a_in_io,
        probe2(0) => trigger_type_en_q,
        probe3(0) => trigger_timeout,
        probe4(0) => bcn_mismatch,
        probe5(0) => one_bit_ref,
        probe6(0) => trigger_type_save,
        probe7(0) => trigger_type_read(0),
        probe8 => local_ecr(7 downto 0), --8
        probe9 => trigger_type_in, --8
        probe10 => evtid, --24
        probe11 => local_orbit_ctr, --24
        probe12 => bcid,--12
        probe13(0) => ttc_evt_h_str_in,--
        probe14(0) => ttc_l1a_in,--
        probe15(0) => ttc_bcnt_str_in,--
        probe16(0) => ttc_evt_l_str_in,--
        probe17 => ttc_bcnt_in,--12
        probe18(0) => ttc_evtcntrrst_in,--
        probe19(0) => ttc_bcnrst_in,--
        probe20(0) => ttc_dout_str_in,--
        probe21 => ttc_dout_in,--4
        probe22 => ttc_subaddr_in,--8
        probe23 => local_bcn,--12
        probe24 => trigger_type_out,--8
        probe25(0) => trigger_type_full,--
        probe26(0) => trigger_type_empty,--
        probe27(0) => ttc_dout_str_in_io_sync1,--
        probe28(0) => ttc_dout_str_in_io_sync2,--
        probe29 => std_logic_vector(trigger_type_cntr(4 downto 0)),
        probe30(0) => ttc_bcnt_str_in_io,
        probe31(0) => ttc_l1a_in_io,
        probe32(0) => l1a(0),
        probe33(0) => l1a(1),
        probe34 => ttc_bcnt_in_io
        );--5
    end generate EN_DEBUG;
    COUNT_EVT_TYPES : process (CLK_160MHZ)
    begin
      if rising_edge(CLK_160MHZ) then
        if evt_trigge_sync = '0' and trigger_type_en = '1' then
          evt_trigger_cntr_fast     <= evt_trigger_cntr_fast + 1;
          evt_trigge_sync           <= trigger_type_en;
          GENERAL_EVT_TYPE_CNTR_OUT <= std_logic_vector(evt_trigger_cntr_fast);
        else
          evt_trigger_cntr_fast     <= evt_trigger_cntr_fast;
          evt_trigge_sync           <= trigger_type_en;
          GENERAL_EVT_TYPE_CNTR_OUT <= std_logic_vector(evt_trigger_cntr_fast);
        end if;
      end if;
    end process COUNT_EVT_TYPES;

--    TTC_ILA : ila_ttc_debug
--      port map (
--        CONTROL => ttc_icon_control,
--        CLK     => CLK_160MHZ,
--        TRIG0   => ttc_ila_trg_sync);
--
--    TTC_ICON : icon_ttc_debug
--      port map (
--        CONTROL0 => ttc_icon_control
--        );
  end generate TTC_SIM_EN;
  
end Behavioral;


