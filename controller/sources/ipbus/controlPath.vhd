----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:48:47 07/07/2014 
-- Design Name: 
-- Module Name:    ctrlPath - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.l1topo_package.all;
use work.rod_l1_topo_types_const.all;
use work.ipbus.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity controlPath is
	port(
		sysclk40: in std_logic;
		sysclk80: in std_logic;
		sysclk200: in std_logic;
		sysclk400: in std_logic;
		eth_gt_refclk_bufg: in std_logic;
                ETH_GTREFCLK_P:     in  std_logic :=     '0';
                ETH_GTREFCLK_N:     in  std_logic :=     '1';
 		eth_gt_outclk: out std_logic;
		ethclk62_5: in std_logic;
		ethclk125: out std_logic;
		
		gck_mmcm_locked: in std_logic;
		eth_mmcm_locked: in std_logic;
		
		eth_gt_refclk: in std_logic;
		eth_gt_rx_p, eth_gt_rx_n: in std_logic;
		eth_gt_tx_p, eth_gt_tx_n: out std_logic;
		
		eth_sgmiiphy_done_out: out std_logic;
		rst_extphy: out std_logic;
		mac_addr: in std_logic_vector(47 downto 0) := X"000000000000"; -- Static MAC and IP addresses
		ip_addr: in std_logic_vector(31 downto 0) := X"00000000";
		pkt_rx_led: out std_logic;
		pkt_tx_led: out std_logic;
		
		ipbBridge_u1_in:  in  arraySLV2(2 downto 0);
		ipbBridge_u1_out: out arraySLV2(4 downto 0);
		ipbBridge_u2_in:  in  arraySLV2(2 downto 0);
		ipbBridge_u2_out: out arraySLV2(4 downto 0);
		
		
		i2c_scl: 			out std_logic_vector(4 downto 0);
		i2c_sda_in:			in  std_logic_vector(4 downto 0);
		i2c_sda_out:		out std_logic_vector(4 downto 0);
		
		
		clocksStatus: in std_logic_vector(3 downto 0);
		
		ctrlbus_delay_control: out arraySLV6(41 downto 0);
		ctrlbus_delay_pulse:   out arraySLV2(41 downto 0);
		ctrlbus_delay_status:  in  arraySLV5(41 downto 0);
		
		ttcStatus: in std_logic_vector(1 downto 0);
		ttcBridgeControl: out std_logic_vector(2 downto 0);
		
		xadc_control:  	out std_logic_vector(25 downto 0);
		xadc_status:		in  std_logic_vector(22 downto 0);
		usr_access:			in  std_logic_vector(31 downto 0);
		serialID_pulse:	out std_logic;
		serialID_status:	in  std_logic_vector(48 downto 0);
		
--		jcLossOfLock: 			in  std_logic;
--		jcInterruptClk1Bad:	in  std_logic;
--		jcSlaveSelect_b:		out std_logic;
--		jcSerialDataOut:		out std_logic;
--		jcSerialDataIn:		in  std_logic;
--		jcSerialClk:			out std_logic;
		
		
		
		ModuleID: in std_logic_vector(31 downto 0);
		Serial_No_Module: in std_logic_vector(31 downto 0);
		Serial_No_ExtensionBoard: in std_logic_vector(31 downto 0);
		Firmware_Version: in std_logic_vector(31 downto 0);
		FirmwareDate:		in std_logic_vector(31 downto 0);
		FirmwareRevision:	in std_logic_vector(31 downto 0);
		
		
		GeneralControl: out std_logic_vector(31 downto 0);
		GeneralPulse:   out std_logic_vector(31 downto 0);
		
		
		rod_control_registers: out rod_control_registers_array;
                rod_status_registers:  in  rod_status_registers_array
--		
--		i2c_dataOut:     in  std_logic_vector(7 downto 0);
--		i2c_error:       in  std_logic_vector(0 downto 0);
--		i2c_address:     out std_logic_vector(6 downto 0);
--		i2c_writeEnable: out std_logic_vector(0 downto 0);
--		i2c_pointer:     out std_logic_vector(7 downto 0);
--		i2c_dataIn:      out std_logic_vector(7 downto 0);
--		i2c_busSelect:   out std_logic_vector(5 downto 0);
		
--		debugIPBus: out std_logic_vector(99 downto 0);
--		debugIPBusBridgeU2: out std_logic_vector(229 downto 0)
		
	);
end controlPath;

architecture Behavioral of controlPath is

	signal rst: std_logic;
	signal rst_sgmiiphy: std_logic;
	signal rst_mac: std_logic;
	signal rst_ipb125: std_logic;
	signal rst_ipb: std_logic;
	signal eth_sgmiiphy_done: std_logic;
	signal eth_locked: std_logic;
	
	signal mac_tx_data: std_logic_vector(7 downto 0);
	signal mac_rx_data: std_logic_vector(7 downto 0);
	signal mac_tx_valid: std_logic;
	signal mac_tx_last: std_logic;
	signal mac_tx_error: std_logic;
	signal mac_tx_ready: std_logic;
	signal mac_rx_valid: std_logic;
	signal mac_rx_last: std_logic;
	signal mac_rx_error: std_logic;
	
	signal ipb_master_out : ipb_wbus;
	signal ipb_master_in : ipb_rbus;
	
	signal ethControl: std_logic_vector(0 downto 0);
	signal ethStatus:  std_logic_vector(13 downto 0);

	
	signal i2c_control:	std_logic_vector(26 downto 0);
	signal i2c_pulse:		std_logic;
	signal i2c_status:	std_logic_vector( 9 downto 0);
	
	

begin

process(eth_gt_refclk_bufg) begin
	if rising_edge(eth_gt_refclk_bufg) then
		rst <= not gck_mmcm_locked;
--		rst_sgmiiphy <= rst;
--		eth_locked <= eth_mmcm_locked and eth_sgmiiphy_done;
	end if;
end process;

--	rst_mac <= not(eth_locked) or rst_sgmiiphy;



process(ethclk125) begin
	if rising_edge(ethclk125) then
		rst_ipb125 <= rst or not eth_locked;
	end if;
end process;



process(sysclk40) begin
	if rising_edge(sysclk40) then
		rst_ipb <= rst;
	end if;
end process;



--eth: entity work.eth_sgmii
--	port map(
--		eth_gt_refclk  => eth_gt_refclk,
--                ETH_GTREFCLK_P => ETH_GTREFCLK_P,
--                ETH_GTREFCLK_N => ETH_GTREFCLK_N,
--		eth_gt_outclk	=> eth_gt_outclk,
--		ethclk62_5 			=> ethclk62_5,
--		ethclk125 			=> ethclk125,
--		sysclk200 			=> sysclk200,
		
--		rst_sgmiiphy => rst_sgmiiphy,
--		rst_mac => rst_mac,
--		rst_extphy => rst_extphy,
		
--		eth_mmcm_locked => eth_mmcm_locked,
--		sgmiiphy_done => eth_sgmiiphy_done,
	
--		eth_gt_rx_p 		=> eth_gt_rx_p,
--		eth_gt_rx_n 		=> eth_gt_rx_n,
--		eth_gt_tx_p 		=> eth_gt_tx_p,
--		eth_gt_tx_n 		=> eth_gt_tx_n,
		
--		mac_rx_data => mac_rx_data,
--		mac_rx_valid => mac_rx_valid,
--		mac_rx_error => mac_rx_error,
--		mac_rx_last => mac_rx_last,
		
--		mac_tx_data => mac_tx_data,
--		mac_tx_ready => mac_tx_ready,
--		mac_tx_valid => mac_tx_valid,
--		mac_tx_error => mac_tx_error,
--		mac_tx_last => mac_tx_last,
		
--		ethControl(0) => '0', --ethControl,
--		ethStatus  => ethStatus,
--                gt0_qplloutrefclk_out => gt0_qplloutrefclk_out,
--                gt0_qplloutclk_out => gt0_qplloutclk_out
--	);

U_ETH: entity work.ethernet
port map(

    sysclk200       => sysclk200,
    sysclk_locked   => gck_mmcm_locked,
    
    ETH_MGTREFCLK_P => ETH_GTREFCLK_P,
    ETH_MGTREFCLK_N => ETH_GTREFCLK_N,
    ETH_MGT_RX_P    => ETH_GT_RX_P,
    ETH_MGT_RX_N    => ETH_GT_RX_N,
    ETH_MGT_TX_P    => ETH_GT_TX_P,
    ETH_MGT_TX_N    => ETH_GT_TX_N,
        
    RESET_ETH_PHY_N => rst_extphy,--RESET_ETH_PHY_N,
    
    ethclk125       => ethclk125,
    eth_locked      => eth_locked,--eth_locked_i,
    gtrefclk_out    => eth_gt_outclk,
    
    mac_rx_data     => mac_rx_data,
    mac_rx_valid    => mac_rx_valid,
    mac_rx_last     => mac_rx_last,
    mac_rx_error    => mac_rx_error,

    mac_tx_data     => mac_tx_data,
    mac_tx_valid    => mac_tx_valid,
    mac_tx_last     => mac_tx_last,
    mac_tx_error    => mac_tx_error,
    mac_tx_ready    => mac_tx_ready,
    
    eth_control     => (others => '0'),
    eth_status      => open--eth_status
    
);

ipbus_master: entity work.ipbus_ctrl
	port map(
		mac_clk => ethclk125,
		rst_macclk => rst_ipb125,
		ipb_clk => sysclk40,
		rst_ipb => rst_ipb,
		
		mac_addr => mac_addr,
		ip_addr => ip_addr,
		
		mac_rx_data => mac_rx_data,
		mac_rx_valid => mac_rx_valid,
		mac_rx_error => mac_rx_error,
		mac_rx_last => mac_rx_last,
		
		mac_tx_data => mac_tx_data,
		mac_tx_ready => mac_tx_ready,
		mac_tx_valid => mac_tx_valid,
		mac_tx_error => mac_tx_error,
		mac_tx_last => mac_tx_last,
		
		ipb_out => ipb_master_out,
		ipb_in => ipb_master_in,
		
		pkt_rx => open,
		pkt_tx => open,
		pkt_rx_led => pkt_rx_led,
		pkt_tx_led => pkt_tx_led
	);



ipbus_slaves: entity work.slaves
		port map(
			sysclk40  => sysclk40,
			sysclk80  => sysclk80,
			sysclk200 => sysclk200,
			sysclk400 => sysclk400,
			
			gck_mmcm_locked => gck_mmcm_locked,
			
			ipb_rst => rst_ipb,
			ipb_in  => ipb_master_out,
			ipb_out => ipb_master_in,
			
			ipbBridge_u1_out => ipbBridge_u1_out,
			ipbBridge_u1_in  => ipbBridge_u1_in,
			
			ipbBridge_u2_out => ipbBridge_u2_out,
			ipbBridge_u2_in  => ipbBridge_u2_in,
			
			
			i2c_control	=> i2c_control,
			i2c_pulse	=> i2c_pulse,
			i2c_status	=> i2c_status,
			
			
			
			
			-- submodules
			
			clocksStatus => clocksStatus,
			
			ctrlbus_delay_control	=> ctrlbus_delay_control,
			ctrlbus_delay_pulse		=> ctrlbus_delay_pulse,
			ctrlbus_delay_status		=> ctrlbus_delay_status,
			
			ttcStatus => ttcStatus,
			ttcBridgeControl => ttcBridgeControl,
			
			xadc_control 		=> xadc_control,
			xadc_status  		=> xadc_status,
			usr_access   		=> usr_access,
			serialID_pulse		=> serialID_pulse,
			serialID_status	=> serialID_status,
			
--			jcLossOfLock 			=> jcLossOfLock,
--			jcInterruptClk1Bad 	=> jcInterruptClk1Bad,
--			jcSlaveSelect_b 		=> jcSlaveSelect_b,
--			jcSerialDataOut 		=> jcSerialDataOut,
--			jcSerialDataIn 		=> jcSerialDataIn,
--			jcSerialClk 			=> jcSerialClk,
			
			
			--misc
			
			ModuleID => ModuleID,
			Serial_No_Module => Serial_No_Module,
			Serial_No_ExtensionBoard => Serial_No_ExtensionBoard,
			Firmware_Version	=> Firmware_Version,
			FirmwareDate		=> FirmwareDate,
			FirmwareRevision	=> FirmwareRevision,
			
			GeneralControl => GeneralControl,
			GeneralPulse_out => GeneralPulse,
			
			rod_control_registers => rod_control_registers,
			rod_status_registers => rod_status_registers

			
		);



i2c: entity work.i2c
port map(
	CLK40MHz 	=> sysclk40,
	i2c_control => i2c_control,
	i2c_pulse	=> i2c_pulse,
	i2c_status	=> i2c_status,
	
	i2c_scl		=> i2c_scl,
	i2c_sda_in	=> i2c_sda_in,
	i2c_sda_out	=> i2c_sda_out
);



end Behavioral;

