----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Andreas Rei� (areiss01@uni-mainz.de)
-- 
-- Create Date: 08.01.2015 14:00:00
-- Design Name: 
-- Module Name: Top_K7_325_900_Control - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity Top_K7_325_900_Control is
	Port ( 
		-- GCK1_P : in  STD_LOGIC;--TTC Clock
		-- GCK1_N : in  STD_LOGIC;

		GCK2_P : in  STD_LOGIC;--Crystal Clock
		GCK2_N : in  STD_LOGIC;

		AVAGO_SDA_0 : inout STD_LOGIC;
		AVAGO_SCL_0 : inout STD_LOGIC;
		
		AVAGO_SDA_1 : inout STD_LOGIC;
        AVAGO_SCL_1 : inout STD_LOGIC;
        
		AVAGO_SDA_2 : inout STD_LOGIC;
        AVAGO_SCL_2 : inout STD_LOGIC;
        
 		POWER_SDA : inout STD_LOGIC;
        POWER_SCL : inout STD_LOGIC;		
		
		TTC_CTRL_21 : out std_logic:='1';--TTCDEC RESET_B
		TTC_CTRL_26 : out std_logic:='0';--TTCDEC P/D
		TTC_CTRL_27 : out std_logic:='0'--CLKSEL
		);
end Top_K7_325_900_Control;

architecture Behavioral of Top_K7_325_900_Control is
	attribute DONT_TOUCH : string; 
	attribute shreg_extract : string;	
	attribute keep : string;	
	attribute DONT_TOUCH of Behavioral : architecture is "TRUE";
	ATTRIBUTE SYN_BLACK_BOX : BOOLEAN;
	ATTRIBUTE BLACK_BOX_PAD_PIN : STRING;

	COMPONENT vio_0
	PORT (
	clk : IN STD_LOGIC;
	probe_in0 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
	probe_in1 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
	probe_in2 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
	probe_out0 : OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
	probe_out1 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
	probe_out2 : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	probe_out3 : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	probe_out4 : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
	probe_out5 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0)
	);
	END COMPONENT;
	ATTRIBUTE SYN_BLACK_BOX OF vio_0 : COMPONENT IS TRUE;
	ATTRIBUTE BLACK_BOX_PAD_PIN OF vio_0 : COMPONENT IS "clk,probe_in0[7:0],probe_in1[0:0],probe_in2[0:0],probe_out0[6:0],probe_out1[0:0],probe_out2[7:0],probe_out3[7:0],probe_out4[1:0],probe_out5[0:0]";

	component clk_wiz_0
	port
	(-- Clock in ports
	clk_in1_p         : in     std_logic;
	clk_in1_n         : in     std_logic;
	-- Clock out ports
	clk_out1          : out    std_logic
	);
	end component;
	ATTRIBUTE SYN_BLACK_BOX OF clk_wiz_0 : COMPONENT IS TRUE;
	ATTRIBUTE BLACK_BOX_PAD_PIN OF clk_wiz_0 : COMPONENT IS "clk_in1_p,clk_in1_n,clk_out1";

	component clk_wiz_1
	port
	(-- Clock in ports
	clk_in1_p         : in     std_logic;
	clk_in1_n         : in     std_logic;
	-- Clock out ports
	clk_out1          : out    std_logic
	);
	end component;
	ATTRIBUTE SYN_BLACK_BOX OF clk_wiz_1 : COMPONENT IS TRUE;
	ATTRIBUTE BLACK_BOX_PAD_PIN OF clk_wiz_1 : COMPONENT IS "clk_in1_p,clk_in1_n,clk_out1";	
	
	COMPONENT vio_1
	PORT (
	clk : IN STD_LOGIC;
	probe_in0 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
	probe_out0 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0)
	);
	END COMPONENT;	
	
	--VIO TTC Clock Test Signal
	signal VIO_Test: std_logic_vector(0 downto 0):="0";
	
	--I2C
	signal WRITING,STRB,ERROR,BUSY: std_logic:='0';
	signal SDAIN,SDAOUT,SCL: std_logic:='Z';
	signal ADDRESS:STD_LOGIC_VECTOR(6 DOWNTO 0):="0000000";
	signal DATA_IN,DATA_OUT,POINTER: STD_LOGIC_VECTOR(7 DOWNTO 0):="00000000";

	--Clocks an delays
	signal CLK40MHz,CLK40MHzTTC: std_logic:='0';

	--counter
	signal StateCounter: integer range 0 to 3:=1;	
	
	--input registers
	signal I2C_Chain: std_logic_vector(1 downto 0):="00";
	signal I2CAddress: std_logic_vector(6 downto 0):="0000000";
	signal I2CRegisterAddress: std_logic_vector(7 downto 0):="00000000";
	signal I2CReadOrWrite: std_logic:='0';
	signal I2CDataIn: std_logic_vector(7 downto 0):="00000000";
	
	--output registers
	signal I2CDataOut: std_logic_vector(7 downto 0):="00000000";
	signal I2CError,I2C_Busy: std_logic_vector(0 downto 0):="0";
	
	--pulse registers
	signal I2CStrobe: std_logic_vector(0 downto 0):="0";
	
	----------------IP Bus Registers Start ---------------------
	signal IPBusSet_I2C,IPBusGet_I2C,IPBusPulse_I2C: std_logic_vector(31 downto 0):=(others=>'0');
	
	--input registers
	signal IPBus_I2C_Chain: std_logic_vector(1 downto 0):="00";
	signal IPBus_I2C_ADDRESS: std_logic_vector(6 downto 0):="0000000";
	signal IPBus_I2C_RegisterNumber: std_logic_vector(7 downto 0):="00000000";
	signal IPBus_I2C_WriteEnable: std_logic_vector(0 downto 0):="0";
	signal IPBus_I2C_DataSend: std_logic_vector(7 downto 0):="00000000";
	
	--output registers
	signal IPBus_I2C_DataReceived: std_logic_vector(7 downto 0):="00000000";
	signal IPBus_I2C_Error,IPBus_I2C_Busy: std_logic_vector(0 downto 0):="0";
	
	--pulse registers
	signal IPBus_I2C_Strobe: std_logic_vector(0 downto 0):="0";	
	----------------IP Bus Registers End ---------------------
	
	attribute keep of IPBus_I2C_Chain : signal is "true";	  
	attribute keep of IPBus_I2C_ADDRESS : signal is "true";	  
	attribute keep of IPBus_I2C_RegisterNumber : signal is "true";	  
	attribute keep of IPBus_I2C_WriteEnable : signal is "true";	  
	attribute keep of IPBus_I2C_DataSend : signal is "true";	  
	attribute keep of IPBus_I2C_DataReceived : signal is "true";	  
	attribute keep of IPBus_I2C_Error : signal is "true";	  
	attribute keep of IPBus_I2C_Busy : signal is "true";	  
	attribute keep of IPBus_I2C_Strobe : signal is "true";
begin
	--TTC
	TTC_CTRL_21<='1';--TTCDEC RESET_B--'1';--Stop reset
    TTC_CTRL_26<='0';--TTCDEC P/D--'0';--Debug Mode	
    TTC_CTRL_27<='1';--CLKSEL--'1';--Clock source automatic changeover
	
	clk_wiz_0_i : clk_wiz_0
    port map
    (-- Clock in ports
    clk_in1_p => GCK2_P,
    clk_in1_n => GCK2_N,
    -- Clock out ports
    clk_out1 => CLK40MHz);
	
    i2c_i : entity work.i2c
    Port map(
    CLK40MHz => CLK40MHz,
    DATA_IN => DATA_IN,
    POINTER => POINTER,
    ADDRESS => ADDRESS,
    WRITING => WRITING,
    STRB => STRB,
    DATA_OUT => DATA_OUT,
    ERROR => ERROR,
    BUSY => BUSY,
    SDAIN => SDAIN,
    SDAOUT => SDAOUT,
    SCL => SCL);

	--AVAGO_0
	SDAIN<=AVAGO_SDA_0 when I2C_Chain="00" else 'Z';
	AVAGO_SDA_0<='0' when I2C_Chain="00" and SDAOUT='0' else 'Z';
	AVAGO_SCL_0<='0' when I2C_Chain="00" and SCL='0' else 'Z';
	
	--AVAGO_1
	SDAIN<=AVAGO_SDA_1 when I2C_Chain="01" else 'Z';
	AVAGO_SDA_1<='0' when I2C_Chain="01" and SDAOUT='0' else 'Z';
	AVAGO_SCL_1<='0' when I2C_Chain="01" and SCL='0' else 'Z';
	
	--AVAGO_2
	SDAIN<=AVAGO_SDA_2 when I2C_Chain="10" else 'Z';
	AVAGO_SDA_2<='0' when I2C_Chain="10" and SDAOUT='0' else 'Z';
	AVAGO_SCL_2<='0' when I2C_Chain="10" and SCL='0' else 'Z';
	
	--Power
	SDAIN<=POWER_SDA when I2C_Chain="11" else 'Z';
	POWER_SDA<='0' when I2C_Chain="11" and SDAOUT='0' else 'Z';
	POWER_SCL<='0' when I2C_Chain="11" and SCL='0' else 'Z';
	
	process(CLK40MHz)
		variable Timer: integer range 0 to 511:=0;
	begin
		if (rising_edge(CLK40MHz)) then
			if BUSY = '0' then
				if Timer=500 then
					I2C_Busy<="0";
					if StateCounter=1 and I2CStrobe="1" then
						Timer:=0;
						ADDRESS<=I2CAddress;
						WRITING<=I2CReadOrWrite;
						POINTER<=I2CRegisterAddress;
						DATA_IN<=I2CDataIn;
						STRB<='1';
						StateCounter<=2;
					elsif StateCounter=2 then
						I2CError(0)<=ERROR;
						I2CDataOut<=DATA_OUT;
						StateCounter<=1;
					end if;
				else
					Timer:=Timer+1;
				end if;
			elsif BUSY='1' then
				I2C_Busy<="1";
				STRB<='0';
			end if;
		end if;
	end process;
	
	StrobeI2C: process(CLK40MHz)
		variable Strobe_Old: std_logic_vector(0 downto 0):="0";
	begin
		if (rising_edge(CLK40MHz)) then
			if IPBus_I2C_Strobe="1" and Strobe_Old="0" then
				I2CStrobe<="1";
			else 
				I2CStrobe<="0";
			end if;
			Strobe_Old:=IPBus_I2C_Strobe;
		end if;
	end process;
	
	vio_0_i : vio_0
    PORT MAP (
    CLK => CLK40MHz,
    PROBE_IN0 => IPBus_I2C_DataReceived,--8
    PROBE_IN1 => IPBus_I2C_Error,--1
	PROBE_IN2 => IPBus_I2C_Busy,--1
    PROBE_OUT0 => IPBus_I2C_ADDRESS,--7
    PROBE_OUT1 => IPBus_I2C_WriteEnable,--1
    PROBE_OUT2 => IPBus_I2C_RegisterNumber,--8
    PROBE_OUT3 => IPBus_I2C_DataSend,--8
    PROBE_OUT4 => IPBus_I2C_Chain,--2
	PROBE_OUT5 => IPBus_I2C_Strobe--1
    );
	
	--input registers
	I2CAddress<=IPBus_I2C_ADDRESS;
	I2C_Chain<=IPBus_I2C_Chain;
	I2CReadOrWrite<=IPBus_I2C_WriteEnable(0);
	I2CRegisterAddress<=IPBus_I2C_RegisterNumber;
	I2CDataIn<=IPBus_I2C_DataSend;
	-- I2CAddress<=IPBusSet_I2C(6 downto 0);
	-- I2C_Chain<=IPBusSet_I2C(8 downto 7);	
	-- I2CReadOrWrite<=IPBusSet_I2C(9);
	-- I2CRegisterAddress<=IPBusSet_I2C(16 downto 9);
	-- I2CDataIn<=IPBusSet_I2C(24 downto 17);
	
	--output registers	
	IPBus_I2C_DataReceived<=I2CDataOut;
	IPBus_I2C_Error<=I2CError;
	IPBus_I2C_Busy<=I2C_Busy;	
	-- IPBusGet_I2C(7 downto 0)<=I2CDataOut;
	-- IPBusGet_I2C(8)<=I2C_Busy;	
	-- IPBusGet_I2C(9)<=I2CError;
	
	--strobe registers
	-- IPBus_I2C_Strobe<=IPBusPulse_I2C(0 downto 0);
	-- I2CStrobe<=IPBusPulse_I2C(0 downto 0);--alternative if I2CStrobe process is removed
	
	-- clk_wiz_1_i : clk_wiz_1
    -- port map
    -- (-- Clock in ports
    -- clk_in1_p => GCK1_P,
    -- clk_in1_n => GCK1_N,
    -- -- Clock out ports
    -- clk_out1 => CLK40MHzTTC);	
	
	-- vio_1_i : vio_1
	-- PORT MAP (
	-- clk => CLK40MHzTTC,
	-- probe_in0 => VIO_Test,
	-- probe_out0 => VIO_Test
	-- );	
end Behavioral;