#TTC Clock Input
# set_property PACKAGE_PIN T26 [get_ports GCK1_P]
# set_property IOSTANDARD LVDS_25 [get_ports GCK1_P]
# set_property PACKAGE_PIN T27 [get_ports GCK1_N]
# set_property IOSTANDARD LVDS_25 [get_ports GCK1_N]

#40 MHZ Crystal Clock Input
set_property PACKAGE_PIN U27 [get_ports GCK2_P]
set_property IOSTANDARD LVDS_25 [get_ports GCK2_P]
set_property PACKAGE_PIN U28 [get_ports GCK2_N]
set_property IOSTANDARD LVDS_25 [get_ports GCK2_N]

#TTCDEC RESET_B Output
set_property PACKAGE_PIN W28 [get_ports TTC_CTRL_21]
set_property IOSTANDARD LVCMOS33 [get_ports TTC_CTRL_21]
set_property DRIVE 4 [get_ports TTC_CTRL_21]

#TTCDEC P/D Output
set_property PACKAGE_PIN V27 [get_ports TTC_CTRL_26]
set_property IOSTANDARD LVCMOS33 [get_ports TTC_CTRL_26]
set_property DRIVE 4 [get_ports TTC_CTRL_26]

#TTC CLKSEL Output
set_property PACKAGE_PIN R29 [get_ports TTC_CTRL_27]
set_property IOSTANDARD LVCMOS33 [get_ports TTC_CTRL_27]
set_property DRIVE 4 [get_ports TTC_CTRL_27]

#I2C AVAGO 2 Input and Output
set_property PACKAGE_PIN J13 [get_ports AVAGO_SDA_2]
set_property IOSTANDARD LVCMOS33 [get_ports AVAGO_SDA_2]
set_property DRIVE 4 [get_ports AVAGO_SDA_2]
set_property PACKAGE_PIN K11 [get_ports AVAGO_SCL_2]
set_property IOSTANDARD LVCMOS33 [get_ports AVAGO_SCL_2]
set_property DRIVE 4 [get_ports AVAGO_SCL_2]

#I2C AVAGO 1 Input and Output
set_property PACKAGE_PIN K16 [get_ports AVAGO_SDA_1]
set_property IOSTANDARD LVCMOS33 [get_ports AVAGO_SDA_1]
set_property DRIVE 4 [get_ports AVAGO_SDA_1]
set_property PACKAGE_PIN H14 [get_ports AVAGO_SCL_1]
set_property IOSTANDARD LVCMOS33 [get_ports AVAGO_SCL_1]
set_property DRIVE 4 [get_ports AVAGO_SCL_1]

#I2C AVAGO 0 Input and Output
set_property PACKAGE_PIN F15 [get_ports AVAGO_SDA_0]
set_property IOSTANDARD LVCMOS33 [get_ports AVAGO_SDA_0]
set_property DRIVE 4 [get_ports AVAGO_SDA_0]
set_property PACKAGE_PIN F13 [get_ports AVAGO_SCL_0]
set_property IOSTANDARD LVCMOS33 [get_ports AVAGO_SCL_0]
set_property DRIVE 4 [get_ports AVAGO_SCL_0]

#I2C Power monitors Input and Output
set_property PACKAGE_PIN L15 [get_ports POWER_SDA]
set_property IOSTANDARD LVCMOS33 [get_ports POWER_SDA]
set_property DRIVE 4 [get_ports POWER_SDA]
set_property PACKAGE_PIN L16 [get_ports POWER_SCL]
set_property IOSTANDARD LVCMOS33 [get_ports POWER_SCL]
set_property DRIVE 4 [get_ports POWER_SCL]