----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 15.06.2015 16:55:25
-- Design Name: 
-- Module Name: serial_id_2 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


--library IEEE;
--use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- synthesis translate_off
-- synopsys translate_off
library unisim;
use unisim.vcomponents.all;
-- synopsys translate_on
-- synthesis translate_on
--use work.test_pkg.all;

entity serial_id_2 is
    port (
           clock_40  : in  std_logic;             
           start_seq     : in  std_logic;             
            --dq        : inout std_logic; --onewire serial line        
           singlewire_in: in std_logic;
           singlewire_out: out std_logic;
           
           serial_ready     : out std_logic;     -- set to 1 if checksum ok        
           serial_id   : out std_logic_vector (47 downto 0)                                                  
              );
end serial_id_2;

architecture behavioral of serial_id_2 is

signal din : std_logic; 

begin

process(clock_40)

variable Cmd      :  std_logic_vector(7 downto 0) := x"33";

variable feedback,crc : std_logic_vector(7 downto 0);

variable cnt: integer range 0 to 199:=0;
variable ticks: integer range 0 to 12;
variable slots: integer range 0 to 7;
variable blocks: integer range 0 to 11;
variable inp,dout : std_logic;
variable sdata : std_logic_vector(47 downto 0);
variable crcbit: std_logic;
variable s_ready: std_logic;
variable s_id: std_logic_vector (47 downto 0);

begin

if rising_edge(clock_40)then
if start_seq='1' then blocks:=0;slots:=0;ticks:=0;cnt:=0; s_ready:='0'; s_id:=(others=>'0'); end if;

din <=singlewire_in;

dout:='1';

if cnt=199 then -- 5 microsec ticks
  cnt:=0;  
  if ticks=2 then 
    if blocks < 10  then 
		sdata:=din & sdata(47 downto 1);  
		feedback:=(others=>(crc(0) xor din));
		feedback:=feedback and "10001100";
		crc := feedback xor '0' & crc(7 downto 1);
end if;

if blocks=10 then crcbit:=crcbit and not (crc(slots) xor din); end if;  


  end if;
  if ticks=12 then -- 13 ticks per slot
    ticks:=0;
    if slots=7 then -- 8 slots per block
      slots:=0;
      if blocks/=11 then blocks:=blocks+1; end if;
    else slots:=slots+1; end if;
  else ticks:=ticks+1; end if;
else cnt:=cnt+1; end if;

if blocks=2 then crc :=(others=>'0'); end if;
 
if blocks=2 then dout:= cmd(slots); end if; -- command data, to be framed by write start and recovery
if ticks=0 then dout:='0'; end if; -- read / write start
if ticks=12 then dout:='1'; end if; -- recovery 

if blocks=0 then dout:='0'; end if;--reset
if blocks=1 then dout:='1'; end if;--receive present pulse
if blocks=11 then dout:='1'; end if;-- all done, disable output

if blocks=9 then crcbit:='1'; end if;
if blocks=11 then s_ready:=crcbit; end if;
if dout='0' then singlewire_out<='0'; else singlewire_out<='1'; end if; 
if blocks=11 and crcbit='1' then s_id:=sdata; end if;
--sn_data<=sdata;
--crc(0):='1'; -- debug

serial_ready 	<= s_ready;
serial_id		<= s_id;

end if;

end process;     
          
end behavioral;