----------------------------------------------------------------------------------
-- Company: 		Johannes Gutenberg - Universitaet Mainz	
-- Engineer: 		Christian Kahra
-- 
-- Create Date:    12:04:49 05/19/2015 
-- Project Name:	 L1Topo_ControlFPGA
-- Module Name:    infrastructure - Behavioral 

-- Target Devices: Xilinx Kintex-7 xc7k325t-2ffg900
-- Tool versions:	 Xilinx ISE 14.7

----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;



entity infrastructure is port(

	sysclk40:			in  std_logic;
	xadc_control:		in  std_logic_vector(25 downto 0);
   xadc_status:		out std_logic_vector(22 downto 0);
	usr_access:			out std_logic_vector(31 downto 0);
	serialID_in:		in  std_logic;
	serialID_out:		out std_logic;
	serialID_pulse:	in  std_logic;
	serialID_status:	out std_logic_vector(48 downto 0)
	
);
end infrastructure;



architecture Behavioral of infrastructure is

begin


xadc: entity work.read_out_xadc
port map(
    drpclkin    => sysclk40,
    drpadress   => xadc_control( 6 downto  0),
    drp_control => xadc_control( 9 downto  7),
    drp_input   => xadc_control(25 downto 10),
    drp_output  => xadc_status( 15 downto  0),
    drp_ready   => xadc_status( 16),
    drp_alarms  => xadc_status( 22 downto 17)
);



USR_ACCESSE2_inst: USR_ACCESSE2
port map (
	CFGCLK => open,
	DATA => usr_access,
	DATAVALID => open
);


SERIALID_inst: entity work.serial_id_2
port map(
	clock_40			=> sysclk40,
	start_seq		=> serialID_pulse,
	singlewire_in	=> serialID_in,
	singlewire_out	=> serialID_out,
	serial_ready	=> serialID_status(0),
	serial_id		=> serialID_status(48 downto 1)
);

end Behavioral;

