 ----------------------------------------------------------------------------------
-- Company:Main University,Jagiellonian University
-- Engineer: Christian Kahra , Marek Palka
-- 
-- Create Date:    15:21:33 06/27/2014 
-- Design Name: 
-- Module Name:    clocks - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity clocks_controller is
	port(
		GCK1_P: in std_logic;
                GCK1_N: in std_logic;
		gck2: in std_logic;
		sysclk40_out: out std_logic;
		sysclk80_out: out std_logic;
                sysclk160_out : out std_logic;
		sysclk200_out: out std_logic;
		sysclk400_out: out std_logic;
                sysclk_reset : in std_logic;
		ethclk62_5_out: out std_logic;
		ethclk125_out: out std_logic;
		sysclk_pll_locked_out: out std_logic;
		eth_mmcm_locked_out: out std_logic;
		errorflagsReset: in std_logic;
		--clocksControl: in  std_logic_vector(2 downto 0);
		clocksStatus:  out std_logic_vector(2 downto 0)
	);
	
end clocks_controller;

architecture Behavioral of clocks_controller is

	signal sysclk40, sysclk40_unbuffered: std_logic;
	signal sysclk80, sysclk80_unbuffered: std_logic;
	signal sysclk200, sysclk200_unbuffered: std_logic;

	signal sysclk400, sysclk400_unbuffered: std_logic;
	signal sysclk_pll_feedback, sysclk_pll_feedback_unbuffered: std_logic;
	signal sysclk_pll_locked: std_logic;
	signal sysclk_pll_reset: std_logic;

	signal eth_gt_txoutclk_bufg: std_logic;
	signal ethclk125, ethclk125_unbuffered: std_logic;

	signal eth_mmcm_locked: std_logic;
	signal eth_mmcm_reset: std_logic;

	signal counter125mhz: unsigned(27 downto 0);

	signal gck2_buffered: std_logic;
	signal crystalclk, crystalclk_unbuffered: std_logic;
	signal crystalclk_pll_feedback, crystalclk_pll_feedback_unbuffered: std_logic;
	signal crystalclk_pll_locked: std_logic;
	signal crystalclk_pll_reset: std_logic;
	
	signal errorflags: std_logic_vector(2 downto 0);
        signal sysclk160_unbuffered, sysclk160 : std_logic;
        signal sysclk_reset_delayed : std_logic:='0';
begin

  DELAY_PLL_RESET: entity work.delay
    generic map (
      VECTOR_WIDTH => 1,
      DELAY_INT    => 50)
    port map (
      CLK              => sysclk40,
      DELAY_VECTOR_IN(0)  => sysclk_reset,
      DELAY_VECTOR_OUT(0) => sysclk_reset_delayed);

  clocksStatus          <= errorflags;
  sysclk_pll_reset      <= sysclk_reset_delayed and sysclk_pll_locked;
  sysclk_pll_locked_out <= sysclk_pll_locked;

  process(crystalclk)
  begin
    if rising_edge(crystalclk) then
      if errorflagsReset = '1' then
        errorflags(0) <= '0';
      elsif sysclk_pll_locked = '0' then
        errorflags(0) <= '1';
      end if;
    end if;
  end process;
  
  process(sysclk40)
  begin
    if rising_edge(sysclk40) then
      if errorflagsReset = '1' then errorflags(2) <= '0';
      elsif crystalclk_pll_locked = '0' then errorflags(2) <= '1';
      end if;
    end if;
  end process;

  GLOBAL_CLK_MANAGER_INST : entity work.global_clk_manager
    port map(
      gck1_p  => GCK1_P,
      gck1_n  => GCK1_N,
      clk_40  => sysclk40,
      clk_80  => sysclk80,
      clk_160 => sysclk160,
      clk_400 => sysclk400,
      clk_200 => sysclk200,
      reset   => sysclk_pll_reset,
      locked  => sysclk_pll_locked
      );

  sysclk40_out  <= sysclk40;
  sysclk80_out  <= sysclk80;
  sysclk160_out <= sysclk160;
  sysclk200_out <= sysclk200;
  sysclk400_out <= sysclk400;

  eth_mmcm_reset      <= '0';
  eth_mmcm_locked_out <= eth_mmcm_locked;

  process(crystalclk)
  begin
    if rising_edge(crystalclk) then
      if errorflagsReset = '1' then errorflags(1) <= '0';
      elsif eth_mmcm_locked = '0' then errorflags(1) <= '1';
      end if;
    end if;
  end process;


end Behavioral;

