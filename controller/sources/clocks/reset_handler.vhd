library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNISIM;
use UNISIM.VComponents.all;

use work.rod_l1_topo_types_const.all;

entity reset_handler is
port (
        CR_CLK_IN         : in std_logic;
	GCK_CLK_IN        : in std_logic;
	ETH_CLK_IN        : in std_logic;
	GCK_CLK_LOCKED_IN : in std_logic;
	ETH_CLK_LOCKED_IN : in std_logic;
	
	ROD_RST_OUT   : out std_logic;
	TTC_RST_OUT   : out std_logic;
	SLINK_RST_OUT : out std_logic_vector(31 downto 0);

	DDR_RDY_IN    : in std_logic;
	
	GLOBAL_RST : in std_logic;
	DDR_IPB_RST_IN : in std_logic;
	ROD_IPB_RST_IN : in std_logic;
	TTC_IPB_RST_IN : in std_logic;
	SLINK_IPB_RST_IN : in std_logic_vector(31 downto 0)
);
end reset_handler;

architecture Behavioral of reset_handler is

  signal rod_rst_sync  : std_logic_vector(4 downto 0) := (others => '0');
  signal ttc_rst_sync   : std_logic_vector(4 downto 0) := (others => '0');
  type slink_rst_array is array (4 downto 0) of std_logic_vector(31 downto 0);
  signal slink_rst_sync : slink_rst_array := (others => (others => '0'));
  signal rod_rst_out_l, ttc_rst_out_l : std_logic:='0';
  signal slink_rst_out_l : std_logic_vector(31 downto 0) := (others => '0');
  
begin

  GEN_LONGER_RESET: for i in 1 to 4 generate
    FIVE_TICKS_FOR_TIG_AND_FIFO:process(GCK_CLK_IN)
    begin
      if rising_edge(GCK_CLK_IN) then
        rod_rst_sync(i) <= rod_rst_sync(i-1);
        ttc_rst_sync(i) <= ttc_rst_sync(i-1);
        slink_rst_sync(i) <= slink_rst_sync(i - 1);
      end if;    
    end process FIVE_TICKS_FOR_TIG_AND_FIFO;
  end generate GEN_LONGER_RESET;
  
  TICKS_FOR_FIFO:process(GCK_CLK_IN)
  begin
    if rising_edge(GCK_CLK_IN) then
      rod_rst_sync(0) <= (not (DDR_RDY_IN)) or ROD_IPB_RST_IN or GLOBAL_RST;
      ttc_rst_sync(0) <= TTC_IPB_RST_IN or GLOBAL_RST;
      slink_rst_sync(0) <= SLINK_IPB_RST_IN; 
      rod_rst_out_l <= rod_rst_sync(4) or rod_rst_sync(3) or rod_rst_sync(2) or rod_rst_sync(1) or rod_rst_sync(0); 
      ttc_rst_out_l <= ttc_rst_sync(4) or ttc_rst_sync(3) or ttc_rst_sync(2) or ttc_rst_sync(1) or ttc_rst_sync(0); 
      slink_rst_out_l <= slink_rst_sync(4) or slink_rst_sync(3) or slink_rst_sync(2) or slink_rst_sync(1) or slink_rst_sync(0);
      TTC_RST_OUT <= ttc_rst_out_l;
    end if;
  end process TICKS_FOR_FIFO;
  
  CROSS_CLK_DOMAINS: process(CR_CLK_IN)
  begin
    if rising_edge(CR_CLK_IN) then
      ROD_RST_OUT <= rod_rst_out_l;
      SLINK_RST_OUT <= slink_rst_out_l;
    end if;
  end process;
  
  
end Behavioral;

