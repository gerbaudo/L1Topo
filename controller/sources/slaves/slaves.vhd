-- The ipbus slaves live in this entity - modify according to requirements
--
-- Ports can be added to give ipbus slaves access to the chip top level.
--
-- Dave Newbold, February 2011

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.l1topo_package.all;
use work.rod_l1_topo_types_const.all;
use work.ipbus.ALL;
use work.ipbus_addr_decode.ALL;

entity slaves is
	port(
		sysclk40: in std_logic;
		sysclk80: in std_logic;
		sysclk200: in std_logic;
		sysclk400: in std_logic;
		ipb_rst: in std_logic;
		ipb_in: in ipb_wbus;
		ipb_out: out ipb_rbus;
		gck_mmcm_locked: in std_logic;
		ipbBridge_u1_in:  in  arraySLV2(2 downto 0);
		ipbBridge_u1_out: out arraySLV2(4 downto 0);
		ipbBridge_u2_in:  in  arraySLV2(2 downto 0);
		ipbBridge_u2_out: out arraySLV2(4 downto 0);
		
		clocksStatus: in std_logic_vector(3 downto 0);
		
		ctrlbus_delay_control: out arraySLV6(41 downto 0);
		ctrlbus_delay_pulse:   out arraySLV2(41 downto 0);
		ctrlbus_delay_status:  in  arraySLV5(41 downto 0);
		
		
		i2c_control:	out std_logic_vector(26 downto 0);
		i2c_pulse:		out std_logic;
		i2c_status:		in  std_logic_vector( 9 downto 0);
		
		
		ttcStatus: in std_logic_vector(1 downto 0);
		TTCBridgeControl: out std_logic_vector(2 downto 0);
		
		xadc_control:  	out std_logic_vector(25 downto 0);
		xadc_status:		in  std_logic_vector(22 downto 0);
		usr_access:			in  std_logic_vector(31 downto 0);
		serialID_pulse:	out std_logic;
		serialID_status:	in  std_logic_vector(48 downto 0);
		
--		jcLossOfLock: 			in  std_logic;
--		jcInterruptClk1Bad:	in  std_logic;
--		jcSlaveSelect_b:		out std_logic;
--		jcSerialDataOut:		out std_logic;
--		jcSerialDataIn:		in  std_logic;
--		jcSerialClk:			out std_logic;
		
		ModuleID: in std_logic_vector(31 downto 0);
		Serial_No_Module: in std_logic_vector(31 downto 0);
		Serial_No_ExtensionBoard: in std_logic_vector(31 downto 0);
		Firmware_Version: in std_logic_vector(31 downto 0);
		FirmwareDate:		in std_logic_vector(31 downto 0);
		FirmwareRevision:	in std_logic_vector(31 downto 0);
		
		GeneralControl: out std_logic_vector(31 downto 0);
		GeneralPulse_out:   out std_logic_vector(31 downto 0);
		
		
		
		rod_control_registers: out rod_control_registers_array;
      rod_status_registers:  in  rod_status_registers_array
		
		--debugIPBusBridgeU1: out std_logic_vector(227 downto 0);
--		debugIPBusBridgeU2: out std_logic_vector(227 downto 0)
		
	);

end slaves;




architecture rtl of slaves is

	constant NSLV: positive := 99;
	signal ipbw: ipb_wbus_array(NSLV-1 downto 0);
	signal ipbr: ipb_rbus_array(NSLV-1 downto 0);
	
	signal ipbr_ack: std_logic_vector(NSLV-1 downto 0);
	attribute s: string;
	attribute s of ipbr_ack: signal is "true";


	signal GeneralStatus_32bit: std_logic_vector(31 downto 0);
	signal ttcStatusError: std_logic;
	signal TTC_Status_32bit: std_logic_vector(31 downto 0);
	signal JitterCleaner_Status_32bit: std_logic_vector(31 downto 0);
	signal I2C_DataOut_32bit: std_logic_vector(31 downto 0);
	signal SPI_DataOut_32bit: std_logic_vector(31 downto 0);
	signal ipbBridgeU1ErrorCounter: std_logic_vector(31 downto 0);
	signal ipbBridgeU2ErrorCounter: std_logic_vector(31 downto 0);
	signal ethErrorCounter_32bit: std_logic_vector(31 downto 0);
	signal i2c_control_32bit:	std_logic_vector(31 downto 0);
	signal i2c_pulse_32bit:		std_logic_vector(31 downto 0);
	signal i2c_status_32bit: 	std_logic_vector(31 downto 0);
	
	signal serialID_status_64bit:	std_logic_vector(63 downto 0);
	
	signal ctrlbus_delay_control_252bit: std_logic_vector(251 downto 0);
	signal ctrlbus_delay_pulse_84bit: std_logic_vector(83 downto 0);
	signal ctrlbus_delay_status_210bit: std_logic_vector(209 downto 0);
	signal ctrlbus_codeErrorCounter: arraySLV8(41 downto 0);
	signal ctrlbus_codeErrorCounter_336bit: std_logic_vector(335 downto 0);
	
	signal ipbBridgeCurrentIDelayValue_u1_32bit: std_logic_vector(31 downto 0);
	signal ipbBridgeCurrentIDelayValue_u2_32bit: std_logic_vector(31 downto 0);
	
	signal GeneralControl_32bit: std_logic_vector(31 downto 0);
	signal GeneralPulse: std_logic_vector(31 downto 0);
	signal BC_Reset_32bit: std_logic_vector(31 downto 0);
	signal TTC_Bridge_control_32bit: std_logic_vector(31 downto 0);
	signal SPI_DataIn_32bit: std_logic_vector(31 downto 0);
	signal SPI_Strobe_32bit: std_logic_vector(31 downto 0);
	signal ReadRequestDelay_32bit: std_logic_vector(31 downto 0);
	signal InputLatency_32bit: std_logic_vector(31 downto 0);
	signal SystemAce_32bit: std_logic_vector(31 downto 0);
	
	signal ROD_Hist_128bit: std_logic_vector(127 downto 0);
	signal ROD_Hist_Conf_128bit: std_logic_vector(127 downto 0);
	signal ROD_Err_Counter_352bit: std_logic_vector(351 downto 0);
	signal ROD_Gen_Dbg_64bit: std_logic_vector(63 downto 0);
	signal ROD_Input_To_Out_Assignment_352bit: std_logic_vector(351 downto 0);
	signal ROD_CrateNumber_320bit: std_logic_vector(319 downto 0);
	signal ROD_IDelays_128bit: std_logic_vector(127 downto 0);
	signal ROD_ByteCounter_128bit: std_logic_vector(127 downto 0);
	signal ROD_DDR_ErrorCounter_128bit: std_logic_vector(127 downto 0);
	signal ROD_DDR_Gen_Dbg_64bit: std_logic_vector(63 downto 0);
	signal ROD_SlinkStatus_64bit: std_logic_vector(63 downto 0);
	signal TRANSPORT_ON_CRC_96bit: std_logic_vector(95 downto 0);
	signal ROD_DDR_Delay_Readback_128bit: std_logic_vector(127 downto 0);
	signal ROD_SLink_ID_384bit: std_logic_vector(383 downto 0);
	signal ROD_Counter_Debug_1024bit: std_logic_vector(1023 downto 0);
	signal ROD_DummyControl_8192bit: std_logic_vector(8191 downto 0);
	signal ROD_DummyPulse_8192bit: std_logic_vector(8191 downto 0);
	signal ROD_DummyStatus_8192bit: std_logic_vector(8191 downto 0);

	signal ipbBridgeU1Control: std_logic_vector(0 downto 0);
	signal ipbBridgeU1Status:  std_logic_vector(14 downto 0);
	
	
	signal ipbBridgeU2Control: std_logic_vector(0 downto 0);
	signal ipbBridgeU2Status:  std_logic_vector(14 downto 0);
	
	
	
	signal ipbBridgeIDelayCurrentValue_64bit: std_logic_vector(63 downto 0);
	signal ipbBridgeErrorCounter_64bit: std_logic_vector(63 downto 0);
	signal ipbBridgeErrorCounterReset_32bit: std_logic_vector(31 downto 0);

	signal iconControl0, iconControl1: std_logic_vector(35 downto 0);
        signal ipbus_sel: std_logic_vector(7 downto 0);
        signal tmp_rst: std_logic_vector(31 downto 0);
begin

--icon: entity work.chipscope_icon
--port map(
--	CONTROL0 => iconControl0,
--	CONTROL1 => iconControl1
--);


  fabric: entity work.ipbus_fabric
    generic map(NSLV => NSLV)
    port map(
      ipb_in => ipb_in,
      ipb_out => ipb_out,
      ipb_to_slaves => ipbw,
      ipb_from_slaves => ipbr
	);

  
-- ipbus_sel <= std_logic_vector(to_unsigned(ipbus_addr_sel(ipb_in.ipb_addr),8));
 
  --ipbus_sel_ila_inst: entity work.ipbus_sel_ila
  --  port map(
  --  clk => sysclk40,
  --  Probe0 => ipbus_sel,
  --  Probe1(0) => tmp_rst(0)--rod_control_registers(6)(0)
  --  );
  rod_control_registers(6) <= tmp_rst;

IPBR_ACK_GEN: for sl in NSLV-1 downto 0 generate
	ipbr_ack(sl) <= ipbr(sl).ipb_ack;
        
end generate;


ipbBridgeU1Control(0) <= GeneralPulse(0);

ipbBridgeU1: entity work.ipbusBridge
generic map(
		bridge => 1,
		DELAY_GROUP_NAME => "bank32_delay_group"
		)
	port map(
		sysclk => sysclk40,
		parallelclk => sysclk80,
		serialclk => sysclk400,
		idelayctrl_refclk => sysclk200,
		mmcm_locked => gck_mmcm_locked,
		ipb_write => ipbw(97),
		ipbBridgeBus_out => ipbBridge_u1_out,
		ipbBridgeBus_in => ipbBridge_u1_in,
		ipb_read => ipbr(97),
		control => ipbBridgeU1Control,
		status => ipbBridgeCurrentIDelayValue_u1_32bit(14 downto 0),
		request_errorCounter => ctrlbus_codeErrorCounter(4 downto 0),
		response_errorCounter => ctrlbus_codeErrorCounter(20 downto 18),
		iconControl => iconControl0
	);

ipbBridgeU2Control(0) <= GeneralPulse(0);

ipbBridgeU2: entity work.ipbusBridge
generic map(
		bridge => 2,
		DELAY_GROUP_NAME => "bank17_delay_group"
		)
	port map(
		sysclk => sysclk40,
		parallelclk => sysclk80,
		serialclk => sysclk400,
		idelayctrl_refclk => sysclk200,
		mmcm_locked => gck_mmcm_locked,
		ipb_write => ipbw(98),
		ipbBridgeBus_out => ipbBridge_u2_out,
		ipbBridgeBus_in => ipbBridge_u2_in,
		ipb_read => ipbr(98),
		control => ipbBridgeU2Control,
		status => ipbBridgeCurrentIDelayValue_u2_32bit(14 downto 0),
		request_errorCounter => ctrlbus_codeErrorCounter(25 downto 21),
		response_errorCounter => ctrlbus_codeErrorCounter(41 downto 39),
		iconControl => iconControl1
	);







slave0_ModuleID: entity work.ipbus_slave_status
		generic map(addr_width => 0)
		port map(
			clk => sysclk40,
			reset => ipb_rst,
			ipbus_in => ipbw(0),
			ipbus_out => ipbr(0),
			d => ModuleID
		);

-------------------------------------------


slave1_SerialNo_Module: entity work.ipbus_slave_status
        generic map(addr_width => 0)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(1),
            ipbus_out => ipbr(1),
            d => Serial_No_Module
        );

-------------------------------------------


slave2_SerialNo_ExtensionBoard: entity work.ipbus_slave_status
        generic map(addr_width => 0)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(2),
            ipbus_out => ipbr(2),
            d => Serial_No_ExtensionBoard
        );

-------------------------------------------


slave3_Firmware_Version: entity work.ipbus_slave_status
        generic map(addr_width => 0)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(3),
            ipbus_out => ipbr(3),
            d => Firmware_Version
        );
		  



----------------------------------------------------------------

   
slave94_Usr_Access: entity work.ipbus_slave_status
        generic map(addr_width => 0)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(94),
            ipbus_out => ipbr(94),
            d => usr_access
        );
		  


-------------------------------------------


slave95_FirmwareDate: entity work.ipbus_slave_status
        generic map(addr_width => 0)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(95),
            ipbus_out => ipbr(95),
            d => FirmwareDate
        );


-------------------------------------------


slave96_FirmwareRevision: entity work.ipbus_slave_status
        generic map(addr_width => 0)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(96),
            ipbus_out => ipbr(96),
            d => FirmwareRevision
        );


-------------------------------------------

	GeneralStatus_32bit(4 downto 1) <= clocksStatus;

slave4_GeneralStatus: entity work.ipbus_slave_status
        generic map(addr_width => 0)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(4),
            ipbus_out => ipbr(4),
            d => GeneralStatus_32bit
        );

-------------------------------------------
	
	
	process(sysclk40) begin
		if rising_edge(sysclk40) then
			if GeneralPulse(0)='1' then ttcStatusError <= '0';
			elsif ttcStatus(0)='0' or ttcStatus(1)='0' then ttcStatusError <= '1';
			end if;
		end if;
	end process;

	TTC_Status_32bit(2 downto 0) <= ttcStatusError & ttcStatus;
	

slave5_TTC_Status: entity work.ipbus_slave_status
        generic map(addr_width => 0)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(5),
            ipbus_out => ipbr(5),
            d => TTC_Status_32bit
        );

-------------------------------------------

--	JitterCleaner_Status_32bit(1 downto 0) <= jcInterruptClk1Bad & jcLossOfLock;
--
--slave6_JitterCleaner_Status: entity work.ipbus_slave_status
--        generic map(addr_width => 0)
--        port map(
--            clk => sysclk40,
--            reset => ipb_rst,
--            ipbus_in => ipbw(6),
--            ipbus_out => ipbr(6),
--            d => JitterCleaner_Status_32bit
--        );


-------------------------------------------

	
slave7_ipbBridgeErrorCounterU1: entity work.ipbus_slave_status
        generic map(addr_width => 0)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(7),
            ipbus_out => ipbr(7),
            d => ipbBridgeU1ErrorCounter
        );


-------------------------------------------

	
slave8_ipbBridgeErrorCounterU2: entity work.ipbus_slave_status
        generic map(addr_width => 0)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(8),
            ipbus_out => ipbr(8),
            d => ipbBridgeU2ErrorCounter
        );


-------------------------------------------

	ethErrorCounter_32bit <= X"00000000";

slave9_EthErrorCounter: entity work.ipbus_slave_status
        generic map(addr_width => 0)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(9),
            ipbus_out => ipbr(9),
            d => ethErrorCounter_32bit
        );


-------------------------------------------



-------------------------------------------

--	ipbBridgeCurrentIDelayValue_u1_32bit(31 downto 15) <= (31 downto 15 => '0');

slave11_ipbBridgeCurrentIDelayValue_U1: entity work.ipbus_slave_status
        generic map(addr_width => 0)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(11),
            ipbus_out => ipbr(11),
            d => ipbBridgeCurrentIDelayValue_u1_32bit
        );


-------------------------------------------

--	ipbBridgeCurrentIDelayValue_u2_32bit <= X"00000000";

slave12_ipbBridgeCurrentIDelayValue_U2: entity work.ipbus_slave_status
        generic map(addr_width => 0)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(12),
            ipbus_out => ipbr(12),
            d => ipbBridgeCurrentIDelayValue_u2_32bit
        );


-------------------------------------------
-------------------------------------------


slave13_GeneralControl: entity work.ipbus_slave_control
    generic map(addr_width => 0)
    port map(
        clk => sysclk40,
        reset => ipb_rst,
        ipbus_in => ipbw(13),
        ipbus_out => ipbr(13),
        q => GeneralControl_32bit
    );

	GeneralControl <= GeneralControl_32bit;

-------------------------------------------


slave14_GeneralPulse: entity work.ipbus_slave_reg_pulse
    generic map(addr_width => 0)
    port map(
        clk => sysclk40,
        reset => ipb_rst,
        ipbus_in => ipbw(14),
        ipbus_out => ipbr(14),
        q => GeneralPulse
    );

	GeneralPulse_out <= GeneralPulse;


	serialID_pulse <= GeneralPulse(1);

-------------------------------------------


slave15_BC_Reset: entity work.ipbus_slave_reg_pulse
    generic map(addr_width => 0)
    port map(
        clk => sysclk40,
        reset => ipb_rst,
        ipbus_in => ipbw(15),
        ipbus_out => ipbr(15),
        q => BC_Reset_32bit
    );


-------------------------------------------


slave16_TTC_Bridge_control: entity work.ipbus_slave_reg_pulse
    generic map(addr_width => 0)
    port map(
        clk => sysclk40,
        reset => ipb_rst,
        ipbus_in => ipbw(16),
        ipbus_out => ipbr(16),
        q => TTC_Bridge_control_32bit
    );

	TTCBridgeControl <= TTC_Bridge_control_32bit(2 downto 0);


-------------------------------------------


slave17_ReadRequestDelay: entity work.ipbus_slave_control
    generic map(addr_width => 0)
    port map(
        clk => sysclk40,
        reset => ipb_rst,
        ipbus_in => ipbw(17),
        ipbus_out => ipbr(17),
        q => ReadRequestDelay_32bit
    );


-------------------------------------------


slave18_InputLatency: entity work.ipbus_slave_control
    generic map(addr_width => 0)
    port map(
        clk => sysclk40,
        reset => ipb_rst,
        ipbus_in => ipbw(18),
        ipbus_out => ipbr(18),
        q => InputLatency_32bit
    );


-------------------------------------------



-------------------------------------------


slave20_i2c_control: entity work.ipbus_slave_control
generic map(addr_width => 0)
port map(
	clk			=> sysclk40,
	reset			=> ipb_rst,
	ipbus_in		=> ipbw(20),
	ipbus_out	=> ipbr(20),
	q				=> i2c_control_32bit
);

	i2c_control <= i2c_control_32bit(26 downto 0);


-------------------------------------------


slave21_i2c_pulse: entity work.ipbus_slave_reg_pulse
generic map(addr_width => 0)
port map(
	clk			=> sysclk40,
	reset			=> ipb_rst,
	ipbus_in		=> ipbw(21),
	ipbus_out	=> ipbr(21),
	q				=> i2c_pulse_32bit
);

	i2c_pulse <= i2c_pulse_32bit(0);


-------------------------------------------


i2c_status_32bit(9 downto 0) <= i2c_status;

slave22_i2c_status: entity work.ipbus_slave_status
generic map(addr_width => 0)
port map(
	clk			=> sysclk40,
	reset 		=> ipb_rst,
	ipbus_in		=> ipbw(22),
	ipbus_out	=> ipbr(22),
	d				=> i2c_status_32bit
);


-------------------------------------------


slave23_XADC_Interface: entity work.ipbus_slave_xadc
     port map(
         clk => sysclk40,
			reset => ipb_rst,
			ipbus_in => ipbw(23),
			ipbus_out => ipbr(23),
			xadc_control => xadc_control,
			xadc_status  => xadc_status   
     );


-------------------------------------------


--slave24_JitterCleaner: entity work.ipbus_slave_spi
--     port map(
--         clk => sysclk40,
--         reset => ipb_rst,
--         ipbus_in => ipbw(24),
--         ipbus_out => ipbr(24),
--         sclk => jcSerialClk,
--			ssb => jcSlaveSelect_b,
--			mosi => jcSerialDataOut,
--			miso => jcSerialDataIn
--     );


-------------------------------------------


slave25_TestRAM: entity work.ipbus_ram
        generic map(addr_width => 10)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(25),
            ipbus_out => ipbr(25)
        );


-------------------------------------------


slave26_Ctrlbus_Delay_Control: entity work.ipbus_slave_control_generic
     generic map(
		numberOfRegisters => 42,
		registerWidth => 6)
     port map(
         clk => sysclk40,
         reset => ipb_rst,
         ipbus_in => ipbw(26),
         ipbus_out => ipbr(26),
         q => ctrlbus_delay_control_252bit
     );
	  
CTRLBUS_DELAY_CONTROL_GEN: for i in 41 downto 0 generate
		ctrlbus_delay_control(i) <= ctrlbus_delay_control_252bit(i*6+5 downto i*6);
end generate;

	  
-------------------------------------------


slave27_Ctrlbus_Delay_Pulse: entity work.ipbus_slave_pulse_generic
    generic map(
		numberOfRegisters => 42,
		registerWidth => 2
	 )
    port map(
        clk => sysclk40,
        reset => ipb_rst,
        ipbus_in => ipbw(27),
        ipbus_out => ipbr(27),
        q => ctrlbus_delay_pulse_84bit
    );
	  
CTRLBUS_DELAY_PULSE_GEN: for i in 41 downto 0 generate
		ctrlbus_delay_pulse(i) <= ctrlbus_delay_pulse_84bit(i*2+1 downto i*2);
end generate;
	  
	  
-------------------------------------------


CTRLBUS_DELAY_STATUS_GEN: for i in 41 downto 0 generate
	ctrlbus_delay_status_210bit(i*5+4 downto i*5) <= ctrlbus_delay_status(i);
end generate;

slave28_Ctrlbus_Delay_Status: entity work.ipbus_slave_status_generic
        generic map(
			numberOfRegisters => 42,
			registerWidth => 5)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(28),
            ipbus_out => ipbr(28),
            d => ctrlbus_delay_status_210bit
        );
	  
	  
-------------------------------------------

CTRLBUS_CODEERRORCOUNTER_GEN: for i in 41 downto 0 generate
	ctrlbus_codeErrorCounter_336bit(i*8+7 downto i*8) <= ctrlbus_codeErrorCounter(i);
end generate;

slave29_Ctrlbus_CodeErrorCounter: entity work.ipbus_slave_status_generic
        generic map(
			numberOfRegisters => 42,
			registerWidth => 8)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(29),
            ipbus_out => ipbr(29),
            d => ctrlbus_codeErrorCounter_336bit
        );
		  

-------------------------------------------


slave32_OV_Busy: entity work.ipbus_slave_control
    generic map(addr_width => 0)
    port map(
        clk => sysclk40,
        reset => ipb_rst,
        ipbus_in => ipbw(32),
        ipbus_out => ipbr(32),
        q => rod_control_registers(0)
    );


-------------------------------------------


slave33_GenerateL1Accept: entity work.ipbus_slave_reg_pulse
    generic map(addr_width => 0)
    port map(
        clk => sysclk40,
        reset => ipb_rst,
        ipbus_in => ipbw(33),
        ipbus_out => ipbr(33),
        q => rod_control_registers(1)
    );


-------------------------------------------


slave34_RunTypeNumber: entity work.ipbus_slave_control
    generic map(addr_width => 0)
    port map(
        clk => sysclk40,
        reset => ipb_rst,
        ipbus_in => ipbw(34),
        ipbus_out => ipbr(34),
        q => rod_control_registers(2)
    );


-------------------------------------------


slave35_TriggerType: entity work.ipbus_slave_control
    generic map(addr_width => 0)
    port map(
        clk => sysclk40,
        reset => ipb_rst,
        ipbus_in => ipbw(35),
        ipbus_out => ipbr(35),
        q => rod_control_registers(3)
    );


-------------------------------------------


slave36_Level1ID_Gen: entity work.ipbus_slave_control
    generic map(addr_width => 0)
    port map(
        clk => sysclk40,
        reset => ipb_rst,
        ipbus_in => ipbw(36),
        ipbus_out => ipbr(36),
        q => rod_control_registers(4)
    );


-------------------------------------------


slave37_RunReset: entity work.ipbus_slave_reg_pulse
    generic map(addr_width => 0)
    port map(
        clk => sysclk40,
        reset => ipb_rst,
        ipbus_in => ipbw(37),
        ipbus_out => ipbr(37),
        q => rod_control_registers(5)
    );


-------------------------------------------


slave38_ROD_Sys_FirmwareVersion: entity work.ipbus_slave_status
        generic map(addr_width => 0)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(38),
            ipbus_out => ipbr(38),
            d => rod_status_registers(0)
        );


-------------------------------------------


slave39_Run_Gen_Debug: entity work.ipbus_slave_status
        generic map(addr_width => 0)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(39),
            ipbus_out => ipbr(39),
            d => rod_status_registers(1)
        );


-------------------------------------------


slave40_RODReset: entity work.ipbus_slave_reg_pulse
    generic map(addr_width => 0,
                enable_ila => 0
                )
    port map(
        clk => sysclk40,
        reset => ipb_rst,
        ipbus_in => ipbw(40),
        ipbus_out => ipbr(40),
        q => tmp_rst--rod_control_registers(6)
    );


-------------------------------------------


slave41_Busy_Idle_Fr_Conf: entity work.ipbus_slave_control
    generic map(addr_width => 0)
    port map(
        clk => sysclk40,
        reset => ipb_rst,
        ipbus_in => ipbw(41),
        ipbus_out => ipbr(41),
        q => rod_control_registers(7)
    );


-------------------------------------------


slave42_FifoThreshold_L1A: entity work.ipbus_slave_control
    generic map(addr_width => 0)
    port map(
        clk => sysclk40,
        reset => ipb_rst,
        ipbus_in => ipbw(42),
        ipbus_out => ipbr(42),
        q => rod_control_registers(8)
    );


-------------------------------------------


slave43_FifoThreshold_Data: entity work.ipbus_slave_control
    generic map(addr_width => 0)
    port map(
        clk => sysclk40,
        reset => ipb_rst,
        ipbus_in => ipbw(43),
        ipbus_out => ipbr(43),
        q => rod_control_registers(9)
    );


-------------------------------------------


slave44_Busy_Idle_Fr: entity work.ipbus_slave_status
    generic map(addr_width => 0)
    port map(
        clk => sysclk40,
        reset => ipb_rst,
        ipbus_in => ipbw(44),
        ipbus_out => ipbr(44),
        d => rod_status_registers(2)
    );


-------------------------------------------


ROD_Hist_Gen: for i in 3 downto 0 generate begin
	ROD_Hist_128bit(i*32+31 downto i*32) <= rod_status_registers(3+i);
end generate;

slave45_ROD_Histogram: entity work.ipbus_slave_status
        generic map(addr_width => 2)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(45),
            ipbus_out => ipbr(45),
            d => ROD_Hist_128bit
        );


-------------------------------------------


slave46_HistogramConfiguration: entity work.ipbus_slave_control
        generic map(addr_width => 2)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(46),
            ipbus_out => ipbr(46),
            q => ROD_Hist_Conf_128bit
        );

ROD_Hist_Conf_Gen: for i in 3 downto 0 generate begin
	rod_control_registers(10+i) <= ROD_Hist_Conf_128bit(i*32+31 downto i*32);
end generate;

-------------------------------------------


slave47_ROD_FifoStat: entity work.ipbus_slave_status
        generic map(addr_width => 0)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(47),
            ipbus_out => ipbr(47),
            d => rod_status_registers(7)
        );


-------------------------------------------

ROD_Err_Counter_Gen: for i in 10 downto 0 generate begin
	ROD_Err_Counter_352bit(i*32+31 downto i*32) <= rod_status_registers(8+i);
end generate;

slave48_ROD_ErrorCounter: entity work.ipbus_slave_status_N
        generic map(numberOfRegisters => 11)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(48),
            ipbus_out => ipbr(48),
            d => ROD_Err_Counter_352bit
        );


-------------------------------------------

ROD_Gen_Debug_Gen: for i in 1 downto 0 generate begin
	ROD_Gen_Dbg_64bit(i*32+31 downto i*32) <= rod_status_registers(19+i);
end generate;

slave49_ROD_Gen_Debug: entity work.ipbus_slave_status
        generic map(addr_width => 1)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(49),
            ipbus_out => ipbr(49),
            d => ROD_Gen_Dbg_64bit
        );


-------------------------------------------


slave50_ROD_Input_To_Out_Assignment: entity work.ipbus_slave_control_N
    generic map(numberOfRegisters => 11)
    port map(
        clk => sysclk40,
        reset => ipb_rst,
        ipbus_in => ipbw(50),
        ipbus_out => ipbr(50),
        q => ROD_Input_To_Out_Assignment_352bit
    );


ROD_Input_To_Out_Assignment_Gen: for i in 10 downto 0 generate begin
	rod_control_registers(14+i) <= ROD_Input_To_Out_Assignment_352bit(i*32+31 downto i*32);
end generate;

-------------------------------------------


slave51_ROD_CrateNumber: entity work.ipbus_slave_control_N
        generic map(numberOfRegisters => 10)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(51),
            ipbus_out => ipbr(51),
            q => ROD_CrateNumber_320bit
        );


ROD_CrateNumber_Gen: for i in 9 downto 0 generate begin
	rod_control_registers(25+i) <= ROD_CrateNumber_320bit(i*32+31 downto i*32);
end generate;

-------------------------------------------


slave52_LinkReset: entity work.ipbus_slave_reg_pulse
        generic map(addr_width => 0)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(52),
            ipbus_out => ipbr(52),
            q => rod_control_registers(35)
        );

-------------------------------------------


slave53_LinkEnable: entity work.ipbus_slave_control
        generic map(addr_width => 0)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(53),
            ipbus_out => ipbr(53),
            q => rod_control_registers(36)
        );


-------------------------------------------


slave54_IDelays: entity work.ipbus_slave_control
        generic map(addr_width => 2)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(54),
            ipbus_out => ipbr(54),
            q => ROD_IDelays_128bit
        );


ROD_IDelays_Gen: for i in 3 downto 0 generate begin
	rod_control_registers(37+i) <= ROD_IDelays_128bit(i*32+31 downto i*32);
end generate;

-------------------------------------------


slave55_ROD_SynStatus: entity work.ipbus_slave_status
        generic map(addr_width => 0)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(55),
            ipbus_out => ipbr(55),
            d => rod_status_registers(21)
        );


-------------------------------------------


slave56_ROD_ByteCounter: entity work.ipbus_slave_status
        generic map(addr_width => 2)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(56),
            ipbus_out => ipbr(56),
            d => ROD_ByteCounter_128bit
        );


ROD_ByteCounter_Gen: for i in 3 downto 0 generate begin
	ROD_ByteCounter_128bit(i*32+31 downto i*32) <= rod_status_registers(22+i);
end generate;

-------------------------------------------


slave57_ROD_DDR_ErrorCounter: entity work.ipbus_slave_status
        generic map(addr_width => 2)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(57),
            ipbus_out => ipbr(57),
            d => ROD_DDR_ErrorCounter_128bit
        );
		  
		  
ROD_DDR_ErrorCounter_Gen: for i in 3 downto 0 generate begin
	ROD_DDR_ErrorCounter_128bit(i*32+31 downto i*32) <= rod_status_registers(26+i);
end generate;

-------------------------------------------

ROD_DDR_Gen_Dbg: for i in 1 downto 0 generate begin
	ROD_DDR_Gen_Dbg_64bit(i*32+31 downto i*32) <= rod_status_registers(30+i);
end generate;

slave58_DDR_Gen_Dbg: entity work.ipbus_slave_status
        generic map(addr_width => 1)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(58),
            ipbus_out => ipbr(58),
            d => ROD_DDR_Gen_Dbg_64bit
        );


-------------------------------------------


slave59_ROD_SlinkReset: entity work.ipbus_slave_reg_pulse
    generic map(addr_width => 0)
    port map(
        clk => sysclk40,
        reset => ipb_rst,
        ipbus_in => ipbw(59),
        ipbus_out => ipbr(59),
        q => rod_control_registers(41)
    );


-------------------------------------------


slave60_ROD_SlinkEnable: entity work.ipbus_slave_control
        generic map(addr_width => 0)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(60),
            ipbus_out => ipbr(60),
            q => rod_control_registers(42)
        );


-------------------------------------------


slave61_ROD_Format_ROS_Ver: entity work.ipbus_slave_control
        generic map(addr_width => 0)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(61),
            ipbus_out => ipbr(61),
            q => rod_control_registers(43)
        );


-------------------------------------------


slave62_ROD_Format_ROIB_Ver: entity work.ipbus_slave_control
        generic map(addr_width => 0)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(62),
            ipbus_out => ipbr(62),
            q => rod_control_registers(44)
        );


-------------------------------------------

slave63_ROD_SubDet_ModuleID: entity work.ipbus_slave_control
        generic map(addr_width => 0)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(63),
            ipbus_out => ipbr(63),
            q => rod_control_registers(45)
        );


-------------------------------------------

ROD_SlinkStatus_Gen: for i in 1 downto 0 generate begin
	ROD_SlinkStatus_64bit(i*32+31 downto i*32) <= rod_status_registers(32+i);
end generate;

slave64_ROD_SlinkStatus: entity work.ipbus_slave_status
        generic map(addr_width => 1)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(64),
            ipbus_out => ipbr(64),
            d => ROD_SlinkStatus_64bit
        );


-------------------------------------------


slave65_ROD_Slink_Busy_Idle_Fr: entity work.ipbus_slave_status
        generic map(addr_width => 0)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(65),
            ipbus_out => ipbr(65),
            d =>  rod_status_registers(34)
        );


-------------------------------------------


slave66_Slink_Busy_Idle_Conf: entity work.ipbus_slave_control
generic map(addr_width => 0)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(66),
            ipbus_out => ipbr(66),
            q => rod_control_registers(46)
        );


-------------------------------------------


slave67_ROD_ROIB_ROS_COPY: entity work.ipbus_slave_control
generic map(addr_width => 0)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(67),
            ipbus_out => ipbr(67),
            q => rod_control_registers(47)
        );


-------------------------------------------


slave68_U1_ROD_ROIB_INPUTS: entity work.ipbus_slave_control
generic map(addr_width => 0)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(68),
            ipbus_out => ipbr(68),
            q => rod_control_registers(48)
        );


-------------------------------------------


slave69_U2_ROD_ROIB_INPUTS: entity work.ipbus_slave_control
generic map(addr_width => 0)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(69),
            ipbus_out => ipbr(69),
            q => rod_control_registers(49)
        );


-------------------------------------------


slave70_TRANSPORT_ON_CRC: entity work.ipbus_slave_control_N
        generic map(numberOfRegisters => 3)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(70),
            ipbus_out => ipbr(70),
            q => TRANSPORT_ON_CRC_96bit
        );



TRANSPORT_ON_CRC_Gen: for i in 2 downto 0 generate begin
	rod_control_registers(50+i) <= TRANSPORT_ON_CRC_96bit(i*32+31 downto i*32);
end generate;


-------------------------------------------


slave71_MAX_OFFSET: entity work.ipbus_slave_control
generic map(addr_width => 0)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(71),
            ipbus_out => ipbr(71),
            q => rod_control_registers(53)
        );


-------------------------------------------


ROD_DDR_Delay_Readback_Gen: for i in 3 downto 0 generate begin
    ROD_DDR_Delay_Readback_128bit(i*32+31 downto i*32) <= rod_status_registers(35+i);
end generate;


slave72_ROD_DDR_Delay_Readback: entity work.ipbus_slave_status
        generic map(addr_width => 2)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(72),
            ipbus_out => ipbr(72),
            d =>  ROD_DDR_Delay_Readback_128bit
        );


-------------------------------------------


slave73_ROD_DDR_Load_Values: entity work.ipbus_slave_reg_pulse
    generic map(addr_width => 0)
    port map(
        clk => sysclk40,
        reset => ipb_rst,
        ipbus_in => ipbw(73),
        ipbus_out => ipbr(73),
        q => rod_control_registers(54)
    );


-------------------------------------------


slave74_ROD_SLink_ID: entity work.ipbus_slave_control_N
generic map(numberOfRegisters => 12)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(74),
            ipbus_out => ipbr(74),
            q => ROD_SLink_ID_384bit
        );


ROD_SLink_ID_Gen: for i in 11 downto 0 generate begin
    rod_control_registers(55+i) <= ROD_SLink_ID_384bit(i*32+31 downto i*32);
end generate;


-------------------------------------------


slave75_ROD_L1A_Counter: entity work.ipbus_slave_status
        generic map(addr_width => 0)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(75),
            ipbus_out => ipbr(75),
            d => rod_status_registers(39)
        );


-------------------------------------------


slave76_ROD_Packet_Counter: entity work.ipbus_slave_status
        generic map(addr_width => 0)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(76),
            ipbus_out => ipbr(76),
            d => rod_status_registers(40)
        );


-------------------------------------------


slave77_MAX_OFFSET: entity work.ipbus_slave_control
generic map(addr_width => 0)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(77),
            ipbus_out => ipbr(77),
            q => rod_control_registers(67)
        );


-------------------------------------------


slave78_ROD_EventCounterResetID_Reset: entity work.ipbus_slave_reg_pulse
    generic map(addr_width => 0)
    port map(
        clk => sysclk40,
        reset => ipb_rst,
        ipbus_in => ipbw(78),
        ipbus_out => ipbr(78),
        q => rod_control_registers(68)
    );


-------------------------------------------


slave79_ROD_EventCounterResetID_Set: entity work.ipbus_slave_control
	generic map(addr_width => 0)
	port map(
		clk => sysclk40,
		reset => ipb_rst,
		ipbus_in => ipbw(79),
		ipbus_out => ipbr(79),
		q => rod_control_registers(69)
	);


-------------------------------------------


slave80_ROD_Level1ID_Value: entity work.ipbus_slave_status
	generic map(addr_width => 0)
	port map(
		clk => sysclk40,
		reset => ipb_rst,
		ipbus_in => ipbw(80),
		ipbus_out => ipbr(80),
		d => rod_status_registers(41)
	);


-------------------------------------------


slave81_ROD_Level1ID_Reset: entity work.ipbus_slave_reg_pulse
	generic map(addr_width => 0)
	port map(
		clk => sysclk40,
		reset => ipb_rst,
		ipbus_in => ipbw(81),
		ipbus_out => ipbr(81),
		q => rod_control_registers(70)
	);


-------------------------------------------


slave82_ROD_Orbit_Wrap: entity work.ipbus_slave_control
    generic map(addr_width => 0)
    port map(
        clk => sysclk40,
        reset => ipb_rst,
        ipbus_in => ipbw(82),
        ipbus_out => ipbr(82),
        q => rod_control_registers(71)
    );


-------------------------------------------


slave83_ROD_Orbit_Reset: entity work.ipbus_slave_reg_pulse
	generic map(addr_width => 0)
	port map(
		clk => sysclk40,
		reset => ipb_rst,
		ipbus_in => ipbw(83),
		ipbus_out => ipbr(83),
		q => rod_control_registers(72)
	);


-------------------------------------------


slave84_Sequence_Type: entity work.ipbus_slave_control
    generic map(addr_width => 0)
    port map(
        clk => sysclk40,
        reset => ipb_rst,
        ipbus_in => ipbw(84),
        ipbus_out => ipbr(84),
        q => rod_control_registers(73)
    );


-------------------------------------------


slave85_EventCounterReset_Counter: entity work.ipbus_slave_status
	generic map(addr_width => 0)
	port map(
	  clk => sysclk40,
	  reset => ipb_rst,
	  ipbus_in => ipbw(85),
	  ipbus_out => ipbr(85),
	  d => rod_status_registers(42)
	);


-------------------------------------------


slave86_TOB_Builder_FIFO_Thr: entity work.ipbus_slave_control
    generic map(addr_width => 0)
    port map(
        clk => sysclk40,
        reset => ipb_rst,
        ipbus_in => ipbw(86),
        ipbus_out => ipbr(86),
        q => rod_control_registers(74)
    );


-------------------------------------------


slave87_Fiber_Fifo_Thr: entity work.ipbus_slave_control
    generic map(addr_width => 0)
    port map(
        clk => sysclk40,
        reset => ipb_rst,
        ipbus_in => ipbw(87),
        ipbus_out => ipbr(87),
        q => rod_control_registers(75)
    );


-------------------------------------------


slave88_Slink_Fifo_Thr: entity work.ipbus_slave_control
    generic map(addr_width => 0)
    port map(
        clk => sysclk40,
        reset => ipb_rst,
        ipbus_in => ipbw(88),
        ipbus_out => ipbr(88),
        q => rod_control_registers(76)
    );


-------------------------------------------

ROD_Counter_Debug_GEN: for i in 31 downto 0 generate
	ROD_Counter_Debug_1024bit(i*32+31 downto i*32) <= rod_status_registers(43+i);
end generate;

slave89_ROD_Counter_Debug: entity work.ipbus_slave_status
    generic map(addr_width => 5)
    port map(
        clk => sysclk40,
        reset => ipb_rst,
        ipbus_in => ipbw(89),
        ipbus_out => ipbr(89),
        d => ROD_Counter_Debug_1024bit
    );


-------------------------------------------


slave90_Slink_Fifo_Thr: entity work.ipbus_slave_control
    generic map(addr_width => 0)
    port map(
        clk => sysclk40,
        reset => ipb_rst,
        ipbus_in => ipbw(90),
        ipbus_out => ipbr(90),
        q => rod_control_registers(77)
    );


-------------------------------------------


	serialID_status_64bit <= (63 downto 49 => '0') & serialID_status(48 downto 41) & serialID_status(8 downto 0) & serialID_status(40 downto 9);

slave93_serialID_status: entity work.ipbus_slave_status
	generic map(addr_width => 1)
	port map(
		clk			=> sysclk40,
		reset			=>	ipb_rst,
		ipbus_in		=> ipbw(93),
		ipbus_out	=> ipbr(93),
		d				=> serialID_status_64bit
	);





end rtl;
