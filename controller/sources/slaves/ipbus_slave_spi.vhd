library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use work.ipbus.all;

entity ipbus_slave_spi is
	port(
		clk: in std_logic;
		reset: in std_logic;
		ipbus_in: in ipb_wbus;
		ipbus_out: out ipb_rbus;
		
		sclk: out std_logic;
		ssb:  out std_logic;
		mosi: out std_logic;
		miso: in  std_logic
		
	);
	
end ipbus_slave_spi;

architecture rtl of ipbus_slave_spi is

	signal addr:   std_logic_vector(7 downto 0);
	signal txdata: std_logic_vector(9 downto 0);
	signal rxdata: std_logic_vector(7 downto 0);
	signal ack:    std_logic;
	signal err:    std_logic;
	
	type   state_type is (ST_IDLE, ST_ADDRESS, ST_PAUSE, ST_DATA);
	signal state: state_type;
	signal spi_reset: std_logic;
	signal spi_start: std_logic;
	signal spi_busy:  std_logic;
	
	signal clockDivider:  std_logic_vector(5 downto 0);
	signal serialCounter: std_logic_vector(4 downto 0);
	signal pauseShiftReg: std_logic_vector(3 downto 0);
	
	constant SPI_INSTRUCTION_SETADDRESS: 	std_logic_vector(7 downto 0) := "00000000";
	constant SPI_INSTRUCTION_WRITE: 			std_logic_vector(7 downto 0) := "01000000";
	constant SPI_INSTRUCTION_WRITE_INCR:	std_logic_vector(7 downto 0) := "01100000";
	constant SPI_INSTRUCTION_READ: 			std_logic_vector(7 downto 0) := "10000000";
	constant SPI_INSTRUCTION_READ_INCR:		std_logic_vector(7 downto 0) := "10100000";
	signal   SPI_INSTRUCTION_DATAMODE:		std_logic_vector(7 downto 0);

begin
	
	
	
	process(clk) begin
		if rising_edge(clk) then
			if reset='1' then 
				ack <= '0';
				err <= '0';
			elsif ipbus_in.ipb_strobe='1' then
		
				if ipbus_in.ipb_write='1' then 
					if spi_busy='0' then
						spi_start <= '1';
						addr   <= ipbus_in.ipb_addr( 7 downto 0);
						txdata <= ipbus_in.ipb_wdata(9 downto 0);
						ack    <= not ack;
						err    <= '0';
					else
						spi_start <= '0';
						ack <= '0';
						err <= not err;
					end if;
				else
					spi_start <= '0';
					ack <= not ack;
					err <= '0';
				end if;
			else
				spi_start <= '0';
				ack <= '0';
				err <= '0';
			end if;
		end if;
	end process;
	
	
	ipbus_out.ipb_rdata <= (31 downto 9 => '0') & spi_busy & rxdata;
	ipbus_out.ipb_ack   <= ack;
	ipbus_out.ipb_err   <= err;
	
	
	spi_reset <= '1' when reset='1' or (ipbus_in.ipb_wdata(10)='1' and ipbus_in.ipb_strobe='1' and ipbus_in.ipb_write='1') else '0';
	
	
	SPI_INSTRUCTION_DATAMODE <= SPI_INSTRUCTION_READ      when txdata(9 downto 8)="00"
								  else SPI_INSTRUCTION_READ_INCR when txdata(9 downto 8)="01"
								  else SPI_INSTRUCTION_WRITE     when txdata(9 downto 8)="10"
								  else SPI_INSTRUCTION_WRITE_INCR;
	
	process(clk) begin
		if rising_edge(clk) then
			if spi_reset='1' then state <= ST_IDLE;
			elsif clockDivider="000000" then 
				case state is
					when ST_IDLE    => if spi_start='1'        then state <= ST_ADDRESS; end if;
					when ST_ADDRESS => if serialCounter(4)='1' then state <= ST_PAUSE;   end if;
					when ST_PAUSE   => if pauseShiftReg(3)='1' then state <= ST_DATA;		end if;
					when ST_DATA    => if serialCounter(4)='1' then state <= ST_IDLE;    end if;
				end case;
			end if;
		end if;
	end process;
	
	spi_busy <= '0' when state=ST_IDLE else '1';
	
	ssb <= '1' when spi_reset='1' or state=ST_IDLE or state=ST_PAUSE else '0';
	
	process(clk) begin
		if rising_edge(clk) then
			if spi_reset='1' or state=ST_IDLE or state=ST_PAUSE then clockDivider <= "000000";
			else clockDivider <= std_logic_vector(unsigned(clockDivider)+1);
			end if;
		end if;
	end process;
	
	process(clk) begin
		if rising_edge(clk) then
			if spi_reset='1' or state=ST_IDLE or state=ST_PAUSE then serialCounter <= "00000";
			elsif clockDivider="000000" then serialCounter <= std_logic_vector(unsigned(serialCounter)+1);
			end if;
		end if;
	end process;
	
	process(clk) begin
		if rising_edge(clk) then
			if state=ST_PAUSE then pauseShiftReg(0) <= '1';
			else pauseShiftReg(0) <= '0';
			end if;
			pauseShiftReg(3 downto 1) <= pauseShiftReg(2 downto 0);
		end if;
	end process;
	
	process(clk) begin
		if rising_edge(clk) then
			if spi_reset='1' or state=ST_IDLE or state=ST_PAUSE or clockDivider="100000" then sclk <= '1';
			elsif clockDivider="000000" and serialCounter(4)='0' then sclk <= '0';
			end if;
		end if;
	end process;
	
	
	process(clk) begin
		if rising_edge(clk) then
			if spi_reset='1' or state=ST_IDLE or state=ST_PAUSE then mosi <= 'Z';
			elsif clockDivider="000000" then
				if state=ST_ADDRESS then
					if serialCounter(3)='0' then mosi <= SPI_INSTRUCTION_SETADDRESS(7-to_integer(unsigned(serialCounter(2 downto 0))));
					else mosi <= addr(7-to_integer(unsigned(serialCounter(2 downto 0))));
					end if;
				elsif state=ST_DATA then
					if serialCounter(3)='0' then mosi <= SPI_INSTRUCTION_DATAMODE(7-to_integer(unsigned(serialCounter(2 downto 0))));
					elsif txdata(9)='1' then mosi <= txdata(7-to_integer(unsigned(serialCounter(2 downto 0))));
					else mosi <= 'Z';
					end if;
				else
					mosi <= 'Z';
				end if;
			end if;
		end if;
	end process;
	
	process(clk) begin
		if rising_edge(clk) then
			if state=ST_DATA and txdata(9)='0' and clockDivider="100000" then
				rxdata <= rxdata(6 downto 0) & miso;
			end if;
		end if;
	end process;
	
	
	
	
end rtl;