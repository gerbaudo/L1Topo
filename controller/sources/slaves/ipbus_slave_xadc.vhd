----------------------------------------------------------------------------------
-- Company: Johannes Gutenberg - Universitaet Mainz
-- Engineer: Christian Kahra
-- 
-- Create Date: 11.11.2014 12:11:04
-- Design Name: L1Topo
-- Module Name: ipbus_slave_xadc - Behavioral
-- Project Name: L1TopoProcessor
-- Target Devices: xc7vx690tffg1927-3
-- Tool Versions: Vivado 2014.3
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.ipbus.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ipbus_slave_xadc is
  port(
    clk: in std_logic;
    reset: in std_logic;
    ipbus_in: in ipb_wbus;
    ipbus_out: out ipb_rbus;
    xadc_control: out std_logic_vector(25 downto 0);
    xadc_status:  in  std_logic_vector(22 downto 0)
    
  );
end ipbus_slave_xadc;

architecture Behavioral of ipbus_slave_xadc is

    type stateType is (state_IDLE, state_ENABLE, state_WAIT);
    signal state: stateType;
    signal acked: std_logic;

begin


    
    
    

    process(clk) begin
        if rising_edge(clk) then 
            if reset='1' or ipbus_in.ipb_strobe='0' then 
                state <= state_IDLE;
                acked <= '0';
            else
                case state is
                
                when state_IDLE =>
                    if ipbus_in.ipb_strobe='1' then state <= state_ENABLE; end if;
                    
                when state_ENABLE =>
                    state <= state_WAIT;
                    
                when state_WAIT =>
                    state <= state_WAIT;
                    
                end case;
                
                if xadc_status(16)='1' then acked <= '1';
                else acked <= acked;
                end if;
                
            end if;
        end if;
    end process;

    xadc_control(6 downto  0)   <= ipbus_in.ipb_addr(6 downto 0);
    xadc_control(7)             <= '1' when state=state_ENABLE else '0';
    xadc_control(8)             <= '1' when state=state_ENABLE and ipbus_in.ipb_write='1' else '0';
    xadc_control(9)             <= '1' when reset='1' or (ipbus_in.ipb_strobe='1' and ipbus_in.ipb_wdata(16)='1') else '0';
    xadc_control(25 downto 10)  <= ipbus_in.ipb_wdata(15 downto 0);
    

    ipbus_out.ipb_rdata <= (31 downto 23 => '0') & xadc_status;
    ipbus_out.ipb_ack <= '1' when state=state_WAIT and xadc_status(16)='1' and acked='0' else '0';

end Behavioral;
