-- original filename ipbus_ctrlreg.vhd

-- Generic control / status register block
--
-- Provides 2**n control registers (32b each), rw
-- Provides 2**m status registers (32b each), ro
--
-- Bottom part of read address space is control, top is status
--
-- Useful for misc control of firmware block
-- Unused registers should be optimised away
--
-- Dave Newbold, July 2012



-- control register cut out for pure status register block

-- Christian Kahra, September 2013

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

use work.l1topo_package.all;
use work.ipbus.all;


entity ipbus_slave_status_generic is
	generic(
		numberOfRegisters : natural := 1;
		registerWidth: natural := 32
	);
	port(
		clk: in std_logic;
		reset: in std_logic;
		ipbus_in: in ipb_wbus;
		ipbus_out: out ipb_rbus;
		d: in std_logic_vector(numberOfRegisters * registerWidth - 1 downto 0)
	);
	
end ipbus_slave_status_generic;

architecture rtl of ipbus_slave_status_generic is

	signal sel: integer;
	signal ack: std_logic;

begin

	sel <= to_integer(unsigned(ipbus_in.ipb_addr(ld(numberOfRegisters) - 1 downto 0))) when numberOfRegisters > 1 else 0;

	process(clk)
	begin
		if rising_edge(clk) then
			
			ipbus_out.ipb_rdata(registerWidth-1 downto 0) <= d(registerWidth * (sel + 1) - 1 downto registerWidth * sel); 

			ack <= ipbus_in.ipb_strobe and not ack;

		end if;
	end process;
	
	ipbus_out.ipb_ack <= ack;
	ipbus_out.ipb_err <= '0';

	
end rtl;
