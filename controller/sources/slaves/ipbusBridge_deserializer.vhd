----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:05:52 08/29/2014 
-- Design Name: 
-- Module Name:    ipbusBridge_deserializer - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

use work.decode_8b10b_pkg.all;


entity ipbusBridge_deserializer is
	 generic (
		bridge: natural;
		line: natural;
		DELAY_GROUP_NAME : string := "delay_group"
	 );
    port( 
		sysclk			: in  std_logic;
		serialclk 		: in  std_logic;
		parallelclk 	: in  std_logic;
		mmcm_locked 	: in  std_logic;
		serialData 		: in  std_logic_vector(1 downto 0);
--		idelayValueIn 	: in  std_logic_vector(4 downto 0);
--		idelayLoad		: in  std_logic;
--		idelayInc		: in  std_logic;
--		idelayValueOut : out std_logic_vector(4 downto 0);
--		bitslip			: in  std_logic;
		decodedData 	: out std_logic_vector(7 downto 0);
		charIsK 			: out std_logic;
		codeErr 			: out std_logic;
		synchronisedData_out : out std_logic_vector(9 downto 0);
		synchronizerPointer_out: out std_logic_vector(3 downto 0);
		foundK28_5_out: out std_logic;
		enableResynchronization_out: out std_logic
	 );
	
end ipbusBridge_deserializer;




architecture Behavioral of ipbusBridge_deserializer is

	constant K28_5_n: std_logic_vector(9 downto 0) := "0101111100";
	constant K28_5_p: std_logic_vector(9 downto 0) := "1010000011";

	attribute shreg_extract: string;

	signal reset: std_logic;
	
	signal idelayValue_reg: std_logic_vector(4 downto 0);
	signal idelayValueChanged: std_logic;
	signal idelayLoadMuxed: std_logic;

	signal delayedData: std_logic;
	
	signal iddr_q1_p, iddr_q2_p: std_logic;
	signal iddr_q1, 	iddr_q2:   std_logic;
	
	
	signal dataShiftReg: std_logic_vector(9 downto 0);
	
	signal deserialisedDataBuffer: std_logic_vector(18 downto 0);
	attribute shreg_extract of deserialisedDataBuffer: signal is "no";
	
	
	
	signal bitslipCounter: std_logic_vector(3 downto 0);
	
	signal enableResynchronization: std_logic;
	signal foundK28_5: std_logic;
	signal synchronizerPointer: std_logic_vector(3 downto 0);
	signal synchronisedData: std_logic_vector(9 downto 0);
	
	
	
	signal charNotInTable: std_logic;
	signal dispErr: std_logic;

--	attribute IODELAY_GROUP : string;
--	attribute IODELAY_GROUP of ipbusBridge_idelay: label is DELAY_GROUP_NAME;
	
	
	
begin


	reset <= not mmcm_locked;


--	process(sysclk) begin
--		if rising_edge(sysclk) then
--			idelayValue_reg <= idelayValueIn;
--			if idelayValue_reg /= idelayValueIn then idelayValueChanged <= '1';
--			else idelayValueChanged <= '0';
--			end if;			
--		end if;
--	end process;
--
--	idelayLoadMuxed <= idelayLoad or idelayValueChanged;
--
--
--ipbusBridge_idelay : IDELAYE2
--	generic map (
--		CINVCTRL_SEL           => "FALSE",            -- TRUE, FALSE
--		DELAY_SRC              => "IDATAIN",        -- IDATAIN, DATAIN
--		HIGH_PERFORMANCE_MODE  => "FALSE",             -- TRUE, FALSE
--		IDELAY_TYPE            => "VAR_LOAD",          -- FIXED, VARIABLE, or VAR_LOADABLE
--		IDELAY_VALUE           => 5,                -- 0 to 31
--		REFCLK_FREQUENCY       => 200.0,
--		PIPE_SEL               => "FALSE",
--		SIGNAL_PATTERN         => "DATA"           -- CLOCK, DATA
--	)
--	port map (
--		IDATAIN                => serialData, -- Driven by IOB
--		DATAOUT                => delayedData,
--		
--		C                      => sysclk,
--		CE                     => idelayInc, --IN_DELAY_DATA_CE,
--		INC                    => '1', --IN_DELAY_DATA_INC,
--		LD                     => idelayLoadMuxed,
--		CNTVALUEIN             => idelayValueIn, --IN_DELAY_TAP_IN,
--		CNTVALUEOUT            => idelayValueOut, --IN_DELAY_TAP_OUT,
--		
--		--unused ports
--		DATAIN                 => '0', -- Data from FPGA logic
--		REGRST                 => '0',
--		LDPIPEEN               => '0',
--		CINVCTRL               => '0'
--	);
--
--
--
--
--
--
--
--IDDR_inst : IDDR
--		generic map (
--		DDR_CLK_EDGE => "SAME_EDGE_PIPELINED",
--		INIT_Q1 => '0',
--		INIT_Q2 => '0',
--		SRTYPE => "SYNC")
--		port map (
--		Q1 => iddr_q1_p,
--		Q2 => iddr_q2_p,
--		C => serialclk,
--		CE => '1',
--		D => delayedData,
--		R => reset,
--		S => '0'
--		);
--
--POLARITY_GEN_U1: if bridge=1 generate begin
--LINE_0: if line=0 generate begin
--	iddr_q1 <= iddr_q1_p;
--	iddr_q2 <= iddr_q2_p;
--end generate LINE_0;
--
--LINE_1_2: if line/=0 generate begin
--	iddr_q1 <= not iddr_q1_p;
--	iddr_q2 <= not iddr_q2_p;
--end generate LINE_1_2;
--end generate POLARITY_GEN_U1;
--
--POLARITY_GEN_U2: if bridge=2 generate begin
--	iddr_q1 <= not iddr_q1_p;
--	iddr_q2 <= not iddr_q2_p;
--end generate POLARITY_GEN_U2;



--	process(serialclk) begin
--		if rising_edge(serialclk) then
--			dataShiftReg(0) <= iddr_q2;
--			dataShiftReg(1) <= iddr_q1;
--			dataShiftReg(9 downto 2) <= dataShiftReg(7 downto 0);
--		end if;
--	end process;
	
	process(serialclk) begin
		if rising_edge(serialclk) then
			dataShiftReg(9 downto 8) <= serialData;
			dataShiftReg(7 downto 0) <= dataShiftReg(9 downto 2);
		end if;
	end process;


	process(parallelclk) 
		variable pointer: natural;
	begin
		if rising_edge(parallelclk) then
			deserialisedDataBuffer(18 downto  9) <= dataShiftReg;
			deserialisedDataBuffer( 8 downto  0) <= deserialisedDataBuffer(18 downto 10);
						
--			if bitslip='1' then 
--				if bitslipCounter="1001" then bitslipCounter <= "0000";
--				else bitslipCounter <= std_logic_vector( unsigned(bitslipCounter) +1 );
--				end if;
--			else bitslipCounter <= bitslipCounter;
--			end if;
		
			if enableResynchronization='1' then
				if		deserialisedDataBuffer( 9 downto  0) = K28_5_p or deserialisedDataBuffer( 9 downto  0) = K28_5_n then synchronizerPointer <= "0000"; foundK28_5 <= '1';
				elsif deserialisedDataBuffer(10 downto  1) = K28_5_p or deserialisedDataBuffer(10 downto  1) = K28_5_n then synchronizerPointer <= "0001"; foundK28_5 <= '1';
				elsif deserialisedDataBuffer(11 downto  2) = K28_5_p or deserialisedDataBuffer(11 downto  2) = K28_5_n then synchronizerPointer <= "0010"; foundK28_5 <= '1';
				elsif deserialisedDataBuffer(12 downto  3) = K28_5_p or deserialisedDataBuffer(12 downto  3) = K28_5_n then synchronizerPointer <= "0011"; foundK28_5 <= '1';
				elsif deserialisedDataBuffer(13 downto  4) = K28_5_p or deserialisedDataBuffer(13 downto  4) = K28_5_n then synchronizerPointer <= "0100"; foundK28_5 <= '1';
				elsif deserialisedDataBuffer(14 downto  5) = K28_5_p or deserialisedDataBuffer(14 downto  5) = K28_5_n then synchronizerPointer <= "0101"; foundK28_5 <= '1';
				elsif deserialisedDataBuffer(15 downto  6) = K28_5_p or deserialisedDataBuffer(15 downto  6) = K28_5_n then synchronizerPointer <= "0110"; foundK28_5 <= '1';
				elsif deserialisedDataBuffer(16 downto  7) = K28_5_p or deserialisedDataBuffer(16 downto  7) = K28_5_n then synchronizerPointer <= "0111"; foundK28_5 <= '1';
				elsif deserialisedDataBuffer(17 downto  8) = K28_5_p or deserialisedDataBuffer(17 downto  8) = K28_5_n then synchronizerPointer <= "1000"; foundK28_5 <= '1';
				elsif deserialisedDataBuffer(18 downto  9) = K28_5_p or deserialisedDataBuffer(18 downto  9) = K28_5_n then synchronizerPointer <= "1001"; foundK28_5 <= '1';
				else  synchronizerPointer <= synchronizerPointer;  foundK28_5 <= '0';
				end if;
			end if;
			synchronisedData <= deserialisedDataBuffer(to_integer(unsigned(synchronizerPointer))+9 downto  to_integer(unsigned(synchronizerPointer)));
			
--			pointer := to_integer( unsigned(bitslipCounter) );
--			synchronisedData <= deserialisedDataBuffer(pointer+9 downto pointer);
		

		end if;
	end process;


	synchronisedData_out <= synchronisedData;
	synchronizerPointer_out <= synchronizerPointer;
	foundK28_5_out <= foundK28_5;
	enableResynchronization_out <= enableResynchronization;



decoder: entity work.decode_8b10b_lut_base 
	generic map(
		C_HAS_CODE_ERR => 1,
		C_HAS_DISP_ERR => 1
	)
	port map(
		CLK => parallelclk,
		DIN => synchronisedData,
		DOUT => decodedData,
		KOUT => charIsK,
		CE => '1',
		CODE_ERR => charNotInTable,
		DISP_ERR => dispErr,
		ND => open,
		RUN_DISP => open,
		SYM_DISP => open
	);


	codeErr <= charNotInTable or dispErr;

process(parallelclk) begin
if rising_edge(parallelclk) then
	if charNotInTable='1' or dispErr='1' then enableResynchronization <= '1';
	elsif foundK28_5='1' then enableResynchronization <= '0';
	end if;
end if;
end process;	


	
	

end Behavioral;

