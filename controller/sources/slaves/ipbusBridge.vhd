----------------------------------------------------------------------------------
-- Company: Johannes Gutenberg-Universitaet Mainz
-- Engineer: Christian Kahra
-- 
-- Create Date:    20:42:46 08/28/2014 
-- Design Name: 
-- Module Name:    ipbusBridge - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 	based on the old ctrlbus module
--
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

use work.l1topo_package.all;
use work.ipbus.all;



entity ipbusBridge is
	generic (
		bridge: natural;
		DELAY_GROUP_NAME : string := "delay_group"
		);
	port(
		sysclk:            in  std_logic;
		parallelclk:	    in  std_logic;
		serialclk:         in  std_logic;
		idelayctrl_refclk: in  std_logic;
		
		mmcm_locked: in  std_logic;
		
		
		ipb_write: in ipb_wbus;
		ipbBridgeBus_out: out arraySLV2(4 downto 0);
		
		ipbBridgeBus_in:  in  arraySLV2(2 downto 0);
		ipb_read: out  ipb_rbus;
		
		control:  in  std_logic_vector(0 downto 0);
		status:   out  std_logic_vector(14 downto 0);
		request_errorCounter: out arraySLV8(4 downto 0);
		response_errorCounter: out arraySLV8(2 downto 0);
		
		iconControl: inout std_logic_vector(35 downto 0)
		
		
	);
end ipbusBridge;


architecture Behavioral of ipbusBridge is

	constant inWidth: natural := 3;
	constant outWidth: natural := 5;

	constant K28_0: std_logic_vector(7 downto 0) := X"1C";
	constant K28_1: std_logic_vector(7 downto 0) := X"3C";
	constant K28_2: std_logic_vector(7 downto 0) := X"5C";
	constant K28_3: std_logic_vector(7 downto 0) := X"7C";
	constant K28_4: std_logic_vector(7 downto 0) := X"9C";
	constant K28_5: std_logic_vector(7 downto 0) := X"BC";
	constant K28_6: std_logic_vector(7 downto 0) := X"DC";
	constant K28_7: std_logic_vector(7 downto 0) := X"FC";
	constant K23_7: std_logic_vector(7 downto 0) := X"F7";
	constant K27_7: std_logic_vector(7 downto 0) := X"FB";
	constant K29_7: std_logic_vector(7 downto 0) := X"FD";
	constant K30_7: std_logic_vector(7 downto 0) := X"FE";

	attribute shreg_extract: string;

	type array4 is array(natural range <>) of std_logic_vector(3 downto 0);

	signal idelayctrlReset: std_logic;
	signal idelayctrlResetCounter: std_logic_vector(1 downto 0);

	signal dataOut: std_logic_vector(outWidth*2*8-1	downto 0);
	signal dataToSerialize: std_logic_vector(outWidth*8-1 downto 0);
	signal charIsKOut: std_logic_vector(outWidth-1 downto 0);
	
	type   dataOutStateType is (dataOutState_IDLE, dataOutState_BOF, dataOutState_DATA0, dataOutState_DATA1);
	signal dataOutState: dataOutStateType;
	signal dataOutState_slv: std_logic_vector(1 downto 0);
	
	signal synchronisedData: std_logic_vector(inWidth*10-1 downto 0);
	signal synchronizerPointer: std_logic_vector(inWidth*4+3 downto 0);
	signal foundK28_5: std_logic_vector(inWidth-1 downto 0);
	signal enableResynchronization: std_logic_vector(inWidth-1 downto 0);
	signal deserializedData: std_logic_vector(inWidth*8-1 downto 0);
	signal dataIn: std_logic_vector(inWidth*2*8-1 downto 0);
	signal charIsKIn: std_logic_vector(inWidth-1 downto 0);
	
	signal codeErr: std_logic_vector(inWidth-1 downto 0);
--	signal errorCounter: std_logic_vector(17 downto 0);
	signal request_errorCounter_i:	arraySLV8(outWidth-1 downto 0);
	signal response_errorCounter_i:	arraySLV8(inWidth-1 downto 0);
	
	signal bitslipErrorCounter: array4(inWidth-1 downto 0);
	signal bitslip: std_logic_vector(inWidth-1 downto 0);
	
	
	signal charIsKInAnded: std_logic_vector(inWidth downto 0);
	signal charIsKInOred:  std_logic_vector(inWidth downto 0);
	signal codeErrOred:    std_logic_vector(inWidth downto 0);
	signal charIsBofOred:  std_logic_vector(inWidth downto 0);
	signal charIsBofAnded: std_logic_vector(inWidth downto 0);
	signal charIsIdle0Anded: std_logic_vector(inWidth downto 0);
	signal charIsErrFlagAnded: std_logic_vector(inWidth downto 0);
	
	signal dataIsValid:  std_logic;
	
	type   dataInStateType is (dataInState_IDLE, dataInState_DATA0, dataInState_DATA1);
	signal dataInState: dataInStateType;
	signal dataInState_slv: std_logic_vector(1 downto 0);
	
	signal dataIn_codeErr: std_logic_vector(23 downto 0);
	
	signal ack, ack_reg, ack_prev: std_logic;
	signal err, err_reg, err_prev: std_logic;
	
--	attribute IODELAY_GROUP : string;
--	attribute IODELAY_GROUP of ipbusBridge_idelayctrl : label is DELAY_GROUP_NAME;
	
	signal ipb_write_reg: ipb_wbus;
	signal ipb_read_reg: ipb_rbus;
	
	attribute shreg_extract of ipb_write_reg: signal is "no";
	attribute shreg_extract of ipb_read_reg: signal is "no";
	
	
begin


	process(sysclk) begin
		if rising_edge(sysclk) then
			ipb_write_reg <= ipb_write;
			ipb_read  <=  ipb_read_reg;
		end if;
	end process;


--process(sysclk) begin
--		if rising_edge(sysclk) then
--			if mmcm_locked='0' then
--				idelayctrlReset <= '1';
--				idelayctrlResetCounter <= (others => '0');
--			else
--				if idelayctrlResetCounter = "11" then
--					idelayctrlReset <= '0';
--					idelayctrlResetCounter <= idelayctrlResetCounter;
--				else
--					idelayctrlReset <= '1';
--					idelayctrlResetCounter <= std_logic_vector( unsigned(idelayctrlResetCounter) + 1);
--				end if;
--			end if;
--		end if;
--	end process;





--ipbusBridge_idelayctrl : IDELAYCTRL
--	 port map (
--		  REFCLK => idelayctrl_refclk,
--		  RST    => idelayctrlReset,
--		  RDY    => open
--	  );



	dataOut( 0) <= '0';
	dataOut(32 downto  1) <= ipb_write_reg.ipb_addr;
	dataOut(39 downto 33) <= ipb_write_reg.ipb_wdata( 6 downto 0);
	dataOut(40) <= '1';
	dataOut(65 downto 41) <= ipb_write_reg.ipb_wdata(31 downto 7);
	dataOut(66) <= ipb_write_reg.ipb_strobe;
	dataOut(67) <= ipb_write_reg.ipb_write;
	dataOut(79 downto 68) <= (others => '0');
	
	

	
	
	
	process(parallelclk) begin
		if rising_edge(parallelclk) then
			
			if mmcm_locked='0' then dataOutState <= dataOutState_IDLE;
			else 
				case dataOutState is
				
				when dataOutState_IDLE =>
					if ipb_write_reg.ipb_strobe='1' then 
					     dataOutState <= dataOutState_BOF;
					else dataOutState <= dataOutState_IDLE;
					end if;
				
				when dataOutState_BOF =>
					dataOutState <= dataOutState_DATA0;
				
				when dataOutState_DATA0 =>
					if ipb_write_reg.ipb_strobe='1' then 
					     dataOutState <= dataOutState_DATA1;
					else dataOutState <= dataOutState_IDLE;
					end if;
				
				when dataOutState_DATA1 =>
					dataOutState <= dataOutState_DATA0;
					
				end case;
			end if;
			
		end if;
	end process;

	dataOutState_slv <= "00" when dataOutState = dataOutState_IDLE
						else "01" when dataOutState = dataOutState_BOF
						else "10" when dataOutState = dataOutState_DATA0
						else "11";


	dataToSerialize <= K28_5 & K28_5 & K28_5 & K28_5 & K28_5 when (dataOutState=dataOutState_IDLE or ipb_write_reg.ipb_strobe='0')
					  else K27_7 & K27_7 & K27_7 & K27_7 & K27_7 when  dataOutState=dataOutState_BOF
					  else dataOut(outWidth*8-1 downto  0) when  dataOutState=dataOutState_DATA0 
					  else dataOut(outWidth*2*8-1 downto outWidth*8);






SERIALIZER_GEN: for i in outWidth-1 downto 0 generate begin


	charIsKOut(i) <= '1' when dataOutState=dataOutState_IDLE or ipb_write_reg.ipb_strobe='0' or dataOutState=dataOutState_BOF else '0';


	serializer: entity work.ipbusBridge_serializer
		generic map(
			bridge => bridge,
			line => i
		)
		port map(
			parallelclk  => parallelclk,
			serialclk    => serialclk,
			mmcm_locked  => mmcm_locked,
			parallelData => dataToSerialize(i*8+7 downto i*8),
			charIsK      => charIsKOut(i),
			serialData => ipbBridgeBus_out(i)
		);
		
end generate;






	charIsKInAnded(inWidth)   <= '1';
	charIsKInOred(inWidth)    <= '0';
	codeErrOred(inWidth)      <= '0';
	charIsBofAnded(inWidth)   <= '1';
	charIsBofOred(inWidth)    <= '0';
	charIsIdle0Anded(inWidth) <= '1';
	charIsErrFlagAnded(inWidth) <= '1';




DESERIALIZER_GEN: for i in inWidth-1 downto 0 generate begin



	deserializer: entity work.ipbusBridge_deserializer
		generic map(
			bridge => bridge,
			line => i,
			DELAY_GROUP_NAME => DELAY_GROUP_NAME
		)
		port map(
			sysclk         => sysclk,
			parallelclk    => parallelclk,
			serialclk      => serialclk,
			mmcm_locked    => mmcm_locked,
			serialData     => ipbBridgeBus_in(i),
--			bitslip        => bitslip(i),
			decodedData    => deserializedData(i*8+7 downto i*8),
			charIsK        => charIsKIn(i),
			codeErr        => codeErr(i),
			synchronisedData_out => synchronisedData(i*10+9 downto i*10),
			synchronizerPointer_out => synchronizerPointer(i*4+3 downto i*4),
			foundK28_5_out => foundK28_5(i),
			enableResynchronization_out => enableResynchronization(i)
		);

	



	charIsKInAnded(i) <= charIsKInAnded(i+1) and charIsKIn(i);
	charIsKInOred(i)  <= charIsKInOred(i+1)  or  charIsKIn(i);
	codeErrOred(i)    <= codeErrOred(i+1)    or  codeErr(i);
	charIsBofAnded(i) <= '1' when charIsBofAnded(i+1)='1' and deserializedData(i*8+7 downto i*8)=K27_7 else '0';
	charIsBofOred(i)  <= '1' when charIsBofOred(i+1) ='1' or  deserializedData(i*8+7 downto i*8)=K27_7 else '0';
	charIsIdle0Anded(i) <= '1' when charIsIdle0Anded(i+1) = '1' and deserializedData(i*8+7 downto i*8)=K28_5 else '0';
	charIsErrFlagAnded(i) <= '1' when charIsErrFlagAnded(i+1)='1' 
											and (deserializedData(i*8+7 downto i*8)=K28_0
													or deserializedData(i*8+7 downto i*8)=K28_1
													or deserializedData(i*8+7 downto i*8)=K28_2
													or deserializedData(i*8+7 downto i*8)=K28_3) else '0';
		
		
		
	process(parallelclk) begin
		if rising_edge(parallelclk) then
			
--			if codeErr(i)='1' then bitslipErrorCounter(i) <= std_logic_vector( unsigned(bitslipErrorCounter(i)) + 1);
--			else bitslipErrorCounter(i) <= "0000";
--			end if;
			
			if control(0)='1' then response_errorCounter_i(i) <= (7 downto 0 => '0');
			elsif codeErr(i)='1' and response_errorCounter_i(i) /= x"ff" then response_errorCounter_i(i) <= std_logic_vector( unsigned(response_errorCounter_i(i)) + 1);
			end if;
			
		end if;
	end process;
	
--	bitslip(i) <= '1' when bitslipErrorCounter(i)="1111" else '0';
	
		
end generate;



	dataIsValid <= '1' when ( codeErrOred(0)='0' and charIsKInOred(0)='0') else '0';

	response_errorCounter <= response_errorCounter_i;
	


process(parallelclk) begin
		if rising_edge(parallelclk) then
			
			if mmcm_locked='0' then dataInState <= dataInState_IDLE;
			else 
				case dataInState is
				
				when dataInState_IDLE =>
					if charIsBofAnded(0) = '1' then dataInState <= dataInState_Data0; end if;
				
				when dataInState_Data0 =>
					if dataIsValid='1' then
						dataInState <= dataInState_Data1;
						dataIn(inWidth*8-1 downto 0) <= deserializedData;
					else 
						dataInState <= dataInState_IDLE;
						dataIn <= (inWidth*2*8-1 downto 0 => '0');
					end if;
				when dataInState_Data1 =>
					dataIn(inWidth*2*8-1 downto inWidth*8) <= deserializedData;
					if dataIsValid='1' then dataInState <= dataInState_Data0;
					else dataInState <= dataInState_IDLE;
					end if;
				end case;
			end if;
			
		end if;
end process;

	dataInState_slv <= "00" when dataInState = dataInState_IDLE
					  else "01" when dataInState = dataInState_Data0
					  else "10" when dataInState = dataInState_Data1
					  else "11";


	dataIn_codeErr <= deserializedData when charIsErrFlagAnded(0)='1' and charIsKInAnded(0)='1' else K28_0 & K28_0 & K28_0;
	
process(parallelclk) begin
if rising_edge(parallelclk) then
	if control(0)='1' then request_errorCounter_i(0) <= x"00";
	elsif request_errorCounter_i(0) /= x"ff" and (dataIn_codeErr(7 downto 0) = K28_1 or dataIn_codeErr(7 downto 0) = K28_3) then request_errorCounter_i(0) <= std_logic_vector(unsigned(request_errorCounter_i(0)) + 1);
	end if;
	
	if control(0)='1' then request_errorCounter_i(1) <= x"00";
	elsif request_errorCounter_i(1) /= x"ff" and (dataIn_codeErr(7 downto 0) = K28_2 or dataIn_codeErr(7 downto 0) = K28_3) then request_errorCounter_i(1) <= std_logic_vector(unsigned(request_errorCounter_i(1)) + 1);
	end if;
	
	if control(0)='1' then request_errorCounter_i(2) <= x"00";
	elsif request_errorCounter_i(2) /= x"ff" and (dataIn_codeErr(15 downto 8) = K28_1 or dataIn_codeErr(7 downto 0) = K28_3) then request_errorCounter_i(2) <= std_logic_vector(unsigned(request_errorCounter_i(2)) + 1);
	end if;
	
	if control(0)='1' then request_errorCounter_i(3) <= x"00";
	elsif request_errorCounter_i(3) /= x"ff" and (dataIn_codeErr(15 downto 8) = K28_2 or dataIn_codeErr(7 downto 0) = K28_3) then request_errorCounter_i(3) <= std_logic_vector(unsigned(request_errorCounter_i(3)) + 1);
	end if;
	
	if control(0)='1' then request_errorCounter_i(4) <= x"00";
	elsif request_errorCounter_i(4) /= x"ff" and (dataIn_codeErr(23 downto 16) = K28_1) then request_errorCounter_i(4) <= std_logic_vector(unsigned(request_errorCounter_i(4)) + 1);
	end if;
	
end if;
end process;
	
	
	request_errorCounter <= request_errorCounter_i;
	
	
	
	ipb_read_reg.ipb_rdata(22 downto  0) <= dataIn(23 downto  1);
	ipb_read_reg.ipb_rdata(31 downto 23) <= dataIn(33 downto 25);
	ack <= '1' when deserializedData(34-24)='1' and dataIsValid='1' and dataInState=dataInState_DATA1 and ipb_write_reg.ipb_strobe='1' else '0';
	err <= '1' when deserializedData(35-24)='1' and dataIsValid='1' and dataInState=dataInState_DATA1 and ipb_write_reg.ipb_strobe='1' else '0';

	process(parallelclk) begin
		if rising_edge(parallelclk) then
			ack_reg <= ack;
			err_reg <= err;
		end if;
	end process;
	
	
process(sysclk) begin
if rising_edge(sysclk) then
	ack_prev <= ipb_read_reg.ipb_ack;
	err_prev <= ipb_read_reg.ipb_err;
end if;
end process;
	
	
	ipb_read_reg.ipb_ack <= '1' when ipb_write_reg.ipb_strobe='1' and (ack='1' or ack_reg='1') and ack_prev='0' else '0';
	ipb_read_reg.ipb_err <= '1' when ipb_write_reg.ipb_strobe='1' and (err='1' or err_reg='1') and err_prev='0' else '0';
	
	
	

--ila_ipbBridgeU1: entity work.ila_ipbBridge
--port map(
--	CONTROL => iconControl,
--	CLK => parallelclk,
--	TRIG0( 7 downto  0) => deserializedData( 7 downto  0),
--	TRIG0(15 downto  8) => deserializedData(15 downto  8),
--	TRIG0(23 downto 16) => deserializedData(23 downto 16),
--	TRIG0(26 downto 24) => charIsKIn,
--	TRIG0(29 downto 27) => codeErr,
--	TRIG0(31 downto 30) => dataInState_slv,
--	TRIG0(33 downto 32) => dataOutState_slv,
--	TRIG0(34) => charIsKInAnded(0),
--	TRIG0(35) => charIsKInOred(0),
--	TRIG0(36) => codeErrOred(0),
--	TRIG0(37) => charIsBofOred(0),
--	TRIG0(38) => charIsBofAnded(0),
--	TRIG0(39) => charIsIdle0Anded(0),
--	TRIG0(40) => charIsErrFlagAnded(0),
--	TRIG0(64 downto 41) => dataIn_codeErr,
--	TRIG0(72 downto 65) => request_errorCounter_i(0),
--	TRIG0(80 downto 73) => request_errorCounter_i(1),
--	TRIG0(88 downto 81) => request_errorCounter_i(2),
--	TRIG0(90 downto 89) => dataIn(24) & dataIn(0),
--	TRIG0(122 downto 91) => dataIn(33 downto 25) & dataIn(23 downto 1),
--	TRIG0(123) => dataIn(34),
--	TRIG0(124) => dataIn(35),
--	TRIG0(125) => ipb_write_reg.ipb_strobe,
--	TRIG0(126) => ipb_write_reg.ipb_write,
--	TRIG0(136 downto 127) => synchronisedData( 9 downto  0),
--	TRIG0(146 downto 137) => synchronisedData(19 downto 10),
--	TRIG0(156 downto 147) => synchronisedData(29 downto 20),
--	TRIG0(160 downto 157) => synchronizerPointer(3 downto 0),
--	TRIG0(164 downto 161) => synchronizerPointer(7 downto 4),
--	TRIG0(168 downto 165) => synchronizerPointer(11 downto 8),
--	TRIG0(171 downto 169) => foundK28_5,
--	TRIG0(174 downto 172) => enableResynchronization,
--	TRIG0(206 downto 175) => ipb_write_reg.ipb_addr,
--	TRIG0(229 downto 207) => ipb_write_reg.ipb_wdata(22 downto 0),
--	TRIG0(253 downto 230) => ipb_read_reg.ipb_rdata(23 downto 0),
--	TRIG0(255 downto 254) => ipb_read_reg.ipb_err & ipb_read_reg.ipb_ack
--);


--	debugIPBusBridge( 47 downto   0) <= dataIn;
--	debugIPBusBridge( 71 downto  48) <= deserializedData;
--	debugIPBusBridge( 74 downto  72) <= charIsKIn;
--	debugIPBusBridge(75) <= '0' when dataInState=dataInState_IDLE  or dataInState=dataInState_DATA0 else '1';
--	debugIPBusBridge(76) <= '1' when dataInState=dataInState_DATA1 or dataInState=dataInState_DATA0 else '0';
--	debugIPBusBridge(116 downto  77) <= dataToSerialize;
--	debugIPBusBridge(196 downto 117) <= dataOut;
--	debugIPBusBridge(201 downto 197) <= charIsKOut;
--	debugIPBusBridge(204 downto 202) <= codeErr;
--	debugIPBusBridge(216 downto 205) <= bitslipErrorCounter(2) & bitslipErrorCounter(1) & bitslipErrorCounter(0);
--	debugIPBusBridge(219 downto 217) <= bitslip;
--	debugIPBusBridge(220) <= charIsKInAnded(0);
--	debugIPBusBridge(221) <= charIsKInOred(0);
--	debugIPBusBridge(222) <= codeErrOred(0);
--	debugIPBusBridge(223) <= charIsBofOred(0);
--	debugIPBusBridge(224) <= charIsBofAnded(0);
--	debugIPBusBridge(225) <= dataIsValid;
--	debugIPBusBridge(226) <= '0' when dataOutState=dataOutState_IDLE  or dataOutState=dataOutState_DATA0 else '1';
--	debugIPBusBridge(227) <= '1' when dataOutState=dataOutState_DATA1 or dataOutState=dataOutState_DATA0 else '0';
--	debugIPBusBridge(228) <= ack;
--	debugIPBusBridge(229) <= err;



end Behavioral;

