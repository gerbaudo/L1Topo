library IEEE;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.rod_l1_topo_types_const.all;

entity tob_parser is
  port (
    CLK                        : in  std_logic;
    LINK_NUMBER                : in  std_logic_vector(6 downto 0);
    TOB_DATA_IN                : in  std_logic_vector(127 downto 0);
    CRC_ERR_IN                 : in  std_logic;
    FPGA_NUMBER                : in  std_logic;
    CRATE_NUMBER               : in  std_logic_vector(1 downto 0);
    TOB_DATA_OUT               : out std_logic_vector(9*32-1 downto 0) := (others => '0')
  );
end tob_parser;

architecture tob_parser of tob_parser is
  signal tob_type_l : std_logic_vector(3 downto 0);
  signal tob_data_in_l : std_logic_vector(127 downto 0);
  signal local_crc_error_word : std_logic_vector(31 downto 0);

begin

  --fpga_number_v(0) <= FPGA_NUMBER;
  SAT_TOB_TYPE : process (CLK)
  begin
    if rising_edge(CLK) then
      if FPGA_NUMBER = '0' then
        tob_type_l <= TOB_MAP(0)(to_integer(unsigned(LINK_NUMBER)));
        tob_data_in_l <= TOB_DATA_IN;
      else 
        tob_type_l <= TOB_MAP(1)(to_integer(unsigned(LINK_NUMBER)));
        tob_data_in_l <= TOB_DATA_IN;
      end if;     
    end if;
  end process SAT_TOB_TYPE;

  SET_CRC_DATA_WORD : process (CLK)
  begin
    if rising_edge(CLK) then
      if CRC_ERR_IN = '1' then
        local_crc_error_word <= STATUS_WORD_TYPE & x"4000000";
        TOB_DATA_OUT(9*32-1 downto 8*32) <= local_crc_error_word;
      else
        local_crc_error_word <= (others => '0');
        TOB_DATA_OUT(9*32-1 downto 8*32) <= local_crc_error_word;
      end if;
    end if;
  end process SET_CRC_DATA_WORD;
  
  TOB_ENCAPSULATE: process (CLK)
  begin  -- process
    if rising_edge(CLK) then
      case tob_type_l is
        
        when EM_TOB_TYPE =>
          for i in 0 to (all_tob_array(std_int(EM_TOB_TYPE)).max_tob_number-1) loop
--          for i in 0 to all_tob_array(0).max_tob_number-1 loop
            if all_tob_array(std_int(EM_TOB_TYPE)).tob_start(i) /= 0 then
              TOB_DATA_OUT(32*(i+1)-1 downto 32*i) <=
                EM_TOB_TYPE & CRATE_NUMBER & "00" &
                tob_data_in_l(
                  all_tob_array(std_int(EM_TOB_TYPE)).tob_start(i) +
                  all_tob_array(std_int(EM_TOB_TYPE)).tob_length(i)-1
                  downto 
                   all_tob_array(std_int(EM_TOB_TYPE)).tob_start(i) +
                all_tob_array(std_int(EM_TOB_TYPE)).tob_length(i)-4) & '0' &
                tob_data_in_l(
                  all_tob_array(std_int(EM_TOB_TYPE)).tob_start(i) +
                all_tob_array(std_int(EM_TOB_TYPE)).tob_length(i)-5
                  downto
                all_tob_array(std_int(EM_TOB_TYPE)).tob_start(i));
            else
              TOB_DATA_OUT(32*(i+1)-1 downto 32*i) <= (others => '0');
            end if;
          end loop;

          for i in all_tob_array(std_int(EM_TOB_TYPE)).max_tob_number to 7 loop
            TOB_DATA_OUT(32*(i+1)-1 downto 32*i) <= (others => '0');
          end loop;

          
        when TAU_TOB_TYPE =>
          for i in 0 to all_tob_array(std_int(TAU_TOB_TYPE)).max_tob_number-1 loop
            if all_tob_array(std_int(TAU_TOB_TYPE)).tob_start(i) /= 0 then
              TOB_DATA_OUT(32*(i+1)-1 downto 32*i) <=
                TAU_TOB_TYPE & CRATE_NUMBER & "00" &
                tob_data_in_l(
                  all_tob_array(std_int(TAU_TOB_TYPE)).tob_start(i) +
                all_tob_array(std_int(TAU_TOB_TYPE)).tob_length(i)-1
                  downto 
                   all_tob_array(std_int(TAU_TOB_TYPE)).tob_start(i) +
                all_tob_array(std_int(TAU_TOB_TYPE)).tob_length(i)-4) & '0' &
                tob_data_in_l(
                  all_tob_array(std_int(TAU_TOB_TYPE)).tob_start(i) +
                all_tob_array(std_int(TAU_TOB_TYPE)).tob_length(i)-5
                  downto
                all_tob_array(std_int(TAU_TOB_TYPE)).tob_start(i));
            else
              TOB_DATA_OUT(32*(i+1)-1 downto 32*i) <= (others => '0');
            end if;
          end loop;
          
          for i in all_tob_array(std_int(TAU_TOB_TYPE)).max_tob_number to 7 loop
            TOB_DATA_OUT(32*(i+1)-1 downto 32*i) <= (others => '0');
          end loop;


        when MUON_TOB_TYPE =>
          for i in 0 to all_tob_array(std_int(MUON_TOB_TYPE)).max_tob_number-1 loop
--            if all_tob_array(std_int(MUON_TOB_TYPE)).tob_start(i) /= 0 then
              TOB_DATA_OUT(32*(i+1)-1 downto 32*i) <=
                MUON_TOB_TYPE & CRATE_NUMBER(0) & std_logic_vector(to_unsigned(i,3)) & x"00" &
                tob_data_in_l(
                  all_tob_array(std_int(MUON_TOB_TYPE)).tob_start(i) +
                all_tob_array(std_int(MUON_TOB_TYPE)).tob_length(i)-1 downto
                all_tob_array(std_int(MUON_TOB_TYPE)).tob_start(i));
--            else
--              TOB_DATA_OUT(32*(i+1)-1 downto 32*i) <= (others => '0');
--            end if;
          end loop;
--          for i in all_tob_array(std_int(MUON_TOB_TYPE)).max_tob_number to 7 loop
--            TOB_DATA_OUT(32*(i+1)-1 downto 32*i) <= (others => '0');
--          end loop;

        when MUON_TOB_TYPE_D =>
          for i in 0 to all_tob_array(std_int(MUON_TOB_TYPE_D)).max_tob_number-1 loop
--            if all_tob_array(std_int(MUON_TOB_TYPE)).tob_start(i) /= 0 then
              TOB_DATA_OUT(32*(i+1)-1 downto 32*i) <=
                MUON_TOB_TYPE & CRATE_NUMBER(0) & std_logic_vector(to_unsigned(i,3)) & '1'  & "000" & x"0" &
                tob_data_in_l(
                  all_tob_array(std_int(MUON_TOB_TYPE)).tob_start(i) +
                all_tob_array(std_int(MUON_TOB_TYPE)).tob_length(i)-1 downto
                all_tob_array(std_int(MUON_TOB_TYPE)).tob_start(i));
--            else
--              TOB_DATA_OUT(32*(i+1)-1 downto 32*i) <= (others => '0');
--            end if;
          end loop;
--          for i in all_tob_array(std_int(MUON_TOB_TYPE)).max_tob_number to 7 loop
--            TOB_DATA_OUT(32*(i+1)-1 downto 32*i) <= (others => '0');
--          end loop;
          
        when JET_TOB_TYPE =>
          for i in 0 to all_tob_array(std_int(JET_TOB_TYPE)).max_tob_number-1 loop
            if all_tob_array(std_int(JET_TOB_TYPE)).tob_start(i) /= 0 then
              TOB_DATA_OUT(32*(i+1)-1 downto 32*i) <=
                JET_TOB_TYPE(3 downto 1) & CRATE_NUMBER(0)  &
                tob_data_in_l(
                  all_tob_array(std_int(JET_TOB_TYPE)).tob_start(i) +
                all_tob_array(std_int(JET_TOB_TYPE)).tob_length(i)-1 downto
                all_tob_array(std_int(JET_TOB_TYPE)).tob_start(i));
            else
              TOB_DATA_OUT(32*(i+1)-1 downto 32*i) <= (others => '0');
            end if;
          end loop;

          for i in all_tob_array(std_int(JET_TOB_TYPE)).max_tob_number to 7 loop
            TOB_DATA_OUT(32*(i+1)-1 downto 32*i) <= (others => '0');
          end loop;


--        when ENERGY_TOB_TYPE =>
--          for i in 0 to all_tob_array(std_int(ENERGY_TOB_TYPE)).max_tob_number-1 loop
--            if all_tob_array(std_int(ENERGY_TOB_TYPE)).tob_start(i) /= 0 then
--              TOB_DATA_OUT(32*(i+1)-1 downto 32*i) <=
--                ENERGY_TOB_TYPE(3 downto 0) & std_logic_vector(to_unsigned(i,4)) & x"00" &
--                tob_data_in_l(
--                  all_tob_array(std_int(ENERGY_TOB_TYPE)).tob_start(i) +
--                all_tob_array(std_int(ENERGY_TOB_TYPE)).tob_length(i)-1 downto
--                all_tob_array(std_int(ENERGY_TOB_TYPE)).tob_start(i));
--            else
--              TOB_DATA_OUT(32*(i+1)-1 downto 32*i) <= (others => '0');
--            end if;
--          end loop;  

        
        when ENERGY_TOB_TYPE =>
         for i in 0 to all_tob_array(std_int(ENERGY_TOB_TYPE)).max_tob_number-1 loop
           
            if all_tob_array(std_int(ENERGY_TOB_TYPE)).tob_start(i) /= 0 and (i = 0 or i = 1) then
              TOB_DATA_OUT(32*(i+1)-1 downto 32*i) <=
                ENERGY_TOB_TYPE(3 downto 0) & CRATE_NUMBER(0) &std_logic_vector(to_unsigned(i+1,3)) & x"00" & --x"0000"; --ps
                tob_data_in_l(
                  all_tob_array(std_int(ENERGY_TOB_TYPE)).tob_start(i) +
                all_tob_array(std_int(ENERGY_TOB_TYPE)).tob_length(i)-1 downto
                all_tob_array(std_int(ENERGY_TOB_TYPE)).tob_start(i));
              
            elsif all_tob_array(std_int(ENERGY_TOB_TYPE)).tob_start(i) /= 0 and i = 2 then
              TOB_DATA_OUT(32*(i+1)-1 downto 32*i) <=
                ENERGY_TOB_TYPE(3 downto 0) & CRATE_NUMBER(0) & std_logic_vector(to_unsigned(i+1,3)) & x"00" & --x"0000"; --ps
                tob_data_in_l(
                  all_tob_array(std_int(ENERGY_TOB_TYPE)).tob_start(i+1) +
                all_tob_array(std_int(ENERGY_TOB_TYPE)).tob_length(i+1)-1 downto
                all_tob_array(std_int(ENERGY_TOB_TYPE)).tob_start(i+1)) &
                tob_data_in_l(
                  all_tob_array(std_int(ENERGY_TOB_TYPE)).tob_start(i) +
                all_tob_array(std_int(ENERGY_TOB_TYPE)).tob_length(i)-1 downto
                all_tob_array(std_int(ENERGY_TOB_TYPE)).tob_start(i));
              
            elsif all_tob_array(std_int(ENERGY_TOB_TYPE)).tob_start(i) /= 0 and i = 3 then
              TOB_DATA_OUT(32*(i+1)-1 downto 32*i) <=
                ENERGY_TOB_TYPE(3 downto 0) & CRATE_NUMBER(0) & std_logic_vector(to_unsigned(i+1,3)) & x"00" & --x"0000"; --ps
                tob_data_in_l(
                all_tob_array(std_int(ENERGY_TOB_TYPE)).tob_start(i+1) +
                all_tob_array(std_int(ENERGY_TOB_TYPE)).tob_length(i+1)-1 downto
                all_tob_array(std_int(ENERGY_TOB_TYPE)).tob_start(i+1));

            elsif all_tob_array(std_int(ENERGY_TOB_TYPE)).tob_start(i) /= 0 and i = 4 then
              TOB_DATA_OUT(32*(i+1)-1 downto 32*i) <=
                ENERGY_TOB_TYPE(3 downto 0) & CRATE_NUMBER(0) & std_logic_vector(to_unsigned(6,3)) & x"00" & --x"0000"; --ps
                tob_data_in_l(
                all_tob_array(std_int(ENERGY_TOB_TYPE)).tob_start(i+1) +
                all_tob_array(std_int(ENERGY_TOB_TYPE)).tob_length(i+1)-1 downto
                all_tob_array(std_int(ENERGY_TOB_TYPE)).tob_start(i+1));
            elsif all_tob_array(std_int(ENERGY_TOB_TYPE)).tob_start(i) /= 0 and i = 5 then
              TOB_DATA_OUT(32*(i+1)-1 downto 32*i) <=
                ENERGY_TOB_TYPE(3 downto 0) & CRATE_NUMBER(0) & std_logic_vector(to_unsigned(5,3)) & x"00" & --x"0000"; --ps
                tob_data_in_l(
                all_tob_array(std_int(ENERGY_TOB_TYPE)).tob_start(i+1) +
                all_tob_array(std_int(ENERGY_TOB_TYPE)).tob_length(i+1)-1 downto
                all_tob_array(std_int(ENERGY_TOB_TYPE)).tob_start(i+1));
           

           
            else
              TOB_DATA_OUT(32*(i+1)-1 downto 32*i) <= (others => '0');
            end if;
          end loop;

         for i in all_tob_array(std_int(ENERGY_TOB_TYPE)).max_tob_number to 7 loop
            TOB_DATA_OUT(32*(i+1)-1 downto 32*i) <= (others => '0');
          end loop;


        when TRIGGER_TOB_TYPE =>
          for i in 0 to all_tob_array(std_int(TRIGGER_TOB_TYPE)).max_tob_number-1 loop
            if all_tob_array(std_int(TRIGGER_TOB_TYPE)).tob_start(i) /= 0 then
              TOB_DATA_OUT(32*(i+1)-1 downto 32*i) <=
                TRIGGER_TOB_TYPE(3 downto 0) & FPGA_NUMBER & std_logic_vector(to_unsigned(i,3)) & "000" & tob_data_in_l(69 downto 65) & --x"0000" ; --ps
                tob_data_in_l(
                  all_tob_array(std_int(TRIGGER_TOB_TYPE)).tob_start(i) +
                  all_tob_array(std_int(TRIGGER_TOB_TYPE)).tob_length(i)-1 downto
                  all_tob_array(std_int(TRIGGER_TOB_TYPE)).tob_start(i));
            else
              TOB_DATA_OUT(32*(i+1)-1 downto 32*i) <= (others => '0');
            end if;
          end loop;

          for i in all_tob_array(std_int(TRIGGER_TOB_TYPE)).max_tob_number to 7 loop
            TOB_DATA_OUT(32*(i+1)-1 downto 32*i) <= (others => '0');
          end loop;
            
        when others =>
          TOB_DATA_OUT(8*32-1 downto 7*32) <= (others => '0');
      end case;
    end if;
  end process;
  
end tob_parser;
