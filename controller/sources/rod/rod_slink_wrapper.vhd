library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_UNSIGNED.all;
use IEEE.NUMERIC_STD.all;
library UNISIM;
use UNISIM.VComponents.all;
use work.ipbus.all;

use work.rod_l1_topo_types_const.all;

entity rod_slink_wrapper is
  generic (
    SIMULATION                    : boolean              := false;
    INT_SIM                       : integer              := 0;
    VIVADO                        : boolean              := false;
    NUMBER_OF_PROCESSORS          : integer range 1 to 2 := 0;
    DEBUG                         : integer              := 0
    );
  port(
    -- clock and resets
    TTC_CLK40     : in std_logic;
    GCK_CLK40     : in std_logic;
    GCK_CLK65     : in std_logic;
    RODGCK_CLK80     : in std_logic_vector(1 downto 0);
    GCK_CLK80     : in std_logic;
    --GCK_CLK200    : in std_logic := '0';
    --GCK_CLK320    : in std_logic := '0';
    ROD_CLK       : in std_logic;
    MGT7_CLK      : in std_logic;
    MGT5_CLK      : in std_logic;
    MGT2_CLK      : in std_logic;
    ROD_GTX_RXP   : in std_logic_vector(1 downto 0);
    ROD_GTX_RXN   : in std_logic_vector(1 downto 0);
    GTX_ROD_SOFT_RST : in std_logic_vector(2 downto 0);
    ROD_GTX_DATA_EN  : in std_logic_vector(1 downto 0);
    ROD_RESET     : in std_logic;  -- tob builder, ddr-to-rod
    SLINK_RESET   : in std_logic_vector(31 downto 0);  -- pckBuilder, hola
    CLK_LOCKED_IN : in std_logic;
    ROD_RW_REG_IN  : in  rod_control_registers_array;
    ROD_RW_REG_OUT  : out  rod_status_registers_array;
    
    -- slink outputs
    OPTO_KR1_N, OPTO_KR1_P            : in  std_logic_vector(1 downto 0);
    OPTO_KT1_N, OPTO_KT1_P            : out std_logic_vector(1 downto 0);
--      SLINK_LFF_N_OUT : out std_logic;
--      SLINK_DOWN_N_OUT : out std_logic;
--      SLINK_FIFOFULL_OUT : out std_logic;
    SLINK_STATUS_REG_OUT              : out std_logic_vector(63 downto 0);
    SLINK_BUSY_CNT_TIME_PERIOD_REG_IN : in  std_logic_vector(31 downto 0);
    SLINK_BUSY_CNT_REG_OUT            : out std_logic_vector(31 downto 0);
    SLINK_IDLE_CNT_REG_OUT            : out std_logic_vector(31 downto 0);
    SLINK_FORMAT_VERSION_ROS          : in  std_logic_vector(31 downto 0);
    SLINK_FORMAT_VERSION_ROIB         : in  std_logic_vector(31 downto 0);

    RUN_NUMBER_IN   : in std_logic_vector(23 downto 0);
    RUN_TYPE_IN     : in std_logic_vector(7 downto 0);
    TRIGGER_TYPE_IN : in std_logic_vector(7 downto 0);
    TRIGGER_TYPE_READY_IN : in std_logic;
    --SUBDET_ID_IN    : in std_logic_vector(7 downto 0);
    MODULE_ID_IN    : in std_logic_vector(15 downto 0);
    ECR_IN          : in std_logic_vector(7 downto 0);
	 ORBIT_CTR_IN : in std_logic_vector(15 downto 0);
	 SEQUENCE_TYPE_IN : in std_logic_vector(15 downto 0);

    -- ttc inputs
    L1A_IN   : in std_logic;
    BCID_IN  : in std_logic_vector(11 downto 0);
    EVTID_IN : in std_logic_vector(23 downto 0);
	 
    STAT_WORD1_IN, STAT_WORD2_IN : in std_logic_vector(31 downto 0);

    BUSY_FROM_U2_IN : in  std_logic;
    L1_BUSY_DAQ         : out std_logic;
    L1_BUSY_ROIB         : out std_logic;
    DEBUG_OUT       : out std_logic_vector(255 downto 0);
    ICON_CONTROL_OUT : inout std_logic_vector(35 downto 0);
    CNTR_DEBUG    : out cntr_debug_array
    );
end rod_slink_wrapper;

architecture rtl of rod_slink_wrapper is
  
  component ila_rod_debug
    PORT (
      CONTROL : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
      CLK : IN STD_LOGIC;
      TRIG0 : IN STD_LOGIC_VECTOR(255 DOWNTO 0));
  end component;
  
  component icon_rod_debug
    PORT (
      CONTROL0 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
      CONTROL1 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
      CONTROL2 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
      CONTROL3 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
      CONTROL4 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
      CONTROL5 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
      CONTROL6 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
      CONTROL7 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
      CONTROL8 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
      CONTROL9 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
      CONTROL10 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0)
      );
  end component;

  

  signal slink_ready : std_logic_vector(11 downto 0);
  type ros_roi_bus_assignment_sig_array is array (0 to 2) of in_cntrl_array;
  signal ros_roi_bus_assignment_sig       : ros_roi_bus_assignment_sig_array;
  
  type number_of_slices_out_l_array is array (0 to 2) of slice_parameters_array_u;
  signal number_of_slices_out_l           : number_of_slices_out_l_array;
  type lvl0_offset_out_l_array is array (0 to 3) of std_logic_vector(3 downto 0);--offset_array;
  signal lvl0_offset_out_l                : lvl0_offset_out_l_array;
  type data_out_l_array is array (0 to 3) of std_logic_vector(127 downto 0);--out_data_array;
  signal data_out_l                       : data_out_l_array;
  type data_valid_in_l_array is array (0 to 3) of std_logic_vector(NUMBER_OF_OUTPUT_LINKS-1 downto 0);
  signal data_valid_in_l                  : data_valid_in_l_array;
  type actual_bus_number_out_l_array is array (0 to 3) of std_logic_vector(6 downto 0); --bus_number_array;
  signal actual_bus_number_out_l          : actual_bus_number_out_l_array;
  signal start_of_frame_l, end_of_frame_l : std_logic_vector(3 downto 0) := (others => '0');
  signal slink_gtx_reset : std_logic := '0';
  signal slink_ready_in_l, slink_event_ready_out_l, local_slink_ev_ready : std_logic_vector(NUMBER_OF_OUTPUT_LINKS-1 downto 0);
  type   slink_data_out_array is array (0 to NUMBER_OF_OUTPUT_LINKS-1) of std_logic_vector(31 downto 0);
  signal slink_data_out_a, local_slink_data                          : slink_data_out_array;
  type   link_number_array is array (0 to NUMBER_OF_OUTPUT_LINKS-1) of std_logic_vector(6 downto 0);
  signal link_number_l                             : link_number_array := (others => (others => '0'));
 
  signal dbg_slink : std_logic_vector(47 downto 0);

  type   ros_roib_sel is array (0 to NUMBER_OF_OUTPUT_LINKS-1) of std_logic_vector(31 downto 0);
  signal ros_roib_sel_table : ros_roib_sel;
  signal roib_input_links_numbers : proc_roib_input_links_numbers_array;
  signal send_data_on_crc_only_l : std_logic_vector(NUMBER_OF_ROS_ROI_INPUT_BUSES-1 downto 0):=(others => '0');
--Slink pck builder 
  signal link_lff_n      : std_logic_vector (NUMBER_OF_OUTPUT_LINKS-1 downto 0);
  signal slink_downt_n   : std_logic_vector (NUMBER_OF_OUTPUT_LINKS-1 downto 0);
  signal slink_fifo_full : std_logic_vector (NUMBER_OF_OUTPUT_LINKS-1 downto 0);

--busy fraction

  type rod_busy_cntr_array is array (0 to 7) of unsigned(31 downto 0);
  signal rod_busy_cntr      : rod_busy_cntr_array := (others => (others => '0'));
  type rod_real_busy_cntr_array is array (0 to 11) of unsigned(7 downto 0);
  signal rod_real_busy_cntr : rod_real_busy_cntr_array := (others => (others => '0'));
  signal cntr_for_busy      : unsigned(31 downto 0) :=(others => '0');
  signal l1_busy_l          : std_logic_vector(1 downto 0) := (others => '0');

  signal builder_busy_cntr      : unsigned(15 downto 0);
  signal builder_real_busy_cntr : unsigned(15 downto 0);
  signal builder_busy_l, builder_busy_ov : std_logic_vector(11 downto 0) := x"000";
  signal l1A_cntr               : unsigned(31 downto 0);
  signal packet_cntr            : std_logic_vector(31 downto 0);
  signal l1_busy_l_synch        : std_logic_vector(1 downto 0);
  signal token_in, token_out    : std_logic_vector(1 downto 0);
  signal mux_proc               : std_logic_vector(2 downto 0);

  signal crate_number : crate_number_array;

  signal gtx_rxusrclk2, gtx_rxencommaalign, gtx_txusrclk2 : std_logic_vector(11 downto 0);
  signal mgt7_clk_bufg, mgt2_clk_bufg : std_logic;
  type gtx_arr is array (0 to 11) of std_logic_vector(15 downto 0); 
  signal gtx_rxdata, gtx_txdata : gtx_arr;
  type gtx_cik_arr is array (0 to 11) of std_logic_vector(1 downto 0);
  signal gtx_rxcharisk, gtx_txcharisk : gtx_cik_arr;
  signal cplllock : std_logic_vector(11 downto 0);
  type gtx_loopback_arr is array(0 to 11) of std_logic_vector(2 downto 0);
  signal gtx_loopback : gtx_loopback_arr;
  signal refclk_bufg : std_logic;
  signal gtx_txresetdone, gtx_rxresetdone, gtx_txoutclk, gtx_txreset, gtx_rxreset, gtx_rxrecclk : std_logic_vector(11 downto 0);
  signal gtx_rxresetdone_i_r                                                                    : std_logic_vector(11 downto 0);
  signal gtx_rxresetdone_r                                                                      : std_logic_vector(11 downto 0);
  signal gtx_rxresetdone_r2                                                                     : std_logic_vector(11 downto 0);
  signal gtx_rxresetdone_r3                                                                     : std_logic_vector(11 downto 0);
  signal gtx_txresetdone_r                                                                      : std_logic_vector(11 downto 0);
  signal gtx_txresetdone_r2                                                                     : std_logic_vector(11 downto 0);
  signal gtx_reset                                                                              : std_logic_vector(11 downto 0) := (others => '0');
  signal gtx_tlk_reset                                                                          : std_logic_vector(11 downto 0);
  signal gtx_resetdone                                                                          : std_logic_vector(11 downto 0);
  signal lsc_reset                                                                              : std_logic_vector(11 downto 0);
  signal tlk_txclk, tlk_rxclk : std_logic_vector(11 downto 0);
  type bcid_offset_in_l_array is array (0 to NUMBER_OF_OUTPUT_LINKS) of std_logic_vector(3 downto 0);
  signal bcid_offset_in_l : bcid_offset_in_l_array;
  signal iconControl0, iconControl1, iconControl2, iconControl3,
         iconControl4, iconControl5, iconControl6, iconControl7, iconControl8, iconControl9, iconControl10 : std_logic_vector(35 downto 0);
  signal ila_trg_0, ila_trg_1, ila_trg_2, ila_trg_3, ila_trg_4,
         ila_trg_5, ila_trg_6, ila_trg_7, ila_trg_8, ila_trg_9, ila_trg_10  : std_logic_vector(255 downto 0);
  type slink_debug_array is array (0 to NUMBER_OF_OUTPUT_LINKS-1) of std_logic_vector(255 downto 0);
  signal slink_debug : slink_debug_array;
  signal status1, status2 : std_logic_vector(31 downto 0);
  signal startup_reset : std_logic_vector(11 downto 0);
  signal ttc_timeout, limited_roi_set, glink_error, cmm_parity_error, lvds_link_error, rod_fifo_overflow, data_transport_error, gt_timeout, bcn_mismatch : std_logic;
  
  type start_ctr_arr is array (0 to 2) of std_logic_vector(15 downto 0);
  type dv_ctr_arr is array (0 to 2) of std_logic_vector(7 downto 0);
  signal start_counter : start_ctr_arr;
  signal data_valid_ctr : dv_ctr_arr;
  signal crc_l: std_logic_vector(3 downto 0);
  
  attribute keep : string;
  attribute keep of gtx_txdata, gtx_txcharisk, gtx_txusrclk2: signal is "true";
  
  signal slink_up : std_logic_vector(NUMBER_OF_OUTPUT_LINKS - 1 downto 0);
  signal slink_ready_sim : std_logic;
  signal detector_event_type_l : std_logic_vector(31 downto 0);
  type builder_debug_array is array (0 to 2) of std_logic_vector(255 downto 0);
  signal builder_debug, builder_debug2 : builder_debug_array;
  --signal ddr_to_rod_dbg : std_logic_vector(255 downto 0);
  type debug_array_a is array (0 to 1) of std_logic_vector(255 downto 0);
  signal ddr_to_rod_dbg : debug_array_a;
  constant ros_select : std_logic_vector(11 downto 0) := x"ff9";

  signal gt_tx_fsm_reset_done_out : std_logic_vector(11 downto 0);
  signal gt_rx_fsm_reset_done_out : std_logic_vector(11 downto 0); 
  signal gtx_cntr : unsigned(15 downto 0) :=(others => '0');
  signal gtx_rst : std_logic;
  signal qpll_lock : std_logic;
  signal qpll_reset : std_logic:='0';
  signal rxcdrreset_l : std_logic_vector(11 downto 0);
  type rxcdrreset_cntr_array is array (0 to 11) of std_logic_vector(31 downto 0);
  signal rxcdrreset_cntr : rxcdrreset_cntr_array;
  signal slink_l1adata_fifo_full : std_logic_vector(11 downto 0) := (others => '0');
  type subdet_id_in_l_array is array (0 to 11) of std_logic_vector(31 downto 0);
  signal subdet_id_in_l : subdet_id_in_l_array;
  type builder_busy_debug_array is array (0 to 11) of std_logic_vector(2 downto 0);
  signal builder_busy_debug : builder_busy_debug_array;
  type cntr_debug_array_array is array (0 to 5) of cntr_debug_array;
  signal cntr_debug_l : cntr_debug_array_array;
  signal local_busy : std_logic_vector(11 downto 0) := (others => '0');
  --rod_gtx
  signal rod_gtx_soft_reset : std_logic := '0';
  signal gtx_rx_fsm_reset_done, rod_gtx_rxresetdone : std_logic_vector(1 downto 0) := "00";
  signal rod_gtx_rxusrclk_in, rod_gtx_rxoutclk : std_logic_vector(1 downto 0) := "00";
  type rod_gtx_rxdata_array is array (0 to 1) of std_logic_vector(15 downto 0);
  signal rod_gtx_rxdata, rod_gtx_rxdata_ila : rod_gtx_rxdata_array := (others => (others => '0'));
  type rod_gtx_rx_array is array (0 to 1) of std_logic_vector(1 downto 0);
  signal rod_gtx_rxdisperr, rod_gtx_rxnotintable, rod_gtx_rxcharisk : rod_gtx_rx_array := (others => (others => '0'));
  
  signal rxcharisk_saved, rod_gtx_rxcharisk_sync : rod_gtx_rx_array;
  signal rod_gtx_rxdata_sync, gtx_data_selected, gtx_data_to_fifo, gtx_data_to_fifo_pipe : rod_gtx_rxdata_array;
  signal rod_gtx_fifo_empty, rod_gtx_fifo_full, gtx_data_valid, gtx_data_valid_fifo, gtx_data_valid_fifo_pipe, gtx_data_valid_sync, rod_gtx_fifo_rd, rod_gtx_fifo_rd_sync, gtx_data_in_valid : std_logic_vector(1 downto 0);
  signal rod_gtx_qplllock, rod_gtx_qplllockdet : std_logic;
  type local_gtx_rx_array is array (0 to 1) of std_logic_vector(63 downto 0);
  signal gtx_data_in: local_gtx_rx_array;
  signal gtx_cplllock : std_logic_vector(1 downto 0) := "00";
  type gtx_error_cntr_array is array (0 to 1) of std_logic_vector(7 downto 0);
  signal gtx_err_cntr : gtx_error_cntr_array  :=(others => (others => '0'));
  signal gck_test, sl_testrx, sl_testtx : std_logic := '0';
  constant C0 : integer := 0;
  constant C1 : integer := 1;
  signal reset_fifos : std_logic_vector(2 downto 0) := (others => '0');
  type reset_fifos_cntr_array is array (0 to 1) of unsigned(15 downto 0);
  signal reset_fifos_cntr: reset_fifos_cntr_array := (others => (others => '0'));
begin

   SYNC_CNTR_DEBUG: process (GCK_CLK40)
    begin
      if rising_edge(GCK_CLK40) then
        --ddr to rod
        --U1
        CNTR_DEBUG(0) <= cntr_debug_l(0)(0);  --from ddr start
        CNTR_DEBUG(1) <= cntr_debug_l(0)(1);  --finished sending
        CNTR_DEBUG(2) <= cntr_debug_l(0)(2);  --fifo_data_cntr  
        --U2
        CNTR_DEBUG(3) <= cntr_debug_l(1)(0);  --from ddr start  
        CNTR_DEBUG(4) <= cntr_debug_l(1)(1);  --finished sending
        CNTR_DEBUG(5) <= cntr_debug_l(1)(2);  --fifo_data_cntr  
        --tob builder
        --U1
        CNTR_DEBUG(6) <= cntr_debug_l(2)(0);  --data to builder start
        CNTR_DEBUG(7) <= cntr_debug_l(2)(1);  --data from builder to slink finished
        CNTR_DEBUG(8) <= cntr_debug_l(2)(2);  --builder fifo cntr
        CNTR_DEBUG(9) <= cntr_debug_l(2)(3);  --fiber headers cntr 
        CNTR_DEBUG(10) <= cntr_debug_l(2)(4); --slink data cntr
        --U2
        CNTR_DEBUG(11) <= cntr_debug_l(3)(0); --data to builder start               
        CNTR_DEBUG(12) <= cntr_debug_l(3)(1); --data from builder to slink finished 
        CNTR_DEBUG(13) <= cntr_debug_l(3)(2); --builder fifo cntr                   
        CNTR_DEBUG(14) <= cntr_debug_l(3)(3); --fiber headers cntr                  
        CNTR_DEBUG(15) <= cntr_debug_l(3)(4); --slink data cntr
        --slink
        --U1
        CNTR_DEBUG(16) <= cntr_debug_l(4)(0); --start of data
        CNTR_DEBUG(17) <= cntr_debug_l(4)(1); --end of sending data to slink
        CNTR_DEBUG(18) <= cntr_debug_l(4)(2); --number of L1A headers saved
        CNTR_DEBUG(19) <= cntr_debug_l(4)(3); --number of L1A headers read
        CNTR_DEBUG(20) <= cntr_debug_l(4)(4); --fifo write when busy !
        CNTR_DEBUG(21) <= cntr_debug_l(4)(5); --nr of trigger type saved !                                      
        
        --U2
        CNTR_DEBUG(22) <= cntr_debug_l(5)(0);--start of data               
        CNTR_DEBUG(23) <= cntr_debug_l(5)(1);--end of sending data to slink
        CNTR_DEBUG(24) <= cntr_debug_l(5)(2);--number of L1A headers saved 
        CNTR_DEBUG(25) <= cntr_debug_l(5)(3);--number of L1A headers read  
        CNTR_DEBUG(26) <= cntr_debug_l(5)(4);--fifo write when busy !
        CNTR_DEBUG(27) <= cntr_debug_l(5)(5); --nr of trigger type saved !                                                                                   
      end if;
    end process SYNC_CNTR_DEBUG;

    
  -----------------------------------------------------------------------------
  -- ila
  -----------------------------------------------------------------------------
    ENABLE_SLINK_ILA: if DEBUG = 1 generate
      SLINK_ILA_INST: entity work.slink_ila
        port map (
          clk     => tlk_txclk(0),
          probe0  => gtx_rxdata(0),
          probe1  => gtx_txdata(0),
          probe2  => gtx_rxcharisk(0),
          probe3  => gtx_txcharisk(0),
          probe4(0)  => SLINK_RESET(15),
          probe5(0)  => gt_tx_fsm_reset_done_out(0),
          probe6(0)  => gt_rx_fsm_reset_done_out(0),
          probe7(0)  => CLK_LOCKED_IN,
          probe8(0)  => gtx_tlk_reset(0),
          probe9(0)  => slink_up(0),
          probe10(0) => link_lff_n(0),
          probe11(0) => gck_test,
          probe12(0) => sl_testrx,
          probe13(0) => sl_testtx,
          probe14(0) => '0',
          probe15(0) => '0');
      end generate ENABLE_SLINK_ILA;

  -----------------------------------------------------------------------------
  -- main 
  -----------------------------------------------------------------------------

  
  L1A_CTR_PROC : process (GCK_CLK40)
  begin
    if rising_edge(GCK_CLK40) then
      if ROD_RW_REG_IN(6)(1) = '1' then
        l1A_cntr           <= (others => '0');
        l1_busy_l_synch(0) <= '0';
      elsif l1_busy_l_synch(0) = '0' and l1_busy_l(0) = '1' then
        l1A_cntr        <= l1A_cntr + 1;
        l1_busy_l_synch <= l1_busy_l;
      else
        l1A_cntr        <= l1A_cntr;
        l1_busy_l_synch <= l1_busy_l;
      end if;
    end if;
  end process L1A_CTR_PROC;
  ROD_RW_REG_OUT(39) <= std_logic_vector(l1A_cntr);

  MAIN_BUSY_CNTR : process (GCK_CLK40)
    begin
    if rising_edge(GCK_CLK40) then
--    if cntr_for_busy  = unsigned(ROD_RW_REG_IN(7)) then
--      cntr_for_busy     <= (others => '0');
--    else
      if cntr_for_busy  = x"01ffffff" then
        cntr_for_busy <= (others => '0');
      else
        cntr_for_busy <= cntr_for_busy + 1;
      end if;
    end if;  
  end process MAIN_BUSY_CNTR;

  COUNT_BUSY_FRACTION: for i in 0 to 7 generate
    CALC_ROD_BUSY : process (GCK_CLK40)
    begin
      if rising_edge(GCK_CLK40) then
        if cntr_for_busy  = x"01ffffff" then 
          rod_busy_cntr(i)  <= (others => '0');
          for j in 0 to 7 loop
            rod_real_busy_cntr(i) <= rod_busy_cntr(i)(25 downto 18);
          end loop;
        elsif local_busy(i) = '1' then
          rod_busy_cntr(i)      <= rod_busy_cntr(i) + 1;
          rod_real_busy_cntr(i) <= rod_real_busy_cntr(i);
        else
          rod_busy_cntr(i)      <= rod_busy_cntr(i);
          rod_real_busy_cntr(i) <= rod_real_busy_cntr(i);
        end if;
      end if;
    end process CALC_ROD_BUSY;
  end generate COUNT_BUSY_FRACTION;

    
    SYNCH_BUSY_CNTR :process (GCK_CLK40)
    begin
      if rising_edge(GCK_CLK40) then
        local_busy(9 downto 0) <=
        builder_busy_debug(1)(2) & builder_busy_debug(0)(2) & builder_busy_debug(1)(1) & builder_busy_debug(0)(1) & builder_busy_debug(1)(0) & builder_busy_debug(0)(0) &
        slink_l1adata_fifo_full(1) & slink_l1adata_fifo_full(0) &
        (not link_lff_n(1)) & (not link_lff_n(0));

        ROD_RW_REG_OUT(20) <=
          std_logic_vector(rod_real_busy_cntr(3)) & std_logic_vector(rod_real_busy_cntr(2)) &
          std_logic_vector(rod_real_busy_cntr(1)) & std_logic_vector(rod_real_busy_cntr(0));
        ROD_RW_REG_OUT(34) <=
          std_logic_vector(rod_real_busy_cntr(7)) & std_logic_vector(rod_real_busy_cntr(6)) &
          std_logic_vector(rod_real_busy_cntr(5)) & std_logic_vector(rod_real_busy_cntr(4));
        ROD_RW_REG_OUT(40) <=
          std_logic_vector(rod_real_busy_cntr(11)) & std_logic_vector(rod_real_busy_cntr(10)) &
          std_logic_vector(rod_real_busy_cntr(9)) & std_logic_vector(rod_real_busy_cntr(8));
      end if;
    end process SYNCH_BUSY_CNTR;


  ROD_RW_REG_OUT(2)(l1_busy_l'length-1 downto 0) <= l1_busy_l;
  ROD_RW_REG_OUT(2)(builder_busy_l'length-1+16 downto 16) <= builder_busy_l;

  ITERATE_PROCESSORS: for j in 0 to 1 generate
    SET_ASSIGNMENTS : for i in 0 to ros_roi_bus_assignment_sig(0)'high generate
      ros_roi_bus_assignment_sig(j)(i) <= ROD_RW_REG_IN(j*11+14+(i/8))((i mod 8)*4+ros_roi_bus_assignment_sig(0)(0)'length-1 downto (i mod 8)*4);
    end generate SET_ASSIGNMENTS;
  end generate ITERATE_PROCESSORS;

  SET_CRATE_NUMBER_FOR_INPUT_LINK: for i in 0 to NUMBER_OF_ROS_ROI_INPUT_BUSES - 1 generate
    crate_number(i) <= ROD_RW_REG_IN(25+(i/8))(((i mod 8)+1)*4-1 downto (i mod 8)*4);
  end generate   SET_CRATE_NUMBER_FOR_INPUT_LINK;

  ITERATE_PROCESSORS_B: for j in 0 to 1 generate
    SET_INPUT_LINKS_TO_COPY: for i in 0 to 3 generate
      roib_input_links_numbers(j)(i) <= ROD_RW_REG_IN(48+j)((i+1)*8-2 downto i*8);
    end generate SET_INPUT_LINKS_TO_COPY;
  end generate ITERATE_PROCESSORS_B;

  CONNECT_BUSY_DEBUG: for i in 0 to NUMBER_OF_OUTPUT_LINKS-1 generate
    SLINK_STATUS_REG_OUT(16*i+2) <= slink_l1adata_fifo_full(i);
    SLINK_STATUS_REG_OUT(16*i+5 downto 16*i+3) <= builder_busy_debug(i);--slink_fifo_prog_full & fiber_cntr_prog_full & builder_fifo_prog_full;
    SLINK_STATUS_REG_OUT(16*i+6) <= l1_busy_l(0);
    SLINK_STATUS_REG_OUT(16*i+7) <= l1_busy_l(1);
    SLINK_STATUS_REG_OUT(16*i+8) <= gt_rx_fsm_reset_done_out(i);
    SLINK_STATUS_REG_OUT(16*i+9) <= gt_tx_fsm_reset_done_out(i);
  end generate CONNECT_BUSY_DEBUG;
    
  L1_BUSY_DAQ <=  ((l1_busy_l(0) or l1_busy_l(1) or slink_l1adata_fifo_full(0)) or builder_busy_l(0)) and (not ROD_RW_REG_IN(0)(0+16));
  L1_BUSY_ROIB <= (((l1_busy_l(0) or l1_busy_l(1) or slink_l1adata_fifo_full(1)) or builder_busy_l(1)) and (not ROD_RW_REG_IN(0)(1+16)))
                  or 
                  (((l1_busy_l(0) or l1_busy_l(1) or slink_l1adata_fifo_full(2)) or builder_busy_l(2)) and (not ROD_RW_REG_IN(0)(2+16)));
    
  GENERATE_V1_V2_DDR_TO_ROD : for i in 0 to NUMBER_OF_PROCESSORS -1 generate
    DDR_TO_ROD_INST : entity work.ddr_to_rod
      generic map (
        VIVADO => VIVADO,
        EN_DEBUG => 0,
        NUMBER_OF_PROCESSORS => NUMBER_OF_PROCESSORS,
        PROCESSOR_NUMBER => i
        )
      port map (
        RESET                       => ROD_RESET,
        DATA_IN_CLK                 => RODGCK_CLK80(i),
        DATA_OUT_CLK                => ROD_CLK,
        FIFO_DATA_THR               => ROD_RW_REG_IN(9)(16*(i+1)-4 downto 16*i),
        L1_BUSY                     => l1_busy_l(i),
        DDR_ROS_ROI_IN_DATA         => gtx_data_in(i),
        DATA_VALID_IN               => gtx_data_in_valid(i),
        OUT_DATA                    => data_out_l(i),
        CRC_OUT                     => crc_l(i),
        DATA_VALID_OUT              => data_valid_in_l(i),
        ROIB_INPUT_LINKS            => roib_input_links_numbers(i),
        MAX_OFFSET_IN               => ROD_RW_REG_IN(53)(3 downto 0), 
        ACTUAL_BUS_NUMBER_OUT       => actual_bus_number_out_l(i),
        LVL0_OFFSET_OUT             => lvl0_offset_out_l(i),
        ROS_ROI_BUS_ASSIGNMENT      => ros_roi_bus_assignment_sig(0),
        START_OF_FRAME              => start_of_frame_l(i),
        END_OF_FRAME                => end_of_frame_l(i),
        TOKEN_IN                    => token_in(i),
        TOKEN_OUT                   => token_out(i),
        MUX_PROC                    => mux_proc(i),
        DEBUG                       => ddr_to_rod_dbg(i),
        CNTR_DEBUG                  => cntr_debug_l(i)
        );
  end generate GENERATE_V1_V2_DDR_TO_ROD;
  token_in(1) <= token_out(0);
  token_in(0) <= token_out(1);

  
  MUX_ROD_PROCESSOR_DATA : process (ROD_CLK)
  begin
    if rising_edge(ROD_CLK) then
      if mux_proc(0) = '0' then
        start_of_frame_l(2) <= start_of_frame_l(0);
        end_of_frame_l(2) <= end_of_frame_l(0);
        data_out_l(2) <= data_out_l(0);
        --data_valid_in_l(2) <=  data_valid_in_l(0);
        actual_bus_number_out_l(2) <= actual_bus_number_out_l(0);
        number_of_slices_out_l(2) <=  number_of_slices_out_l(0);
        lvl0_offset_out_l(2) <= lvl0_offset_out_l(0);
        ros_roi_bus_assignment_sig(2) <= ros_roi_bus_assignment_sig(0);
        crc_l(2) <= crc_l(0);
      else
        start_of_frame_l(2) <= start_of_frame_l(1);
        end_of_frame_l(2) <= end_of_frame_l(1);
        data_out_l(2) <= data_out_l(1);
        --data_valid_in_l(2) <=  data_valid_in_l(1);
        actual_bus_number_out_l(2) <= actual_bus_number_out_l(1);
        number_of_slices_out_l(2) <=  number_of_slices_out_l(1);
        lvl0_offset_out_l(2) <= lvl0_offset_out_l(1);
        ros_roi_bus_assignment_sig(2) <= ros_roi_bus_assignment_sig(1);
        crc_l(2) <= crc_l(1);
      end if;
    end if;
  end process MUX_ROD_PROCESSOR_DATA;



  SEND_DATA_VALID_FOR_U1_U2_DATA: process (ROD_CLK) 
  begin
    if rising_edge(ROD_CLK) then
      data_valid_in_l(2) <= data_valid_in_l(0) or data_valid_in_l(1);
    end if;  
  end process SEND_DATA_VALID_FOR_U1_U2_DATA;
  
  
  CRC_CHECK_AND_SEND_DATA: for i in 0 to NUMBER_OF_ROS_ROI_INPUT_BUSES - 1 generate
    send_data_on_crc_only_l(i) <= ROD_RW_REG_IN(50+i/32)(i mod 32);
  end generate CRC_CHECK_AND_SEND_DATA;

  ASSIGN_SLINK_ID: for i in 0 to 11 generate
    subdet_id_in_l(i) <= ROD_RW_REG_IN(55+i);
  end generate ASSIGN_SLINK_ID;


    
    
  PIPE_FOR_RELAX_TIMING: process (ROD_CLK)
  begin
    if rising_edge(ROD_CLK) then
      data_out_l(3) <= data_out_l(2);
      actual_bus_number_out_l(3) <= actual_bus_number_out_l(2);
      lvl0_offset_out_l(3) <= lvl0_offset_out_l(2);
      crc_l(3) <= crc_l(2);
      data_valid_in_l(3) <= data_valid_in_l(2);
      start_of_frame_l(3) <= start_of_frame_l(2);
      end_of_frame_l(3) <= end_of_frame_l(2);
      mux_proc(2) <= mux_proc(0);       --in purpose 1st bit is used by second processor
    end if;
  end process PIPE_FOR_RELAX_TIMING;

--    ROD_MUX_ILA_INST: entity work.rod_mux_ila
--      port map(
--        clk => ROD_CLK,
--        probe0(0) => start_of_frame_l(3),
--        probe1(0) => end_of_frame_l(3),
--        probe2 => data_out_l(3),
--        probe3 => actual_bus_number_out_l(3));
        

    
  GENERATE_OUPUT_SLINKS : for i in 0 to NUMBER_OF_OUTPUT_LINKS -1 generate

    TOB_SLINK_BUILDER_INST : entity work.tob_slink_builder
      generic map (
        VIVADO => VIVADO,
        DEBUG => 0,
        NUMBER_OF_PROCESSORS => NUMBER_OF_PROCESSORS
        )
      port map (
        RESET                 => ROD_RESET,
        CLK                   => ROD_CLK,
        TOB_DATA_IN           => data_out_l(3),--data_out_l(2),
        LINK_NUMBER           => actual_bus_number_out_l(3),--actual_bus_number_out_l(2),  
        BCID_OFFSET_IN        => lvl0_offset_out_l(3),--lvl0_offset_out_l(2),
        TOTAL_NR_OF_SLICES    => ROD_RW_REG_IN(77)(3 downto 0), 
        MAX_OFFSET_IN         => ROD_RW_REG_IN(53)(i*4+3 downto i*4), 
        CRC_IN                => crc_l(3),--crc_l(2),
        ENABLE_ALL_CMX_DATA   => ROD_RW_REG_IN(46)(0),
        CRATE_ASSIGNMENT      => crate_number,
        ROS_SELECTED          => ros_select(i),
        DATA_VALID_IN         => data_valid_in_l(3)(i),--data_valid_in_l(2)(i),
        BEGINNING_OF_DATA     => start_of_frame_l(3),--start_of_frame_l(2),
        END_OF_DATA           => end_of_frame_l(3),--end_of_frame_l(2),
        ACTIVE_LINKS_NUMBER   => ROD_RW_REG_IN(47)((i+1)*8-2 downto i*8), 
        FPGA_NUMBER           => mux_proc(2),--mux_proc(0),here two by purpose
        TOB_BUILDER_FIFO_THR  => ROD_RW_REG_IN(74)(13 downto 0),
        FIBER_CNTR_FIFO_THR   => ROD_RW_REG_IN(75)(10 downto 0),
        SLINK_FIFO_32kW_THR   => ROD_RW_REG_IN(76)(14 downto 0),
        BUILDER_BUSY          => builder_busy_l(i),
        SLINK_CLK             => GCK_CLK40,
        SLINK_DATA_OUT        => slink_data_out_a(i),
        SLINK_READY_IN        => slink_ready_in_l(i),
        SLINK_EVENT_READY_OUT => slink_event_ready_out_l(i),
        BUSY_DEBUG            => builder_busy_debug(i),
--        DEBUG                 => builder_debug(i),
--        DEBUG2                => builder_debug2(i),
        CNTR_DEBUG            => cntr_debug_l(i+2)
        );

    
    detector_event_type_l <= ORBIT_CTR_IN & SEQUENCE_TYPE_IN;
    SLINKPCKBUILDER_INST : entity work.slinkPckBuilder
      generic map (
        EN_DEBUG => 0,
        SIMULATION => SIMULATION
        )
      port map (
        TTC_CLK40       => TTC_CLK40,
        SYSCLK          => GCK_CLK40,
        CLK_LOCKED_IN   => CLK_LOCKED_IN,
        URESET_IN       => SLINK_RESET(i+24),
        RESET           => ROD_RESET,
        SLINK_RESET     => SLINK_RESET(i+16),
        SLINK_READY_OUT => slink_ready(i),
        DIABLE_SLINK    => ROD_RW_REG_IN(0)(16+i),
        --from parser to slink
        SAVE_HEADERS_IN     => TRIGGER_TYPE_READY_IN,
        ENABLE_IN           => slink_event_ready_out_l(i),
        READY_OUT           => slink_ready_in_l(i),
        PAYLOAD_IN          => slink_data_out_a(i), 
        L1A_IN              => L1A_IN,
        --header words (from IPBUS or TTCrc)
        MODULE_ID           => MODULE_ID_IN,
        RUN_TYPE            => RUN_TYPE_IN,  --0 = physics, 1 = Calibration, 2 = Cosmics, 15=test
        RUN_NUMBER          => RUN_NUMBER_IN,
        ECR_ID              => ECR_IN,
        ROD_L1_ID           => EVTID_IN,     --x"00_0000",
        ROD_BCN             => BCID_IN,      --x"000",
        TRIGGER_TYPE        => TRIGGER_TYPE_IN,
        TRIGGER_TYPE_READY_IN => TRIGGER_TYPE_READY_IN,
        TRG_TYPE_TIMEOUT    => ROD_RW_REG_IN(67)(11 downto 0),    
        SUB_DET_ID_IN       => subdet_id_in_l(i),
        DETECTOR_EVENT_TYPE => detector_event_type_l,--ORBIT_CTR_IN & SEQUENCE_TYPE_IN,
        FORMAT_VERSION_IN   => ros_roib_sel_table(i),  
        DEBUG_OUT               => slink_debug(i),--dbg_slink,
        LFF_N_OUT               => link_lff_n(i),--SLINK_STATUS_REG_OUT(16*i+1),  --link_lff_n(i),
        LDOWN_N_OUT             => slink_up(i),  --slink_downt_n(i),
        FIFOFULL_OUT            => slink_l1adata_fifo_full(i),--)SLINK_STATUS_REG_OUT(16*i+2),  --slink_fifo_full(i),
        BUSY_CNT_TIME_PERIOD_IN => SLINK_BUSY_CNT_TIME_PERIOD_REG_IN,
        BUSY_CNT_OUT            => open,--SLINK_BUSY_CNT_REG_OUT(i),
        IDLE_CNT_OUT            => open,--SLINK_IDLE_CNT_REG_OUT,
        -- connections to external gtx 
        TLK_SYS_RST        => gtx_tlk_reset(i),
        TLK_RXCLK_IN       => tlk_rxclk(i),  
        TLK_GTXCLK_IN      => tlk_txclk(i),  
        TLK_GTXCLK_DIV     => '0',
        -- GTX receive ports
        GTX_RXUSRCLK2      => tlk_rxclk(i),
        GTX_RXDATA         => gtx_rxdata(i),
        GTX_RXCHARISK      => gtx_rxcharisk(i),
        GTX_RXENCOMMAALIGN => gtx_rxencommaalign(i),
        -- GTX transmit ports
        GTX_TXUSRCLK2      => tlk_txclk(i),
        GTX_TXCHARISK      => gtx_txcharisk(i),
        GTX_TXDATA         => gtx_txdata(i),
        --stat data
        STAT_WORD1_IN => STAT_WORD1_IN,
        STAT_WORD2_IN => STAT_WORD2_IN,
        CNTR_DEBUG    => cntr_debug_l(i+4)
        );
    SLINK_STATUS_REG_OUT(16*i+1) <= link_lff_n(i);	  

    SLINK_STATUS_REG_OUT(16*i) <= slink_up(i);
        
  end generate GENERATE_OUPUT_SLINKS;
  
  Gen_ros_roib_sel_table1 : for i in 0 to NUMBER_OF_OUTPUT_LINKS -1 generate
  begin
    ros_roib_sel_table(i) <= SLINK_FORMAT_VERSION_ROS;
  end generate;
  Gen_ros_roib_sel_table2 : for i in NUMBER_OF_OUTPUT_LINKS to NUMBER_OF_OUTPUT_LINKS -1 generate
  begin
    ros_roib_sel_table(i) <= SLINK_FORMAT_VERSION_ROIB;
  end generate;

  status1(0) <= bcn_mismatch;
  status1(1) <= '0';
  status1(2) <= gt_timeout;
  status1(3) <= data_transport_error;
  status1(4) <= rod_fifo_overflow;
  status1(15 downto 5) <= (others => '0');
  status1(16) <= lvds_link_error;
  status1(17) <= cmm_parity_error;
  status1(18) <= glink_error;
  status1(31 downto 19) <= (others => '0');
  status2(0) <= '0';
  status2(1) <= limited_roi_set;
  status2(15 downto 2) <= (others => '0');
  status2(16) <= ttc_timeout;
  status2(31 downto 17) <= (others => '0'); 
  DEBUG_OUT(47 downto 0)   <= dbg_slink;
  DEBUG_OUT(219 downto 48) <= (others => '0');
  DEBUG_OUT(255 downto 220) <= (others => '0'); 
  
  ----------------------------------GTX----------------------------------
  
 DISABLE_GTX_WHEN_SIM: if SIMULATION = false generate
    
    LINK_MANAGE_PROC_GEN : for i in 0 to 1 generate
                                                       
      --txoutclk_bufg0_i : BUFG port map (I => gtx_txoutclk(i), O => tlk_txclk(i));
      --rxrecclk_bufg1_i : BUFH port map (I => gtx_rxrecclk(i), O => tlk_rxclk(i));
      gtx_tlk_reset(i) <= not gt_tx_fsm_reset_done_out(i) or not gt_rx_fsm_reset_done_out(i) or ROD_RESET; --

    end generate LINK_MANAGE_PROC_GEN;

    --DEBUG_CNTR40: process(GCK_CLK40)
    --begin
    --  if rising_edge(GCK_CLK40) then
    --    gck_test <= not gck_test;
    --  end if;
    --end process DEBUG_CNTR40;

    --DEBUG_CNTR_SLINKCLK_RX: process(tlk_rxclk(0))
    --begin
    --  if rising_edge(tlk_rxclk(0)) then
    --    sl_testrx <= not sl_testrx;
    --  end if;
    --end process DEBUG_CNTR_SLINKCLK_RX;

    --DEBUG_CNTR_SLINKCLK_TX: process(tlk_txclk(0))
    --begin
    --  if rising_edge(tlk_txclk(0)) then
    --    sl_testtx <= not sl_testtx;
    --  end if;
    --end process DEBUG_CNTR_SLINKCLK_TX;

    

    slink_gtx_reset <= ROD_RESET or SLINK_RESET(15);
      
    new_gtx_slink_i: entity work.slink_gtx_vivado
      port map (
        SOFT_RESET_TX_IN            => slink_gtx_reset,--SLINK_RESET(15),
        SOFT_RESET_RX_IN            => slink_gtx_reset,--SLINK_RESET(15),
        DONT_RESET_ON_DATA_ERROR_IN => '1',
        GTREFCLK_IN                 => MGT7_CLK, --modified to cooperate with IPbus
                                                 --ethernet quad
        GT0_TX_FSM_RESET_DONE_OUT   => gt_tx_fsm_reset_done_out(0),
        GT0_RX_FSM_RESET_DONE_OUT   => gt_rx_fsm_reset_done_out(0),
        GT0_DATA_VALID_IN           => '1',
        GT1_TX_FSM_RESET_DONE_OUT   => gt_tx_fsm_reset_done_out(1),
        GT1_RX_FSM_RESET_DONE_OUT   => gt_rx_fsm_reset_done_out(1),
        GT1_DATA_VALID_IN           => '1',
        GT0_TXUSRCLK_OUT            => tlk_txclk(0),
        GT0_TXUSRCLK2_OUT           => open,
        GT0_RXUSRCLK_OUT            => tlk_rxclk(0),
        GT0_RXUSRCLK2_OUT           => open,           
        GT1_TXUSRCLK_OUT            => tlk_txclk(1),
        GT1_TXUSRCLK2_OUT           => open,           
        GT1_RXUSRCLK_OUT            => tlk_rxclk(1),
        GT1_RXUSRCLK2_OUT           => open,           
        gt0_cpllfbclklost_out       => open,
        gt0_cplllock_out            => open,
        gt0_cpllreset_in            => '0',
        gt0_drpaddr_in              => (others => '0'),
        gt0_drpdi_in                => (others => '0'),
        gt0_drpdo_out               => open,
        gt0_drpen_in                => '0',
        gt0_drprdy_out              => open,
        gt0_drpwe_in                => '0',
        gt0_dmonitorout_out         => open,
        gt0_eyescanreset_in         => '0',
        gt0_rxuserrdy_in            => CLK_LOCKED_IN,
        gt0_eyescandataerror_out    => open,
        gt0_eyescantrigger_in       => '0',
        gt0_rxdata_out              => gtx_rxdata(0),
        gt0_rxdisperr_out           => open,
        gt0_rxnotintable_out        => open,
        gt0_gtxrxp_in               => OPTO_KR1_P(0),
        gt0_gtxrxn_in               => OPTO_KR1_N(0),
        gt0_rxdfelpmreset_in        => '0',
        gt0_rxmonitorout_out        => open,
        gt0_rxmonitorsel_in         => (others => '0'),
        gt0_rxoutclkfabric_out      => open,
        gt0_gtrxreset_in            => '0',--gtx_rxreset(0),
        gt0_rxpmareset_in           => '0',
        gt0_rxcharisk_out           => gtx_rxcharisk(0),
        gt0_rxresetdone_out         => open,
        gt0_gttxreset_in            => '0',--gtx_txreset(0),
        gt0_txuserrdy_in            => CLK_LOCKED_IN,
        gt0_txdata_in               => gtx_txdata(0),
        gt0_gtxtxn_out              => OPTO_KT1_N(0),
        gt0_gtxtxp_out              => OPTO_KT1_P(0),
        gt0_txoutclkfabric_out      => open,
        gt0_txoutclkpcs_out         => open,
        gt0_txcharisk_in            => gtx_txcharisk(0),
        gt0_txresetdone_out         => open,
        gt1_cpllfbclklost_out       => open,                       
        gt1_cplllock_out            => open,                       
        gt1_cpllreset_in            => '0',                        
        gt1_drpaddr_in              => (others => '0'),            
        gt1_drpdi_in                => (others => '0'),            
        gt1_drpdo_out               => open,                       
        gt1_drpen_in                => '0',                        
        gt1_drprdy_out              => open,                       
        gt1_drpwe_in                => '0',                        
        gt1_dmonitorout_out         => open,                       
        gt1_eyescanreset_in         => '0',                        
        gt1_rxuserrdy_in            => CLK_LOCKED_IN,              
        gt1_eyescandataerror_out    => open,                       
        gt1_eyescantrigger_in       => '0',                        
        gt1_rxdata_out              => gtx_rxdata(1),              
        gt1_rxdisperr_out           => open,                       
        gt1_rxnotintable_out        => open,                       
        gt1_gtxrxp_in               => OPTO_KR1_P(1),              
        gt1_gtxrxn_in               => OPTO_KR1_N(1),              
        gt1_rxdfelpmreset_in        => '0',                        
        gt1_rxmonitorout_out        => open,                       
        gt1_rxmonitorsel_in         => (others => '0'),                        
        gt1_rxoutclkfabric_out      => open,                       
        gt1_gtrxreset_in            => '0',--gtx_rxreset(1),       
        gt1_rxpmareset_in           => '0',                        
        gt1_rxcharisk_out           => gtx_rxcharisk(1),           
        gt1_rxresetdone_out         => open,
        gt1_gttxreset_in            => '0',--gtx_txreset(1),       
        gt1_txuserrdy_in            => CLK_LOCKED_IN,              
        gt1_txdata_in               => gtx_txdata(1),              
        gt1_gtxtxn_out              => OPTO_KT1_N(1),              
        gt1_gtxtxp_out              => OPTO_KT1_P(1),              
        gt1_txoutclkfabric_out      => open,                       
        gt1_txoutclkpcs_out         => open,                       
        gt1_txcharisk_in            => gtx_txcharisk(1),           
        gt1_txresetdone_out         => open,
        GT0_QPLLOUTCLK_OUT          => open,
        GT0_QPLLOUTREFCLK_OUT       => open,
        sysclk_in                   => GCK_CLK80   );
 end generate DISABLE_GTX_WHEN_SIM;

------------------------------------------------------------------
-- gtx for slink vivado versoin
------------------------------------------------------------------

    
-------------------------------------------------------------------------------
-- ROD GTX -> Virt to Kint
-------------------------------------------------------------------------------
--    ROD_GTX_GLOBAL_BUF: BUFG port map(I => MGT2_CLK , O => mgt2_clk_bufg);
--    ISE_SYS: if VIVADO = FALSE generate
    
    ROD_GTX_INST: entity work.rod_gth_init
      generic map (
        EXAMPLE_SIMULATION => INT_SIM)
      port map (
        SYSCLK_IN                   => GCK_CLK80,
        SOFT_RESET_IN               => GTX_ROD_SOFT_RST(0),--rod_gtx_soft_reset,
        DONT_RESET_ON_DATA_ERROR_IN => '1',
        GT0_TX_FSM_RESET_DONE_OUT   => open,
        GT0_RX_FSM_RESET_DONE_OUT   => gtx_rx_fsm_reset_done(0),
        GT0_DATA_VALID_IN           => '1',
        GT1_TX_FSM_RESET_DONE_OUT   => open,
        GT1_RX_FSM_RESET_DONE_OUT   => gtx_rx_fsm_reset_done(1),
        GT1_DATA_VALID_IN           => '1',
        GT0_CPLLFBCLKLOST_OUT       => open,
        GT0_CPLLLOCK_OUT            => gtx_cplllock(0),
        GT0_CPLLLOCKDETCLK_IN       => GCK_CLK80,
        GT0_CPLLRESET_IN            => '0',
        GT0_GTREFCLK0_IN            => MGT2_CLK,                    
        GT0_DRPADDR_IN              => (others => '0'),
        GT0_DRPCLK_IN               => GCK_CLK80,      
        GT0_DRPDI_IN                => (others => '0'),
        GT0_DRPDO_OUT               => open,          
        GT0_DRPEN_IN                => '0',            
        GT0_DRPRDY_OUT              => open,          
        GT0_DRPWE_IN                => '0',                        
        GT0_RXUSERRDY_IN            => CLK_LOCKED_IN,
        GT0_EYESCANDATAERROR_OUT    => open,
        GT0_RXCDRLOCK_OUT           => open,
        GT0_RXUSRCLK_IN             => rod_gtx_rxusrclk_in(0),
        GT0_RXUSRCLK2_IN            => rod_gtx_rxusrclk_in(0),
        GT0_RXDATA_OUT              => rod_gtx_rxdata(0),
        GT0_RXDISPERR_OUT           => rod_gtx_rxdisperr(0),
        GT0_RXNOTINTABLE_OUT        => rod_gtx_rxnotintable(0),
        GT0_GTXRXP_IN               => ROD_GTX_RXP(0),
        GT0_GTXRXN_IN               => ROD_GTX_RXN(0),
        GT0_RXOUTCLK_OUT            => rod_gtx_rxoutclk(0),
        GT0_GTRXRESET_IN            => '0',
        GT0_RXPMARESET_IN           => '0',
        GT0_RXCHARISK_OUT           => rod_gtx_rxcharisk(0),
        GT0_RXRESETDONE_OUT         => rod_gtx_rxresetdone(0),
        GT0_GTTXRESET_IN            => GTX_ROD_SOFT_RST(1),
        GT1_CPLLFBCLKLOST_OUT       => open,
        GT1_CPLLLOCK_OUT            => gtx_cplllock(1),
        GT1_CPLLLOCKDETCLK_IN       => GCK_CLK80,
        GT1_CPLLRESET_IN            => '0',
        GT1_GTREFCLK0_IN            => MGT2_CLK,
        GT1_DRPADDR_IN              => (others => '0'),      
        GT1_DRPCLK_IN               => GCK_CLK80,            
        GT1_DRPDI_IN                => (others => '0'),      
        GT1_DRPDO_OUT               => open,                  
        GT1_DRPEN_IN                => '0',                  
        GT1_DRPRDY_OUT              => open,                  
        GT1_DRPWE_IN                => '0',                  
        GT1_RXUSERRDY_IN            => CLK_LOCKED_IN,        
        GT1_EYESCANDATAERROR_OUT    => open,                  
        GT1_RXCDRLOCK_OUT           => open,                  
        GT1_RXUSRCLK_IN             => rod_gtx_rxusrclk_in(1),  
        GT1_RXUSRCLK2_IN            => rod_gtx_rxusrclk_in(1),  
        GT1_RXDATA_OUT              => rod_gtx_rxdata(1),    
        GT1_RXDISPERR_OUT           => rod_gtx_rxdisperr(1),  
        GT1_RXNOTINTABLE_OUT        => rod_gtx_rxnotintable(1),
        GT1_GTXRXP_IN               => ROD_GTX_RXP(1),        
        GT1_GTXRXN_IN               => ROD_GTX_RXN(1),        
        GT1_RXOUTCLK_OUT            => rod_gtx_rxoutclk(1),      
        GT1_GTRXRESET_IN            => '0',                  
        GT1_RXPMARESET_IN           => '0',                  
        GT1_RXCHARISK_OUT           => rod_gtx_rxcharisk(1),  
        GT1_RXRESETDONE_OUT         => rod_gtx_rxresetdone(1),
        GT1_GTTXRESET_IN            => GTX_ROD_SOFT_RST(2),
 
        GT0_GTREFCLK0_COMMON_IN     => '0',
        GT0_QPLLLOCK_OUT            => open,
        GT0_QPLLLOCKDETCLK_IN       => GCK_CLK80,
        GT0_QPLLRESET_IN            => '0');

    
--    end generate ISE_SYS;
    --VV_SYS: if VIVADO = TRUE generate
    --  rod_gtx_vv_init_1: entity work.rod_gtx_vv_init
    --    generic map (
    --      EXAMPLE_SIMULATION          => int_sim)
          
    --    port map (
    --      SYSCLK_IN                   => GCK_CLK80,
    --      SOFT_RESET_RX_IN            => GTX_ROD_SOFT_RST,--rod_gtx_soft_reset,
    --      DONT_RESET_ON_DATA_ERROR_IN => '1',                         
    --      GT0_TX_FSM_RESET_DONE_OUT   => open,                        
    --      GT0_RX_FSM_RESET_DONE_OUT   => gtx_rx_fsm_reset_done(0),    
    --      GT0_DATA_VALID_IN           => '1',                         
    --      GT1_TX_FSM_RESET_DONE_OUT   => open,                        
    --      GT1_RX_FSM_RESET_DONE_OUT   => gtx_rx_fsm_reset_done(1),    
    --      GT1_DATA_VALID_IN           => '1',                         
    --      gt0_cpllfbclklost_out       => open,                        
    --      gt0_cplllock_out            => gtx_cplllock(0),             
    --      gt0_cplllockdetclk_in       => GCK_CLK80,                   
    --      gt0_cpllreset_in            => '0',                         
    --      gt0_gtrefclk0_in            => MGT2_CLK,                    
    --      gt0_gtrefclk1_in            => MGT2_CLK,  
    --      gt0_drpclk_in               => GCK_CLK80,
    --      gt0_eyescanreset_in         => '0',
    --      gt0_rxuserrdy_in            => CLK_LOCKED_IN,
    --      gt0_eyescandataerror_out    => open,
    --      gt0_eyescantrigger_in       => '0',
    --      gt0_dmonitorout_out         => open,
    --      gt0_rxusrclk_in             => rod_gtx_rxusrclk_in(0),
    --      gt0_rxusrclk2_in            => rod_gtx_rxusrclk_in(0),
    --      gt0_rxdata_out              => rod_gtx_rxdata(0),
    --      gt0_rxdisperr_out           => rod_gtx_rxdisperr(0),
    --      gt0_rxnotintable_out        => rod_gtx_rxnotintable(0),
    --      gt0_gthrxn_in               => ROD_GTX_RXN(0),
    --      gt0_rxmonitorout_out        => open,
    --      gt0_rxmonitorsel_in         => (others => '0'),
    --      gt0_rxoutclk_out            => rod_gtx_rxoutclk(0),
    --      gt0_gtrxreset_in            => '0',
    --      gt0_rxcharisk_out           => rod_gtx_rxcharisk(0),
    --      gt0_gthrxp_in               => ROD_GTX_RXP(0),
    --      gt0_rxresetdone_out         => rod_gtx_rxresetdone(0),
    --      gt0_gttxreset_in            => '0',                        
    --      gt1_cpllfbclklost_out       => open,                      
    --      gt1_cplllock_out            => gtx_cplllock(1),           
    --      gt1_cplllockdetclk_in       => GCK_CLK80,                 
    --      gt1_cpllreset_in            => '0',                       
    --      gt1_gtrefclk0_in            => MGT2_CLK,                  
    --      gt1_gtrefclk1_in            => MGT2_CLK,  
    --      gt1_drpclk_in               => GCK_CLK80,                 
    --      gt1_eyescanreset_in         => '0',                       
    --      gt1_rxuserrdy_in            => CLK_LOCKED_IN,             
    --      gt1_eyescandataerror_out    => open,                      
    --      gt1_eyescantrigger_in       => '0',                       
    --      gt1_dmonitorout_out         => open,                      
    --      gt1_rxusrclk_in             => rod_gtx_rxusrclk_in(1),    
    --      gt1_rxusrclk2_in            => rod_gtx_rxusrclk_in(1),    
    --      gt1_rxdata_out              => rod_gtx_rxdata(1),         
    --      gt1_rxdisperr_out           => rod_gtx_rxdisperr(1),      
    --      gt1_rxnotintable_out        => rod_gtx_rxnotintable(1),   
    --      gt1_gthrxn_in               => ROD_GTX_RXN(1),            
    --      gt1_rxmonitorout_out        => open,                      
    --      gt1_rxmonitorsel_in         => (others => '0'),                       
    --      gt1_rxoutclk_out            => rod_gtx_rxoutclk(1),       
    --      gt1_gtrxreset_in            => '0',                       
    --      gt1_rxcharisk_out           => rod_gtx_rxcharisk(1),      
    --      gt1_gthrxp_in               => ROD_GTX_RXP(1),            
    --      gt1_rxresetdone_out         => rod_gtx_rxresetdone(1),    
    --      gt1_gttxreset_in            => '0',                       
    --      GT0_QPLLOUTCLK_IN           => GCK_CLK80,
    --      GT0_QPLLOUTREFCLK_IN        => GCK_CLK80  );
    --end generate VV_SYS;

    GTX_LOGIC: for i in 0 to 1 generate
      
      GTX_RXRECCLK_BUFG : BUFG port map (I => rod_gtx_rxoutclk(i), O => rod_gtx_rxusrclk_in(i));
      
      --GTX_SAVE_CHAR_K: process (rod_gtx_rxusrclk_in(i))
      --begin 
      --  if rising_edge(rod_gtx_rxusrclk_in(i)) then  
      --    if rod_gtx_rxcharisk(i) /= "11" and rod_gtx_rxcharisk_sync(i) = "11" then
      --      rxcharisk_saved(i) <= rod_gtx_rxcharisk(i);
      --    else
      --      rxcharisk_saved(i) <= rxcharisk_saved(i);
      --    end if;
      --  end if;
      --end process GTX_SAVE_CHAR_K;

      SYNC_GTX_DATA: process (rod_gtx_rxusrclk_in(i))
      begin 
        if rising_edge(rod_gtx_rxusrclk_in(i)) then
          rod_gtx_rxcharisk_sync(i) <= rod_gtx_rxcharisk(i);
          rod_gtx_rxdata_sync(i) <= rod_gtx_rxdata(i);
        end if;
      end process SYNC_GTX_DATA;

      SELECT_GTX_DATA: process (rod_gtx_rxusrclk_in(i))
      begin
        if rising_edge(rod_gtx_rxusrclk_in(i)) then
          if ROD_GTX_DATA_EN = "11" and gtx_rx_fsm_reset_done(i) = '1' then
            gtx_data_selected(i) <= rod_gtx_rxdata(i)(15 downto 0);
            gtx_data_valid(i) <= not rod_gtx_rxcharisk(i)(0);
          else
            gtx_data_selected(i) <= rod_gtx_rxdata(i)(15 downto 0);
            gtx_data_valid(i) <= '0';
          end if;
        end if;
      end process SELECT_GTX_DATA;

      PIPE_GTX_DATA: process (rod_gtx_rxusrclk_in(i))
      begin
        if rising_edge(rod_gtx_rxusrclk_in(i)) then
          gtx_data_to_fifo_pipe(i) <= gtx_data_selected(i);
          gtx_data_valid_fifo_pipe(i) <= gtx_data_valid(i);
          gtx_data_to_fifo(i) <=    gtx_data_to_fifo_pipe(i);
          gtx_data_valid_fifo(i) <= gtx_data_valid_fifo_pipe(i);
        end if;
      end process PIPE_GTX_DATA;
--      GTX_READOUT_ILA_INST : entity work.gtx_readout_ila
--      port map (
--      clk => rod_gtx_rxusrclk_in(i),
--      probe0 => gtx_data_to_fifo(i),
--      probe1(0) => gtx_data_valid_fifo(i),
--      probe2 => rod_gtx_rxnotintable(i)
--      );

  MAKE_SEQ_FIFO_RESET_CNTR: process(rod_gtx_rxusrclk_in(i))
  begin
    if rising_edge(rod_gtx_rxusrclk_in(i)) then
      if ROD_RESET = '1' then 
         reset_fifos_cntr(i) <= (others => '0');
       elsif reset_fifos_cntr(i) < x"ffff" then
         reset_fifos_cntr(i) <= reset_fifos_cntr(i) + 1;
       else
         reset_fifos_cntr(i) <= reset_fifos_cntr(i);
      end if;
    end if;
  end process MAKE_SEQ_FIFO_RESET_CNTR;

  MAKE_SEQ_FIFO_RESET0_CNTR: process(rod_gtx_rxusrclk_in(i))
  begin
    if rising_edge(rod_gtx_rxusrclk_in(i)) then
      if reset_fifos_cntr(i) > (6000+i*1000) and reset_fifos_cntr(i) < (6200 + i*1000) then
        reset_fifos(i) <= '1';
      else
        reset_fifos(i) <= '0';
      end if;
    end if;
  end process MAKE_SEQ_FIFO_RESET0_CNTR;
  --new code for removing different ratio data
--   GTX16_TO_64BIT: process(rod_gtx_rxusrclk_in(i))
--   begin
--     if rising_edge(rod_gtx_rxusrclk_in(i)) then
--       if gtx_data_valid_fifo(i) then
--         gtx_data_cntr(i) <= gtx_data_cntr(i) + 1;
--         gtx_data_to_fifo_64bit(i) <= gtx_data_to_fifo_64bit(i)(47 downto 0) & gtx_data_to_fifo(i); 
--       else
--         gtx_data_to_fifo_64bit(i) <= gtx_data_to_fifo_64bit(i);
--       end if;
--     end if;
--   end process GTX16_TO_64BIT;
   
--   SAVE_DATA_TO_FIFO:process(rod_gtx_rxusrclk_in(i))
--   begin
--     if rising_edge(rod_gtx_rxusrclk_in(i)) then
--       if gtx_data_cntr(i) = 0 and gtx_data_cntr_synch(i) = 3 then
--           write_gtx_fifo(i) <= '1';
--       else
--           write_gtx_fifo(i) <= '0';
--       end if;
--     end if;
--   end process SAVE_DATA_TO_FIFO;    
   
--    ROD_GTX_FIFO_INST : entity work.gtx_built_in_fifo
--          PORT MAP (
--            rst => reset_fifos(i),--ROD_RESET,
--            wr_clk => rod_gtx_rxusrclk_in(i),
--            rd_clk => RODGCK_CLK80(i),
--            din => gtx_data_to_fifo_64bit(i),
--            wr_en => write_gtx_fifo(i),
--            rd_en => rod_gtx_fifo_rd(i),
--            dout => gtx_data_in(i),
--            full => rod_gtx_fifo_full(i),
--            prog_empty => open,
--            empty => rod_gtx_fifo_empty(i)
--            );
--      --end of new cod
      
      
      
      
      ROD_GTX_FIFO_INST : entity work.rod_gtx_fifo
        PORT MAP (
          rst => reset_fifos(i),--ROD_RESET,
          wr_clk => rod_gtx_rxusrclk_in(i),
          rd_clk => RODGCK_CLK80(i),
          din => gtx_data_to_fifo(i),
          wr_en => gtx_data_valid_fifo(i),
          rd_en => rod_gtx_fifo_rd(i),
          dout => gtx_data_in(i),
          full => rod_gtx_fifo_full(i),
          empty => rod_gtx_fifo_empty(i)
          );

      READ_GTX_FIFO_DATA: process (RODGCK_CLK80(i))
      begin
        if rising_edge(RODGCK_CLK80(i)) then
          if rod_gtx_fifo_empty(i) = '0' then
            rod_gtx_fifo_rd_sync(i) <= '1';
            rod_gtx_fifo_rd(i) <= rod_gtx_fifo_rd_sync(i);
            gtx_data_in_valid(i) <= rod_gtx_fifo_rd(i);
          else
            rod_gtx_fifo_rd_sync(i) <= '0';
            rod_gtx_fifo_rd(i) <= rod_gtx_fifo_rd_sync(i);  
            gtx_data_in_valid(i) <= rod_gtx_fifo_rd(i) and (not rod_gtx_fifo_empty(i));
          end if;
        end if;
      end process READ_GTX_FIFO_DATA;
      
--      GTX_FIFO_ILA2_INST : entity work.gtx_readout_ila
--      port map (
--      clk => RODGCK_CLK80(i),
--      probe0 => gtx_data_in(i)(15 downto 0),
--      probe1(0) => gtx_data_in_valid(i),
--      probe2(0) => rod_gtx_fifo_rd(i),
--      probe2(1) => rod_gtx_fifo_full(i)
--      );
      --err counters

      GT_ERR_CNTR_PROC: process(rod_gtx_rxusrclk_in(i))
      begin
        if rising_edge(rod_gtx_rxusrclk_in(i)) then
          if ROD_RESET = '1' then
            gtx_err_cntr(i) <= (others => '0');
          elsif (rod_gtx_rxdisperr(i) /= "00" or rod_gtx_rxnotintable(i) /= "00") and gtx_err_cntr(i) /= x"ff" then
            gtx_err_cntr(i) <=   gtx_err_cntr(i) + 1;
          else
            gtx_err_cntr(i) <= gtx_err_cntr(i);
          end if;
        end if;
      end process GT_ERR_CNTR_PROC;

      ROD_RW_REG_OUT(8+i)(7 downto 0) <= gtx_err_cntr(i);
      ROD_RW_REG_OUT(10+i)(4 downto 0) <= gtx_data_valid_fifo(i) & rod_gtx_fifo_full(i) & rod_gtx_fifo_empty(i) & gtx_rx_fsm_reset_done(i) & gtx_cplllock(i);
      
    end generate GTX_LOGIC;


      

end rtl;
