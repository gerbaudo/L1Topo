library IEEE;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library UNISIM;
use UNISIM.VCOMPONENTS.all;
use work.rod_l1_topo_types_const.all;

entity ddr_to_rod is
  generic (
    VIVADO               : boolean              := false;
    EN_DEBUG                : integer := 0;
    NUMBER_OF_PROCESSORS : integer range 1 to 2 := 1;
    PROCESSOR_NUMBER     : integer range 0 to 1 := 0
    );
  port (
    RESET                       : in  std_logic;
    DATA_IN_CLK                 : in  std_logic;
    DATA_OUT_CLK                : in  std_logic;
    --LVL1_ACCEPTED         : in  std_logic;
    --LVL1_VALID            : in  std_logic;
    FIFO_DATA_THR               : in  std_logic_vector(12 downto 0);
    LVL1_WR_DATA_CNTR           : out std_logic_vector(7 downto 0);
    L1_BUSY                     : out std_logic;
    DDR_ROS_ROI_IN_DATA         : in  std_logic_vector(63 downto 0);
    DATA_VALID_IN               : in  std_logic;
    OUT_DATA                    : out std_logic_vector(127 downto 0);--out_data_array;
    DATA_VALID_OUT              : out std_logic_vector(NUMBER_OF_OUTPUT_LINKS-1 downto 0);
    ROIB_INPUT_LINKS            : in  roib_input_links_numbers_array;
    MAX_OFFSET_IN               : in  std_logic_vector(3 downto 0);
    --Parametres of transmission
    ACTUAL_BUS_NUMBER_OUT       : out std_logic_vector(6 downto 0);--bus_number_array;          
    LVL0_OFFSET_OUT             : out std_logic_vector(3 downto 0);--offset_array;
    CRC_OUT                     : out std_logic;
    --READ_LINK             : in  std_logic_vector(NUMBER_OF_OUTPUT_LINKS-1 downto 0);
    ROS_ROI_BUS_ASSIGNMENT      : in  in_cntrl_array;
    START_OF_FRAME              : out std_logic;
    END_OF_FRAME                : out std_logic;
    TOKEN_OUT                   : out std_logic;
    TOKEN_IN                    : in std_logic;
    MUX_PROC                    : out std_logic;
    DEBUG                       : out std_logic_vector(255 downto 0);
    CNTR_DEBUG                  : out cntr_debug_array
    );
end ddr_to_rod;

architecture ddr_to_rod of ddr_to_rod is

  type SEND_DATA_FSM is (IDLE, READ_CONF_WAIT_A, READ_CONF_WAIT_B, READ_CONF, READ_CRC, SEND_DATA, SEND_DATA_NEXT_SLICE, SEND_DATA_NEXT_SLICE_WAIT, SEND_DATA_NEXT_SLICE_CHECK, WAIT_FOR_END_A, WAIT_FOR_END_B);
  signal SEND_DATA_FSM_CURRENT, SEND_DATA_FSM_NEXT : SEND_DATA_FSM;

  signal ddr_data_in_synch                                                : std_logic_vector(63 downto 0);
  signal data_valid_in_synch                                              : std_logic;
  signal data_rd_en, data_rd_en_fsm                                       : std_logic;
  signal fifo_data_thresh                                                 : std_logic_vector(12 downto 0);
  signal fifo_data_out, fifo_data_out_synch                               : std_logic_vector(127 downto 0);
  signal fifo_data_full, fifo_data_prog_full, fifo_data_empty             : std_logic;
  signal fifo_data_cntr                                                   : std_logic_vector(11 downto 0);
  signal data_valid_synch, new_event_pulse                                : std_logic;
  signal send_data_dbg, send_data_dbg_fsm                                 : std_logic_vector(3 downto 0);
  signal new_event_pulse_delayed                                          : std_logic;
  signal event_finished_synch, event_finished, event_finished_fsm         : std_logic;
  signal event_readout_pulse                                              : std_logic;
  signal event_cntr                                                       : unsigned(15 downto 0) := (others => '0');
  signal enable_data_valid, enable_data_valid_fsm                         : std_logic;
  signal wait_for_second_processor, token_in_saved                        : std_logic             := '0';
  signal number_of_conf_words_cntr, number_of_crc_words_cntr              : unsigned(3 downto 0)  := (others => '0');
  type active_links_array is array (0 to 6) of std_logic_vector(NUMBER_OF_ROS_ROI_INPUT_BUSES-1 downto 0);
  signal active_links_a, crc_saved_a                                      : active_links_array;
  signal active_link_scan_cntr                                            : unsigned(7 downto 0)  := (others => '0');
  signal active_link_scan_cntr_delayed_a, active_link_scan_cntr_delayed_b : unsigned(7 downto 0)  := (others => '0');
  signal actual_offset, actual_offset_a                                   : unsigned(3 downto 0);
  type saved_offset_array is array (0 to 6) of std_logic_vector(3 downto 0);
  signal saved_offset_a                                                   : saved_offset_array;
  signal start_event, start_event_fsm                                     : std_logic;
  signal data_valid_fast_synch                                            : std_logic;
  signal cntr_debug_l                                                     : cntr_debug_array;
  signal reset_fifos : std_logic_vector(2 downto 0) := (others => '0');
  signal reset_fifos_cntr: unsigned(15 downto 0) := (others => '0');
begin

ENABLE_DDR_TO_ROD_ILA: if EN_DEBUG = 1 generate  
  PROCS_TO_ROD_ILA_INST: entity work.procs_to_rod_ila
    port map (
      clk => DATA_OUT_CLK,
      probe0(0) => '0',
      probe1(0) => new_event_pulse,
      probe2(0) => event_finished,
      probe3(0) => '0',--event_readout_pulse,
      probe4 => std_logic_vector(event_cntr),
      probe5 => fifo_data_out_synch,
      probe6(0) => fifo_data_full,
      probe7(0) => fifo_data_empty,
      probe8(0) => fifo_data_prog_full,
      probe9(0) => data_rd_en,
      probe10(0) => event_finished,
      probe11(0) => start_event,
      probe12(0) => enable_data_valid,
      probe13 => std_logic_vector(number_of_conf_words_cntr),
      probe14 => std_logic_vector(number_of_crc_words_cntr),
      probe15 => std_logic_vector(active_link_scan_cntr_delayed_b),
      probe16 => std_logic_vector(actual_offset_a),
      probe17(0) => wait_for_second_processor,
      probe18(0) => token_in_saved);
end generate ENABLE_DDR_TO_ROD_ILA;
  
  MAKE_SEQ_FIFO_RESET_CNTR: process(DATA_IN_CLK)
  begin
    if rising_edge(DATA_IN_CLK) then
      if RESET = '1' then 
         reset_fifos_cntr <= (others => '0');
       elsif reset_fifos_cntr < x"ffff" then
         reset_fifos_cntr <= reset_fifos_cntr + 1;
       else
         reset_fifos_cntr <= reset_fifos_cntr;
      end if;
    end if;
  end process MAKE_SEQ_FIFO_RESET_CNTR;

  MAKE_SEQ_FIFO_RESET0_CNTR: process(DATA_IN_CLK)
  begin
    if rising_edge(DATA_IN_CLK) then
      if PROCESSOR_NUMBER = 0 and reset_fifos_cntr > 4000 and reset_fifos_cntr < 4015 then
        reset_fifos(0) <= '1';
      elsif PROCESSOR_NUMBER = 1 and reset_fifos_cntr > 5000 and reset_fifos_cntr < 5015 then
        reset_fifos(0) <= '1'; 
      else
        reset_fifos(0) <= '0';
      end if;
    end if;
  end process MAKE_SEQ_FIFO_RESET0_CNTR;
  
  -- saving data and  parameters of transmission from virtex
  SET_DATA_THRESHOLD : process (DATA_IN_CLK)
  begin
    if rising_edge(DATA_IN_CLK) then
      if unsigned(FIFO_DATA_THR) = 0 then
        fifo_data_thresh <= '1' & x"000";    --4096
      else
        fifo_data_thresh <= FIFO_DATA_THR;
      end if;
    end if;
  end process SET_DATA_THRESHOLD;

  SYNC_IN_DATA : process(DATA_IN_CLK)
  begin
    if rising_edge(DATA_IN_CLK) then
      ddr_data_in_synch <= DDR_ROS_ROI_IN_DATA;
      data_valid_synch <= DATA_VALID_IN;
    end if;
  end process SYNC_IN_DATA;

  MARK_INCOMING_EVENTS : process(DATA_OUT_CLK)
  begin
    if rising_edge(DATA_OUT_CLK) then
      if data_valid_fast_synch = '1' and DATA_VALID_IN = '0' then
        data_valid_fast_synch <= DATA_VALID_IN;
        new_event_pulse <= '1';
      else
        data_valid_fast_synch <= DATA_VALID_IN;
        new_event_pulse <= '0';
      end if;
    end if;
  end process MARK_INCOMING_EVENTS;
  
  DELAY_NEW_EVENT_PULSE: entity work.delay
    generic map (
      VECTOR_WIDTH => 1,
      DELAY_INT    => 4)
    port map (
      CLK              => DATA_OUT_CLK,
      DELAY_VECTOR_IN(0)  => new_event_pulse,
      DELAY_VECTOR_OUT(0) => new_event_pulse_delayed);
  
  --MARK_READOUT_EVENTS : process(DATA_OUT_CLK)
  --begin
  --  if rising_edge(DATA_OUT_CLK) then
  --    if event_finished_synch = '1' and event_finished = '0' then
  --      event_finished_synch <= event_finished;
  --      event_readout_pulse <= '1';
  --    else
  --      event_finished_synch <= event_finished;
  --      event_readout_pulse <= '0';
  --    end if;
  --  end if;
  --end process MARK_READOUT_EVENTS;

  
  COUNT_EVENTS:process(DATA_OUT_CLK)
  begin
    if rising_edge(DATA_OUT_CLK) then
      if RESET = '1' then
        event_cntr <= (others => '0');
      elsif new_event_pulse_delayed = '1' and event_finished = '0' then
        event_cntr <= event_cntr + 1;
      elsif new_event_pulse_delayed = '0' and event_finished = '1' then
        event_cntr <= event_cntr - 1;
      else
        event_cntr <= event_cntr;
      end if;
    end if;
  end process COUNT_EVENTS;

VIRTEX_TO_KINTEX_FIFO_INST: entity work.virtex_to_kintex_fifo --2kWords-7.5bigRAMS,FirstWordFallThrough
  port map (
    rst              => reset_fifos(0),--RESET,
    wr_clk           => DATA_IN_CLK,
    rd_clk           => DATA_OUT_CLK,
    din              => ddr_data_in_synch,--DDR_ROS_ROI_IN_DATA
    wr_en            => data_valid_synch,--DATA_VALID_IN,
    rd_en            => data_rd_en,
    prog_full_thresh => fifo_data_thresh,
    dout             => fifo_data_out,
    full             => fifo_data_full,
    empty            => fifo_data_empty,
    rd_data_count    => fifo_data_cntr,
    prog_full        => fifo_data_prog_full);
  
  L1_BUSY <= fifo_data_prog_full;

  --sending data to parsers
  SEND_DATA_FSM_CLK : process (DATA_OUT_CLK, RESET)
  begin
    if rising_edge(DATA_OUT_CLK) then
      if RESET = '1' then
        SEND_DATA_FSM_CURRENT <= IDLE;
        event_finished   <= '0';
        data_rd_en       <= '0';
        start_event      <= '0';
        enable_data_valid <= '0';
        send_data_dbg     <= (others => '0');
      else
        SEND_DATA_FSM_CURRENT <= SEND_DATA_FSM_NEXT;
        event_finished   <= event_finished_fsm;
        data_rd_en       <= data_rd_en_fsm;
        start_event      <= start_event_fsm;
        enable_data_valid <= enable_data_valid_fsm;
        send_data_dbg    <= send_data_dbg_fsm;
      end if;
    end if;
  end process SEND_DATA_FSM_CLK;

  SEND_DATA_FSM_PROC : process (all)
  begin
    event_finished_fsm     <= '0';
    data_rd_en_fsm         <= '0';
    start_event_fsm        <= '0';
    enable_data_valid_fsm  <= '0';
    send_data_dbg_fsm      <= (others => '0');
    case (SEND_DATA_FSM_CURRENT) is
      when IDLE =>
        send_data_dbg_fsm <= x"1";
        if (PROCESSOR_NUMBER = 0 and event_cntr /= 0 and wait_for_second_processor = '0') or
           (PROCESSOR_NUMBER = 1 and event_cntr /= 0 and token_in_saved = '1') then--l1_trigger_value_out(31) = '1' then
          SEND_DATA_FSM_NEXT <= READ_CONF_WAIT_A;
        else
          SEND_DATA_FSM_NEXT <= IDLE;
        end if;
      when READ_CONF_WAIT_A =>
        send_data_dbg_fsm <= x"2";
        --if event_cntr = '1' then
        --  data_rd_en_fsm         <= '1';
        --else
        --  data_rd_en_fsm         <= '0';
        --end if;
        SEND_DATA_FSM_NEXT <= READ_CONF_WAIT_B;
      when READ_CONF_WAIT_B =>
        send_data_dbg_fsm <= x"3";
        data_rd_en_fsm         <= '0';
        SEND_DATA_FSM_NEXT <= READ_CONF;
      when READ_CONF =>
        send_data_dbg_fsm <= x"4";
        if fifo_data_out(127) = '0' then
          SEND_DATA_FSM_NEXT <= READ_CRC;
        else
          data_rd_en_fsm     <= '1';
          SEND_DATA_FSM_NEXT <= READ_CONF_WAIT_A;
        end if;
      when READ_CRC =>
        send_data_dbg_fsm <= x"5";
        data_rd_en_fsm         <= '1';
        if number_of_crc_words_cntr + 1 >= number_of_conf_words_cntr then
           start_event_fsm        <= '1';
          SEND_DATA_FSM_NEXT <= SEND_DATA;
        else
          SEND_DATA_FSM_NEXT <= READ_CRC;
        end if;
      when SEND_DATA =>
        enable_data_valid_fsm <= '1';
        if active_links_a(to_integer(actual_offset))(to_integer(active_link_scan_cntr)) = '1' then
          data_rd_en_fsm <= '1';
        else
          data_rd_en_fsm <= '0';
        end if;
        if active_link_scan_cntr = NUMBER_OF_ROS_ROI_INPUT_BUSES - 1 then
          SEND_DATA_FSM_NEXT <= SEND_DATA_NEXT_SLICE;
        else
          SEND_DATA_FSM_NEXT <= SEND_DATA;
        end if;
      when SEND_DATA_NEXT_SLICE =>
        if actual_offset < 5 then
          SEND_DATA_FSM_NEXT <= SEND_DATA_NEXT_SLICE_WAIT;
        else
          event_finished_fsm <= '1';
          SEND_DATA_FSM_NEXT <= WAIT_FOR_END_A;
        end if;
      when SEND_DATA_NEXT_SLICE_WAIT =>
        SEND_DATA_FSM_NEXT <= SEND_DATA_NEXT_SLICE_CHECK;
      when SEND_DATA_NEXT_SLICE_CHECK =>
        if unsigned(active_links_a(to_integer(actual_offset))) /= 0 then
          SEND_DATA_FSM_NEXT <=  SEND_DATA;
        else
          SEND_DATA_FSM_NEXT <=  SEND_DATA_NEXT_SLICE;
        end if;
      when WAIT_FOR_END_A =>
        SEND_DATA_FSM_NEXT <= WAIT_FOR_END_B;
      when WAIT_FOR_END_B =>
        SEND_DATA_FSM_NEXT <= IDLE;
      when others =>
        SEND_DATA_FSM_NEXT <= IDLE;
    end case;
  end process SEND_DATA_FSM_PROC;
  
  CALC_AND_SAVE_CONF: process(DATA_OUT_CLK)
  begin
    if rising_edge(DATA_OUT_CLK) then
      if SEND_DATA_FSM_CURRENT = IDLE then
        number_of_conf_words_cntr <= (others => '0');
        active_links_a <= (others => (others => '0'));
        saved_offset_a <= (others => (others => '0'));
      elsif SEND_DATA_FSM_CURRENT = READ_CONF and fifo_data_out(127) = '1' and number_of_conf_words_cntr < 6 then
        active_links_a(to_integer(number_of_conf_words_cntr)) <= fifo_data_out(active_links_a(0)'range);
        number_of_conf_words_cntr <= number_of_conf_words_cntr + 1;
        saved_offset_a(to_integer(number_of_conf_words_cntr)) <= fifo_data_out(123 downto 120);
      else
        active_links_a <= active_links_a;
        number_of_conf_words_cntr <= number_of_conf_words_cntr;
        saved_offset_a <= saved_offset_a;
      end if;
    end if;
  end process CALC_AND_SAVE_CONF;

  CALC_AND_SAVE_CRC: process(DATA_OUT_CLK)
  begin
    if rising_edge(DATA_OUT_CLK) then
      if SEND_DATA_FSM_CURRENT = IDLE then
        number_of_crc_words_cntr <= (others => '0');
        crc_saved_a <= (others => (others => '0'));
      elsif SEND_DATA_FSM_CURRENT = READ_CRC then
        number_of_crc_words_cntr <= number_of_crc_words_cntr + 1;
        crc_saved_a(to_integer(number_of_crc_words_cntr)) <= fifo_data_out(crc_saved_a(0)'range);
      else
        crc_saved_a <= crc_saved_a;
      end if;
    end if;
  end process CALC_AND_SAVE_CRC;  

  ACTIVE_LINK_SCAN_CNTR_PROC : process (DATA_OUT_CLK)
  begin
    if rising_edge(DATA_OUT_CLK) then
      if SEND_DATA_FSM_CURRENT = SEND_DATA and active_link_scan_cntr < NUMBER_OF_ROS_ROI_INPUT_BUSES - 1 then
       active_link_scan_cntr <= active_link_scan_cntr + 1;
      else
       active_link_scan_cntr <= (others => '0');
      end if;
    end if;
  end process ACTIVE_LINK_SCAN_CNTR_PROC;
  
  ACTUAL_OFFSET_PROC : process (DATA_OUT_CLK)
  begin
    if rising_edge(DATA_OUT_CLK) then
      if SEND_DATA_FSM_CURRENT = IDLE then
        actual_offset <= (others => '0');
        actual_offset_a <= (others => '0');
      elsif SEND_DATA_FSM_CURRENT = SEND_DATA_NEXT_SLICE then
        actual_offset <= actual_offset + 1;
        actual_offset_a <= actual_offset;
      else
        actual_offset <= actual_offset;
        actual_offset_a <= actual_offset;
      end if;
    end if;
  end process ACTUAL_OFFSET_PROC;
  
  SEND_DATA_OFFSET_AND_LINK_NUMBER: for i in 0 to NUMBER_OF_OUTPUT_LINKS-1 generate
    ROS_ONLY : if i /= 1 and  i /= 2 generate
      SET_DATA_VALID: process(DATA_OUT_CLK)
      begin
        if rising_edge(DATA_OUT_CLK) then
          if ROS_ROI_BUS_ASSIGNMENT(to_integer(active_link_scan_cntr_delayed_a)) = i then
            active_link_scan_cntr_delayed_a <= active_link_scan_cntr;
            active_link_scan_cntr_delayed_b <= active_link_scan_cntr_delayed_a;
            DATA_VALID_OUT(i) <= data_rd_en and enable_data_valid;
            fifo_data_out_synch <= fifo_data_out;
          else
            active_link_scan_cntr_delayed_a <= active_link_scan_cntr;
            active_link_scan_cntr_delayed_b <= active_link_scan_cntr_delayed_a;
            DATA_VALID_OUT(i) <= '0';
            fifo_data_out_synch <= fifo_data_out;
          end if;
        end if;
      end process SET_DATA_VALID;
    end generate ROS_ONLY;
    ROIB_ONLY: if i = 1 or i = 2 generate
      SEND_DATA_VALID_ROIB_PROC : process (DATA_OUT_CLK)
      begin
        if rising_edge(DATA_OUT_CLK) then
          if (active_link_scan_cntr_delayed_a = unsigned(ROIB_INPUT_LINKS(0)) or
             active_link_scan_cntr_delayed_a = unsigned(ROIB_INPUT_LINKS(1)) or
             active_link_scan_cntr_delayed_a = unsigned(ROIB_INPUT_LINKS(2)) or
             active_link_scan_cntr_delayed_a = unsigned(ROIB_INPUT_LINKS(3)))
            and std_logic_vector(actual_offset_a(3 downto 0)) = MAX_OFFSET_IN then  
            DATA_VALID_OUT(i)         <= data_rd_en  and enable_data_valid;
          else
            DATA_VALID_OUT(i)         <= '0';
          end if;
        end if;
      end process SEND_DATA_VALID_ROIB_PROC;
    end generate ROIB_ONLY;
  end generate   SEND_DATA_OFFSET_AND_LINK_NUMBER;
  OUT_DATA <= fifo_data_out_synch;
  ACTUAL_BUS_NUMBER_OUT <= std_logic_vector(active_link_scan_cntr_delayed_b(6 downto 0));
  LVL0_OFFSET_OUT <= std_logic_vector(actual_offset_a(3 downto 0));
  CRC_OUT <= crc_saved_a(to_integer(actual_offset_a))(to_integer(active_link_scan_cntr_delayed_b));

  process (DATA_OUT_CLK) is
  begin  -- process
    if rising_edge(DATA_OUT_CLK) then 
      if PROCESSOR_NUMBER = 0 and NUMBER_OF_PROCESSORS = 2 then
        START_OF_FRAME <= start_event;
        END_OF_FRAME   <= '0';
        TOKEN_OUT <=  event_finished;
      elsif PROCESSOR_NUMBER = 0 and NUMBER_OF_PROCESSORS = 1 then
        START_OF_FRAME <= start_event;
        END_OF_FRAME   <= event_finished;
        TOKEN_OUT <=  event_finished;
      elsif PROCESSOR_NUMBER = 1 and NUMBER_OF_PROCESSORS = 2 then
        START_OF_FRAME <= '0';
        END_OF_FRAME   <= event_finished;
        TOKEN_OUT <=  event_finished;
      else
        START_OF_FRAME <= start_event;
        END_OF_FRAME   <= event_finished;
        TOKEN_OUT <=  event_finished;
      end if;
    end if;
  end process;

  

  WAIT_FOR_SECOND_ROD : process (DATA_OUT_CLK)
  begin
    if  rising_edge(DATA_OUT_CLK) then
      if event_finished = '1' and PROCESSOR_NUMBER = 0 and NUMBER_OF_PROCESSORS = 2  then
        wait_for_second_processor <= '1';
      elsif TOKEN_IN = '1' then
        wait_for_second_processor <= '0';
      else
        wait_for_second_processor <= wait_for_second_processor;
      end if;   
    end if;
  end process WAIT_FOR_SECOND_ROD;

  MUX_PROC <= wait_for_second_processor;

  WAIT_FOR_FIRST_ROD : process (DATA_OUT_CLK)
  begin
    if rising_edge(DATA_OUT_CLK) then
      if TOKEN_IN = '1' and PROCESSOR_NUMBER = 1 then
        token_in_saved <= '1';
      elsif event_finished = '1' and PROCESSOR_NUMBER = 1 then
        token_in_saved <= '0';
      else
        token_in_saved <= token_in_saved;
      end if;
    end if;
  end process WAIT_FOR_FIRST_ROD;
  
  -----------------------------------------------------------------------------
  -- counters for debug
  -----------------------------------------------------------------------------
  COUNT_RECIVED: process (DATA_OUT_CLK)
  begin
    if rising_edge(DATA_OUT_CLK) then
      if RESET = '1' then
        cntr_debug_l(0) <= (others => '0');
      elsif new_event_pulse = '1'  then
        cntr_debug_l(0) <= cntr_debug_l(0) + 1;
      else
        cntr_debug_l(0) <= cntr_debug_l(0);
      end if;
    end if;
  end process COUNT_RECIVED;

  COUNT_SENT: process (DATA_OUT_CLK)
  begin
    if rising_edge(DATA_OUT_CLK) then
      if RESET = '1' then
        cntr_debug_l(1) <= (others => '0');
      elsif event_finished = '1'  then
        cntr_debug_l(1) <= cntr_debug_l(1) + 1;
      else
        cntr_debug_l(1) <= cntr_debug_l(1);
      end if;
    end if;
  end process COUNT_SENT;
  
  PIPE_CNTRS: process(DATA_OUT_CLK)
  begin
    if rising_edge(DATA_OUT_CLK) then
      CNTR_DEBUG(0) <= cntr_debug_l(0);
      CNTR_DEBUG(1) <= cntr_debug_l(1);
      CNTR_DEBUG(2)(11 downto 0) <= unsigned(fifo_data_cntr);
    end if;
  end process PIPE_CNTRS;

end ddr_to_rod;
