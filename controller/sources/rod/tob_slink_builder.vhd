library IEEE;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.rod_l1_topo_types_const.all;
library UNISIM;
use UNISIM.VComponents.all;

entity tob_slink_builder is
  generic (
    VIVADO : boolean := false;
    DEBUG : integer := 0;
    NUMBER_OF_PROCESSORS : integer := 1
    
    );
  port (
    RESET                      : in  std_logic;
    CLK                        : in  std_logic;
    --TOB data and configuration
    TOB_DATA_IN                : in  std_logic_vector(127 downto 0) := (others => '0'); --
    --data from input FIFOs already arranged as on the input Virtex
    LINK_NUMBER                : in  std_logic_vector(6 downto 0);
    BCID_OFFSET_IN             : in  std_logic_vector(3 downto 0);
    TOTAL_NR_OF_SLICES         : in  std_logic_vector(3 downto 0);
    MAX_OFFSET_IN              : in  std_logic_vector(3 downto 0);
    CRC_IN                     : in  std_logic;
    ENABLE_ALL_CMX_DATA        : in  std_logic;
    CRATE_ASSIGNMENT           : in  crate_number_array;
    ROS_SELECTED               : in  std_logic;
    --based on this different crate number is put into the data
    DATA_VALID_IN              : in  std_logic;
    BEGINNING_OF_DATA          : in  std_logic;
    END_OF_DATA                : in  std_logic;
--    SLINK_PACKET_BUSY          : in  std_logic;
    ACTIVE_LINKS_NUMBER        : in  std_logic_vector(log2_int(NUMBER_OF_ROS_ROI_INPUT_BUSES) downto 0);
    TOB_BUILDER_FIFO_THR       : in  std_logic_vector(13 downto 0);
    FIBER_CNTR_FIFO_THR        : in  std_logic_vector(10 downto 0);
    SLINK_FIFO_32kW_THR        : in  std_logic_vector(14 downto 0);
    BUILDER_BUSY               : out std_logic;
    FPGA_NUMBER                : in  std_logic;
    --from/to slink wrapper
    SLINK_CLK                  : in  std_logic;
    SLINK_DATA_OUT             : out std_logic_vector(31 downto 0);
    SLINK_READY_IN             : in  std_logic;
    SLINK_EVENT_READY_OUT      : out std_logic;
    BUSY_DEBUG                 : out std_logic_vector(2 downto 0);
--    DEBUG                      : out std_logic_vector(255 downto 0);
--    DEBUG2                     : out std_logic_vector(255 downto 0);
    CNTR_DEBUG                 : out cntr_debug_array
    );
end tob_slink_builder;

architecture tob_slink_builder of tob_slink_builder is

  signal builder_fifo_data_in, builder_fifo_data_out : std_logic_vector(143 downto 0) := (others => '0');
  alias end_data                                     : std_logic is builder_fifo_data_out(143);
  alias fpga_nr                                      : std_logic is builder_fifo_data_out(140);
  alias crc                                          : std_logic is builder_fifo_data_out(139);
  alias offset                                       : std_logic_vector(3 downto 0) is builder_fifo_data_out(138 downto 135);
  alias link_nr                                      : std_logic_vector(6 downto 0) is builder_fifo_data_out(134 downto 128);
  alias tob_data                                     : std_logic_vector(127 downto 0) is builder_fifo_data_out(127 downto 0);
  signal offset_saved                                           : std_logic_vector(3 downto 0):=x"0";
  signal fpga_nr_synch                                          : std_logic :='0';
  signal second_fpga_pulse, fpga_number_synch, fpga_nr_saved    : std_logic :='0';
  signal builder_debug, builder_debug_fsm                       : std_logic_vector(3 downto 0);
  signal builder_fifo_wr, builder_fifo_rd, builder_fifo_empty   : std_logic;
  signal builder_fifo_full, builder_fifo_prog_full              : std_logic;
  signal builder_fifo_rd_fsm                                    : std_logic;
  signal builder_fifo_thr, builder_fifo_cntr                    : std_logic_vector(13 downto 0);
  signal crc_synch                                              : std_logic;
  type fiber_cntr_array is array (0 to NUMBER_OF_ROS_ROI_INPUT_BUSES-1 + 4) of std_logic_vector(4 downto 0);
  type fiber_cntr_array3d is array (0 to 6*NUMBER_OF_PROCESSORS) of fiber_cntr_array;
  signal fiber_cntr_a                                           : fiber_cntr_array3d:= (others => (others => (others => '0')));
  signal if_tob : std_logic_vector(NUMBER_OF_ROS_ROI_INPUT_BUSES downto 0) := (others => '0');
  signal next_active_link, next_out_active_link : unsigned(6 downto 0) := (others => '0');
  type conseq_active_links_array is array (NUMBER_OF_ROS_ROI_INPUT_BUSES downto 0) of std_logic_vector(6 downto 0);
  type conseq_active_links_array3d is array (1 downto 0) of conseq_active_links_array;
  signal conseq_active_links_a, conseq_active_links_a_synch : conseq_active_links_array3d := (others => (others => (others => '0')));
  
  signal there_is_tob, there_is_tob_synch : std_logic := '0';
  signal crate_number_l                                         : std_logic_vector(1 downto 0);
  signal link_number_synch_a, link_number_synch_b, link_number_synch_c : std_logic_vector(6 downto 0);
  signal select_fiber_cntr, select_fiber_cntr_synch             : integer range 0 to 8 :=0;
  signal bcid_offset_synch                                      : std_logic_vector(3 downto 0);
  signal data_valid_synch_a, data_valid_synch_b, data_valid_synch_c : std_logic;
  type fiber_cntr_data_in_array is array (0 to 6*NUMBER_OF_PROCESSORS) of std_logic_vector(431 downto 0);
  signal fiber_cntr_data_in_a, fiber_cntr_data_in_saved_a       : fiber_cntr_data_in_array := (others => (others => '0'));
  signal fiber_cntr_data_in, fiber_cntr_data_out                : std_logic_vector(431 downto 0)                     := (others => '0');
  signal fiber_cntr_prog_full, fiber_cntr_rd, fiber_cntr_rd_fsm : std_logic;
  signal fiber_cntr_wr, fiber_cntr_full, fiber_cntr_empty       : std_logic;
  signal fiber_cntr_cntr, fiber_cntr_thr                        : std_logic_vector(10 downto 0);
  signal tob_data_out                                           : std_logic_vector((MAX_TOB_NUMBER+1)*32-1 downto 0) := (others => '0');
  signal tob_type_l, tob_type_l_from_input : std_logic_vector(3 downto 0)      := x"4";
  signal tob_cntr_out_l                    : std_logic_vector(3 downto 0);
  signal tob_conseq_nrs_l                  : std_logic_vector(4*11-1 downto 0) := (others => '0');
  signal tob_conseq_nrs_l_part             : std_logic_vector(3 downto 0)      := (others => '0');
  signal tob_conseq_pntr                   : unsigned(3 downto 0) := (others => '0');--integer range 0 to 9              := 0;
  signal offset_cntr                       : unsigned(3 downto 0)              := (others => '0');
  signal word_fiber_cntr                   : integer range 0 to 31             := 0;
  signal wait_cntr                         : unsigned(2 downto 0);

  type SLINK_BUILDER is (IDLE, WAIT_FOR_BLOCK_HEADER, BLOCK_HEADER, WAIT_FOR_FIFO_DATA, DATA_WRITE, FIBER_CNTRS, FINISHED, WAIT_FOR_SLINK);
  signal SLINK_BUILDER_CURRENT, SLINK_BUILDER_NEXT                                      : SLINK_BUILDER;
  
  --slink
  signal slink_data_in, slink_data_in_fsm, slink_data_out_l       : std_logic_vector(32 downto 0);
  signal slink_fifo_rd_en, slink_fifo_wr_en, slink_fifo_wr_en_fsm : std_logic;
  signal slink_fifo_thr, slink_fifo_cntr                          : std_logic_vector(14 downto 0);
  signal slink_fifo_empty, slink_fifo_full, slink_fifo_prog_full  : std_logic;
  signal events_in_slink_buffer_cntr                              : unsigned(14 downto 0) := (others => '0');
  signal slink_readout_pulse, slink_valid_synch                   : std_logic;
  signal slink_fifo_rd_en_pulse, slink_fifo_rd_en_pulse_fsm       : std_logic;
  signal slink_debug, slink_debug_fsm                             : std_logic_vector(1 downto 0);
  
  type SLINK_DATA_SEND is (IDLE, WAIT_FOR_DATA, DATA_SEND, FINISHED);
  signal SLINK_DATA_SEND_CURRENT, SLINK_DATA_SEND_NEXT : SLINK_DATA_SEND;
  signal start_of_building, start_of_building_fsm : std_logic;
  signal cntr_debug_l                                                     : cntr_debug_array;
  signal reset_fifos : std_logic_vector(2 downto 0) := (others => '0');
  signal reset_fifos_cntr: unsigned(15 downto 0) := (others => '0');
   
begin
   
  ENABLE_TOB_BUILDER_ILA: if debug = 1 generate
    TOB_BUILDER_ILA_INST: entity work.tob_builder_ila 
      port map  (
        clk => CLK,
        probe0 => builder_fifo_data_in, --144bits
        probe1 => tob_cntr_out_l, --4bits 
        probe2 => std_logic_vector(to_unsigned(select_fiber_cntr, 4)), --4bits
        probe3(0) => fiber_cntr_wr,
        probe4(0) => fiber_cntr_data_in(431),
        probe5(0) => END_OF_DATA,
        probe6(0) => fiber_cntr_rd,
        probe7 => std_logic_vector(offset_cntr), --4bits
        probe8(0) => fiber_cntr_full,
        probe9(0) => fiber_cntr_empty,
        probe10(0) => fiber_cntr_prog_full,
        probe11(0) => end_data,
        probe12 => tob_conseq_nrs_l_part, --4bits
        probe13(0) => builder_fifo_rd,
        probe14(0) => slink_fifo_wr_en,
        probe15(0) => fpga_nr,
        probe16(0) => builder_fifo_wr,
        probe17 => offset, --4bits
        probe18 => slink_data_in(31 downto 0), --32bits
        probe19 => builder_debug, --4bits
        probe20 => std_logic_vector(tob_conseq_pntr), --4bits
        probe21 => std_logic_vector(to_unsigned(word_fiber_cntr,4)),--4bits
        probe22 => std_logic_vector(wait_cntr(2 downto 1)),--2bits
        probe23 => std_logic_vector(events_in_slink_buffer_cntr),--15bits
        probe24(0) => slink_data_in(32),
        probe25(0) => slink_fifo_rd_en,
        probe26(0) => slink_fifo_rd_en_pulse,
        probe27(0) => SLINK_READY_IN,
        probe28(0) => slink_data_out_l(32),
        probe29 => slink_data_out_l(31 downto 24),--8bits
        probe30 => slink_debug,--2bits
        probe31 => builder_fifo_data_out,--144bits
        probe32 => tob_type_l,--4bits
        probe33 => tob_conseq_nrs_l);--44bits
  end generate ENABLE_TOB_BUILDER_ILA;
  
  MAKE_SEQ_FIFO_RESET_CNTR: process(CLK)
  begin
    if rising_edge(CLK) then
       if RESET = '1' then 
          reset_fifos_cntr <= (others => '0');
        elsif reset_fifos_cntr < x"ffff" then
          reset_fifos_cntr <= reset_fifos_cntr + 1;
        else
          reset_fifos_cntr <= reset_fifos_cntr;
       end if;
    end if;
  end process MAKE_SEQ_FIFO_RESET_CNTR;
  
  MAKE_SEQ_FIFO_RESET0_CNTR: process(CLK)
    begin
      if rising_edge(CLK) then
        if reset_fifos_cntr > 1000 and reset_fifos_cntr < 1015 then
           reset_fifos(0) <= '1';
        else
           reset_fifos(0) <= '0';
        end if;
     end if;
  end process MAKE_SEQ_FIFO_RESET0_CNTR;
            
  MAKE_SEQ_FIFO_RESET1_CNTR: process(CLK)
    begin
      if rising_edge(CLK) then
        if reset_fifos_cntr > 2000 and reset_fifos_cntr < 2015 then
           reset_fifos(1) <= '1';
        else
           reset_fifos(1) <= '0';
        end if;
     end if;
  end process MAKE_SEQ_FIFO_RESET1_CNTR;
  
  MAKE_SEQ_FIFO_RESET2_CNTR: process(CLK)
    begin
      if rising_edge(CLK) then
        if reset_fifos_cntr > 3000 and reset_fifos_cntr < 3015 then
           reset_fifos(2) <= '1';
        else
           reset_fifos(2) <= '0';
        end if;
     end if;
  end process MAKE_SEQ_FIFO_RESET2_CNTR;
  
  SYNCH_DATA_IN: process(CLK)
  begin
    if rising_edge(CLK) then
      builder_fifo_data_in(143) <= END_OF_DATA;
      builder_fifo_data_in(140) <= FPGA_NUMBER;
      builder_fifo_data_in(139) <= CRC_IN;
      builder_fifo_data_in(138 downto 135) <= BCID_OFFSET_IN;
      builder_fifo_data_in(134 downto 128) <= LINK_NUMBER;
      builder_fifo_data_in(127 downto 0)   <= TOB_DATA_IN;
      builder_fifo_wr <= DATA_VALID_IN or END_OF_DATA;
    end if;
  end process SYNCH_DATA_IN;
  
  TOB_BUILDER_FIFO_INST: entity work.tob_builder_fifo
      port map (
        clk              => CLK,
        srst             => reset_fifos(0),--RESET,
        din              => builder_fifo_data_in,
        wr_en            => builder_fifo_wr,
        rd_en            => builder_fifo_rd,
        prog_full_thresh => builder_fifo_thr,
        dout             => builder_fifo_data_out,
        full             => builder_fifo_full,
        empty            => builder_fifo_empty,
        data_count       => builder_fifo_cntr,
        prog_full        => builder_fifo_prog_full);

  SET_BUILDER_THRESHOLD : process (CLK)
  begin
    if rising_edge(CLK) then
      if unsigned(TOB_BUILDER_FIFO_THR) = 0 then
        builder_fifo_thr <= "01" & x"9c0";
      else
        builder_fifo_thr <= TOB_BUILDER_FIFO_THR; 
      end if;
    end if;
  end process SET_BUILDER_THRESHOLD;

  tob_type_l_from_input <= TOB_MAP(0)(std_int(LINK_NUMBER)) when FPGA_NUMBER = '0' else TOB_MAP(1)(std_int(LINK_NUMBER));
  TOB_BCID_CNTR_INST: entity work.tob_bcid_cntr
    --depending on TOB type it calculates ToBs amount
    port map (
      CLK            => CLK,
      TOB_DATA_IN    => TOB_DATA_IN,
      TOB_TYPE       => tob_type_l_from_input,
      TOB_CNTR_OUT   => tob_cntr_out_l,
      CRC_ERR_IN     => '0',
      EN_ALL_DATA    => ENABLE_ALL_CMX_DATA,
      SEND_DATA_ON_CRC_ONLY  => '0',
      ROS_SELECTED   => ROS_SELECTED,
      TOB_CONSEQ_NRS => open--tob_conseq_nrs
      );

  SYNC:process(CLK)
  begin
    if rising_edge(CLK) then
      link_number_synch_a <= LINK_NUMBER;
      link_number_synch_b <= link_number_synch_a;
      link_number_synch_c <= link_number_synch_b;
      data_valid_synch_a  <= DATA_VALID_IN;
      data_valid_synch_b  <= data_valid_synch_a;
      data_valid_synch_c  <= data_valid_synch_b;
      --bcid_offset_synch   <= BCID_OFFSET_IN;
      crc_synch           <= CRC_IN;
    end if;
  end process SYNC;

  SECOND_FPGA_PULSE_PROC: process (CLK)
  begin  
    if rising_edge(CLK) then
      if FPGA_NUMBER = '1' and fpga_number_synch = '0' then
        second_fpga_pulse <= '1';
        fpga_number_synch <= FPGA_NUMBER;
      else
        second_fpga_pulse <= '0';
        fpga_number_synch <= FPGA_NUMBER;
      end if;
    end if;
  end process SECOND_FPGA_PULSE_PROC;

  DETECT_NEXT_OFFSET: process (CLK)
  begin
    if rising_edge(CLK) then
      if BEGINNING_OF_DATA = '1' then 
        bcid_offset_synch <= x"f";
      elsif DATA_VALID_IN = '1' then
        bcid_offset_synch <= BCID_OFFSET_IN;
      else
        bcid_offset_synch <= bcid_offset_synch;
      end if;
    end if;
  end process DETECT_NEXT_OFFSET;
  
  SELECT_FIBER_WORD: process(CLK)
  begin
    if rising_edge(CLK) then
      if BEGINNING_OF_DATA = '1' then
        select_fiber_cntr <= 0;
        select_fiber_cntr_synch <= 0;
      elsif ((bcid_offset_synch /= BCID_OFFSET_IN and bcid_offset_synch /= x"f" and DATA_VALID_IN = '1') or (second_fpga_pulse = '1' and TOTAL_NR_OF_SLICES < x"2")) and select_fiber_cntr < 6*NUMBER_OF_PROCESSORS then
        --§§T or second_fpga_pulse = '1' ... < 12 - to write fiber headers
        --after first processor
        select_fiber_cntr <= select_fiber_cntr + 1;
        select_fiber_cntr_synch <= select_fiber_cntr;
      else
        select_fiber_cntr <= select_fiber_cntr;
        select_fiber_cntr_synch <= select_fiber_cntr;
      end if;
    end if;
  end process SELECT_FIBER_WORD;

  SAVE_LOOP_OVER_OFFSETS: for j in 0 to 5*NUMBER_OF_PROCESSORS generate  --§§T 0 to 10
    SAVE_NUMBER_OF_TOBS_CRC_FOR_FIBER_HEADER: for i in 0 to NUMBER_OF_ROS_ROI_INPUT_BUSES - 1  generate
      SAVE_NUMBER_OF_TOBS: process(CLK)
      begin
        if rising_edge(CLK) then
          if BEGINNING_OF_DATA = '1' then
            fiber_cntr_a(j)(i)(3 downto 0) <= (others => '0');
          elsif link_number_synch_c = i and j = select_fiber_cntr_synch and data_valid_synch_c = '1' then
            fiber_cntr_a(j)(i)(3 downto 0) <= tob_cntr_out_l;
          else
            fiber_cntr_a(j)(i)(3 downto 0) <= fiber_cntr_a(j)(i)(3 downto 0);
          end if;
        end if;
      end process SAVE_NUMBER_OF_TOBS;
  
    SAVE_CRC:process(CLK)
    begin
      if rising_edge(CLK) then
        if BEGINNING_OF_DATA = '1' then
          fiber_cntr_a(j)(i)(4) <= '0';
        elsif link_number_synch_a = i and select_fiber_cntr = j and data_valid_synch_a = '1' then
          fiber_cntr_a(j)(i)(4) <= crc_synch;
        else
          fiber_cntr_a(j)(i)(4) <= fiber_cntr_a(j)(i)(4);
        end if;
      end if;
    end process SAVE_CRC;
  end generate SAVE_NUMBER_OF_TOBS_CRC_FOR_FIBER_HEADER;
  end generate SAVE_LOOP_OVER_OFFSETS;

  REWRITE_LOOP_OVER_OFFSETS: for j in 0 to 5*NUMBER_OF_PROCESSORS generate
    REWRITE_FIBER_DATA: for i in 0 to NUMBER_OF_ROS_ROI_INPUT_BUSES + 3 generate --
      --+1 for delayed muons §§D
      fiber_cntr_data_in_a(j)((i+1)*5-1 downto i*5) <= fiber_cntr_a(j)(i/5*5+5-1-(i mod 5));
    end generate REWRITE_FIBER_DATA;
  end generate REWRITE_LOOP_OVER_OFFSETS;

  --SAVE_FIBER_CNTRS:process(CLK)
  --begin
  --  if rising_edge(CLK) then
  --    if END_OF_DATA = '1' then --and unsigned(fiber_cntr_data_in_a(1)) = 0  then
  --      fiber_cntr_data_in_saved_a <= fiber_cntr_data_in_a;
  --      fiber_cntr_wr <= '0';
  --      fiber_cntr_data_in <= (others => '0');
  --      offset_cntr <= (others => '0');
  --    elsif  unsigned(fiber_cntr_data_in_saved_a(to_integer(offset_cntr+1))) /= 0 and offset_cntr < 5*NUMBER_OF_PROCESSORS then
  --      --< 10 §§T
  --      fiber_cntr_data_in_saved_a <= fiber_cntr_data_in_saved_a;
  --      fiber_cntr_wr <= '1';
  --      fiber_cntr_data_in(423 downto 0) <= fiber_cntr_data_in_a(to_integer(offset_cntr))(423 downto 0);
  --      --§§M ..<= fiber_cntr_data_in_saved_a
  --      fiber_cntr_data_in(431) <= '0';
  --      offset_cntr <= offset_cntr + 1;
  --    elsif  unsigned(fiber_cntr_data_in_saved_a(to_integer(offset_cntr+1))) = 0 and offset_cntr < 5*NUMBER_OF_PROCESSORS then
  --      --< 10 §§T
  --      fiber_cntr_data_in_saved_a <= fiber_cntr_data_in_saved_a;
  --      fiber_cntr_wr <= '1';
  --      fiber_cntr_data_in(423 downto 0) <= fiber_cntr_data_in_a(to_integer(offset_cntr))(423 downto 0);
  --      --§§M ..<= fiber_cntr_data_in_saved_a
  --      fiber_cntr_data_in(431) <= '1';
  --      offset_cntr <= to_unsigned(5*NUMBER_OF_PROCESSORS,offset_cntr'length);            --§§T "a" instead 5
  --    else
  --      fiber_cntr_data_in_saved_a <= fiber_cntr_data_in_saved_a;
  --      fiber_cntr_wr <= '0';
  --      fiber_cntr_data_in <= (others => '0');
  --      offset_cntr <= offset_cntr;
  --    end if;
  --  end if;
  --end process SAVE_FIBER_CNTRS;
  SAVE_FIBER_CNTRS:process(CLK)
  begin
    if rising_edge(CLK) then
      if END_OF_DATA = '1' then --and unsigned(fiber_cntr_data_in_a(1)) = 0  then
        fiber_cntr_data_in_saved_a <= fiber_cntr_data_in_a;
        fiber_cntr_wr <= '0';
        fiber_cntr_data_in <= (others => '0');
        offset_cntr <= (others => '0');
      elsif  unsigned(fiber_cntr_data_in_saved_a(1)) /= 0 and offset_cntr < 5*NUMBER_OF_PROCESSORS then
        fiber_cntr_data_in_saved_a <= fiber_cntr_data_in_saved_a;
        fiber_cntr_wr <= '1';
        for i in 0 to 5*NUMBER_OF_PROCESSORS-1 loop
          fiber_cntr_data_in_saved_a(i)(423 downto 0) <= fiber_cntr_data_in_saved_a(i+1)(423 downto 0);
        end loop;
        fiber_cntr_data_in(423 downto 0) <= fiber_cntr_data_in_a(0)(423 downto 0);
        fiber_cntr_data_in(431) <= '0';
        offset_cntr <= offset_cntr + 1;
      elsif  unsigned(fiber_cntr_data_in_saved_a(1)) = 0 and offset_cntr < 5*NUMBER_OF_PROCESSORS then
        fiber_cntr_data_in_saved_a <= fiber_cntr_data_in_saved_a;
        fiber_cntr_wr <= '1';
        for i in 0 to 5*NUMBER_OF_PROCESSORS-1 loop
          fiber_cntr_data_in_saved_a(i)(423 downto 0) <= fiber_cntr_data_in_saved_a(i+1)(423 downto 0);
        end loop;
        fiber_cntr_data_in(423 downto 0) <= fiber_cntr_data_in_a(0)(423 downto 0);
        fiber_cntr_data_in(431) <= '1';
        offset_cntr <= to_unsigned(5*NUMBER_OF_PROCESSORS,offset_cntr'length);            
      else
        fiber_cntr_data_in_saved_a <= fiber_cntr_data_in_saved_a;
        fiber_cntr_wr <= '0';
        fiber_cntr_data_in <= (others => '0');
        offset_cntr <= offset_cntr;
      end if;
    end if;
  end process SAVE_FIBER_CNTRS;  

  FIBER_CNTR_FIFO_INST: entity work.fiber_cntr_fifo  --§§T enlarge fifo to 1k/2k -
                                               --timing problems ?
    port map (
      clk              => CLK,
      srst              => reset_fifos(1),--RESET,
      din              => fiber_cntr_data_in,
      wr_en            => fiber_cntr_wr,
      rd_en            => fiber_cntr_rd,
      prog_full_thresh => fiber_cntr_thr,
      dout             => fiber_cntr_data_out,
      full             => fiber_cntr_full,
      empty            => fiber_cntr_empty,
      data_count       => fiber_cntr_cntr,
      prog_full        => fiber_cntr_prog_full);

  SET_FIBER_CNTR_THRESHOLD : process (CLK)
  begin
    if rising_edge(CLK) then
      if unsigned(FIBER_CNTR_FIFO_THR) = 0 then
        fiber_cntr_thr <= "111" & x"00";
      else
        fiber_cntr_thr <= FIBER_CNTR_FIFO_THR;
      end if;
    end if;
  end process SET_FIBER_CNTR_THRESHOLD;

  CRATE_NUMBER_PROC : process (CLK)
  begin
    if rising_edge(CLK) then
      crate_number_l <= CRATE_ASSIGNMENT(std_int(link_nr))(1 downto 0);--change for diff U1/U2
    end if;
  end process CRATE_NUMBER_PROC;

  
  TOB_PARSER_INST: entity work.tob_parser  --2 clocks latency
      port map (
        CLK          => CLK,
        LINK_NUMBER  => link_nr,--change for diff U1/U2
        TOB_DATA_IN  => tob_data,
        CRC_ERR_IN   => crc,
        FPGA_NUMBER  => fpga_nr_synch,
        CRATE_NUMBER => crate_number_l,
        TOB_DATA_OUT => tob_data_out);

  RELAX_TIMING_FPGA_NR: process (CLK)
  begin 
    if rising_edge(CLK) then
      fpga_nr_synch <= fpga_nr;
    end if;
  end process RELAX_TIMING_FPGA_NR;
  
  tob_type_l <= TOB_MAP(0)(std_int(link_nr)) when fpga_nr_synch = '0' else TOB_MAP(1)(std_int(link_nr));  --change for diff U1/U2
  TOB_CONSEQ_CNTR_INST: entity work.tob_bcid_cntr  --2 clocks latency
    port map (
      CLK            => CLK,
      TOB_DATA_IN    => tob_data,
      TOB_TYPE       => tob_type_l,--TOB_MAP(std_int(link_number_l)),
      CRC_ERR_IN     => crc,
      EN_ALL_DATA    => ENABLE_ALL_CMX_DATA,
      SEND_DATA_ON_CRC_ONLY  => '0',
      ROS_SELECTED  => ROS_SELECTED,
      TOB_CNTR_OUT   => open,
      TOB_CONSEQ_NRS => tob_conseq_nrs_l(39 downto 0)
      );

  SAVE_PREV_FPGA_NR: process (CLK)
  begin
    if rising_edge(CLK) then
      if SLINK_BUILDER_CURRENT = IDLE then
        fpga_nr_saved <= '0';
      elsif SLINK_BUILDER_CURRENT = DATA_WRITE then
        fpga_nr_saved <=  fpga_nr;
      else
        fpga_nr_saved <=  fpga_nr_saved;
      end if;
    end if;
  end process SAVE_PREV_FPGA_NR;
  
  SLINK_BUILDER_FSM_CLK : process (CLK)
  begin
    if rising_edge(CLK) then
      if RESET = '1' then
        SLINK_BUILDER_CURRENT   <= IDLE;
        builder_fifo_rd     <= '0';
        fiber_cntr_rd       <= '0';
        slink_data_in       <= (others => '0');
        slink_fifo_wr_en    <= '0';
        builder_debug       <= (others => '0');
        start_of_building   <= '0';
      else
        SLINK_BUILDER_CURRENT   <= SLINK_BUILDER_NEXT;
        builder_fifo_rd         <= builder_fifo_rd_fsm;
        fiber_cntr_rd           <= fiber_cntr_rd_fsm;
        slink_data_in       <= slink_data_in_fsm;
        slink_fifo_wr_en    <= slink_fifo_wr_en_fsm;
        builder_debug       <= builder_debug_fsm;
        start_of_building   <= start_of_building_fsm;
      end if;
    end if;
  end process SLINK_BUILDER_FSM_CLK;
  
--ps
  tob_conseq_nrs_l_part <= tob_conseq_nrs_l((to_integer(tob_conseq_pntr)+1)*4-1 downto to_integer(tob_conseq_pntr)*4);
  
  SLINK_BUILDER_FSM_PROC : process (all)
  begin
    builder_fifo_rd_fsm <= '0';
    fiber_cntr_rd_fsm <= '0';
    slink_data_in_fsm       <= ('1',others => '0');
    slink_fifo_wr_en_fsm    <= '0';
    builder_debug_fsm <= x"1";
    start_of_building_fsm <= '0';
    
    case (SLINK_BUILDER_CURRENT) is
      when IDLE      =>
        
        builder_debug_fsm <= x"1";
        if fiber_cntr_empty = '0' and slink_fifo_prog_full = '0' then  
          builder_fifo_rd_fsm <= '1';
          fiber_cntr_rd_fsm <= '1';
          SLINK_BUILDER_NEXT <= WAIT_FOR_BLOCK_HEADER;
        else
          
          SLINK_BUILDER_NEXT <= IDLE;
        end if;
      when WAIT_FOR_BLOCK_HEADER =>
        builder_debug_fsm <= x"2";
        start_of_building_fsm <= '1';
        SLINK_BUILDER_NEXT <= BLOCK_HEADER;
      when BLOCK_HEADER =>
        builder_debug_fsm <= x"3";
        slink_fifo_wr_en_fsm <= '1' and ROS_SELECTED;
        slink_data_in_fsm(31 downto 5) <= BLOCK_WORD_TYPE & VERSION_NUMBER & "0" & ACTIVE_LINKS_NUMBER & x"00" & "00" & fpga_nr_synch;
        slink_data_in_fsm(4) <= fiber_cntr_data_out(431); --last block marker
        if unsigned(offset) > unsigned(MAX_OFFSET_IN) then  --saving current offset (data format)
          slink_data_in_fsm(2 downto 0) <= std_logic_vector(unsigned(offset(2 downto 0)) - unsigned(MAX_OFFSET_IN(2 downto 0)));
          slink_data_in_fsm(3) <= '1'; --for negative numbers
        else
          slink_data_in_fsm(2 downto 0) <= std_logic_vector(unsigned(MAX_OFFSET_IN(2 downto 0)) - unsigned(offset(2 downto 0)));
          slink_data_in_fsm(3) <= '0';  
        end if;
        SLINK_BUILDER_NEXT <= FIBER_CNTRS;
      when FIBER_CNTRS =>
        builder_debug_fsm <= x"4";
        if word_fiber_cntr = NUMBER_OF_ROS_ROI_INPUT_BUSES/5 + 1 then 
          --divided by 5 since 5 fiber cntrs fit into 32 bit data fomrat word
          SLINK_BUILDER_NEXT <= DATA_WRITE;
        else
          slink_fifo_wr_en_fsm <= '1' and ROS_SELECTED;
          slink_data_in_fsm(31 downto 28) <= FIBER_WORD_TYPE;
          slink_data_in_fsm(27 downto 3) <= fiber_cntr_data_out(25*(word_fiber_cntr+1)-1 downto 25*word_fiber_cntr); 
          slink_data_in_fsm(2 downto 0) <= (others => '0');
          SLINK_BUILDER_NEXT <= FIBER_CNTRS;
        end if;
      when WAIT_FOR_FIFO_DATA =>
        builder_debug_fsm <= x"5";
        slink_fifo_wr_en_fsm    <= '0';
        if wait_cntr = 4 and end_data = '0' then 
          SLINK_BUILDER_NEXT <= DATA_WRITE;
        elsif(fpga_nr_saved /= fpga_nr_synch or offset_saved /= offset) and fiber_cntr_data_out(431) = '0' then
          fiber_cntr_rd_fsm <= '1';
          SLINK_BUILDER_NEXT <= WAIT_FOR_BLOCK_HEADER;
        elsif wait_cntr = 1 and end_data = '1' then 
          SLINK_BUILDER_NEXT <= FINISHED;
        elsif wait_cntr = 0 and there_is_tob = '0' and conseq_active_links_a_synch(to_integer(unsigned'(""&fpga_nr_synch)))(to_integer(next_out_active_link)) /= "0000000" then
          SLINK_BUILDER_NEXT <= DATA_WRITE;          
        else
          SLINK_BUILDER_NEXT <= WAIT_FOR_FIFO_DATA;
        end if;
      when DATA_WRITE =>
        builder_debug_fsm <= x"6";
        --  --tob_conseq_nrs_l - all TOBs has to be transferred - x"f" indicates no TOB
        --  --tob_conseq_pntr - maximum 8 TOBs in one 128bit data 9th is reserved
        --  --for error/status data
        if (end_data = '0' and ( tob_conseq_nrs_l((to_integer(tob_conseq_pntr)+1)*4-1 downto to_integer(tob_conseq_pntr)*4) = x"f" or tob_conseq_pntr = 10)) or (there_is_tob = '0' and wait_cntr = 1)  then--multiclice Nov04.2015
          --fifo rd takes 1 clock, next one clock for mux and 2 for creating
          --tobs,"f" indicates empty data in checked area
          builder_fifo_rd_fsm <= '1';
          SLINK_BUILDER_NEXT <= WAIT_FOR_FIFO_DATA;
          --read next 128 bit data to convert it to TOBs
        elsif end_data = '1' and 
          (tob_conseq_nrs_l((to_integer(tob_conseq_pntr)+1)*4-1 downto to_integer(tob_conseq_pntr)*4) = x"f" or tob_conseq_pntr = 10) then
          SLINK_BUILDER_NEXT <= FINISHED;
        else
          slink_fifo_wr_en_fsm <= '1' ;
          slink_data_in_fsm(31 downto 0) <=
          tob_data_out(32*(std_int(tob_conseq_nrs_l_part)+1)-1  downto 32*(std_int(tob_conseq_nrs_l_part)));
          SLINK_BUILDER_NEXT <= DATA_WRITE;
        end if;
      when FINISHED =>
        builder_debug_fsm <= x"7";
        slink_fifo_wr_en_fsm <= '1';
        slink_data_in_fsm(32) <= '0';
        SLINK_BUILDER_NEXT <= IDLE;
      when others    =>
        builder_debug_fsm <= x"0";
        SLINK_BUILDER_NEXT <= IDLE;
    end case;
  end process SLINK_BUILDER_FSM_PROC;

  TOB_CONSEQ_CNTR : process (CLK)
  begin
    if rising_edge(CLK) then
      if SLINK_BUILDER_CURRENT = WAIT_FOR_FIFO_DATA or SLINK_BUILDER_CURRENT = BLOCK_HEADER then
        tob_conseq_pntr <= (others => '0');
      elsif SLINK_BUILDER_CURRENT = DATA_WRITE and tob_conseq_pntr < 10 then
        tob_conseq_pntr <= tob_conseq_pntr + 1;
      else
        tob_conseq_pntr <= tob_conseq_pntr;
      end if;
    end if;
  end process TOB_CONSEQ_CNTR;
  
  FIBER_CNTR_INST : process (CLK)
  begin
    if rising_edge(CLK) then
      if SLINK_BUILDER_CURRENT /= FIBER_CNTRS then
        word_fiber_cntr <= 0;
      elsif SLINK_BUILDER_CURRENT = FIBER_CNTRS then
        word_fiber_cntr <= word_fiber_cntr + 1;
      else
        word_fiber_cntr <= word_fiber_cntr;
      end if;
    end if;
  end process FIBER_CNTR_INST;

  ITERATE_ON_FIBER_SIZE: for fiber in 0 to NUMBER_OF_ROS_ROI_INPUT_BUSES-1 generate
    SAVE_NUMBER_OF_TOBS : process (CLK)
    begin
      if rising_edge(CLK) then
        if SLINK_BUILDER_CURRENT = FIBER_CNTRS
          and (fiber/5 + 1) = integer(word_fiber_cntr) 
          and slink_data_in(26-(fiber mod 5)*5 downto 23-(fiber mod 5)*5) > 0
          and word_fiber_cntr > 0 
        then
          if_tob(fiber) <= '1';
        elsif SLINK_BUILDER_CURRENT = WAIT_FOR_BLOCK_HEADER then
          if_tob(fiber) <= '0';
        else
          if_tob(fiber) <= if_tob(fiber);
        end if;
      end if;
    end process SAVE_NUMBER_OF_TOBS;
  end generate ITERATE_ON_FIBER_SIZE;

  CNTR_FOR_CONSEQ_ACTIVE_LINKS: process(CLK)
  begin
    if rising_edge(CLK) then
      if (BEGINNING_OF_DATA = '1' or second_fpga_pulse = '1') and fiber_cntr_empty = '1' then
        next_active_link <= (others => '0');
      elsif data_valid_synch_b = '1' and fiber_cntr_empty = '1' and next_active_link < NUMBER_OF_ROS_ROI_INPUT_BUSES then
        next_active_link <= next_active_link  + 1;
      else
        next_active_link <= next_active_link;
      end if;
    end if;
  end process CNTR_FOR_CONSEQ_ACTIVE_LINKS;

  
  SAVE_CONSEQ_ITERATE_BY_PROC: for proc in 0 to 1 generate
  SAVE_CONSEQ_ACTIVE_LINKS: process(CLK)
  begin
    if rising_edge(CLK) then
      if (data_valid_synch_b = '1') and
         ( fiber_cntr_empty = '1') and
         ( to_integer(unsigned'(""&fpga_number_synch)) = proc ) and
         (next_active_link < unsigned(ACTIVE_LINKS_NUMBER(6 downto 0) -1))
      then 
        conseq_active_links_a(proc)(to_integer(next_active_link)) <= link_number_synch_b;
        conseq_active_links_a_synch(proc) <= conseq_active_links_a(proc);
      else
        conseq_active_links_a(proc) <= conseq_active_links_a(proc);
        conseq_active_links_a_synch(proc) <= conseq_active_links_a(proc);
      end if;
    end if;
  end process SAVE_CONSEQ_ACTIVE_LINKS;
  end generate SAVE_CONSEQ_ITERATE_BY_PROC;

  CNTR_FOR_CONSEQ_OUT_ACTIVE_LINKS: process(CLK)
  begin
    if rising_edge(CLK) then
      if SLINK_BUILDER_CURRENT = BLOCK_HEADER then
        next_out_active_link <= "0000001";
      elsif builder_fifo_rd = '1' and next_out_active_link < NUMBER_OF_ROS_ROI_INPUT_BUSES then
        next_out_active_link <= next_out_active_link  + 1;
      else
        next_out_active_link <= next_out_active_link;
      end if;
    end if;
  end process CNTR_FOR_CONSEQ_OUT_ACTIVE_LINKS;
  
  
  
  IF_THERE_IS_TOB: process(CLK)
  begin
    if rising_edge(CLK) then
      there_is_tob <= if_tob(to_integer(unsigned(conseq_active_links_a_synch(to_integer(unsigned'(""&fpga_nr_synch)))(to_integer(next_out_active_link)))));
      there_is_tob_synch <= there_is_tob;
    end if;
  end process IF_THERE_IS_TOB;
  
  SAVE_OFSSET : process (CLK)
  begin
    if rising_edge(CLK) then
      if builder_fifo_rd = '1' or start_of_building = '1' then
        offset_saved <= offset;
      else
        offset_saved <= offset_saved;
      end if;
    end if;
  end process ;

  WAIT_CNTR_INST : process (CLK)
  begin
    if rising_edge(CLK) then
      if SLINK_BUILDER_CURRENT = WAIT_FOR_FIFO_DATA then
        wait_cntr <= wait_cntr + 1;
      else
        wait_cntr <= (others => '0');
      end if;
    end if;
  end process WAIT_CNTR_INST;

  COUNT_EVENTS_IN_SLINK_BUFFER :process (CLK)
  begin
    if rising_edge(CLK) then
      if RESET = '1' then
        events_in_slink_buffer_cntr <= (others => '0');
      elsif SLINK_BUILDER_CURRENT = FINISHED and slink_readout_pulse = '0' then
        events_in_slink_buffer_cntr <= events_in_slink_buffer_cntr + 1;
      elsif SLINK_BUILDER_CURRENT /= FINISHED and slink_readout_pulse = '1' then
        events_in_slink_buffer_cntr <= events_in_slink_buffer_cntr - 1;
      else
        events_in_slink_buffer_cntr <= events_in_slink_buffer_cntr;
      end if;
    end if;
  end process COUNT_EVENTS_IN_SLINK_BUFFER;

  SLINK_READOUT_PULSE_PROC: process (CLK)
  begin
    if rising_edge(CLK) then
      if slink_valid_synch = '1' and slink_data_out_l(32) = '0' then
        slink_valid_synch <= slink_data_out_l(32);
        slink_readout_pulse <= '1';
      else
        slink_valid_synch <= slink_data_out_l(32);
        slink_readout_pulse <= '0';
      end if;
    end if;
  end process SLINK_READOUT_PULSE_PROC;

  -----------------------------------------------------------------------------
  -- SLINK
  -----------------------------------------------------------------------------

  SLINK_FIFO_32KW_INST: entity work.slink_fifo_32kW
    port map (
      rst              => reset_fifos(2),--RESET,
      wr_clk           => CLK,
      rd_clk           => SLINK_CLK,
      din              => slink_data_in,
      wr_en            => slink_fifo_wr_en,
      rd_en            => slink_fifo_rd_en,
      prog_full_thresh => slink_fifo_thr,
      dout             => slink_data_out_l,
      full             => slink_fifo_full,
      empty            => slink_fifo_empty,
      rd_data_count       => slink_fifo_cntr,
      prog_full        => slink_fifo_prog_full);

  SET_SLINK_THRESHOLD : process (SLINK_CLK)
  begin
    if rising_edge(SLINK_CLK) then
      if unsigned(SLINK_FIFO_32kW_THR) = 0 then
        slink_fifo_thr <= "111" & x"000";
      else
        slink_fifo_thr <= SLINK_FIFO_32kW_THR;
      end if;
    end if;
  end process SET_SLINK_THRESHOLD;
  
  SLINK_DATA_OUT   <= slink_data_out_l(31 downto 0);
  slink_fifo_rd_en <= (SLINK_READY_IN and slink_data_out_l(32)) or slink_fifo_rd_en_pulse;



  SLINK_DATA_SEND_CLK : process (SLINK_CLK, RESET)
  begin
    if rising_edge(SLINK_CLK) then
      if RESET = '1' then
        SLINK_DATA_SEND_CURRENT   <= IDLE;
        slink_fifo_rd_en_pulse    <= '0';
        slink_debug               <= "00";
      else
        SLINK_DATA_SEND_CURRENT   <= SLINK_DATA_SEND_NEXT;
        slink_fifo_rd_en_pulse    <= slink_fifo_rd_en_pulse_fsm;
        slink_debug               <= slink_debug_fsm;
      end if;
    end if;
  end process SLINK_DATA_SEND_CLK;

  SLINK_DATA_SEND_PROC : process (all)
  begin
    slink_fifo_rd_en_pulse_fsm <= '0';
    slink_debug_fsm            <= "00";
    case (SLINK_DATA_SEND_CURRENT) is
      when IDLE      =>
        slink_debug_fsm            <= "01";

        if events_in_slink_buffer_cntr /= 0 then
          SLINK_DATA_SEND_NEXT <= WAIT_FOR_DATA;
          slink_fifo_rd_en_pulse_fsm <= '1';
        else
          SLINK_DATA_SEND_NEXT <= IDLE;
        end if;
      when WAIT_FOR_DATA =>
        SLINK_DATA_SEND_NEXT <= DATA_SEND;
      when DATA_SEND =>
        slink_debug_fsm            <= "10";
        if slink_data_out_l(32) = '0' then
          SLINK_DATA_SEND_NEXT <= FINISHED;
        else
          SLINK_DATA_SEND_NEXT <= DATA_SEND;
        end if;
      when FINISHED =>
        slink_debug_fsm            <= "11";
        SLINK_DATA_SEND_NEXT <= IDLE;
      when others    =>
        slink_debug_fsm            <= "00";
        SLINK_DATA_SEND_NEXT <= IDLE;
    end case;
  end process SLINK_DATA_SEND_PROC;
    
  SLINK_EVENT_READY_OUT <= slink_data_out_l(32);
  
  BUILDER_BUSY <= slink_fifo_prog_full or fiber_cntr_prog_full or builder_fifo_prog_full;
  BUSY_DEBUG <= slink_fifo_prog_full & fiber_cntr_prog_full & builder_fifo_prog_full;

  -----------------------------------------------------------------------------
  -- counters for debug
  -----------------------------------------------------------------------------
  --builder
  COUNT_RECIVED_BUILDER: process (CLK)
  begin
    if rising_edge(CLK) then
      if RESET = '1' then
        cntr_debug_l(0) <= (others => '0');
      elsif BEGINNING_OF_DATA = '1' then
        cntr_debug_l(0) <= cntr_debug_l(0) + 1;
      else
        cntr_debug_l(0) <= cntr_debug_l(0);
      end if;
    end if;
  end process COUNT_RECIVED_BUILDER;

  COUNT_SENT_BUILDER: process (CLK)
  begin
    if rising_edge(CLK) then
      if RESET = '1' then
        cntr_debug_l(1) <= (others => '0');
      elsif SLINK_BUILDER_CURRENT = FINISHED  then
        cntr_debug_l(1) <= cntr_debug_l(1) + 1;
      else
        cntr_debug_l(1) <= cntr_debug_l(1);
      end if;
    end if;
  end process COUNT_SENT_BUILDER;
    
  PIPE_CNTRS: process(CLK)
  begin
    if rising_edge(CLK) then
      --builder
      CNTR_DEBUG(0) <= cntr_debug_l(0);
      CNTR_DEBUG(1) <= cntr_debug_l(1);
      CNTR_DEBUG(2)(13 downto 0) <= unsigned(builder_fifo_cntr);
      --fiber
      CNTR_DEBUG(3)(10 downto 0) <= unsigned(fiber_cntr_cntr);
      --slink
    end if;
  end process PIPE_CNTRS;
  
  PIPE_CNTRS_SLOW: process(SLINK_CLK)
  begin
    if rising_edge(SLINK_CLK) then
      CNTR_DEBUG(4)(14 downto 0) <= unsigned(slink_fifo_cntr);
    end if;
  end process PIPE_CNTRS_SLOW;
  
end tob_slink_builder;
    
 
