----------------------------------------------------------------------------------
-- Create Date:    15:26:21 07/04/2014 
-- Design Name: 
-- Module Name:    slinkPckBuilder - Behavioral 
-- Project Name:   ROD implementation on L1Topo
-- Target Devices: L1Topo board. Kintex 7 325T FBG900
-- Tool versions: ISE 14.7
-- Description: This component is receiving TOB (trigger object) data and puts it into slink frames. It uses 
--		hola_lsc_vtx6 component for slink protocol implementation. hola_lsc_vtx6 accepts 32 bits words, 
--		these words are parts of complete slink packet which is built by slinkPckBuilder component. 
--		The slink pck building starts when user sets enable_in on 1 and the link itself is ready (internal
--		busy_out signal is low). Readiness of the component to built packetes is signalized by issuing 
--		ready_out to 1. This signal is set one clk cycle before the data form payload_in is transfered via
--		link. What is more the component takes as the input l1a_in signal which is level one accepted trigger
--		signal. Once the signal arrives the status of the ports:    MODULE_ID,RUN_TYPE,RUN_NUMBER,ECR_ID,
--		ROD_L1_ID,ROD_BCN,TRIGGER_TYPE,DETECTOR_EVENT_TYPE is written to the fifo because the data (payload) 
--		corresponding to the given values arrives to the component with some delay. The signals which are saved
--		in fifo are part of slink header.

-- Dependencies: this component requires ip core fifo component. The settings of the ip core component are as follow:
--		+ common clk built in fifo
--		+ standard fifo 132 bit width and 512 or 1024 deep
--		+ rest settings form core generator remains unchanged
--
-- Revision: 
-- 0.02 - buffering fifo added
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;
use ieee.std_logic_unsigned.all;
use work.rod_l1_topo_types_const.all;

library UNISIM;
use UNISIM.VComponents.all;


entity slinkPckBuilder is
  generic(
    EN_DEBUG: integer := 0;
    SIMULATION : boolean := TRUE
    );
  port(
    --clocks
    TTC_CLK40    :in std_logic;
    SYSCLK        : in std_logic;       --should be 40.08 Mhz 
    CLK_LOCKED_IN : in std_logic;
    URESET_IN : in  std_logic;
    RESET : in std_logic;
    SLINK_RESET : in std_logic;
    SLINK_READY_OUT : out std_logic;
    DIABLE_SLINK : in std_logic;
    --from parser to slink
    SAVE_HEADERS_IN : in std_logic;
    ENABLE_IN : in  std_logic;  -- static value on this line enables taking data from payload fifo and send it. 
    READY_OUT : out std_logic;  -- this line goes high when data (payload) can be send.
    PAYLOAD_IN   : in std_logic_vector(31 downto 0);  --user data. Data is send on each rising edge of sysClk
    L1A_IN : in std_logic := '0'; -- level one acceptance trigger signal. This signal cause some input ports (which are part of the header of slink packet) to be stored in fifo
	 
	 --header data
    MODULE_ID           : in std_logic_vector (15 downto 0);
    RUN_TYPE            : in std_logic_vector (7 downto 0);
    RUN_NUMBER          : in std_logic_vector(23 downto 0);
    ECR_ID              : in std_logic_vector(7 downto 0);  --
    ROD_L1_ID           : in std_logic_vector(23 downto 0);  --
    ROD_BCN             : in std_logic_vector(11 downto 0);  --
    TRIGGER_TYPE        : in std_logic_vector(7 downto 0);  --
    TRIGGER_TYPE_READY_IN : in std_logic;
    TRG_TYPE_TIMEOUT : in std_logic_vector(11 downto 0);
    SUB_DET_ID_IN       : in std_logic_vector(31 downto 0);  --
    DETECTOR_EVENT_TYPE : in std_logic_vector(31 downto 0);  --
    FORMAT_VERSION_IN : in std_logic_vector(31 downto 0);
    BUSY_CNT_TIME_PERIOD_IN : in std_logic_vector (31 downto 0); -- This input defines a time period in which information about component 	 or idle state is gathered. The value should come from ipbus register
    BUSY_CNT_OUT : out std_logic_vector (31 downto 0); -- as long as ldown or lff is blocking transmission, this counter is incr.
    IDLE_CNT_OUT : out std_logic_vector (31 downto 0); -- as longas UWEN_N is set to 1 this counter is incremented.
	 
    LFF_N_OUT : out std_logic;   -- link full flag 
    LDOWN_N_OUT : out std_logic; --link down
    FIFOFULL_OUT : out std_logic;
	 
	    -- GK connections to external gtx 
    TLK_SYS_RST        : in  std_logic;
    TLK_RXCLK_IN       : in std_logic;
    TLK_GTXCLK_IN      : in std_logic;
    TLK_GTXCLK_DIV     : in std_logic;
    -- GTX receive ports
    GTX_RXUSRCLK2      : in  std_logic;
    GTX_RXDATA         : in  std_logic_vector(15 downto 0);
    GTX_RXCHARISK      : in  std_logic_vector(1 downto 0);
    GTX_RXENCOMMAALIGN : out std_logic;
    -- GTX transmit ports
    GTX_TXUSRCLK2      : in  std_logic;
    GTX_TXCHARISK      : out std_logic_vector(1 downto 0);
    GTX_TXDATA         : out std_logic_vector(15 downto 0);
	 
	 
	 DEBUG_OUT           : out std_logic_vector(255 downto 0);
         CNTR_DEBUG                 : out cntr_debug_array;
	 
	 DBG_TXD_OUT, DBG_RXD_OUT : out std_logic_vector(15 downto 0);
	 DBG_TX_EN_OUT, DBG_TX_ER_OUT, DBG_RX_DV_OUT, DBG_RX_ER_OUT : out std_logic;
	 
    --stat data
    STAT_WORD1_IN : in std_logic_vector(31 downto 0);  --any spotted errors in data should be reported in 
    STAT_WORD2_IN : in std_logic_vector(31 downto 0)  --this two status words
    
    );
end slinkPckBuilder;

architecture Behavioral of slinkPckBuilder is

  constant beginOfFrame       : std_logic_vector(31 downto 0) := x"B0F00000";
  constant endOfFrame         : std_logic_vector(31 downto 0) := x"E0F00000";
  constant startOfHeader      : std_logic_vector(31 downto 0) := x"EE1234EE";
  --constant majorFormatVersion : std_logic_vector(15 downto 0) := x"0301";
  constant HEADER_SIZE        : std_logic_vector(31 downto 0) := x"00000009";
  constant STAT_BLOCK_POS     : std_logic := '1';  --1 means data go first in payload, and after data status words
  --constant SUB_DET_ID         : std_logic_vector (7 downto 0) :=x"91";
  --constant MINOR_FORMAT_VER   : std_logic_vector(15 downto 0) :=x"1002";  -- format version
  constant NUMBER_OF_STAT_EL  : std_logic_vector(1 downto 0)  :="10";  --number of status words
                                                             
  type   state_type is (starting, reseting, idle, sendingHeader, sendingTrailer, sendingData, FirstWordA, FirstWordB, BUSY_A, BUSY_B);
  signal state, next_state : state_type := starting;

  
  signal ureset_n_sgn, ureset_n_sgn_fsm   : std_logic                     := '1';
  signal LDOWN_N_sgn    : std_logic                     := '1';
  signal uwen_cnt       : std_logic_vector(1 downto 0)  := "00";

  signal enable_in_q, enable_in_start, enable_in_end : std_logic := '0';
  
  signal uwen_allowed_n : std_logic                     := '1';
  signal word2send      : std_logic_vector(31 downto 0) := (others => '0');
  signal LFF_N_sgn      : std_logic                     := '1';
  signal busy_sgn   : std_logic                     := '0';
  signal uctrl_n_sgn    : std_logic                     := '1';
  signal ready_out_l    : std_logic;
  signal oh_index : integer range 0 to 19:= 19;
  signal number_of_data_el : unsigned(15 downto 0) := (others => '0');
  signal l1a_sgn_q ,l1a_sgn,l1a_sgn_qq, headerSave : std_logic:= '0';
  --signal enable_in_sgn, enable_in_sgn_q,  enable_in_sgn_qq, readFifo : std_logic  := '0';
  signal enable_in_sgn, readFifo_flag_q,  readFifo_flag, readFifo : std_logic  := '0';
  signal savedHeader, inputFifo, status_reg, saved_status : std_logic_vector(131 downto 0) := (others => '0');
  signal ddd_fifo_empty : std_logic  := '0';
  
  type   overhead is array (19 downto 0) of std_logic_vector(31 downto 0);  --header and trailer words
  signal oh : overhead;
  signal debug, debug_fsm : std_logic_vector(2 downto 0);

  
  signal busy_cnt_sgn,time_cnt_for_idle_busy,idle_cnt_sgn : UNSIGNED(31 downto 0);
  
  signal status_fifo_empty : std_logic;
  signal u_rst_cntr : unsigned(15 downto 0);
  signal hola_debug_l : std_logic_vector(255 downto 0);
  signal cntr_debug_l                                                     : cntr_debug_array;
  signal end_of_event : std_logic;
  signal full_local : std_logic;
  signal status_fifo_rst : std_logic;
  signal timeout_cntr : unsigned(11 downto 0):=(others => '1');
  signal local_timeout_value : std_logic_vector(11 downto 0);
  signal timeout_marker : std_logic:='0';
  signal header_fifo_cntr, status_fifo_cntr : std_logic_vector(9 downto 0);
  signal every_four_tick_cntr : unsigned(1 downto 0) :=(others => '0');
  signal status_readfifo : std_logic := '0';
  signal timeouted_status_readFifo : std_logic := '0';
  


	signal cnt : std_logic_vector(7 downto 0):=(others=>'0');
	signal cntLFF : std_logic_vector(15 downto 0):=(others=>'0');
	signal ldown_latch : std_logic := '0';
	
	signal local_reset : std_logic;
  attribute dont_touch : string;
  attribute dont_touch of status_fifo_rst, status_readFifo: signal is "true";
begin
  
  HOLA_SIM_GEN : if SIMULATION = true generate
    --process(sysClk,ureset_n_sgn)
    --begin
    --  if rising_edge(sysClk) then
    --    if(ureset_n_sgn = '0') then
    --      cnt <= cnt + 1;
    --    else
    --      cnt <= (others=>'0');
    --    end if;
    --    cntLFF <= cntLFF + 1;
    --  end if;
    --end process;
    
    --process(sysClk, cnt,ldown_latch)
    --begin
    --  if(cnt > 64) then
    --    ldown_latch <= '1';
    --  else
    --    ldown_latch <= ldown_latch;
    --  end if;
    --end process;
    --LDOWN_N_sgn <= ldown_latch;
    --LFF_N_sgn <= not LFF_N_sgn when cntLFF = 250 else LFF_N_sgn;
    --process -- (sysClk)
    --begin --the commented part can be used to generate non trivial busy structure
      --for i in 1 to 1 loop

      --  for j in 0 to 2 loop
      --    LFF_N_sgn <= '0';
      --    wait until rising_edge(sysCLK);
      --    LFF_N_sgn <= '1';
      --    wait until rising_edge(sysCLK);
      --  end loop;  

      --  for j in 0 to 2 loop
      --    LFF_N_sgn <= '0';
      --    wait until rising_edge(sysCLK);
      --    wait until rising_edge(sysCLK);
      --    LFF_N_sgn <= '1';
      --    wait until rising_edge(sysCLK);
      --    wait until rising_edge(sysCLK);
      --  end loop;  

      --  for j in 0 to 2 loop
      --    LFF_N_sgn <= '0';
      --    wait until rising_edge(sysCLK);
      --    wait until rising_edge(sysCLK);
      --    wait until rising_edge(sysCLK);
      --    LFF_N_sgn <= '1';
      --    wait until rising_edge(sysCLK);
      --    wait until rising_edge(sysCLK);
      --    wait until rising_edge(sysCLK);
      --  end loop;  
      --  LFF_N_sgn <= '0';
      --  wait until rising_edge(sysCLK);
      --end loop;
      --wait until rising_edge(sysCLK);
      --LFF_N_sgn <= '0';
      --wait until rising_edge(sysCLK);
      --LFF_N_sgn <= '1';
      --wait for 800 ns;
      --LFF_N_sgn <= '0';
    --end process;
    LFF_N_sgn <= '1';
    LDOWN_N_sgn <= '1';
  end generate HOLA_SIM_GEN;

  DISABLE_HOLA_FOR_SIM: if SIMULATION = false generate
    hola_inst : entity work.hola_lsc_vtx6
      port map(
        TLK_RXCLK_IN  => TLK_RXCLK_IN,  --100MHz from gtx ref
        TLK_GTXCLK_IN => TLK_GTXCLK_IN,  --100MHz from gtx tx
        TLK_GTXCLK_DIV => TLK_GTXCLK_DIV,  
        SYS_RST       => URESET_IN,
        UD            => word2send,   
        URESET_N      => ureset_n_sgn,  
        UTEST_N       => '1',
        UCTRL_N       => uctrl_n_sgn,
        UWEN_N        => uwen_allowed_n,
        UCLK          => sysClk,        --should be 40.08 MHz
        LFF_N         => LFF_N_sgn,
        LRL           => open,
        LDOWN_N       => LDOWN_N_sgn,
        TX_ER_OUT         => open,
        DBG_TXD_OUT => DBG_TXD_OUT,
        DBG_RXD_OUT => DBG_RXD_OUT,
        DBG_TX_EN_OUT => DBG_TX_EN_OUT,
        DBG_TX_ER_OUT => DBG_TX_ER_OUT,
        DBG_RX_DV_OUT => DBG_RX_DV_OUT,
        DBG_RX_ER_OUT => DBG_RX_ER_OUT,
        GTX_TXRESETDONE_DEBUG => open,
        GTX_RESET_DEBUG  => open,
        GTX_CPLLOCK_DEBUG  => open,
        TLK_RXCLK => open,
        TLK_SYS_RST        => TLK_SYS_RST,
        GTX_RXUSRCLK2      => GTX_RXUSRCLK2,  --100MHz
        GTX_RXDATA         => GTX_RXDATA,
        GTX_RXCHARISK      => GTX_RXCHARISK,
        GTX_RXENCOMMAALIGN => GTX_RXENCOMMAALIGN,
        GTX_TXUSRCLK2      => GTX_TXUSRCLK2,  --100MHz
        GTX_TXCHARISK      => GTX_TXCHARISK,
        GTX_TXDATA         => GTX_TXDATA,
        TESTLED_N     => open,
        LDERRLED_N    => open,
        LUPLED_N      => open,
        FLOWCTLLED_N  => open,
        ACTIVITYLED_N => open, 
        HOLA_DEBUG    => hola_debug_l
      );
  end generate DISABLE_HOLA_FOR_SIM;
  LFF_N_OUT <= LFF_N_sgn;
  LDOWN_N_OUT <= LDOWN_N_sgn;

------------------------------------------------------------------------------
--debug ila
-----------------------------------------------------------------------------
  ENABLE_SLINK_PACKET_ILA: if EN_DEBUG = 1 generate
    SLINK_PACKET_ILA: entity work.slink_packet_ila
      port map(
        clk => sysClk,
        probe0(0) => URESET_IN,
        probe1(0) => ENABLE_IN,
        probe2 => ROD_L1_ID, --24
        probe3(0) => headerSave,
        probe4(0) => readFifo,
        probe5(0) => full_local, --1
        probe6 => header_fifo_cntr, --10
        probe7(0) => ddd_fifo_empty, --1
        probe8 => savedHeader(75 downto 52),--24
        probe9 => std_logic_vector(to_unsigned(oh_Index,5)),--
        probe10(0) => timeout_marker,
        probe11 => word2send,
        probe12(0) => uwen_allowed_n
        );
  end generate ENABLE_SLINK_PACKET_ILA;
  
-------------------------------------------------------------------------------
-- save trigger info
-------------------------------------------------------------------------------  
  l1a_sgn <= L1A_IN;             
  L1A_handling : process (TTC_CLK40)
  begin
    if rising_edge(TTC_CLK40) then
      l1a_sgn_q <= l1a_sgn;
      l1a_sgn_qq <= l1a_sgn_q;
      inputFifo <= MODULE_ID & RUN_TYPE & RUN_NUMBER & ECR_ID & ROD_L1_ID &
                   ROD_BCN & TRIGGER_TYPE & DETECTOR_EVENT_TYPE;
    end if;
  end process;

  EN_HEADER_WR: process(TTC_CLK40)
  begin
    if rising_edge(TTC_CLK40) then
      if l1a_sgn_q = '1' and l1a_sgn_qq = '0' then
        headerSave <= '1';
      else
        headerSave <= '0';
      end if;
    end if;
  end process EN_HEADER_WR;

  headerBuffering : entity work.slinkPckBuilder_headerBuffer_132b_1024w
    PORT MAP (
        --clk => SYSCLK,
        wr_clk => TTC_CLK40,
        rd_CLK => SYSCLK,
        rst => URESET_IN,
        din =>  inputFifo,
        wr_en => headerSave,
        rd_en => readFifo,
        dout => savedHeader,
        full => full_local,--FIFOFULL_OUT,
        prog_full => FIFOFULL_OUT,
        rd_data_count => header_fifo_cntr,
        empty => ddd_fifo_empty
        );
 
    statusBuffering : entity work.slinkPckBuilder_headerBuffer_132b_1024w
      PORT MAP (
        --clk => SYSCLK,
        wr_clk => TTC_CLK40,
        rd_clk => SYSCLK,
        rst => status_fifo_rst,
        din =>  status_reg,
        wr_en => SAVE_HEADERS_IN,
        rd_en => status_readFifo,
        dout => saved_status,
        full => open,
        prog_full => open,
        rd_data_count => status_fifo_cntr,
        empty => status_fifo_empty
        );

  RESET_STATUS_FIFO_WHEN_LATE: process (sysCLK)
  begin
    if rising_edge(sysClk) then
      if URESET_IN = '1' or (ddd_fifo_empty = '1' and status_fifo_empty = '0') then
        status_fifo_rst <= '1';
      else
        status_fifo_rst <= '0';
      end if;
    end if;    
  end process RESET_STATUS_FIFO_WHEN_LATE;

  READ_OUT_STATUS_WHEN_TIMEOUTED_DATA: process (SYSCLK)
  begin  
    if rising_edge(sysClk) then
      if (unsigned(status_fifo_cntr) > unsigned(header_fifo_cntr)) and every_four_tick_cntr = "11" then
        every_four_tick_cntr <= every_four_tick_cntr + 1;
        timeouted_status_readFifo <= '1';
        status_readFifo <= readFifo or timeouted_status_readFifo;
      else
        every_four_tick_cntr <= every_four_tick_cntr + 1;
        timeouted_status_readFifo <= '0';
        status_readFifo <= readFifo or timeouted_status_readFifo;
      end if;
    end if;
  end process READ_OUT_STATUS_WHEN_TIMEOUTED_DATA;
  

-------------------------------------------------------------------------------
--slink startup 
-------------------------------------------------------------------------------

  MAKE_PULSE_SLINK_RESET : process (sysClk)
  begin
    if rising_edge(sysClk) then
      if state = idle and LDOWN_N_sgn = '0' then
        u_rst_cntr <= u_rst_cntr + 1;
      else
        u_rst_cntr <= (others => '0');       
      end if;
    end if;
  end process MAKE_PULSE_SLINK_RESET;
                
  SLINK_FSM_sync : process(sysClk)
  begin
    if rising_edge(sysClk) then
      if ureset_in = '1' then
        state <= idle;
        ureset_n_sgn <= '1';
        debug <= "000";
      elsif SLINK_RESET = '1' then 
        state <= starting;
        ureset_n_sgn <= '1';
        debug <= "000";
      else
        ureset_n_sgn <= ureset_n_sgn_fsm;
        state <= next_state;
        debug <= debug_fsm;
      end if;
    end if;
  end process;

  UWEN_CNTR_PROC: process(sysClk)
  begin
    if rising_edge(sysClk) then
      if(state = reseting and LDOWN_N_sgn = '1' and uwen_cnt /= "10") then
        uwen_cnt <= uwen_cnt + '1';
      else
        uwen_cnt <= "00";
      end if;
    end if;
  end process UWEN_CNTR_PROC; 
  
  SLINK_FSM : process(sysClk, CLK_LOCKED_IN, LDOWN_N_sgn, uwen_cnt, state)
  begin
    ureset_n_sgn_fsm <= '1';
    debug_fsm <= "000";
    case state is
      when starting =>
        debug_fsm <= "001";
        if(CLK_LOCKED_IN = '0') then
          next_state <= starting;
        else
          next_state <= reseting;
        end if;
      when reseting =>
        debug_fsm <= "010";
        ureset_n_sgn_fsm <= '0';
        if(LDOWN_N_sgn = '1' and uwen_cnt = "10") then  --link becomes ready
          next_state <= idle;
        else
          next_state <= reseting;
        end if;
      when idle =>
        debug_fsm <= "011";
        next_state <= idle;
      when others =>
        next_state <= idle;
    end case;
  end process;   

-------------------------------------------------------------------------------
-- timeout
-------------------------------------------------------------------------------
  DEFAULT_TIMOUT:process(sysClk)
  begin
    if rising_edge(sysClk) then
      if TRG_TYPE_TIMEOUT(11 downto 0) = x"000" then
        local_timeout_value <= x"0ff"; 
      else
        local_timeout_value <= TRG_TYPE_TIMEOUT(11 downto 0);
      end if;
    end if;
  end process DEFAULT_TIMOUT;
  
  TRIG_TYPE_TIMEOUT: process (sysClk)
  begin
    if rising_edge(sysClk) then
      if ENABLE_IN = '1' and oh_index = 19 and ddd_fifo_empty = '0' and status_fifo_empty = '1' and timeout_cntr > (unsigned(local_timeout_value) + 1) then
        timeout_cntr <= (others => '0');
      elsif status_fifo_empty = '0' then
        timeout_cntr <= unsigned(local_timeout_value) + 2;
      elsif timeout_cntr < (unsigned(local_timeout_value) + 2) then
        timeout_cntr <= timeout_cntr + 1;
      else
        timeout_cntr <= timeout_cntr;
      end if;
    end if;
  end process TRIG_TYPE_TIMEOUT;

  MARK_TIMEOUT: process (sysClk)
  begin
    if rising_edge(sysClk) then
      if timeout_cntr = unsigned(local_timeout_value) then
        timeout_marker <= '1';
      elsif oh_index = 16 then
        timeout_marker <= '0';
      else
        timeout_marker <= timeout_marker;
      end if;
    end if;  
  end process MARK_TIMEOUT;
    
-------------------------------------------------------------------------------
-- send packet - async processes -> clock is 25ns period no denger of timing problems
-------------------------------------------------------------------------------
  busy_sgn <= not(LFF_N_sgn and LDOWN_N_sgn);
  
  SET_OH_INDEX: process (sysClk)
  begin
    if rising_edge(sysClk) then
      if URESET_IN = '1' then
        oh_index <= 19;
      elsif (ENABLE_IN = '1' and oh_index = 19 and ddd_fifo_empty = '0' and status_fifo_empty = '0') or timeout_cntr = unsigned(local_timeout_value) then
        oh_index <= 0;
      elsif busy_sgn = '0' and oh_index /= 10 and oh_index < 17 then
        oh_Index <= oh_Index + 1;
      elsif oh_index = 10 and ENABLE_IN = '0' then
        oh_Index <= oh_Index + 1;
      elsif oh_index > 16 and oh_index < 19 then
        oh_Index <= oh_Index + 1;
      else
        oh_Index <= oh_Index;
      end if;
    end if;
  end process SET_OH_INDEX;

  READ_HEADER_FIFO: process (sysCLK)
  begin  
    if rising_edge(sysClk) then
      if uctrl_n_sgn = '0' and oh_Index = 1  then
        readFifo <= '1';
      else
        readFifo <= '0';
      end if;
    end if;
  end process READ_HEADER_FIFO;
  
  --header
  oh(0)  <= beginOfFrame; --const
  oh(1)  <= startOfHeader;--const
  oh(2)  <= HEADER_SIZE;  --const
  oh(3)  <= FORMAT_VERSION_IN;--this part of header is read from ipbus register
                              ----majorFormatVersion & MINOR_FORMAT_VER; --const
  oh(4)  <= SUB_DET_ID_IN;
  oh(5)  <= savedHeader(115 downto 84); --RUN_TYPE & RUN_NUMBER; 8 and 24 bits
  oh(6)  <= savedHeader(83 downto 52); --    ECR_ID & ROD_L1_ID; 8 and 24 bits
  oh(7)  <= x"00000" & savedHeader(51 downto 40); --ROD_BCN
  oh(8)  <= x"000000" & saved_status(71 downto 64) when timeout_marker = '0' else x"000000ff"; --savedHeader(39 downto 32); --TRIGGER_TYPE
  oh(9)  <= savedHeader(31 downto 0);--DETECTOR_EVENT_TYPE;
  --payload
  oh(10) <= payload_in;
  --trailer
  oh(11) <= saved_status(31 downto 0); --(others => '0');
  oh(12) <= saved_status(63 downto 32) when timeout_marker = '0' else  saved_status(63 downto 49) & '1' & saved_status(47 downto 32);
  oh(13) <= b"00_0000_0000_0000_0000_0000_0000_0000" & NUMBER_OF_STAT_EL;--const
  oh(14) <= x"0000" & std_logic_vector(number_of_data_el - 1);
  oh(15) <= b"000_0000_0000_0000_0000_0000_0000_0000" & STAT_BLOCK_POS; --const
  oh(16) <= endOfFrame; --const
  oh(17) <= (others => '0');
  
  --uctrl line is used to signalize of start and stop of payload data
  SET_UCTRL_PROC: process (sysClk)
  begin  -- process
    if rising_edge(sysClk) then
      if (oh_Index = 0 or oh_Index = 16) and LFF_N_sgn = '1' and DIABLE_SLINK = '0' then
        uctrl_n_sgn <= '0';
      else
        uctrl_n_sgn <= '1';
      end if;
    end if;
  end process SET_UCTRL_PROC;

  SET_UWEN : process (sysClk)
  begin  -- process
    if rising_edge(sysClk) then
      if oh_index < 17 and LFF_N_sgn = '1' and oh_index /= 10 and DIABLE_SLINK = '0' then
        uwen_allowed_n <= '0';
      elsif ENABLE_IN = '1' and oh_index = 10 and LFF_N_sgn = '1' and DIABLE_SLINK = '0' then
        uwen_allowed_n <= '0';
      else
        uwen_allowed_n <= '1';
      end if;
    end if;
  end process SET_UWEN;

  SET_WORD : process (sysClk)
  begin  -- process
    if rising_edge(sysClk) then
      word2send <= oh(oh_Index);
    end if;
  end process SET_WORD;

  --uctrl_n_sgn <= '0' when ((oh_Index = 0 or oh_Index = 16) and busy_sgn = '0') else '1';
  --uwen_allowed_n <= '0' when (oh_index < 17 and busy_sgn = '0') else '1' ;
  
  READY_OUT <= '1' when ENABLE_IN = '1' and LFF_N_sgn = '1' and oh_index = 10 else '0';
  
  COUNT_DATA_WORDS : process (sysCLK)
  begin
    if rising_edge(sysCLK) then
      if oh_index = 0 then
        number_of_data_el <= (others => '0');
      elsif uwen_allowed_n = '0' and oh_index = 10 then
        number_of_data_el <= number_of_data_el + 1;
      else
        number_of_data_el <= number_of_data_el;
      end if;  
    end if;  
  end process COUNT_DATA_WORDS;

-----------------------------------------------------------------------------
-- debug
-----------------------------------------------------------------------------
  status_reg(63 downto 0)   <= STAT_WORD1_IN & STAT_WORD2_IN;
  status_reg(71 downto 64)  <= TRIGGER_TYPE;
  status_reg(131 downto 72) <= (others => '0');


  IDLE_CNT_OUT <= std_logic_vector(idle_cnt_sgn);
  IDLE_CNT_PROCESS : process(sysClk)
  begin
      
    if rising_edge(sysClk) then
      if (URESET_IN = '1' or time_cnt_for_idle_busy > unsigned(BUSY_CNT_TIME_PERIOD_IN)) then
        idle_cnt_sgn <= (others => '0');
        time_cnt_for_idle_busy <= (others=>'0');
      elsif(uwen_allowed_n = '1') then
        idle_cnt_sgn <= idle_cnt_sgn + 1;
        time_cnt_for_idle_busy <= time_cnt_for_idle_busy + 1;
      else
        idle_cnt_sgn <= idle_cnt_sgn;
        time_cnt_for_idle_busy <= time_cnt_for_idle_busy + 1;
      end if;
    end if;
  end process;
  BUSY_CNT_OUT <= std_logic_vector(busy_cnt_sgn);
  
  busy_cnt_process : process(sysClk)
  begin      
    if rising_edge(sysClk) then
      if (URESET_IN = '1'  or time_cnt_for_idle_busy > unsigned(BUSY_CNT_TIME_PERIOD_IN)) then
        busy_cnt_sgn <= (others => '0');      
      elsif( busy_sgn = '1') then
        busy_cnt_sgn <= busy_cnt_sgn + 1;
      else
        busy_cnt_sgn <= busy_cnt_sgn;
      end if;
    end if;
  end process;

  --in
  COUNT_RECIVED: process (sysCLK)
  begin
    if rising_edge(sysCLK) then
      if RESET = '1' then
        cntr_debug_l(0) <= (others => '0');
        enable_in_sgn <= ENABLE_IN;
      elsif ENABLE_IN = '1' and enable_in_sgn  = '0' then
        enable_in_sgn <= ENABLE_IN;
        cntr_debug_l(0) <= cntr_debug_l(0) + 1;
      else
        enable_in_sgn <= ENABLE_IN;
        cntr_debug_l(0) <= cntr_debug_l(0);
      end if;
    end if;
  end process COUNT_RECIVED;

  SET_UCTRL: process (sysClk)
  begin
    if rising_edge(sysClk) then
      if oh_Index = 16 and busy_sgn = '0' then
        end_of_event <= '1';
      else
        end_of_event <= '0';
      end if;
    end if;
  end process SET_UCTRL;
  
  COUNT_SENT: process (sysCLK)
  begin
    if rising_edge(sysCLK) then
      if RESET = '1' then
        cntr_debug_l(1) <= (others => '0');
      elsif end_of_event = '1'  then
        cntr_debug_l(1) <= cntr_debug_l(1) + 1;
      else
        cntr_debug_l(1) <= cntr_debug_l(1);
      end if;
    end if;
  end process COUNT_SENT;

  COUNT_L1A_WRITES: process (TTC_CLK40)
  begin
    if rising_edge(TTC_CLK40) then
      if RESET = '1' then
        cntr_debug_l(2) <= (others => '0');
      elsif headerSave = '1'  then
        cntr_debug_l(2) <= cntr_debug_l(2) + 1;
      else
        cntr_debug_l(2) <= cntr_debug_l(2);
      end if;
    end if;
  end process COUNT_L1A_WRITES;

  COUNT_L1A_READS: process (sysCLK)
  begin
    if rising_edge(sysCLK) then
      if RESET = '1' then
        cntr_debug_l(3) <= (others => '0');
      elsif readFifo = '1' then
        cntr_debug_l(3) <= cntr_debug_l(3) + 1;
      else
        cntr_debug_l(3) <= cntr_debug_l(3);
      end if;
    end if;
  end process COUNT_L1A_READS;

  COUNT_L1A_WRITES_WHEN_BUSY: process (TTC_CLK40)
  begin
    if rising_edge(TTC_CLK40) then
      if RESET = '1' then
        cntr_debug_l(4) <= (others => '0');
      elsif headerSave = '1' and full_local = '1' then
        cntr_debug_l(4) <= cntr_debug_l(4) + 1;
      else
        cntr_debug_l(4) <= cntr_debug_l(4);
      end if;
    end if;
  end process COUNT_L1A_WRITES_WHEN_BUSY;

  COUNT_L1HEADERS_WRITES_WHEN_BUSY: process (TTC_CLK40)
  begin
    if rising_edge(TTC_CLK40) then
      if RESET = '1' then
        cntr_debug_l(5) <= (others => '0');
      elsif SAVE_HEADERS_IN = '1' then
        cntr_debug_l(5) <= cntr_debug_l(5) + 1;
      else
        cntr_debug_l(5) <= cntr_debug_l(5);
      end if;
    end if;
  end process COUNT_L1HEADERS_WRITES_WHEN_BUSY;
    
  PIPE_CNTRS: process(TTC_CLK40)
  begin
    if rising_edge(TTC_CLK40) then
      CNTR_DEBUG(0) <= cntr_debug_l(0);
      CNTR_DEBUG(1) <= cntr_debug_l(1);
      CNTR_DEBUG(2) <= cntr_debug_l(2);
      CNTR_DEBUG(3) <= cntr_debug_l(3);
      CNTR_DEBUG(4) <= cntr_debug_l(4);
      CNTR_DEBUG(5) <= cntr_debug_l(5);
    end if;
  end process PIPE_CNTRS;


end Behavioral;

