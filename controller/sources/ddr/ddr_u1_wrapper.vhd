library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNISIM;
use UNISIM.VComponents.all;

use work.rod_l1_topo_types_const.all;

entity ddr_u1_wrapper is
generic (
	SIMULATION		: boolean := false;
	VIVADO			: boolean := false;
	LINES_ON_BANK17 : integer range 0 to 16 := 1;
	LINES_ON_BANK32 : integer range 0 to 16 := 7
);
port(
	GCK40_IN 			: in std_logic;
	DELAY_CTRL_CLK_IN : in std_logic;
	DDR_RST_IN 			: in std_logic;
	
	EXT_DDR_CLK_IN		: in std_logic;
	EXT_DDR_CLK_X8_IN	: in std_logic;
	
	DATA_BANK17_IN_P, DATA_BANK17_IN_N : in std_logic_vector(LINES_ON_BANK17 - 1 downto 0);
	DATA_BANK32_IN_P, DATA_BANK32_IN_N : in std_logic_vector(LINES_ON_BANK32 - 1 downto 0);
	
	DATA_OUT			: out std_logic_vector(8 * 8 - 1 downto 0);
	DATA_DV_OUT		: out std_logic_vector(7 downto 0); 
	DATA_KCTRL_OUT	: out std_logic_vector(7 downto 0);
	
	DELAY_VALS_IN      : in std_logic_vector((LINES_ON_BANK17 + LINES_ON_BANK32) * 5 - 1 downto 0);
	DELAY_LOAD_IN      : in std_logic_vector((LINES_ON_BANK17 + LINES_ON_BANK32) - 1 downto 0);
        DELAY_VALS_OUT     : out std_logic_vector((LINES_ON_BANK17 + LINES_ON_BANK32) * 5 - 1 downto 0);
	
	TRANSMITTED_BYTES_CTR_OUT	: out std_logic_vector(127 downto 0);
	INVALID_CHAR_CTR_OUT			: out std_logic_vector(127 downto 0);
	
	DDR_SYNCED_OUT : out std_logic_vector(7 downto 0);
	
	DEBUG_OUT : out std_logic_vector(63 downto 0)	
);
end ddr_u1_wrapper;

architecture rtl of ddr_u1_wrapper is
	
	signal ones : std_logic_vector(7 downto 0);
	
	signal ddr_receivers_synced_bank17 : std_logic_vector(LINES_ON_BANK17 - 1 downto 0);
	signal ddr_receivers_synced_bank32 : std_logic_vector(LINES_ON_BANK32 - 1 downto 0);
	
	signal ddr_data_from_bank17 : std_logic_vector(LINES_ON_BANK17 * 8 - 1 downto 0);
	signal ddr_data_from_bank32 : std_logic_vector(LINES_ON_BANK32 * 8 - 1 downto 0);
	signal ddr_dv_from_bank17 : std_logic_vector(LINES_ON_BANK17 - 1 downto 0);
	signal ddr_dv_from_bank32 : std_logic_vector(LINES_ON_BANK32 - 1 downto 0);
	signal ddr_kctrl_from_bank17 : std_logic_vector(LINES_ON_BANK17 - 1 downto 0);
	signal ddr_kctrl_from_bank32 : std_logic_vector(LINES_ON_BANK32 - 1 downto 0);
	
	signal links_synced_u1 : std_logic_vector(7 downto 0);
	
	type ctr_array is array(7 downto 0) of std_logic_vector(15 downto 0);
	signal trans_bytes_array, invalid_char_array : ctr_array;
	
	signal data_dv : std_logic_vector(7 downto 0);
	signal ddr_code_err_from_bank17, ddr_disp_err_from_bank17 : std_logic_vector(LINES_ON_BANK17 - 1 downto 0);
	signal ddr_code_err_from_bank32, ddr_disp_err_from_bank32 : std_logic_vector(LINES_ON_BANK32 - 1 downto 0);
	signal code_err, disp_err : std_logic_vector(7 downto 0);

begin
	
DEBUG_OUT <= (others => '0');

ones <= (others => '1');

ddr_bank17 : entity work.ddr_links_wrapper
generic map(
                DELAY_GROUP_NAME				=> "bank17_delay_group",
                AVAILABLE_LVDS_LINES		=> LINES_ON_BANK17,
                EXCLUDE_DCM_IDELAY_CTRL	=> TRUE,
                MANUAL_SYNC					=> TRUE,
                SIMULATION						=> SIMULATION,
                VIVADO							=> VIVADO
)
port map(
                GCLK_40_IN         => GCK40_IN,
                DELAY_CLK_IN       => DELAY_CTRL_CLK_IN,
                EXT_DDR_CLK_IN     => EXT_DDR_CLK_IN,
                EXT_DDR_CLK_X8_IN  => EXT_DDR_CLK_X8_IN,
                INT_DDR_CLK_OUT    => open,
                RESET_IN           => DDR_RST_IN,
                
                LVDS_IN_P          => DATA_BANK17_IN_P,
                LVDS_IN_N          => DATA_BANK17_IN_N,
               
                LINKS_SYNCED_OUT   => ddr_receivers_synced_bank17,
                RESET_TRANS_OUT    => open,
                
                DELAY_VALS_IN      => DELAY_VALS_IN(LINES_ON_BANK17 * 5 - 1 downto 0),
                DELAY_LOAD_IN      => DELAY_LOAD_IN(LINES_ON_BANK17 - 1 downto 0),
                DELAY_VALS_OUT     => DELAY_VALS_OUT(LINES_ON_BANK17 * 5 - 1 downto 0),
                               
                DATA_OUT           => ddr_data_from_bank17,
                DATA_VALID_OUT     => ddr_dv_from_bank17,
                DATA_KCTRL_OUT     => ddr_kctrl_from_bank17,
                DATA_CODE_ERR_OUT  => ddr_code_err_from_bank17,
                DATA_DISP_ERR_OUT  => ddr_disp_err_from_bank17, 
                
                DBG_STATE_OUT    => open,
                DBG_REG_DATA_OUT => open,
                DBG_BITSLIP_OUT  => open,
                DBG_INC_OUT      => open,
                DBG_PAUSE_OUT    => open,
                DBG_STEP_OUT     => open,
                DBG_RETRY_OUT    => open
);
 
ddr_bank32 : entity work.ddr_links_wrapper
generic map(
                DELAY_GROUP_NAME				=> "bank32_delay_group",
                AVAILABLE_LVDS_LINES		=> LINES_ON_BANK32,
                EXCLUDE_DCM_IDELAY_CTRL	=> TRUE,
                MANUAL_SYNC					=> TRUE,
                SIMULATION						=> SIMULATION,
                VIVADO							=> VIVADO
)
port map(
                GCLK_40_IN         => GCK40_IN,
                DELAY_CLK_IN       => DELAY_CTRL_CLK_IN,
                EXT_DDR_CLK_IN     => EXT_DDR_CLK_IN,
                EXT_DDR_CLK_X8_IN  => EXT_DDR_CLK_X8_IN,
                INT_DDR_CLK_OUT    => open,
                RESET_IN           => DDR_RST_IN,
               
                LVDS_IN_P          => DATA_BANK32_IN_P,
                LVDS_IN_N          => DATA_BANK32_IN_N,
               
                LINKS_SYNCED_OUT   => ddr_receivers_synced_bank32,
                RESET_TRANS_OUT    => open,           
               
                DELAY_VALS_IN      => DELAY_VALS_IN((LINES_ON_BANK17 + LINES_ON_BANK32) * 5 - 1 downto LINES_ON_BANK17 * 5),
                DELAY_LOAD_IN      => DELAY_LOAD_IN((LINES_ON_BANK17 + LINES_ON_BANK32) - 1 downto LINES_ON_BANK17),
                DELAY_VALS_OUT     => DELAY_VALS_OUT((LINES_ON_BANK17 + LINES_ON_BANK32) * 5 - 1 downto LINES_ON_BANK17 * 5),                    
                
                DATA_OUT           => ddr_data_from_bank32,
                DATA_VALID_OUT     => ddr_dv_from_bank32,
                DATA_KCTRL_OUT     => ddr_kctrl_from_bank32,
                DATA_CODE_ERR_OUT  => ddr_code_err_from_bank32,
                DATA_DISP_ERR_OUT  => ddr_disp_err_from_bank32, 
                
                DBG_STATE_OUT    => open,
                DBG_REG_DATA_OUT => open,
                DBG_BITSLIP_OUT  => open,
                DBG_INC_OUT      => open,
                DBG_PAUSE_OUT    => open,
                DBG_STEP_OUT     => open,
                DBG_RETRY_OUT    => open
);

--DDR_SYNCED_OUT <= '1' when (links_synced_u2 = ones) else '0'; --ps
DDR_SYNCED_OUT <= links_synced_u1;


-- #### debugging
trans_ctrs_gen : for i in 0 to 7 generate
process(GCK40_IN)
begin
	if rising_edge(GCK40_IN) then
		if (DDR_RST_IN = '1') then
			trans_bytes_array(i) <= (others => '0');
		elsif (data_dv(i) = '1') then
			trans_bytes_array(i) <= trans_bytes_array(i) + 1;
		else
			trans_bytes_array(i) <= trans_bytes_array(i);
		end if;
	end if;
end process;

TRANSMITTED_BYTES_CTR_OUT((i + 1) * 16 - 1 downto i * 16) <= trans_bytes_array(i);
end generate trans_ctrs_gen;

invalid_ctrs_gen : for i in 0 to 7 generate
process(GCK40_IN)
begin
	if rising_edge(GCK40_IN) then
		if (DDR_RST_IN = '1') then
			invalid_char_array(i) <= (others => '0');
		elsif (code_err(i) = '1' or disp_err(i) = '1') then
			invalid_char_array(i) <= invalid_char_array(i) + 1;
		else
			invalid_char_array(i) <= invalid_char_array(i);
		end if;
	end if;
end process;

INVALID_CHAR_CTR_OUT((i + 1) * 16 - 1 downto i * 16) <= invalid_char_array(i);
end generate invalid_ctrs_gen;

--################### UGLY LINKS MAPPING

DATA_OUT(1 * 8 - 1 downto 0 * 8) <= ddr_data_from_bank17(1 * 8 - 1 downto 0 * 8);
DATA_OUT(2 * 8 - 1 downto 1 * 8) <= ddr_data_from_bank32(1 * 8 - 1 downto 0 * 8);
DATA_OUT(3 * 8 - 1 downto 2 * 8) <= ddr_data_from_bank32(2 * 8 - 1 downto 1 * 8);
DATA_OUT(4 * 8 - 1 downto 3 * 8) <= ddr_data_from_bank32(3 * 8 - 1 downto 2 * 8);
DATA_OUT(5 * 8 - 1 downto 4 * 8) <= ddr_data_from_bank32(4 * 8 - 1 downto 3 * 8);
DATA_OUT(6 * 8 - 1 downto 5 * 8) <= ddr_data_from_bank32(5 * 8 - 1 downto 4 * 8);
DATA_OUT(7 * 8 - 1 downto 6 * 8) <= ddr_data_from_bank32(6 * 8 - 1 downto 5 * 8);
DATA_OUT(8 * 8 - 1 downto 7 * 8) <= ddr_data_from_bank32(7 * 8 - 1 downto 6 * 8);
 
data_dv(0) <= ddr_dv_from_bank17(0);
data_dv(1) <= ddr_dv_from_bank32(0);
data_dv(2) <= ddr_dv_from_bank32(1);
data_dv(3) <= ddr_dv_from_bank32(2);
data_dv(4) <= ddr_dv_from_bank32(3);
data_dv(5) <= ddr_dv_from_bank32(4);
data_dv(6) <= ddr_dv_from_bank32(5);
data_dv(7) <= ddr_dv_from_bank32(6);

DATA_DV_OUT(0) <= data_dv(0);
DATA_DV_OUT(1) <= data_dv(1);
DATA_DV_OUT(2) <= data_dv(4);
DATA_DV_OUT(3) <= data_dv(2);
DATA_DV_OUT(4) <= data_dv(3);
DATA_DV_OUT(5) <= data_dv(5);
DATA_DV_OUT(6) <= data_dv(6);
DATA_DV_OUT(7) <= data_dv(7);

DATA_KCTRL_OUT(0) <= ddr_kctrl_from_bank17(0);
DATA_KCTRL_OUT(1) <= ddr_kctrl_from_bank32(0);
DATA_KCTRL_OUT(2) <= ddr_kctrl_from_bank32(1);
DATA_KCTRL_OUT(3) <= ddr_kctrl_from_bank32(2);
DATA_KCTRL_OUT(4) <= ddr_kctrl_from_bank32(3);
DATA_KCTRL_OUT(5) <= ddr_kctrl_from_bank32(4);
DATA_KCTRL_OUT(6) <= ddr_kctrl_from_bank32(5);
DATA_KCTRL_OUT(7) <= ddr_kctrl_from_bank32(6);

links_synced_u1(0) <= ddr_receivers_synced_bank17(0);
links_synced_u1(1) <= ddr_receivers_synced_bank32(0);
links_synced_u1(2) <= ddr_receivers_synced_bank32(1);
links_synced_u1(3) <= ddr_receivers_synced_bank32(2);
links_synced_u1(4) <= ddr_receivers_synced_bank32(3);
links_synced_u1(5) <= ddr_receivers_synced_bank32(4);
links_synced_u1(6) <= ddr_receivers_synced_bank32(5);
links_synced_u1(7) <= ddr_receivers_synced_bank32(6);

code_err(0) <= ddr_code_err_from_bank17(0);
code_err(1) <= ddr_code_err_from_bank32(0);
code_err(2) <= ddr_code_err_from_bank32(1);
code_err(3) <= ddr_code_err_from_bank32(2);
code_err(4) <= ddr_code_err_from_bank32(3);
code_err(5) <= ddr_code_err_from_bank32(4);
code_err(6) <= ddr_code_err_from_bank32(5);
code_err(7) <= ddr_code_err_from_bank32(6);

disp_err(0) <= ddr_disp_err_from_bank17(0);
disp_err(1) <= ddr_disp_err_from_bank32(0);
disp_err(2) <= ddr_disp_err_from_bank32(1);
disp_err(3) <= ddr_disp_err_from_bank32(2);
disp_err(4) <= ddr_disp_err_from_bank32(3);
disp_err(5) <= ddr_disp_err_from_bank32(4);
disp_err(6) <= ddr_disp_err_from_bank32(5);
disp_err(7) <= ddr_disp_err_from_bank32(6);

end rtl;
