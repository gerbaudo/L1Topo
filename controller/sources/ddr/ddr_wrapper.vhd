library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_UNSIGNED.all;
use IEEE.NUMERIC_STD.all;
library UNISIM;
use UNISIM.VComponents.all;
use work.ipbus.all;
use work.rod_l1_topo_types_const.all;

entity ddr_wrapper is
  generic (
    SIMULATION : boolean              := false;
    VIVADO     : boolean              := false;
    PROCESSOR  : integer range 0 to 1 := 0;
    DELAY_GROUP_NAME : string;
    DELAY_VALUE : integer
    
    );
  port(
    GCK40_IN          : in std_logic;
    CLK_LOCKED_IN     : in std_logic;
    DELAY_CTRL_CLK_IN : in std_logic;
    DDR_RST_IN        : in std_logic;

    EXT_DDR_CLK_IN    : in std_logic;
    EXT_DDR_CLK_X8_IN_IO : in std_logic;
    EXT_DDR_CLK_X8_IN_R : in std_logic;
    
    DDR_DATA_IN_P       : in std_logic_vector(7 downto 0);
    DDR_DATA_IN_N       : in std_logic_vector(7 downto 0);
    
    DATA_OUT       : out std_logic_vector(8 * 8 - 1 downto 0);
    DATA_DV_OUT    : out std_logic_vector(7 downto 0);

    DELAY_VALS_IN  : in std_logic_vector(39 downto 0); 
    DELAY_LOAD_IN  : in std_logic_vector(7 downto 0);
    DELAY_VALS_OUT : out std_logic_vector(39 downto 0);
    
    TRANSMITTED_BYTES_CTR_OUT : out std_logic_vector(63 downto 0);
    INVALID_CHAR_CTR_OUT      : out std_logic_vector(63 downto 0)
    );
end ddr_wrapper;

architecture rtl of ddr_wrapper is
  
  signal ones                : std_logic_vector(7 downto 0);
  signal local_data_lines    : std_logic_vector(7 downto 0);
  signal codeErr             : std_logic_vector(8-1 downto 0);
  signal errorCounter        : std_logic_vector(8 * 8 - 1 downto 0); 
  type   array4 is array (natural range <>) of std_logic_vector(3 downto 0);
  signal bitslipErrorCounter : array4(8-1 downto 0);
  signal bitslip             : std_logic_vector(8-1 downto 0);
  signal charIsKInAnded      : std_logic_vector(8 downto 0);
  signal charIsKInOred       : std_logic_vector(8 downto 0);
  signal charIsKIn           : std_logic_vector(8 downto 0);
  signal codeErrOred         : std_logic_vector(8 downto 0);

  signal charIsBofOred  : std_logic_vector(8 downto 0);
  signal charIsBofAnded : std_logic_vector(8 downto 0);
  signal DATA_OUT_l     : std_logic_vector(63 downto 0);
  constant invertU1 : std_logic_vector(7 downto 0) := x"45";
  constant invertU2 : std_logic_vector(7 downto 0) := x"02";
  signal invert : std_logic_vector(7 downto 0);
  type bytes_cntr_array is array (0 to 7) of unsigned(7 downto 0);
  signal bytes_cntr : bytes_cntr_array  := (others => (others => '0'));
  
begin
  
  ones <= (others => '1');

  PROCESSOR_SEL0: if PROCESSOR = 0 generate
    invert <= invertU1;
  end generate PROCESSOR_SEL0;

  PROCESSOR_SEL1: if PROCESSOR = 1 generate
    invert <= invertU2;
  end generate PROCESSOR_SEL1;
  

  GENERATE_DDR_BUFFERS: for i in 0 to 7 generate
      l0_buf : IBUFDS port map (I => DDR_DATA_IN_P(i), IB =>DDR_DATA_IN_N (i), O => local_data_lines(i));
  end generate GENERATE_DDR_BUFFERS;

  --ddr_deserializer_gen : for i in 0 to 7 generate
  --  --bank32_gen : if PROCESSOR = 0 generate
  --    deserializer : entity work.ddr_deserializer
  --      generic map(
  --        DELAY_GROUP_NAME => DELAY_GROUP_NAME,
  --        DELAY_VALUE => DELAY_VALUE
  --        )
  --      port map(
  --        sysclk         => GCK40_IN,  -- not used in this component, originally used for ipbus packets sync
  --        parallelclk    => EXT_DDR_CLK_IN,
  --        serialclk_io      => EXT_DDR_CLK_X8_IN_IO,
  --        serialclk_r       => EXT_DDR_CLK_X8_IN_R,
  --        mmcm_locked    => CLK_LOCKED_IN,
  --        serialData     => local_data_lines(i),
  --        idelayValueIn  => DELAY_VALS_IN(i*5+4 downto i*5),
  --        idelayLoad     => DELAY_LOAD_IN(i),
  --        idelayInc      => '0',  -- writing in only fixed values, no increments
  --        idelayValueOut => DELAY_VALS_OUT(i*5+4 downto i*5),
  --        bitslip        => bitslip(i),
  --        decodedData    => DATA_OUT_l(i*8+7 downto i*8),
  --        charIsK        => charIsKIn(i),
  --        codeErr        => codeErr(i),
  --        inverted       => invert(i)
  --        );
  --  --end generate;

  --  charIsKInAnded(i) <= charIsKInAnded(i+1) and charIsKIn(i);
  --  charIsKInOred(i)  <= charIsKInOred(i+1) or charIsKIn(i);
  --  codeErrOred(i)    <= codeErrOred(i+1) or codeErr(i);

  --  process(EXT_DDR_CLK_IN)
  --  begin
  --    if rising_edge(EXT_DDR_CLK_IN) then
  --      if codeErr(i) = '1' then
  --        bitslipErrorCounter(i) <= std_logic_vector(unsigned(bitslipErrorCounter(i)) + 1);
  --      else
  --        bitslipErrorCounter(i) <= "0000";
  --      end if;
        
  --      if DDR_RST_IN = '1' then
  --        errorCounter(i*8 + 7 downto i*8) <= (others => '0');
  --      elsif codeErr(i) = '1' then
  --        errorCounter(i*8 + 7 downto i*8) <= std_logic_vector(unsigned(errorCounter(i*8 + 7 downto i*8) + 1));
  --      else
  --        errorCounter(i*8 + 7 downto i*8) <= errorCounter(i*8 + 7 downto i*8);
  --      end if;   
  --    end if;
  --  end process;
  --  bitslip(i) <= '1' when bitslipErrorCounter(i) = "1111" else '0';


      
  --  COUNT_TRANSFERRED_DATA: process (EXT_DDR_CLK_IN)
  --  begin  -- process COUNT_TRANSFERRED_DATA
  --    if rising_edge(EXT_DDR_CLK_IN) then
  --      if DDR_RST_IN = '1' then
  --        bytes_cntr(i) <= (others => '0');
  --      elsif charIsKIn(i) = '0' and codeErr(i) = '0' then
  --        bytes_cntr(i) <= bytes_cntr(i) + 1;
  --      else
  --        bytes_cntr(i) <= bytes_cntr(i);
  --      end if;
  --    end if;
  --  end process COUNT_TRANSFERRED_DATA;
    
  --  process(EXT_DDR_CLK_IN)
  --  begin
  --    if rising_edge(EXT_DDR_CLK_IN) then
  --      DATA_OUT((i+1)*8-1 downto i*8) <= DATA_OUT_l((i+1)*8-1 downto i*8);
  --      if (DATA_OUT_l((i+1)*8-1 downto i*8) = x"5C" and charIsKIn(i) = '1') then
  --        DATA_DV_OUT(i)    <= '0';
  --      else
  --        DATA_DV_OUT(i)    <= (not charIsKIn(i));
  --      end if;
  --    end if;
  --  end process;
  --end generate;

  --INVALID_CHAR_CTR_OUT <= errorCounter;
  --SEND_CNTR_OF_BYTES: for i in 0 to 7 generate
  --  TRANSMITTED_BYTES_CTR_OUT((i+1)*8-1 downto i*8) <= std_logic_vector(bytes_cntr(i));
  --end generate SEND_CNTR_OF_BYTES;

end rtl;
