----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:05:52 08/29/2014 
-- Design Name: 
-- Module Name:    ipbusBridge_deserializer - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

use work.decode_8b10b_pkg.all;


entity ddr_deserializer is
	 generic (
		DELAY_GROUP_NAME : string := "delay_group";
                DELAY_VALUE : integer
	 );
    port( 
		sysclk			: in  std_logic;
		serialclk_io 		: in  std_logic;
                serialclk_r 		: in  std_logic;
		parallelclk 	: in  std_logic;
		mmcm_locked 	: in  std_logic;
		serialData 		: in  std_logic;
		idelayValueIn 	: in  std_logic_vector(4 downto 0);
		idelayLoad		: in  std_logic;
		idelayInc		: in  std_logic;
		idelayValueOut : out std_logic_vector(4 downto 0);
		bitslip			: in  std_logic;
		decodedData 	: out std_logic_vector(7 downto 0);
		charIsK 			: out std_logic;
		codeErr 			: out std_logic;
                inverted : in std_logic
	 );
	
end ddr_deserializer;

architecture Behavioral of ddr_deserializer is

  attribute shreg_extract: string;
  signal reset: std_logic;
  signal idelayValue_reg: std_logic_vector(4 downto 0);
  signal idelayValueChanged: std_logic;
  signal idelayLoadMuxed: std_logic;
  signal delayedData: std_logic;
  signal iddr_q1, iddr_q2: std_logic;
  signal dataShiftReg, dataShiftReg_synch: std_logic_vector(9 downto 0);
  signal deserialisedDataBuffer: std_logic_vector(18 downto 0);
  attribute shreg_extract of deserialisedDataBuffer: signal is "no";
  signal bitslipCounter: std_logic_vector(3 downto 0) := x"0";
  signal synchronisedData, synchronisedData_corr: std_logic_vector(9 downto 0);
  signal charNotInterpretable: std_logic;
  signal dispErr: std_logic;

begin

  reset <= not mmcm_locked;
  
  process(sysclk) begin
    if rising_edge(sysclk) then
      idelayValue_reg <= idelayValueIn;
      if idelayValue_reg /= idelayValueIn then idelayValueChanged <= '1';
      else idelayValueChanged <= '0';
      end if;			
    end if;
  end process;
  idelayLoadMuxed <= idelayLoad or idelayValueChanged;


  ipbusBridge_idelay : IDELAYE2
    generic map (
      CINVCTRL_SEL           => "FALSE",          -- TRUE, FALSE
      DELAY_SRC              => "IDATAIN",        -- IDATAIN, DATAIN
      HIGH_PERFORMANCE_MODE  => "TRUE",           -- TRUE, FALSE
      IDELAY_TYPE            => "VAR_LOAD",   --"FIXED",          -- FIXED, VARIABLE, or VAR_LOAD
      IDELAY_VALUE           => DELAY_VALUE,      --15U1,                -- 0 to 31
      REFCLK_FREQUENCY       => 200.4,
      PIPE_SEL               => "FALSE",
      SIGNAL_PATTERN         => "DATA"           -- CLOCK, DATA
      )
    port map (
      IDATAIN                => serialData, -- Driven by IOB
      DATAOUT                => delayedData,
      C                      => sysclk,
      CE                     => idelayInc, --IN_DELAY_DATA_CE,
      INC                    => '1', --IN_DELAY_DATA_INC,
      LD                     => idelayLoadMuxed,
      CNTVALUEIN             => idelayValueIn, --IN_DELAY_TAP_IN,
      CNTVALUEOUT            => idelayValueOut, --IN_DELAY_TAP_OUT,
      --unused ports
      DATAIN                 => '0', -- Data from FPGA logic
      REGRST                 => '0',
      LDPIPEEN               => '0',
      CINVCTRL               => '0'
      );

  IDDR_inst : IDDR
    generic map (
      DDR_CLK_EDGE => "SAME_EDGE_PIPELINED", -- "OPPOSITE_EDGE", "SAME_EDGE"
      -- or "SAME_EDGE_PIPELINED"
      INIT_Q1 => '0', -- Initial value of Q1: '0' or '1'
      INIT_Q2 => '0', -- Initial value of Q2: '0' or '1'
      SRTYPE => "SYNC") -- Set/Reset type: "SYNC" or "ASYNC"
    port map (
      Q1 => iddr_q1, -- 1-bit output for positive edge of clock
      Q2 => iddr_q2, -- 1-bit output for negative edge of clock
      C => serialclk_io,
      -- 1-bit clock input
      CE => '1', -- 1-bit clock enable input
      D => delayedData,
      -- 1-bit DDR data input
      R => reset,
      -- 1-bit reset
      S => '0'
      -- 1-bit set
      );

  process(serialclk_r) begin
    if rising_edge(serialclk_r) then
      dataShiftReg(0) <= iddr_q2;
      dataShiftReg(1) <= iddr_q1;
      dataShiftReg(9 downto 2) <= dataShiftReg(7 downto 0);
      dataShiftReg_synch <= dataShiftReg;
    end if;
  end process;

  process(parallelclk) 
    variable pointer: natural;
  begin
    if rising_edge(parallelclk) then
      deserialisedDataBuffer(9 downto 0) <= dataShiftReg_synch;
      deserialisedDataBuffer(18 downto 10) <= deserialisedDataBuffer(8 downto 0);
       if bitslip='1' then 
        if bitslipCounter="1001" then bitslipCounter <= "0000";
        else bitslipCounter <= std_logic_vector( unsigned(bitslipCounter) +1 );
        end if;
      else bitslipCounter <= bitslipCounter;
      end if;
      pointer := to_integer( unsigned(bitslipCounter) );
      synchronisedData <= deserialisedDataBuffer(pointer+9 downto pointer);
    end if;
  end process;

  INVERT_SIGNAL_FOR_INVERTED_PINS: process (parallelclk)
  begin 
    if rising_edge(parallelclk) then
      if inverted = '1' then
        synchronisedData_corr <= not synchronisedData;
      else
        synchronisedData_corr <= synchronisedData;
      end if;
    end if;
  end process INVERT_SIGNAL_FOR_INVERTED_PINS;


  decoder: entity work.decode_8b10b_lut_base 
    generic map(
      C_HAS_CODE_ERR => 1,
      C_HAS_DISP_ERR => 1
      )
    port map(
      CLK => parallelclk,
      DIN => synchronisedData_corr,
      DOUT => decodedData,
      KOUT => charIsK,
      CE => '1',
      CODE_ERR => charNotInterpretable,
      DISP_ERR => dispErr,
      ND => open,
      RUN_DISP => open,
      SYM_DISP => open
      );
  codeErr <= charNotInterpretable or dispErr;

end Behavioral;

