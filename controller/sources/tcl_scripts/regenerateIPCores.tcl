proc buildcore {f} {
    puts "\n\n*** Building core: $f"
    set bname [exec basename $f]
#    puts [format {%s%s} "bname=" $bname]
#    cd $::env(SVN_ROOT)
    set dname [exec dirname $f]
#    puts [format {%s%s} "dname=" $dname]
    cd $dname
    set new_pwd [exec pwd]
#    puts [format {%s%s} "new_pwd=" $new_pwd]
    set coregen_cmd "coregen -r -b $bname -p ../ipcores/coregen.cgp >& coregen.out"
    puts $coregen_cmd
    exec coregen -r -b $bname -p ../ipcores/coregen.cgp >& coregen.out
}

set fd_fileList [open fileList.txt r]
set fileList [read $fd_fileList]
close $fd_fileList
foreach f_line [split $fileList "\n"] {
    if {$f_line == "" || [string index $f_line 0] == "#"} {
            continue
        }
#    puts [format {%s%s} "f_line=" $f_line]
    buildcore $f_line
    }