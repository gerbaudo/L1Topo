----------------------------------------------------------------------------------
-- Engineers:			Christian Kahra,	Johannes Gutenberg - Universitaet Mainz
--		        	Marek Palka,		Uniwersytet Jagielloński w Krakowie
-- Create Date:		12:57:41 06/27/2014 
-- Project Name:	L1Topo_ControlFPGA
-- Module Name:		L1Topo-ControlFPGA_top - Behavioral 
-- Target Devices:	XC7K325t-2FFG900
-- Tool versions:	Vivado 16.4
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNISIM;
use UNISIM.VComponents.all;
use work.l1topo_package.all;
use work.rod_l1_topo_types_const.all;
use work.ipbus.all;

entity L1Topo_ControlFPGA_top is
  generic (
    MODULE_NUMBER        : natural               := 2; 
    LINKS_NUMBER         : integer range 0 to 40 := 8;
    SIMULATION           : boolean               := false;
    INT_SIM              : integer               := 0;
    VIVADO               : boolean               := false;
    NUMBER_OF_PROCESSORS : integer range 1 to 2  := 2;
    DEBUG                : integer               := 0
    );
  port (
    --Clocks
    GCK1_P, GCK1_N         : in std_logic;  --TTC clock 40.08 MHz, lack of term.!
    GCK2_P, GCK2_N         : in std_logic;  --crystal clock 40.08MHz, lack of term.!
    MGT5_CLK_P, MGT5_CLK_N : in std_logic;
    MGT7_CLK_P, MGT7_CLK_N : in std_logic;  --mgt reference clock (crystal clock 125 MHz)   
    MGT2_CLK_P, MGT2_CLK_N : in std_logic;
    --High-speed serial links, IPBus MGT (connection to ethernet phy chip on mezzanine)
    K7_MGTRX_3_P, K7_MGTRX_3_N               : in    std_logic;  --mgtxrx3_118 X0Y15 SGMII RX
    K7_MGTTX_3_P, K7_MGTTX_3_N               : out   std_logic;  --mgtxtx3_118 X0Y15 SGMII TX
    --ROS minipod fiber
    OPTO_KT1_P, OPTO_KT1_N                   : out   std_logic_vector(1 downto 0);  --mgtxtx0_117 minipod line 8  X0Y8
    OPTO_KR1_P, OPTO_KR1_N                   : in    std_logic_vector(1 downto 0);  --mgtxrx0_117 minipod line 8  X0Y8
    --TTCRx
    TTC_RESET_OUT                            : out   std_logic;  --21
    TTC_EVT_H_STR_IN                         : in    std_logic;  --44
    TTC_L1A_IN                               : in    std_logic;  --43
    TTC_BCNT_STR_IN                          : in    std_logic;  --42
    TTC_EVT_L_STR_IN                         : in    std_logic;  --41
    TTC_BCNT_IN                              : in    std_logic_vector(11 downto 0);  --29..40
    TTC_EVTCNTRRST_IN                        : in    std_logic;  --45
    TTC_BCNRST_IN                            : in    std_logic;  --46
    TTC_BRCST_IN                             : in    std_logic_vector(5 downto 0);  --47..52
    TTC_BCSTR1_IN                            : in    std_logic;  --54
    TTC_BCSTR2_IN                            : in    std_logic;  --53
    TTC_DOUT_STR_IN                          : in    std_logic;  --0
    TTC_DOUT_IN                              : in    std_logic_vector(7 downto 0);  --20..13
    TTC_SUBADDR_IN                           : in    std_logic_vector(7 downto 0);  --12..5
    TTC_CTRL_22                              : in    std_logic;  --STATUS1
    TTC_CTRL_23                              : in    std_logic;  --STATUS2
    TTC_CTRL_24                              : out   std_logic;  --I2C SCL
    TTC_CTRL_25                              : inout std_logic;  --I2C SDA
    TTC_CTRL_26                              : out   std_logic;  --P/D (TTC mode)
    TTC_CTRL_27                              : out   std_logic;  --CLKSEL            
    --ROD GTX
    ROD_GTX_RXP                              : in    std_logic_vector(1 downto 0);
    ROD_GTX_RXN                              : in    std_logic_vector(1 downto 0);
    --I2C
    AVAGO_SCL                                : out   std_logic_vector(2 downto 0);
    AVAGO_SDA                                : inout std_logic_vector(2 downto 0);
    POWER_SCL                                : out   std_logic;
    POWER_SDA                                : inout std_logic;
    ATCA_HRDW_ADDR                           : in    std_logic_vector(7 downto 0);
--              JitterCleaner
--              JC1_CTRL_1:  in         std_logic;      -- LOL
--              JC1_CTRL_2:  in         std_logic;      -- C2B
--              JC1_CTRL_3:  in         std_logic;      -- INT_C1B
--              JC1_CTRL_4:  out                std_logic;      -- A2
--              JC1_CTRL_7:  out                std_logic;      -- SDI
--              JC1_CTRL_8:  in         std_logic;      -- SDA
--              JC1_CTRL_9:  out                std_logic;      -- SCL
--              JC1_CTRL_10: out                std_logic;      -- RESET_B
--              JC1_CTRL_11: out                std_logic;      -- CMODE                                
--              SerialID-Chip DS2411 (U89)
    DS2411                                   : inout std_logic;
--U1 CTRLBUS
    CTRLBUS_U1_OUT_P, CTRLBUS_U1_OUT_N       : out   std_logic_vector(5 downto 0);    --(4- 0: ipbBridgeRequest,   5: ttcBridge)
    CTRLBUS_U1_OUT_P_8, CTRLBUS_U1_OUT_N_8   : out   std_logic;  --TTC_L1Accept
    CTRLBUS_U1_IN_P, CTRLBUS_U1_IN_N         : in    std_logic_vector(20 downto 18);  --ipbBridgeResponse
    CTRLBUS_U1_IN_P_12, CTRLBUS_U1_IN_N_12   : in    std_logic;  --GCK1
    CTRLBUS_U1_IN_P_6, CTRLBUS_U1_IN_N_6     : in    std_logic;  --ROD_Busy
--U2 CTRLBUS                
    CTRLBUS_U2_OUT_P, CTRLBUS_U2_OUT_N       : out   std_logic_vector(26 downto 21);  --(25-21: ipbBridgeRequeset, 26: ttcBridge)
    CTRLBUS_U2_OUT_P_29, CTRLBUS_U2_OUT_N_29 : out   std_logic;  --TTC_L1Accept
    CTRLBUS_U2_IN_P, CTRLBUS_U2_IN_N         : in    std_logic_vector(41 downto 39);  --ipbBridgeResponse
    CTRLBUS_U2_IN_P_33, CTRLBUS_U2_IN_N_33   : in    std_logic;  --GCK1
    CTRLBUS_U2_IN_P_27, CTRLBUS_U2_IN_N_27   : in    std_logic;  --ROD_Busy
    CTRLBUS_U2_IN_P_28, CTRLBUS_U2_IN_N_28   : in    std_logic;  --GCK2_U2
--              connections to the mezzanine board
    EXT_K7_0                                 : out   std_logic;  -- phy_reset_b
    EXT_K7_1                                 : out   std_logic;  -- led0
    EXT_K7_2                                 : out   std_logic;  -- led1
    ROD_ROIB_BUSY                            : out   std_logic;  -- EXT_K7_49
    ROD_DAQ_BUSY                             : out   std_logic;  -- EXT_K7_49
    TEST_OUT                                 : out   std_logic
    );
  
end L1Topo_ControlFPGA_top;

architecture Behavioral of L1Topo_ControlFPGA_top is

  attribute iob: string;
  signal	ip_addr:  std_logic_vector(31 downto 0);
  signal	mac_addr: std_logic_vector(47 downto 0);
  signal	ModuleID: std_logic_vector(31 downto 0) := X"00000000";
  signal	ip_addr_sel:  std_logic_vector(31 downto 0);
  signal	mac_addr_sel: std_logic_vector(47 downto 0);
  signal	ModuleID_sel: std_logic_vector(31 downto 0) := X"00000000";

  constant Serial_No_Module: std_logic_vector(31 downto 0) := X"e0160308";
  constant Serial_No_ExtensionBoard: std_logic_vector(31 downto 0) := X"e0002531";
  -- e.g. FW 2.3.1 translate as 00020301
  constant Firmware_Version: std_logic_vector(31 downto 0) := X"00050200"; --
  constant FirmwareDate:		std_logic_vector(31 downto 0) := X"23052017";
  constant FirmwareRevision: std_logic_vector(31 downto 0) := X"00003108";
  signal gck1: std_logic;
  signal gck2: std_logic;
  signal gck1_u1, gck1_u2: std_logic;
  signal gck2_u2: std_logic;
  signal mgt7_clk, mgt7_clk_buffered, mgt7_clk_rod_gtx, eth_gt_outclk: std_logic;
  signal mgt7_qplloutrefclk_out, mgt7_qplloutclk_out : std_logic;
  signal mgt2_clk : std_logic;
  signal mgt5_clk : std_logic;
  signal eth_gt_txoutclk: std_logic;
  signal sysclk40: std_logic;
  signal sysclk80: std_logic;
  signal sysclk200: std_logic;
  signal sysclk400: std_logic;
  signal ethclk62_5: std_logic;
  signal ethclk125: std_logic;
  signal gck1bufgds : std_logic;
  signal sysclk_pll_locked, sysclk_pll_locked_b: std_logic;
  signal ethclk_pll_locked: std_logic;
  signal clocksStatus: std_logic_vector(3 downto 0);
  signal eth_sgmiiphy_done: std_logic;
  signal locked: std_logic;
  signal rst_extphy: std_logic;
  signal rst_ipb, rod_rst : std_logic;
  signal counter125mhz: unsigned(27 downto 0);	
  signal pkt_rx_led: std_logic;
  signal pkt_tx_led: std_logic;
  signal ttc_Status, ttc_Status_reg: std_logic_vector(1 downto 0);
  signal xadc_control:  std_logic_vector(25 downto 0);
  signal xadc_status:	 std_logic_vector(22 downto 0);
  signal usr_access:	 std_logic_vector(31 downto 0);
  signal serialID_in:		std_logic;
  signal serialID_out:		std_logic;
  signal serialID_pulse:	std_logic;
  signal serialID_status:	std_logic_vector(48 downto 0);
--	signal jcLossOfLock, jcLossOfLock_reg: std_logic;
--	signal jcInterruptClk1Bad, jcInterruptClk1Bad_reg: std_logic;
--	signal jcSlaveSelect_b, jcSlaveSelect_b_reg: std_logic;
--	signal jcSerialDataOut, jcSerialDataOut_reg: std_logic;
--	signal jcSerialDataIn, jcSerialDataIn_reg: std_logic;
--	signal jcSerialClk, jcSerialClk_reg: std_logic;
  signal ctrlbus_idelayctrl_resetCounter:	std_logic_vector(1 downto 0);
  signal ctrlbus_idelayctrl_reset:				std_logic;
  signal ctrlbus_idelayctrl_ready:				std_logic;
  signal ctrlbus_delay_control:	arraySLV6(41 downto 0);
  signal ctrlbus_delay_pulse:   arraySLV2(41 downto 0);
  signal ctrlbus_delay_status:  arraySLV5(41 downto 0);
	
  signal ctrlbus_in_buf:			std_logic_vector(41 downto 0);
  signal ctrlbus_in_delayed:		std_logic_vector(41 downto 0);
  signal ctrlbus_in_ddr:			arraySLV2(41 downto 0);
	
  signal ctrlbus_out_invcorr:	arraySLV2(41 downto 0);
  signal ctrlbus_out_ddr:			std_logic_vector(41 downto 0);
  signal ctrlbus_out_delayed:	std_logic_vector(41 downto 0);
  signal ctrlbus_out_buf:			std_logic_vector(41 downto 0);
	
  signal ipbBridgeRequestU1:  arraySLV2(4 downto 0);
  signal ipbBridgeResponseU1: arraySLV2(2 downto 0);
  signal ipbBridgeRequestU2:  arraySLV2(4 downto 0);
  signal ipbBridgeResponseU2: arraySLV2(2 downto 0);
  
  signal ttc_brcst_reg:               std_logic_vector(5 downto 0);
  signal ttc_bcstr1_reg:              std_logic;
  signal ttc_bcnrst_reg:              std_logic;
  signal ttcBridge_inv_u1, ttcBridge_inv_u2:  std_logic;
  attribute iob of ttcBridge_inv_u1: signal is "true";
  attribute iob of ttcBridge_inv_u2: signal is "true";
  
  signal ttcBridgeControl: std_logic_vector(2 downto 0);
  
  signal rodBridgeU1: arraySLV2(7 downto 0);
  signal rodBridgeU2: arraySLV2(7 downto 0);
  
  signal i2c_scl:			std_logic_vector(4 downto 0);
  signal i2c_scl_obuft:	std_logic_vector(4 downto 0);
  signal i2c_sda_in:		std_logic_vector(4 downto 0);
  signal i2c_sda_out:		std_logic_vector(4 downto 0);
  signal i2c_sda_iobuf:	std_logic_vector(4 downto 0);
	
  signal GeneralControl: std_logic_vector(31 downto 0);
  signal GeneralPulse:   std_logic_vector(31 downto 0);
	
  signal rod_control_registers: rod_control_registers_array := (others => (others => '0'));
  signal rod_status_registers, rod_wrapper_status_registers:  rod_status_registers_array;
  signal main_busy : std_logic;

  signal number_of_slices_out_l           : slice_parameters_array_u;
  signal lvl0_offset_out_l                : slice_parameters_array_u;
  signal data_out_l						: out_data_array;
  signal actual_bus_number_out_l          : bus_number_array;
  
  signal ones : std_logic_vector(LINKS_NUMBER - 1 downto 0);
  signal rst_from_bank18  : std_logic;

  signal rst_from_bank17, rst_from_bank32       : std_logic;
  
  signal ttc_rst : std_logic;
  
  signal start_of_frame_l, end_of_frame_l : std_logic;
  
  signal slink_ready_in_l, slink_event_ready_out_l : std_logic_vector(NUMBER_OF_OUTPUT_LINKS-1 downto 0);
  type slink_data_out_array is array (0 to NUMBER_OF_OUTPUT_LINKS-1) of std_logic_vector(31 downto 0);
  signal slink_data_out_a : slink_data_out_array;
  
  signal busy_from_u2, busy_from_u1 : std_logic := '0';
  signal builder_busy_l : std_logic;
  signal l1_busy_l : std_logic;
  signal slink_status : std_logic_vector(47 downto 0);
  
  signal icon_control0, icon_control1, icon_control : std_logic_vector(35 downto 0);
  signal ila_trg_0, ila_trg_1  : std_logic_vector(255 downto 0);
  
  signal ldown_n, lff_n : std_logic;

  signal slink_rst : std_logic;
  signal slink_rst_vector, slink_rst_tmp : std_logic_vector(31 downto 0);
  
  signal dbg_txd, dbg_rxd : std_logic_vector(15 downto 0);
  signal dbg_tx_en, dbg_tx_er, dbg_rx_dv, dbg_rx_er : std_logic;
  
  signal ttc_l1a, not_ttc_l1a  : std_logic;
  signal ttc_l1a_U1, ttc_l1a_U2            : std_logic;
  attribute iob of ttc_l1a_U1, ttc_l1a_U2  : signal is "true";
  signal ttc_l1a_in_sync, ttc_l1a_in_pulse : std_logic := '0';
  signal ttc_bcid                          : std_logic_vector(11 downto 0);
  signal ttc_evtid                         : std_logic_vector(23 downto 0);
  signal ecr                               : std_logic_vector(7 downto 0);
  signal bcn                               : std_logic_vector(11 downto 0);

  signal rod_dbg : std_logic_vector(255 downto 0);

  signal test_ctr : std_logic_vector(31 downto 0);

  signal l1_pulse_from_ipbus    : std_logic;
  signal fake_l1a, l1a_from_ttc : std_logic;
  signal l1a_frequency          : integer range 0 to 16777214;  -- 24 bits


  signal orbit_ctr : std_logic_vector(23 downto 0);

  ---------REGISTER SIGNALS
  --slink
  signal slink_status_reg               : std_logic_vector(31 downto 0);
  signal slink_format_verison_ros_reg   : std_logic_vector(31 downto 0) := x"03011002";
  signal slink_format_verison_roib_reg  : std_logic_vector(31 downto 0) := x"03011002";
  signal slink_busy_cnt_time_period_reg : std_logic_vector(31 downto 0);
  signal slink_busy_cnt_reg             : std_logic_vector(31 downto 0);
  signal slink_idle_cnt_reg             : std_logic_vector(31 downto 0);
  signal slink_disable_reg              : std_logic_vector(31 downto 0);
  --ddr
  signal l1busy                         : std_logic;

  -- run control
  signal gen_1a               : std_logic_vector(31 downto 0) := (others => '0');
  signal run_type_nbr_reg     : std_logic_vector(31 downto 0) := (others => '0');
  signal trg_type_reg         : std_logic_vector(31 downto 0) := (others => '0');
  signal subdet_module_id_reg : std_logic_vector(31 downto 0) := (others => '0');
  signal rod_sys_fw_ver_reg   : std_logic_vector(31 downto 0) := x"0123_abcd";
  signal ecrid_rst_reg        : std_logic_vector(31 downto 0) := (others => '0');
  signal ecrid_set_reg        : std_logic_vector(31 downto 0) := (others => '0');
  signal l1id_rst_reg         : std_logic_vector(31 downto 0) := (others => '0');
  signal trg_timeout_reg      : std_logic_vector(31 downto 0) := (others => '0');
  signal orbit_wrap_reg       : std_logic_vector(31 downto 0) := (others => '0');
  signal orbit_rst_reg        : std_logic_vector(31 downto 0) := (others => '0');

  signal seq_type_reg : std_logic_vector(31 downto 0) := (others => '0');


  --rod registers
  signal trigger_type    : std_logic_vector(7 downto 0);
  signal trg_timeout_ctr : std_logic_vector(31 downto 0) := (others => '0');



  signal debugIPBusBridgeU2         : std_logic_vector(242 downto 0);
  signal debugIPBus                 : std_logic_vector(99 downto 0);
  signal iconControl0, iconControl1 : std_logic_vector(35 downto 0);

  signal ddr_err_ctr_reg_q : std_logic_vector(127 downto 0);
  signal status1, status2  : std_logic_vector(31 downto 0);
  signal bcn_mismatch, timeout, data_transport_error, rod_fifo_overflow, lvds_link_error, cmm_parity_error, glink_error, limited_roi_set, trigger_timeout : std_logic;
  signal trigger_type_en   : std_logic;

  signal onehz       : std_logic;
  signal ttc_int_rst : std_logic;
--        attribute box_type of ipbus_module: component is "black_box";
  signal local_bcid  : std_logic_vector(11 downto 0);
  signal slink_up    : std_logic_vector(NUMBER_OF_OUTPUT_LINKS - 1 downto 0);

  signal main_daq_busy, main_roib_busy          : std_logic;
  attribute IODELAY_GROUP                       : string;
  attribute IODELAY_GROUP of CTRLBUS_IDELAYCTRL : label is "CTRLBUS_IODELAYGROUP";
  signal l1busy_daq, l1busy_roib                : std_logic;
  signal sysclk160                              : std_logic;
  signal cntr_debug_l, cntr_debug_lsync         : cntr_debug_array;
  signal link_enabl_sync, rod_gtx_soft_reset    : std_logic := '0';

  type reset_cntr_array is array (0 to 3) of unsigned(7 downto 0);
  signal reset_cntr_a                     : reset_cntr_array := (others => (others => '0'));
  signal rod_rst_synch, ttc_int_rst_synch : std_logic        := '0';
  signal cr_clk40, cr_clk80, cr_clk160    : std_logic;
  signal atca_hrdw_addr_sync : std_logic_vector(7 downto 0) := (others => '0');
begin

  CRISTAL_CLK_MMCM_INST: entity work.CRISTAL_CLK_MMCM
    port map(
      cristal_clk_in_n => CTRLBUS_U2_IN_N_28,
      cristal_clk_in_p => CTRLBUS_U2_IN_P_28,
      cr_clk40  => cr_clk40,
      cr_clk80  => cr_clk80,
      cr_clk160 => cr_clk160,
      locked    => rod_status_registers(35)(31)
      );
  
  TTC_RESET_OUT <= '1';                 --active low
  ones <= (others => '1');
--KINTEX_CPLD(0) <= '1';
  
L1Topo00_GEN: if MODULE_NUMBER=0 generate 
	ip_addr_sel  <= X"0a914410";
	mac_addr_sel <= X"080030002600";
	ModuleID_sel <= X"00000000";
end generate;              
L1Topo01_GEN: if MODULE_NUMBER=1 generate 
	ip_addr_sel  <= X"0a914411";
	mac_addr_sel <= X"080030002601";
	ModuleID_sel <= X"00000001";
end generate;
L1Topo02_GEN: if MODULE_NUMBER=2 generate 
	ip_addr_sel  <= X"ac1a68d6"; --172.26.104.214 
	mac_addr_sel <= X"000A3501F610"; -- Mainzer Xilinx ML605-board00-0A-35-01-F6-10
	ModuleID_sel <= X"00000002";
end generate;

ATCA_ILA: entity work.atca_ila
  port map( clk => cr_clk40, probe0 => ATCA_HRDW_ADDR);
              
SELECT_MAC_IP_ID: process(cr_clk40)
begin
  if rising_edge(cr_clk40) then
    case ATCA_HRDW_ADDR(3 downto 0) is
      when x"2" => --module 0, 0xd2 slot 2 USA 15
        ip_addr	<= X"0a914410";
	mac_addr <= X"080030002600";
	ModuleID <= X"00000000";
      when x"4" => --module 1,0x54 slot 4 USA 15
        ip_addr	<= X"0a914411";
	mac_addr <= X"080030002601";
	ModuleID <= X"00000001";
--      when x"c" => --module 2, 0xcc slot 3 testrig 
--        ip_addr	<= X"ac1a68d6"; --172.26.104.214 
--	mac_addr <= X"000A3501F610"; 
--	ModuleID <= X"00000002";
      when x"3" => --module 2, 0x4b slot 4 testrig 
        ip_addr	<= X"ac1a68d6"; --172.26.104.214 
	mac_addr <= X"000A3501F610"; 
	ModuleID <= X"00000002";
      when others =>
        ip_addr <= ip_addr_sel;
        mac_addr <= mac_addr_sel;
        ModuleID <= ModuleID_sel;
    end case;
  end if;
end process SELECT_MAC_IP_ID;
--input mapping


--	ttc_L1Accept <= TTC_CTRL_43;
--	ttc_EventCounterReset <= TTC_CTRL_45;
--	ttc_BunchCounterReset <= TTC_CTRL_46;
--	ttc_Broadcast <= TTC_CTRL_47 & TTC_CTRL_48 & TTC_CTRL_49 & TTC_CTRL_50 & TTC_CTRL_51 & TTC_CTRL_52;
--	ttc_BroadcastStrobe1 <= TTC_CTRL_53;
--	ttc_BroadcastStrobe2 <= TTC_CTRL_54;
	ttc_Status(0) <= TTC_CTRL_22;
	ttc_Status(1) <= TTC_CTRL_23;

	--jcLossOfLock 		 <= JC1_CTRL_1;
	--jcInterruptClk1Bad <= JC1_CTRL_3;
	--jcSerialDataIn 	 <= JC1_CTRL_8;

--GCK1_IBUFGDS: IBUFGDS port map(I  => GCK1_P, IB => GCK1_N, O  => gck1bufgds);
--GLOBAL_BUF: BUFG port map(I => gck1bufgds, O => gck1);        
--GCK2_IBUFGDS: IBUFGDS port map(I  => GCK2_P, IB => GCK2_N, O  => gck2);
--GLOBAL_BUF: BUFG port map(I => gck1_u1, O => gck1);
GCK1_U2_IBUFGDS: IBUFGDS port map(I => CTRLBUS_U2_IN_P_33, IB => CTRLBUS_U2_IN_N_33, O => gck1_u2);



MGT5_IBUFGDS_GTE2: IBUFDS_GTE2 
generic map(
	CLKRCV_TRST => true,
	CLKCM_CFG	=> true,
	CLKSWING_CFG => "11"
)
port map(
	I   => MGT5_CLK_P,
	IB  => MGT5_CLK_N,
	O   => mgt5_clk,
	ceb => '0'
);

MGT2_IBUFGDS_GTE2: IBUFDS_GTE2 
generic map(
	CLKRCV_TRST => true,
	CLKCM_CFG	=> true,
	CLKSWING_CFG => "11"
)
port map(
	I   => MGT2_CLK_P,
	IB  => MGT2_CLK_N,
	O   => mgt2_clk,
	ceb => '0'
);


process(sysclk40)
begin
  if rising_edge(sysclk40) then
    if sysclk_pll_locked='0' then
      ctrlbus_idelayctrl_resetCounter <= "00";
    elsif ctrlbus_idelayctrl_resetCounter /= "11" then
      ctrlbus_idelayctrl_resetCounter <= std_logic_vector( unsigned(ctrlbus_idelayctrl_resetCounter) + 1);
    end if;
  end if;
end process;
ctrlbus_idelayctrl_reset <= '1' when ctrlbus_idelayctrl_resetCounter /= "11" else '0';

CTRLBUS_IDELAYCTRL: IDELAYCTRL port map(REFCLK => sysclk200, RST => ctrlbus_idelayctrl_reset, RDY => ctrlbus_idelayctrl_ready);
clocksStatus(3) <= ctrlbus_idelayctrl_ready;

--------------------------------------------------------------------------------
--  CTRLBUS
--------------------------------------------------------------------------------


CTRLBUS_IN_GEN: for l in 41 downto 0 generate

CTRLBUS_U1_IBUFDS_6_GEN: if l=6 generate
CTRLBUS_U1_IBUFDS_6: IBUFDS
port map(I  => CTRLBUS_U1_IN_P_6,IB => CTRLBUS_U1_IN_N_6,O  => ctrlbus_in_buf(6));
end generate CTRLBUS_U1_IBUFDS_6_GEN;

CTRLBUS_U1_IBUFDS_GEN: if (l>=18 and l<=20) generate
CTRLBUS_U1_IBUFDS: IBUFDS
port map(I  => CTRLBUS_U1_IN_P(l),IB => CTRLBUS_U1_IN_N(l),O  => ctrlbus_in_buf(l));
end generate CTRLBUS_U1_IBUFDS_GEN;

CTRLBUS_U2_IBUFDS_27_GEN: if l=27 generate
CTRLBUS_U2_IBUFDS_27: IBUFDS
port map(I  => CTRLBUS_U2_IN_P_27,IB => CTRLBUS_U2_IN_N_27,O  => ctrlbus_in_buf(27));
end generate CTRLBUS_U2_IBUFDS_27_GEN;

CTRLBUS_U2_IBUFDS_GEN: if l>=39 generate
CTRLBUS_U2_IBUFDS: IBUFDS
port map(I  => CTRLBUS_U2_IN_P(l),IB => CTRLBUS_U2_IN_N(l),O  => ctrlbus_in_buf(l));
end generate CTRLBUS_U2_IBUFDS_GEN;

CTRLBUS_IN_DDR_GEN: if (l>=18 and l<=20) or l>=39 generate
  attribute IODELAY_GROUP of CTRLBUS_IDELAY:  label is "CTRLBUS_IODELAYGROUP";
begin
CTRLBUS_IDELAY: IDELAYE2
generic map (
  REFCLK_FREQUENCY			=> 200.4,
  IDELAY_TYPE					=> "VAR_LOAD",
  PIPE_SEL						=> "FALSE",
  IDELAY_VALUE				=> 0,
  DELAY_SRC					=> "IDATAIN",
  HIGH_PERFORMANCE_MODE	=> "TRUE",
  SIGNAL_PATTERN				=> "DATA",
  CINVCTRL_SEL				=> "FALSE"
)
port map (
  IDATAIN			=> ctrlbus_in_buf(l),
  DATAOUT			=> ctrlbus_in_delayed(l),
  CNTVALUEIN		=> ctrlbus_delay_control(l)(4 downto 0),
  LD					=> ctrlbus_delay_pulse(l)(0),
  CNTVALUEOUT		=> ctrlbus_delay_status(l),
  C					=> sysclk40,
  CE					=> ctrlbus_delay_pulse(l)(1),
  INC				=> ctrlbus_delay_control(l)(5),
  DATAIN			=> '0',
  REGRST			=> '0',
  LDPIPEEN			=> '0',
  CINVCTRL			=> '0'
);

CTRLBUS_IDDR: IDDR
generic map(
	DDR_CLK_EDGE => "SAME_EDGE_PIPELINED",
	INIT_Q1 => '0',
	INIT_Q2 => '0',
	SRTYPE => "SYNC"
)
port map(
	C	=> sysclk400,
	R	=> sysclk_pll_locked_b,
	D	=> ctrlbus_in_delayed(l),
	Q1	=> ctrlbus_in_ddr(l)(0),
	Q2	=> ctrlbus_in_ddr(l)(1),
	CE	=> '1',
	S	=> '0'
);

end generate CTRLBUS_IN_DDR_GEN;
end generate CTRLBUS_IN_GEN;

ctrlbus_out_invcorr(0) 	<= not ipbBridgeRequestU1(0);
ctrlbus_out_invcorr(1) 	<= not ipbBridgeRequestU1(1);
ctrlbus_out_invcorr(2) 	<= not ipbBridgeRequestU1(2);
ctrlbus_out_invcorr(3) 	<= not ipbBridgeRequestU1(3);
ctrlbus_out_invcorr(4) 	<= not ipbBridgeRequestU1(4);	
ctrlbus_out_invcorr(5)(0) <= ttcBridge_inv_u1;
busy_from_u1 <= not ctrlbus_in_buf( 6);
ipbBridgeResponseU1(0)		<=     ctrlbus_in_ddr(18);
ipbBridgeResponseU1(1)		<= not ctrlbus_in_ddr(19);
ipbBridgeResponseU1(2)		<= not ctrlbus_in_ddr(20);
ctrlbus_out_invcorr(21) 	<= not ipbBridgeRequestU2(0);
ctrlbus_out_invcorr(22) 	<=     ipbBridgeRequestU2(1);
ctrlbus_out_invcorr(23) 	<= not ipbBridgeRequestU2(2);
ctrlbus_out_invcorr(24) 	<=     ipbBridgeRequestU2(3);
ctrlbus_out_invcorr(25) 	<= not ipbBridgeRequestU2(4);
ctrlbus_out_invcorr(26)(0) 	<= ttcBridge_inv_u2;
busy_from_u2					<=     ctrlbus_in_buf(27);
ipbBridgeResponseU2(0)  	<= not ctrlbus_in_ddr(39);
ipbBridgeResponseU2(1)  	<= not ctrlbus_in_ddr(40);
ipbBridgeResponseU2(2)  	<= not ctrlbus_in_ddr(41);

CTRLBUS_OUT_GEN: for l in 41 downto 0 generate
  CTRLBUS_OUT_DDR_GEN: if l<=4 or (l>=21 and l<=25) generate
    attribute IODELAY_GROUP of CTRLBUS_ODELAY:  label is "CTRLBUS_IODELAYGROUP";
  begin

    CTRLBUS_ODDR400_GEN: if l<=4 or (l>=21 and l<=25) generate
CTRLBUS_ODDR400: ODDR
generic map(
	DDR_CLK_EDGE => "SAME_EDGE",
	INIT => '0',
	SRTYPE => "SYNC"
)
port map (
	C	=> sysclk400,
	R	=> sysclk_pll_locked_b,
	D1	=> ctrlbus_out_invcorr(l)(0),
	D2	=> ctrlbus_out_invcorr(l)(1),
	Q	=> ctrlbus_out_ddr(l),
	CE	=> '1',
	S	=> '0'
);
end generate CTRLBUS_ODDR400_GEN;

CTRLBUS_ODELAY: ODELAYE2
generic map(
	REFCLK_FREQUENCY => 200.4,
	ODELAY_TYPE => "VAR_LOAD",
	ODELAY_VALUE => 0,
	PIPE_SEL => "FALSE",
	DELAY_SRC => "ODATAIN",
	HIGH_PERFORMANCE_MODE  => "TRUE",
	SIGNAL_PATTERN => "DATA",
	CINVCTRL_SEL => "FALSE"
)
port map(
	ODATAIN     => ctrlbus_out_ddr(l),
	DATAOUT     => ctrlbus_out_delayed(l),
	CNTVALUEIN	=> ctrlbus_delay_control(l)(4 downto 0),
	LD				=> ctrlbus_delay_pulse(l)(0),
	CNTVALUEOUT	=> ctrlbus_delay_status(l),
	C				=> sysclk40,
	CE				=> ctrlbus_delay_pulse(l)(1),
	INC			=> ctrlbus_delay_control(l)(5),
	CLKIN       => '0', 
	REGRST      => '0', 
	LDPIPEEN    => '0', 
	CINVCTRL    => '0'
);

CTRLBUS_U1_OUT_DDR_OBUFDS_GEN: if l<21 generate
CTRLBUS_U1_OUT_DDR_OBUFDS: OBUFDS
port map(I  => ctrlbus_out_delayed(l),O => CTRLBUS_U1_OUT_P(l),OB => CTRLBUS_U1_OUT_N(l));
end generate CTRLBUS_U1_OUT_DDR_OBUFDS_GEN;

CTRLBUS_U2_OUT_DDR_OBUFDS_GEN: if l>=21 generate
CTRLBUS_U2_OUT_DDR_OBUFDS: OBUFDS
port map(I  => ctrlbus_out_delayed(l),O  => CTRLBUS_U2_OUT_P(l),OB => CTRLBUS_U2_OUT_N(l));
end generate CTRLBUS_U2_OUT_DDR_OBUFDS_GEN;
end generate CTRLBUS_OUT_DDR_GEN;

CTRLBUS_U1_OUT_SDR_GEN: if l=8 generate
  CTRLBUS_U1_OUT_SDR_OBUFDS: OBUFDS
    port map(I  => ctrlbus_out_invcorr(8)(0),O  => CTRLBUS_U1_OUT_P_8,OB => CTRLBUS_U1_OUT_N_8);
end generate CTRLBUS_U1_OUT_SDR_GEN;

CTRLBUS_U2_OUT_SDR_GEN: if l=29 generate
  CTRLBUS_U2_ODDR_ODELAY_OBUFDS: OBUFDS
    port map(I  => ctrlbus_out_invcorr(29)(0),O  => CTRLBUS_U2_OUT_P_29,OB => CTRLBUS_U2_OUT_N_29);
end generate CTRLBUS_U2_OUT_SDR_GEN;
end generate CTRLBUS_OUT_GEN;

CTRLBUS_U1_OUT_TTCBRIDGE_OBUFDS: OBUFDS
port map(I  => ctrlbus_out_invcorr(5)(0),O  => CTRLBUS_U1_OUT_P(5),OB => CTRLBUS_U1_OUT_N(5));

CTRLBUS_U2_OUT_TTCBRIDGE_OBUFDS: OBUFDS
port map(I  => ctrlbus_out_invcorr(26)(0),O  => CTRLBUS_U2_OUT_P(26),OB => CTRLBUS_U2_OUT_N(26));

---------------------------------------------------------------------------------------------------------
-- I2C buffers

I2C_BUF_GEN: for i in 4 downto 0 generate
  I2C_SCL_OBUFT_INSTANCE: OBUFT port map (
    I	=> '0',
    T	=> i2c_scl(i),
    O  => i2c_scl_obuft(i)
    );
  I2C_SDA_IOBUF_INSTANCE: IOBUF port map (
    I	=> '0',
    O	=> i2c_sda_in(i),
    T	=> i2c_sda_out(i),
    IO => i2c_sda_iobuf(i)
    );
end generate;

AVAGO_SCL(0) <= i2c_scl_obuft(0);
AVAGO_SCL(1) <= i2c_scl_obuft(1);
AVAGO_SCL(2) <= i2c_scl_obuft(2);
POWER_SCL    <= i2c_scl_obuft(3);
TTC_CTRL_24	 <= i2c_scl_obuft(4);	--TTC_SCL

AVAGO_SDA(0) <= i2c_sda_iobuf(0);
AVAGO_SDA(1) <= i2c_sda_iobuf(1);
AVAGO_SDA(2) <= i2c_sda_iobuf(2);
POWER_SDA	 <= i2c_sda_iobuf(3);
TTC_CTRL_25	 <= i2c_sda_iobuf(4);	--TTC_SDA

---------------------------------------------------------------------------------------------------------
-- Serial-ID Chip DS2411 (U89) Buffer

SERIAL_ID_IOBUF: IOBUF port map (
	I	=> '0',
	O	=> serialID_in,
	T	=> serialID_out,
	IO => DS2411);
---------------------------------------------------------------------------------------------------------
process(sysclk40) begin
	if rising_edge(sysclk40) then
          ttc_Status_reg <= ttc_Status;
--		jcLossOfLock_reg 		  <= jcLossOfLock;
--		jcInterruptClk1Bad_reg <= jcInterruptClk1Bad;
--		jcSerialDataIn_reg 	  <= jcSerialDataIn;
	end if;
end process;

---------------------------------------------------------------------------------------
--		RESET_HANDLER
---------------------------------------------------------------------------------------
	
rst_inst : entity work.reset_handler
port map(
        CR_CLK_IN         => cr_clk40,
	GCK_CLK_IN        => sysclk40,
	ETH_CLK_IN        => ethclk125,
	GCK_CLK_LOCKED_IN => sysclk_pll_locked,
	ETH_CLK_LOCKED_IN => ethclk_pll_locked,
	ROD_RST_OUT   => rod_rst,
	TTC_RST_OUT   => ttc_int_rst,
	SLINK_RST_OUT => slink_rst_vector,
	DDR_RDY_IN    => rod_control_registers(36)(0),
	GLOBAL_RST       => rod_control_registers(5)(0),
	DDR_IPB_RST_IN   => rod_control_registers(6)(30),
	ROD_IPB_RST_IN   => rod_control_registers(6)(0),
	TTC_IPB_RST_IN   => rod_control_registers(6)(1),
	SLINK_IPB_RST_IN => rod_control_registers(41)
);

locked <= sysclk_pll_locked and ethclk_pll_locked;
TEST_OUT <= 'Z';--sysclk40;

RST_CNTR_PROC_A: process (cr_clk40)
begin
  if rising_edge(cr_clk40) then
    if rod_rst = '1' and rod_rst_synch = '0' then
       rod_rst_synch <=  rod_rst;
      reset_cntr_a(0) <=  reset_cntr_a(0) + 1;
    else
      reset_cntr_a(0) <=  reset_cntr_a(0);
      rod_rst_synch <=  rod_rst;
    end if;
  end if;
end process RST_CNTR_PROC_A;

RST_CNTR_PROC_B: process (sysclk40)
begin
  if rising_edge(sysclk40) then
    if ttc_int_rst = '1' and ttc_int_rst_synch = '0' then
      ttc_int_rst_synch <= ttc_int_rst;
      reset_cntr_a(1) <=  reset_cntr_a(1) + 1;
    else
      reset_cntr_a(1) <=  reset_cntr_a(1);
      ttc_int_rst_synch <= ttc_int_rst;
    end if;
  end if;
end process RST_CNTR_PROC_B;

rod_status_registers(35)(7 downto 0) <= std_logic_vector(reset_cntr_a(0));
rod_status_registers(35)(15 downto 8) <= std_logic_vector(reset_cntr_a(1));
rod_status_registers(35)(23 downto 16) <= ATCA_HRDW_ADDR;

process(ethclk125) begin 
	if rising_edge(ethclk125) then counter125mhz <= counter125mhz + 1; end if; 
end process;

	onehz <= counter125mhz(27) when sysclk_pll_locked='1' else '0';


gtx_clk125_gen : if SIMULATION = true generate
	process
	begin
		eth_gt_txoutclk <= '0'; wait for 4 ns;
		eth_gt_txoutclk <= '1'; wait for 4 ns;
	end process;
end generate gtx_clk125_gen;
		
---------------------------------------------------------------------------------------
--	FPGA/Board Infrastructure
---------------------------------------------------------------------------------------

INFR: entity work.infrastructure
port map(
	sysclk40	=> sysclk40,
	xadc_control	=> xadc_control,
	xadc_status	=> xadc_status,
	usr_access	=> usr_access,
	serialID_in	=> serialID_in,
	serialID_out	=> serialID_out,
	serialID_pulse	=> serialID_pulse,
	serialID_status	=> serialID_status
);

clk: entity work.clocks_controller
port map(
	GCK1_P				=> CTRLBUS_U1_IN_P_12,
        GCK1_N				=> CTRLBUS_U1_IN_N_12,
	gck2				=> cr_clk40,
	sysclk40_out	=> sysclk40,
	sysclk80_out	=> sysclk80,
	sysclk160_out	=> sysclk160,
	sysclk200_out	=> sysclk200,
	sysclk400_out	=> sysclk400,
        sysclk_reset    => rod_control_registers(5)(1),
	sysclk_pll_locked_out => sysclk_pll_locked,
	errorflagsReset => GeneralPulse(0),
	clocksStatus => clocksStatus(2 downto 0)
);		
	sysclk_pll_locked_b <= not sysclk_pll_locked;

---------------------------------------------------------------------------------------
--		TTC_Bridge
---------------------------------------------------------------------------------------
-- 26th January 2016, Christian Kahra: Change in the forwarding of ttc signals
-- Up to now, the ttcL1Accept, the ttcBunchCounterReset and the reception of the ttcBroadcast "01" were serialized on the ttcBridge-line
-- From now on, only the reception of the ttcBroadcast "01" will be signaled as 40MHz pulse on the ttcBridge-line (or the IPBus emulation of it)
-- Therefore the ttcBridge serialization module was commented out and the TTC_BROADCAST_PROCESS process controls this line now
--ttcBridge: entity work.ttcBridge
--port map(
--	sysclk40						=> sysclk40,
--	sysclk80						=> sysclk80,
--	ttc_L1Accept				=> TTC_L1A_IN,
--	ttc_EventCounterReset	=> TTC_EVTCNTRRST_IN,
--	ttc_BunchCounterReset	=> TTC_BCNRST_IN,
--	ttc_BroadcastStrobe1		=> TTC_BCSTR1_IN,
--	ttc_BroadcastStrobe2		=> TTC_BCSTR2_IN,
--	ttc_Broadcast				=> TTC_BRCST_IN,
--	ttcBridgeControl			=> ttcBridgeControl,
--	ttcBridge_u1				=> ttcBridge_u1,
--	ttcBridge_u2				=> ttcBridge_u2,
--	debug							=> open
--);
-- The reception of the ttc short broadcast "01" (or the IPBus controlled signal ttcBridgeControl(1) to emulate the broadcast) will be sent as 40 MHz pulse to the processors.
-- It will be sent on the falling-edge of sysclk40 to have a 12.5ns period to the rising-edge of processor's sysclk40, which should be more than enough to deal with the phase shift of the two clocks
-- and the transmission period    
TTC_BRIDGE_PROCESS: process(sysclk40) begin
if falling_edge(sysclk40) then
    if GeneralControl(0)='0' then    
        --21th Feb 2016, ck: inserted multiplexer to switch between ttc short-broadcast forwarding and ttc bunchcounter-reset forwarding 
        if (ttc_brcst_reg(5 downto 4)="01" and ttc_bcstr1_reg='1') or (ttcBridgeControl(1)='1') then            
            ttcBridge_inv_u1 <= not '1'; -- 7th Mar 2016, ck: logic inversion (because of swapped differential pins) has to be put in
            ttcBridge_inv_u2 <= not '1'; --                   front of register to place it in iob
        else
            ttcBridge_inv_u1 <= not '0';
            ttcBridge_inv_u2 <= not '0';
        end if;
    else
        if (ttc_bcnrst_reg='1') or (ttcBridgeControl(2)='1') then
            ttcBridge_inv_u1 <= not '1';
            ttcBridge_inv_u2 <= not '1';
        else
            ttcBridge_inv_u1 <= not '0';
            ttcBridge_inv_u2 <= not '0';
        end if;
    end if;
end if;
end process TTC_BRIDGE_PROCESS;

---------------------------------------------------------------------------------------
--		TTC_Rx
---------------------------------------------------------------------------------------
                
ttcrx : entity work.ttcrx_interface
  generic map (
    SIMULATION => SIMULATION)
port map(
	CLK_40MHZ			=> sysclk40,
        CLK_160MHZ			=> sysclk160,
	MMCX_OUT			=> open,
        CLK_LOCKED_IN  		=> locked,
	RESET_IN       		=> ttc_int_rst,
	TTC_EVT_H_STR_IN 	=> TTC_EVT_H_STR_IN,
	TTC_L1A_IN 			=> TTC_L1A_IN,
	TTC_BCNT_STR_IN 	=> TTC_BCNT_STR_IN,
	TTC_EVT_L_STR_IN	=> TTC_EVT_L_STR_IN, 
	TTC_BCNT_IN			=> TTC_BCNT_IN,
	TTC_EVTCNTRRST_IN  	=> TTC_EVTCNTRRST_IN,
	TTC_BCNRST_IN      	=> TTC_BCNRST_IN,
	TTC_BRCST_IN       	=> TTC_BRCST_IN,
	TTC_BCSTR1_IN      	=> TTC_BCSTR1_IN,	
	ttc_brcst_reg       => ttc_brcst_reg,     --21th Feb 2016, ck: routing registered ttc_broadcast, ttc_broadcast_strobe1 and ttc_bunchcounter_reset up to top module to forward to processors
	ttc_bcstr1_reg      => ttc_bcstr1_reg,
        ttc_bcnrst_reg      => ttc_bcnrst_reg,    
	TTC_BCSTR2_IN      	=> TTC_BCSTR2_IN,
	TTC_DOUT_STR_IN    	=> TTC_DOUT_STR_IN,
	TTC_DOUT_IN        	=> TTC_DOUT_IN,
	TTC_SUBADDR_IN     	=> TTC_SUBADDR_IN,
	IPB_ECRID_RST_IN    => rod_control_registers(68),
	IPB_ECRID_SET_IN    => rod_control_registers(69),
	IPB_ORBIT_WRAP_IN   => rod_control_registers(72),
	IPB_ORBIT_RST_IN    => rod_control_registers(73),
	IPB_L1ID_RST_IN     => rod_control_registers(71),
	IPB_TRG_TIMEOUT_IN  => rod_control_registers(67),
	IPB_TRG_TYPE_IN     => rod_control_registers(3),
	TTC_RESET_OUT		=> open,
	L1A_OUT			=> l1a_from_ttc, --level 1 accepted. Main trigger
	TTC_BCID_OUT		=> ttc_bcid, --BCID is other name for BCN (bunch crossing number)
	TTC_EVTID_OUT		=> ttc_evtid,
	TTC_TRG_TYPE_OUT        => trigger_type,
	TTC_TRG_TYPE_RDY_OUT    => trigger_type_en,
	TTC_TRG_TIMEOUT_OUT     => trigger_timeout,
	LOCAL_ORBIT_CTR_OUT     => orbit_ctr,
	LOCAL_ECR_CTR_OUT       => ecr,
	LOCAL_BCID_OUT          => local_bcid,
	LOCAL_BCID_MISMATCH_OUT => bcn_mismatch,        
	GENERAL_L1A_CTR_OUT     => rod_status_registers(39),
	GENERAL_ECR_CTR_OUT     => rod_status_registers(42),
        GENERAL_EVT_TYPE_CNTR_OUT => rod_status_registers(71)
);
  

SYNCH_L1A : process (sysclk40)
begin
  if rising_edge(sysclk40) then
    l1_pulse_from_ipbus <= rod_control_registers(1)(0);
    ttc_l1a <= l1_pulse_from_ipbus or rod_control_registers(1)(0) or l1a_from_ttc;      
  end if;
end process SYNCH_L1A;

SYNCH_L1A_FALLING : process (sysclk40)
begin
  if falling_edge(sysclk40) then
    ttc_l1a_U1 <= ttc_l1a;
    ttc_l1a_U2 <= ttc_l1a;
  end if;
end process SYNCH_L1A_FALLING;    
ctrlbus_out_invcorr(8)(0)  <= ttc_l1a_U1;
ctrlbus_out_invcorr(29)(0) <= ttc_l1a_U2;
TTC_CTRL_26 <= '0';  --P/D 	(mode:	"Protected"=1, "Debug"=0)
TTC_CTRL_27 <= '1';  --clkSel	(when P/D=1: ClkSel=1 => TTCrx clock, ClkSel=0 => XTAL clk
                     -- when P/D=0: ClkSel=1 => automatic changeover,	ClkSel=1 => XTAL clk)
---------------------------------------------------------------------------------------
--		ControlPath
---------------------------------------------------------------------------------------

CTRL: entity work.controlPath
	port map(
		sysclk40 => sysclk40,
		sysclk80 => sysclk80,
		sysclk200 => sysclk200,
		sysclk400 => sysclk400,
		eth_gt_refclk_bufg => mgt7_clk,--_buffered, --in                
		eth_gt_outclk => eth_gt_outclk,
		ethclk62_5 => ethclk62_5,
		ethclk125 => ethclk125,		
		gck_mmcm_locked => sysclk_pll_locked,
		eth_mmcm_locked => ethclk_pll_locked,                
		eth_gt_refclk => mgt7_clk, --out
                eth_gtrefclk_p => MGT7_CLK_P,
                eth_gtrefclk_n => MGT7_CLK_N,                
		eth_gt_rx_p => K7_MGTRX_3_P,
		eth_gt_rx_n => K7_MGTRX_3_N,
		eth_gt_tx_p => K7_MGTTX_3_P,
		eth_gt_tx_n => K7_MGTTX_3_N,		
		eth_sgmiiphy_done_out => eth_sgmiiphy_done,
		rst_extphy => EXT_K7_0,--rst_extphy,
		mac_addr => mac_addr,
		ip_addr => ip_addr,
		pkt_rx_led => pkt_rx_led,
		pkt_tx_led => pkt_tx_led,
		-- controller-processor connections		
		ipbBridge_u1_in  => ipbBridgeResponseU1,
		ipbBridge_u1_out => ipbBridgeRequestU1,
		ipbBridge_u2_in  => ipbBridgeResponseU2,
		ipbBridge_u2_out => ipbBridgeRequestU2,				
		-- I2C connections
		i2c_scl		=> i2c_scl,
		i2c_sda_in	=> i2c_sda_in,
		i2c_sda_out	=> i2c_sda_out,
                --...
		clocksStatus => clocksStatus,
		ctrlbus_delay_control => ctrlbus_delay_control,
		ctrlbus_delay_pulse => ctrlbus_delay_pulse,
		ctrlbus_delay_status => ctrlbus_delay_status,
		ttcStatus => ttc_Status_reg,
		ttcBridgeControl => ttcBridgeControl,
		xadc_control 		=> xadc_control,
		xadc_status  		=> xadc_status,
		usr_access	 		=> usr_access,
		serialID_pulse		=> serialID_pulse,
		serialID_status	=> serialID_status,	
		--jcLossOfLock 			=> jcLossOfLock_reg,
		--jcInterruptClk1Bad 	=> jcInterruptClk1Bad_reg,
		--jcSlaveSelect_b 		=> jcSlaveSelect_b,
		--jcSerialDataOut 		=> jcSerialDataOut,
		--jcSerialDataIn 		=> jcSerialDataIn_reg,
		--jcSerialClk 			=> jcSerialClk,
		-- misc		
		ModuleID => ModuleID,
		Serial_No_Module => Serial_No_Module,
		Serial_No_ExtensionBoard => Serial_No_ExtensionBoard,
		Firmware_Version	=> Firmware_Version,		
		FirmwareDate		=> FirmwareDate,
		FirmwareRevision	=> FirmwareRevision,
		GeneralControl => GeneralControl,
		GeneralPulse => GeneralPulse,
		-- rod		
		rod_control_registers => rod_control_registers,
		rod_status_registers => rod_status_registers		
--		i2c_dataOut => i2c_dataOut,
--		i2c_error => i2c_error,
--		i2c_address => i2c_address,
--		i2c_writeEnable => i2c_writeEnable,
--		i2c_pointer => i2c_pointer,
--		i2c_dataIn => i2c_dataIn,
--		i2c_busSelect => i2c_busSelect,
--		debugIPBus => debugIPBus,
--		debugIPBusBridgeU2 => debugIPBusBridgeU2(229 downto 0)
	);

-------------------------------------------------------------------------------
-- ROD
-------------------------------------------------------------------------------
-- DATA PATH  
REG_SIM_GEN : if SIMULATION = true generate
  reg_gen : for i in 0 to 34 generate
    rod_control_registers(i) <= (others => '0');
  end generate;
  
  reg_gen2 : for i in 39 to 52 generate
    rod_control_registers(i) <= (others => '0');
  end generate;
  reg_gen3 : for i in 55 to 74 generate
    rod_control_registers(i) <= (others => '0');
  end generate;
  process
  begin
    rod_control_registers(35) <= (others => '0');
    rod_control_registers(36) <= (others => '0');
    wait for 200 ns;
    rod_control_registers(35) <= x"00000001";
    wait for 40 ns;
    rod_control_registers(35) <= (others => '0');
    wait for 15 us;
    rod_control_registers(36) <= (others => '1');
    wait;
  end process;
  rod_control_registers(38)(7 downto 0) <= "01110011";
  rod_control_registers(37) <= "10011100111001110011100111001110";
  rod_control_registers(77)(3 downto 0) <= x"2";  --number of max slices
  rod_control_registers(53)(3 downto 0) <= x"2";  --maxo ffset
end generate REG_SIM_GEN;
                
rod_status_registers(2) <= rod_wrapper_status_registers(2);
rod_status_registers(8) <= rod_wrapper_status_registers(8);
rod_status_registers(9) <= rod_wrapper_status_registers(9);
rod_status_registers(10) <= rod_wrapper_status_registers(10);
rod_status_registers(11) <= rod_wrapper_status_registers(11);  
rod_status_registers(17) <= rod_wrapper_status_registers(17);
rod_status_registers(18) <= rod_wrapper_status_registers(18);
rod_status_registers(20) <= rod_wrapper_status_registers(20);
rod_status_registers(34) <= rod_wrapper_status_registers(34);
rod_status_registers(40) <= rod_wrapper_status_registers(40);

rod_with_slink : entity work.rod_slink_wrapper
generic map(
	SIMULATION             => SIMULATION,
        INT_SIM                => INT_SIM,
	VIVADO                 => VIVADO,
        NUMBER_OF_PROCESSORS   => NUMBER_OF_PROCESSORS,
        DEBUG                  => DEBUG
        )
port map(
        TTC_CLK40        => sysclk40,
	GCK_CLK40        => cr_clk40, 
        GCK_CLK65        => ethclk62_5,
	GCK_CLK80        => cr_clk80, 
        RODGCK_CLK80(0)     => cr_clk80, 
        RODGCK_CLK80(1)     => cr_clk80, 
        ROD_CLK          => cr_clk160, 
        MGT5_CLK         => mgt5_clk, 
	MGT7_CLK         => eth_gt_outclk, --same clock for slink and Ethernet,ibufgds in the Ethernet core
        MGT2_CLK       => mgt2_clk,
        ROD_GTX_RXP      => ROD_GTX_RXP,
        ROD_GTX_RXN      => ROD_GTX_RXN,
        GTX_ROD_SOFT_RST => rod_control_registers(35)(2 downto 0),
        ROD_GTX_DATA_EN  => rod_control_registers(36)(1 downto 0),
	ROD_RESET        => rod_rst,
	SLINK_RESET      => slink_rst_vector,
	CLK_LOCKED_IN    => locked, 
        ROD_RW_REG_IN    => rod_control_registers,
        ROD_RW_REG_OUT   => rod_wrapper_status_registers,
	OPTO_KR1_N       => OPTO_KR1_N,
	OPTO_KR1_P       => OPTO_KR1_P,
	OPTO_KT1_N       => OPTO_KT1_N,
	OPTO_KT1_P       => OPTO_KT1_P,
	SLINK_STATUS_REG_OUT(31 downto 0) => rod_status_registers(32),
	SLINK_STATUS_REG_OUT(63 downto 32) => rod_status_registers(33),
	SLINK_FORMAT_VERSION_ROS => rod_control_registers(43),
	SLINK_FORMAT_VERSION_ROIB => rod_control_registers(44),
	SLINK_BUSY_CNT_TIME_PERIOD_REG_IN => slink_busy_cnt_time_period_reg,
	SLINK_BUSY_CNT_REG_OUT => slink_busy_cnt_reg,
	SLINK_IDLE_CNT_REG_OUT => slink_idle_cnt_reg,	
	L1A_IN           => ttc_l1a,
	BCID_IN          => ttc_bcid,
	EVTID_IN         => ttc_evtid,
	RUN_NUMBER_IN    => rod_control_registers(2)(23 downto 0),
	RUN_TYPE_IN      => rod_control_registers(2)(31 downto 24),
	TRIGGER_TYPE_IN  => trigger_type,
	TRIGGER_TYPE_READY_IN => trigger_type_en,
	MODULE_ID_IN     => rod_control_registers(45)(31 downto 16),
	ECR_IN           => ecr,
	ORBIT_CTR_IN	  => orbit_ctr(15 downto 0),
	SEQUENCE_TYPE_IN => rod_control_registers(74)(15 downto 0),
	STAT_WORD1_IN => status1,
	STAT_WORD2_IN => status2,
	BUSY_FROM_U2_IN  => busy_from_u2,
        L1_BUSY_DAQ      => l1busy_daq,
        L1_BUSY_ROIB      => l1busy_roib,
	DEBUG_OUT        => rod_dbg,
	ICON_CONTROL_OUT => icon_control,
        CNTR_DEBUG  => cntr_debug_l
);

        status1(0) <= bcn_mismatch; --status words send with slink data
	status1(1) <= '0';
	status1(2) <= timeout;
	status1(3) <= data_transport_error;
	status1(4) <= rod_fifo_overflow;
	status1(15 downto 5) <= (others => '0');
	status1(16) <= lvds_link_error;
	status1(17) <= cmm_parity_error;
	status1(18) <= glink_error;
	status1(31 downto 19) <= (others => '0');	
	status2(0) <= '0';
	status2(1) <= limited_roi_set;
	status2(15 downto 2) <= (others => '0');
	status2(16) <= trigger_timeout;
	status2(31 downto 17) <= (others => '0');	
               
SEND_BUSY_OUT : process (sysclk40)
begin
  if rising_edge(sysclk40) then
    if NUMBER_OF_PROCESSORS = 2 then 
      main_daq_busy <= ((busy_from_u2 or --not
                         (busy_from_u1) or l1busy_daq) and not(rod_control_registers(0)(31))) or rod_control_registers(0)(0);
      main_roib_busy <= ((busy_from_u2 or --not
                          (busy_from_u1) or l1busy_roib) and not(rod_control_registers(0)(30))) or rod_control_registers(0)(1);
    elsif NUMBER_OF_PROCESSORS = 1 then
      main_daq_busy <= ((--not
        (busy_from_u1) or l1busy_daq) and (not rod_control_registers(0)(31))) or rod_control_registers(0)(0);
      main_roib_busy <= ((--not
        (busy_from_u1) or l1busy_roib) and (not rod_control_registers(0)(30))) or rod_control_registers(0)(1);
    else
      main_daq_busy <= (l1busy_daq and (not rod_control_registers(0)(31)))  or rod_control_registers(0)(0);
      main_roib_busy <= (l1busy_roib and (not rod_control_registers(0)(30))) or rod_control_registers(0)(1);
    end if;
  end if;
end process SEND_BUSY_OUT;
ROD_DAQ_BUSY <=  main_daq_busy;
ROD_ROIB_BUSY <=  main_roib_busy;

-------------------------------------------------------------------------------
-- ROD counters for debug
-------------------------------------------------------------------------------

SAVE_CNTRS: for i in 0 to 27 generate
  SYNC_CNTR_DEBUG: process (sysclk40)
    begin
      if rising_edge(sysclk40) then
        cntr_debug_lsync(i) <= cntr_debug_l(i); 
        rod_status_registers(43+i) <= std_logic_vector(cntr_debug_lsync(i));
      else
        cntr_debug_lsync <= cntr_debug_lsync;
        rod_status_registers(43+i) <= rod_status_registers(43+i);
      end if;
   end process SYNC_CNTR_DEBUG;     
end generate SAVE_CNTRS;

-------------------------------------------------------------------------------
-- others
-------------------------------------------------------------------------------
--output register/buffer
--process(sysclk40) begin
--	if rising_edge(sysclk40) then
--		jcSlaveSelect_b_reg <= jcSlaveSelect_b;
--		jcSerialDataOut_reg <= jcSerialDataOut;
--		jcSerialClk_reg	  <= jcSerialClk;
--	end if;
--end process;
--output mapping
-- JitterCleaner
--	JC1_CTRL_4 <= jcSlaveSelect_b_reg;
--	JC1_CTRL_7 <= jcSerialDataOut_reg;
--	JC1_CTRL_9 <= jcSerialClk_reg;
--	JC1_CTRL_10 <= '1'; --RESET_B
--	JC1_CTRL_11 <= '1'; --CMODE SPI
-- ExtensionBoard
--	EXT_K7_0 <= not rst_extphy;
	EXT_K7_1 <= not onehz;	--1.074 sec blinkenlight
	EXT_K7_2 <= eth_sgmiiphy_done;            

end Behavioral;

