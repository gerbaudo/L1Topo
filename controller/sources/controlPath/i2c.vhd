----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    09:29:18 02/01/2011 
-- Design Name: 
-- Module Name:    i2c - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity i2c is port(
	CLK40MHz:		in  std_logic;
	i2c_control:	in  std_logic_vector(26 downto 0);
	i2c_pulse:		in  std_logic;
	i2c_status:		out std_logic_vector( 9 downto 0);
	
	i2c_scl:			out std_logic_vector(4 downto 0);
	i2c_sda_in:		in  std_logic_vector(4 downto 0);
	i2c_sda_out:	out std_logic_vector(4 downto 0)
	
--	DATA_IN : in  STD_LOGIC_VECTOR (7 downto 0);
--	POINTER : in  STD_LOGIC_VECTOR (7 downto 0);
--	ADDRESS : in  STD_LOGIC_VECTOR (7 downto 1);
--	WRITING : in  STD_LOGIC;
--	STRB : in STD_LOGIC;
--	DATA_OUT : out  STD_LOGIC_VECTOR (7 downto 0):="00000000";
--	ERROR : out  STD_LOGIC:='0';
--	BUSY : out  STD_LOGIC:='0';
--	SDAIN : in STD_LOGIC;
--	SDAOUT : out STD_LOGIC:='Z';
--	SCL : out STD_LOGIC:='Z'
);
end i2c;

architecture Behavioral of i2c is
	attribute DONT_TOUCH : string; 
	attribute shreg_extract : string;	
	attribute keep : string;	
	attribute DONT_TOUCH of Behavioral : architecture is "TRUE";


signal i2c_init	: STD_LOGIC;
signal IDLEn	: STD_LOGIC:='0';	
signal A_Err	: STD_LOGIC;	

signal new_command : STD_LOGIC:='0';					-- new_command flag


TYPE phase IS 			( P0 , P1 , P2 , P3 );

signal clockphase		: phase;

TYPE shiftstate IS 		( IDLE , STRT , ADDR , RDWR, AACK, DATA , DACK , STP1 , STP2 );


SIGNAL output_shiftreg	: STD_LOGIC_VECTOR(16 downto 0):="00000000000000000";
SIGNAL dat_shiftreg		: STD_LOGIC_VECTOR(7 downto 0):="00000000";

SIGNAL sequencer 		: shiftstate;

signal cycle,clken : STD_LOGIC;

signal shift_count		: INTEGER RANGE 0 to 16;
signal s_data,s_clck,i2c_res,i2c_sdata_in	: STD_LOGIC;

signal WRITING,STRB,ERROR,BUSY: std_logic:='0';
signal ADDRESS:STD_LOGIC_VECTOR(7 DOWNTO 1):="0000000";
signal DATA_IN,DATA_OUT,POINTER: STD_LOGIC_VECTOR(7 DOWNTO 0):="00000000";
signal I2CError,I2C_Busy: std_logic_vector(0 downto 0):="0";
signal I2CStrobe: std_logic_vector(0 downto 0):="0";
signal I2CAddress: std_logic_vector(6 downto 0):="0000000";
signal I2C_Chain: std_logic_vector(2 downto 0):="000";
signal I2CRegisterAddress: std_logic_vector(7 downto 0):="00000000";
signal I2CReadOrWrite: std_logic:='0';
signal I2CDataIn: std_logic_vector(7 downto 0):="00000000";
signal I2CDataOut: std_logic_vector(7 downto 0):="00000000";
signal StateCounter: integer range 0 to 3:=1;

signal SDAIN:  std_logic;
signal SDAOUT: std_logic:='Z';
signal SCL: 	std_logic:='Z';

begin

	i2c_res<='0';



	I2CAddress					<= I2C_Control(6 downto 0);
	I2C_Chain					<=	I2C_Control(9 downto 7);	
	I2CReadOrWrite				<= I2C_Control(10);
	I2CRegisterAddress		<= I2C_Control(18 downto 11);
	I2CDataIn					<= I2C_Control(26 downto 19);

	I2CStrobe(0)				<= I2C_Pulse;

	I2C_Status(7 downto 0)	<= I2CDataOut;
	I2C_Status(8)				<= I2C_Busy(0);	
	I2C_Status(9)				<= I2CError(0);


	SDAIN <= i2c_sda_in(0) when I2C_Chain="000"
		 else i2c_sda_in(1) when I2C_Chain="001"
		 else i2c_sda_in(2) when I2C_Chain="010"
		 else i2c_sda_in(3) when I2C_Chain="011"
		 else i2c_sda_in(4) when I2C_Chain="100"
		 else '0';

--AVAGO_0
	
	i2c_sda_out(0)<=SDAOUT when I2C_Chain="000" else '1';
	i2c_scl(0)<=SCL when I2C_Chain="000" else '1';
	
--AVAGO_1
	i2c_sda_out(1)<=SDAOUT when I2C_Chain="001" else '1';
	i2c_scl(1)<=SCL when I2C_Chain="001" else '0';
	
--AVAGO_2
	i2c_sda_out(2)<=SDAOUT when I2C_Chain="010" else '1';
	i2c_scl(2)<=SCL when I2C_Chain="010" else '1';
	
--Power
	i2c_sda_out(3)<=SDAOUT when I2C_Chain="011" else '1';
	i2c_scl(3)<=SCL when I2C_Chain="011" else '1';

--TTC
	i2c_sda_out(4)<=SDAOUT when I2C_Chain="100" else '1';
	i2c_scl(4)<=SCL when I2C_Chain="100" else '1';




process(CLK40MHz)
	variable Timer: integer range 0 to 511:=0;
begin
	if (rising_edge(CLK40MHz)) then
		if BUSY = '0' then
			if Timer=500 then
				I2C_Busy<="0";
				if StateCounter=1 and I2CStrobe="1" then
					Timer:=0;
					ADDRESS<=I2CAddress;
					WRITING<=I2CReadOrWrite;
					POINTER<=I2CRegisterAddress;
					DATA_IN<=I2CDataIn;
					STRB<='1';
					StateCounter<=2;
				elsif StateCounter=2 then
					I2CError(0)<=ERROR;
					I2CDataOut<=DATA_OUT;
					StateCounter<=1;
				end if;
			else
				Timer:=Timer+1;
			end if;
		elsif BUSY='1' then
			I2C_Busy<="1";
			STRB<='0';
		end if;
	end if;
end process;










process(CLK40MHz)
variable cnt:integer range 0 to 500:=0;
variable tds,tmp:std_logic:='0';
begin
	if rising_edge(CLK40MHz) then
		----------------------------------------------------
		i2c_sdata_in<=SDAIN;
		tmp:='0';
		if cnt=25 then 
			tmp:='1';
			cnt:=0;
		else
			cnt:=cnt+1;
		end if;
		clken<=tmp;

		if (IDLEn = '1' ) then
			new_command <= '0';						--clear flag once controller starts
		elsif(STRB='1') then
			new_command <= '1';						-- trailing/falling edge sets flag
		end if;
	--------------------clken
		if  clken='1' then     --*** *************************************

			i2c_init <= new_command;-- i2c_initiate I2C cycles
		-------------------------------------------------------------------------------
		-- 4-phase clock generator
		-------------------------------------------------------------------------------

			case clockphase is
				when P0  	=> clockphase <= P1;
				when P1  	=> clockphase <= P2;
				when P2  	=> clockphase <= P3;
				when others => clockphase <= P0;
			end case;
			

		-------------------------------------------------------------------------------
		-- I2C sequencer 
		-------------------------------------------------------------------------------

			IF (clockphase = P3) THEN

				IDLEn <= '1';
				shift_count <= shift_count + 1;

				case sequencer is 	
					when IDLE =>
						IDLEn <= i2c_init;
						cycle <= '0';			-- First cycle sets pointer, second cycle xfers data
						IF (i2c_init = '1' ) THEN sequencer <= STRT; 
						end if;

					when STRT => 
						sequencer <= ADDR;
						shift_count <= 0;

					when ADDR => 
						IF (shift_count = 6 ) THEN sequencer <= RDWR; 
						end if;

					when RDWR => 
						sequencer <= AACK;

					when AACK => 
						sequencer <= DATA;
						shift_count <= 0;

					when DATA => 
						IF (shift_count = 7 ) THEN 
							if WRITING = '1' and cycle = '0' then
								sequencer <= AACK;
								cycle <= '1';
							else
								sequencer <= DACK;
							end if;
						end if;

					when DACK => 
						sequencer <= STP1;

					when STP1 => 
						sequencer <= STP2;

					when STP2 => 
						IF (cycle = '0' ) 
							THEN sequencer <= STRT;
							else sequencer <= IDLE;
						end if;
						cycle <= '1';

					when others => 
						sequencer <= IDLE; 
				end case;

			END IF;

		-------------------------------------------------------------------------------
		-- start - stop protocol bits 
		-------------------------------------------------------------------------------
			if i2c_res = '1' then
				s_clck <= '1';
			else     --*** *************************************

				s_data <= s_data; 	-- hold state until specified
				s_clck <= s_clck;

				case sequencer is
				when IDLE =>
					s_data <= '1';
					s_clck <= '1';

				when STRT =>
					s_data <= '0';
					if(clockphase = P2 ) then
						s_clck <= '0';
					end if;

				when ADDR | RDWR | DATA | AACK | DACK =>
					s_data <= output_shiftreg(16);

					case clockphase is
						when P1 =>
							s_clck <= '1';
						when P3 =>
							s_clck <= '0';
						when others =>
					end case;	

				when STP1 =>
					case clockphase is
						when P0 =>
							s_clck <= '0';
							s_data <= '0';
						when P1 =>
							s_clck <= '1';
						when P2 =>
							s_data <= '1';
						when others =>
					end case;	
				
				when others => 
				end case;
			end if;
		-------------------------------------------------------------------------------
		-- output shift register
		-------------------------------------------------------------------------------

			IF (clockphase = P3) THEN
				output_shiftreg (16 downto 1) <= output_shiftreg (15 downto 0) ;
				output_shiftreg (0) <= '1' ;

				case sequencer is 	-- First cycle sets pointer, second cycle xfers data
					when IDLE =>
					when STRT => 															-- loadup shift register
						output_shiftreg(16 downto 10) <= ADDRESS(7 downto 1);
						output_shiftreg(9) <= (not WRITING) and cycle;								--1st cycle is always a write
						output_shiftreg(8) <= '1';											-- AACK bit, could also be Hi-Z
						IF (cycle = '0' ) 
							THEN output_shiftreg(7 downto 0) <= POINTER;			--1st cycle loads Register No.
						elsif (WRITING = '0') 
							then output_shiftreg(7 downto 0) <= (others => '1');	--2nd cycle loads Data, even if read
						end if;
					
					when DATA =>		--Mod. Andi for write
							if WRITING = '1' and shift_count = 7 and cycle = '0' then
								output_shiftreg(15 downto 8) <= DATA_IN;
								output_shiftreg(16)<= '1';--AACK
							end if;
							
					when others => 
				end case;
			END IF;
		-------------------------------------------------------------------------------
		-- read register
		-------------------------------------------------------------------------------

			IF (clockphase = P2) THEN
				IF (sequencer = DATA and cycle = '1') THEN
					dat_shiftreg(0) <= i2c_sdata_in;
					dat_shiftreg(7 downto 1) <= dat_shiftreg(6 downto 0);
				ELSE
					dat_shiftreg <= dat_shiftreg;
				end if;
			end if;

		-------------------------------------------------------------------------------
		-- I2C Address ACK error flag
		-------------------------------------------------------------------------------


			IF (clockphase = P2) THEN

				IF (sequencer = STRT and cycle = '0') THEN
					A_Err <= '0';
				-- ELSIF	(sequencer = AACK ) THEN
					-- A_Err <= A_Err or i2c_sdata_in; -- i2c_sda should be low
				ELSIF	(sequencer = AACK and i2c_sdata_in/='0') THEN
					A_Err <= '1'; -- i2c_sda should be low					
				ELSE
					A_Err <= A_Err;
				end if;
			end if;
		END IF;--clken
	END IF;--rising edge
END process;
BUSY <= new_command OR IDLEn;
ERROR <= A_Err;
SCL <= '0' when s_clck = '0' else '1';
SDAOUT <=  '0'  when s_data  = '0' else '1';
DATA_OUT <= dat_shiftreg;

end Behavioral;

