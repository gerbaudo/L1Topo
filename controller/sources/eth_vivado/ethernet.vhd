--------------------------------------------------------------------------------
-- Institute:       Johannes Gutenberg - Universitaet Mainz, Institut fuer Physik
-- Author:          Christian Kahra (ckahra@uni-mainz.de)
-- Create Date:     2016-06-14
--------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

use work.L1Topo_package.all;


entity ethernet is
port(
    
    sysclk200:                  in  std_logic                       :=     '0';
    sysclk_locked:              in  std_logic                       :=     '0';

    ETH_MGTREFCLK_P:            in  std_logic                       :=     '0';
    ETH_MGTREFCLK_N:            in  std_logic                       :=     '1'; 
    ETH_MGT_RX_P:               in  std_logic                       :=     '0';
    ETH_MGT_RX_N:               in  std_logic                       :=     '1';
    ETH_MGT_TX_P:               out std_logic                       :=     '0';
    ETH_MGT_TX_N:               out std_logic                       :=     '1';
    
    RESET_ETH_PHY_N:            out std_logic                       :=     '1';
    
    ethclk125:                  out std_logic                       :=     '0';
    eth_locked:                 out std_logic                       :=     '0';
    gtrefclk_out:                 out std_logic                       :=     '0';
    
    mac_rx_data:                out std_logic_vector(7 downto 0)    :=   x"00";
    mac_rx_valid:               out std_logic                       :=     '0';
    mac_rx_last:                out std_logic                       :=     '0';
    mac_rx_error:               out std_logic                       :=     '0';
    
    mac_tx_data:                in  std_logic_vector(7 downto 0)    :=   x"00";
    mac_tx_valid:               in  std_logic                       :=     '0';
    mac_tx_last:                in  std_logic                       :=     '0';
    mac_tx_error:               in  std_logic                       :=     '0';
    mac_tx_ready:               out std_logic                       :=     '0';
    
    eth_control:                in  std_logic_vector( 4 downto 0)   := "00000";
    eth_status:                 out std_logic_vector(18 downto 0)   := (others => '0')
    
);
end entity ethernet;




architecture ethernet_sgmii of ethernet is

    signal eth_mgtrefclk_bufg:           std_logic                       := '0';
    signal ethclk125_i:             std_logic                       := '0';
    
    signal eth_phy_reset_counter:   std_logic_vector(5 downto 0)    := "000000";

    signal status_vector:           std_logic_vector(15 downto 0)   := x"0000";

    signal gmii_txd:                std_logic_vector(7 downto 0)    := x"00";
    signal gmii_tx_en:              std_logic                       := '0';
    signal gmii_tx_er:              std_logic                       := '0';
    signal gmii_rxd:                std_logic_vector(7 downto 0)    := x"00";
    signal gmii_rx_dv:              std_logic                       := '0';
    signal gmii_rx_er:              std_logic                       := '0';

    signal reset_eth_phy_i:         std_logic                       := '0';
    signal reset_eth_phy_n_reg:     std_logic                       := '0';
    signal reset_sgmii_phy:         std_logic                       := '0';
    signal reset_mac_glb_n:         std_logic                       := '0';
    signal reset_mac_rx_n:          std_logic                       := '0';
    signal reset_mac_tx_n:          std_logic                       := '0';
    

    signal phy_resetdone:           std_logic                       := '0';
    signal pma_reset_ongoing:       std_logic                       := '0';
    signal eth_mmcm_locked:         std_logic                       := '0';

    function inc(arg: std_logic_vector) return std_logic_vector is
      variable result: std_logic_vector(arg'RANGE);
    begin
      result := std_logic_vector(unsigned(arg)+1);
      return result;
    end function inc;
    
begin

    reset_eth_phy_i <=     eth_control(0);
    reset_sgmii_phy <=     eth_control(1);
    reset_mac_glb_n <= not eth_control(2);
    reset_mac_rx_n  <= not eth_control(3);
    reset_mac_tx_n  <= not eth_control(4);



P_ETH_PHY_RESET : process (eth_mgtrefclk_bufg) begin
if rising_edge(eth_mgtrefclk_bufg) then
    if sysclk_locked = '0' or reset_eth_phy_i='1' then eth_phy_reset_counter <= "000000";
    elsif eth_phy_reset_counter /= "111111"       then eth_phy_reset_counter <= inc(eth_phy_reset_counter);
    end if;
    
-- IOB register for active-low reset
    if eth_phy_reset_counter /= "111111" then reset_eth_phy_n_reg <= '0';
    else reset_eth_phy_n_reg <= '1';
    end if;
    
end if;
end process P_ETH_PHY_RESET;

-- top level port
    RESET_ETH_PHY_N <= reset_eth_phy_n_reg;



U_SGMII_PHY: entity work.gig_ethernet_pcs_pma_sgmii
port map(
    gtrefclk_p              => ETH_MGTREFCLK_P,
    gtrefclk_n              => ETH_MGTREFCLK_N,
    gtrefclk_out            => gtrefclk_out,
    gtrefclk_bufg_out       => eth_mgtrefclk_bufg,
    rxp                     => ETH_MGT_RX_P,
    rxn                     => ETH_MGT_RX_N,
    txp                     => ETH_MGT_TX_P,
    txn                     => ETH_MGT_TX_N,
    independent_clock_bufg  => sysclk200,
    userclk_out             => open,
    userclk2_out            => ethclk125_i,
    rxuserclk_out           => open,
    rxuserclk2_out          => open,
    resetdone               => phy_resetdone,
    pma_reset_out           => pma_reset_ongoing,
    mmcm_locked_out         => eth_mmcm_locked,
    sgmii_clk_r             => open,
    sgmii_clk_f             => open,
    sgmii_clk_en            => open,
    gmii_txd                => gmii_txd,
    gmii_tx_en              => gmii_tx_en,
    gmii_tx_er              => gmii_tx_er,
    gmii_rxd                => gmii_rxd,
    gmii_rx_dv              => gmii_rx_dv,
    gmii_rx_er              => gmii_rx_er,
    gmii_isolate            => open,
    configuration_vector    => "10001",
    an_interrupt            => open,
    an_adv_config_vector    => x"4001",
    an_restart_config       => '0',
    speed_is_10_100         => '0',
    speed_is_100            => '0',
    status_vector           => status_vector,
    reset                   => reset_sgmii_phy,
    signal_detect           => '1',
    gt0_qplloutclk_out      => open,
    gt0_qplloutrefclk_out   => open
);



U_MAC: entity work.tri_mode_ethernet_mac_1000mbit
PORT MAP (
    gtx_clk                 => ethclk125_i,
    glbl_rstn               => reset_mac_glb_n,
    rx_axi_rstn             => reset_mac_rx_n,
    tx_axi_rstn             => reset_mac_tx_n,
    rx_statistics_vector    => open,
    rx_statistics_valid     => open,
    rx_mac_aclk             => open,
    rx_reset                => open,
    rx_axis_mac_tdata       => mac_rx_data,
    rx_axis_mac_tvalid      => mac_rx_valid,
    rx_axis_mac_tlast       => mac_rx_last,
    rx_axis_mac_tuser       => mac_rx_error,
    tx_ifg_delay            => x"00",
    tx_statistics_vector    => open,
    tx_statistics_valid     => open,
    tx_mac_aclk             => open,
    tx_reset                => open,
    tx_axis_mac_tdata       => mac_tx_data,
    tx_axis_mac_tvalid      => mac_tx_valid,
    tx_axis_mac_tlast       => mac_tx_last,
    tx_axis_mac_tuser(0)    => mac_tx_error,
    tx_axis_mac_tready      => mac_tx_ready,
    pause_req               => '0',
    pause_val               => x"0000",
    speedis100              => open,
    speedis10100            => open,
    gmii_txd                => gmii_txd,
    gmii_tx_en              => gmii_tx_en,
    gmii_tx_er              => gmii_tx_er,
    gmii_rxd                => gmii_rxd,
    gmii_rx_dv              => gmii_rx_dv,
    gmii_rx_er              => gmii_rx_er,
    rx_configuration_vector => x"0000_0000_0000_0000_0812",
    tx_configuration_vector => x"0000_0000_0000_0000_0012"
);



    ethclk125 <= ethclk125_i;
    eth_locked <= '1' when phy_resetdone='1' and eth_mmcm_locked='1' else '0';

    eth_status(15 downto 0) <= status_vector;
    eth_status(         16) <= phy_resetdone;
    eth_status(         17) <= pma_reset_ongoing; 
    eth_status(         18) <= eth_mmcm_locked;

    


end architecture ethernet_sgmii;
