#set_system_jitter 0.4
create_clock  -period 16.000 [get_pins -hier -filter {name =~  *pcs_pma_block_i/transceiver_inst/gtwizard_inst/*/gtwizard_i/gt0_GTWIZARD_i/gtxe2_i/TXOUTCLK}]


set_property PACKAGE_PIN G29 [get_ports ATCA_HRDW_ADDR[0]]
set_property IOSTANDARD LVCMOS33 [get_ports ATCA_HRDW_ADDR[0]]
set_property PACKAGE_PIN H30 [get_ports ATCA_HRDW_ADDR[1]]
set_property IOSTANDARD LVCMOS33 [get_ports ATCA_HRDW_ADDR[1]]
set_property PACKAGE_PIN H27 [get_ports ATCA_HRDW_ADDR[2]]
set_property IOSTANDARD LVCMOS33 [get_ports ATCA_HRDW_ADDR[2]]
set_property PACKAGE_PIN E30 [get_ports ATCA_HRDW_ADDR[3]]
set_property IOSTANDARD LVCMOS33 [get_ports ATCA_HRDW_ADDR[3]]
set_property PACKAGE_PIN F30 [get_ports ATCA_HRDW_ADDR[4]]
set_property IOSTANDARD LVCMOS33 [get_ports ATCA_HRDW_ADDR[4]]
set_property PACKAGE_PIN E29 [get_ports ATCA_HRDW_ADDR[5]]
set_property IOSTANDARD LVCMOS33 [get_ports ATCA_HRDW_ADDR[5]]
set_property PACKAGE_PIN F28 [get_ports ATCA_HRDW_ADDR[6]]
set_property IOSTANDARD LVCMOS33 [get_ports ATCA_HRDW_ADDR[6]]
set_property PACKAGE_PIN G30 [get_ports ATCA_HRDW_ADDR[7]]
set_property IOSTANDARD LVCMOS33 [get_ports ATCA_HRDW_ADDR[7]]

set_property PACKAGE_PIN C8 [get_ports MGT7_CLK_P]
create_clock -name MGT7_CLK_P -period 8.000 [get_ports MGT7_CLK_P]

set_property PACKAGE_PIN L8 [get_ports MGT5_CLK_P]
create_clock -name MGT5_CLK_P -period 10.000 [get_ports MGT5_CLK_P]

set_property PACKAGE_PIN J8 [get_ports MGT2_CLK_P]
create_clock -name MGT2_CLK_P -period 6.238 [get_ports MGT2_CLK_P]

set_property PACKAGE_PIN A8 [get_ports K7_MGTRX_3_P]
set_property PACKAGE_PIN A4 [get_ports K7_MGTTX_3_P]


###rodclk

set_property LOC GTXE2_CHANNEL_X0Y11 [get_cells rod_with_slink/DISABLE_GTX_WHEN_SIM.new_gtx_slink_i/U0/slink_gtx_vivado_init_i/slink_gtx_vivado_i/gt0_slink_gtx_vivado_i/gtxe2_i]
set_property LOC GTXE2_CHANNEL_X0Y10 [get_cells rod_with_slink/DISABLE_GTX_WHEN_SIM.new_gtx_slink_i/U0/slink_gtx_vivado_init_i/slink_gtx_vivado_i/gt1_slink_gtx_vivado_i/gtxe2_i]

create_clock -period 10.0 [get_pins -hier -filter {name=~*gt0_slink_gtx_vivado_i*gtxe2_i*TXOUTCLK}]
create_clock -period 10.0 [get_pins -hier -filter {name=~*gt1_slink_gtx_vivado_i*gtxe2_i*TXOUTCLK}]

#set_property PACKAGE_PIN F6 [get_ports {OPTO_KR1_P[0]}]
#set_property PACKAGE_PIN F2 [get_ports {OPTO_KT1_P[0]}]
#set_property PACKAGE_PIN G4 [get_ports {OPTO_KR1_P[1]}]
#set_property PACKAGE_PIN H2 [get_ports {OPTO_KT1_P[1]}]
#set_property PACKAGE_PIN H6 [get_ports {OPTO_KR1_P[2]}]
#set_property PACKAGE_PIN J4 [get_ports {OPTO_KT1_P[2]}]
#set_property PACKAGE_PIN K6 [get_ports {OPTO_KR1_P[3]}]
#set_property PACKAGE_PIN K2 [get_ports {OPTO_KT1_P[3]}]
#NET "KINTEX_CPLD[0]" LOC = F17 | IOSTANDARD = LVCMOS33 | PULLUP;

set_property LOC GTXE2_CHANNEL_X0Y12 [get_cells rod_with_slink/ROD_GTX_INST/rod_gth_i/gt0_rod_gth_i/gtxe2_i]
set_property LOC GTXE2_CHANNEL_X0Y14 [get_cells rod_with_slink/ROD_GTX_INST/rod_gth_i/gt1_rod_gth_i/gtxe2_i]

create_clock -name rod_gtx_rxusrclk_in0 -period 3.117 -waveform {0.000 1.559} [get_pins {rod_with_slink/ROD_GTX_INST/rod_gth_i/gt0_rod_gth_i/gtxe2_i/RXOUTCLK}]
create_clock -name rod_gtx_rxusrclk_in1 -period 3.117 -waveform {0.000 1.559} [get_pins {rod_with_slink/ROD_GTX_INST/rod_gth_i/gt1_rod_gth_i/gtxe2_i/RXOUTCLK}]

set_property PACKAGE_PIN C22 [get_ports TEST_OUT]
set_property IOSTANDARD LVCMOS33 [get_ports TEST_OUT]

set_property PACKAGE_PIN AF26 [get_ports TTC_BCSTR1_IN]
set_property IOSTANDARD LVCMOS33 [get_ports TTC_BCSTR1_IN]
set_property PACKAGE_PIN AC26 [get_ports TTC_BCSTR2_IN]
set_property IOSTANDARD LVCMOS33 [get_ports TTC_BCSTR2_IN]
set_property PACKAGE_PIN AG30 [get_ports {TTC_BRCST_IN[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {TTC_BRCST_IN[0]}]
set_property PACKAGE_PIN AJ27 [get_ports {TTC_BRCST_IN[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {TTC_BRCST_IN[1]}]
set_property PACKAGE_PIN AF28 [get_ports {TTC_BRCST_IN[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {TTC_BRCST_IN[2]}]
set_property PACKAGE_PIN AH26 [get_ports {TTC_BRCST_IN[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {TTC_BRCST_IN[3]}]
set_property PACKAGE_PIN AF30 [get_ports {TTC_BRCST_IN[4]}]
set_property IOSTANDARD LVCMOS33 [get_ports {TTC_BRCST_IN[4]}]
set_property PACKAGE_PIN AJ26 [get_ports {TTC_BRCST_IN[5]}]
set_property IOSTANDARD LVCMOS33 [get_ports {TTC_BRCST_IN[5]}]
set_property PACKAGE_PIN AK26 [get_ports TTC_BCNRST_IN]
set_property IOSTANDARD LVCMOS33 [get_ports TTC_BCNRST_IN]
set_property PACKAGE_PIN AG29 [get_ports TTC_EVTCNTRRST_IN]
set_property IOSTANDARD LVCMOS33 [get_ports TTC_EVTCNTRRST_IN]
set_property PACKAGE_PIN AE26 [get_ports TTC_EVT_H_STR_IN]
set_property IOSTANDARD LVCMOS33 [get_ports TTC_EVT_H_STR_IN]
set_property PACKAGE_PIN AH30 [get_ports TTC_L1A_IN]
set_property IOSTANDARD LVCMOS33 [get_ports TTC_L1A_IN]
set_property PACKAGE_PIN AK28 [get_ports TTC_BCNT_STR_IN]
set_property IOSTANDARD LVCMOS33 [get_ports TTC_BCNT_STR_IN]
set_property PACKAGE_PIN AE28 [get_ports TTC_EVT_L_STR_IN]
set_property IOSTANDARD LVCMOS33 [get_ports TTC_EVT_L_STR_IN]
set_property PACKAGE_PIN AE29 [get_ports {TTC_BCNT_IN[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {TTC_BCNT_IN[0]}]
set_property PACKAGE_PIN AJ28 [get_ports {TTC_BCNT_IN[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {TTC_BCNT_IN[1]}]
set_property PACKAGE_PIN AG28 [get_ports {TTC_BCNT_IN[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {TTC_BCNT_IN[2]}]
set_property PACKAGE_PIN AK29 [get_ports {TTC_BCNT_IN[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {TTC_BCNT_IN[3]}]
set_property PACKAGE_PIN AE30 [get_ports {TTC_BCNT_IN[4]}]
set_property IOSTANDARD LVCMOS33 [get_ports {TTC_BCNT_IN[4]}]
set_property PACKAGE_PIN AD27 [get_ports {TTC_BCNT_IN[5]}]
set_property IOSTANDARD LVCMOS33 [get_ports {TTC_BCNT_IN[5]}]
set_property PACKAGE_PIN AD28 [get_ports {TTC_BCNT_IN[6]}]
set_property IOSTANDARD LVCMOS33 [get_ports {TTC_BCNT_IN[6]}]
set_property PACKAGE_PIN AK30 [get_ports {TTC_BCNT_IN[7]}]
set_property IOSTANDARD LVCMOS33 [get_ports {TTC_BCNT_IN[7]}]
set_property PACKAGE_PIN AH29 [get_ports {TTC_BCNT_IN[8]}]
set_property IOSTANDARD LVCMOS33 [get_ports {TTC_BCNT_IN[8]}]
set_property PACKAGE_PIN AJ29 [get_ports {TTC_BCNT_IN[9]}]
set_property IOSTANDARD LVCMOS33 [get_ports {TTC_BCNT_IN[9]}]
set_property PACKAGE_PIN AD29 [get_ports {TTC_BCNT_IN[10]}]
set_property IOSTANDARD LVCMOS33 [get_ports {TTC_BCNT_IN[10]}]
set_property PACKAGE_PIN AG27 [get_ports {TTC_BCNT_IN[11]}]
set_property IOSTANDARD LVCMOS33 [get_ports {TTC_BCNT_IN[11]}]
set_property PACKAGE_PIN T30 [get_ports TTC_CTRL_23]
set_property IOSTANDARD LVCMOS33 [get_ports TTC_CTRL_23]
set_property PACKAGE_PIN V29 [get_ports TTC_CTRL_22]
set_property IOSTANDARD LVCMOS33 [get_ports TTC_CTRL_22]
set_property PACKAGE_PIN V30 [get_ports {TTC_DOUT_IN[7]}]
set_property IOSTANDARD LVCMOS33 [get_ports {TTC_DOUT_IN[7]}]
set_property PACKAGE_PIN W27 [get_ports {TTC_DOUT_IN[6]}]
set_property IOSTANDARD LVCMOS33 [get_ports {TTC_DOUT_IN[6]}]
set_property PACKAGE_PIN T25 [get_ports {TTC_DOUT_IN[5]}]
set_property IOSTANDARD LVCMOS33 [get_ports {TTC_DOUT_IN[5]}]
set_property PACKAGE_PIN AC30 [get_ports {TTC_DOUT_IN[4]}]
set_property IOSTANDARD LVCMOS33 [get_ports {TTC_DOUT_IN[4]}]
set_property PACKAGE_PIN V26 [get_ports {TTC_DOUT_IN[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {TTC_DOUT_IN[3]}]
set_property PACKAGE_PIN Y30 [get_ports {TTC_DOUT_IN[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {TTC_DOUT_IN[2]}]
set_property PACKAGE_PIN R30 [get_ports {TTC_DOUT_IN[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {TTC_DOUT_IN[1]}]
set_property PACKAGE_PIN AA27 [get_ports {TTC_DOUT_IN[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {TTC_DOUT_IN[0]}]
set_property PACKAGE_PIN V25 [get_ports {TTC_SUBADDR_IN[7]}]
set_property IOSTANDARD LVCMOS33 [get_ports {TTC_SUBADDR_IN[7]}]
set_property PACKAGE_PIN AB28 [get_ports {TTC_SUBADDR_IN[6]}]
set_property IOSTANDARD LVCMOS33 [get_ports {TTC_SUBADDR_IN[6]}]
set_property PACKAGE_PIN U29 [get_ports {TTC_SUBADDR_IN[5]}]
set_property IOSTANDARD LVCMOS33 [get_ports {TTC_SUBADDR_IN[5]}]
set_property PACKAGE_PIN Y29 [get_ports {TTC_SUBADDR_IN[4]}]
set_property IOSTANDARD LVCMOS33 [get_ports {TTC_SUBADDR_IN[4]}]
set_property PACKAGE_PIN U24 [get_ports {TTC_SUBADDR_IN[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {TTC_SUBADDR_IN[3]}]
set_property PACKAGE_PIN AB30 [get_ports {TTC_SUBADDR_IN[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {TTC_SUBADDR_IN[2]}]
set_property PACKAGE_PIN U25 [get_ports {TTC_SUBADDR_IN[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {TTC_SUBADDR_IN[1]}]
set_property PACKAGE_PIN Y28 [get_ports {TTC_SUBADDR_IN[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {TTC_SUBADDR_IN[0]}]
set_property PACKAGE_PIN AA28 [get_ports TTC_DOUT_STR_IN]
set_property IOSTANDARD LVCMOS33 [get_ports TTC_DOUT_STR_IN]

set_property PACKAGE_PIN W28 [get_ports TTC_RESET_OUT]
set_property IOSTANDARD LVCMOS33 [get_ports TTC_RESET_OUT]

# TTCDEC P/D
set_property PACKAGE_PIN V27 [get_ports TTC_CTRL_26]
set_property IOSTANDARD LVCMOS33 [get_ports TTC_CTRL_26]
# TTCDEC CLKSEL
set_property PACKAGE_PIN R29 [get_ports TTC_CTRL_27]
set_property IOSTANDARD LVCMOS33 [get_ports TTC_CTRL_27]

# I2C
# TTCDEC SCL
set_property PACKAGE_PIN AC29 [get_ports TTC_CTRL_24]
set_property IOSTANDARD LVCMOS33 [get_ports TTC_CTRL_24]
# TTCDEC SDA
set_property PACKAGE_PIN W29 [get_ports TTC_CTRL_25]
set_property IOSTANDARD LVCMOS33 [get_ports TTC_CTRL_25]
set_property PACKAGE_PIN F13 [get_ports {AVAGO_SCL[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {AVAGO_SCL[0]}]
set_property PACKAGE_PIN F15 [get_ports {AVAGO_SDA[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {AVAGO_SDA[0]}]
set_property PACKAGE_PIN H14 [get_ports {AVAGO_SCL[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {AVAGO_SCL[1]}]
set_property PACKAGE_PIN K16 [get_ports {AVAGO_SDA[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {AVAGO_SDA[1]}]
set_property PACKAGE_PIN K11 [get_ports {AVAGO_SCL[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {AVAGO_SCL[2]}]
set_property PACKAGE_PIN J13 [get_ports {AVAGO_SDA[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {AVAGO_SDA[2]}]
set_property PACKAGE_PIN L16 [get_ports POWER_SCL]
set_property IOSTANDARD LVCMOS33 [get_ports POWER_SCL]
set_property PACKAGE_PIN L15 [get_ports POWER_SDA]
set_property IOSTANDARD LVCMOS33 [get_ports POWER_SDA]


# SerialID-Chip DS2411 (U89)
set_property PACKAGE_PIN H19 [get_ports DS2411]
set_property IOSTANDARD LVCMOS33 [get_ports DS2411]



# CTRLBUS_P<0>, IO_L14N_T2_SRCC_32 , inverted
set_property PACKAGE_PIN AD16 [get_ports {CTRLBUS_U1_OUT_N[0]}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U1_OUT_N[0]}]
set_property DIFF_TERM FALSE [get_ports {CTRLBUS_U1_OUT_N[0]}]
# CTRLBUS_N<0>, IO_L14P_T2_SRCC_32 , inverted
set_property PACKAGE_PIN AD17 [get_ports {CTRLBUS_U1_OUT_P[0]}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U1_OUT_P[0]}]
set_property DIFF_TERM FALSE [get_ports {CTRLBUS_U1_OUT_P[0]}]
# CTRLBUS_P<1>, IO_L17N_T2_32 , inverted
set_property PACKAGE_PIN AC19 [get_ports {CTRLBUS_U1_OUT_N[1]}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U1_OUT_N[1]}]
set_property DIFF_TERM FALSE [get_ports {CTRLBUS_U1_OUT_N[1]}]
# CTRLBUS_N<1>, IO_L17P_T2_32 , inverted
set_property PACKAGE_PIN AB19 [get_ports {CTRLBUS_U1_OUT_P[1]}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U1_OUT_P[1]}]
set_property DIFF_TERM FALSE [get_ports {CTRLBUS_U1_OUT_P[1]}]
# CTRLBUS_P<2>, IO_L15N_T2_DQS_32 , inverted
set_property PACKAGE_PIN Y18 [get_ports {CTRLBUS_U1_OUT_N[2]}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U1_OUT_N[2]}]
set_property DIFF_TERM FALSE [get_ports {CTRLBUS_U1_OUT_N[2]}]
# CTRLBUS_N<2>, IO_L15P_T2_DQS_32 , inverted
set_property PACKAGE_PIN Y19 [get_ports {CTRLBUS_U1_OUT_P[2]}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U1_OUT_P[2]}]
set_property DIFF_TERM FALSE [get_ports {CTRLBUS_U1_OUT_P[2]}]
# CTRLBUS_P<3>, IO_L16N_T2_32 , inverted
set_property PACKAGE_PIN AB18 [get_ports {CTRLBUS_U1_OUT_N[3]}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U1_OUT_N[3]}]
set_property DIFF_TERM FALSE [get_ports {CTRLBUS_U1_OUT_N[3]}]
# CTRLBUS_N<3>, IO_L16P_T2_32 , inverted
set_property PACKAGE_PIN AA18 [get_ports {CTRLBUS_U1_OUT_P[3]}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U1_OUT_P[3]}]
set_property DIFF_TERM FALSE [get_ports {CTRLBUS_U1_OUT_P[3]}]
# CTRLBUS_P<4>, IO_L13N_T2_MRCC_32 , inverted
set_property PACKAGE_PIN AE18 [get_ports {CTRLBUS_U1_OUT_N[4]}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U1_OUT_N[4]}]
set_property DIFF_TERM FALSE [get_ports {CTRLBUS_U1_OUT_N[4]}]
# CTRLBUS_N<4>, IO_L13P_T2_MRCC_32 , inverted
set_property PACKAGE_PIN AD18 [get_ports {CTRLBUS_U1_OUT_P[4]}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U1_OUT_P[4]}]
set_property DIFF_TERM FALSE [get_ports {CTRLBUS_U1_OUT_P[4]}]
# CTRLBUS_P<5>, IO_L21N_T3_DQS_32 , inverted
set_property PACKAGE_PIN AC15 [get_ports {CTRLBUS_U1_OUT_N[5]}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U1_OUT_N[5]}]
set_property DIFF_TERM FALSE [get_ports {CTRLBUS_U1_OUT_N[5]}]
# CTRLBUS_N<5>, IO_L21P_T3_DQS_32 , inverted
set_property PACKAGE_PIN AC16 [get_ports {CTRLBUS_U1_OUT_P[5]}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U1_OUT_P[5]}]
set_property DIFF_TERM FALSE [get_ports {CTRLBUS_U1_OUT_P[5]}]
## CTRLBUS_P<6>, IO_L10N_T1_32 , inverted
#NET "CTRLBUS_U1_OUT_N[6]" LOC = AE19;
#NET "CTRLBUS_U1_OUT_N[6]" IOSTANDARD = LVDS;
#NET "CTRLBUS_U1_OUT_N[6]" DIFF_TERM = "FALSE";
## CTRLBUS_N<6>, IO_L10P_T1_32 , inverted
#NET "CTRLBUS_U1_OUT_P[6]" LOC = AD19;
#NET "CTRLBUS_U1_OUT_P[6]" IOSTANDARD = LVDS;
#NET "CTRLBUS_U1_OUT_P[6]" DIFF_TERM = "FALSE";

# CTRLBUS_P<6>, IO_L10N_T1_32 , inverted
set_property PACKAGE_PIN AE19 [get_ports CTRLBUS_U1_IN_N_6]
set_property IOSTANDARD LVDS [get_ports CTRLBUS_U1_IN_N_6]
set_property DIFF_TERM TRUE [get_ports CTRLBUS_U1_IN_N_6]
# CTRLBUS_N<6>, IO_L10P_T1_32 , inverted
set_property PACKAGE_PIN AD19 [get_ports CTRLBUS_U1_IN_P_6]
set_property IOSTANDARD LVDS [get_ports CTRLBUS_U1_IN_P_6]
set_property DIFF_TERM TRUE [get_ports CTRLBUS_U1_IN_P_6]

#NET "CTRLBUS_U1_IN_N_7"	LOC = AC17	| IOSTANDARD = LVDS	 | DIFF_TERM = TRUE;		# CTRLBUS_P<7>, IO_L18N_T2_32 , inverted
#NET "CTRLBUS_U1_IN_P_7"	LOC = AB17	| IOSTANDARD = LVDS	 | DIFF_TERM = TRUE;		# CTRLBUS_N<7>, IO_L18P_T2_32 , inverted

# CTRLBUS_P<8>, IO_L6P_T0_32 
set_property PACKAGE_PIN AE16 [get_ports CTRLBUS_U1_OUT_P_8]
set_property IOSTANDARD LVDS [get_ports CTRLBUS_U1_OUT_P_8]
set_property DIFF_TERM FALSE [get_ports CTRLBUS_U1_OUT_P_8]
# CTRLBUS_N<8>, IO_L6N_T0_VREF_32 
set_property PACKAGE_PIN AF16 [get_ports CTRLBUS_U1_OUT_N_8]
set_property IOSTANDARD LVDS [get_ports CTRLBUS_U1_OUT_N_8]
set_property DIFF_TERM FALSE [get_ports CTRLBUS_U1_OUT_N_8]

#Constraint to set delay L1A to processor U1

#NET "CTRLBUS_U1_OUT_P_8" OFFSET = OUT 11 ns AFTER "CTRLBUS_U1_IN_P[12]";

# CTRLBUS_P<9>, IO_L20N_T3_32 , inverted
#set_property PACKAGE_PIN AB15 [get_ports {CTRLBUS_U1_IN_N[9]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U1_IN_N[9]}]
#set_property DIFF_TERM TRUE [get_ports {CTRLBUS_U1_IN_N[9]}]
# CTRLBUS_N<9>, IO_L20P_T3_32 , inverted
#set_property PACKAGE_PIN AA15 [get_ports {CTRLBUS_U1_IN_P[9]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U1_IN_P[9]}]
#set_property DIFF_TERM TRUE [get_ports {CTRLBUS_U1_IN_P[9]}]
# CTRLBUS_P<10>, IO_L8P_T1_32 
#set_property PACKAGE_PIN AG19 [get_ports {CTRLBUS_U1_IN_P[10]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U1_IN_P[10]}]
#set_property DIFF_TERM TRUE [get_ports {CTRLBUS_U1_IN_P[10]}]
# CTRLBUS_N<10>, IO_L8N_T1_32 
#set_property PACKAGE_PIN AH19 [get_ports {CTRLBUS_U1_IN_N[10]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U1_IN_N[10]}]
#set_property DIFF_TERM TRUE [get_ports {CTRLBUS_U1_IN_N[10]}]
# CTRLBUS_P<11>, IO_L11N_T1_SRCC_32 , inverted
#set_property PACKAGE_PIN AG18 [get_ports {CTRLBUS_U1_IN_N[11]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U1_IN_N[11]}]
#set_property DIFF_TERM TRUE [get_ports {CTRLBUS_U1_IN_N[11]}]
# CTRLBUS_N<11>, IO_L11P_T1_SRCC_32 , inverted
#set_property PACKAGE_PIN AF18 [get_ports {CTRLBUS_U1_IN_P[11]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U1_IN_P[11]}]
#set_property DIFF_TERM TRUE [get_ports {CTRLBUS_U1_IN_P[11]}]
# CTRLBUS_P<12>, IO_L12P_T1_MRCC_32 
set_property PACKAGE_PIN AF17 [get_ports {CTRLBUS_U1_IN_P_12}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U1_IN_P_12}]
set_property DIFF_TERM TRUE [get_ports {CTRLBUS_U1_IN_P_12}]
# CTRLBUS_N<12>, IO_L12N_T1_MRCC_32 
set_property PACKAGE_PIN AG17 [get_ports {CTRLBUS_U1_IN_N_12}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U1_IN_N_12}]
set_property DIFF_TERM TRUE [get_ports {CTRLBUS_U1_IN_N_12}]

create_clock -name {CTRLBUS_U1_IN_P_12} -period 24.950 [get_ports {CTRLBUS_U1_IN_P[12]}]

# CTRLBUS_P<13>, IO_L9P_T1_DQS_32 
#set_property PACKAGE_PIN AJ18 [get_ports {CTRLBUS_U1_IN_P[13]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U1_IN_P[13]}]
#set_property DIFF_TERM TRUE [get_ports {CTRLBUS_U1_IN_P[13]}]
# CTRLBUS_N<13>, IO_L9N_T1_DQS_32 
#set_property PACKAGE_PIN AK18 [get_ports {CTRLBUS_U1_IN_N[13]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U1_IN_N[13]}]
#set_property DIFF_TERM TRUE [get_ports {CTRLBUS_U1_IN_N[13]}]
# CTRLBUS_P<14>, IO_L7P_T1_32 
#set_property PACKAGE_PIN AJ19 [get_ports {CTRLBUS_U1_IN_P[14]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U1_IN_P[14]}]
#set_property DIFF_TERM TRUE [get_ports {CTRLBUS_U1_IN_P[14]}]
# CTRLBUS_N<14>, IO_L7N_T1_32 
#set_property PACKAGE_PIN AK19 [get_ports {CTRLBUS_U1_IN_N[14]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U1_IN_N[14]}]
#set_property DIFF_TERM TRUE [get_ports {CTRLBUS_U1_IN_N[14]}]
# CTRLBUS_P<15>, IO_L5P_T0_32 
#set_property PACKAGE_PIN AH17 [get_ports {CTRLBUS_U1_IN_P[15]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U1_IN_P[15]}]
#set_property DIFF_TERM TRUE [get_ports {CTRLBUS_U1_IN_P[15]}]
# CTRLBUS_N<15>, IO_L5N_T0_32 
#set_property PACKAGE_PIN AJ17 [get_ports {CTRLBUS_U1_IN_N[15]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U1_IN_N[15]}]
#set_property DIFF_TERM TRUE [get_ports {CTRLBUS_U1_IN_N[15]}]
# CTRLBUS_P<16>, IO_L4N_T0_32 , inverted
#set_property PACKAGE_PIN AG14 [get_ports {CTRLBUS_U1_IN_N[16]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U1_IN_N[16]}]
#set_property DIFF_TERM TRUE [get_ports {CTRLBUS_U1_IN_N[16]}]
# CTRLBUS_N<16>, IO_L4P_T0_32 , inverted
#set_property PACKAGE_PIN AF15 [get_ports {CTRLBUS_U1_IN_P[16]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U1_IN_P[16]}]
#set_property DIFF_TERM TRUE [get_ports {CTRLBUS_U1_IN_P[16]}]
# CTRLBUS_P<17>, IO_L2P_T0_32 
#set_property PACKAGE_PIN AG15 [get_ports {CTRLBUS_U1_IN_P[17]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U1_IN_P[17]}]
#set_property DIFF_TERM TRUE [get_ports {CTRLBUS_U1_IN_P[17]}]
# CTRLBUS_N<17>, IO_L2N_T0_32 
#set_property PACKAGE_PIN AH15 [get_ports {CTRLBUS_U1_IN_N[17]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U1_IN_N[17]}]
#set_property DIFF_TERM TRUE [get_ports {CTRLBUS_U1_IN_N[17]}]
# CTRLBUS_P<18>, IO_L19P_T3_32 
set_property PACKAGE_PIN AE15 [get_ports {CTRLBUS_U1_IN_P[18]}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U1_IN_P[18]}]
set_property DIFF_TERM TRUE [get_ports {CTRLBUS_U1_IN_P[18]}]
# CTRLBUS_N<18>, IO_L19N_T3_VREF_32 
set_property PACKAGE_PIN AE14 [get_ports {CTRLBUS_U1_IN_N[18]}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U1_IN_N[18]}]
set_property DIFF_TERM TRUE [get_ports {CTRLBUS_U1_IN_N[18]}]
# CTRLBUS_P<19>, IO_L1N_T0_32 , inverted
set_property PACKAGE_PIN AK15 [get_ports {CTRLBUS_U1_IN_N[19]}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U1_IN_N[19]}]
set_property DIFF_TERM TRUE [get_ports {CTRLBUS_U1_IN_N[19]}]
# CTRLBUS_N<19>, IO_L1P_T0_32 , inverted
set_property PACKAGE_PIN AK16 [get_ports {CTRLBUS_U1_IN_P[19]}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U1_IN_P[19]}]
set_property DIFF_TERM TRUE [get_ports {CTRLBUS_U1_IN_P[19]}]
# CTRLBUS_P<20>, IO_L3N_T0_DQS_32 , inverted
set_property PACKAGE_PIN AJ16 [get_ports {CTRLBUS_U1_IN_N[20]}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U1_IN_N[20]}]
set_property DIFF_TERM TRUE [get_ports {CTRLBUS_U1_IN_N[20]}]
# CTRLBUS_N<20>, IO_L3P_T0_DQS_32 , inverted
set_property PACKAGE_PIN AH16 [get_ports {CTRLBUS_U1_IN_P[20]}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U1_IN_P[20]}]
set_property DIFF_TERM TRUE [get_ports {CTRLBUS_U1_IN_P[20]}]


# CTRLBUS_P<21>, IO_L10N_T1_33 , inverted
set_property PACKAGE_PIN AE9 [get_ports {CTRLBUS_U2_OUT_N[21]}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U2_OUT_N[21]}]
set_property DIFF_TERM FALSE [get_ports {CTRLBUS_U2_OUT_N[21]}]
# CTRLBUS_N<21>, IO_L10P_T1_33 , inverted
set_property PACKAGE_PIN AD9 [get_ports {CTRLBUS_U2_OUT_P[21]}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U2_OUT_P[21]}]
set_property DIFF_TERM FALSE [get_ports {CTRLBUS_U2_OUT_P[21]}]
# CTRLBUS_P<22>, IO_L8P_T1_33 
set_property PACKAGE_PIN AD8 [get_ports {CTRLBUS_U2_OUT_P[22]}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U2_OUT_P[22]}]
set_property DIFF_TERM FALSE [get_ports {CTRLBUS_U2_OUT_P[22]}]
# CTRLBUS_N<22>, IO_L8N_T1_33 
set_property PACKAGE_PIN AE8 [get_ports {CTRLBUS_U2_OUT_N[22]}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U2_OUT_N[22]}]
set_property DIFF_TERM FALSE [get_ports {CTRLBUS_U2_OUT_N[22]}]
# CTRLBUS_P<23>, IO_L14N_T2_SRCC_33 , inverted
set_property PACKAGE_PIN AF10 [get_ports {CTRLBUS_U2_OUT_N[23]}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U2_OUT_N[23]}]
set_property DIFF_TERM FALSE [get_ports {CTRLBUS_U2_OUT_N[23]}]
# CTRLBUS_N<23>, IO_L14P_T2_SRCC_33 , inverted
set_property PACKAGE_PIN AE10 [get_ports {CTRLBUS_U2_OUT_P[23]}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U2_OUT_P[23]}]
set_property DIFF_TERM FALSE [get_ports {CTRLBUS_U2_OUT_P[23]}]
# CTRLBUS_P<24>, IO_L16P_T2_33 
set_property PACKAGE_PIN AG9 [get_ports {CTRLBUS_U2_OUT_P[24]}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U2_OUT_P[24]}]
set_property DIFF_TERM FALSE [get_ports {CTRLBUS_U2_OUT_P[24]}]
# CTRLBUS_N<24>, IO_L16N_T2_33 
set_property PACKAGE_PIN AH9 [get_ports {CTRLBUS_U2_OUT_N[24]}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U2_OUT_N[24]}]
set_property DIFF_TERM FALSE [get_ports {CTRLBUS_U2_OUT_N[24]}]
# CTRLBUS_P<25>, IO_L18N_T2_33 , inverted
set_property PACKAGE_PIN AJ11 [get_ports {CTRLBUS_U2_OUT_N[25]}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U2_OUT_N[25]}]
set_property DIFF_TERM FALSE [get_ports {CTRLBUS_U2_OUT_N[25]}]
# CTRLBUS_N<25>, IO_L18P_T2_33 , inverted
set_property PACKAGE_PIN AH11 [get_ports {CTRLBUS_U2_OUT_P[25]}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U2_OUT_P[25]}]
set_property DIFF_TERM FALSE [get_ports {CTRLBUS_U2_OUT_P[25]}]
# CTRLBUS_P<26>, IO_L15N_T2_DQS_33 , inverted
set_property PACKAGE_PIN AK9 [get_ports {CTRLBUS_U2_OUT_N[26]}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U2_OUT_N[26]}]
set_property DIFF_TERM FALSE [get_ports {CTRLBUS_U2_OUT_N[26]}]
# CTRLBUS_N<26>, IO_L15P_T2_DQS_33 , inverted
set_property PACKAGE_PIN AJ9 [get_ports {CTRLBUS_U2_OUT_P[26]}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U2_OUT_P[26]}]
set_property DIFF_TERM FALSE [get_ports {CTRLBUS_U2_OUT_P[26]}]
# CTRLBUS_P<27>, IO_L12P_T1_MRCC_33 
set_property PACKAGE_PIN AD12 [get_ports CTRLBUS_U2_IN_P_27]
set_property IOSTANDARD LVDS [get_ports CTRLBUS_U2_IN_P_27]
set_property DIFF_TERM TRUE [get_ports CTRLBUS_U2_IN_P_27]
# CTRLBUS_N<27>, IO_L12N_T1_MRCC_33 
set_property PACKAGE_PIN AD11 [get_ports CTRLBUS_U2_IN_N_27]
set_property IOSTANDARD LVDS [get_ports CTRLBUS_U2_IN_N_27]
set_property DIFF_TERM TRUE [get_ports CTRLBUS_U2_IN_N_27]
#Constraint to set delay L1A to processor U2


# CTRLBUS_P<28>, IO_L13N_T2_MRCC_33 , inverted
set_property PACKAGE_PIN AH10 [get_ports CTRLBUS_U2_IN_N_28]
set_property IOSTANDARD LVDS [get_ports CTRLBUS_U2_IN_N_28]
set_property DIFF_TERM TRUE [get_ports CTRLBUS_U2_IN_N_28]
# CTRLBUS_N<28>, IO_L13P_T2_MRCC_33 , inverted
set_property PACKAGE_PIN AG10 [get_ports CTRLBUS_U2_IN_P_28]
set_property IOSTANDARD LVDS [get_ports CTRLBUS_U2_IN_P_28]
set_property DIFF_TERM TRUE [get_ports CTRLBUS_U2_IN_P_28]

create_clock -period 24.950 [get_ports CTRLBUS_U2_IN_P_28]



# CTRLBUS_P<29>, IO_L2P_T0_33 
set_property PACKAGE_PIN AA8 [get_ports CTRLBUS_U2_OUT_P_29]
set_property IOSTANDARD LVDS [get_ports CTRLBUS_U2_OUT_P_29]
set_property DIFF_TERM FALSE [get_ports CTRLBUS_U2_OUT_P_29]
# CTRLBUS_N<29>, IO_L2N_T0_33 
set_property PACKAGE_PIN AB8 [get_ports CTRLBUS_U2_OUT_N_29]
set_property IOSTANDARD LVDS [get_ports CTRLBUS_U2_OUT_N_29]
set_property DIFF_TERM FALSE [get_ports CTRLBUS_U2_OUT_N_29]

#NET "CTRLBUS_U2_OUT_P_29" OFFSET = OUT 12 ns BEFORE "CTRLBUS_U1_IN_P[12]";

# CTRLBUS_P<30>, IO_L17P_T2_33 
#set_property PACKAGE_PIN AK11 [get_ports {CTRLBUS_U2_IN_P[30]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U2_IN_P[30]}]
#set_property DIFF_TERM TRUE [get_ports {CTRLBUS_U2_IN_P[30]}]
# CTRLBUS_N<30>, IO_L17N_T2_33 
#set_property PACKAGE_PIN AK10 [get_ports {CTRLBUS_U2_IN_N[30]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U2_IN_N[30]}]
#set_property DIFF_TERM TRUE [get_ports {CTRLBUS_U2_IN_N[30]}]
# CTRLBUS_P<31>, IO_L3N_T0_DQS_33 , inverted
#set_property PACKAGE_PIN AC9 [get_ports {CTRLBUS_U2_IN_N[31]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U2_IN_N[31]}]
#set_property DIFF_TERM TRUE [get_ports {CTRLBUS_U2_IN_N[31]}]
# CTRLBUS_N<31>, IO_L3P_T0_DQS_33 , inverted
#set_property PACKAGE_PIN AB9 [get_ports {CTRLBUS_U2_IN_P[31]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U2_IN_P[31]}]
#set_property DIFF_TERM TRUE [get_ports {CTRLBUS_U2_IN_P[31]}]
# CTRLBUS_P<32>, IO_L7P_T1_33 
#set_property PACKAGE_PIN AB10 [get_ports {CTRLBUS_U2_IN_P[32]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U2_IN_P[32]}]
#set_property DIFF_TERM TRUE [get_ports {CTRLBUS_U2_IN_P[32]}]
# CTRLBUS_N<32>, IO_L7N_T1_33 
#set_property PACKAGE_PIN AC10 [get_ports {CTRLBUS_U2_IN_N[32]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U2_IN_N[32]}]
#set_property DIFF_TERM TRUE [get_ports {CTRLBUS_U2_IN_N[32]}]
# CTRLBUS_P<33>, IO_L11N_T1_SRCC_33 , inverted
set_property PACKAGE_PIN AF11 [get_ports {CTRLBUS_U2_IN_N_33}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U2_IN_N_33}]
set_property DIFF_TERM TRUE [get_ports {CTRLBUS_U2_IN_N_33}]
# CTRLBUS_N<33>, IO_L11P_T1_SRCC_33 , inverted
set_property PACKAGE_PIN AE11 [get_ports {CTRLBUS_U2_IN_P_33}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U2_IN_P_33}]
set_property DIFF_TERM TRUE [get_ports {CTRLBUS_U2_IN_P_33}]
# CTRLBUS_P<34>, IO_L5P_T0_33 
#set_property PACKAGE_PIN AA11 [get_ports {CTRLBUS_U2_IN_P[34]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U2_IN_P[34]}]
#set_property DIFF_TERM TRUE [get_ports {CTRLBUS_U2_IN_P[34]}]
# CTRLBUS_N<34>, IO_L5N_T0_33 
#set_property PACKAGE_PIN AA10 [get_ports {CTRLBUS_U2_IN_N[34]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U2_IN_N[34]}]
#set_property DIFF_TERM TRUE [get_ports {CTRLBUS_U2_IN_N[34]}]
# CTRLBUS_P<35>, IO_L19P_T3_33 
#set_property PACKAGE_PIN AE13 [get_ports {CTRLBUS_U2_IN_P[35]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U2_IN_P[35]}]
#set_property DIFF_TERM TRUE [get_ports {CTRLBUS_U2_IN_P[35]}]
# CTRLBUS_N<35>, IO_L19N_T3_VREF_33 
#set_property PACKAGE_PIN AF13 [get_ports {CTRLBUS_U2_IN_N[35]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U2_IN_N[35]}]
#set_property DIFF_TERM TRUE [get_ports {CTRLBUS_U2_IN_N[35]}]
# CTRLBUS_P<36>, IO_L20P_T3_33 
#set_property PACKAGE_PIN AK14 [get_ports {CTRLBUS_U2_IN_P[36]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U2_IN_P[36]}]
#set_property DIFF_TERM TRUE [get_ports {CTRLBUS_U2_IN_P[36]}]
# CTRLBUS_N<36>, IO_L20N_T3_33 
#set_property PACKAGE_PIN AK13 [get_ports {CTRLBUS_U2_IN_N[36]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U2_IN_N[36]}]
#set_property DIFF_TERM TRUE [get_ports {CTRLBUS_U2_IN_N[36]}]
# CTRLBUS_P<37>, IO_L4P_T0_33 
#set_property PACKAGE_PIN Y11 [get_ports {CTRLBUS_U2_IN_P[37]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U2_IN_P[37]}]
#set_property DIFF_TERM TRUE [get_ports {CTRLBUS_U2_IN_P[37]}]
# CTRLBUS_N<37>, IO_L4N_T0_33 
#set_property PACKAGE_PIN Y10 [get_ports {CTRLBUS_U2_IN_N[37]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U2_IN_N[37]}]
#set_property DIFF_TERM TRUE [get_ports {CTRLBUS_U2_IN_N[37]}]
# CTRLBUS_P<38>, IO_L1P_T0_33 
#set_property PACKAGE_PIN AA12 [get_ports {CTRLBUS_U2_IN_P[38]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U2_IN_P[38]}]
#set_property DIFF_TERM TRUE [get_ports {CTRLBUS_U2_IN_P[38]}]
# CTRLBUS_N<38>, IO_L1N_T0_33 
#set_property PACKAGE_PIN AB12 [get_ports {CTRLBUS_U2_IN_N[38]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U2_IN_N[38]}]
#set_property DIFF_TERM TRUE [get_ports {CTRLBUS_U2_IN_N[38]}]
# CTRLBUS_P<39>, IO_L21N_T3_DQS_33 , inverted
set_property PACKAGE_PIN AJ14 [get_ports {CTRLBUS_U2_IN_N[39]}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U2_IN_N[39]}]
set_property DIFF_TERM TRUE [get_ports {CTRLBUS_U2_IN_N[39]}]
# CTRLBUS_N<39>, IO_L21P_T3_DQS_33 , inverted
set_property PACKAGE_PIN AH14 [get_ports {CTRLBUS_U2_IN_P[39]}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U2_IN_P[39]}]
set_property DIFF_TERM TRUE [get_ports {CTRLBUS_U2_IN_P[39]}]
# CTRLBUS_P<40>, IO_L6N_T0_VREF_33 , inverted
set_property PACKAGE_PIN AB13 [get_ports {CTRLBUS_U2_IN_N[40]}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U2_IN_N[40]}]
set_property DIFF_TERM TRUE [get_ports {CTRLBUS_U2_IN_N[40]}]
# CTRLBUS_N<40>, IO_L6P_T0_33 , inverted
set_property PACKAGE_PIN AA13 [get_ports {CTRLBUS_U2_IN_P[40]}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U2_IN_P[40]}]
set_property DIFF_TERM TRUE [get_ports {CTRLBUS_U2_IN_P[40]}]
# CTRLBUS_P<41>, IO_L9N_T1_DQS_33 , inverted
set_property PACKAGE_PIN AC11 [get_ports {CTRLBUS_U2_IN_N[41]}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U2_IN_N[41]}]
set_property DIFF_TERM TRUE [get_ports {CTRLBUS_U2_IN_N[41]}]
# CTRLBUS_N<41>, IO_L9P_T1_DQS_33 , inverted
set_property PACKAGE_PIN AC12 [get_ports {CTRLBUS_U2_IN_P[41]}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_U2_IN_P[41]}]
set_property DIFF_TERM TRUE [get_ports {CTRLBUS_U2_IN_P[41]}]

# phy_reset_n
set_property PACKAGE_PIN AJ22 [get_ports EXT_K7_0]
set_property IOSTANDARD LVCMOS33 [get_ports EXT_K7_0]
# led0
set_property PACKAGE_PIN Y21 [get_ports EXT_K7_1]
set_property IOSTANDARD LVCMOS33 [get_ports EXT_K7_1]
# led1
set_property PACKAGE_PIN Y24 [get_ports EXT_K7_2]
set_property IOSTANDARD LVCMOS33 [get_ports EXT_K7_2]
# 
set_property PACKAGE_PIN AK20 [get_ports ROD_ROIB_BUSY]
set_property IOSTANDARD LVCMOS33 [get_ports ROD_ROIB_BUSY]
# 
set_property PACKAGE_PIN AA23 [get_ports ROD_DAQ_BUSY]
set_property IOSTANDARD LVCMOS33 [get_ports ROD_DAQ_BUSY]




##########################################################
# async paths 
##########################################################

set_clock_groups -asynchronous -group [list [get_clocks -of_objects [get_pins CTRL/U_ETH/U_SGMII_PHY/U0/core_clocking_i/mmcm_adv_inst/CLKOUT0]] [get_clocks [list  [get_clocks -of_objects [get_pins CTRL/U_ETH/U_SGMII_PHY/U0/core_clocking_i/mmcm_adv_inst/CLKOUT1]]]] ] -group [list [get_clocks -of_objects [get_pins clk/GLOBAL_CLK_MANAGER_INST/inst/plle2_adv_inst/CLKOUT0]] [get_clocks [list  [get_clocks -of_objects [get_pins clk/GLOBAL_CLK_MANAGER_INST/inst/plle2_adv_inst/CLKOUT0]] [get_clocks -of_objects [get_pins clk/GLOBAL_CLK_MANAGER_INST/inst/plle2_adv_inst/CLKOUT1]] [get_clocks -of_objects [get_pins clk/GLOBAL_CLK_MANAGER_INST/inst/plle2_adv_inst/CLKOUT2]] [get_clocks -of_objects [get_pins clk/GLOBAL_CLK_MANAGER_INST/inst/plle2_adv_inst/CLKOUT4]] [get_clocks -of_objects [get_pins clk/GLOBAL_CLK_MANAGER_INST/inst/plle2_adv_inst/CLKOUT3]]]] ] -group [get_clocks  "*cristal*"] -group [get_clocks rod_gtx_rxusrclk_in0] -group [get_clocks rod_gtx_rxusrclk_in1] -group [list [get_clocks rod_with_slink/DISABLE_GTX_WHEN_SIM.new_gtx_slink_i/U0/slink_gtx_vivado_init_i/slink_gtx_vivado_i/gt0_slink_gtx_vivado_i/gtxe2_i/TXOUTCLK] [get_clocks rod_with_slink/DISABLE_GTX_WHEN_SIM.new_gtx_slink_i/U0/slink_gtx_vivado_init_i/slink_gtx_vivado_i/gt1_slink_gtx_vivado_i/gtxe2_i/TXOUTCLK] [get_clocks -of_objects [get_pins {rod_with_slink/GENERATE_OUPUT_SLINKS[0].SLINKPCKBUILDER_INST/DISABLE_HOLA_FOR_SIM.hola_inst/holalsc_core_1/XILINX_PLL.CLK50_PHASE/inst/mmcm_adv_inst/CLKOUT0}]] [get_clocks -of_objects [get_pins {rod_with_slink/GENERATE_OUPUT_SLINKS[1].SLINKPCKBUILDER_INST/DISABLE_HOLA_FOR_SIM.hola_inst/holalsc_core_1/XILINX_PLL.CLK50_PHASE/inst/mmcm_adv_inst/CLKOUT0}]]]














































