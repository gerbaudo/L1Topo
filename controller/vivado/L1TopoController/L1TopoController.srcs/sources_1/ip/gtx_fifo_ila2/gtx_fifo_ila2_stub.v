// Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2016.2 (lin64) Build 1577090 Thu Jun  2 16:32:35 MDT 2016
// Date        : Mon Jan 16 19:37:31 2017
// Host        : atlasuj running 64-bit Ubuntu 14.04.5 LTS
// Command     : write_verilog -force -mode synth_stub
//               /home/marek/ATLAS/L1Topo_ROD/vhdl_svn/trunk/controller/vivado/L1TopoController/L1TopoController.srcs/sources_1/ip/gtx_fifo_ila2/gtx_fifo_ila2_stub.v
// Design      : gtx_fifo_ila2
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7k325tffg900-2
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "ila,Vivado 2016.2" *)
module gtx_fifo_ila2(clk, probe0, probe1)
/* synthesis syn_black_box black_box_pad_pin="clk,probe0[63:0],probe1[0:0]" */;
  input clk;
  input [63:0]probe0;
  input [0:0]probe1;
endmodule
