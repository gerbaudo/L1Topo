// Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2016.4 (lin64) Build 1756540 Mon Jan 23 19:11:19 MST 2017
// Date        : Tue May  2 14:14:16 2017
// Host        : abraham.cern.ch running 64-bit Scientific Linux CERN SLC release 6.7 (Carbon)
// Command     : write_verilog -force -mode funcsim
//               /home/esimioni/new_trunk/trunk/controller/vivado/L1TopoController/L1TopoController.srcs/sources_1/ip/global_clk_manager/global_clk_manager_sim_netlist.v
// Design      : global_clk_manager
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7k325tffg900-2
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* NotValidForBitStream *)
module global_clk_manager
   (clk_40,
    clk_80,
    clk_160,
    clk_400,
    clk_200,
    reset,
    locked,
    gck1_p,
    gck1_n);
  output clk_40;
  output clk_80;
  output clk_160;
  output clk_400;
  output clk_200;
  input reset;
  output locked;
  input gck1_p;
  input gck1_n;

  wire clk_160;
  wire clk_200;
  wire clk_40;
  wire clk_400;
  wire clk_80;
  (* DIFF_TERM = 0 *) (* IBUF_LOW_PWR *) wire gck1_n;
  (* DIFF_TERM = 0 *) (* IBUF_LOW_PWR *) wire gck1_p;
  wire locked;
  wire reset;

  global_clk_manager_global_clk_manager_clk_wiz inst
       (.clk_160(clk_160),
        .clk_200(clk_200),
        .clk_40(clk_40),
        .clk_400(clk_400),
        .clk_80(clk_80),
        .gck1_n(gck1_n),
        .gck1_p(gck1_p),
        .locked(locked),
        .reset(reset));
endmodule

(* ORIG_REF_NAME = "global_clk_manager_clk_wiz" *) 
module global_clk_manager_global_clk_manager_clk_wiz
   (clk_40,
    clk_80,
    clk_160,
    clk_400,
    clk_200,
    reset,
    locked,
    gck1_p,
    gck1_n);
  output clk_40;
  output clk_80;
  output clk_160;
  output clk_400;
  output clk_200;
  input reset;
  output locked;
  input gck1_p;
  input gck1_n;

  wire clk_160;
  wire clk_160_global_clk_manager;
  wire clk_200;
  wire clk_200_global_clk_manager;
  wire clk_40;
  wire clk_400;
  wire clk_400_global_clk_manager;
  wire clk_40_global_clk_manager;
  wire clk_80;
  wire clk_80_global_clk_manager;
  wire clkfbout_buf_global_clk_manager;
  wire clkfbout_global_clk_manager;
  wire gck1_global_clk_manager;
  wire gck1_n;
  wire gck1_p;
  wire locked;
  wire reset;
  wire NLW_plle2_adv_inst_CLKOUT5_UNCONNECTED;
  wire NLW_plle2_adv_inst_DRDY_UNCONNECTED;
  wire [15:0]NLW_plle2_adv_inst_DO_UNCONNECTED;

  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFG clkf_buf
       (.I(clkfbout_global_clk_manager),
        .O(clkfbout_buf_global_clk_manager));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  IBUFDS #(
    .DQS_BIAS("FALSE"),
    .IOSTANDARD("DEFAULT")) 
    clkin1_ibufgds
       (.I(gck1_p),
        .IB(gck1_n),
        .O(gck1_global_clk_manager));
  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFG clkout1_buf
       (.I(clk_40_global_clk_manager),
        .O(clk_40));
  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFG clkout2_buf
       (.I(clk_80_global_clk_manager),
        .O(clk_80));
  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFG clkout3_buf
       (.I(clk_160_global_clk_manager),
        .O(clk_160));
  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFG clkout4_buf
       (.I(clk_400_global_clk_manager),
        .O(clk_400));
  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFG clkout5_buf
       (.I(clk_200_global_clk_manager),
        .O(clk_200));
  (* BOX_TYPE = "PRIMITIVE" *) 
  PLLE2_ADV #(
    .BANDWIDTH("LOW"),
    .CLKFBOUT_MULT(20),
    .CLKFBOUT_PHASE(0.000000),
    .CLKIN1_PERIOD(24.950001),
    .CLKIN2_PERIOD(0.000000),
    .CLKOUT0_DIVIDE(20),
    .CLKOUT0_DUTY_CYCLE(0.500000),
    .CLKOUT0_PHASE(0.000000),
    .CLKOUT1_DIVIDE(10),
    .CLKOUT1_DUTY_CYCLE(0.500000),
    .CLKOUT1_PHASE(0.000000),
    .CLKOUT2_DIVIDE(5),
    .CLKOUT2_DUTY_CYCLE(0.500000),
    .CLKOUT2_PHASE(0.000000),
    .CLKOUT3_DIVIDE(2),
    .CLKOUT3_DUTY_CYCLE(0.500000),
    .CLKOUT3_PHASE(0.000000),
    .CLKOUT4_DIVIDE(4),
    .CLKOUT4_DUTY_CYCLE(0.500000),
    .CLKOUT4_PHASE(0.000000),
    .CLKOUT5_DIVIDE(1),
    .CLKOUT5_DUTY_CYCLE(0.500000),
    .CLKOUT5_PHASE(0.000000),
    .COMPENSATION("ZHOLD"),
    .DIVCLK_DIVIDE(1),
    .IS_CLKINSEL_INVERTED(1'b0),
    .IS_PWRDWN_INVERTED(1'b0),
    .IS_RST_INVERTED(1'b0),
    .REF_JITTER1(0.010000),
    .REF_JITTER2(0.010000),
    .STARTUP_WAIT("FALSE")) 
    plle2_adv_inst
       (.CLKFBIN(clkfbout_buf_global_clk_manager),
        .CLKFBOUT(clkfbout_global_clk_manager),
        .CLKIN1(gck1_global_clk_manager),
        .CLKIN2(1'b0),
        .CLKINSEL(1'b1),
        .CLKOUT0(clk_40_global_clk_manager),
        .CLKOUT1(clk_80_global_clk_manager),
        .CLKOUT2(clk_160_global_clk_manager),
        .CLKOUT3(clk_400_global_clk_manager),
        .CLKOUT4(clk_200_global_clk_manager),
        .CLKOUT5(NLW_plle2_adv_inst_CLKOUT5_UNCONNECTED),
        .DADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DCLK(1'b0),
        .DEN(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DO(NLW_plle2_adv_inst_DO_UNCONNECTED[15:0]),
        .DRDY(NLW_plle2_adv_inst_DRDY_UNCONNECTED),
        .DWE(1'b0),
        .LOCKED(locked),
        .PWRDWN(1'b0),
        .RST(reset));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (weak1, weak0) GSR = GSR_int;
    assign (weak1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
