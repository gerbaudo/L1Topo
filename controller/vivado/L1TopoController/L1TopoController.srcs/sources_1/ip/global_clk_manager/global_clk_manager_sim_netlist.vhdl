-- Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2016.4 (lin64) Build 1756540 Mon Jan 23 19:11:19 MST 2017
-- Date        : Tue May  2 14:14:17 2017
-- Host        : abraham.cern.ch running 64-bit Scientific Linux CERN SLC release 6.7 (Carbon)
-- Command     : write_vhdl -force -mode funcsim
--               /home/esimioni/new_trunk/trunk/controller/vivado/L1TopoController/L1TopoController.srcs/sources_1/ip/global_clk_manager/global_clk_manager_sim_netlist.vhdl
-- Design      : global_clk_manager
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7k325tffg900-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity global_clk_manager_global_clk_manager_clk_wiz is
  port (
    clk_40 : out STD_LOGIC;
    clk_80 : out STD_LOGIC;
    clk_160 : out STD_LOGIC;
    clk_400 : out STD_LOGIC;
    clk_200 : out STD_LOGIC;
    reset : in STD_LOGIC;
    locked : out STD_LOGIC;
    gck1_p : in STD_LOGIC;
    gck1_n : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of global_clk_manager_global_clk_manager_clk_wiz : entity is "global_clk_manager_clk_wiz";
end global_clk_manager_global_clk_manager_clk_wiz;

architecture STRUCTURE of global_clk_manager_global_clk_manager_clk_wiz is
  signal clk_160_global_clk_manager : STD_LOGIC;
  signal clk_200_global_clk_manager : STD_LOGIC;
  signal clk_400_global_clk_manager : STD_LOGIC;
  signal clk_40_global_clk_manager : STD_LOGIC;
  signal clk_80_global_clk_manager : STD_LOGIC;
  signal clkfbout_buf_global_clk_manager : STD_LOGIC;
  signal clkfbout_global_clk_manager : STD_LOGIC;
  signal gck1_global_clk_manager : STD_LOGIC;
  signal NLW_plle2_adv_inst_CLKOUT5_UNCONNECTED : STD_LOGIC;
  signal NLW_plle2_adv_inst_DRDY_UNCONNECTED : STD_LOGIC;
  signal NLW_plle2_adv_inst_DO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of clkf_buf : label is "PRIMITIVE";
  attribute BOX_TYPE of clkin1_ibufgds : label is "PRIMITIVE";
  attribute CAPACITANCE : string;
  attribute CAPACITANCE of clkin1_ibufgds : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE : string;
  attribute IBUF_DELAY_VALUE of clkin1_ibufgds : label is "0";
  attribute IFD_DELAY_VALUE : string;
  attribute IFD_DELAY_VALUE of clkin1_ibufgds : label is "AUTO";
  attribute BOX_TYPE of clkout1_buf : label is "PRIMITIVE";
  attribute BOX_TYPE of clkout2_buf : label is "PRIMITIVE";
  attribute BOX_TYPE of clkout3_buf : label is "PRIMITIVE";
  attribute BOX_TYPE of clkout4_buf : label is "PRIMITIVE";
  attribute BOX_TYPE of clkout5_buf : label is "PRIMITIVE";
  attribute BOX_TYPE of plle2_adv_inst : label is "PRIMITIVE";
begin
clkf_buf: unisim.vcomponents.BUFG
     port map (
      I => clkfbout_global_clk_manager,
      O => clkfbout_buf_global_clk_manager
    );
clkin1_ibufgds: unisim.vcomponents.IBUFDS
    generic map(
      DQS_BIAS => "FALSE",
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => gck1_p,
      IB => gck1_n,
      O => gck1_global_clk_manager
    );
clkout1_buf: unisim.vcomponents.BUFG
     port map (
      I => clk_40_global_clk_manager,
      O => clk_40
    );
clkout2_buf: unisim.vcomponents.BUFG
     port map (
      I => clk_80_global_clk_manager,
      O => clk_80
    );
clkout3_buf: unisim.vcomponents.BUFG
     port map (
      I => clk_160_global_clk_manager,
      O => clk_160
    );
clkout4_buf: unisim.vcomponents.BUFG
     port map (
      I => clk_400_global_clk_manager,
      O => clk_400
    );
clkout5_buf: unisim.vcomponents.BUFG
     port map (
      I => clk_200_global_clk_manager,
      O => clk_200
    );
plle2_adv_inst: unisim.vcomponents.PLLE2_ADV
    generic map(
      BANDWIDTH => "LOW",
      CLKFBOUT_MULT => 20,
      CLKFBOUT_PHASE => 0.000000,
      CLKIN1_PERIOD => 24.950001,
      CLKIN2_PERIOD => 0.000000,
      CLKOUT0_DIVIDE => 20,
      CLKOUT0_DUTY_CYCLE => 0.500000,
      CLKOUT0_PHASE => 0.000000,
      CLKOUT1_DIVIDE => 10,
      CLKOUT1_DUTY_CYCLE => 0.500000,
      CLKOUT1_PHASE => 0.000000,
      CLKOUT2_DIVIDE => 5,
      CLKOUT2_DUTY_CYCLE => 0.500000,
      CLKOUT2_PHASE => 0.000000,
      CLKOUT3_DIVIDE => 2,
      CLKOUT3_DUTY_CYCLE => 0.500000,
      CLKOUT3_PHASE => 0.000000,
      CLKOUT4_DIVIDE => 4,
      CLKOUT4_DUTY_CYCLE => 0.500000,
      CLKOUT4_PHASE => 0.000000,
      CLKOUT5_DIVIDE => 1,
      CLKOUT5_DUTY_CYCLE => 0.500000,
      CLKOUT5_PHASE => 0.000000,
      COMPENSATION => "ZHOLD",
      DIVCLK_DIVIDE => 1,
      IS_CLKINSEL_INVERTED => '0',
      IS_PWRDWN_INVERTED => '0',
      IS_RST_INVERTED => '0',
      REF_JITTER1 => 0.010000,
      REF_JITTER2 => 0.010000,
      STARTUP_WAIT => "FALSE"
    )
        port map (
      CLKFBIN => clkfbout_buf_global_clk_manager,
      CLKFBOUT => clkfbout_global_clk_manager,
      CLKIN1 => gck1_global_clk_manager,
      CLKIN2 => '0',
      CLKINSEL => '1',
      CLKOUT0 => clk_40_global_clk_manager,
      CLKOUT1 => clk_80_global_clk_manager,
      CLKOUT2 => clk_160_global_clk_manager,
      CLKOUT3 => clk_400_global_clk_manager,
      CLKOUT4 => clk_200_global_clk_manager,
      CLKOUT5 => NLW_plle2_adv_inst_CLKOUT5_UNCONNECTED,
      DADDR(6 downto 0) => B"0000000",
      DCLK => '0',
      DEN => '0',
      DI(15 downto 0) => B"0000000000000000",
      DO(15 downto 0) => NLW_plle2_adv_inst_DO_UNCONNECTED(15 downto 0),
      DRDY => NLW_plle2_adv_inst_DRDY_UNCONNECTED,
      DWE => '0',
      LOCKED => locked,
      PWRDWN => '0',
      RST => reset
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity global_clk_manager is
  port (
    clk_40 : out STD_LOGIC;
    clk_80 : out STD_LOGIC;
    clk_160 : out STD_LOGIC;
    clk_400 : out STD_LOGIC;
    clk_200 : out STD_LOGIC;
    reset : in STD_LOGIC;
    locked : out STD_LOGIC;
    gck1_p : in STD_LOGIC;
    gck1_n : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of global_clk_manager : entity is true;
end global_clk_manager;

architecture STRUCTURE of global_clk_manager is
begin
inst: entity work.global_clk_manager_global_clk_manager_clk_wiz
     port map (
      clk_160 => clk_160,
      clk_200 => clk_200,
      clk_40 => clk_40,
      clk_400 => clk_400,
      clk_80 => clk_80,
      gck1_n => gck1_n,
      gck1_p => gck1_p,
      locked => locked,
      reset => reset
    );
end STRUCTURE;
