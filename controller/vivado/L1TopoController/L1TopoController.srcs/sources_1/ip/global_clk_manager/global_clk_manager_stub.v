// Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2016.4 (lin64) Build 1756540 Mon Jan 23 19:11:19 MST 2017
// Date        : Tue May  2 14:14:16 2017
// Host        : abraham.cern.ch running 64-bit Scientific Linux CERN SLC release 6.7 (Carbon)
// Command     : write_verilog -force -mode synth_stub
//               /home/esimioni/new_trunk/trunk/controller/vivado/L1TopoController/L1TopoController.srcs/sources_1/ip/global_clk_manager/global_clk_manager_stub.v
// Design      : global_clk_manager
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7k325tffg900-2
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
module global_clk_manager(clk_40, clk_80, clk_160, clk_400, clk_200, reset, 
  locked, gck1_p, gck1_n)
/* synthesis syn_black_box black_box_pad_pin="clk_40,clk_80,clk_160,clk_400,clk_200,reset,locked,gck1_p,gck1_n" */;
  output clk_40;
  output clk_80;
  output clk_160;
  output clk_400;
  output clk_200;
  input reset;
  output locked;
  input gck1_p;
  input gck1_n;
endmodule
