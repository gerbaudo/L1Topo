-- Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2016.4 (lin64) Build 1756540 Mon Jan 23 19:11:19 MST 2017
-- Date        : Tue May  2 14:14:16 2017
-- Host        : abraham.cern.ch running 64-bit Scientific Linux CERN SLC release 6.7 (Carbon)
-- Command     : write_vhdl -force -mode synth_stub
--               /home/esimioni/new_trunk/trunk/controller/vivado/L1TopoController/L1TopoController.srcs/sources_1/ip/global_clk_manager/global_clk_manager_stub.vhdl
-- Design      : global_clk_manager
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7k325tffg900-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity global_clk_manager is
  Port ( 
    clk_40 : out STD_LOGIC;
    clk_80 : out STD_LOGIC;
    clk_160 : out STD_LOGIC;
    clk_400 : out STD_LOGIC;
    clk_200 : out STD_LOGIC;
    reset : in STD_LOGIC;
    locked : out STD_LOGIC;
    gck1_p : in STD_LOGIC;
    gck1_n : in STD_LOGIC
  );

end global_clk_manager;

architecture stub of global_clk_manager is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "clk_40,clk_80,clk_160,clk_400,clk_200,reset,locked,gck1_p,gck1_n";
begin
end;
