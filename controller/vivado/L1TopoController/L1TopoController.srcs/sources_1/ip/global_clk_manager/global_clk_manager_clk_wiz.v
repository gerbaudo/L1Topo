
// file: global_clk_manager.v
// 
// (c) Copyright 2008 - 2013 Xilinx, Inc. All rights reserved.
// 
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
// 
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
// 
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
// 
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
// 
//----------------------------------------------------------------------------
// User entered comments
//----------------------------------------------------------------------------
// None
//
//----------------------------------------------------------------------------
//  Output     Output      Phase    Duty Cycle   Pk-to-Pk     Phase
//   Clock     Freq (MHz)  (degrees)    (%)     Jitter (ps)  Error (ps)
//----------------------------------------------------------------------------
// __clk_40____40.080______0.000______50.0______246.458____585.377
// __clk_80____80.160______0.000______50.0______214.082____585.377
// _clk_160___160.321______0.000______50.0______191.218____585.377
// _clk_400___400.802______0.000______50.0______165.417____585.377
// _clk_200___200.401______0.000______50.0______184.494____585.377
//
//----------------------------------------------------------------------------
// Input Clock   Freq (MHz)    Input Jitter (UI)
//----------------------------------------------------------------------------
// __primary________40.08016___________900.000

`timescale 1ps/1ps

module global_clk_manager_clk_wiz 

 (// Clock in ports
  // Clock out ports
  output        clk_40,
  output        clk_80,
  output        clk_160,
  output        clk_400,
  output        clk_200,
  // Status and control signals
  input         reset,
  output        locked,
  input         gck1_p,
  input         gck1_n
 );
  // Input buffering
  //------------------------------------
wire gck1_global_clk_manager;
wire clk_in2_global_clk_manager;
  IBUFDS clkin1_ibufgds
   (.O  (gck1_global_clk_manager),
    .I  (gck1_p),
    .IB (gck1_n));


  // Clocking PRIMITIVE
  //------------------------------------

  // Instantiation of the MMCM PRIMITIVE
  //    * Unused inputs are tied off
  //    * Unused outputs are labeled unused

  wire        clk_40_global_clk_manager;
  wire        clk_80_global_clk_manager;
  wire        clk_160_global_clk_manager;
  wire        clk_400_global_clk_manager;
  wire        clk_200_global_clk_manager;
  wire        clk_out6_global_clk_manager;
  wire        clk_out7_global_clk_manager;

  wire [15:0] do_unused;
  wire        drdy_unused;
  wire        psdone_unused;
  wire        locked_int;
  wire        clkfbout_global_clk_manager;
  wire        clkfbout_buf_global_clk_manager;
  wire        clkfboutb_unused;
  wire        clkout5_unused;
  wire        clkout6_unused;
  wire        clkfbstopped_unused;
  wire        clkinstopped_unused;
  wire        reset_high;

  PLLE2_ADV
  #(.BANDWIDTH            ("LOW"),
    .COMPENSATION         ("ZHOLD"),
    .DIVCLK_DIVIDE        (1),
    .CLKFBOUT_MULT        (20),
    .CLKFBOUT_PHASE       (0.000),
    .CLKOUT0_DIVIDE       (20),
    .CLKOUT0_PHASE        (0.000),
    .CLKOUT0_DUTY_CYCLE   (0.500),
    .CLKOUT1_DIVIDE       (10),
    .CLKOUT1_PHASE        (0.000),
    .CLKOUT1_DUTY_CYCLE   (0.500),
    .CLKOUT2_DIVIDE       (5),
    .CLKOUT2_PHASE        (0.000),
    .CLKOUT2_DUTY_CYCLE   (0.500),
    .CLKOUT3_DIVIDE       (2),
    .CLKOUT3_PHASE        (0.000),
    .CLKOUT3_DUTY_CYCLE   (0.500),
    .CLKOUT4_DIVIDE       (4),
    .CLKOUT4_PHASE        (0.000),
    .CLKOUT4_DUTY_CYCLE   (0.500),
    .CLKIN1_PERIOD        (24.95))
  plle2_adv_inst
    // Output clocks
   (
    .CLKFBOUT            (clkfbout_global_clk_manager),
    .CLKOUT0             (clk_40_global_clk_manager),
    .CLKOUT1             (clk_80_global_clk_manager),
    .CLKOUT2             (clk_160_global_clk_manager),
    .CLKOUT3             (clk_400_global_clk_manager),
    .CLKOUT4             (clk_200_global_clk_manager),
    .CLKOUT5             (clkout5_unused),
     // Input clock control
    .CLKFBIN             (clkfbout_buf_global_clk_manager),
    .CLKIN1              (gck1_global_clk_manager),
    .CLKIN2              (1'b0),
     // Tied to always select the primary input clock
    .CLKINSEL            (1'b1),
    // Ports for dynamic reconfiguration
    .DADDR               (7'h0),
    .DCLK                (1'b0),
    .DEN                 (1'b0),
    .DI                  (16'h0),
    .DO                  (do_unused),
    .DRDY                (drdy_unused),
    .DWE                 (1'b0),
    // Other control and status signals
    .LOCKED              (locked_int),
    .PWRDWN              (1'b0),
    .RST                 (reset_high));
  assign reset_high = reset; 

  assign locked = locked_int;
// Clock Monitor clock assigning
//--------------------------------------
 // Output buffering
  //-----------------------------------

  BUFG clkf_buf
   (.O (clkfbout_buf_global_clk_manager),
    .I (clkfbout_global_clk_manager));



  BUFG clkout1_buf
   (.O   (clk_40),
    .I   (clk_40_global_clk_manager));


  BUFG clkout2_buf
   (.O   (clk_80),
    .I   (clk_80_global_clk_manager));

  BUFG clkout3_buf
   (.O   (clk_160),
    .I   (clk_160_global_clk_manager));

  BUFG clkout4_buf
   (.O   (clk_400),
    .I   (clk_400_global_clk_manager));

  BUFG clkout5_buf
   (.O   (clk_200),
    .I   (clk_200_global_clk_manager));



endmodule
