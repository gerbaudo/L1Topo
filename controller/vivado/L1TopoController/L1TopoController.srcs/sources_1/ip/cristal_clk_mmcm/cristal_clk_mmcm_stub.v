// Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2016.4 (lin64) Build 1756540 Mon Jan 23 19:11:19 MST 2017
// Date        : Tue May  2 14:14:16 2017
// Host        : abraham.cern.ch running 64-bit Scientific Linux CERN SLC release 6.7 (Carbon)
// Command     : write_verilog -force -mode synth_stub
//               /home/esimioni/new_trunk/trunk/controller/vivado/L1TopoController/L1TopoController.srcs/sources_1/ip/cristal_clk_mmcm/cristal_clk_mmcm_stub.v
// Design      : cristal_clk_mmcm
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7k325tffg900-2
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
module cristal_clk_mmcm(cr_clk40, cr_clk80, cr_clk160, locked, 
  cristal_clk_in_p, cristal_clk_in_n)
/* synthesis syn_black_box black_box_pad_pin="cr_clk40,cr_clk80,cr_clk160,locked,cristal_clk_in_p,cristal_clk_in_n" */;
  output cr_clk40;
  output cr_clk80;
  output cr_clk160;
  output locked;
  input cristal_clk_in_p;
  input cristal_clk_in_n;
endmodule
