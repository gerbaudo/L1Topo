-- Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2016.4 (lin64) Build 1756540 Mon Jan 23 19:11:19 MST 2017
-- Date        : Tue May  2 14:14:16 2017
-- Host        : abraham.cern.ch running 64-bit Scientific Linux CERN SLC release 6.7 (Carbon)
-- Command     : write_vhdl -force -mode synth_stub
--               /home/esimioni/new_trunk/trunk/controller/vivado/L1TopoController/L1TopoController.srcs/sources_1/ip/cristal_clk_mmcm/cristal_clk_mmcm_stub.vhdl
-- Design      : cristal_clk_mmcm
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7k325tffg900-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity cristal_clk_mmcm is
  Port ( 
    cr_clk40 : out STD_LOGIC;
    cr_clk80 : out STD_LOGIC;
    cr_clk160 : out STD_LOGIC;
    locked : out STD_LOGIC;
    cristal_clk_in_p : in STD_LOGIC;
    cristal_clk_in_n : in STD_LOGIC
  );

end cristal_clk_mmcm;

architecture stub of cristal_clk_mmcm is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "cr_clk40,cr_clk80,cr_clk160,locked,cristal_clk_in_p,cristal_clk_in_n";
begin
end;
