// Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2016.4 (lin64) Build 1756540 Mon Jan 23 19:11:19 MST 2017
// Date        : Tue May  2 14:23:18 2017
// Host        : abraham.cern.ch running 64-bit Scientific Linux CERN SLC release 6.7 (Carbon)
// Command     : write_verilog -force -mode synth_stub
//               /home/esimioni/new_trunk/trunk/controller/vivado/L1TopoController/L1TopoController.srcs/sources_1/ip/fiber_cntr_fifo/fiber_cntr_fifo_stub.v
// Design      : fiber_cntr_fifo
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7k325tffg900-2
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "fifo_generator_v13_1_3,Vivado 2016.4" *)
module fiber_cntr_fifo(clk, srst, din, wr_en, rd_en, prog_full_thresh, dout, 
  full, empty, data_count, prog_full)
/* synthesis syn_black_box black_box_pad_pin="clk,srst,din[431:0],wr_en,rd_en,prog_full_thresh[10:0],dout[431:0],full,empty,data_count[10:0],prog_full" */;
  input clk;
  input srst;
  input [431:0]din;
  input wr_en;
  input rd_en;
  input [10:0]prog_full_thresh;
  output [431:0]dout;
  output full;
  output empty;
  output [10:0]data_count;
  output prog_full;
endmodule
