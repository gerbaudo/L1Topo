FNR==1{
    fileCounter += 1;
}

fileCounter==1 && $1~/^GCK1_CLK_P/ && $2~/^U3./{gck1Pin = substr($2, 4)}
fileCounter==1 && $1~/^GCK2_CLK_P/ && $2~/^U3./{gck2Pin = substr($2, 4)}
fileCounter==1 && $1~/^MGT7_CLK_P/ && $3~/^U3./{mgt7Pin = substr($3, 4)}
fileCounter==1 && $1=="K7_MGTRX_P<3>" && $3~/^U3./{ethMgtRxPin = substr($3, 4)}
fileCounter==1 && $1=="K7_MGTTX_P<3>" && $3~/^U3./{ethMgtTxPin = substr($3, 4)}
fileCounter==1 && $1~/TTC_CTRL/{
	if($3~/^U3./){ttcCtrlPin[substr($1, match($1, "<")+1, match($1, ">")-match($1, "<")-1)] = substr($3, 4);}
	else{
		if($4~/^U3./){ttcCtrlPin[substr($1, match($1, "<")+1, match($1, ">")-match($1, "<")-1)] = substr($4, 4);}
		else{
			if($5~/^U3./){ttcCtrlPin[substr($1, match($1, "<")+1, match($1, ">")-match($1, "<")-1)] = substr($5, 4);}
		}
	}
}



fileCounter==1 && $1~/^CTRLBUS_P/ && $3~/^U3\./{
    ctrlbusPinsP[substr($1, match($1, "<")+1, match($1, ">")-match($1, "<")-1)] = substr($3, 4);
    ctrlbusNameP[substr($1, match($1, "<")+1, match($1, ">")-match($1, "<")-1)] = $1;
}

fileCounter==1 && $1~/^CTRLBUS_N/ && $3~/^U3\./{
    ctrlbusPinsN[substr($1, match($1, "<")+1, match($1, ">")-match($1, "<")-1)] = substr($3, 4);
    ctrlbusNameN[substr($1, match($1, "<")+1, match($1, ">")-match($1, "<")-1)] = $1;
}

fileCounter==2{
	for(i in ctrlbusPinsP){
		if(ctrlbusPinsP[i]==$1){
			tmp = substr($2, match($2, "_")+1);
			ctrlbusPolarity[i]=substr(tmp, match(tmp, "_")-1, 1);
			xilinxPinNameP[i] = $2;
		}
	}
	for(i in ctrlbusPinsN){
        if(ctrlbusPinsN[i]==$1){
            xilinxPinNameN[i] = $2;
        }
	}
}

fileCounter==1 && $1~/^EXT_K7</ && $3~/^U3./{ExtK7Pin[substr($1, match($1, "<")+1, match($1, ">")-match($1, "<")-1)] = substr($3, 4);}

fileCounter==1 && $1~/^AVAGO_SCL</ && $3~/^U3./{AvagoSclPin[substr($1, match($1, "<")+1, match($1, ">")-match($1, "<")-1)] = substr($3, 4);}
fileCounter==1 && $1~/^AVAGO_SDA</ && $3~/^U3./{AvagoSdaPin[substr($1, match($1, "<")+1, match($1, ">")-match($1, "<")-1)] = substr($3, 4);}
fileCounter==1 && $1~/^POWER_SCL/ && $3~/^U3./{PowerSclPin = substr($3, 4);}
fileCounter==1 && $1~/^POWER_SDA/ && $3~/^U3./{PowerSdaPin = substr($3, 4);}


END{
	printf("NET \"GCK1_P\"\tLOC = %s\t| IOSTANDARD = LVDS_25\t| TNM_NET = \"GCK1_TNM\";\n", gck1Pin);
	printf("TIMESPEC TS_GCK1 = PERIOD \"GCK1_TNM\" 24.95 ns;\n");

	print "\n";

	printf("NET \"GCK2_P\"\tLOC = %s\t| IOSTANDARD = LVDS_25\t| TNM_NET = \"GCK2_TNM\";\n", gck2Pin);
	printf("TIMESPEC TS_GCK2 = PERIOD \"GCK2_TNM\" 24.95 ns;\n");

	print "\n";

	printf("NET \"MGT7_CLK_P\"\tLOC = %s\t| TNM_NET = \"MGT7_TNM\";\n", mgt7Pin);
	printf("TIMESPEC TS_MGT7 = PERIOD \"MGT7_TNM\" 8 ns;\n");

	print "\n";

	printf("NET \"clk/eth_gt_txoutclk\" TNM_NET = \"eth_gt_txoutclk_tnm\";\n");
	printf("TIMESPEC TS_txoutclk = PERIOD \"eth_gt_txoutclk_tnm\" 16 ns;\n");

	print "\n";

	printf("NET \"K7_MGTRX_3_P\"\tLOC = %s;\n", ethMgtRxPin);
	printf("NET \"K7_MGTTX_3_P\"\tLOC = %s;\n", ethMgtTxPin);

	print "\n";

	printf("INST \"rod_with_slink/gtx_minipod_12x_k7_i/gt0_gtx_minipod_12x_k7_i/gtxe2_i\" LOC=\"GTXE2_CHANNEL_X0Y11\";\n");
	printf("INST \"rod_with_slink/gtx_minipod_12x_k7_i/gt1_gtx_minipod_12x_k7_i/gtxe2_i\" LOC=\"GTXE2_CHANNEL_X0Y10\";\n");
	printf("INST \"rod_with_slink/gtx_minipod_12x_k7_i/gt2_gtx_minipod_12x_k7_i/gtxe2_i\" LOC=\"GTXE2_CHANNEL_X0Y9\";\n");
	printf("INST \"rod_with_slink/gtx_minipod_12x_k7_i/gt3_gtx_minipod_12x_k7_i/gtxe2_i\" LOC=\"GTXE2_CHANNEL_X0Y8\";\n");
	printf("INST \"rod_with_slink/gtx_minipod_12x_k7_i/gt4_gtx_minipod_12x_k7_i/gtxe2_i\" LOC=\"GTXE2_CHANNEL_X0Y7\";\n");
	printf("INST \"rod_with_slink/gtx_minipod_12x_k7_i/gt5_gtx_minipod_12x_k7_i/gtxe2_i\" LOC=\"GTXE2_CHANNEL_X0Y6\";\n");
	printf("INST \"rod_with_slink/gtx_minipod_12x_k7_i/gt6_gtx_minipod_12x_k7_i/gtxe2_i\" LOC=\"GTXE2_CHANNEL_X0Y5\";\n");
	printf("INST \"rod_with_slink/gtx_minipod_12x_k7_i/gt7_gtx_minipod_12x_k7_i/gtxe2_i\" LOC=\"GTXE2_CHANNEL_X0Y4\";\n");
	printf("INST \"rod_with_slink/gtx_minipod_12x_k7_i/gt8_gtx_minipod_12x_k7_i/gtxe2_i\" LOC=\"GTXE2_CHANNEL_X0Y3\";\n");
	printf("INST \"rod_with_slink/gtx_minipod_12x_k7_i/gt9_gtx_minipod_12x_k7_i/gtxe2_i\" LOC=\"GTXE2_CHANNEL_X0Y2\";\n");
	printf("INST \"rod_with_slink/gtx_minipod_12x_k7_i/gt10_gtx_minipod_12x_k7_i/gtxe2_i\" LOC=\"GTXE2_CHANNEL_X0Y1\";\n");
	printf("INST \"rod_with_slink/gtx_minipod_12x_k7_i/gt11_gtx_minipod_12x_k7_i/gtxe2_i\" LOC=\"GTXE2_CHANNEL_X0Y0\";\n");

	print "\n";


printf("NET \"TTC_BCSTR1_IN\"\tLOC = %s\t|IOSTANDARD = LVCMOS33;\n", ttcCtrlPin[54]);
printf("NET \"TTC_BCSTR2_IN\"\tLOC = %s\t|IOSTANDARD = LVCMOS33;\n", ttcCtrlPin[53]);
printf("NET \"TTC_BRCST_IN[0]\"\tLOC = %s\t|IOSTANDARD = LVCMOS33;\n", ttcCtrlPin[52]);	
printf("NET \"TTC_BRCST_IN[1]\"\tLOC = %s\t|IOSTANDARD = LVCMOS33;\n", ttcCtrlPin[51]);
printf("NET \"TTC_BRCST_IN[2]\"\tLOC = %s\t|IOSTANDARD = LVCMOS33;\n", ttcCtrlPin[50]);
printf("NET \"TTC_BRCST_IN[3]\"\tLOC = %s\t|IOSTANDARD = LVCMOS33;\n", ttcCtrlPin[49]);
printf("NET \"TTC_BRCST_IN[4]\"\tLOC = %s\t|IOSTANDARD = LVCMOS33;\n", ttcCtrlPin[48]);
printf("NET \"TTC_BRCST_IN[5]\"\tLOC = %s\t|IOSTANDARD = LVCMOS33;\n", ttcCtrlPin[47]);
printf("NET \"TTC_BCNRST_IN\"\tLOC = %s\t|IOSTANDARD = LVCMOS33;\n", ttcCtrlPin[46]);
printf("NET \"TTC_EVTCNTRRST_IN\"\tLOC = %s\t|IOSTANDARD = LVCMOS33;\n", ttcCtrlPin[45]);
printf("NET \"TTC_EVT_H_STR_IN\"\tLOC = %s\t|IOSTANDARD = LVCMOS33;\n", ttcCtrlPin[44]);
printf("NET \"TTC_L1A_IN\"\tLOC = %s\t|IOSTANDARD = LVCMOS33;\n", ttcCtrlPin[43]);
printf("NET \"TTC_BCNT_STR_IN\"\tLOC = %s\t|IOSTANDARD = LVCMOS33;\n", ttcCtrlPin[42]);
printf("NET \"TTC_EVT_L_STR_IN\"\tLOC = %s\t|IOSTANDARD = LVCMOS33;\n", ttcCtrlPin[41]);
printf("NET \"TTC_BCNT_IN[0]\"\tLOC = %s\t|IOSTANDARD = LVCMOS33;\n", ttcCtrlPin[40]);
printf("NET \"TTC_BCNT_IN[1]\"\tLOC = %s\t|IOSTANDARD = LVCMOS33;\n", ttcCtrlPin[39]);
printf("NET \"TTC_BCNT_IN[2]\"\tLOC = %s\t|IOSTANDARD = LVCMOS33;\n", ttcCtrlPin[38]);
printf("NET \"TTC_BCNT_IN[3]\"\tLOC = %s\t|IOSTANDARD = LVCMOS33;\n", ttcCtrlPin[37]);
printf("NET \"TTC_BCNT_IN[4]\"\tLOC = %s\t|IOSTANDARD = LVCMOS33;\n", ttcCtrlPin[36]);
printf("NET \"TTC_BCNT_IN[5]\"\tLOC = %s\t|IOSTANDARD = LVCMOS33;\n", ttcCtrlPin[35]);
printf("NET \"TTC_BCNT_IN[6]\"\tLOC = %s\t|IOSTANDARD = LVCMOS33;\n", ttcCtrlPin[34]);
printf("NET \"TTC_BCNT_IN[7]\"\tLOC = %s\t|IOSTANDARD = LVCMOS33;\n", ttcCtrlPin[33]);
printf("NET \"TTC_BCNT_IN[8]\"\tLOC = %s\t|IOSTANDARD = LVCMOS33;\n", ttcCtrlPin[32]);
printf("NET \"TTC_BCNT_IN[9]\"\tLOC = %s\t|IOSTANDARD = LVCMOS33;\n", ttcCtrlPin[31]);
printf("NET \"TTC_BCNT_IN[10]\"\tLOC = %s\t|IOSTANDARD = LVCMOS33;\n", ttcCtrlPin[30]);
printf("NET \"TTC_BCNT_IN[11]\"\tLOC = %s\t|IOSTANDARD = LVCMOS33;\n", ttcCtrlPin[29]);
printf("NET \"TTC_CTRL_23\"\tLOC = %s\t|IOSTANDARD = LVCMOS33;\n", ttcCtrlPin[23]);
printf("NET \"TTC_CTRL_22\"\tLOC = %s\t|IOSTANDARD = LVCMOS33;\n", ttcCtrlPin[22]);
printf("NET \"TTC_RESET_OUT\"\tLOC = %s\t|IOSTANDARD = LVCMOS33;\n", ttcCtrlPin[21]);
printf("NET \"TTC_DOUT_IN[7]\"\tLOC = %s\t|IOSTANDARD = LVCMOS33;\n", ttcCtrlPin[20]);
printf("NET \"TTC_DOUT_IN[6]\"\tLOC = %s\t|IOSTANDARD = LVCMOS33;\n", ttcCtrlPin[19]);
printf("NET \"TTC_DOUT_IN[5]\"\tLOC = %s\t|IOSTANDARD = LVCMOS33;\n", ttcCtrlPin[18]);
printf("NET \"TTC_DOUT_IN[4]\"\tLOC = %s\t|IOSTANDARD = LVCMOS33;\n", ttcCtrlPin[17]);
printf("NET \"TTC_DOUT_IN[3]\"\tLOC = %s\t|IOSTANDARD = LVCMOS33;\n", ttcCtrlPin[16]);
printf("NET \"TTC_DOUT_IN[2]\"\tLOC = %s\t|IOSTANDARD = LVCMOS33;\n", ttcCtrlPin[15]);
printf("NET \"TTC_DOUT_IN[1]\"\tLOC = %s\t|IOSTANDARD = LVCMOS33;\n", ttcCtrlPin[14]);
printf("NET \"TTC_DOUT_IN[0]\"\tLOC = %s\t|IOSTANDARD = LVCMOS33;\n", ttcCtrlPin[13]);
printf("NET \"TTC_SUBADDR_IN[7]\"\tLOC = %s\t|IOSTANDARD = LVCMOS33;\n", ttcCtrlPin[12]);
printf("NET \"TTC_SUBADDR_IN[6]\"\tLOC = %s\t|IOSTANDARD = LVCMOS33;\n", ttcCtrlPin[11]);
printf("NET \"TTC_SUBADDR_IN[5]\"\tLOC = %s\t|IOSTANDARD = LVCMOS33;\n", ttcCtrlPin[10]);
printf("NET \"TTC_SUBADDR_IN[4]\"\tLOC = %s\t|IOSTANDARD = LVCMOS33;\n", ttcCtrlPin[9]);
printf("NET \"TTC_SUBADDR_IN[3]\"\tLOC = %s\t|IOSTANDARD = LVCMOS33;\n", ttcCtrlPin[8]);
printf("NET \"TTC_SUBADDR_IN[2]\"\tLOC = %s\t|IOSTANDARD = LVCMOS33;\n", ttcCtrlPin[7]);
printf("NET \"TTC_SUBADDR_IN[1]\"\tLOC = %s\t|IOSTANDARD = LVCMOS33;\n", ttcCtrlPin[6]);
printf("NET \"TTC_SUBADDR_IN[0]\"\tLOC = %s\t|IOSTANDARD = LVCMOS33;\n", ttcCtrlPin[5]);
printf("NET \"TTC_DOUT_STR_IN\"\tLOC = %s\t|IOSTANDARD = LVCMOS33;\n", ttcCtrlPin[0]);
printf("NET \"TTC_CTRL_26\"\tLOC = %s\t|IOSTANDARD = LVCMOS33;	# TTCDEC P/D\n", ttcCtrlPin[26]);
printf("NET \"TTC_CTRL_27\"\tLOC = %s\t|IOSTANDARD = LVCMOS33;	# TTCDEC CLKSEL\n", ttcCtrlPin[27]);

print "\n";


printf("# I2C\n");
printf("NET \"TTC_CTRL_24\"\tLOC = %s\t|IOSTANDARD = LVCMOS33; # TTCDEC SCL\n", ttcCtrlPin[24]);
printf("NET \"TTC_CTRL_25\"\tLOC = %s\t|IOSTANDARD = LVCMOS33; # TTCDEC SDA\n", ttcCtrlPin[25]);
for(av=0; av<3; ++av){
	printf("NET \"AVAGO_SCL[%u]\"\tLOC = %s\t|IOSTANDARD = LVCMOS33;\n", av, AvagoSclPin[av]);
	printf("NET \"AVAGO_SDA[%u]\"\tLOC = %s\t|IOSTANDARD = LVCMOS33;\n", av, AvagoSdaPin[av]);
}
printf("NET \"POWER_SCL\"\tLOC = %s\t|IOSTANDARD = LVCMOS33;\n", PowerSclPin);
printf("NET \"POWER_SDA\"\tLOC = %s\t|IOSTANDARD = LVCMOS33;\n", PowerSdaPin);


for(l=0; l<8; ++l){
	if(l>7){printf("#\t");}
	printf("NET \"CTRLBUS_U1_OUT_%s[%u]\"\tLOC = %s\t| IOSTANDARD = LVDS\t | DIFF_TERM = FALSE;\t\t# %s, %s %s\n",  ctrlbusPolarity[l], l, ctrlbusPinsP[l], ctrlbusNameP[l], xilinxPinNameP[l], (ctrlbusPolarity[l]=="N")?", inverted":"");
	if(l>7){printf("#\t");}
      	printf("NET \"CTRLBUS_U1_OUT_%s[%u]\"\tLOC = %s\t| IOSTANDARD = LVDS\t | DIFF_TERM = FALSE;\t\t# %s, %s %s\n", (ctrlbusPolarity[l]=="P")?"N":"P", l, ctrlbusPinsN[l], ctrlbusNameN[l], xilinxPinNameN[l], (ctrlbusPolarity[l]=="N")?", inverted":"");

}


for(l=8; l<21; ++l){
	if(l<9){printf("#\t");}
	printf("NET \"CTRLBUS_U1_IN_%s[%u]\"\tLOC = %s\t| IOSTANDARD = LVDS\t | DIFF_TERM = TRUE;\t\t# %s, %s %s\n",  ctrlbusPolarity[l], l, ctrlbusPinsP[l], ctrlbusNameP[l], xilinxPinNameP[l], (ctrlbusPolarity[l]=="N")?", inverted":"");
	if(l<9){printf("#\t");}
       	printf("NET \"CTRLBUS_U1_IN_%s[%u]\"\tLOC = %s\t| IOSTANDARD = LVDS\t | DIFF_TERM = TRUE;\t\t# %s, %s %s\n", (ctrlbusPolarity[l]=="P")?"N":"P", l, ctrlbusPinsN[l], ctrlbusNameN[l], xilinxPinNameN[l], (ctrlbusPolarity[l]=="N")?", inverted":"");	
}
	print "\n";
	
for(l=21; l<29; ++l){
	if(l>28){printf("#\t");}
	printf("NET \"CTRLBUS_U2_OUT_%s[%u]\"\tLOC = %s\t| IOSTANDARD = LVDS\t | DIFF_TERM = FALSE;\t\t# %s, %s %s\n",  ctrlbusPolarity[l], l, ctrlbusPinsP[l], ctrlbusNameP[l], xilinxPinNameP[l], (ctrlbusPolarity[l]=="N")?", inverted":"");
	if(l>28){printf("#\t");}
       	printf("NET \"CTRLBUS_U2_OUT_%s[%u]\"\tLOC = %s\t| IOSTANDARD = LVDS\t | DIFF_TERM = FALSE;\t\t# %s, %s %s\n", (ctrlbusPolarity[l]=="P")?"N":"P", l, ctrlbusPinsN[l], ctrlbusNameN[l], xilinxPinNameN[l], (ctrlbusPolarity[l]=="N")?", inverted":"");
}


for(l=29; l<42; ++l){
	if(l<30){printf("#\t");}
	printf("NET \"CTRLBUS_U2_IN_%s[%u]\"\tLOC = %s\t| IOSTANDARD = LVDS\t | DIFF_TERM = TRUE;\t\t# %s, %s %s\n",  ctrlbusPolarity[l], l, ctrlbusPinsP[l], ctrlbusNameP[l], xilinxPinNameP[l], (ctrlbusPolarity[l]=="N")?", inverted":"");
	if(l<30){printf("#\t");}
       	printf("NET \"CTRLBUS_U2_IN_%s[%u]\"\tLOC = %s\t| IOSTANDARD = LVDS\t | DIFF_TERM = TRUE;\t\t# %s, %s %s\n", (ctrlbusPolarity[l]=="P")?"N":"P", l, ctrlbusPinsN[l], ctrlbusNameN[l], xilinxPinNameN[l], (ctrlbusPolarity[l]=="N")?", inverted":"");	
}
	print "\n";


printf("NET \"EXT_K7_0\"\tLOC = %s\t| IOSTANDARD = LVCMOS15; # phy_reset_n\n", ExtK7Pin[0]);
printf("NET \"EXT_K7_1\"\tLOC = %s\t| IOSTANDARD = LVCMOS15; # led0\n", ExtK7Pin[1]);
printf("NET \"EXT_K7_2\"\tLOC = %s\t| IOSTANDARD = LVCMOS15; # led1\n", ExtK7Pin[2]);
printf("NET \"ROD_BUSY\"\tLOC = %s\t| IOSTANDARD = LVCMOS15; # led1\n", ExtK7Pin[49]);


}


