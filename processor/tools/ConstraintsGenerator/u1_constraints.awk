$1~/^MGT2_CLK_P/ && $3~/^U1./{
	MGT2Pin[substr($1, match($1, "<")+1, match($1, ">")-match($1, "<")-1)] = substr($3, 4);
	print $1;
}

$1~/^MGT4_CLK_P/ && $3~/^U1./{
	MGT4Pin[substr($1, match($1, "<")+1, match($1, ">")-match($1, "<")-1)] = substr($3, 4);
	print $1;
}


END{
	for(i=1; i<12; ++i){if(i in MGT2Pin){printf("set_property PACKAGE_PIN\t%s\t[get_ports {MGT2_CLK_P[%u]}]\n", MGT2Pin[i], i);}}
	print "\n";
	for(i=1; i<12; ++i){if(i in MGT4Pin){printf("set_property PACKAGE_PIN\t%s\t[get_ports {MGT4_CLK_P[%u]}]\n", MGT4Pin[i], i);}}
	print "\n";
	printf("create_clock -period 6.237 -name MGT2_CLK -waveform {0.000 3.118} [get_ports {");
	for(i=1; i<11; ++i){if(i in MGT2Pin){printf("MGT2_CLK_P[%u] ", i);}}
	printf("MGT2_CLK_P[11]}]\n");
	printf("create_clock -period 6.237 -name MGT4_CLK -waveform {0.000 3.118} [get_ports {");
	for(i=1; i<11; ++i){if(i in MGT4Pin){printf("MGT4_CLK_P[%u] ", i);}}
	printf("MGT4_CLK_P[11]}]\n");
}
