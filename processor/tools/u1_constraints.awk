BEGIN{
	print "--------------";
}

$1~/^MGT2_CLK_P/ && $3~/^U3./{
	MGT2Pin[substr($1, match($1, "<")+1, match($1, ">")-match($1, "<")-1)] = substr($3, 4);
	print $1;
}


END{
	print "------";
	for(i in MGT2Pin){
		printf("%u/t%s\n", i, MGT2Pin[i]);
	}
}
