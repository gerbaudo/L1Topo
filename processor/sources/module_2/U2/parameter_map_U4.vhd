library IEEE;
use IEEE.STD_LOGIC_1164.all;
use work.L1TopoDataTypes.all;
package parameter_map is
		constant NumberOfParameters : natural := 119;
		type ParameterRegisters is array (NumberOfParameters - 1 downto 0) of std_logic_vector(31 downto 0);
		function SortParameterMapper(reg : ParameterRegisters) return ParameterSpace;
		function AlgoParameterMapper(reg : ParameterRegisters) return ParameterSpace;
end parameter_map;
package body parameter_map is
		function SortParameterMapper(reg : ParameterRegisters) return ParameterSpace is
				variable result : ParameterSpace(NumberOfSortAlgorithms - 1 downto 0);
		begin
				--TAUabi
				result(1)(0) := reg(0);		--MinET_0
				result(1)(1) := reg(1);		--IsoMask_1
				result(1)(2) := reg(2);		--MinEta_2
				result(1)(3) := reg(3);		--MaxEta_3

				--AJall

				--AJjall

				--AJMatchall
				result(10)(0) := reg(4);		--MinET1_0
				result(10)(1) := reg(5);		--MinET2_1

				--Jab
				result(11)(0) := reg(6);		--MinET_0
				result(11)(1) := reg(7);		--MinEta_1
				result(11)(2) := reg(8);		--MaxEta_2

				--XENoSort

				--XE

				--MUab
				result(18)(0) := reg(9);		--MinET_0
				result(18)(1) := reg(10);		--MinEta_1
				result(18)(2) := reg(11);		--MaxEta_2

				return result;
		end function;
		function AlgoParameterMapper(reg : ParameterRegisters) return ParameterSpace is
				variable result : ParameterSpace(NumberOfAlgorithms - 1 downto 0);
		begin
				--0DR25-TAU20abi-TAU12abi
				result(24)(0) := reg(12);		--MinET1_0
				result(24)(1) := reg(13);		--MinET2_1
				result(24)(2) := reg(14);		--DeltaRMin_2
				result(24)(3) := reg(15);		--DeltaRMax_3

				--1DISAMB-J25ab-0DR25-TAU20abi-TAU12abi
				result(71)(0) := reg(16);		--MinET1_0
				result(71)(1) := reg(17);		--MinET2_1
				result(71)(2) := reg(18);		--MinET3_2
				result(71)(3) := reg(19);		--DisambDRSqrMin_3
				result(71)(4) := reg(20);		--DisambDRSqrMax_4
				result(71)(5) := reg(21);		--DisambDRSqr_5

				--KF-XE-AJall
				result(74)(0) := reg(22);		--MinET_0
				result(74)(1) := reg(23);		--KFXE_1
				result(74)(2) := reg(24);		--KFXE_2
				result(74)(3) := reg(25);		--KFXE_3
				result(74)(4) := reg(26);		--KFXE_4
				result(74)(5) := reg(27);		--KFXE_5
				result(74)(6) := reg(28);		--KFXE_6

				--0MATCH-4AJ20.ETA31-4AJj15.ETA31
				result(57)(0) := reg(29);		--MinET_0
				result(57)(1) := reg(30);		--MinEta_1
				result(57)(2) := reg(31);		--MaxEta_2
				result(57)(3) := reg(32);		--MinMultiplicity_3

				--HT190-AJ15all.ETA21
				result(27)(0) := reg(33);		--MinET_0
				result(27)(1) := reg(34);		--MinEta_1
				result(27)(2) := reg(35);		--MaxEta_2
				result(27)(3) := reg(36);		--MinHt_3

				--HT150-AJ20all.ETA31
				result(28)(0) := reg(37);		--MinET_0
				result(28)(1) := reg(38);		--MinEta_1
				result(28)(2) := reg(39);		--MaxEta_2
				result(28)(3) := reg(40);		--MinHt_3

				--HT150-AJj15all.ETA49
				result(29)(0) := reg(41);		--MinET_0
				result(29)(1) := reg(42);		--MinEta_1
				result(29)(2) := reg(43);		--MaxEta_2
				result(29)(3) := reg(44);		--MinHt_3

				--HT20-AJj15all.ETA49
				result(30)(0) := reg(45);		--MinET_0
				result(30)(1) := reg(46);		--MinEta_1
				result(30)(2) := reg(47);		--MaxEta_2
				result(30)(3) := reg(48);		--MinHt_3

				--05RATIO-XE0-HT0-AJj15all.ETA49
				result(59)(0) := reg(49);		--MinET2_0
				result(59)(1) := reg(50);		--EtaMin_1
				result(59)(2) := reg(51);		--EtaMax_2
				result(59)(3) := reg(52);		--MinET1_3
				result(59)(4) := reg(53);		--HT_4
				result(59)(5) := reg(54);		--Ratio_5

				--90RATIO2-XE0-HT0-AJj15all.ETA49
				result(60)(0) := reg(55);		--MinET2_0
				result(60)(1) := reg(56);		--EtaMin_1
				result(60)(2) := reg(57);		--EtaMax_2
				result(60)(3) := reg(58);		--MinET1_3
				result(60)(4) := reg(59);		--HT_4
				result(60)(5) := reg(60);		--Ratio_5

				--250RATIO2-XE0-HT0-AJj15all.ETA49
				result(61)(0) := reg(61);		--MinET2_0
				result(61)(1) := reg(62);		--EtaMin_1
				result(61)(2) := reg(63);		--EtaMax_2
				result(61)(3) := reg(64);		--MinET1_3
				result(61)(4) := reg(65);		--HT_4
				result(61)(5) := reg(66);		--Ratio_5

				--10MINDPHI-J20ab-XE50
				result(40)(0) := reg(67);		--MinET1_0
				result(40)(1) := reg(68);		--MinET2_1
				result(40)(2) := reg(69);		--DeltaPhiMin_2

				--0DR28-TAU20abi-TAU12abi
				result(23)(0) := reg(70);		--MinET1_0
				result(23)(1) := reg(71);		--MinET2_1
				result(23)(2) := reg(72);		--DeltaRMin_2
				result(23)(3) := reg(73);		--DeltaRMax_3

				--1DISAMB-J25ab-0DR28-TAU20abi-TAU12abi
				result(70)(0) := reg(74);		--MinET1_0
				result(70)(1) := reg(75);		--MinET2_1
				result(70)(2) := reg(76);		--MinET3_2
				result(70)(3) := reg(77);		--DisambDRSqrMin_3
				result(70)(4) := reg(78);		--DisambDRSqrMax_4
				result(70)(5) := reg(79);		--DisambDRSqr_5

				--1DISAMB-TAU12abi-J25ab
				result(66)(0) := reg(80);		--MinET1_0
				result(66)(1) := reg(81);		--MinET2_1
				result(66)(2) := reg(82);		--DisambDRSqrMin_2

				--0DR10-MU10ab-MU6ab
				result(11)(0) := reg(83);		--MinET1_0
				result(11)(1) := reg(84);		--MinET2_1
				result(11)(2) := reg(85);		--DeltaRMin_2
				result(11)(3) := reg(86);		--DeltaRMax_3

				--2DR99-2MU4ab
				result(10)(0) := reg(87);		--MinET1_0
				result(10)(1) := reg(88);		--MinET2_1
				result(10)(2) := reg(89);		--DeltaRMin_2
				result(10)(3) := reg(90);		--DeltaRMax_3

				--0DR34-2MU4ab
				result(15)(0) := reg(91);		--MinET1_0
				result(15)(1) := reg(92);		--MinET2_1
				result(15)(2) := reg(93);		--DeltaRMin_2
				result(15)(3) := reg(94);		--DeltaRMax_3

				--2DR15-2MU6ab
				result(12)(0) := reg(95);		--MinET1_0
				result(12)(1) := reg(96);		--MinET2_1
				result(12)(2) := reg(97);		--DeltaRMin_2
				result(12)(3) := reg(98);		--DeltaRMax_3

				--0DR15-MU6ab-MU4ab
				result(14)(0) := reg(99);		--MinET1_0
				result(14)(1) := reg(100);		--MinET2_1
				result(14)(2) := reg(101);		--DeltaRMin_2
				result(14)(3) := reg(102);		--DeltaRMax_3

				--8INVM15-MU6ab-MU4ab
				result(4)(0) := reg(103);		--MinET1_0
				result(4)(1) := reg(104);		--MinET2_1
				result(4)(2) := reg(105);		--MinMSqr_2
				result(4)(3) := reg(106);		--MaxMSqr_3

				--8INVM15-2MU6ab
				result(7)(0) := reg(107);		--MinET1_0
				result(7)(1) := reg(108);		--MinET2_1
				result(7)(2) := reg(109);		--MinMSqr_2
				result(7)(3) := reg(110);		--MaxMSqr_3

				--2INVM8-2MU4ab
				result(2)(0) := reg(111);		--MinET1_0
				result(2)(1) := reg(112);		--MinET2_1
				result(2)(2) := reg(113);		--MinMSqr_2
				result(2)(3) := reg(114);		--MaxMSqr_3

				--2INVM8-MU6ab-MU4ab
				result(5)(0) := reg(115);		--MinET1_0
				result(5)(1) := reg(116);		--MinET2_1
				result(5)(2) := reg(117);		--MinMSqr_2
				result(5)(3) := reg(118);		--MaxMSqr_3

				return result;
		end function;
end package body;
