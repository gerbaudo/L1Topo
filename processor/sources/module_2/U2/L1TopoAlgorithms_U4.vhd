library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use work.L1TopoDataTypes.all;

entity L1TopoAlgorithms_U4 is
    Port(ClockBus     : in  std_logic_vector(2 downto 0);

         EmTobArray   : in  ClusterArray(InputWidthEM - 1 downto 0);
         TauTobArray  : in  ClusterArray(InputWidthTAU - 1 downto 0);
         JetTobArray  : in  JetArray(InputWidthJET - 1 downto 0);
         MuonTobArray : in  MuonArray(InputWidthMU - 1 downto 0);
         MetTobArray  : in  MetArray(0 downto 0);
         
         Parameters   : in  ParameterSpace(NumberOfAlgorithms - 1 downto 0);
         SortParameters     : in  ParameterSpace(NumberOfSortAlgorithms - 1 downto 0);
         
         Results      : out std_logic_vector(NumberOfResultBits - 1 downto 0);
         Overflow     : out std_logic_vector(NumberOfResultBits - 1 downto 0)
    );
end L1TopoAlgorithms_U4;

architecture Behavioral of L1TopoAlgorithms_U4 is-- Menu Version: 1

-- Module/FPGA : 1/1
-- General configuration (<TopoConfig>):

  constant AlgoOffset24 : integer := 26;   --   0DR25-TAU20abi-TAU12abi Firstbit=10 clock=1
  constant AlgoOffset71 : integer := 27;   --   1DISAMB-J25ab-0DR25-TAU20abi-TAU12abi Firstbit=11 clock=1
  constant AlgoOffset74 : integer := 0;   --   KF-XE-AJall Firstbit=0 clock=0
  constant AlgoOffset57 : integer := 6;   --   0MATCH-4AJ20.ETA31-4AJj15.ETA31 Firstbit=6 clock=0
  constant AlgoOffset27 : integer := 7;   --   HT190-AJ15all.ETA21 Firstbit=7 clock=0
  constant AlgoOffset28 : integer := 8;   --   HT150-AJ20all.ETA31 Firstbit=8 clock=0
  constant AlgoOffset29 : integer := 9;   --   HT150-AJj15all.ETA49 Firstbit=9 clock=0
  constant AlgoOffset30 : integer := 10;   --   HT20-AJj15all.ETA49 Firstbit=10 clock=0
  constant AlgoOffset59 : integer := 13;   --   05RATIO-XE0-HT0-AJj15all.ETA49 Firstbit=13 clock=0
  constant AlgoOffset60 : integer := 15;   --   90RATIO2-XE0-HT0-AJj15all.ETA49 Firstbit=15 clock=0
  constant AlgoOffset61 : integer := 16;   --   250RATIO2-XE0-HT0-AJj15all.ETA49 Firstbit=0 clock=1
  constant AlgoOffset40 : integer := 17;   --   10MINDPHI-J20ab-XE50 Firstbit=1 clock=1
  constant AlgoOffset23 : integer := 18;   --   0DR28-TAU20abi-TAU12abi Firstbit=2 clock=1
  constant AlgoOffset70 : integer := 19;   --   1DISAMB-J25ab-0DR28-TAU20abi-TAU12abi Firstbit=3 clock=1
  constant AlgoOffset66 : integer := 20;   --   1DISAMB-TAU12abi-J25ab Firstbit=4 clock=1
  constant AlgoOffset11 : integer := 21;   --   0DR10-MU10ab-MU6ab Firstbit=5 clock=1
  constant AlgoOffset10 : integer := 22;   --   2DR99-2MU4ab Firstbit=6 clock=1
  constant AlgoOffset15 : integer := 23;   --   0DR34-2MU4ab Firstbit=7 clock=1
  constant AlgoOffset12 : integer := 24;   --   2DR15-2MU6ab Firstbit=8 clock=1
  constant AlgoOffset14 : integer := 25;   --   0DR15-MU6ab-MU4ab Firstbit=9 clock=1
  constant AlgoOffset4 : integer := 28;   --   8INVM15-MU6ab-MU4ab Firstbit=12 clock=1
  constant AlgoOffset7 : integer := 29;   --   8INVM15-2MU6ab Firstbit=13 clock=1
  constant AlgoOffset2 : integer := 30;   --   2INVM8-2MU4ab Firstbit=14 clock=1
  constant AlgoOffset5 : integer := 31;   --   2INVM8-MU6ab-MU4ab Firstbit=15 clock=1
 -- Ordered list of sorted TOBArrays:
  signal EMall  :    TOBArray ((InputWidthEM - 1)  downto 0);
  signal TAUabi  :    TOBArray ((OutputWidthSelectTAU - 1)  downto 0);
  signal EMabi  :    TOBArray ((OutputWidthSelectEM - 1)  downto 0);
  signal TAUab  :    TOBArray ((OutputWidthSelectTAU - 1)  downto 0);
  signal EMs  :    TOBArray ((OutputWidthSortEM - 1)  downto 0);
  signal EMshi  :    TOBArray ((OutputWidthSortEM - 1)  downto 0);
  signal TAUsi  :    TOBArray ((OutputWidthSortTAU - 1)  downto 0);
  signal AJall  :    TOBArray ((InputWidthJET - 1)  downto 0);
  signal AJjall  :    TOBArray ((InputWidthJET - 1)  downto 0);
  signal AJMatchall  :    TOBArray ((InputWidthJET - 1)  downto 0);
  signal Jab  :    TOBArray ((OutputWidthSelectJET - 1)  downto 0);
  signal CJab  :    TOBArray ((OutputWidthSelectJET - 1)  downto 0);
  signal AJjs  :    TOBArray ((OutputWidthSortJET - 1)  downto 0);
  signal AJs  :    TOBArray ((OutputWidthSortJET - 1)  downto 0);
  signal Js  :    TOBArray ((OutputWidthSortJET - 1)  downto 0);
  signal XENoSort  :    MetArray ((OutputWidthMET - 1)  downto 0);
  signal XE  :    TOBArray ((OutputWidthMET - 1)  downto 0);
  signal MUab  :    TOBArray ((OutputWidthSelectMU - 1)  downto 0);
  signal CMUab  :    TOBArray ((OutputWidthSelectMU - 1)  downto 0);
  signal LMUs  :    TOBArray ((OutputWidthSortMU - 1)  downto 0);

begin

sortalgo1 : entity work.ClusterSelect --  Algorithm name: TAUabi
  generic map (
     InputWidth => InputWidthTAU,
     InputWidth1stStage => InputWidth1stStageSelectTAU,
     OutputWidth => OutputWidthSelectTAU,
     DoIsoCut => 1 )
  port map (
     ClusterTobArray => TauTobArray,
     TobArrayOut =>  TAUabi,
     ClockBus => ClockBus,
     Parameters => SortParameters(1)
 );

sortalgo8 : entity work.JetNoSort --  Algorithm name: AJall
  generic map (
     InputWidth => InputWidthJET,
     OutputWidth => InputWidthJET,
     JetSize => DefaultJetSize )
  port map (
     JetTobArray => JetTobArray,
     TobArrayOut =>  AJall,
     ClockBus => ClockBus,
     Parameters => SortParameters(8)
 );

sortalgo9 : entity work.JetNoSort --  Algorithm name: AJjall
  generic map (
     InputWidth => InputWidthJET,
     OutputWidth => InputWidthJET,
     JetSize => 1 )
  port map (
     JetTobArray => JetTobArray,
     TobArrayOut =>  AJjall,
     ClockBus => ClockBus,
     Parameters => SortParameters(9)
 );

sortalgo10 : entity work.JetNoSortMatch --  Algorithm name: AJMatchall
  generic map (
     InputWidth => InputWidthJET,
     OutputWidth => InputWidthJET,
     JetSize => 2 )
  port map (
     JetTobArray => JetTobArray,
     TobArrayOut =>  AJMatchall,
     ClockBus => ClockBus,
     Parameters => SortParameters(10)
 );

sortalgo11 : entity work.JetSelect --  Algorithm name: Jab
  generic map (
     InputWidth => InputWidthJET,
     InputWidth1stStage => InputWidth1stStageSelectJET,
     OutputWidth => OutputWidthSelectJET,
     JetSize => 2,
     DoEtaCut => 1 )
  port map (
     JetTobArray => JetTobArray,
     TobArrayOut =>  Jab,
     ClockBus => ClockBus,
     Parameters => SortParameters(11)
 );

sortalgo16 : entity work.METNoSort --  Algorithm name: XENoSort
  generic map (
     InputWidth => InputWidthMET,
     OutputWidth => OutputWidthMET )
  port map (
     MetTobArray => MetTobArray,
     TobArrayOut =>  XENoSort,
     ClockBus => ClockBus,
     Parameters => SortParameters(16)
 );

sortalgo17 : entity work.MetSort --  Algorithm name: XE
  generic map (
     InputWidth => InputWidthMET,
     OutputWidth => OutputWidthMET )
  port map (
     MetTobArray => MetTobArray,
     TobArrayOut =>  XE,
     ClockBus => ClockBus,
     Parameters => SortParameters(17)
 );

sortalgo18 : entity work.MuonSelect --  Algorithm name: MUab
  generic map (
     InputWidth => InputWidthMU,
     InputWidth1stStage => InputWidth1stStageSelectMU,
     OutputWidth => OutputWidthSelectMU )
  port map (
     MuonTobArray => MuonTobArray,
     TobArrayOut =>  MUab,
     ClockBus => ClockBus,
     Parameters => SortParameters(18)
 );

decisionalgo24 : entity work.DeltaRSqrIncl1 --  Algorithm name: 0DR25-TAU20abi-TAU12abi
  generic map (
     InputWidth => OutputWidthSelectTAU,
     MaxTob => OutputWidthSelectTAU,
     NumResultBits => 1 )
  port map (
     Tob => TAUabi,
     Results => Results(AlgoOffset24+0 downto AlgoOffset24), 
     Overflow => Overflow(AlgoOffset24+0 downto AlgoOffset24), 
     ClockBus => ClockBus,
     Parameters => Parameters(24)
 );

decisionalgo71 : entity work.DisambiguationDRIncl3 --  Algorithm name: 1DISAMB-J25ab-0DR25-TAU20abi-TAU12abi
  generic map (
     InputWidth1 => OutputWidthSelectTAU,
     InputWidth2 => OutputWidthSelectTAU,
     InputWidth3 => OutputWidthSelectJET,
     MaxTob1 => OutputWidthSelectTAU,
     MaxTob2 => OutputWidthSelectTAU,
     MaxTob3 => OutputWidthSelectJET,
     NumResultBits => 1 )
  port map (
     Tob1 => TAUabi,
     Tob2 => TAUabi,
     Tob3 => Jab,
     Results => Results(AlgoOffset71+0 downto AlgoOffset71), 
     Overflow => Overflow(AlgoOffset71+0 downto AlgoOffset71), 
     ClockBus => ClockBus,
     Parameters => Parameters(71)
 );

decisionalgo74 : entity work.KalmanMETCorrection --  Algorithm name: KF-XE-AJall
  generic map (
     InputWidth => InputWidthJET,
     NumResultBits => 6 )
  port map (
     Tob1 => XENoSort,
     Tob2 => AJall,
     Results => Results(AlgoOffset74+5 downto AlgoOffset74), 
     Overflow => Overflow(AlgoOffset74+5 downto AlgoOffset74), 
     ClockBus => ClockBus,
     Parameters => Parameters(74)
 );

decisionalgo57 : entity work.MultiplicityCustom --  Algorithm name: 0MATCH-4AJ20.ETA31-4AJj15.ETA31
  generic map (
     InputWidth => InputWidthJET,
     NumResultBits => 1 )
  port map (
     Tob => AJMatchall,
     Results => Results(AlgoOffset57+0 downto AlgoOffset57), 
     Overflow => Overflow(AlgoOffset57+0 downto AlgoOffset57), 
     ClockBus => ClockBus,
     Parameters => Parameters(57)
 );

decisionalgo27 : entity work.JetHT --  Algorithm name: HT190-AJ15all.ETA21
  generic map (
     InputWidth => InputWidthJET,
     MaxTob => InputWidthJET,
     NumRegisters => 2,
     NumResultBits => 1 )
  port map (
     Tob => AJall,
     Results => Results(AlgoOffset27+0 downto AlgoOffset27), 
     Overflow => Overflow(AlgoOffset27+0 downto AlgoOffset27), 
     ClockBus => ClockBus,
     Parameters => Parameters(27)
 );

decisionalgo28 : entity work.JetHT --  Algorithm name: HT150-AJ20all.ETA31
  generic map (
     InputWidth => InputWidthJET,
     MaxTob => InputWidthJET,
     NumRegisters => 2,
     NumResultBits => 1 )
  port map (
     Tob => AJall,
     Results => Results(AlgoOffset28+0 downto AlgoOffset28), 
     Overflow => Overflow(AlgoOffset28+0 downto AlgoOffset28), 
     ClockBus => ClockBus,
     Parameters => Parameters(28)
 );

decisionalgo29 : entity work.JetHT --  Algorithm name: HT150-AJj15all.ETA49
  generic map (
     InputWidth => InputWidthJET,
     MaxTob => InputWidthJET,
     NumRegisters => 2,
     NumResultBits => 1 )
  port map (
     Tob => AJjall,
     Results => Results(AlgoOffset29+0 downto AlgoOffset29), 
     Overflow => Overflow(AlgoOffset29+0 downto AlgoOffset29), 
     ClockBus => ClockBus,
     Parameters => Parameters(29)
 );

decisionalgo30 : entity work.JetHT --  Algorithm name: HT20-AJj15all.ETA49
  generic map (
     InputWidth => InputWidthJET,
     MaxTob => InputWidthJET,
     NumRegisters => 2,
     NumResultBits => 1 )
  port map (
     Tob => AJjall,
     Results => Results(AlgoOffset30+0 downto AlgoOffset30), 
     Overflow => Overflow(AlgoOffset30+0 downto AlgoOffset30), 
     ClockBus => ClockBus,
     Parameters => Parameters(30)
 );

decisionalgo59 : entity work.Ratio --  Algorithm name: 05RATIO-XE0-HT0-AJj15all.ETA49
  generic map (
     InputWidth1 => 1,
     InputWidth2 => InputWidthJET,
     MaxTob1 => 1,
     MaxTob2 => InputWidthJET,
     NumResultBits => 1,
     isXE2 => 0 )
  port map (
     Tob1 => XE,
     Tob2 => AJjall,
     Results => Results(AlgoOffset59+0 downto AlgoOffset59), 
     Overflow => Overflow(AlgoOffset59+0 downto AlgoOffset59), 
     ClockBus => ClockBus,
     Parameters => Parameters(59)
 );

decisionalgo60 : entity work.Ratio --  Algorithm name: 90RATIO2-XE0-HT0-AJj15all.ETA49
  generic map (
     InputWidth1 => 1,
     InputWidth2 => InputWidthJET,
     MaxTob1 => 1,
     MaxTob2 => InputWidthJET,
     NumResultBits => 1,
     isXE2 => 1 )
  port map (
     Tob1 => XE,
     Tob2 => AJjall,
     Results => Results(AlgoOffset60+0 downto AlgoOffset60), 
     Overflow => Overflow(AlgoOffset60+0 downto AlgoOffset60), 
     ClockBus => ClockBus,
     Parameters => Parameters(60)
 );

decisionalgo61 : entity work.Ratio --  Algorithm name: 250RATIO2-XE0-HT0-AJj15all.ETA49
  generic map (
     InputWidth1 => 1,
     InputWidth2 => InputWidthJET,
     MaxTob1 => 1,
     MaxTob2 => InputWidthJET,
     NumResultBits => 1,
     isXE2 => 1 )
  port map (
     Tob1 => XE,
     Tob2 => AJjall,
     Results => Results(AlgoOffset61+0 downto AlgoOffset61), 
     Overflow => Overflow(AlgoOffset61+0 downto AlgoOffset61), 
     ClockBus => ClockBus,
     Parameters => Parameters(61)
 );

decisionalgo40 : entity work.MinDeltaPhiIncl2 --  Algorithm name: 10MINDPHI-J20ab-XE50
  generic map (
     InputWidth1 => OutputWidthSelectJET,
     InputWidth2 => 1,
     MaxTob1 => OutputWidthSelectJET,
     MaxTob2 => 1,
     NumResultBits => 1 )
  port map (
     Tob1 => Jab,
     Tob2 => XE,
     Results => Results(AlgoOffset40+0 downto AlgoOffset40), 
     Overflow => Overflow(AlgoOffset40+0 downto AlgoOffset40), 
     ClockBus => ClockBus,
     Parameters => Parameters(40)
 );

decisionalgo23 : entity work.DeltaRSqrIncl1 --  Algorithm name: 0DR28-TAU20abi-TAU12abi
  generic map (
     InputWidth => OutputWidthSelectTAU,
     MaxTob => OutputWidthSelectTAU,
     NumResultBits => 1 )
  port map (
     Tob => TAUabi,
     Results => Results(AlgoOffset23+0 downto AlgoOffset23), 
     Overflow => Overflow(AlgoOffset23+0 downto AlgoOffset23), 
     ClockBus => ClockBus,
     Parameters => Parameters(23)
 );

decisionalgo70 : entity work.DisambiguationDRIncl3 --  Algorithm name: 1DISAMB-J25ab-0DR28-TAU20abi-TAU12abi
  generic map (
     InputWidth1 => OutputWidthSelectTAU,
     InputWidth2 => OutputWidthSelectTAU,
     InputWidth3 => OutputWidthSelectJET,
     MaxTob1 => OutputWidthSelectTAU,
     MaxTob2 => OutputWidthSelectTAU,
     MaxTob3 => OutputWidthSelectJET,
     NumResultBits => 1 )
  port map (
     Tob1 => TAUabi,
     Tob2 => TAUabi,
     Tob3 => Jab,
     Results => Results(AlgoOffset70+0 downto AlgoOffset70), 
     Overflow => Overflow(AlgoOffset70+0 downto AlgoOffset70), 
     ClockBus => ClockBus,
     Parameters => Parameters(70)
 );

decisionalgo66 : entity work.DisambiguationIncl2 --  Algorithm name: 1DISAMB-TAU12abi-J25ab
  generic map (
     InputWidth1 => OutputWidthSelectTAU,
     InputWidth2 => OutputWidthSelectJET,
     MaxTob1 => OutputWidthSelectTAU,
     MaxTob2 => OutputWidthSelectJET,
     NumResultBits => 1,
     ClusterOnly => 0,
     ApplyDR => 0 )
  port map (
     Tob1 => TAUabi,
     Tob2 => Jab,
     Results => Results(AlgoOffset66+0 downto AlgoOffset66), 
     Overflow => Overflow(AlgoOffset66+0 downto AlgoOffset66), 
     ClockBus => ClockBus,
     Parameters => Parameters(66)
 );

decisionalgo11 : entity work.DeltaRSqrIncl1 --  Algorithm name: 0DR10-MU10ab-MU6ab
  generic map (
     InputWidth => OutputWidthSelectMU,
     MaxTob => OutputWidthSelectMU,
     RequireOneBarrel => 0,
     NumResultBits => 1 )
  port map (
     Tob => MUab,
     Results => Results(AlgoOffset11+0 downto AlgoOffset11), 
     Overflow => Overflow(AlgoOffset11+0 downto AlgoOffset11), 
     ClockBus => ClockBus,
     Parameters => Parameters(11)
 );

decisionalgo10 : entity work.DeltaRSqrIncl1 --  Algorithm name: 2DR99-2MU4ab
  generic map (
     InputWidth => OutputWidthSelectMU,
     MaxTob => OutputWidthSelectMU,
     RequireOneBarrel => 0,
     NumResultBits => 1 )
  port map (
     Tob => MUab,
     Results => Results(AlgoOffset10+0 downto AlgoOffset10), 
     Overflow => Overflow(AlgoOffset10+0 downto AlgoOffset10), 
     ClockBus => ClockBus,
     Parameters => Parameters(10)
 );

decisionalgo15 : entity work.DeltaRSqrIncl1 --  Algorithm name: 0DR34-2MU4ab
  generic map (
     InputWidth => OutputWidthSelectMU,
     MaxTob => OutputWidthSelectMU,
     RequireOneBarrel => 0,
     NumResultBits => 1 )
  port map (
     Tob => MUab,
     Results => Results(AlgoOffset15+0 downto AlgoOffset15), 
     Overflow => Overflow(AlgoOffset15+0 downto AlgoOffset15), 
     ClockBus => ClockBus,
     Parameters => Parameters(15)
 );

decisionalgo12 : entity work.DeltaRSqrIncl1 --  Algorithm name: 2DR15-2MU6ab
  generic map (
     InputWidth => OutputWidthSelectMU,
     MaxTob => OutputWidthSelectMU,
     RequireOneBarrel => 0,
     NumResultBits => 1 )
  port map (
     Tob => MUab,
     Results => Results(AlgoOffset12+0 downto AlgoOffset12), 
     Overflow => Overflow(AlgoOffset12+0 downto AlgoOffset12), 
     ClockBus => ClockBus,
     Parameters => Parameters(12)
 );

decisionalgo14 : entity work.DeltaRSqrIncl1 --  Algorithm name: 0DR15-MU6ab-MU4ab
  generic map (
     InputWidth => OutputWidthSelectMU,
     MaxTob => OutputWidthSelectMU,
     RequireOneBarrel => 0,
     NumResultBits => 1 )
  port map (
     Tob => MUab,
     Results => Results(AlgoOffset14+0 downto AlgoOffset14), 
     Overflow => Overflow(AlgoOffset14+0 downto AlgoOffset14), 
     ClockBus => ClockBus,
     Parameters => Parameters(14)
 );

decisionalgo4 : entity work.InvariantMassInclusive1 --  Algorithm name: 8INVM15-MU6ab-MU4ab
  generic map (
     InputWidth => OutputWidthSelectMU,
     MaxTob => OutputWidthSelectMU,
     RequireOneBarrel => 0,
     NumResultBits => 1 )
  port map (
     Tob => MUab,
     Results => Results(AlgoOffset4+0 downto AlgoOffset4), 
     Overflow => Overflow(AlgoOffset4+0 downto AlgoOffset4), 
     ClockBus => ClockBus,
     Parameters => Parameters(4)
 );

decisionalgo7 : entity work.InvariantMassInclusive1 --  Algorithm name: 8INVM15-2MU6ab
  generic map (
     InputWidth => OutputWidthSelectMU,
     MaxTob => OutputWidthSelectMU,
     RequireOneBarrel => 0,
     NumResultBits => 1 )
  port map (
     Tob => MUab,
     Results => Results(AlgoOffset7+0 downto AlgoOffset7), 
     Overflow => Overflow(AlgoOffset7+0 downto AlgoOffset7), 
     ClockBus => ClockBus,
     Parameters => Parameters(7)
 );

decisionalgo2 : entity work.InvariantMassInclusive1 --  Algorithm name: 2INVM8-2MU4ab
  generic map (
     InputWidth => OutputWidthSelectMU,
     MaxTob => OutputWidthSelectMU,
     RequireOneBarrel => 0,
     NumResultBits => 1 )
  port map (
     Tob => MUab,
     Results => Results(AlgoOffset2+0 downto AlgoOffset2), 
     Overflow => Overflow(AlgoOffset2+0 downto AlgoOffset2), 
     ClockBus => ClockBus,
     Parameters => Parameters(2)
 );

decisionalgo5 : entity work.InvariantMassInclusive1 --  Algorithm name: 2INVM8-MU6ab-MU4ab
  generic map (
     InputWidth => OutputWidthSelectMU,
     MaxTob => OutputWidthSelectMU,
     RequireOneBarrel => 0,
     NumResultBits => 1 )
  port map (
     Tob => MUab,
     Results => Results(AlgoOffset5+0 downto AlgoOffset5), 
     Overflow => Overflow(AlgoOffset5+0 downto AlgoOffset5), 
     ClockBus => ClockBus,
     Parameters => Parameters(5)
 );

end Behavioral;