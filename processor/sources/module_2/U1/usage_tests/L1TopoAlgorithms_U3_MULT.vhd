library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use work.L1TopoDataTypes.all;

entity L1TopoAlgorithms_U3 is
    Port(ClockBus     : in  std_logic_vector(2 downto 0);

         EmTobArray   : in  ClusterArray(InputWidthEM - 1 downto 0);
         TauTobArray  : in  ClusterArray(InputWidthTAU - 1 downto 0);
         JetTobArray  : in  JetArray(InputWidthJET - 1 downto 0);
         MuonTobArray : in  MuonArray(InputWidthMU - 1 downto 0);
         MetTobArray  : in  MetArray(0 downto 0);
         
         Parameters   : in  ParameterSpace(NumberOfAlgorithms - 1 downto 0);
         SortParameters     : in  ParameterSpace(NumberOfSortAlgorithms - 1 downto 0);
         
         Results      : out std_logic_vector(NumberOfResultBits - 1 downto 0);
         Overflow     : out std_logic_vector(NumberOfResultBits - 1 downto 0)
    );
end L1TopoAlgorithms_U3;

architecture Behavioral of L1TopoAlgorithms_U3 is-- Menu Version: 1

-- Module/FPGA : 1/0
-- General configuration (<TopoConfig>):

--  constant AlgoOffset62 : integer := 0;   --   05MINDPHI-AJj20s6-XE0
--  constant AlgoOffset63 : integer := 1;   --   10MINDPHI-AJj20s6-XE0
--  constant AlgoOffset64 : integer := 2;   --   15MINDPHI-AJj20s6-XE0
--  constant AlgoOffset42 : integer := 3;   --   0DR04-MU4ab-CJ15ab
--  constant AlgoOffset46 : integer := 4;   --   0DR04-MU4ab-CJ20ab
--  constant AlgoOffset43 : integer := 5;   --   0DR04-MU4ab-CJ30ab
--  constant AlgoOffset44 : integer := 6;   --   0DR04-MU6ab-CJ20ab
--  constant AlgoOffset45 : integer := 7;   --   0DR04-MU6ab-CJ25ab
--  constant AlgoOffset36 : integer := 8;   --   10MINDPHI-CJ20ab-XE50
--  constant AlgoOffset56 : integer := 9;   --   2DR15-2CMU4ab
--  constant AlgoOffset58 : integer := 10;   --   2DR15-CMU6ab-CMU4ab
  constant AlgoOffset80 : integer := 11;   --   MULT-CMU4ab
  constant AlgoOffset81 : integer := 13;   --   MULT-CMU6ab
--  constant AlgoOffset47 : integer := 15;   --   2INVM999-2CMU4ab
--  constant AlgoOffset50 : integer := 16;   --   2INVM999-CMU6ab-CMU4ab
--  constant AlgoOffset51 : integer := 17;   --   4INVM8-2CMU4ab
--  constant AlgoOffset54 : integer := 18;   --   4INVM8-CMU6ab-CMU4ab
--  constant AlgoOffset55 : integer := 21;   --   2DR15-CMU4ab-MU4ab
--  constant AlgoOffset48 : integer := 22;   --   2INVM999-CMU4ab-MU4ab
--  constant AlgoOffset52 : integer := 23;   --   4INVM8-CMU4ab-MU4ab
 -- Ordered list of sorted TOBArrays:
--  signal EMall  :    TOBArray ((InputWidthEM - 1)  downto 0);
--  signal EMs  :    TOBArray ((OutputWidthSortEM - 1)  downto 0);
--  signal TAUsi  :    TOBArray ((OutputWidthSortTAU - 1)  downto 0);
--  signal AJall  :    TOBArray ((InputWidthJET - 1)  downto 0);
--  signal AJjall  :    TOBArray ((InputWidthJET - 1)  downto 0);
--  signal AJMatchall  :    TOBArray ((InputWidthJET - 1)  downto 0);
--  signal Jab  :    TOBArray ((OutputWidthSelectJET - 1)  downto 0);
--  signal CJab  :    TOBArray ((OutputWidthSelectJET - 1)  downto 0);
--  signal AJjs  :    TOBArray ((OutputWidthSortJET - 1)  downto 0);
--  signal AJs  :    TOBArray ((OutputWidthSortJET - 1)  downto 0);
--  signal Js  :    TOBArray ((OutputWidthSortJET - 1)  downto 0);
--  signal XE  :    TOBArray ((OutputWidthMET - 1)  downto 0);
--  signal MUab  :    TOBArray ((OutputWidthSelectMU - 1)  downto 0);
  signal CMUab  :    TOBArray ((OutputWidthSelectMU - 1)  downto 0);

begin

--sortalgo9 : entity work.JetSelect --  Algorithm name: CJab
--  generic map (
--     InputWidth => InputWidthJET,
--     InputWidth1stStage => InputWidth1stStageSelectJET,
--     OutputWidth => OutputWidthSelectJET,
--     JetSize => 2,
--     DoEtaCut => 1 )
--  port map (
--     JetTobArray => JetTobArray,
--     TobArrayOut =>  CJab,
--     ClockBus => ClockBus,
--     Parameters => SortParameters(9)
-- );
--
--sortalgo10 : entity work.JetSort --  Algorithm name: AJjs
--  generic map (
--     InputWidth => InputWidthJET,
--     InputWidth1stStage => InputWidth1stStageSortJET,
--     OutputWidth => OutputWidthSortJET,
--     JetSize => 1,
--     DoEtaCut => 0 )
--  port map (
--     JetTobArray => JetTobArray,
--     TobArrayOut =>  AJjs,
--     ClockBus => ClockBus,
--     Parameters => SortParameters(10)
-- );
--
--sortalgo15 : entity work.MetSort --  Algorithm name: XE
--  generic map (
--     InputWidth => InputWidthMET,
--     OutputWidth => OutputWidthMET )
--  port map (
--     MetTobArray => MetTobArray,
--     TobArrayOut =>  XE,
--     ClockBus => ClockBus,
--     Parameters => SortParameters(15)
-- );
--
--sortalgo2 : entity work.MuonSelect --  Algorithm name: MUab
--  generic map (
--     InputWidth => InputWidthMU,
--     InputWidth1stStage => InputWidth1stStageSelectMU,
--     OutputWidth => OutputWidthSelectMU )
--  port map (
--     MuonTobArray => MuonTobArray,
--     TobArrayOut =>  MUab,
--     ClockBus => ClockBus,
--     Parameters => SortParameters(2)
-- );
--
sortalgo21 : entity work.MuonSelect --  Algorithm name: CMUab
  generic map (
     InputWidth => InputWidthMU,
     InputWidth1stStage => InputWidth1stStageSelectMU,
     OutputWidth => OutputWidthSelectMU )
  port map (
     MuonTobArray => MuonTobArray,
     TobArrayOut =>  CMUab,
     ClockBus => ClockBus,
     Parameters => SortParameters(21)
 );
--
--decisionalgo62 : entity work.MinDeltaPhiIncl2 --  Algorithm name: 05MINDPHI-AJj20s6-XE0
--  generic map (
--     InputWidth1 => OutputWidthSortJET,
--     InputWidth2 => 1,
--     MaxTob1 => 6,
--     MaxTob2 => 1,
--     NumResultBits => 1 )
--  port map (
--     Tob1 => AJjs,
--     Tob2 => XE,
--     Results => Results(AlgoOffset62+0 downto AlgoOffset62), 
--     Overflow => Overflow(AlgoOffset62+0 downto AlgoOffset62), 
--     ClockBus => ClockBus,
--     Parameters => Parameters(62)
-- );
--
--decisionalgo63 : entity work.MinDeltaPhiIncl2 --  Algorithm name: 10MINDPHI-AJj20s6-XE0
--  generic map (
--     InputWidth1 => OutputWidthSortJET,
--     InputWidth2 => 1,
--     MaxTob1 => 6,
--     MaxTob2 => 1,
--     NumResultBits => 1 )
--  port map (
--     Tob1 => AJjs,
--     Tob2 => XE,
--     Results => Results(AlgoOffset63+0 downto AlgoOffset63), 
--     Overflow => Overflow(AlgoOffset63+0 downto AlgoOffset63), 
--     ClockBus => ClockBus,
--     Parameters => Parameters(63)
-- );
--
--decisionalgo64 : entity work.MinDeltaPhiIncl2 --  Algorithm name: 15MINDPHI-AJj20s6-XE0
--  generic map (
--     InputWidth1 => OutputWidthSortJET,
--     InputWidth2 => 1,
--     MaxTob1 => 6,
--     MaxTob2 => 1,
--     NumResultBits => 1 )
--  port map (
--     Tob1 => AJjs,
--     Tob2 => XE,
--     Results => Results(AlgoOffset64+0 downto AlgoOffset64), 
--     Overflow => Overflow(AlgoOffset64+0 downto AlgoOffset64), 
--     ClockBus => ClockBus,
--     Parameters => Parameters(64)
-- );
--
--decisionalgo42 : entity work.DeltaRSqrIncl2 --  Algorithm name: 0DR04-MU4ab-CJ15ab
--  generic map (
--     InputWidth1 => OutputWidthSelectMU,
--     InputWidth2 => OutputWidthSelectJET,
--     MaxTob1 => OutputWidthSelectMU,
--     MaxTob2 => OutputWidthSelectJET,
--     NumResultBits => 1 )
--  port map (
--     Tob1 => MUab,
--     Tob2 => CJab,
--     Results => Results(AlgoOffset42+0 downto AlgoOffset42), 
--     Overflow => Overflow(AlgoOffset42+0 downto AlgoOffset42), 
--     ClockBus => ClockBus,
--     Parameters => Parameters(42)
-- );
--
--decisionalgo46 : entity work.DeltaRSqrIncl2 --  Algorithm name: 0DR04-MU4ab-CJ20ab
--  generic map (
--     InputWidth1 => OutputWidthSelectMU,
--     InputWidth2 => OutputWidthSelectJET,
--     MaxTob1 => OutputWidthSelectMU,
--     MaxTob2 => OutputWidthSelectJET,
--     NumResultBits => 1 )
--  port map (
--     Tob1 => MUab,
--     Tob2 => CJab,
--     Results => Results(AlgoOffset46+0 downto AlgoOffset46), 
--     Overflow => Overflow(AlgoOffset46+0 downto AlgoOffset46), 
--     ClockBus => ClockBus,
--     Parameters => Parameters(46)
-- );
--
--decisionalgo43 : entity work.DeltaRSqrIncl2 --  Algorithm name: 0DR04-MU4ab-CJ30ab
--  generic map (
--     InputWidth1 => OutputWidthSelectMU,
--     InputWidth2 => OutputWidthSelectJET,
--     MaxTob1 => OutputWidthSelectMU,
--     MaxTob2 => OutputWidthSelectJET,
--     NumResultBits => 1 )
--  port map (
--     Tob1 => MUab,
--     Tob2 => CJab,
--     Results => Results(AlgoOffset43+0 downto AlgoOffset43), 
--     Overflow => Overflow(AlgoOffset43+0 downto AlgoOffset43), 
--     ClockBus => ClockBus,
--     Parameters => Parameters(43)
-- );
--
--decisionalgo44 : entity work.DeltaRSqrIncl2 --  Algorithm name: 0DR04-MU6ab-CJ20ab
--  generic map (
--     InputWidth1 => OutputWidthSelectMU,
--     InputWidth2 => OutputWidthSelectJET,
--     MaxTob1 => OutputWidthSelectMU,
--     MaxTob2 => OutputWidthSelectJET,
--     NumResultBits => 1 )
--  port map (
--     Tob1 => MUab,
--     Tob2 => CJab,
--     Results => Results(AlgoOffset44+0 downto AlgoOffset44), 
--     Overflow => Overflow(AlgoOffset44+0 downto AlgoOffset44), 
--     ClockBus => ClockBus,
--     Parameters => Parameters(44)
-- );
--
--decisionalgo45 : entity work.DeltaRSqrIncl2 --  Algorithm name: 0DR04-MU6ab-CJ25ab
--  generic map (
--     InputWidth1 => OutputWidthSelectMU,
--     InputWidth2 => OutputWidthSelectJET,
--     MaxTob1 => OutputWidthSelectMU,
--     MaxTob2 => OutputWidthSelectJET,
--     NumResultBits => 1 )
--  port map (
--     Tob1 => MUab,
--     Tob2 => CJab,
--     Results => Results(AlgoOffset45+0 downto AlgoOffset45), 
--     Overflow => Overflow(AlgoOffset45+0 downto AlgoOffset45), 
--     ClockBus => ClockBus,
--     Parameters => Parameters(45)
-- );
--
--decisionalgo36 : entity work.MinDeltaPhiIncl2 --  Algorithm name: 10MINDPHI-CJ20ab-XE50
--  generic map (
--     InputWidth1 => OutputWidthSelectJET,
--     InputWidth2 => 1,
--     MaxTob1 => OutputWidthSelectJET,
--     MaxTob2 => 1,
--     NumResultBits => 1 )
--  port map (
--     Tob1 => CJab,
--     Tob2 => XE,
--     Results => Results(AlgoOffset36+0 downto AlgoOffset36), 
--     Overflow => Overflow(AlgoOffset36+0 downto AlgoOffset36), 
--     ClockBus => ClockBus,
--     Parameters => Parameters(36)
-- );
--
--decisionalgo56 : entity work.DeltaRSqrIncl1 --  Algorithm name: 2DR15-2CMU4ab
--  generic map (
--     InputWidth => OutputWidthSelectMU,
--     MaxTob => OutputWidthSelectMU,
--     RequireOneBarrel => 0,
--     NumResultBits => 1 )
--  port map (
--     Tob => CMUab,
--     Results => Results(AlgoOffset56+0 downto AlgoOffset56), 
--     Overflow => Overflow(AlgoOffset56+0 downto AlgoOffset56), 
--     ClockBus => ClockBus,
--     Parameters => Parameters(56)
-- );
--
--decisionalgo58 : entity work.DeltaRSqrIncl1 --  Algorithm name: 2DR15-CMU6ab-CMU4ab
--  generic map (
--     InputWidth => OutputWidthSelectMU,
--     MaxTob => OutputWidthSelectMU,
--     RequireOneBarrel => 0,
--     NumResultBits => 1 )
--  port map (
--     Tob => CMUab,
--     Results => Results(AlgoOffset58+0 downto AlgoOffset58), 
--     Overflow => Overflow(AlgoOffset58+0 downto AlgoOffset58), 
--     ClockBus => ClockBus,
--     Parameters => Parameters(58)
-- );
--
decisionalgo80 : entity work.Multiplicity --  Algorithm name: MULT-CMU4ab
  generic map (
     InputWidth => OutputWidthSelectMU,
     NumResultBits => 2 )
  port map (
     Tob => CMUab,
     Results => Results(AlgoOffset80+1 downto AlgoOffset80), 
     Overflow => Overflow(AlgoOffset80+1 downto AlgoOffset80), 
     ClockBus => ClockBus,
     Parameters => Parameters(80)
 );

decisionalgo81 : entity work.Multiplicity --  Algorithm name: MULT-CMU6ab
  generic map (
     InputWidth => OutputWidthSelectMU,
     NumResultBits => 2 )
  port map (
     Tob => CMUab,
     Results => Results(AlgoOffset81+1 downto AlgoOffset81), 
     Overflow => Overflow(AlgoOffset81+1 downto AlgoOffset81), 
     ClockBus => ClockBus,
     Parameters => Parameters(81)
 );
--
--decisionalgo47 : entity work.InvariantMassInclusive1 --  Algorithm name: 2INVM999-2CMU4ab
--  generic map (
--     InputWidth => OutputWidthSelectMU,
--     MaxTob => OutputWidthSelectMU,
--     RequireOneBarrel => 0,
--     NumResultBits => 1 )
--  port map (
--     Tob => CMUab,
--     Results => Results(AlgoOffset47+0 downto AlgoOffset47), 
--     Overflow => Overflow(AlgoOffset47+0 downto AlgoOffset47), 
--     ClockBus => ClockBus,
--     Parameters => Parameters(47)
-- );
--
--decisionalgo50 : entity work.InvariantMassInclusive1 --  Algorithm name: 2INVM999-CMU6ab-CMU4ab
--  generic map (
--     InputWidth => OutputWidthSelectMU,
--     MaxTob => OutputWidthSelectMU,
--     RequireOneBarrel => 0,
--     NumResultBits => 1 )
--  port map (
--     Tob => CMUab,
--     Results => Results(AlgoOffset50+0 downto AlgoOffset50), 
--     Overflow => Overflow(AlgoOffset50+0 downto AlgoOffset50), 
--     ClockBus => ClockBus,
--     Parameters => Parameters(50)
-- );
--
--decisionalgo51 : entity work.InvariantMassInclusive1 --  Algorithm name: 4INVM8-2CMU4ab
--  generic map (
--     InputWidth => OutputWidthSelectMU,
--     MaxTob => OutputWidthSelectMU,
--     RequireOneBarrel => 0,
--     NumResultBits => 1 )
--  port map (
--     Tob => CMUab,
--     Results => Results(AlgoOffset51+0 downto AlgoOffset51), 
--     Overflow => Overflow(AlgoOffset51+0 downto AlgoOffset51), 
--     ClockBus => ClockBus,
--     Parameters => Parameters(51)
-- );
--
--decisionalgo54 : entity work.InvariantMassInclusive1 --  Algorithm name: 4INVM8-CMU6ab-CMU4ab
--  generic map (
--     InputWidth => OutputWidthSelectMU,
--     MaxTob => OutputWidthSelectMU,
--     RequireOneBarrel => 0,
--     NumResultBits => 1 )
--  port map (
--     Tob => CMUab,
--     Results => Results(AlgoOffset54+0 downto AlgoOffset54), 
--     Overflow => Overflow(AlgoOffset54+0 downto AlgoOffset54), 
--     ClockBus => ClockBus,
--     Parameters => Parameters(54)
-- );
--
--decisionalgo55 : entity work.DeltaRSqrIncl2 --  Algorithm name: 2DR15-CMU4ab-MU4ab
--  generic map (
--     InputWidth1 => OutputWidthSelectMU,
--     InputWidth2 => OutputWidthSelectMU,
--     MaxTob1 => OutputWidthSelectMU,
--     MaxTob2 => OutputWidthSelectMU,
--     NumResultBits => 1 )
--  port map (
--     Tob1 => CMUab,
--     Tob2 => MUab,
--     Results => Results(AlgoOffset55+0 downto AlgoOffset55), 
--     Overflow => Overflow(AlgoOffset55+0 downto AlgoOffset55), 
--     ClockBus => ClockBus,
--     Parameters => Parameters(55)
-- );
--
--decisionalgo48 : entity work.InvariantMassInclusive2 --  Algorithm name: 2INVM999-CMU4ab-MU4ab
--  generic map (
--     InputWidth1 => OutputWidthSelectMU,
--     InputWidth2 => OutputWidthSelectMU,
--     MaxTob1 => OutputWidthSelectMU,
--     MaxTob2 => OutputWidthSelectMU,
--     NumResultBits => 1 )
--  port map (
--     Tob1 => CMUab,
--     Tob2 => MUab,
--     Results => Results(AlgoOffset48+0 downto AlgoOffset48), 
--     Overflow => Overflow(AlgoOffset48+0 downto AlgoOffset48), 
--     ClockBus => ClockBus,
--     Parameters => Parameters(48)
-- );
--
--decisionalgo52 : entity work.InvariantMassInclusive2 --  Algorithm name: 4INVM8-CMU4ab-MU4ab
--  generic map (
--     InputWidth1 => OutputWidthSelectMU,
--     InputWidth2 => OutputWidthSelectMU,
--     MaxTob1 => OutputWidthSelectMU,
--     MaxTob2 => OutputWidthSelectMU,
--     NumResultBits => 1 )
--  port map (
--     Tob1 => CMUab,
--     Tob2 => MUab,
--     Results => Results(AlgoOffset52+0 downto AlgoOffset52), 
--     Overflow => Overflow(AlgoOffset52+0 downto AlgoOffset52), 
--     ClockBus => ClockBus,
--     Parameters => Parameters(52)
-- );

end Behavioral;