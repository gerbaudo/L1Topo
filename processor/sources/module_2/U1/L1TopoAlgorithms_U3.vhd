library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use work.L1TopoDataTypes.all;

entity L1TopoAlgorithms_U3 is
    Port(ClockBus     : in  std_logic_vector(2 downto 0);

         EmTobArray   : in  ClusterArray(InputWidthEM - 1 downto 0);
         TauTobArray  : in  ClusterArray(InputWidthTAU - 1 downto 0);
         JetTobArray  : in  JetArray(InputWidthJET - 1 downto 0);
         MuonTobArray : in  MuonArray(InputWidthMU - 1 downto 0);
         MetTobArray  : in  MetArray(0 downto 0);
         
         Parameters   : in  ParameterSpace(NumberOfAlgorithms - 1 downto 0);
         SortParameters     : in  ParameterSpace(NumberOfSortAlgorithms - 1 downto 0);
         
         Results      : out std_logic_vector(NumberOfResultBits - 1 downto 0);
         Overflow     : out std_logic_vector(NumberOfResultBits - 1 downto 0)
    );
end L1TopoAlgorithms_U3;

architecture Behavioral of L1TopoAlgorithms_U3 is-- Menu Version: 1

-- Module/FPGA : 1/0
-- General configuration (<TopoConfig>):

  constant AlgoOffset32 : integer := 0;   --   05MINDPHI-AJj10s6-XE0 Firstbit=0 clock=0
  constant AlgoOffset33 : integer := 1;   --   10MINDPHI-AJj10s6-XE0 Firstbit=1 clock=0
  constant AlgoOffset34 : integer := 2;   --   15MINDPHI-AJj10s6-XE0 Firstbit=2 clock=0
  constant AlgoOffset43 : integer := 3;   --   0DR04-MU4ab-CJ15ab Firstbit=3 clock=0
  constant AlgoOffset47 : integer := 4;   --   0DR04-MU4ab-CJ20ab Firstbit=4 clock=0
  constant AlgoOffset44 : integer := 5;   --   0DR04-MU4ab-CJ30ab Firstbit=5 clock=0
  constant AlgoOffset45 : integer := 6;   --   0DR04-MU6ab-CJ20ab Firstbit=6 clock=0
  constant AlgoOffset46 : integer := 7;   --   0DR04-MU6ab-CJ25ab Firstbit=7 clock=0
  constant AlgoOffset41 : integer := 8;   --   10MINDPHI-CJ20ab-XE50 Firstbit=8 clock=0
  constant AlgoOffset50 : integer := 9;   --   0DR24-2CMU4ab Firstbit=9 clock=0
  constant AlgoOffset64 : integer := 11;   --   MULT-CMU4ab Firstbit=11 clock=0
  constant AlgoOffset65 : integer := 13;   --   MULT-CMU6ab Firstbit=13 clock=0
  constant AlgoOffset53 : integer := 19;   --   0DETA04-EM8abi-MU10ab Firstbit=3 clock=1
  constant AlgoOffset55 : integer := 22;   --   0DPHI03-EM8abi-MU10ab Firstbit=6 clock=1
  constant AlgoOffset54 : integer := 20;   --   0DETA04-EM15abi-MUab Firstbit=4 clock=1
  constant AlgoOffset56 : integer := 24;   --   0DPHI03-EM15abi-MUab Firstbit=8 clock=1
  constant AlgoOffset51 : integer := 21;   --   0DR24-CMU4ab-MU4ab Firstbit=5 clock=1
  constant AlgoOffset48 : integer := 23;   --   2INVM8-CMU4ab-MU4ab Firstbit=7 clock=1
  constant AlgoOffset79 : integer := 25;   --   10MINDPHI-AJ20s2-XE50 Firstbit=9 clock=1
  constant AlgoOffset80 : integer := 26;   --   LATE-MU10s1 Firstbit=10 clock=1
  constant AlgoOffset81 : integer := 27;   --   SC111-CJ15ab.ETA26 Firstbit=11 clock=1
  constant AlgoOffset82 : integer := 28;   --   SC85-CJ15ab.ETA26 Firstbit=12 clock=1
 -- Ordered list of sorted TOBArrays:
  signal EMall  :    TOBArray ((InputWidthEM - 1)  downto 0);
  signal TAUabi  :    TOBArray ((OutputWidthSelectTAU - 1)  downto 0);
  signal EMabi  :    TOBArray ((OutputWidthSelectEM - 1)  downto 0);
  signal TAUab  :    TOBArray ((OutputWidthSelectTAU - 1)  downto 0);
  signal EMs  :    TOBArray ((OutputWidthSortEM - 1)  downto 0);
  signal EMshi  :    TOBArray ((OutputWidthSortEM - 1)  downto 0);
  signal TAUsi  :    TOBArray ((OutputWidthSortTAU - 1)  downto 0);
  signal AJall  :    TOBArray ((InputWidthJET - 1)  downto 0);
  signal AJjall  :    TOBArray ((InputWidthJET - 1)  downto 0);
  signal AJMatchall  :    TOBArray ((InputWidthJET - 1)  downto 0);
  signal Jab  :    TOBArray ((OutputWidthSelectJET - 1)  downto 0);
  signal CJab  :    TOBArray ((OutputWidthSelectJET - 1)  downto 0);
  signal AJjs  :    TOBArray ((OutputWidthSortJET - 1)  downto 0);
  signal AJs  :    TOBArray ((OutputWidthSortJET - 1)  downto 0);
  signal Js  :    TOBArray ((OutputWidthSortJET - 1)  downto 0);
  signal XENoSort  :    TOBArray ((OutputWidthMET - 1)  downto 0);
  signal XE  :    TOBArray ((OutputWidthMET - 1)  downto 0);
  signal MUab  :    TOBArray ((OutputWidthSelectMU - 1)  downto 0);
  signal CMUab  :    TOBArray ((OutputWidthSelectMU - 1)  downto 0);
  signal LMUs  :    TOBArray ((NumberOfDelayedMuons - 1)  downto 0);

begin

sortalgo2 : entity work.ClusterSelect --  Algorithm name: EMabi
  generic map (
     InputWidth => InputWidthEM,
     InputWidth1stStage => InputWidth1stStageSelectEM,
     OutputWidth => OutputWidthSelectEM,
     DoIsoCut => 1 )
  port map (
     ClusterTobArray => EmTobArray,
     TobArrayOut =>  EMabi,
     ClockBus => ClockBus,
     Parameters => SortParameters(2)
 );

sortalgo12 : entity work.JetSelect --  Algorithm name: CJab
  generic map (
     InputWidth => InputWidthJET,
     InputWidth1stStage => InputWidth1stStageSelectJET,
     OutputWidth => OutputWidthSelectJET,
     JetSize => 2,
     DoEtaCut => 1 )
  port map (
     JetTobArray => JetTobArray,
     TobArrayOut =>  CJab,
     ClockBus => ClockBus,
     Parameters => SortParameters(12)
 );

sortalgo13 : entity work.JetSort --  Algorithm name: AJjs
  generic map (
     InputWidth => InputWidthJET,
     InputWidth1stStage => InputWidth1stStageSortJET,
     OutputWidth => OutputWidthSortJET,
     JetSize => 1,
     DoEtaCut => 0 )
  port map (
     JetTobArray => JetTobArray,
     TobArrayOut =>  AJjs,
     ClockBus => ClockBus,
     Parameters => SortParameters(13)
 );

sortalgo14 : entity work.JetSort --  Algorithm name: AJs
  generic map (
     InputWidth => InputWidthJET,
     InputWidth1stStage => InputWidth1stStageSortJET,
     OutputWidth => OutputWidthSortJET,
     JetSize => 2,
     DoEtaCut => 0 )
  port map (
     JetTobArray => JetTobArray,
     TobArrayOut =>  AJs,
     ClockBus => ClockBus,
     Parameters => SortParameters(14)
 );

sortalgo17 : entity work.MetSort --  Algorithm name: XE
  generic map (
     InputWidth => InputWidthMET,
     OutputWidth => OutputWidthMET )
  port map (
     MetTobArray => MetTobArray,
     TobArrayOut =>  XE,
     ClockBus => ClockBus,
     Parameters => SortParameters(17)
 );

sortalgo18 : entity work.MuonSelect --  Algorithm name: MUab
  generic map (
     InputWidth => InputWidthMU,
     InputWidth1stStage => InputWidth1stStageSelectMU,
     OutputWidth => OutputWidthSelectMU )
  port map (
     MuonTobArray => MuonTobArray,
     TobArrayOut =>  MUab,
     ClockBus => ClockBus,
     Parameters => SortParameters(18)
 );

sortalgo19 : entity work.MuonSelect --  Algorithm name: CMUab
  generic map (
     InputWidth => InputWidthMU,
     InputWidth1stStage => InputWidth1stStageSelectMU,
     OutputWidth => OutputWidthSelectMU )
  port map (
     MuonTobArray => MuonTobArray,
     TobArrayOut =>  CMUab,
     ClockBus => ClockBus,
     Parameters => SortParameters(19)
 );

sortalgo20 : entity work.MuonSort_1BC --  Algorithm name: LMUs
  generic map (
     InputWidth => InputWidthMU,
     OutputWidth => OutputWidthSortMU,
     nDelayedMuons => 1 )
  port map (
     MuonTobArray => MuonTobArray,
     TobArrayOut =>  LMUs,
     ClockBus => ClockBus,
     Parameters => SortParameters(20)
 );

decisionalgo32 : entity work.MinDeltaPhiIncl2 --  Algorithm name: 05MINDPHI-AJj10s6-XE0
  generic map (
     InputWidth1 => OutputWidthSortJET,
     InputWidth2 => 1,
     MaxTob1 => 6,
     MaxTob2 => 1,
     NumResultBits => 1 )
  port map (
     Tob1 => AJjs,
     Tob2 => XE,
     Results => Results(AlgoOffset32+0 downto AlgoOffset32), 
     Overflow => Overflow(AlgoOffset32+0 downto AlgoOffset32), 
     ClockBus => ClockBus,
     Parameters => Parameters(32)
 );

decisionalgo33 : entity work.MinDeltaPhiIncl2 --  Algorithm name: 10MINDPHI-AJj10s6-XE0
  generic map (
     InputWidth1 => OutputWidthSortJET,
     InputWidth2 => 1,
     MaxTob1 => 6,
     MaxTob2 => 1,
     NumResultBits => 1 )
  port map (
     Tob1 => AJjs,
     Tob2 => XE,
     Results => Results(AlgoOffset33+0 downto AlgoOffset33), 
     Overflow => Overflow(AlgoOffset33+0 downto AlgoOffset33), 
     ClockBus => ClockBus,
     Parameters => Parameters(33)
 );

decisionalgo34 : entity work.MinDeltaPhiIncl2 --  Algorithm name: 15MINDPHI-AJj10s6-XE0
  generic map (
     InputWidth1 => OutputWidthSortJET,
     InputWidth2 => 1,
     MaxTob1 => 6,
     MaxTob2 => 1,
     NumResultBits => 1 )
  port map (
     Tob1 => AJjs,
     Tob2 => XE,
     Results => Results(AlgoOffset34+0 downto AlgoOffset34), 
     Overflow => Overflow(AlgoOffset34+0 downto AlgoOffset34), 
     ClockBus => ClockBus,
     Parameters => Parameters(34)
 );

decisionalgo43 : entity work.DeltaRSqrIncl2 --  Algorithm name: 0DR04-MU4ab-CJ15ab
  generic map (
     InputWidth1 => OutputWidthSelectMU,
     InputWidth2 => OutputWidthSelectJET,
     MaxTob1 => OutputWidthSelectMU,
     MaxTob2 => OutputWidthSelectJET,
     NumResultBits => 1 )
  port map (
     Tob1 => MUab,
     Tob2 => CJab,
     Results => Results(AlgoOffset43+0 downto AlgoOffset43), 
     Overflow => Overflow(AlgoOffset43+0 downto AlgoOffset43), 
     ClockBus => ClockBus,
     Parameters => Parameters(43)
 );

decisionalgo47 : entity work.DeltaRSqrIncl2 --  Algorithm name: 0DR04-MU4ab-CJ20ab
  generic map (
     InputWidth1 => OutputWidthSelectMU,
     InputWidth2 => OutputWidthSelectJET,
     MaxTob1 => OutputWidthSelectMU,
     MaxTob2 => OutputWidthSelectJET,
     NumResultBits => 1 )
  port map (
     Tob1 => MUab,
     Tob2 => CJab,
     Results => Results(AlgoOffset47+0 downto AlgoOffset47), 
     Overflow => Overflow(AlgoOffset47+0 downto AlgoOffset47), 
     ClockBus => ClockBus,
     Parameters => Parameters(47)
 );

decisionalgo44 : entity work.DeltaRSqrIncl2 --  Algorithm name: 0DR04-MU4ab-CJ30ab
  generic map (
     InputWidth1 => OutputWidthSelectMU,
     InputWidth2 => OutputWidthSelectJET,
     MaxTob1 => OutputWidthSelectMU,
     MaxTob2 => OutputWidthSelectJET,
     NumResultBits => 1 )
  port map (
     Tob1 => MUab,
     Tob2 => CJab,
     Results => Results(AlgoOffset44+0 downto AlgoOffset44), 
     Overflow => Overflow(AlgoOffset44+0 downto AlgoOffset44), 
     ClockBus => ClockBus,
     Parameters => Parameters(44)
 );

decisionalgo45 : entity work.DeltaRSqrIncl2 --  Algorithm name: 0DR04-MU6ab-CJ20ab
  generic map (
     InputWidth1 => OutputWidthSelectMU,
     InputWidth2 => OutputWidthSelectJET,
     MaxTob1 => OutputWidthSelectMU,
     MaxTob2 => OutputWidthSelectJET,
     NumResultBits => 1 )
  port map (
     Tob1 => MUab,
     Tob2 => CJab,
     Results => Results(AlgoOffset45+0 downto AlgoOffset45), 
     Overflow => Overflow(AlgoOffset45+0 downto AlgoOffset45), 
     ClockBus => ClockBus,
     Parameters => Parameters(45)
 );

decisionalgo46 : entity work.DeltaRSqrIncl2 --  Algorithm name: 0DR04-MU6ab-CJ25ab
  generic map (
     InputWidth1 => OutputWidthSelectMU,
     InputWidth2 => OutputWidthSelectJET,
     MaxTob1 => OutputWidthSelectMU,
     MaxTob2 => OutputWidthSelectJET,
     NumResultBits => 1 )
  port map (
     Tob1 => MUab,
     Tob2 => CJab,
     Results => Results(AlgoOffset46+0 downto AlgoOffset46), 
     Overflow => Overflow(AlgoOffset46+0 downto AlgoOffset46), 
     ClockBus => ClockBus,
     Parameters => Parameters(46)
 );

decisionalgo41 : entity work.MinDeltaPhiIncl2 --  Algorithm name: 10MINDPHI-CJ20ab-XE50
  generic map (
     InputWidth1 => OutputWidthSelectJET,
     InputWidth2 => 1,
     MaxTob1 => OutputWidthSelectJET,
     MaxTob2 => 1,
     NumResultBits => 1 )
  port map (
     Tob1 => CJab,
     Tob2 => XE,
     Results => Results(AlgoOffset41+0 downto AlgoOffset41), 
     Overflow => Overflow(AlgoOffset41+0 downto AlgoOffset41), 
     ClockBus => ClockBus,
     Parameters => Parameters(41)
 );

decisionalgo50 : entity work.DeltaRSqrIncl1 --  Algorithm name: 0DR24-2CMU4ab
  generic map (
     InputWidth => OutputWidthSelectMU,
     MaxTob => OutputWidthSelectMU,
     RequireOneBarrel => 0,
     NumResultBits => 1 )
  port map (
     Tob => CMUab,
     Results => Results(AlgoOffset50+0 downto AlgoOffset50), 
     Overflow => Overflow(AlgoOffset50+0 downto AlgoOffset50), 
     ClockBus => ClockBus,
     Parameters => Parameters(50)
 );

decisionalgo64 : entity work.Multiplicity --  Algorithm name: MULT-CMU4ab
  generic map (
     InputWidth => OutputWidthSelectMU,
     NumResultBits => 2 )
  port map (
     Tob => CMUab,
     Results => Results(AlgoOffset64+1 downto AlgoOffset64), 
     Overflow => Overflow(AlgoOffset64+1 downto AlgoOffset64), 
     ClockBus => ClockBus,
     Parameters => Parameters(64)
 );

decisionalgo65 : entity work.Multiplicity --  Algorithm name: MULT-CMU6ab
  generic map (
     InputWidth => OutputWidthSelectMU,
     NumResultBits => 2 )
  port map (
     Tob => CMUab,
     Results => Results(AlgoOffset65+1 downto AlgoOffset65), 
     Overflow => Overflow(AlgoOffset65+1 downto AlgoOffset65), 
     ClockBus => ClockBus,
     Parameters => Parameters(65)
 );

decisionalgo53 : entity work.DeltaEtaIncl2 --  Algorithm name: 0DETA04-EM8abi-MU10ab
  generic map (
     NumResultBits => 1,
     InputWidth1 => OutputWidthSelectEM,
     InputWidth2 => OutputWidthSelectMU,
     MaxTob1 => OutputWidthSelectEM,
     MaxTob2 => OutputWidthSelectMU )
  port map (
     Tob1 => EMabi,
     Tob2 => MUab,
     Results => Results(AlgoOffset53+0 downto AlgoOffset53), 
     Overflow => Overflow(AlgoOffset53+0 downto AlgoOffset53), 
     ClockBus => ClockBus,
     Parameters => Parameters(53)
 );

decisionalgo55 : entity work.DeltaPhiIncl2 --  Algorithm name: 0DPHI03-EM8abi-MU10ab
  generic map (
     NumResultBits => 1,
     InputWidth1 => OutputWidthSelectEM,
     InputWidth2 => OutputWidthSelectMU,
     MaxTob1 => OutputWidthSelectEM,
     MaxTob2 => OutputWidthSelectMU )
  port map (
     Tob1 => EMabi,
     Tob2 => MUab,
     Results => Results(AlgoOffset55+0 downto AlgoOffset55), 
     Overflow => Overflow(AlgoOffset55+0 downto AlgoOffset55), 
     ClockBus => ClockBus,
     Parameters => Parameters(55)
 );

decisionalgo54 : entity work.DeltaEtaIncl2 --  Algorithm name: 0DETA04-EM15abi-MUab
  generic map (
     NumResultBits => 1,
     InputWidth1 => OutputWidthSelectEM,
     InputWidth2 => OutputWidthSelectMU,
     MaxTob1 => OutputWidthSelectEM,
     MaxTob2 => OutputWidthSelectMU )
  port map (
     Tob1 => EMabi,
     Tob2 => MUab,
     Results => Results(AlgoOffset54+0 downto AlgoOffset54), 
     Overflow => Overflow(AlgoOffset54+0 downto AlgoOffset54), 
     ClockBus => ClockBus,
     Parameters => Parameters(54)
 );

decisionalgo56 : entity work.DeltaPhiIncl2 --  Algorithm name: 0DPHI03-EM15abi-MUab
  generic map (
     NumResultBits => 1,
     InputWidth1 => OutputWidthSelectEM,
     InputWidth2 => OutputWidthSelectMU,
     MaxTob1 => OutputWidthSelectEM,
     MaxTob2 => OutputWidthSelectMU )
  port map (
     Tob1 => EMabi,
     Tob2 => MUab,
     Results => Results(AlgoOffset56+0 downto AlgoOffset56), 
     Overflow => Overflow(AlgoOffset56+0 downto AlgoOffset56), 
     ClockBus => ClockBus,
     Parameters => Parameters(56)
 );

decisionalgo51 : entity work.DeltaRSqrIncl2 --  Algorithm name: 0DR24-CMU4ab-MU4ab
  generic map (
     InputWidth1 => OutputWidthSelectMU,
     InputWidth2 => OutputWidthSelectMU,
     MaxTob1 => OutputWidthSelectMU,
     MaxTob2 => OutputWidthSelectMU,
     NumResultBits => 1 )
  port map (
     Tob1 => CMUab,
     Tob2 => MUab,
     Results => Results(AlgoOffset51+0 downto AlgoOffset51), 
     Overflow => Overflow(AlgoOffset51+0 downto AlgoOffset51), 
     ClockBus => ClockBus,
     Parameters => Parameters(51)
 );

decisionalgo48 : entity work.InvariantMassInclusive2 --  Algorithm name: 2INVM8-CMU4ab-MU4ab
  generic map (
     InputWidth1 => OutputWidthSelectMU,
     InputWidth2 => OutputWidthSelectMU,
     MaxTob1 => OutputWidthSelectMU,
     MaxTob2 => OutputWidthSelectMU,
     NumResultBits => 1 )
  port map (
     Tob1 => CMUab,
     Tob2 => MUab,
     Results => Results(AlgoOffset48+0 downto AlgoOffset48), 
     Overflow => Overflow(AlgoOffset48+0 downto AlgoOffset48), 
     ClockBus => ClockBus,
     Parameters => Parameters(48)
 );

decisionalgo79 : entity work.MinDeltaPhiIncl2 --  Algorithm name: 10MINDPHI-AJ20s2-XE50
  generic map (
     InputWidth1 => OutputWidthSortJET,
     InputWidth2 => 1,
     MaxTob1 => 2,
     MaxTob2 => 1,
     NumResultBits => 1 )
  port map (
     Tob1 => AJs,
     Tob2 => XE,
     Results => Results(AlgoOffset79+0 downto AlgoOffset79), 
     Overflow => Overflow(AlgoOffset79+0 downto AlgoOffset79), 
     ClockBus => ClockBus,
     Parameters => Parameters(79)
 );

decisionalgo80 : entity work.EtCut --  Algorithm name: LATE-MU10s1
  generic map (
     InputWidth => NumberOfDelayedMuons,
     MaxTob => 1,
     NumResultBits => 1 )
  port map (
     Tob => LMUs,
     Results => Results(AlgoOffset80+0 downto AlgoOffset80), 
     Overflow => Overflow(AlgoOffset80+0 downto AlgoOffset80), 
     ClockBus => ClockBus,
     Parameters => Parameters(80)
 );

decisionalgo81 : entity work.SimpleCone --  Algorithm name: SC111-CJ15ab.ETA26
  generic map (
     InputWidth => OutputWidthSelectJET )
  port map (
     Tob => CJab,
     Results => Results(AlgoOffset81+0 downto AlgoOffset81), 
     Overflow => Overflow(AlgoOffset81+0 downto AlgoOffset81), 
     ClockBus => ClockBus,
     Parameters => Parameters(81)
 );

decisionalgo82 : entity work.SimpleCone --  Algorithm name: SC85-CJ15ab.ETA26
  generic map (
     InputWidth => OutputWidthSelectJET )
  port map (
     Tob => CJab,
     Results => Results(AlgoOffset82+0 downto AlgoOffset82), 
     Overflow => Overflow(AlgoOffset82+0 downto AlgoOffset82), 
     ClockBus => ClockBus,
     Parameters => Parameters(82)
 );

end Behavioral;
