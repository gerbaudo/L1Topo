library IEEE;
use IEEE.STD_LOGIC_1164.all;
use work.L1TopoDataTypes.all;
package parameter_map is
		constant NumberOfParameters : natural := 91;
		type ParameterRegisters is array (NumberOfParameters - 1 downto 0) of std_logic_vector(31 downto 0);
		function SortParameterMapper(reg : ParameterRegisters) return ParameterSpace;
		function AlgoParameterMapper(reg : ParameterRegisters) return ParameterSpace;
end parameter_map;
package body parameter_map is
		function SortParameterMapper(reg : ParameterRegisters) return ParameterSpace is
				variable result : ParameterSpace(NumberOfSortAlgorithms - 1 downto 0);
		begin
				--EMabi
				result(2)(0) := reg(0);		--MinET_0
				result(2)(1) := reg(1);		--IsoMask_1
				result(2)(2) := reg(2);		--MinEta_2
				result(2)(3) := reg(3);		--MaxEta_3

				--CJab
				result(12)(0) := reg(4);		--MinET_0
				result(12)(1) := reg(5);		--MinEta_1
				result(12)(2) := reg(6);		--MaxEta_2

				--AJjs
				result(13)(0) := reg(7);		--MinEta_0
				result(13)(1) := reg(8);		--MaxEta_1

				--AJs
				result(14)(0) := reg(9);		--MinEta_0
				result(14)(1) := reg(10);		--MaxEta_1

				--XE

				--MUab
				result(18)(0) := reg(11);		--MinET_0
				result(18)(1) := reg(12);		--MinEta_1
				result(18)(2) := reg(13);		--MaxEta_2

				--CMUab
				result(19)(0) := reg(14);		--MinET_0
				result(19)(1) := reg(15);		--MinEta_1
				result(19)(2) := reg(16);		--MaxEta_2

				--LMUs
				result(20)(0) := reg(17);		--MinEta_0
				result(20)(1) := reg(18);		--MaxEta_1

				return result;
		end function;
		function AlgoParameterMapper(reg : ParameterRegisters) return ParameterSpace is
				variable result : ParameterSpace(NumberOfAlgorithms - 1 downto 0);
		begin
				--05MINDPHI-AJj10s6-XE0
				result(32)(0) := reg(19);		--MinET1_0
				result(32)(1) := reg(20);		--MinET2_1
				result(32)(2) := reg(21);		--DeltaPhiMin_2

				--10MINDPHI-AJj10s6-XE0
				result(33)(0) := reg(22);		--MinET1_0
				result(33)(1) := reg(23);		--MinET2_1
				result(33)(2) := reg(24);		--DeltaPhiMin_2

				--15MINDPHI-AJj10s6-XE0
				result(34)(0) := reg(25);		--MinET1_0
				result(34)(1) := reg(26);		--MinET2_1
				result(34)(2) := reg(27);		--DeltaPhiMin_2

				--0DR04-MU4ab-CJ15ab
				result(43)(0) := reg(28);		--MinET1_0
				result(43)(1) := reg(29);		--MinET2_1
				result(43)(2) := reg(30);		--DeltaRMin_2
				result(43)(3) := reg(31);		--DeltaRMax_3

				--0DR04-MU4ab-CJ20ab
				result(47)(0) := reg(32);		--MinET1_0
				result(47)(1) := reg(33);		--MinET2_1
				result(47)(2) := reg(34);		--DeltaRMin_2
				result(47)(3) := reg(35);		--DeltaRMax_3

				--0DR04-MU4ab-CJ30ab
				result(44)(0) := reg(36);		--MinET1_0
				result(44)(1) := reg(37);		--MinET2_1
				result(44)(2) := reg(38);		--DeltaRMin_2
				result(44)(3) := reg(39);		--DeltaRMax_3

				--0DR04-MU6ab-CJ20ab
				result(45)(0) := reg(40);		--MinET1_0
				result(45)(1) := reg(41);		--MinET2_1
				result(45)(2) := reg(42);		--DeltaRMin_2
				result(45)(3) := reg(43);		--DeltaRMax_3

				--0DR04-MU6ab-CJ25ab
				result(46)(0) := reg(44);		--MinET1_0
				result(46)(1) := reg(45);		--MinET2_1
				result(46)(2) := reg(46);		--DeltaRMin_2
				result(46)(3) := reg(47);		--DeltaRMax_3

				--10MINDPHI-CJ20ab-XE50
				result(41)(0) := reg(48);		--MinET1_0
				result(41)(1) := reg(49);		--MinET2_1
				result(41)(2) := reg(50);		--DeltaPhiMin_2

				--0DR24-2CMU4ab
				result(50)(0) := reg(51);		--MinET1_0
				result(50)(1) := reg(52);		--MinET2_1
				result(50)(2) := reg(53);		--DeltaRMin_2
				result(50)(3) := reg(54);		--DeltaRMax_3

				--MULT-CMU4ab
				result(64)(0) := reg(55);		--MinET_0

				--MULT-CMU6ab
				result(65)(0) := reg(56);		--MinET_0

				--0DETA04-EM8abi-MU10ab
				result(53)(0) := reg(57);		--MinET1_0
				result(53)(1) := reg(58);		--MinET2_1
				result(53)(2) := reg(59);		--MinDeltaEta_2
				result(53)(3) := reg(60);		--MaxDeltaEta_3

				--0DPHI03-EM8abi-MU10ab
				result(55)(0) := reg(61);		--MinET1_0
				result(55)(1) := reg(62);		--MinET2_1
				result(55)(2) := reg(63);		--MinDeltaPhi_2
				result(55)(3) := reg(64);		--MaxDeltaPhi_3

				--0DETA04-EM15abi-MUab
				result(54)(0) := reg(65);		--MinET1_0
				result(54)(1) := reg(66);		--MinET2_1
				result(54)(2) := reg(67);		--MinDeltaEta_2
				result(54)(3) := reg(68);		--MaxDeltaEta_3

				--0DPHI03-EM15abi-MUab
				result(56)(0) := reg(69);		--MinET1_0
				result(56)(1) := reg(70);		--MinET2_1
				result(56)(2) := reg(71);		--MinDeltaPhi_2
				result(56)(3) := reg(72);		--MaxDeltaPhi_3

				--0DR24-CMU4ab-MU4ab
				result(51)(0) := reg(73);		--MinET1_0
				result(51)(1) := reg(74);		--MinET2_1
				result(51)(2) := reg(75);		--DeltaRMin_2
				result(51)(3) := reg(76);		--DeltaRMax_3

				--2INVM8-CMU4ab-MU4ab
				result(48)(0) := reg(77);		--MinET1_0
				result(48)(1) := reg(78);		--MinET2_1
				result(48)(2) := reg(79);		--MinMSqr_2
				result(48)(3) := reg(80);		--MaxMSqr_3

				--10MINDPHI-AJ20s2-XE50
				result(79)(0) := reg(81);		--MinET1_0
				result(79)(1) := reg(82);		--MinET2_1
				result(79)(2) := reg(83);		--DeltaPhiMin_2

				--LATE-MU10s1
				result(80)(0) := reg(84);		--MinET_0

				--SC111-CJ15ab.ETA26
				result(81)(0) := reg(85);		--MinET_0
				result(81)(1) := reg(86);		--MinSumET_1
				result(81)(2) := reg(87);		--MaxRSqr_2

				--SC85-CJ15ab.ETA26
				result(82)(0) := reg(88);		--MinET_0
				result(82)(1) := reg(89);		--MinSumET_1
				result(82)(2) := reg(90);		--MaxRSqr_2

				return result;
		end function;
end package body;
