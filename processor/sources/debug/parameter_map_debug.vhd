--------------------
-- Parameter Map  --
--------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use work.L1TopoDataTypes.all;

package parameter_map is

	-- Number of parameters: --TODO: adjust to your needs!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	constant NumberOfParameters : natural := 7;  --"debug_select"


	type ParameterRegisters is array (NumberOfParameters - 1 downto 0) of std_logic_vector(31 downto 0);

	function SortParameterMapper(reg : ParameterRegisters) return ParameterSpace;
	function AlgoParameterMapper(reg : ParameterRegisters) return ParameterSpace;

end parameter_map;

package body parameter_map is
	function SortParameterMapper(reg : ParameterRegisters) return ParameterSpace is
		variable result : ParameterSpace(NumberOfSortAlgorithms - 1 downto 0);
	begin
		--mapping of sort algorithm parameters:
		--result(algo_nr)(parameter_nr) := reg(parameter_register_nr);
		
		--sortalgo0: JetSelect
		result(0)(0) := reg(0); --minEt
		result(0)(1) := reg(1); --minEta
		result(0)(2) := reg(2); --maxEta

		return result;
	end function;

	function AlgoParameterMapper(reg : ParameterRegisters) return ParameterSpace is
		variable result : ParameterSpace(NumberOfAlgorithms - 1 downto 0);
	begin
		--mapping of decision algorithm parameters:
		--result(algo_nr)(parameter_nr) := reg(parameter_register_nr);
        
        --decisionalgo0: JetHT
		result(0)(0)  := reg(3); --MinEt
		result(0)(1)  := reg(4); --MinEta
		result(0)(2)  := reg(5); --MaxEta
		result(0)(3)  := reg(6); --MinHt

		return result;    -- REMEMBER TO UPDATE NumberOfParameters in this file !!!

	end function;

end package body;