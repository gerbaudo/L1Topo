-- The ipbus slaves live in this entity - modify according to requirements
--
-- Ports can be added to give ipbus slaves access to the chip top level.
--
-- Dave Newbold, February 2011

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.l1topo_package.all;
use work.ipbus.ALL;
use work.L1TopoDataTypes.all;
use work.L1TopoGTConfiguration.all;
use work.rod_l1_topo_types_const.all;

entity slaves_debug is
	port(
		sysclk40: in std_logic;
		sysclk80: in std_logic;
		sysclk160: in std_logic;
		sysclk_pll_locked: in std_logic;
		
		ipb_in: in ipb_wbus;
		ipb_out: out ipb_rbus;
		
		
		ipbBridge_idelayStatus    : in std_logic_vector(24 downto 0);
        ipbBridge_alignmentStatus : in std_logic_vector(19 downto 0);
		ipbBridgeErrorCounter     : in std_logic_vector(29 downto 0);
		ipbBridgeErrorCounterReset: out std_logic_vector( 5 downto 0);
		
		
        xadc_control: out std_logic_vector(25 downto 0);
        xadc_status:  in  std_logic_vector(22 downto 0);
                
		
		ttcBridge_control: out std_logic_vector( 5 downto 0);
        ttcBridge_status : in  std_logic_vector(14 downto 0);
        ttcBroadcast    : in  std_logic;
		
		
		rod_control_register: out rod_control_registers_array;
        rod_status_register : in  rod_status_registers_array;
		
		QuadControl: out arraySLV32(QuadHigh downto QuadLow);
		QuadStatus:  in  arraySLV32(QuadHigh downto QuadLow);
        ChannelControl: out arraySLV32(79 downto 0);
        ChannelStatus: in arraySLV32(79 downto 0);
        DataShift40MHz: out  arraySLV3(MGTHigh downto MGTLow);--+-25 ns delay, do not expect this to work for large delays
		
		
		
--		spyData36BitFromMGTs: in arraySLV36(MGTHigh downto MGTLow);
--        enablePlaybackOfMGTs: out std_logic;
--        playbackData36BitFakingMGTs: out arraySLV36(MGTHigh downto MGTLow);
        
        spyData128BitFromDeserialisers: in arraySLV128(79 downto 0);
        enablePlaybackOfDeserialisers: out std_logic;
        playbackData128BitFakingDeserialisers: out arraySLV128(79 downto 0);
        
        
        SortParameters: out ParameterSpace(NumberOfSortAlgorithms - 1 downto 0);
        AlgoParameters: out ParameterSpace(NumberOfAlgorithms - 1 downto 0);
        
        
        spyData32BitFromAlgos: in std_logic_vector(31 downto 0);
        enablePlaybackOfAlgos: out std_logic;
        playbackData32BitFakingAlgos: out std_logic_vector(31 downto 0);
        
        ctpOutputControl: out std_logic_vector(1 downto 0);
        ctpOutputMask: out std_logic_vector(31 downto 0);
        
        -- MISC
        
        ProcessorID:     in std_logic_vector(31 downto 0);
        FirmwareVersion: in std_logic_vector(31 downto 0);
        
        -- DEBUG
        
        DebugOutput  : in BitArray128(256 - 1 downto 0);
        
--        AlgoDebug: in BitArray128(256 - 1 downto 0);
        debugCRC: in arraySLV256(11 downto 0)
        
--        debugJetArray: in std_logic_vector(127 downto 0);
--        debugClusterArray: in std_logic_vector(127 downto 0);
--        debugGenericJetArray: in std_logic_vector(127 downto 0);
--        debugGenericClusterArray: in std_logic_vector(127 downto 0);
--        debugDPhi: in std_logic_vector(127 downto 0 )
--        debugDPhi2: in std_logic_vector(127 downto 0);
        
        
--		debugPlaybackSpy128: out std_logic_vector(432 downto 0);
--		debugPlaybackSpy32: out std_logic_vector(176 downto 0)
		
		
		
--		idelay_counter_0: in std_logic_vector(4 downto 0);
--		idelay_counter_1: in std_logic_vector(4 downto 0);
--		idelay_counter_2: in std_logic_vector(4 downto 0);
--		idelay_counter_3: in std_logic_vector(4 downto 0);
--		idelay_counter_4: in std_logic_vector(4 downto 0)
	);

end slaves_debug;

architecture rtl of slaves_debug is

	constant NSLV: positive := 54;
	
	attribute shreg_extract: string;
	
	type ipbw_regArray is array(natural range<>) of ipb_wbus_array(NSLV-1 downto 0);
	type ipbr_regArray is array(natural range<>) of ipb_rbus_array(NSLV-1 downto 0);
	
	signal ipbw: ipb_wbus_array(NSLV-1 downto 0);
	signal ipbr: ipb_rbus_array(NSLV-1 downto 0);
	
	signal ipbw_reg: ipbw_regArray(1 downto 0);
    attribute shreg_extract of ipbw_reg: signal is "no";
    
    signal ipbr_reg: ipbr_regArray(1 downto 0);
	attribute shreg_extract of ipbr_reg: signal is "no";
	
	signal ipb_out_int: ipb_rbus;
	
	signal ipb_rst: std_logic;
	
	
	signal GeneralStatus_32bit:  std_logic_vector(31 downto 0);
	signal GeneralControl_32bit: std_logic_vector(31 downto 0);
	signal GeneralPulse_32bit:   std_logic_vector(31 downto 0);
	
	
	signal ipbBridge_idelayStatus_32bit    : std_logic_vector(31 downto 0);
    signal ipbBridge_alignmentStatus_32bit : std_logic_vector(31 downto 0);
	signal ipbBridgeErrorCounter_32bit     : std_logic_vector(31 downto 0);
	signal ipbBridgeErrorCounterReset_32bit: std_logic_vector(31 downto 0);
		
	signal ttcBridge_status_32bit:  std_logic_vector(31 downto 0);
	signal ttcBridge_control_32bit: std_logic_vector(31 downto 0);
	signal ttcErrorCounter_32bit:       std_logic_vector(31 downto 0);
	signal ttcErrorCounterReset_32bit:  std_logic_vector(31 downto 0);
	
	signal PlaybackSpyControl: std_logic_vector(31 downto 0);
	

    signal QuadControl_vector: std_logic_vector( 20*32-1 downto 0);
    signal QuadStatus_vector:  std_logic_vector( 20*32-1 downto 0);
    signal ChannelControl_vector: std_logic_vector( 80*32-1 downto 0);
    signal ChannelStatus_vector:  std_logic_vector( 80*32-1 downto 0);
    signal DataShift40MHz_vector: std_logic_vector( (MGTHigh-MGTLow+1)*32-1 downto 0);
    signal CRCErrorCounter_vector: std_logic_vector( 20*32-1 downto 0);
    
    
    
    

    signal ROD_Debug: std_logic_vector(63 downto 0);
    signal ROD_Slices_vector: std_logic_vector(11*32-1 downto 0);
    signal ROD_Offsets_vector: std_logic_vector(11*32-1 downto 0);


    signal fakeDataCounter: std_logic_vector(127 downto 0);
    signal fakeData: arraySLV128(MGTHigh downto MGTLow);
    signal playbackData128BitFakingDeserialisers_int: arraySLV128(79 downto 0);
    signal ipbScopeControl: std_logic_vector(31 downto 0);
--    signal debugParameters: std_logic_vector(127 downto 0);
--	signal ipbusPlaybackCharIsKFakingMGTs: std_logic_vector(31 downto 0);
--	signal ipbusSpyCharIsKFromMGTs: std_logic_vector(31 downto 0);

	signal DebugOutput128 : std_logic_vector(127 downto 0);
--	signal AlgoDebug128 : std_logic_vector(127 downto 0);

    signal rewindPlaybackSpy:   std_logic;

    signal ctpOutputControl_32bit: std_logic_vector(31 downto 0);
    

begin
    
    ipb_rst <= not sysclk_pll_locked;
    
    
    fabric: entity work.ipbus_fabric
        generic map(NSLV => NSLV)
        port map(
            ipb_in => ipb_in,
            ipb_out => ipb_out_int,
            ipb_to_slaves => ipbw_reg(0),
            ipb_from_slaves => ipbr_reg(0)
        );
    
    ipb_out <= ipb_out_int;



    process(sysclk40) begin
        if rising_edge(sysclk40) then 
            for i in NSLV-1 downto 0 loop
                for stage in 1 downto 1 loop 
                    ipbw_reg(stage) <= ipbw_reg(stage-1); 
                    ipbr_reg(stage-1) <= ipbr_reg(stage);     
                end loop;
                ipbw(i) <= ipbw_reg(1)(i);
                ipbr_reg(1)(i) <= ipbr(i);
            end loop;
        end if;
    end process;




----------------------------------------------------------------

    
slave0_ProcessorID: entity work.ipbus_slave_status
        generic map(addr_width => 0)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(0),
            ipbus_out => ipbr(0),
            d => ProcessorID
        );


----------------------------------------------------------------

    
slave1_FirmwareVersion: entity work.ipbus_slave_status
        generic map(addr_width => 0)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(1),
            ipbus_out => ipbr(1),
            d => FirmwareVersion
        );
            
            
----------------------------------------------------------------

    process(sysclk40, sysclk_pll_locked) begin
        if sysclk_pll_locked='0' then GeneralStatus_32bit(0) <= '1';
        elsif rising_edge(sysclk40) then
            if GeneralPulse_32bit(0)='1' or GeneralPulse_32bit(1)='1' then GeneralStatus_32bit(0) <= '0'; end if;
        end if; 
    end process;

    
slave2_GeneralStatus: entity work.ipbus_slave_status
        generic map(addr_width => 0)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(2),
            ipbus_out => ipbr(2),
            d => GeneralStatus_32bit
        );
    

----------------------------------------------------------------    


slave3_GeneralControl: entity work.ipbus_slave_control
        generic map(addr_width => 0)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(3),
            ipbus_out => ipbr(3),
            q => GeneralControl_32bit
        );
                
            
----------------------------------------------------------------    


slave4_GeneralPulse: entity work.ipbus_slave_control
        generic map(addr_width => 0)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(4),
            ipbus_out => ipbr(4),
            q => GeneralPulse_32bit
        );

----------------------------------------------------------------


    ipbBridge_idelayStatus_32bit <= (31 downto 25 => '0') & ipbBridge_idelayStatus;
    
slave5_ipbBridgeIDelayStatus: entity work.ipbus_slave_status
        generic map(addr_width => 0)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(5),
            ipbus_out => ipbr(5),
            d => ipbBridge_idelayStatus_32bit
        );
            
----------------------------------------------------------------


    ipbBridge_alignmentStatus_32bit <= (31 downto 20 => '0') & ipbBridge_alignmentStatus;
    
slave6_ipbBridgeAlignmentStatus: entity work.ipbus_slave_status
        generic map(addr_width => 0)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(6),
            ipbus_out => ipbr(6),
            d => ipbBridge_alignmentStatus_32bit
        );

    
----------------------------------------------------------------    
    
    
    ipbBridgeErrorCounter_32bit <= (31 downto 30 => '0') & ipbBridgeErrorCounter(29 downto 0);  
          
slave7_ipbBridgeErrorCounter: entity work.ipbus_slave_status
        generic map(addr_width => 0)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(7),
            ipbus_out => ipbr(7),
            d => ipbBridgeErrorCounter_32bit
        );


----------------------------------------------------------------


slave8_ipbBridgeErrorCounterReset: entity work.ipbus_slave_reg_pulse
        generic map(addr_width => 0)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(8),
            ipbus_out => ipbr(8),
            q => ipbBridgeErrorCounterReset_32bit
        );
        
    ipbBridgeErrorCounterReset(4 downto 0) <= ipbBridgeErrorCounterReset_32bit(4 downto 0); 
    ipbBridgeErrorCounterReset(5) <= ipbBridgeErrorCounterReset_32bit(5) or GeneralPulse_32bit(1); 


----------------------------------------------------------------    


    ttcBridge_status_32bit <= (31 downto 15 => '0') & ttcBridge_status(14 downto 0);  
      
slave9_ttcBridgeStatus: entity work.ipbus_slave_status
        generic map(addr_width => 0)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(9),
            ipbus_out => ipbr(9),
            d => ttcBridge_status_32bit
        );
                
            
----------------------------------------------------------------    


slave10_ttcBridgeControl: entity work.ipbus_slave_control
        generic map(addr_width => 0)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(10),
            ipbus_out => ipbr(10),
            q => ttcBridge_control_32bit
        );
        
    ttcBridge_control(4 downto 0) <= ttcBridge_control_32bit(4 downto 0);


----------------------------------------------------------------


slave11_ttcBridgeErrorCounterReset: entity work.ipbus_slave_reg_pulse
        generic map(addr_width => 0)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(11),
            ipbus_out => ipbr(11),
            q => ttcErrorCounterReset_32bit
        );
        
    ttcBridge_control(5) <= ttcErrorCounterReset_32bit(0);


----------------------------------------------------------------



slave12_playbackSpyControl: entity work.ipbus_slave_control
        generic map(addr_width => 0)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(12),
            ipbus_out => ipbr(12),
            q => PlaybackSpyControl
        );
    
--    enablePlaybackOfMGTs <= PlaybackSpyControl(3);
    enablePlaybackOfDeserialisers <= PlaybackSpyControl(4);
    enablePlaybackOfAlgos <= PlaybackSpyControl(5);
                
            
----------------------------------------------------------------    


slave13_xadc_drpInterface: entity work.ipbus_slave_xadc
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(13),
            ipbus_out => ipbr(13),
            xadc_control => xadc_control,
            xadc_status  => xadc_status   
        );
        
        
----------------------------------------------------------------


slave14_TestRAM: entity work.ipbus_slave_testram
        generic map(addr_width => 10)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(14),
            ipbus_out => ipbr(14)
        );


----------------------------------------------------------------


slave15_QuadControl: entity work.ipbus_slave_control_N
        generic map(numberOfRegisters => 20 )
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(15),
            ipbus_out => ipbr(15),
            q => QuadControl_vector
        );
  
  
    QuadControl_GEN: for i in QuadHigh downto QuadLow generate begin   
        QuadControl(i) <= QuadControl_vector(i*32+31 downto i*32);
    end generate;


----------------------------------------------------------------


    QuadStatus_GEN: for i in QuadHigh downto QuadLow generate begin     
       QuadStatus_vector(i*32+31 downto i*32) <= QuadStatus(i);
    end generate;
    
    
    
slave16_QuadStatus: entity work.ipbus_slave_status_N
        generic map(numberOfRegisters => 20 )
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(16),
            ipbus_out => ipbr(16),
            d => QuadStatus_vector
        );


----------------------------------------------------------------


slave17_ChannelControl: entity work.ipbus_slave_control_N
        generic map(numberOfRegisters => 80 )
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(17),
            ipbus_out => ipbr(17),
            q => ChannelControl_vector
        );
  
  
    ChannelControl_GEN: for i in 79 downto 0 generate begin   
        ChannelControl(i) <= ChannelControl_vector(i*32+31 downto i*32);
    end generate;


----------------------------------------------------------------


    ChannelStatus_GEN: for i in 79 downto 0 generate begin     
       ChannelStatus_vector(i*32+31 downto i*32) <= ChannelStatus(i);
    end generate;
    
    
    
slave18_ChannelStatus: entity work.ipbus_slave_status_N
        generic map(numberOfRegisters => 80 )
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(18),
            ipbus_out => ipbr(18),
            d => ChannelStatus_vector
        );


----------------------------------------------------------------


slave19_DataDelay: entity work.ipbus_slave_control_N
        generic map(numberOfRegisters => (MGTHigh-MGTLow+1) )
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(19),
            ipbus_out => ipbr(19),
            q => DataShift40MHz_vector
        );
  
  
    DataShift40MHz_GEN: for i in (MGTHigh-MGTLow) downto 0 generate begin   
        DataShift40MHz(MGTLow+i) <= DataShift40MHz_vector(i*32+2 downto i*32);
    end generate;
    
    
----------------------------------------------------------------

----------------------------------------------------------------


--    process(sysclk40) begin
--        if rising_edge(sysclk40) then
--            fakeDataCounter <= std_logic_vector(unsigned(fakeDataCounter)+1);
--        end if;
--    end process;

--FAKEDATA_GEN: for i in MGTHigh downto MGTLow generate begin
--    fakeData(i) <= fakeDataCounter;
--end generate;

    rewindPlaybackSpy <= '1' when ttcBroadcast='1' or GeneralPulse_32bit(2)='1' else '0';


slave21_playbackSpyDataOfDeserialisers: entity work.ipbus_slave_playbackspy128
        port map(
            sysclk40 => sysclk40,
            sysclk160 => sysclk160,
            ipbus_in => ipbw(21),
            ipbus_out => ipbr(21),
            
            spyData128Bit => spyData128BitFromDeserialisers(MGTHigh downto MGTLow),
            playbackData128Bit => playbackData128BitFakingDeserialisers_int,
           
            enableSpy => PlaybackspyControl(1),
            
            enableAddressIncrement => '1',
            enableAddressCounterSaturation => PlaybackSpyControl(6),
            rewindPlaybackSpy => rewindPlaybackSpy,
            fineDelay => PlaybackSpyControl(8 downto 7)
--            debugPlaybackSpy128 => debugPlaybackSpy128
        );


    playbackData128BitFakingDeserialisers <= playbackData128BitFakingDeserialisers_int;


----------------------------------------------------------------


--slave51_playbackSpyDataOfDeserialisersParallel: entity work.ipbus_slave_playbackspy128parallel
--        port map(
--            sysclk40  => sysclk40,
--            ipbus_in  => ipbw(51),
--            ipbus_out => ipbr(51),
            
--            spyData128Bit       => spyData128BitFromDeserialisers(MGTHigh downto MGTLow),
--            playbackData128Bit  => open,
           
--            enablePlayback => PlaybackspyControl(4),
--            enableSpy => PlaybackspyControl(1),
            
--            enableAddressCounterSaturation => PlaybackSpyControl(6),
--            rewindPlaybackSpy => rewindPlaybackSpy
--        );
        
        
----------------------------------------------------------------

slave22_PlaybackSpyDataOfAlgoResults: entity work.ipbus_slave_playbackspy32
        port map(
            sysclk40 => sysclk40,
            ipbus_in => ipbw(22),
            ipbus_out => ipbr(22),
            
            spyData32Bit => spyData32BitFromAlgos,
            playbackData32Bit => playbackData32BitFakingAlgos,
            enableSpy => PlaybackspyControl(2),
            rewindPlaybackSpy => rewindPlaybackSpy,
            enableAddressCounterSaturation => PlaybackSpyControl(6)  
--            debugPlaybackSpy32 => debugPlaybackSpy32            
        );
    
------------------------------------------------------------------

--slave22_ROD_OvBusy: entity work.ipbus_slave_control
--        generic map(addr_width => 0)
--        port map(
--            clk => sysclk40,
--            reset => ipb_rst,
--            ipbus_in => ipbw(22),
--            ipbus_out => ipbr(22),
--            q => ROD_OverwriteBusy
--        );
    
----------------------------------------------------------------

slave23_ROD_FirmwareVersion: entity work.ipbus_slave_status
        generic map(addr_width => 0)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(23),
            ipbus_out => ipbr(23),
            d => rod_status_register(0)
        );

----------------------------------------------------------------

slave24_ROD_Debug: entity work.ipbus_slave_status
        generic map(addr_width => 0)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(24),
            ipbus_out => ipbr(24),
            d => rod_status_register(2)
        );


----------------------------------------------------------------

slave25_ROD_Reset: entity work.ipbus_slave_reg_pulse
        generic map(addr_width => 0)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(25),
            ipbus_out => ipbr(25),
            q => rod_control_register(0)          
        );

----------------------------------------------------------------

slave26_ROD_L1A_Offset: entity work.ipbus_slave_control
        generic map(addr_width => 0)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(26),
            ipbus_out => ipbr(26),
            q => rod_control_register(1)
        );


----------------------------------------------------------------

slave27_ROD_L1A_Fill: entity work.ipbus_slave_control
        generic map(addr_width => 0)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(27),
            ipbus_out => ipbr(27),
            q => rod_control_register(2)
        );


----------------------------------------------------------------

slave28_ROD_Slices: entity work.ipbus_slave_control_N
        generic map(numberOfRegisters => 11)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(28),
            ipbus_out => ipbr(28),
            q => ROD_Slices_vector
        );

ROD_Slices_GEN: for i in 13 downto 3 generate
    rod_control_register(i) <= ROD_Slices_vector((i-3)*32+31 downto (i-3)*32);
end generate;

----------------------------------------------------------------

slave29_ROD_Offsets: entity work.ipbus_slave_control_N
        generic map(numberOfRegisters => 11)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(29),
            ipbus_out => ipbr(29),
            q => ROD_Offsets_vector
        );

ROD_Offsets_GEN: for i in 24 downto 14 generate
    rod_control_register(i) <= ROD_Offsets_vector((i-14)*32+31 downto (i-14)*32);
end generate;


----------------------------------------------------------------


slave30_Delay_CMX_Data: entity work.ipbus_slave_control
        generic map(addr_width => 0)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(30),
            ipbus_out => ipbr(30),
            q => rod_control_register(25)
        );


----------------------------------------------------------------


slave31_AlgoParameters: entity work.ipbus_slave_parameters
        port map(
            clk => sysclk40,
            ipbus_in => ipbw(31),
            ipbus_out => ipbr(31),
            SortParameters => SortParameters,
            AlgoParameters => AlgoParameters
--            debug => debugParameters
        );
        
        
----------------------------------------------------------------


slave32_ipbScopeControl: entity work.ipbus_slave_control
        generic map(addr_width => 0)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(32),
            ipbus_out => ipbr(32),
            q => ipbScopeControl
            
        );
        
----------------------------------------------------------------


-- DEBUG outputs of algorithms
process (sysclk40)
	variable counter: std_logic_vector(8-1 downto 0) := (others => '0');
	begin
	if rising_edge(sysclk40) then
		counter := std_logic_vector(unsigned(counter)+1);
		DebugOutput128 <= DebugOutput(to_integer(unsigned(counter)));
	end if;
end process;

slave33_AlgoDebug : entity work.ipbus_slave_ipbScope
	port map(
		sysclk40  => sysclk40,
		sysclk160 => sysclk160,
		ipbus_in  => ipbw(33),
		ipbus_out => ipbr(33),
		control   => ipbScopeControl,
		probeData => DebugOutput128,
		rewind    => ttcBroadcast
	);

----------------------------------------------------------------
        
        
--slave34_AlgoDebugTAUab: entity work.ipbus_slave_ipbScope
--        port map(
--            sysclk40 => sysclk40,
--            sysclk160 => sysclk160,
--            ipbus_in => ipbw(34),
--            ipbus_out => ipbr(34),
            
--            control => ipbScopeControl,
--            probeData => AlgoDebug(1),
--            rewind => ttcBroadcast
--        );

------------------------------------------------------------------
        
        
--slave35_AlgoDebugEMs: entity work.ipbus_slave_ipbScope
--        port map(
--            sysclk40 => sysclk40,
--            sysclk160 => sysclk160,
--            ipbus_in => ipbw(35),
--            ipbus_out => ipbr(35),
            
--            control => ipbScopeControl,
--            probeData => AlgoDebug(2),
--            rewind => ttcBroadcast
--        );

------------------------------------------------------------------
        
        
--slave36_AlgoDebugTAUs: entity work.ipbus_slave_ipbScope
--        port map(
--            sysclk40 => sysclk40,
--            sysclk160 => sysclk160,
--            ipbus_in => ipbw(36),
--            ipbus_out => ipbr(36),
            
--            control => ipbScopeControl,
--            probeData => AlgoDebug(3),
--            rewind => ttcBroadcast
--        );        

------------------------------------------------------------------
        
        
--slave37_AlgoDebugAJab: entity work.ipbus_slave_ipbScope
--        port map(
--            sysclk40 => sysclk40,
--            sysclk160 => sysclk160,
--            ipbus_in => ipbw(37),
--            ipbus_out => ipbr(37),
            
--            control => ipbScopeControl,
--            probeData => AlgoDebug(4),
--            rewind => ttcBroadcast
--        );

------------------------------------------------------------------
        
        
--slave38_AlgoDebugAJs: entity work.ipbus_slave_ipbScope
--        port map(
--            sysclk40 => sysclk40,
--            sysclk160 => sysclk160,
--            ipbus_in => ipbw(38),
--            ipbus_out => ipbr(38),
            
--            control => ipbScopeControl,
--            probeData => AlgoDebug(5),
--            rewind => ttcBroadcast
--        );
        
----------------------------------------------------------------


--slave_debugCRC_GEN: for i in 11 downto 0 generate begin

--    slave_debugCRC_i: entity work.ipbus_slave_bram256
--        port map(
--            sysclk40 => sysclk40,
--            ipbus_in => ipbw(39+i),
--            ipbus_out => ipbr(39+i),
--            spyData => debugCRC(i),
--            rewindSpyMemory => playbackSpyControl(10)
--        );
        
--end generate;





        
        
----------------------------------------------------------------


slave52_ctpOutputControl: entity work.ipbus_slave_control
        generic map(addr_width => 0)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(52),
            ipbus_out => ipbr(52),
            q => ctpOutputControl_32bit
            
        );

ctpOutputControl <= ctpOutputControl_32bit(1 downto 0);


----------------------------------------------------------------


slave53_ctpOutputMask: entity work.ipbus_slave_control
        generic map(addr_width => 0)
        port map(
            clk => sysclk40,
            reset => ipb_rst,
            ipbus_in => ipbw(53),
            ipbus_out => ipbr(53),
            q => ctpOutputMask
            
        );

----------------------------------------------------------------
        










--    slave5_ipbusPlaybackCharIsKFakingMGTs: entity work.ipbus_slave_control
--        generic map(addr_width => 0)
--        port map(
--            clk => sysclk40,
--            reset => ipb_rst,
--            ipbus_in => ipbw(5),
--            ipbus_out => ipbr(5),
--            q => ipbusPlaybackCharIsKFakingMGTs
            
--        );


----------------------------------------------------------------


  
--    slave6_ipbusSpyCharIsKFromMGTs: entity work.ipbus_slave_status
--        generic map(addr_width => 0)
--        port map(
--            clk => sysclk40,
--            reset => ipb_rst,
--            ipbus_in => ipbw(6),
--            ipbus_out => ipbr(6),
--            d => ipbusSpyCharIsKFromMGTs
            
--        );

----------------------------------------------------------------

--    slave23_playbackSpyDataOfMGTs: entity work.ipbus_slave_playbackspy32_4
--        port map(
--            sysclk40 => sysclk40,
--            sysclk160 => sysclk160,
--            ipbus_in => ipbw(23),
--            ipbus_out => ipbr(23),
            
            
            
--            spyData36Bit => spyData36BitFromMGTs,
--            playbackData36Bit => playbackData36BitFakingMGTs,
            
--            ipbusPlaybackCharIsK => ipbusPlaybackCharIsKFakingMGTs,
--            ipbusSpyCharIsK => ipbusSpyCharIsKFromMGTs,
            
--            enableSpy => PlaybackspyControl(0),
--            rewindPlaybackSpy => ttcBroadcast,
--            enableAddressCounterSaturation => PlaybackSpyControl(6),
            
--            master_strobe => ipb_in.ipb_strobe,
--            ipbr_master => ipb_out_int,
--            ipbr3 => ipbr(3), ???
--            ipbr5 => ipbr(5) ???
            
--        );
    
----------------------------------------------------------------
  
  
  
    
  
  
    

--    slave28: entity work.ipbus_slave_status
--        generic map(addr_width => 2)
--        port map(
--            clk => sysclk40,
--            reset => ipb_rst,
--            ipbus_in => ipbw(28),
--            ipbus_out => ipbr(28),
--            d => debugParameters
--        );


--    slave28: entity work.ipbus_slave_ipbScope
--        port map(
--            sysclk40 => sysclk40,
--            sysclk160 => sysclk160,
--            ipbus_in => ipbw(28),
--            ipbus_out => ipbr(28),
            
--            control => ipbScopeControl,
--            probeData => playbackData128BitFakingDeserialisers_int(12),
--            rewind => ttcBroadcast
--        );


--    slave29: entity work.ipbus_slave_ipbScope
--        port map(
--            sysclk40 => sysclk40,
--            sysclk160 => sysclk160,
--            ipbus_in => ipbw(29),
--            ipbus_out => ipbr(29),
            
--            control => ipbScopeControl,
--            probeData => playbackData128BitFakingDeserialisers_int(52),
--            rewind => ttcBroadcast
--        );


--    slave30: entity work.ipbus_slave_ipbScope
--        port map(
--            sysclk40 => sysclk40,
--            sysclk160 => sysclk160,
--            ipbus_in => ipbw(30),
--            ipbus_out => ipbr(30),
            
--            control => ipbScopeControl,
--            probeData => debugJetArray,
--            rewind => ttcBroadcast
--        );
        
    
--    slave31: entity work.ipbus_slave_ipbScope
--        port map(
--            sysclk40 => sysclk40,
--            sysclk160 => sysclk160,
--            ipbus_in => ipbw(31),
--            ipbus_out => ipbr(31),
            
--            control => ipbScopeControl,
--            probeData => debugGenericJetArray,
--            rewind => ttcBroadcast
--        );


--    slave32: entity work.ipbus_slave_ipbScope
--        port map(
--            sysclk40 => sysclk40,
--            sysclk160 => sysclk160,
--            ipbus_in => ipbw(32),
--            ipbus_out => ipbr(32),
            
--            control => ipbScopeControl,
--            probeData => debugClusterArray,
--            rewind => ttcBroadcast
--        );
        
    
--    slave33: entity work.ipbus_slave_ipbScope
--        port map(
--            sysclk40 => sysclk40,
--            sysclk160 => sysclk160,
--            ipbus_in => ipbw(33),
--            ipbus_out => ipbr(33),
            
--            control => ipbScopeControl,
--            probeData => debugGenericClusterArray,
--            rewind => ttcBroadcast
--        );



--    slave34_ipbScopeDebugDPhi: entity work.ipbus_slave_ipbScope
--        port map(
--            sysclk40 => sysclk40,
--            sysclk160 => sysclk160,
--            ipbus_in => ipbw(34),
--            ipbus_out => ipbr(34),
            
--            control => ipbScopeControl,
--            probeData => debugDPhi,
--            rewind => ttcBroadcast
--        );



--    slave35: entity work.ipbus_slave_ipbScope
--        port map(
--            sysclk40 => sysclk40,
--            sysclk160 => sysclk160,
--            ipbus_in => ipbw(35),
--            ipbus_out => ipbr(35),
            
--            control => ipbScopeControl,
--            probeData => debugDPhi2,
--            rewind => ttcBroadcast
--        );
        
        


end rtl;