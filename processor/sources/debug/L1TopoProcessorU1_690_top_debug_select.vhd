----------------------------------------------------------------------------------
-- Company: Johannes Gutenberg-Universitaet Mainz
-- Engineer: Christian Kahra
-- 
-- Create Date: 14.07.2014 16:06:32
-- Design Name: L1TopoProcessor
-- Module Name: top_L1TopoProcessor - Behavioral
-- Project Name: Atlas Level-1 Topological Processor
-- Target Devices: 
-- Tool Versions: Vivado 14.2
-- Description: top module
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.l1topo_package.all;
use work.L1TopoDataTypes.all;
use work.L1TopoFunctions.all;
use work.L1TopoGTConfiguration.all;
use work.rod_l1_topo_types_const.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity top_L1TopoProcessor_U1_debug_select is
    generic(
        LINES_NUMBER      : integer := NUMBER_OF_ROS_OUTPUT_BUSES;
        SIMULATION        : boolean := false
  );
    Port ( 
        GCK1_P, GCK1_N: in std_logic;
        GCK2_P, GCK2_N: in std_logic;
        
        MGT2_CLK_P, MGT2_CLK_N: in std_logic_vector(11 downto 1);
        MGT4_CLK_P, MGT4_CLK_N: in std_logic_vector(11 downto 1);
        
        RxP : in  std_logic_vector(MGTHigh   downto MGTLow);
        RxN : in  std_logic_vector(MGTHigh   downto MGTLow);
        TxP : out std_logic_vector(TxMGTHigh downto TxMGTLow);
        TxN : out std_logic_vector(TxMGTHigh downto TxMGTLow);
        
        
--        MgtRefClk0P,MgtRefClk0N: in std_logic_vector(QuadMax downto QuadLow);--usually 160.32 MHz                      
--        MgtRefClk1P,MgtRefClk1N: in std_logic_vector(QuadMax downto QuadLow);--usually 160.32 MHz

--        OPTO8_P, OPTO8_N: in std_logic_vector(11 downto 0);
--        OPTO9_P, OPTO9_N: in std_logic_vector(11 downto 0);
--        OPTO10_P, OPTO10_N: in std_logic_vector(11 downto 0);
--        OPTO12_P, OPTO12_N: in std_logic_vector(11 downto 0);
--        OPTO13_P, OPTO13_N: in std_logic_vector(11 downto 0);
--        OPTO14_P, OPTO14_N: in std_logic_vector(11 downto 0);
--        OPTO11_P_11, OPTO11_N_11: in std_logic;
--        OPTO11_P_10, OPTO11_N_10: in std_logic;
--        OPTO11_P_6, OPTO11_N_6: in std_logic;
--        OPTO11_P_8, OPTO11_N_8: in std_logic;
--        OPTO11_P_1, OPTO11_N_1: in std_logic;
--        OPTO11_P_2, OPTO11_N_2: in std_logic;
--        OPTO11_P_4, OPTO11_N_4: in std_logic;
--        OPTO11_P_0, OPTO11_N_0: in std_logic;

--        OPTO_T2_P, OPTO_T2_N: out std_logic_vector(11 downto 8);
        
        
        CTRLBUS_IN_P,  CTRLBUS_IN_N:  in  std_logic_vector( 6 downto 0);
        CTRLBUS_OUT_P, CTRLBUS_OUT_N: out std_logic_vector(20 downto 9);
        
        
        EXT_V7_P, EXT_V7_N: out std_logic_vector(17 downto 0)
        
--        MMCX_U56: out std_logic
--        MMCX_U57: out std_logic
    
        
    );
end top_L1TopoProcessor_U1_debug_select;

architecture Behavioral of top_L1TopoProcessor_U1_debug_select is


    constant ProcessorID:std_logic_vector(31 downto 0) := X"00000001";
    constant FirmwareVersion: std_logic_vector(31 downto 0) := X"20150413";
    



    signal gck1:                std_logic;
    signal gck2, gck2_bufg:     std_logic;
    signal sysclk40:            std_logic;
    signal sysclk80:            std_logic;
    signal sysclk160:           std_logic;
    signal sysclk320:           std_logic;
    signal sysclk400:           std_logic;
    signal clockBus :           std_logic_vector(3 downto 0);
    signal rodclk400_io:   std_logic;
    signal rodclk400_r:    std_logic;
    signal rodclk80:       std_logic;
    signal sysclk_pll_locked:   std_logic;

    signal crystalclk62_5:          std_logic;
    signal crystalclk200:           std_logic;
    signal crystalclk_pll_locked:   std_logic;

    signal idelayctrlReset:         std_logic;

    signal xadc_control: std_logic_vector(25  downto 0);
    signal xadc_status:  std_logic_vector(22  downto 0);


--    signal MgtRefClk0P, MgtRefClk0N: std_logic_vector(QuadHigh downto QuadLow);
	signal MgtRefClk1P, MgtRefClk1N: std_logic_vector(QuadHigh downto QuadLow);


--    signal RxP, RxN: std_logic_vector(MGTHigh downto MGTLow);
--    signal TxP, TxN: std_logic_vector(TXMGTHigh downto TXMGTLow);


    signal ipbusBridgeBusIn,  ipbusBridgeBusIn_p,  ipbusBridgeBusIn_n : std_logic_vector(4 downto 0);
    signal ipbusBridgeBusOut, ipbusBridgeBusOut_p, ipbusBridgeBusOut_n: std_logic_vector(2 downto 0);
    
    signal ttcBridge, ttcBridge_p, ttcBridge_n: std_logic;
    signal ttcBridge_control: std_logic_vector( 5 downto 0);
    signal ttcBridge_status : std_logic_vector(14 downto 0);
    signal ttcBroadcast, ttcL1Accept, ttcBunchCounterReset: std_logic;
     
     --ROD INTERFACE
    signal ROD_DATA_LINES_P : std_logic_vector(LINES_NUMBER-1 downto 0); 
    signal ROD_DATA_LINES_N : std_logic_vector(LINES_NUMBER-1 downto 0);
    
    signal BUSY_TO_KINTEX_P : std_logic;
    signal BUSY_TO_KINTEX_N : std_logic;
    
    signal L1_ACCEPTED_IN_P : std_logic;
    signal L1_ACCEPTED_IN_N : std_logic;
    
    signal rod_reset: std_logic := '0';

    signal l1_accepted_in: std_logic := '0';
    signal l0_busy : std_logic := '0';
       
    signal number_of_slices : slice_parameters_array_u;
    signal lvl0_offset      : slice_parameters_array_u;
 
    signal data_for_ros_roi_bus, data_for_ros_roi_bus_synch : in_data_array := (others => (others => '1'));  --dummy cntr for tests
    signal data_for_ros_roi_bus_algo : std_logic_vector(127 downto 0);
    signal out_data              : std_logic_vector(OUTPUT_DATA_WIDTH-1 downto 0)          := (others => '0');
    signal data_valid_out        : std_logic_vector(NUMBER_OF_ROS_OUTPUT_BUSES-1 downto 0) := (others => '0');
    signal special_character_out : std_logic_vector(NUMBER_OF_ROS_OUTPUT_BUSES-1 downto 0) := (others => '0');
    signal global_reset_cnt      : unsigned(15 downto 0)                                   := (others => '0');
    signal send_on_crc, crc_err  : std_logic_vector(NUMBER_OF_ROS_ROI_INPUT_BUSES-1 downto 0);
    
--    signal rod_rw_register : rod_rw_registers_array;
    signal rod_control_register: rod_control_registers_array := (others => (others => '0'));
    signal rod_status_register: rod_status_registers_array;
    signal l1_accepted_in_pulse, l1_accepted_in_synch_a, l1_accepted_in_synch_b : std_logic := '0';

    
--    signal outOfBand, outOfBand_reg, outOfBand_p, outOfBand_n: std_logic_vector(4 downto 0);

--    signal results: std_logic_vector(31 downto 0);
--    signal ctp, ctp_p, ctp_n: std_logic_vector(15 downto 0);


--    signal receivedData128BitFromDeserialisers_i: arraySLV128(MGTHigh downto MGTLow);
    signal receivedData128BitFromDeserialisers  : arraySLV128(79 downto 0);
    signal muxedData128BitToAlgorithms, muxedData128BitToAlgorithms_reg, muxedData128BitToAlgorithms_reg_shifted: arraySLV128(79 downto 0);
        

    signal QuadControl     : arraySLV32(QuadHigh downto QuadLow);
    signal QuadStatus      : arraySLV32(QuadHigh downto QuadLow);
    signal ChannelControl  : arraySLV32(79 downto 0);
	signal ChannelStatus   : arraySLV32(79 downto 0);
	signal DataShift40MHz  : arraySLV3(MGTHigh downto MGTLow);


    signal CharIsK128:  arraySLV16(MGTHigh downto MGTLow);
    signal CRCError:    arraySL(MGTHigh downto MGTLow);
    signal CRCError128: arraySLV4(11 downto 0);
    signal debugCRC:    arraySLV256(11 downto 0);

--    signal spyData36BitFromMGTs:  arraySLV36(MGTHigh downto MGTLow);
--    signal enablePlaybackOfMGTs:  std_logic;
--    signal playbackData36BitFakingMGTs:  arraySLV36(MGTHigh downto MGTLow);
     
    
    
    
    signal enablePlaybackOfDeserialisers:  std_logic;
    signal enablePlaybackOfDeserialisers_reg:  std_logic_vector(79 downto 0);
    signal playbackData128BitFakingDeserialisers:  arraySLV128(79 downto 0);
    
    
    signal SortParameters: ParameterSpace(NumberOfSortAlgorithms - 1 downto 0);
    signal AlgoParameters: ParameterSpace(NumberOfAlgorithms - 1 downto 0);
    
    
    --algo ports                            
    signal emTOBs      : ClusterArray(InputWidthEM  - 1 downto 0);
    signal tauTOBs     : ClusterArray(InputWidthTau - 1 downto 0);
    signal jetTOBs     : JetArray    (InputWidthJet - 1 downto 0);
    signal muTOBs      : MuonArray   (InputWidthMU  - 1 downto 0);
    signal metTOB_up   : MetTOB;
    signal metTOB_down : MetTOB;
    signal metTOB_sum  : MetTOB;
    signal metTOBs     : MetArray    (InputWidthMET - 1 downto 0);
    
    signal AlgoResults:  std_logic_vector(NumberOfResultBits - 1 downto 0);
    signal AlgoOverflow: std_logic_vector(NumberOfResultBits - 1 downto 0);

    signal AlgoResults32bit, AlgoOverflow32bit : std_logic_vector(31 downto 0):=(others => '0');
    signal muxedAlgoOutput, muxedAlgoOutputCTP, muxedAlgoOutput_reg: std_logic_vector(NumberOfOutputBits-1 downto 0);
    
    signal spyData32BitFromAlgos: std_logic_vector(NumberOfOutputBits-1 downto 0);
    signal enablePlaybackOfAlgos: std_logic;
    signal playbackData32BitFakingAlgos: std_logic_vector(NumberOfOutputBits-1 downto 0);
    
    signal ctpOutputControl: std_logic_vector(1 downto 0);
    signal ctpOutputMask: std_logic_vector(31 downto 0);
    
    signal ctp_counter: std_logic_vector(4 downto 0);
    signal ctp_runningOne: std_logic_vector(31 downto 0);
    
    signal ctp_data: std_logic_vector(31 downto 0); 
    signal ctp_data_ddr, ctp_data_p, ctp_data_n: std_logic_vector(15 downto 0);
    
    signal ctp_parity0, ctp_parity1: std_logic_vector(16 downto 0);
    
    signal ctp_clk_ddr, ctp_clk_p, ctp_clk_n: std_logic;
    signal ctp_parity_ddr, ctp_parity_p, ctp_parity_n: std_logic;
    
    signal DebugOutput : BitArray128(256 - 1 downto 0);
--	signal AlgoDebug : BitArray128(256 - 1 downto 0);
    
    
    
    signal rxoutclk, rxoutclk_bufg: std_logic;
    
    
    
    
--    signal debugTTCBridge: std_logic_vector(17 downto 0);
--    attribute dont_touch of debugTTCBridge: signal is "true";
    
--    signal debugIPBusBridge: std_logic_vector(269 downto 0);
    
    
--    signal debugGenericJetArray: std_logic_vector(127 downto 0);
--    signal debugGenericClusterArray: std_logic_vector(127 downto 0);
--    signal debugJetArray: std_logic_vector(127 downto 0);
--    signal debugClusterArray: std_logic_vector(127 downto 0);
--    signal debugDPhi: std_logic_vector(127 downto 0 );
--    signal debugDPhi2: std_logic_vector(127 downto 0);
    
--    signal debugPlaybackSpy128: std_logic_vector(432 downto 0);
--    attribute dont_touch of debugPlaybackSpy128: signal is "true";
    
--    signal debugPlaybackSpy32: std_logic_vector(176 downto 0);
--    attribute dont_touch of debugPlaybackSpy32: signal is "true";

    attribute IODELAY_GROUP : string;
	attribute IODELAY_GROUP of BANK14_IDELAYCTRL : label is "bank14_iodelaygroup";
	

begin

--    rod_rw_register(1) <= x"14101317";        -- date - yymmddhh
    rod_status_register(0) <= x"15020915";        -- date - yymmddhh



--------------------------------------------------------
--input mapping
--------------------------------------------------------



    
    MgtRefClk1P(0)  <= MGT2_CLK_P(1);
    MgtRefClk1N(0)  <= MGT2_CLK_N(1);
    MgtRefClk1P(1)  <= MGT4_CLK_P(1);
    MgtRefClk1N(1)  <= MGT4_CLK_N(1);
    MgtRefClk1P(2)  <= MGT2_CLK_P(2);
    MgtRefClk1N(2)  <= MGT2_CLK_N(2);
    MgtRefClk1P(3)  <= MGT4_CLK_P(2);
    MgtRefClk1N(3)  <= MGT4_CLK_N(2);
    MgtRefClk1P(4)  <= MGT2_CLK_P(3);
    MgtRefClk1N(4)  <= MGT2_CLK_N(3);
    MgtRefClk1P(5)  <= MGT4_CLK_P(3);
    MgtRefClk1N(5)  <= MGT4_CLK_N(3);
    MgtRefClk1P(6)  <= MGT2_CLK_P(4);
    MgtRefClk1N(6)  <= MGT2_CLK_N(4);
    MgtRefClk1P(7)  <= MGT4_CLK_P(4);
    MgtRefClk1N(7)  <= MGT4_CLK_N(4);
    MgtRefClk1P(8)  <= MGT2_CLK_P(5);
    MgtRefClk1N(8)  <= MGT2_CLK_N(5);
    MgtRefClk1P(9)  <= MGT4_CLK_P(5);
    MgtRefClk1N(9)  <= MGT4_CLK_N(5);
    MgtRefClk1P(10) <= MGT2_CLK_P(7);
    MgtRefClk1N(10) <= MGT2_CLK_N(7);
    MgtRefClk1P(11) <= MGT4_CLK_P(7);
    MgtRefClk1N(11) <= MGT4_CLK_N(7);
    MgtRefClk1P(12) <= MGT2_CLK_P(8);
    MgtRefClk1N(12) <= MGT2_CLK_N(8);
    MgtRefClk1P(13) <= MGT4_CLK_P(8);
    MgtRefClk1N(13) <= MGT4_CLK_N(8);
    MgtRefClk1P(14) <= MGT2_CLK_P(9);
    MgtRefClk1N(14) <= MGT2_CLK_N(9);
    MgtRefClk1P(15) <= MGT4_CLK_P(9);
    MgtRefClk1N(15) <= MGT4_CLK_N(9);
    MgtRefClk1P(16) <= MGT2_CLK_P(10);
    MgtRefClk1N(16) <= MGT2_CLK_N(10);
    MgtRefClk1P(17) <= MGT4_CLK_P(10);
    MgtRefClk1N(17) <= MGT4_CLK_N(10);
    MgtRefClk1P(18) <= MGT2_CLK_P(11);
    MgtRefClk1N(18) <= MGT2_CLK_N(11);
    MgtRefClk1P(19) <= MGT4_CLK_P(11);
    MgtRefClk1N(19) <= MGT4_CLK_N(11);
    
    

--RXMGTMap_GEN: for i in 11 downto 0 generate begin
--    RxP(     11-i) <= OPTO8_P(i);
--    RxN(     11-i) <= OPTO8_N(i);
--    RxP(12 + 11-i) <= OPTO9_P(i);
--    RxN(12 + 11-i) <= OPTO9_N(i);
--    RxP(24 + 11-i) <= OPTO10_P(i);
--    RxN(24 + 11-i) <= OPTO10_N(i);
--    RxP(40 + 11-i) <= OPTO12_P(i);
--    RxN(40 + 11-i) <= OPTO12_N(i);
--    RxP(52 + 11-i) <= OPTO13_P(i);
--    RxN(52 + 11-i) <= OPTO13_N(i);
--    RxP(64 + 11-i) <= OPTO14_P(i);
--    RxN(64 + 11-i) <= OPTO14_N(i);  
--end generate;


--    RxP(36) <= OPTO11_P_11;
--    RxN(36) <= OPTO11_N_11;
--    RxP(37) <= OPTO11_P_10;
--    RxN(37) <= OPTO11_N_10;
--    RxP(38) <= OPTO11_P_6;
--    RxN(38) <= OPTO11_N_6;
--    RxP(39) <= OPTO11_P_8;
--    RxN(39) <= OPTO11_N_8;
--    RxP(76) <= OPTO11_P_1;
--    RxN(76) <= OPTO11_N_1;
--    RxP(77) <= OPTO11_P_2;
--    RxN(77) <= OPTO11_N_2;
--    RxP(78) <= OPTO11_P_4;
--    RxN(78) <= OPTO11_N_4;
--    RxP(79) <= OPTO11_P_0;
--    RxN(79) <= OPTO11_N_0;
    
    
    

    ipbusBridgeBusIn_p(0)  <= CTRLBUS_IN_P(0);
	ipbusBridgeBusIn_n(0)  <= CTRLBUS_IN_N(0);
	ipbusBridgeBusIn_p(1)  <= CTRLBUS_IN_P(1);
	ipbusBridgeBusIn_n(1)  <= CTRLBUS_IN_N(1);
	ipbusBridgeBusIn_p(2)  <= CTRLBUS_IN_P(2);
	ipbusBridgeBusIn_n(2)  <= CTRLBUS_IN_N(2);
	ipbusBridgeBusIn_p(3)  <= CTRLBUS_IN_P(3);
    ipbusBridgeBusIn_n(3)  <= CTRLBUS_IN_N(3);
    ipbusBridgeBusIn_p(4)  <= CTRLBUS_IN_P(4);
    ipbusBridgeBusIn_n(4)  <= CTRLBUS_IN_N(4);


    ttcBridge_p <= CTRLBUS_IN_P(5);
    ttcBridge_n <= CTRLBUS_IN_N(5);


    


--------------------------------------------------------
--input buffer
--------------------------------------------------------


GCK1_IBUFGDS: IBUFGDS
	port map(
		I  => GCK1_P,
		IB => GCK1_N,
		O  => gck1
);


GCK2_IBUFGDS: IBUFGDS
	port map(
		I  => GCK2_P,
		IB => GCK2_N,
		O  => gck2
);

GCK2_GBUF: BUFG port map(I => gck2, O => gck2_bufg);


IPBUSBRIDGE_IN_IBUFDS_GEN: for i in 4 downto 0 generate
	IPBUSBRIDGE_IN_IBUFDS: IBUFDS
		port map(
			I  => ipbusBridgeBusIn_p(i),
			IB => ipbusBridgeBusIn_n(i),
			O  => ipbusBridgeBusIn(i)
		);
		
		
--	OOB_IBUFDS: IBUFDS
--	   port map(
--	       I  => outOfBand_p(i),
--	       IB => outOfBand_n(i),
--	       O  => outOfBand(i)
--	   );	
		
	end generate;




TTC_CTRLBUS_IBUFDS: IBUFDS
	port map(
		I     => ttcBridge_p,
		IB    => ttcBridge_n,
		O     => ttcBridge
	);



BANK14_IDELAYCTRL: IDELAYCTRL
	 port map (
		  REFCLK => crystalclk200,
		  RST    => idelayctrlReset,
		  RDY    => open
	  );



--sub modules

--------------------------------------------------------
-- processor infrastructure
--------------------------------------------------------

INFR: entity work.infrastructure
    port map(
        gck1	                => gck1,
        gck2_bufg               => gck2_bufg,
        sysclk40                => sysclk40,
        sysclk80                => sysclk80,
        sysclk160               => sysclk160,
        sysclk320               => sysclk320,
        sysclk400               => sysclk400,
        rodclk400_io            => rodclk400_io,
        rodclk400_r             => rodclk400_r,
        rodclk80                => rodclk80,
        sysclk_pll_locked       => sysclk_pll_locked,
        crystalclk62_5          => crystalclk62_5,
        crystalclk200           => crystalclk200,
        crystalclk_pll_locked   => crystalclk_pll_locked,
        idelayctrlReset         => idelayctrlReset,
        xadc_control            => xadc_control,
        xadc_status             => xadc_status,
        ttcBridge               => ttcBridge,
        ttcBridge_control       => ttcBridge_control,
        ttcBridge_status        => ttcBridge_status,
        ttcL1Accept             => ttcL1Accept,
        ttcBroadcast            => ttcBroadcast,
        ttcBunchCounterReset    => ttcBunchCounterReset
    );



    clockBus(0) <= sysclk40;--40MHz
	clockBus(1) <= sysclk80;--80MHz
	clockBus(2) <= sysclk160;--160MHz
	clockBus(3) <= sysclk320;--320MHz




--------------------------------------------------------
-- ipbus
--------------------------------------------------------


ipb: entity work.ipbusModule_debug
    port map(
        sysclk40 => sysclk40,
        sysclk80 => sysclk80,
        sysclk160 => sysclk160,
        sysclk400 => sysclk400,
        
        sysclk_pll_locked => sysclk_pll_locked,
        
        
        ipbBridgeBusIn => ipbusBridgeBusIn,
        ipbBridgeBusOut => ipbusBridgeBusOut,
        
        -- module interfacess
        
        xadc_control => xadc_control,
        xadc_status  => xadc_status,
        
        ttcBridge_control => ttcBridge_control,
        ttcBridge_status => ttcBridge_status,
        ttcBroadcast => ttcBroadcast,

        rod_control_register => rod_control_register,
        rod_status_register  => rod_status_register,


        -- Real-Time path configuration

        QuadControl => QuadControl,
        QuadStatus  => QuadStatus,
        ChannelControl => ChannelControl,
        ChannelStatus => ChannelStatus,
        DataShift40MHz => DataShift40MHz,
        
        
        -- Real-Time path data
        
--        spyData36BitFromMGTs => spyData36BitFromMGTs,
--        enablePlaybackOfMGTs => enablePlaybackOfMGTs,
--        playbackData36BitFakingMGTs => playbackData36BitFakingMGTs,
        
        spyData128BitFromDeserialisers => receivedData128BitFromDeserialisers,
        enablePlaybackOfDeserialisers => enablePlaybackOfDeserialisers,
        playbackData128BitFakingDeserialisers => playbackData128BitFakingDeserialisers,
        
        
        SortParameters => SortParameters,
        AlgoParameters => AlgoParameters,
        
        spyData32BitFromAlgos => spyData32BitFromAlgos,
        enablePlaybackOfAlgos => enablePlaybackOfAlgos,
        playbackData32BitFakingAlgos => playbackData32BitFakingAlgos,
        
        ctpOutputControl => ctpOutputControl,
        ctpOutputMask => ctpOutputMask,
        
        -- MISC
        
        ProcessorID => ProcessorID, 
        FirmwareVersion => FirmwareVersion,
        
        -- DEBUG
        
		DebugOutput => DebugOutput,
--		AlgoDebug => AlgoDebug,
		debugCRC => debugCRC
        
--        ipbBridgeControl => ipbBridgeControl,
--        ipbBridgeStatus => ipbBridgeStatus,

        
--        debugIPBusBridge => debugIPBusBridge(239 downto 0)
--        debugPlaybackSpy128 => debugPlaybackSpy128,
--        debugPlaybackSpy32 => debugPlaybackSpy32
        
        
--        debugJetArray => debugJetArray,
--        debugClusterArray => debugClusterArray,
--        debugGenericJetArray => debugGenericJetArray,
--        debugGenericClusterArray => debugGenericClusterArray,
--        debugDPhi => debugDPhi
--        debugDPhi2 => debugDPhi2
        
    );



--------------------------------------------------------
-- data transmission/reception
--------------------------------------------------------




-------------------------------------------------------------------------------
-- start of comment of GTH
-------------------------------------------------------------------------------
GT_Inout_I: entity work.GT_Inout
    Port map
	(
	--physical connection to FPGA pins
	RxP => RxP,
	RxN => RxN,
	TxP => TxP,
	TxN => TxN,
	MgtRefClk1P => MgtRefClk1P,
	MgtRefClk1N => MgtRefClk1N,

	--internal FPGA clock signals after MMCM
	UsrClkIn => sysclk320,
	UsrClk2In => sysclk320,
	SYSCLK40 => sysclk40,
	sysclk80 => sysclk80,
	clockBus => clockBus,
	sysclk_pll_locked => sysclk_pll_locked,
--	crystalclk62_5 => crystalclk62_5,
--	crystalclk_pll_locked => crystalclk_pll_locked,
	

	--IP Bus/VIO related signals
	
    QuadControl => QuadControl,
    QuadStatus  => QuadStatus,
	ChannelControl => ChannelControl(MGTHigh downto MGTLow),
	ChannelStatus => ChannelStatus(MGTHigh downto MGTLow),
	DataShift40MHz => DataShift40MHz,
	
	--Data for algorithms (and CRC)--> this are the only signals needed for algorithms
	Data128 => receivedData128BitFromDeserialisers(MGTHigh downto MGTLow),
	
	--Debug ports, leave open when you do not want to debug and want to save some logic resources
	Charisk128 => Charisk128,--
--	GtRxData => GtRxData,--
--	GtRxCharIsK => GtRxCharIsK,--
--	Data128AndChariK128_Just_Done => Data128AndChariK128_Just_Done,--
	CrcError => CrcError--
--	CRC_Just_Done => CRC_Just_Done,--
	
--	spyData36BitFromMGTs => spyData36BitFromMGTs,
--    enablePlaybackOfMGTs => enablePlaybackOfMGTs,
--    playbackData36BitFakingMGTs => playbackData36BitFakingMGTs
	
--	rxoutclk => rxoutclk
	
	);

-------------------------------------------------------------------------------
-- end of comment of GTH
-------------------------------------------------------------------------------


--DEBUGCRC_GEN1: for i in 15 downto 0 generate begin

--process(sysclk40) begin
--    if rising_edge(sysclk40) then
--        debugCRC(i)(127 downto   0) <= receivedData128BitFromDeserialisers(56+i);
--        debugCRC(i)(143 downto 128) <= CharIsK128(56+i);
--        debugCRC(i)(153 downto 144) <= jetTOBs(4*i+0).Et2;
--        debugCRC(i)(158 downto 154) <= jetTOBs(4*i+0).Eta;
--        debugCRC(i)(163 downto 159) <= jetTOBs(4*i+0).Phi;
--        debugCRC(i)(172 downto 164) <= jetTOBs(4*i+0).Et1;
        
--        debugCRC(i)(182 downto 173) <= jetTOBs(4*i+1).Et2;
--        debugCRC(i)(187 downto 183) <= jetTOBs(4*i+1).Eta;
--        debugCRC(i)(192 downto 188) <= jetTOBs(4*i+1).Phi;
--        debugCRC(i)(201 downto 193) <= jetTOBs(4*i+1).Et1;
        
--        debugCRC(i)(211 downto 202) <= jetTOBs(4*i+2).Et2;
--        debugCRC(i)(216 downto 212) <= jetTOBs(4*i+2).Eta;
--        debugCRC(i)(221 downto 217) <= jetTOBs(4*i+2).Phi;
--        debugCRC(i)(230 downto 222) <= jetTOBs(4*i+2).Et1;
        
--        debugCRC(i)(240 downto 231) <= jetTOBs(4*i+3).Et2;
--        debugCRC(i)(245 downto 241) <= jetTOBs(4*i+3).Eta;
--        debugCRC(i)(250 downto 246) <= jetTOBs(4*i+3).Phi;
--        debugCRC(i)(255 downto 251) <= jetTOBs(4*i+3).Et1(4 downto 0);
--    end if;
--end process;

--end generate;



--DEBUGCRC_GEN2: for i in 11 downto 8 generate begin

--process(sysclk40) begin
--    if rising_edge(sysclk40) then
--        debugCRC(i)(127 downto   0) <= receivedData128BitFromDeserialisers(68+i-8);
--        debugCRC(i)(143 downto 128) <= CharIsK128(68+i-8);
--        debugCRC(i)(153 downto 144) <= jetTOBs(4*i+0).Et2;
--        debugCRC(i)(158 downto 154) <= jetTOBs(4*i+0).Eta;
--        debugCRC(i)(163 downto 159) <= jetTOBs(4*i+0).Phi;
--        debugCRC(i)(172 downto 164) <= jetTOBs(4*i+0).Et1;
        
--        debugCRC(i)(182 downto 173) <= jetTOBs(4*i+1).Et2;
--        debugCRC(i)(187 downto 183) <= jetTOBs(4*i+1).Eta;
--        debugCRC(i)(192 downto 188) <= jetTOBs(4*i+1).Phi;
--        debugCRC(i)(201 downto 193) <= jetTOBs(4*i+1).Et1;
        
--        debugCRC(i)(211 downto 202) <= jetTOBs(4*i+2).Et2;
--        debugCRC(i)(216 downto 212) <= jetTOBs(4*i+2).Eta;
--        debugCRC(i)(221 downto 217) <= jetTOBs(4*i+2).Phi;
--        debugCRC(i)(230 downto 222) <= jetTOBs(4*i+2).Et1;
        
--        debugCRC(i)(240 downto 231) <= jetTOBs(4*i+3).Et2;
--        debugCRC(i)(245 downto 241) <= jetTOBs(4*i+3).Eta;
--        debugCRC(i)(250 downto 246) <= jetTOBs(4*i+3).Phi;
--        debugCRC(i)(255 downto 251) <= jetTOBs(4*i+3).Et1(4 downto 0);
--    end if;
--end process;

--end generate;


    fillUnusedLowerMGTChannels: for ch in MGTLow-1 downto 0 generate begin
        receivedData128BitFromDeserialisers(ch) <= (127 downto 0 => '0');
        ChannelStatus(ch) <= (31 downto 0 => '0');
    end generate;
    fillUnusedHigherMGTChannels: for ch in 79 downto MGTHigh+1 generate begin
        receivedData128BitFromDeserialisers(ch) <= (127 downto 0 => '0');
        ChannelStatus(ch) <= (31 downto 0 => '0');
    end generate;


    enablePlaybackOfDeserialisers_GEN: for ch in 79 downto 0 generate begin
    
        process(sysclk40) begin
            if rising_edge(sysclk40) then
                enablePlaybackOfDeserialisers_reg(ch) <= enablePlaybackOfDeserialisers; 
            end if;
        end process;
    
        muxedData128BitToAlgorithms(ch) <= receivedData128BitFromDeserialisers(ch) when enablePlaybackOfDeserialisers_reg(ch)='0' and ChannelControl(ch)(26)='0' else playbackData128BitFakingDeserialisers(ch);
    end generate;



    process(sysclk40) begin
        if rising_edge(sysclk40) then
            muxedData128BitToAlgorithms_reg <= muxedData128BitToAlgorithms;
        end if;
    end process;




--------------------------------------------------------
-- fiber mapping
--------------------------------------------------------

    ----make data ready to be used by alg.
    
    ----JET_TOBs
    -- Avago(Quad): AV2(113, 114, 115), AV3(116, 117, 118) 
    -- AV4(119), AV6(213, 214, 215), AV7(216, 217, 218), AV4(219)
    --JET_GEN: for MGT in 24-1 downto 0 generate begin
    
    
    EM_GEN_5_0: for MGT in 5 downto 0 generate
        constant emMGT: natural := MGT-0;
    begin
        emTOBs( 5*emMGT + 0 ) <= DeserialisedData_to_ClusterTOB(muxedData128BitToAlgorithms(MGT)(127 downto 105), ChannelControl(MGT)(25 downto 24)) when ChannelControl(MGT)(22)='0' else emptyClusterTOB;
        emTOBs( 5*emMGT + 1 ) <= DeserialisedData_to_ClusterTOB(muxedData128BitToAlgorithms(MGT)(104 downto 82), ChannelControl(MGT)(25 downto 24)) when ChannelControl(MGT)(22)='0' else emptyClusterTOB;
        emTOBs( 5*emMGT + 2 ) <= DeserialisedData_to_ClusterTOB(muxedData128BitToAlgorithms(MGT)(81 downto 59), ChannelControl(MGT)(25 downto 24)) when ChannelControl(MGT)(22)='0' else emptyClusterTOB;
        emTOBs( 5*emMGT + 3 ) <= DeserialisedData_to_ClusterTOB(muxedData128BitToAlgorithms(MGT)(58 downto 36), ChannelControl(MGT)(25 downto 24)) when ChannelControl(MGT)(22)='0' else emptyClusterTOB;
        emTOBs( 5*emMGT + 4 ) <= DeserialisedData_to_ClusterTOB(muxedData128BitToAlgorithms(MGT)(35 downto 13), ChannelControl(MGT)(25 downto 24)) when ChannelControl(MGT)(22)='0' else emptyClusterTOB;
    end generate;
    
    TAU_GEN_11_6: for MGT in 11 downto 6 generate
        constant tauMGT: natural := MGT-6;
    begin
        tauTOBs( 5*tauMGT + 0 ) <= DeserialisedData_to_ClusterTOB(muxedData128BitToAlgorithms(MGT)(127 downto 105), ChannelControl(MGT)(25 downto 24)) when ChannelControl(MGT)(22)='0' else emptyClusterTOB;
        tauTOBs( 5*tauMGT + 1 ) <= DeserialisedData_to_ClusterTOB(muxedData128BitToAlgorithms(MGT)(104 downto 82), ChannelControl(MGT)(25 downto 24)) when ChannelControl(MGT)(22)='0' else emptyClusterTOB;
        tauTOBs( 5*tauMGT + 2 ) <= DeserialisedData_to_ClusterTOB(muxedData128BitToAlgorithms(MGT)(81 downto 59), ChannelControl(MGT)(25 downto 24)) when ChannelControl(MGT)(22)='0' else emptyClusterTOB;
        tauTOBs( 5*tauMGT + 3 ) <= DeserialisedData_to_ClusterTOB(muxedData128BitToAlgorithms(MGT)(58 downto 36), ChannelControl(MGT)(25 downto 24)) when ChannelControl(MGT)(22)='0' else emptyClusterTOB;
        tauTOBs( 5*tauMGT + 4 ) <= DeserialisedData_to_ClusterTOB(muxedData128BitToAlgorithms(MGT)(35 downto 13), ChannelControl(MGT)(25 downto 24)) when ChannelControl(MGT)(22)='0' else emptyClusterTOB;
    end generate;
    
    EM_GEN_17_12: for MGT in 17 downto 12 generate
        constant emMGT: natural := MGT-12+6;
    begin
        emTOBs( 5*emMGT + 0 ) <= DeserialisedData_to_ClusterTOB(muxedData128BitToAlgorithms(MGT)(127 downto 105), ChannelControl(MGT)(25 downto 24)) when ChannelControl(MGT)(22)='0' else emptyClusterTOB;
        emTOBs( 5*emMGT + 1 ) <= DeserialisedData_to_ClusterTOB(muxedData128BitToAlgorithms(MGT)(104 downto 82), ChannelControl(MGT)(25 downto 24)) when ChannelControl(MGT)(22)='0' else emptyClusterTOB;
        emTOBs( 5*emMGT + 2 ) <= DeserialisedData_to_ClusterTOB(muxedData128BitToAlgorithms(MGT)(81 downto 59), ChannelControl(MGT)(25 downto 24)) when ChannelControl(MGT)(22)='0' else emptyClusterTOB;
        emTOBs( 5*emMGT + 3 ) <= DeserialisedData_to_ClusterTOB(muxedData128BitToAlgorithms(MGT)(58 downto 36), ChannelControl(MGT)(25 downto 24)) when ChannelControl(MGT)(22)='0' else emptyClusterTOB;
        emTOBs( 5*emMGT + 4 ) <= DeserialisedData_to_ClusterTOB(muxedData128BitToAlgorithms(MGT)(35 downto 13), ChannelControl(MGT)(25 downto 24)) when ChannelControl(MGT)(22)='0' else emptyClusterTOB;
    end generate;
    
    TAU_GEN_23_18: for MGT in 23 downto 18 generate
        constant tauMGT: natural := MGT-18+6;
    begin
        tauTOBs( 5*tauMGT + 0 ) <= DeserialisedData_to_ClusterTOB(muxedData128BitToAlgorithms(MGT)(127 downto 105), ChannelControl(MGT)(25 downto 24)) when ChannelControl(MGT)(22)='0' else emptyClusterTOB;
        tauTOBs( 5*tauMGT + 1 ) <= DeserialisedData_to_ClusterTOB(muxedData128BitToAlgorithms(MGT)(104 downto 82), ChannelControl(MGT)(25 downto 24)) when ChannelControl(MGT)(22)='0' else emptyClusterTOB;
        tauTOBs( 5*tauMGT + 2 ) <= DeserialisedData_to_ClusterTOB(muxedData128BitToAlgorithms(MGT)(81 downto 59), ChannelControl(MGT)(25 downto 24)) when ChannelControl(MGT)(22)='0' else emptyClusterTOB;
        tauTOBs( 5*tauMGT + 3 ) <= DeserialisedData_to_ClusterTOB(muxedData128BitToAlgorithms(MGT)(58 downto 36), ChannelControl(MGT)(25 downto 24)) when ChannelControl(MGT)(22)='0' else emptyClusterTOB;
        tauTOBs( 5*tauMGT + 4 ) <= DeserialisedData_to_ClusterTOB(muxedData128BitToAlgorithms(MGT)(35 downto 13), ChannelControl(MGT)(25 downto 24)) when ChannelControl(MGT)(22)='0' else emptyClusterTOB;
    end generate;
    
    EM_GEN_29_24: for MGT in 29 downto 24 generate
        constant emMGT: natural := MGT-24+12;
    begin
        emTOBs( 5*emMGT + 0 ) <= DeserialisedData_to_ClusterTOB(muxedData128BitToAlgorithms(MGT)(127 downto 105), ChannelControl(MGT)(25 downto 24)) when ChannelControl(MGT)(22)='0' else emptyClusterTOB;
        emTOBs( 5*emMGT + 1 ) <= DeserialisedData_to_ClusterTOB(muxedData128BitToAlgorithms(MGT)(104 downto 82), ChannelControl(MGT)(25 downto 24)) when ChannelControl(MGT)(22)='0' else emptyClusterTOB;
        emTOBs( 5*emMGT + 2 ) <= DeserialisedData_to_ClusterTOB(muxedData128BitToAlgorithms(MGT)(81 downto 59), ChannelControl(MGT)(25 downto 24)) when ChannelControl(MGT)(22)='0' else emptyClusterTOB;
        emTOBs( 5*emMGT + 3 ) <= DeserialisedData_to_ClusterTOB(muxedData128BitToAlgorithms(MGT)(58 downto 36), ChannelControl(MGT)(25 downto 24)) when ChannelControl(MGT)(22)='0' else emptyClusterTOB;
        emTOBs( 5*emMGT + 4 ) <= DeserialisedData_to_ClusterTOB(muxedData128BitToAlgorithms(MGT)(35 downto 13), ChannelControl(MGT)(25 downto 24)) when ChannelControl(MGT)(22)='0' else emptyClusterTOB;
    end generate;
    
    TAU_GEN_35_30: for MGT in 35 downto 30 generate
        constant tauMGT: natural := MGT-30+12;
    begin
        tauTOBs( 5*tauMGT + 0 ) <= DeserialisedData_to_ClusterTOB(muxedData128BitToAlgorithms(MGT)(127 downto 105), ChannelControl(MGT)(25 downto 24)) when ChannelControl(MGT)(22)='0' else emptyClusterTOB;
        tauTOBs( 5*tauMGT + 1 ) <= DeserialisedData_to_ClusterTOB(muxedData128BitToAlgorithms(MGT)(104 downto 82), ChannelControl(MGT)(25 downto 24)) when ChannelControl(MGT)(22)='0' else emptyClusterTOB;
        tauTOBs( 5*tauMGT + 2 ) <= DeserialisedData_to_ClusterTOB(muxedData128BitToAlgorithms(MGT)(81 downto 59), ChannelControl(MGT)(25 downto 24)) when ChannelControl(MGT)(22)='0' else emptyClusterTOB;
        tauTOBs( 5*tauMGT + 3 ) <= DeserialisedData_to_ClusterTOB(muxedData128BitToAlgorithms(MGT)(58 downto 36), ChannelControl(MGT)(25 downto 24)) when ChannelControl(MGT)(22)='0' else emptyClusterTOB;
        tauTOBs( 5*tauMGT + 4 ) <= DeserialisedData_to_ClusterTOB(muxedData128BitToAlgorithms(MGT)(35 downto 13), ChannelControl(MGT)(25 downto 24)) when ChannelControl(MGT)(22)='0' else emptyClusterTOB;
    end generate;
    
    EM_GEN_45_40: for MGT in 45 downto 40 generate
        constant emMGT: natural := MGT-40+18;
    begin
        emTOBs( 5*emMGT + 0 ) <= DeserialisedData_to_ClusterTOB(muxedData128BitToAlgorithms(MGT)(127 downto 105), ChannelControl(MGT)(25 downto 24)) when ChannelControl(MGT)(22)='0' else emptyClusterTOB;
        emTOBs( 5*emMGT + 1 ) <= DeserialisedData_to_ClusterTOB(muxedData128BitToAlgorithms(MGT)(104 downto 82), ChannelControl(MGT)(25 downto 24)) when ChannelControl(MGT)(22)='0' else emptyClusterTOB;
        emTOBs( 5*emMGT + 2 ) <= DeserialisedData_to_ClusterTOB(muxedData128BitToAlgorithms(MGT)(81 downto 59), ChannelControl(MGT)(25 downto 24)) when ChannelControl(MGT)(22)='0' else emptyClusterTOB;
        emTOBs( 5*emMGT + 3 ) <= DeserialisedData_to_ClusterTOB(muxedData128BitToAlgorithms(MGT)(58 downto 36), ChannelControl(MGT)(25 downto 24)) when ChannelControl(MGT)(22)='0' else emptyClusterTOB;
        emTOBs( 5*emMGT + 4 ) <= DeserialisedData_to_ClusterTOB(muxedData128BitToAlgorithms(MGT)(35 downto 13), ChannelControl(MGT)(25 downto 24)) when ChannelControl(MGT)(22)='0' else emptyClusterTOB;
    end generate;
    
    TAU_GEN_51_46: for MGT in 51 downto 46 generate
        constant tauMGT: natural := MGT-46+18;
    begin
        tauTOBs( 5*tauMGT + 0 ) <= DeserialisedData_to_ClusterTOB(muxedData128BitToAlgorithms(MGT)(127 downto 105), ChannelControl(MGT)(25 downto 24)) when ChannelControl(MGT)(22)='0' else emptyClusterTOB;
        tauTOBs( 5*tauMGT + 1 ) <= DeserialisedData_to_ClusterTOB(muxedData128BitToAlgorithms(MGT)(104 downto 82), ChannelControl(MGT)(25 downto 24)) when ChannelControl(MGT)(22)='0' else emptyClusterTOB;
        tauTOBs( 5*tauMGT + 2 ) <= DeserialisedData_to_ClusterTOB(muxedData128BitToAlgorithms(MGT)(81 downto 59), ChannelControl(MGT)(25 downto 24)) when ChannelControl(MGT)(22)='0' else emptyClusterTOB;
        tauTOBs( 5*tauMGT + 3 ) <= DeserialisedData_to_ClusterTOB(muxedData128BitToAlgorithms(MGT)(58 downto 36), ChannelControl(MGT)(25 downto 24)) when ChannelControl(MGT)(22)='0' else emptyClusterTOB;
        tauTOBs( 5*tauMGT + 4 ) <= DeserialisedData_to_ClusterTOB(muxedData128BitToAlgorithms(MGT)(35 downto 13), ChannelControl(MGT)(25 downto 24)) when ChannelControl(MGT)(22)='0' else emptyClusterTOB;
    end generate;
    
    JET_GEN_59_52: for MGT in 71 downto 56 generate 
        constant jetMGT: natural := MGT-56;
    begin
        jetTOBs( 4*jetMGT + 0 ) <= DeserialisedData_to_JetTOB(muxedData128BitToAlgorithms(MGT)(127 downto 100), ChannelControl(MGT)(24)) when ChannelControl(MGT)(22)='0'else emptyJetTOB;
        jetTOBs( 4*jetMGT + 1 ) <= DeserialisedData_to_JetTOB(muxedData128BitToAlgorithms(MGT)(99 downto 72), ChannelControl(MGT)(24))  when ChannelControl(MGT)(22)='0' else emptyJetTOB;
        jetTOBs( 4*jetMGT + 2 ) <= DeserialisedData_to_JetTOB(muxedData128BitToAlgorithms(MGT)(71 downto 44), ChannelControl(MGT)(24))  when ChannelControl(MGT)(22)='0' else emptyJetTOB;
        jetTOBs( 4*jetMGT + 3 ) <= DeserialisedData_to_JetTOB(muxedData128BitToAlgorithms(MGT)(43 downto 16), ChannelControl(MGT)(24))  when ChannelControl(MGT)(22)='0' else emptyJetTOB;
    end generate;

--63 downto 60
    metTOB_down.Ex               <=  '0' & muxedData128BitToAlgorithms(61)(26  downto 12  ) when ChannelControl(61)(22)='0'else '0' & (26 downto 12 => '0');
    metTOB_down.Ey(15 downto 8)  <=  '0' & muxedData128BitToAlgorithms(61)(78  downto 72  ) when ChannelControl(61)(22)='0'else '0' & (78 downto 72 => '0');  --24
    metTOB_down.Ey( 7 downto 0)  <=  muxedData128BitToAlgorithms(61)(51  downto 44  ) when ChannelControl(61)(22)='0'else (51 downto 44 => '0');                 
    metTOB_down.Et               <=  '0' & muxedData128BitToAlgorithms(61)(110 downto 96  ) when ChannelControl(61)(22)='0'else '0' & (110 downto 96 => '0');
    metTOB_down.Overflow <= muxedData128BitToAlgorithms(61)(27) or muxedData128BitToAlgorithms(61)(79); -- Ex and Ey overflow
             
    metTOB_up.Ex               <=  '0' & muxedData128BitToAlgorithms(63)(26  downto 12  ) when ChannelControl(63)(22)='0'else '0' & (26 downto 12 => '0');
    metTOB_up.Ey(15 downto 8)  <=  '0' & muxedData128BitToAlgorithms(63)(78  downto 72  ) when ChannelControl(63)(22)='0'else '0' & (78 downto 72 => '0');  --24
    metTOB_up.Ey( 7 downto 0)  <=  muxedData128BitToAlgorithms(63)(51  downto 44  ) when ChannelControl(63)(22)='0'else (51 downto 44 => '0');
    metTOB_up.Et               <=  '0' & muxedData128BitToAlgorithms(63)(110 downto 96  ) when ChannelControl(63)(22)='0'else '0' & (110 downto 96 => '0');        
    metTOB_up.Overflow <= muxedData128BitToAlgorithms(63)(27) or muxedData128BitToAlgorithms(63)(79); -- Ex and Ey overflow
    
    metTOB_sum.Ex <= std_logic_vector(signed(metTOB_up.Ex) + signed(metTOB_down.Ex));
    metTOB_sum.Ey <= std_logic_vector(signed(metTOB_up.Ey) + signed(metTOB_down.Ey));
    metTOB_sum.Et <= std_logic_vector(signed(metTOB_up.Et) + signed(metTOB_down.Et));
    metTOB_sum.Overflow <= metTOB_up.Overflow or metTOB_down.Overflow;
    
    metTOBs(0) <= metTOB_sum;
    
    -- calculate overflow
    emTOBs(0).Overflow <= muxedData128BitToAlgorithms(0)(12) or muxedData128BitToAlgorithms(12)(12)
    						or muxedData128BitToAlgorithms(24)(12) or muxedData128BitToAlgorithms(40)(12);
    						
    tauTOBs(0).Overflow <= muxedData128BitToAlgorithms(6)(12) or muxedData128BitToAlgorithms(18)(12)
    						or muxedData128BitToAlgorithms(30)(12) or muxedData128BitToAlgorithms(46)(12);
    					
    jetTOBs(0).Overflow <= muxedData128BitToAlgorithms(56)(15) or muxedData128BitToAlgorithms(64)(15);
    
--    -- calculate overflows EM
--    calc_overflow_em : process (muxedData128BitToAlgorithms)
--    	variable overflow : std_logic;
--    begin
--    	overflow := '0';
--    	for MGT in 5 downto 0 loop
--    		overflow := overflow and muxedData128BitToAlgorithms(MGT)(12);
--    	end loop;
--    	for MGT in 17 downto 12 loop
--    		overflow := overflow and muxedData128BitToAlgorithms(MGT)(12);
--    	end loop;
--    	for MGT in 29 downto 24 loop
--    		overflow := overflow and muxedData128BitToAlgorithms(MGT)(12);
--    	end loop;
--    	for MGT in 45 downto 40 loop
--    		overflow := overflow and muxedData128BitToAlgorithms(MGT)(12);
--    	end loop;
--    	emTOBs(0).Overflow <= overflow;
--    end process;	
--    
--    -- calculate overflows TAU
--    calc_overflow_tau : process (muxedData128BitToAlgorithms)
--    	variable overflow : std_logic;
--    begin
--    	overflow := '0';
--    	for MGT in 11 downto 6 loop
--    		overflow := overflow and muxedData128BitToAlgorithms(MGT)(12);
--    	end loop;
--    	for MGT in 23 downto 18 loop
--    		overflow := overflow and muxedData128BitToAlgorithms(MGT)(12);
--    	end loop;
--    	for MGT in 35 downto 30 loop
--    		overflow := overflow and muxedData128BitToAlgorithms(MGT)(12);
--    	end loop;
--    	for MGT in 51 downto 46 loop
--    		overflow := overflow and muxedData128BitToAlgorithms(MGT)(12);
--    	end loop;
--    	tauTOBs(0).Overflow <= overflow;
--    end process;
--    
--    -- calculate overflows JET
--    calc_overflow_jet : process (muxedData128BitToAlgorithms)
--    	variable overflow : std_logic;
--    begin
--    	overflow := '0';
--    	for MGT in 71 downto 56 loop
--    		overflow := overflow and muxedData128BitToAlgorithms(MGT)(15);
--    	end loop;
--    	jetTOBs(0).Overflow <= overflow;
--    end process;
    
--67 downto 64 empty    
    
--    JET_GEN_75_8: for MGT in 75 downto 68 generate 
--        constant jetMGT: natural := MGT-68+8;
--    begin
--        jetTOBs( 4*jetMGT + 0 ) <= DeserialisedData_to_JetTOB(muxedData128BitToAlgorithms(MGT)(127 downto 100), ChannelControl(MGT)(24)) when ChannelControl(MGT)(22)='0'else emptyJetTOB;
--        jetTOBs( 4*jetMGT + 1 ) <= DeserialisedData_to_JetTOB(muxedData128BitToAlgorithms(MGT)(99 downto 72), ChannelControl(MGT)(24))  when ChannelControl(MGT)(22)='0' else emptyJetTOB;
--        jetTOBs( 4*jetMGT + 2 ) <= DeserialisedData_to_JetTOB(muxedData128BitToAlgorithms(MGT)(71 downto 44), ChannelControl(MGT)(24))  when ChannelControl(MGT)(22)='0' else emptyJetTOB;
--        jetTOBs( 4*jetMGT + 3 ) <= DeserialisedData_to_JetTOB(muxedData128BitToAlgorithms(MGT)(43 downto 16), ChannelControl(MGT)(24))  when ChannelControl(MGT)(22)='0' else emptyJetTOB;
--    end generate;
    
--76 Muon

--78 downto 77 empty

--79 Muon





--    -- AV6(213, 214, 215)
--    --EM_GEN: for MGT in 40-1 downto 28 generate
--    EM_GEN: for MGT in 27 downto 20 generate
--        constant emMGT: natural := MGT-20;
--    begin
--        emTOBs( 5*emMGT + 0 ) <= DeserialisedData_to_ClusterTOB(muxedData128BitToAlgorithms(MGT)(127 downto 105), ChannelControl(MGT)(25 downto 24)) when ChannelControl(MGT)(22)='0' else emptyClusterTOB;
--        emTOBs( 5*emMGT + 1 ) <= DeserialisedData_to_ClusterTOB(muxedData128BitToAlgorithms(MGT)(104 downto 82), ChannelControl(MGT)(25 downto 24)) when ChannelControl(MGT)(22)='0' else emptyClusterTOB;
--        emTOBs( 5*emMGT + 2 ) <= DeserialisedData_to_ClusterTOB(muxedData128BitToAlgorithms(MGT)(81 downto 59), ChannelControl(MGT)(25 downto 24)) when ChannelControl(MGT)(22)='0' else emptyClusterTOB;
--        emTOBs( 5*emMGT + 3 ) <= DeserialisedData_to_ClusterTOB(muxedData128BitToAlgorithms(MGT)(58 downto 36), ChannelControl(MGT)(25 downto 24)) when ChannelControl(MGT)(22)='0' else emptyClusterTOB;
--        emTOBs( 5*emMGT + 4 ) <= DeserialisedData_to_ClusterTOB(muxedData128BitToAlgorithms(MGT)(35 downto 13), ChannelControl(MGT)(25 downto 24)) when ChannelControl(MGT)(22)='0' else emptyClusterTOB;
--    end generate EM_GEN;
    
    
--    -- AV7(216, 217, 218)
--    --TAU_GEN: for MGT in 52-1 downto 40 generate
--    TAU_GEN: for MGT in 35 downto 28 generate
--        constant tauMGT: natural := MGT-28;
--    begin
--        tauTOBs( 5*tauMGT + 0 ) <= DeserialisedData_to_ClusterTOB(muxedData128BitToAlgorithms(MGT)(127 downto 105), ChannelControl(MGT)(25 downto 24)) when ChannelControl(MGT)(22)='0' else emptyClusterTOB;
--        tauTOBs( 5*tauMGT + 1 ) <= DeserialisedData_to_ClusterTOB(muxedData128BitToAlgorithms(MGT)(104 downto 82), ChannelControl(MGT)(25 downto 24)) when ChannelControl(MGT)(22)='0' else emptyClusterTOB;
--        tauTOBs( 5*tauMGT + 2 ) <= DeserialisedData_to_ClusterTOB(muxedData128BitToAlgorithms(MGT)(81 downto 59), ChannelControl(MGT)(25 downto 24)) when ChannelControl(MGT)(22)='0' else emptyClusterTOB;
--        tauTOBs( 5*tauMGT + 3 ) <= DeserialisedData_to_ClusterTOB(muxedData128BitToAlgorithms(MGT)(58 downto 36), ChannelControl(MGT)(25 downto 24)) when ChannelControl(MGT)(22)='0' else emptyClusterTOB;
--        tauTOBs( 5*tauMGT + 4 ) <= DeserialisedData_to_ClusterTOB(muxedData128BitToAlgorithms(MGT)(35 downto 13), ChannelControl(MGT)(25 downto 24)) when ChannelControl(MGT)(22)='0' else emptyClusterTOB;
--    end generate TAU_GEN;
    
    
--    -- AV4(119)
--    --MU_GEN : for MGT in 28-1 downto 24 generate
--    MU_GEN : for MGT in 36-1 downto 32 generate
--        --constant muMGT: natural := MGT-24;
--        constant muMGT: natural := MGT-32;
--    begin
--        muTOBs( 4*muMGT + 0 ) <= DeserialisedData_to_MyonTOB(muxedData128BitToAlgorithms(MGT)(127 downto 120));
--        muTOBs( 4*muMGT + 1 ) <= DeserialisedData_to_MyonTOB(muxedData128BitToAlgorithms(MGT)(119 downto 112));
--        muTOBs( 4*muMGT + 2 ) <= DeserialisedData_to_MyonTOB(muxedData128BitToAlgorithms(MGT)(111 downto 104));
--        muTOBs( 4*muMGT + 3 ) <= DeserialisedData_to_MyonTOB(muxedData128BitToAlgorithms(MGT)(103 downto 96));
--    end generate MU_GEN;
    
    
    
    -- AV4(219)
--    signed_Ey(14 downto 8)<=  muxedData128BitToAlgorithms(15)(78  downto 72  ) when ChannelControl(15)(22)='0'else (78 downto 72 => '0');  --24
--    signed_Ey(7 downto 0) <=  muxedData128BitToAlgorithms(15)(51  downto 44  ) when ChannelControl(15)(22)='0'else (51 downto 44 => '0');                 
--    signed_Ex             <=  muxedData128BitToAlgorithms(15)(26  downto 12  ) when ChannelControl(15)(22)='0'else (26 downto 12 => '0');



--DEBUGJET_GEN: for i in 3 downto 0 generate 
--    constant tobWidth: natural := JetEt1BitWidth + JetEt2BitWidth + JetEtaBitWidth + JetPhiBitWidth;
--    constant e12: natural := JetEt1BitWidth + JetEt2BitWidth;
--    constant e12Eta: natural := e12 + JetEtaBitWidth;
--begin

--    debugJetArray(i*tobWidth + JetEt2BitWidth-1 downto i*tobWidth) <= jetTOBs(i).Et2;
--    debugJetArray(i*tobWidth + e12-1 downto i*tobWidth + JetEt2BitWidth) <= jetTOBs(i).Et1;
--    debugJetArray(i*tobWidth + e12Eta-1 downto i*tobWidth + e12) <= jetTOBs(i).Eta;
--    debugJetArray((i+1)*tobWidth-1 downto i*tobWidth + e12Eta) <= jetTOBs(i).Phi;

--end generate;
    
--debugJetArray(127 downto 116) <= (others => '0');

--DEBUGCLUSTER_GEN: for i in 4 downto 0 generate 
--    constant tobWidth: natural := ClusterEtBitWidth + ClusterIsolBitWidth + ClusterEtaBitWidth + ClusterPhiBitWidth;
--    constant eisol: natural := ClusterEtBitWidth + ClusterIsolBitWidth;
--    constant eisolEta: natural := eisol + ClusterEtaBitWidth;
--begin

--    debugClusterArray(i*tobWidth + ClusterEtBitWidth-1 downto i*tobWidth) <= emTOBs(i).Et;
--    debugClusterArray(i*tobWidth + eisol-1 downto i*tobWidth + ClusterEtBitWidth) <= emTOBs(i).Isol;
--    debugClusterArray(i*tobWidth + eisolEta-1 downto i*tobWidth + eisol) <= emTOBs(i).Eta;
--    debugClusterArray((i+1)*tobWidth-1 downto i*tobWidth + eisolEta) <= emTOBs(i).Phi;

--end generate;

    
--    debugClusterArray(127 downto 125) <= "000";



--------------------------------------------------------
-- algorithms
--------------------------------------------------------
-------------------------------------------------------------------------------
-- start of comment of algo
-------------------------------------------------------------------------------    
algo: entity work.L1TopoAlgorithms_debug_select
	port map(
			ClockBus       => ClockBus(2 downto 0),
			EmTobArray     => emTOBs,
			TauTobArray    => tauTOBs,
			JetTobArray    => jetTOBs,
			MuonTobArray   => muTOBs,
			MetTobArray    => MetTOBs,
			Parameters     => AlgoParameters,
			SortParameters => SortParameters,
			Results        => AlgoResults,
			Overflow       => AlgoOverflow,
			DebugOutput    => DebugOutput
		);



    spyData32BitFromAlgos <= (NumberOfOutputBits-1 downto 2*NumberOfResultBits => '0') & AlgoOverflow & AlgoResults;
  
    muxedAlgoOutput <= (NumberOfOutputBits-1 downto 2*NumberOfResultBits => '0') & AlgoOverflow & AlgoResults when enablePlaybackOfAlgos='0' else playbackData32BitFakingAlgos;

    muxedAlgoOutputCTP <= (NumberOfOutputBits-1 downto NumberOfResultBits => '0') & AlgoResults when enablePlaybackOfAlgos='0' else playbackData32BitFakingAlgos;

-------------------------------------------------------------------------------
-- end of comment of algo
-------------------------------------------------------------------------------
--------start temp connections-----------------------------------------------------------------------
--        muxedAlgoOutput <= playbackData32BitFakingAlgos;
--        spyData32BitFromAlgos <= (others => '0');
--------end temp connections-----------------------------------------------------------------------
        
        
--    spyData32BitFromAlgos <= (others => '0');

--    debugJetArray <= (127 downto 0 => '0');
--    debugGenericArray <= (127 downto 0 => '0');
--    debugDPhi <= (127 downto 0 => '0');
--    debugDPhi2 <= (127 downto 0 => '0');



    process(sysclk40) begin
        if rising_edge(sysclk40) then
            muxedAlgoOutput_reg <= muxedAlgoOutput;
        end if;
    end process;




  ----------------------------------------------------------------------------
  -- CTP Output
  -----------------------------------------------------------------------------
    
    process(sysclk40) begin
        if rising_edge(sysclk40) then
            ctp_counter <= std_logic_vector(unsigned(ctp_counter)+1);
            if ctp_counter="00000" then ctp_runningOne <= x"00000001";
            elsif ctp_counter="11111" then ctp_runningOne <= x"ffffffff";
            else ctp_runningOne(31 downto 0) <= ctp_runningOne(30 downto 0) & '0';
            end if;
        end if;
    end process;

    ctp_data <= muxedAlgoOutputCTP                     when ctpOutputControl="00"
           else (muxedAlgoOutputCTP and ctpOutputMask) when ctpOutputControl="01"
           else ctpOutputMask                          when ctpOutputControl="10" 
           else ctp_runningOne;

    
    

    ctp_parity0(16) <= '0';
    ctp_parity1(16) <= '0';

CTP_PARITY_GEN: for i in 15 downto 0 generate begin
        ctp_parity0(i) <= ctp_parity0(i+1) xor ctp_data(2*i);
        ctp_parity1(i) <= ctp_parity1(i+1) xor ctp_data(2*i+1);
    end generate;


CTP_CLK_ODDR: ODDR
    generic map(
      DDR_CLK_EDGE => "SAME_EDGE", -- "OPPOSITE_EDGE" or "SAME_EDGE" 
      INIT => '0',   -- Initial value for Q port ('1' or '0')
      SRTYPE => "SYNC") -- Reset Type ("ASYNC" or "SYNC")
    port map (
      Q => ctp_clk_ddr,   -- 1-bit DDR output
      C => sysclk40,    -- 1-bit clock input
      CE => '1',  -- 1-bit clock enable input   
      D1 => '1',  -- 1-bit data input (positive edge)
      D2 => '0',  -- 1-bit data input (negative edge)
      R => '0',    -- 1-bit reset input
      S => '0'     -- 1-bit set input
    );


CTP_PARITY_ODDR: ODDR
    generic map(
      DDR_CLK_EDGE => "SAME_EDGE", -- "OPPOSITE_EDGE" or "SAME_EDGE" 
      INIT => '0',   -- Initial value for Q port ('1' or '0')
      SRTYPE => "SYNC") -- Reset Type ("ASYNC" or "SYNC")
    port map (
      Q => ctp_parity_ddr,   -- 1-bit DDR output
      C => sysclk40,    -- 1-bit clock input
      CE => '1',  -- 1-bit clock enable input   
      D1 => ctp_parity0(0),  -- 1-bit data input (positive edge)
      D2 => ctp_parity1(0),  -- 1-bit data input (negative edge)
      R => '0',    -- 1-bit reset input
      S => '0'     -- 1-bit set input
    );


CTP_DATA_ODDR_GEN: for i in 15 downto 0 generate begin
    CTP_DATA_ODDR: ODDR
    generic map(
      DDR_CLK_EDGE => "SAME_EDGE", -- "OPPOSITE_EDGE" or "SAME_EDGE" 
      INIT => '0',   -- Initial value for Q port ('1' or '0')
      SRTYPE => "SYNC") -- Reset Type ("ASYNC" or "SYNC")
    port map (
      Q => ctp_data_ddr(i),   -- 1-bit DDR output
      C => sysclk40,    -- 1-bit clock input
      CE => '1',  -- 1-bit clock enable input   
      D1 => ctp_data(2*i),  -- 1-bit data input (positive edge)
      D2 => ctp_data(2*i+1),  -- 1-bit data input (negative edge)
      R => '0',    -- 1-bit reset input
      S => '0'     -- 1-bit set input
    );
end generate;




-----------------------------------------------------------------------------
-- ROD
-----------------------------------------------------------------------------
  SHIFT_DATA_FROM_CMX: for i in 0 to NUMBER_OF_ROS_ROI_INPUT_BUSES-2  generate
    ITERATE_WITH_SRL: for j in 0 to 127 generate
      SRL16E_inst : SRL16E
        generic map (
          INIT => X"0000")
        port map (
          Q => muxedData128BitToAlgorithms_reg_shifted(i)(j),       
          A0 => rod_control_register(25)(0),     
          A1 => rod_control_register(25)(1),     
          A2 => rod_control_register(25)(2),     
          A3 => rod_control_register(25)(3),     
          CE => '1',     
          CLK => sysclk40,   
          D => muxedData128BitToAlgorithms_reg(i)(j)    
          );
    end generate ITERATE_WITH_SRL;
  end generate SHIFT_DATA_FROM_CMX;

  rod_reset <= (not sysclk_pll_locked) or  rod_control_register(0)(0);
    
  SELECT_SETTINGS_FOR_OP: if SIMULATION = false generate
    DATA_FOR_ROS_ROI_BUS_PROC : process (sysclk40)
    begin
      if rising_edge(sysclk40) then
        for i in 0 to 80-1 loop 
          data_for_ros_roi_bus_synch(i) <= muxedData128BitToAlgorithms_reg_shifted(i);
          data_for_ros_roi_bus(i) <= data_for_ros_roi_bus_synch(i); --to relax
                                                                    --timing
        end loop;
        --data_for_ros_roi_bus(80) <= std_logic_vector(resize(unsigned(muxedAlgoOutput_reg),128));
        
      end if;
    end process DATA_FOR_ROS_ROI_BUS_PROC;

    --    AlgoOverflow & AlgoResults when enablePlaybackOfAlgos='0' else playbackData32BitFakingAlgos;

    AlgoResults32bit(AlgoResults'length-1 downto 0) <= AlgoResults;
    AlgoOverflow32bit(AlgoOverflow'length-1 downto 0) <= AlgoOverflow;
    
    DATA_FROM_ALGO : for i in 0 to 3 generate
      WRITE_ALGO_RESULTS_TO_ROD : process(sysclk40)
      begin
        if rising_edge(sysclk40) then
          if enablePlaybackOfAlgos='0' then
            data_for_ros_roi_bus_algo(i*2*8+7 downto i*2*8) <= AlgoResults32bit(i*8+7 downto i*8);
            data_for_ros_roi_bus_algo(i*2*8+15 downto i*2*8+8) <= AlgoOverflow32bit(i*8+7 downto i*8);
          else
            data_for_ros_roi_bus_algo(i*2*8+7 downto i*2*8) <= playbackData32BitFakingAlgos(i*8+7 downto i*8);
            data_for_ros_roi_bus_algo(i*2*8+15 downto i*2*8+8) <= playbackData32BitFakingAlgos(i*8+7 downto i*8);
          end if;   
        end if;
      end process WRITE_ALGO_RESULTS_TO_ROD;
    end generate DATA_FROM_ALGO;
    
    SYNCH_ALGO_RESULTS: process(sysclk40)
    begin
      if rising_edge(sysclk40) then
        data_for_ros_roi_bus(80)(0) <= '0'; 
        data_for_ros_roi_bus(80)(64 downto 1) <= data_for_ros_roi_bus_algo(63 downto 0);
        data_for_ros_roi_bus(80)(127 downto 65) <= (others => '0');
      end if;
    end process SYNCH_ALGO_RESULTS;
    
    ASSIGN_NUMBER_OF_SLICES_OP : for i in 0 to NUMBER_OF_SLICES'length-1 generate
      --ONLY_ONE_DATA: if i = 80 or (i < 60 and i > 51) or (i < 76 and i > 67) generate
      --  number_of_slices(i) <= to_unsigned(1, NUMBER_OF_SLICES(0)'length);  
      --  lvl0_offset(i)      <= to_unsigned(0, LVL0_OFFSET(0)'length); 
      --end generate ONLY_ONE_DATA;
      --REST_DATA: if i = 0 or (i > 0  and i < 52) or (i > 59  and i < 68) or (i > 75  and i < 80)   generate
      --  number_of_slices(i) <= to_unsigned(0, NUMBER_OF_SLICES(0)'length);  
      --  lvl0_offset(i)      <= to_unsigned(0, LVL0_OFFSET(0)'length); 
      --end generate REST_DATA;


      CMX_INPUTS: if i /= 80 generate
        number_of_slices(i) <= unsigned(rod_control_register(3 + (i/8))(((i mod 8)+1)*4-2 downto (i mod 8)*4));
        lvl0_offset(i)      <= unsigned(rod_control_register(13 + (i/8))(((i mod 8)+1)*4-2 downto (i mod 8)*4));
      end generate CMX_INPUTS;

      TOPO_INPUT: if i = 80 generate
        number_of_slices(i) <= unsigned(rod_control_register(3 + (i/8))(((i mod 8)+1)*4-2 downto (i mod 8)*4)) when unsigned(rod_control_register(3 + (i/8))(((i mod 8)+1)*4-2 downto (i mod 8)*4)) /= 0 else x"1";
        lvl0_offset(i)      <= unsigned(rod_control_register(13 + (i/8))(((i mod 8)+1)*4-2 downto (i mod 8)*4));
      end generate TOPO_INPUT;
      
    end generate ASSIGN_NUMBER_OF_SLICES_OP;
  end generate SELECT_SETTINGS_FOR_OP;
                        
  SELECT_SETTINGS_FOR_SIM: if SIMULATION = true generate
    DATA_FOR_ROS_ROI_BUS_PROC_SIM : process (sysclk40)
    begin
      if rising_edge(sysclk40) then
        for i in 0 to 80 loop 
          data_for_ros_roi_bus(i) <= x"fedcba98765432100123456789abcdef"; 
        end loop;
      end if;
    end process DATA_FOR_ROS_ROI_BUS_PROC_SIM;
    ASSIGN_NUMBER_OF_SLICES_SIM : for i in 0 to NUMBER_OF_SLICES'length-1 generate
      number_of_slices(i) <= to_unsigned(5, NUMBER_OF_SLICES(0)'length);  
      lvl0_offset(i)      <= to_unsigned(i mod 6, LVL0_OFFSET(0)'length); 
    end generate ASSIGN_NUMBER_OF_SLICES_SIM;
  end generate SELECT_SETTINGS_FOR_SIM;
  
  IBUFDS_L1ACCPETED_IN: IBUFDS
  generic map (DIFF_TERM => true, IBUF_LOW_PWR => false, IOSTANDARD => "DEFAULT")
  port map (O            => l1_accepted_in, I => CTRLBUS_IN_P(6), IB => CTRLBUS_IN_N(6));


    L1A_PULSE_PROC : process (sysclk40 )
    begin
      if rising_edge(sysclk40) then
        if l1_accepted_in_synch_a  = '1' and l1_accepted_in_synch_b = '0' then
          l1_accepted_in_pulse <= '1';
          l1_accepted_in_synch_a <= l1_accepted_in;
          l1_accepted_in_synch_b <= l1_accepted_in_synch_a;
        else
          l1_accepted_in_pulse <= '0';
          l1_accepted_in_synch_a <= l1_accepted_in;
          l1_accepted_in_synch_b <= l1_accepted_in_synch_a;
        end if;
      end if;
    end process L1A_PULSE_PROC;

    send_on_crc <= rod_control_register(29)(NUMBER_OF_ROS_ROI_INPUT_BUSES-1-64 downto 0) & rod_control_register(28) & rod_control_register(27);
    
    L1TOPO_TO_DDR_INST : entity work.l1topo_to_ddr
      generic map (
        MAKE_SYNCH_INPUT => 0)
      port map (
        RESET                 => rod_reset,
        DATA_IN_CLK           => sysclk40,
        DATA_OUT_CLK          => sysclk160,
        DDR_CLK               => rodclk80,
        NUMBER_OF_SLICES      => number_of_slices,
        LVL0_ACCEPTED         => l1_accepted_in_pulse,
        LVL0_VALID            => '1',
--        LVL0_GLOBAL_OFFSET    => rod_rw_register(5)(7 downto 0),
        LVL0_GLOBAL_OFFSET    => rod_control_register(1)(7 downto 0),
        LVL0_FULL_THR         => rod_control_register(2)(7 downto 0),
        LVL0_OFFSET           => lvl0_offset,
        ROS_ROI_IN_DATA       => data_for_ros_roi_bus,
        CRC_ERR_IN            => crc_err,
        MAX_OFFSET            => rod_control_register(26)(3 downto 0),
        SEND_ON_CRC           => send_on_crc,
        DATA_VALID_IN         => "1",
        OUT_DATA              => out_data,
        DATA_VALID_OUT        => data_valid_out,
        L0_BUSY               => l0_busy,
        SPECIAL_CHARACTER_OUT => special_character_out,
--        ROD_DBG               => rod_rw_register(8)
--        TEST_IN               => playbackData32BitFakingAlgos(0),
        ROD_DBG               => rod_status_register(2)
    );

    OBUFDS_BUSY_OUT: OBUFDS
      generic map (IOSTANDARD => "DEFAULT")
      port map (I => l0_busy, O => CTRLBUS_OUT_P(9), OB => CTRLBUS_OUT_N(9));

    TRANSMITTERS_WRAPPER_INST : entity work.TransmittersWrapper
      generic map(
        LINKS_NUMBER => LINES_NUMBER,
        SIMULATION   => SIMULATION
        )
      port map(
        RESET          => rod_reset,
        CLK_BIT_IN_IO  => rodclk400_io,
        CLK_BIT_IN_r  => rodclk400_r,
        CLK_WORD_IN    => rodclk80,
        DATA_IN        => out_data,
        DATA_VALID_IN  => data_valid_out,
        DATA_KCTRL_IN  => special_character_out,
        DATA_PIN_P_OUT(7 downto 0) => CTRLBUS_OUT_P(17 downto 10),
        DATA_PIN_N_OUT(7 downto 0) => CTRLBUS_OUT_N(17 downto 10)
    );


--------------------------------------------------------
--output buffer
--------------------------------------------------------

IPBUSBRIDGE_OUT_OBUFDS_GEN: for i in 2 downto 0 generate
	IPBUSBRIDGE_OUT_OBUFDS: OBUFDS
		port map(
			I  => ipbusBridgeBusOut(i),
			O  => ipbusBridgeBusOut_p(i),
			OB => ipbusBridgeBusOut_n(i)
		);
	end generate;



CTP_CLK_OBUFDS: OBUFDS
    port map(
        I => ctp_clk_ddr,
        O => ctp_clk_p,
        OB => ctp_clk_n
    );


CTP_PARITY_OBUFDS: OBUFDS
    port map(
        I => ctp_parity_ddr,
        O => ctp_parity_p,
        OB => ctp_parity_n
    );
  
  
CTP_DATA_OBUFDS_GEN: for i in 15 downto 0 generate
    CTP_DATA_OBUFDS: OBUFDS
        port map(
            I => ctp_data_ddr(i),
            O => ctp_data_p(i),
            OB => ctp_data_n(i)
        );
    end generate;
 
--------------------------------------------------------
--output mapping
--------------------------------------------------------

--TXMGTMap_GEN: for i in TxMGTHigh downto TxMGTLow generate begin
--    OPTO_T2_P(8+15-i) <= TxP(i);
--    OPTO_T2_N(8+15-i) <= TxN(i);  
--end generate;




    CTRLBUS_OUT_P(18) <= ipbusBridgeBusOut_p(0);
	CTRLBUS_OUT_N(18) <= ipbusBridgeBusOut_n(0);
	CTRLBUS_OUT_P(19) <= ipbusBridgeBusOut_p(1);
	CTRLBUS_OUT_N(19) <= ipbusBridgeBusOut_n(1);
	CTRLBUS_OUT_P(20) <= ipbusBridgeBusOut_p(2);
	CTRLBUS_OUT_N(20) <= ipbusBridgeBusOut_n(2);
    
    EXT_V7_P(0) <= ctp_clk_p;
    EXT_V7_N(0) <= ctp_clk_n;
    
    EXT_V7_P(1) <= ctp_parity_p;
    EXT_V7_N(1) <= ctp_parity_n;
    
CTP_DATA_MAP: for i in 15 downto 0 generate begin
    EXT_V7_P(i+2) <= ctp_data_p(i);
    EXT_V7_N(i+2) <= ctp_data_n(i);
end generate;
    
    
--MMCX_U56_DDR: ODDR
--   generic map(
--      DDR_CLK_EDGE => "SAME_EDGE", 
--      INIT => '0',
--      SRTYPE => "SYNC")
--   port map (
--      Q => MMCX_U56,
--      C => sysclk40,
--      CE => '1',
--      D1 => '1',
--      D2 => '0',
--      R => '0',
--      S => '0'
--   );

--MMCX_U56_OBUF: OBUF
--    port map(
--        I => ctp_data(15),
--        O => MMCX_U56
--    );


--RXOUTCLK_GBUF: BUFG port map(I => rxoutclk, O => rxoutclk_bufg);

--MMCX_U57_DDR: ODDR
--   generic map(
--      DDR_CLK_EDGE => "SAME_EDGE", 
--      INIT => '0',
--      SRTYPE => "SYNC")
--   port map (
--      Q => MMCX_U57,
--      C => rxoutclk_bufg,
--      CE => '1',
--      D1 => '1',
--      D2 => '0',
--      R => '0',
--      S => '0'
--   );

end Behavioral;
