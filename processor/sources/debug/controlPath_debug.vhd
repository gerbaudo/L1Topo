----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 15.07.2014 12:06:08
-- Design Name: 
-- Module Name: extended_ipbus - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.l1topo_package.all;
use work.ipbus.all;
use work.L1TopoDataTypes.all;
use work.L1TopoGTConfiguration.all;
use work.rod_l1_topo_types_const.all;


-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ipbusModule_debug is
    Port (
        sysclk40: in std_logic;
        sysclk80: in std_logic;
        sysclk160: in std_logic;
        sysclk400: in std_logic;
        sysclk_pll_locked: in std_logic;
        
        ipbBridgeBusIn: in std_logic_vector(4 downto 0);
        ipbBridgeBusOut: out std_logic_vector(2 downto 0);
        
        
        -- module interfaces
        
        xadc_control: out std_logic_vector(25 downto 0);
        xadc_status:  in  std_logic_vector(22 downto 0);
        
        ttcBridge_control: out std_logic_vector(5 downto 0);
        ttcBridge_status: in std_logic_vector(14 downto 0);
        ttcBroadcast: in std_logic;

        rod_control_register: out rod_control_registers_array;
        rod_status_register : in  rod_status_registers_array;
                

        -- Real-Time path configuration

        QuadControl: out arraySLV32(QuadHigh downto QuadLow);
        QuadStatus:  in  arraySLV32(QuadHigh downto QuadLow);
        ChannelControl: out arraySLV32(79 downto 0);
        ChannelStatus: in arraySLV32(79 downto 0);
        DataShift40MHz: out  arraySLV3(MGTHigh downto MGTLow);--+-25 ns delay, do not expect this to work for large delays
                
        
        -- Real-Time path data
        
--        spyData36BitFromMGTs: in arraySLV36(MGTHigh downto MGTLow);
--        enablePlaybackOfMGTs: out std_logic;
--        playbackData36BitFakingMGTs: out arraySLV36(MGTHigh downto MGTLow);
        
        spyData128BitFromDeserialisers: in arraySLV128(79 downto 0);
        enablePlaybackOfDeserialisers: out std_logic;
        playbackData128BitFakingDeserialisers: out arraySLV128(79 downto 0);
        
        
        SortParameters: out ParameterSpace(NumberOfSortAlgorithms - 1 downto 0);
        AlgoParameters: out ParameterSpace(NumberOfAlgorithms - 1 downto 0);
        
        spyData32BitFromAlgos: in std_logic_vector(31 downto 0);
        enablePlaybackOfAlgos: out std_logic;
        playbackData32BitFakingAlgos: out std_logic_vector(31 downto 0);
        
        ctpOutputControl: out std_logic_vector(1 downto 0);
        ctpOutputMask: out std_logic_vector(31 downto 0);
        
        -- MISC
        
        ProcessorID:     in std_logic_vector(31 downto 0);
        FirmwareVersion: in std_logic_vector(31 downto 0);
        
        -- DEBUG
        
        DebugOutput : in BitArray128(256 - 1 downto 0);
        
--        AlgoDebug: in BitArray128(256 - 1 downto 0);
        debugCRC: in arraySLV256(11 downto 0)
        
--        debugIPBusBridge: out std_logic_vector(239 downto 0)
        
--        debugJetArray: in std_logic_vector(127 downto 0);
--        debugClusterArray: in std_logic_vector(127 downto 0);
--        debugGenericClusterArray: in std_logic_vector(127 downto 0);
--        debugGenericJetArray: in std_logic_vector(127 downto 0);
--        debugDPhi: in std_logic_vector(127 downto 0 )
--        debugDPhi2: in std_logic_vector(127 downto 0)
--        debugPlaybackSpy128: out std_logic_vector(432 downto 0);
--        debugPlaybackSpy32: out std_logic_vector(176 downto 0)
        
        
--        datasource: in std_logic_vector(35 downto 0);
--        results: in std_logic_vector(31 downto 0)
        
    );
end ipbusModule_debug;

architecture Behavioral of ipbusModule_debug is

    signal ipb_rst: std_logic;
    signal ipb_master_write: ipb_wbus;
    signal ipb_master_read: ipb_rbus;
    
    signal ipbBridge_idelayStatus: std_logic_vector(24 downto 0);
    signal ipbBridge_alignmentStatus: std_logic_vector(19 downto 0);
    signal ipbBridgeErrorCounter: std_logic_vector(29 downto 0);
    signal ipbBridgeErrorCounterReset: std_logic_vector(5 downto 0);
    
--    signal idelay_counter_0 :  std_logic_vector(4 downto 0);
--    signal idelay_counter_1 :  std_logic_vector(4 downto 0);
--    signal idelay_counter_2 :  std_logic_vector(4 downto 0);
--    signal idelay_counter_3 :  std_logic_vector(4 downto 0);
--    signal idelay_counter_4 :  std_logic_vector(4 downto 0);
    
begin



ipbBridge: entity work.ipbusBridge
    port map(
        sysclk => sysclk40,
        parallelclk => sysclk80,
        serialclk => sysclk400,
        pll_locked => sysclk_pll_locked,
        
        -- ipbBridge request
        ipbBridgeBusIn => ipbBridgeBusIn,
        ipb_write => ipb_master_write,
        
        -- ipbBridge response
        ipb_read => ipb_master_read,
        ipbBridgeBusOut => ipbBridgeBusOut,
        
        idelayStatus => ipbBridge_idelayStatus, 
        alignmentStatus_out => ipbBridge_alignmentStatus,
        errorCounter_out => ipbBridgeErrorCounter,
        errorCounterReset => ipbBridgeErrorCounterReset
         
    );







    





slaves: entity work.slaves_debug
    port map(
        sysclk40 => sysclk40,
        sysclk80 => sysclk80,
        sysclk160 => sysclk160,
        sysclk_pll_locked => sysclk_pll_locked,
        
        -- ipbBridge
        
        ipb_in => ipb_master_write,
        ipb_out => ipb_master_read,
        
        
        
        ipbBridge_idelayStatus => ipbBridge_idelayStatus,
        ipbBridge_alignmentStatus => ipbBridge_alignmentStatus,
        ipbBridgeErrorCounter => ipbBridgeErrorCounter,
        ipbBridgeErrorCounterReset => ipbBridgeErrorCounterReset,
        
        
        
        -- module interfaces
        
        xadc_control => xadc_control,
        xadc_status  => xadc_status,
        
        
        
        ttcBridge_control => ttcBridge_control,
        ttcBridge_status => ttcBridge_status,
        ttcBroadcast => ttcBroadcast,

        rod_control_register => rod_control_register,
        rod_status_register  => rod_status_register,


        -- Real-Time path configuration

        QuadControl => QuadControl,
        QuadStatus  => QuadStatus,
        ChannelControl => ChannelControl,
        ChannelStatus => ChannelStatus,
        DataShift40MHz => DataShift40MHz,

        
        -- Real-Time path data
        
--        spyData36BitFromMGTs => spyData36BitFromMGTs,
--        enablePlaybackOfMGTs => enablePlaybackOfMGTs,
--        playbackData36BitFakingMGTs => playbackData36BitFakingMGTs,
        
        spyData128BitFromDeserialisers => spyData128BitFromDeserialisers,
        enablePlaybackOfDeserialisers => enablePlaybackOfDeserialisers,
        playbackData128BitFakingDeserialisers => playbackData128BitFakingDeserialisers,
        
        SortParameters => SortParameters,
        AlgoParameters => AlgoParameters,
        
        spyData32BitFromAlgos => spyData32BitFromAlgos,
        enablePlaybackOfAlgos => enablePlaybackOfAlgos,
        playbackData32BitFakingAlgos => playbackData32BitFakingAlgos,
        
        ctpOutputControl => ctpOutputControl,
        ctpOutputMask => ctpOutputMask,
        
        -- MISC
        
        ProcessorID => ProcessorID,
        FirmwareVersion => FirmwareVersion,
      
      
        -- DEBUG
        
        DebugOutput => DebugOutput,
        
--        AlgoDebug => AlgoDebug,
        
        debugCRC => debugCRC
        
      
--        debugPlaybackSpy128 => debugPlaybackSpy128,
--        debugPlaybackSpy32 => debugPlaybackSpy32
        
        
--        debugJetArray => debugJetArray,
--        debugClusterArray => debugClusterArray,
--        debugGenericJetArray => debugGenericJetArray,
--        debugGenericClusterArray => debugGenericClusterArray,
--        debugDPhi => debugDPhi
--        debugDPhi2 => debugDPhi2
        
    );


end Behavioral;
