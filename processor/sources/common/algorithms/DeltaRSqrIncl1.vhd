------------------------------------------------------------------------------------------
-- Author/Modified by	: manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de
-- Description			: applies Et cut on TOB list; calculates delta eta and delta phi of all 
--						  TOB combinations in this list and applies rSquare cut
-- Number of registers	: 0
------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use work.L1TopoDataTypes.ALL;
use work.L1TopoFunctions.all;

entity DeltaRSqrIncl1 is
	generic(InputWidth       : integer := 9;
		    NumResultBits    : integer := 1;
		    MaxTob           : integer := 0; -- number of TOBs which should be considered
		    RequireOneBarrel : integer := 0
	);
	Port(ClockBus   : in  std_logic_vector(2 downto 0);
		 Parameters : in  ParameterArray;
		 Tob        : in  TOBArray(InputWidth - 1 downto 0);
		 Results    : out std_logic_vector(NumResultBits - 1 downto 0);
		 Overflow   : out std_logic_vector(NumResultBits - 1 downto 0)
	);
end DeltaRSqrIncl1;

architecture Behavioral of DeltaRSqrIncl1 is

	-- constants

	constant maxCount : integer := getMaxCount(InputWidth, MaxTob);

	type deltaarrEta is array (integer range <>) of std_logic_vector(GenericEtaBitWidth - 1 downto 0);
	type deltaarrPhi is array (integer range <>) of std_logic_vector(GenericPhiBitWidth - 1 downto 0);
	type deltaarr11 is array (integer range <>) of std_logic_vector(2 * GenericEtaBitWidth downto 0);
	type deltamatEta is array (integer range <>) of deltaarrEta(InputWidth - 1 downto 0);
	type deltamatPhi is array (integer range <>) of deltaarrPhi(InputWidth - 1 downto 0);
	type deltamatRSqr is array (integer range <>) of deltaarr11(InputWidth - 1 downto 0);

	type thrAcceptArray is array (NumResultBits - 1 downto 0) of std_logic_vector(InputWidth - 1 downto 0);
	type RSqrThreshold is array (integer range <>) of std_logic_vector(2 * GenericEtaBitWidth downto 0);
	type etThreshArray is array (NumResultBits - 1 downto 0) of std_logic_vector(GenericEtBitWidth - 1 downto 0);

	signal thr_flag1 : thrAcceptArray;
	signal thr_flag2 : thrAcceptArray;

	signal InsideBarrel : std_logic_vector(InputWidth - 1 downto 0);

	signal deltaEta  : deltamatEta(InputWidth - 1 downto 0);
	signal deltaPhi  : deltamatPhi(InputWidth - 1 downto 0);
	signal deltaRSqr : deltamatRSqr(InputWidth - 1 downto 0);

	signal MinEt1  : etThreshArray;
	signal MinEt2  : etThreshArray;
	signal MaxRSqr : RSqrThreshold(NumResultBits - 1 downto 0);
	signal MinRSqr : RSqrThreshold(NumResultBits - 1 downto 0);

	signal results_out  : std_logic_vector(NumResultBits - 1 downto 0);
	signal overflow_out : std_logic_vector(NumResultBits - 1 downto 0);

begin
	genThrArr : for i in 0 to NumResultBits - 1 generate
		MinEt1(i)  <= Parameters(i * 4 + 0)(GenericEtBitWidth - 1 downto 0);
		MinEt2(i)  <= Parameters(i * 4 + 1)(GenericEtBitWidth - 1 downto 0);
		MinRSqr(i) <= Parameters(i * 4 + 2)(2 * GenericEtaBitWidth downto 0);
		MaxRSqr(i) <= Parameters(i * 4 + 3)(2 * GenericEtaBitWidth downto 0);
	end generate;

	gen_parameter_sets : for k in 0 to (NumResultBits - 1) generate
		genThrFlag1 : for i in 0 to MaxCount - 1 generate
			thr_flag1(k)(i) <= '1' when Tob(i).Et > MinEt1(k) else '0';
		end generate;

		genThrFlag2 : for i in 0 to MaxCount - 1 generate
			thr_flag2(k)(i) <= '1' when Tob(i).Et > MinEt2(k) else '0';
		end generate;
	end generate;

	genInsideBarrel : for i in 0 to maxCount - 1 generate
		InsideBarrel(i) <= '1' when abs (signed(Tob(i).Eta)) <= 10 else '0';
	end generate;

	gen1 : for i in 0 to (maxCount - 2) generate
		gen2 : for j in (i + 1) to (maxCount - 1) generate
			dEta_inst : entity work.DeltaEtaCalc
				port map(
					eta1In      => Tob(i).Eta,
					eta2In      => Tob(j).Eta,
					deltaEtaOut => deltaEta(i)(j)
				);
			dPhi_inst : entity work.DeltaPhiCalc
				port map(phi1In      => Tob(i).Phi,
					     phi2In      => Tob(j).Phi,
					     deltaPhiOut => deltaPhi(i)(j));
			DeltaRSqrCalc_inst : entity work.DeltaRSqrCalc
				Port map(deltaEta  => deltaEta(i)(j),
					     deltaPhi  => deltaPhi(i)(j),
					     deltaRSqr => deltaRSqr(i)(j)
				);
		end generate;
	end generate;

	compare_thresholds : process(deltaRSqr, thr_flag1, thr_flag2, InsideBarrel, MinRSqr, MaxRSqr) -- Compare delta eta with thresholds for all TOB pairs -- TODO: BUGGY?
		variable hits : std_logic_vector(NumResultBits - 1 downto 0);
	begin
		hits := (others => '0');
		for i in 0 to (maxCount - 2) loop -- loop over TOBs i and j
			for j in (i + 1) to (maxCount - 1) loop
				for k in 0 to NumResultBits - 1 loop -- loop over thresholds
					if RequireOneBarrel = 1 then
						if (((deltaRSqr(i)(j) <= MaxRSqr(k)) AND (deltaRSqr(i)(j) >= MinRSqr(k))) and (((thr_flag1(k)(i) = '1') and (thr_flag2(k)(j) = '1')) or ((thr_flag1(k)(j) = '1') and (thr_flag2(k)(i) = '1'))) AND ((InsideBarrel(i) = '1') OR (InsideBarrel(
										j) = '1'))) then
							hits(k) := '1';
						end if;
					end if;
					if RequireOneBarrel = 0 then
						if (((deltaRSqr(i)(j) <= MaxRSqr(k)) AND (deltaRSqr(i)(j) >= MinRSqr(k))) and (((thr_flag1(k)(i) = '1') and (thr_flag2(k)(j) = '1')) or ((thr_flag1(k)(j) = '1') and (thr_flag2(k)(i) = '1')))
						) then
							hits(k) := '1';
						end if;
					end if;
				end loop;
			end loop;
		end loop;
		results_out <= hits;
	end process;

	Results <= results_out;

	-----------------------------
	-- generate overflow bits  --
	-----------------------------
	genOverflows : for result in 0 to NumResultBits - 1 generate
		-- get overflow from input list
		overflow_out(result) <= Tob(0).Overflow;
	end generate;

	Overflow <= overflow_out;

end Behavioral;