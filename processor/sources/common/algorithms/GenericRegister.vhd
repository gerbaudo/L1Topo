------------------------------------------------------------------------------------------
-- Author/Modified by	: manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de
-- Description			: register for std_logic_vector
-- Number of registers	: 1
------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.L1TopoDataTypes.all;

entity GenericRegister is
	generic(reg_width : integer := 5
	);
	port(clk     : in  std_logic;
		 enable  : in  std_logic;
		 reg_in  : in  std_logic_vector(reg_width - 1 downto 0);
		 reg_out : out std_logic_vector(reg_width - 1 downto 0)
	);
end GenericRegister;

architecture behavioral of GenericRegister is
	signal sr, tmp : std_logic_vector(reg_width - 1 downto 0);
	signal clock   : std_logic;

begin
	clock <= clk;
	tmp   <= reg_in;

	process(clock)
	begin
		if (rising_edge(clock)) then
			if (enable = '1') then
				sr <= tmp;
			end if;
		end if;
	end process;

	reg_out <= sr;

end behavioral;