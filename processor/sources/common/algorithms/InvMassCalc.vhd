------------------------------------------------------------------------------------------
-- Author/Modified by	: manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de
-- Description			: calculates the invariant mass of two given TOBs 
-- Number of registers	: 0
------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.L1TopoDataTypes.ALL;

entity InvMassCalc is
	generic(NumRegisters : integer := 0 -- number of registers (0 or 1)
	);

	Port(ClockBus : in  std_logic_vector(2 downto 0);
		 eta1     : in  std_logic_vector(GenericEtaBitWidth - 1 downto 0);
		 eta2     : in  std_logic_vector(GenericEtaBitWidth - 1 downto 0);
		 phi1     : in  std_logic_vector(GenericPhiBitWidth - 1 downto 0);
		 phi2     : in  std_logic_vector(GenericPhiBitWidth - 1 downto 0);
		 energy1  : in  std_logic_vector(GenericEtBitWidth - 1 downto 0);
		 energy2  : in  std_logic_vector(GenericEtBitWidth - 1 downto 0);
		 mass_sqr : out std_logic_vector(InvariantMassSqrBitWidth - 1 downto 0)
	);
end InvMassCalc;

architecture Behavioral of InvMassCalc is
	attribute S : string;

	signal delta_eta : std_logic_vector(GenericEtaBitWidth - 1 downto 0);
	signal delta_phi : std_logic_vector(GenericPhiBitWidth - 1 downto 0);

	signal eta1_sig : std_logic_vector(GenericEtaBitWidth - 1 downto 0);
	signal eta2_sig : std_logic_vector(GenericEtaBitWidth - 1 downto 0);

	signal phi1_sig         : std_logic_vector(GenericPhiBitWidth - 1 downto 0);
	signal phi2_sig         : std_logic_vector(GenericPhiBitWidth - 1 downto 0);
	signal cos_result       : std_logic_vector(CosSinBitWidth - 1 downto 0);
	signal cos_result_reg1  : std_logic_vector(CosSinBitWidth - 1 downto 0);
	signal cosh_result      : std_logic_vector(CoshBitWidth - 1 downto 0);
	signal cosh_result_reg1 : std_logic_vector(CoshBitWidth - 1 downto 0);

	signal energymult : unsigned(2 * GenericEtBitWidth - 1 downto 0);

	signal cos_diff   : std_logic_vector(CoshBitWidth + 1 - 1 downto 0);
	signal massSqrTmp : std_logic_vector(2 * GenericEtBitWidth + CoshBitWidth + 1 - 1 downto 0);

	signal energy1_result_reg1 : std_logic_vector(GenericEtBitWidth - 1 downto 0);
	signal energy2_result_reg1 : std_logic_vector(GenericEtBitWidth - 1 downto 0);

	attribute S of delta_eta : signal is "true";
	attribute S of delta_phi : signal is "true";

begin
	eta1_sig <= eta1;
	eta2_sig <= eta2;

	phi1_sig <= phi1;
	phi2_sig <= phi2;

	DEta_calc : entity work.DeltaEtaCalc
		port map(
			eta1In      => eta1_sig,
			eta2In      => eta2_sig,
			deltaEtaOut => delta_eta
		);

	DPhi_calc : entity work.DeltaPhiCalc
		port map(
			phi1In      => phi1_sig,
			phi2In      => phi2_sig,
			deltaPhiOut => delta_phi
		);

	Cos_module : entity work.Cos
		port map(inp => delta_phi,
			     oup => cos_result
		);

	Cosh_module : entity work.Cosh
		port map(inp => delta_eta,
			     oup => cosh_result
		);

	--Register
	gen_output_1reg : if (NumRegisters = 1) generate
		Cos_reg1 : entity work.GenericRegister
			generic map(reg_width => CosSinBitWidth
			)
			port map(clk     => ClockBus(0),
				     enable  => '1',
				     reg_in  => cos_result,
				     reg_out => cos_result_reg1
			);

		Cosh_reg1 : entity work.GenericRegister
			generic map(reg_width => CoshBitWidth
			)
			port map(clk     => ClockBus(0),
				     enable  => '1',
				     reg_in  => cosh_result,
				     reg_out => cosh_result_reg1
			);

		Energy1_reg1 : entity work.GenericRegister
			generic map(reg_width => GenericEtBitWidth
			)
			port map(clk     => ClockBus(0),
				     enable  => '1',
				     reg_in  => energy1,
				     reg_out => energy1_result_reg1
			);

		Energy2_reg1 : entity work.GenericRegister
			generic map(reg_width => GenericEtBitWidth
			)
			port map(clk     => ClockBus(0),
				     enable  => '1',
				     reg_in  => energy2,
				     reg_out => energy2_result_reg1
			);
	end generate;

	Output_0reg : if (NumRegisters = 0) generate
		energymult <= unsigned(energy1) * unsigned(energy2);

		cos_diff <= std_logic_vector(signed('0' & cosh_result) - signed(cos_result));
	end generate;

	Output_1reg : if (NumRegisters = 1) generate
		energymult <= unsigned(energy1_result_reg1) * unsigned(energy2_result_reg1);

		cos_diff <= std_logic_vector(signed('0' & cosh_result_reg1) - signed(cos_result_reg1));
	end generate;

	massSqrTmp <= std_logic_vector(energymult * unsigned(cos_diff)); -- cos_diff is positive!

	mass_sqr <= massSqrTmp(InvariantMassSqrBitWidth + 7 - 1 - 1 downto 7) & '0'; -- 7 is the number of cos/cosh decimal bits


end Behavioral;
