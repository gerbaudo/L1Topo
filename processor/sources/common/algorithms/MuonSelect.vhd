------------------------------------------------------------------------------------------
-- Author/Modified by	: manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de
-- Description			: applies eta cut to MuonTOB list, selects TOBs
--						  above an Et threshold and converts them to GenericTOBs
-- Number of registers	: 2
------------------------------------------------------------------------------------------

--------------
-- includes --
--------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use work.l1topo_package.all;
use work.L1TopoDataTypes.all;
use work.L1TopoFunctions.all;

------------------------
-- entity declaration --
------------------------

entity MuonSelect is
	generic(InputWidth         : integer := 32; --number of input TOBs
		    InputWidth1stStage : integer := 16; --number of TOBs in 1st stage
		    OutputWidth        : integer := 10 --number of selected output TOBs
	);
	Port(ClockBus     : in  std_logic_vector(2 downto 0);
		 Parameters   : in  ParameterArray;
		 MuonTobArray : in  MuonArray(InputWidth - 1 downto 0);
		 TobArrayOut  : out TOBArray(OutputWidth - 1 downto 0)
	);
end MuonSelect;

-----------------------------------------
-- behavioral description of algorithm --
-----------------------------------------

architecture Behavioral of MuonSelect is
	constant NumSublists : integer := 4;

	-------------
	-- signals --
	-------------

	signal muons_sorted                                 : MuonArray(InputWidth - 1 downto 0);
	signal muons_aligned, muons_aligned_reg1            : MuonArray(OutputWidth - 1 downto 0);
	signal reg_out_3                                    : TOBArray(OutputWidth - 1 downto 0);
	signal overflow_octant, overflow_reg_final          : std_logic;
	signal overflow_reg                                 : std_logic_vector(8 - 1 downto 0);
	signal select_pattern_out                           : std_logic_vector(OutputWidth - 1 downto 0);
	signal NumTobsOut                                   : integer range 0 to OutputWidth - 1;
	signal clkFast_int                                  : std_logic;
	signal clk40_int                                    : std_logic;
	signal overflowPt, overflowPt_reg1, overflowPt_reg3 : std_logic;

begin
	clk40_int   <= ClockBus(0);
	clkFast_int <= ClockBus(2);

	----------------------------------------------------------
	-- translate to generic TOB and select TOBs (Eta range) --
	----------------------------------------------------------

	sel_eta_range : process(MuonTobArray)
		variable tmpOverflow : std_logic;
	begin
		tmpOverflow := '0';

		for i in 0 to InputWidth - 1 loop
			if (MuonTobArray(i).Pt /= "11") then
				muons_sorted(i) <= MuonTobArray(i);
			else
				muons_sorted(i) <= emptyMuonTOB;
				tmpOverflow     := '1';
			end if;
		end loop;
		overflow_octant <= tmpOverflow;
	end process;

	----------------------------------------------------------------------
	-- first stage: sort the input TOBs in groups of InputWidth1stStage --
	----------------------------------------------------------------------

	gen_select_merge_muon : entity work.SelectMergeMuon
		generic map(
			InputWidthBlocks   => NumSublists,
			InputWidthPerBlock => InputWidth / NumSublists,
			OutputWidth        => OutputWidth,
			DoPtCut            => 1
		)
		port map(
			clk40              => ClockBus(0),
			clkFast            => ClockBus(2),
			Parameters         => Parameters,
			inp                => muons_sorted,
			oup                => muons_aligned,
			select_pattern_out => select_pattern_out,
			NumTobsOut         => NumTobsOut,
			overflow           => overflowPt
		);

	delay_160 : process(ClockBus(2))
	begin
		if (rising_edge(ClockBus(2))) then
			muons_aligned_reg1 <= muons_aligned;
			overflowPt_reg1    <= overflowPt;
		end if;
	end process;

	gen_generic : process(ClockBus(0))
	begin
		if (rising_edge(ClockBus(0))) then
			for i in OutputWidth - 1 downto 0 loop
				if (muons_aligned_reg1(i).no_candidate = '1') then
					reg_out_3(i) <= empty_tob;
				else
					reg_out_3(i) <= to_GenericTOB(muons_aligned_reg1(i));
				end if;
			end loop;

			overflowPt_reg3 <= overflowPt_reg1;
		end if;
	end process;

	gen_overflow_reg2 : entity work.StdLogicRegister
		port map(clk     => ClockBus(0),
			     enable  => '1',
			     reg_in  => overflow_reg(overflow_reg'length - 1),
			     reg_out => overflow_reg_final
		);

	overflow_reg(0) <= overflow_octant;

	gen_overflow_reg : for i in 7 - 1 downto 0 generate
		overflow_reg(i + 1) <= overflow_reg(i) when rising_edge(ClockBus(2));
	end generate;

	-- workaround to set overflow bit and reordering of TOBs
	gen_set_overflow : for i in 0 to OutputWidth - 1 generate
		TobArrayOut(i).Et  <= reg_out_3(i).Et;
		TobArrayOut(i).Eta <= reg_out_3(i).Eta;
		TobArrayOut(i).Phi <= reg_out_3(i).Phi;
	end generate;

	TobArrayOut(0).Overflow <= overflow_reg_final or overflowPt_reg3;

	-- only needed for simulation
	gen_set_dummy_overflow : for i in 1 to OutputWidth - 1 generate
		TobArrayOut(i).Overflow <= '0';
	end generate;

end Behavioral;
