------------------------------------------------------------------------------------------
-- Author/Modified by	: manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de
-- Description			: merges selected lists using a faster clock
-- Number of registers	: 1
------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.L1TopoDataTypes.all;
use work.L1TopoFunctions.all;

entity SelectMerge is
	generic(InputWidthBlocks   : integer := 8;
		    InputWidthPerBlock : integer := 11
	);
	Port(clk40         : in  std_logic;
		 clkFast       : in  std_logic;
		 threshold_res : in  std_logic_vector(InputWidthPerBlock * (InputWidthBlocks + 1) - 1 downto 0);
		 inp           : in  TOBArray(InputWidthPerBlock * (InputWidthBlocks + 1) - 1 downto 0);
		 oup           : out TOBArray(InputWidthPerBlock - 1 downto 0);
		 overflow      : out std_logic
	);
end SelectMerge;

architecture Behavioral of SelectMerge is
	constant countOut : integer := InputWidthBlocks - 1;

	signal tmp_tobs : TOBArray(InputWidthPerBlock - 1 downto 0);

	signal current_block                           : TOBArray(InputWidthPerBlock - 1 downto 0);
	signal current_threshold, current_tob_count_in : std_logic_vector(InputWidthPerBlock - 1 downto 0);

	signal ctrl_reg_flags : std_logic_vector(InputWidthPerBlock - 1 downto 0);

	signal reg_ctrl_num : integer range 0 to 15;

	signal clk40_int, clkFast_int : std_logic;
	signal threshold_res_int      : std_logic_vector(InputWidthPerBlock * (InputWidthBlocks + 1) - 1 downto 0);
	signal inp_int                : TOBArray(InputWidthPerBlock * (InputWidthBlocks + 1) - 1 downto 0);

	signal cnt        : integer range 0 to InputWidthBlocks := 1; -- 2 for 4 input lists
	constant cntStart : integer range 0 to InputWidthBlocks := 1; --InputWidthBlocks / 2; --2;
	signal toggle40   : std_logic                           := '0';
	signal toggle160  : std_logic                           := '0';
	signal resetClk   : std_logic                           := '0';

	signal overflowSum              : unsigned(6 - 1 downto 0) := (others => '0');
	signal currentTobCount          : std_logic_vector(4 - 1 downto 0);
	signal overflowTmp, overflowReg : std_logic;

begin
	clk40_int   <= clk40;
	clkFast_int <= clkFast;

	current_tob_count_in <= threshold_res_int((cnt + 1) * InputWidthPerBlock - 1 downto (cnt + 0) * InputWidthPerBlock);
	currentTobCount      <= std_logic_vector(to_unsigned(reg_ctrl(current_tob_count_in), currentTobCount'length));

	reg_ctrl_num <= reg_ctrl(ctrl_reg_flags);

	current_block     <= inp_int(1 * InputWidthPerBlock - 1 downto 0 * InputWidthPerBlock);
	current_threshold <= threshold_res_int(1 * InputWidthPerBlock - 1 downto 0 * InputWidthPerBlock);

	fill_final_array : process(clkFast_int)
	begin
		if rising_edge(clkFast_int) then
			if (resetClk = '1') then
				cnt <= cntStart;
			else
				if (cnt = InputWidthBlocks - 1) then
					cnt <= 0;
				else
					cnt <= cnt + 1;
				end if;
			end if;

			if (cnt = 0) then
				overflowSum <= unsigned('0' & ('0' & currentTobCount));
			else
				overflowSum <= overflowSum + unsigned('0' & ('0' & currentTobCount));
			end if;

			if (cnt = 0) then
				inp_int(InputWidthPerBlock * InputWidthBlocks - 1 downto 0)           <= inp(InputWidthPerBlock * (InputWidthBlocks + 1) - 1 downto InputWidthPerBlock);
				threshold_res_int(InputWidthPerBlock * InputWidthBlocks - 1 downto 0) <= threshold_res(InputWidthPerBlock * (InputWidthBlocks + 1) - 1 downto InputWidthPerBlock);
			else
				inp_int(InputWidthPerBlock * InputWidthBlocks - 1 downto 0)           <= inp_int(InputWidthPerBlock * (InputWidthBlocks + 1) - 1 downto InputWidthPerBlock);
				threshold_res_int(InputWidthPerBlock * InputWidthBlocks - 1 downto 0) <= threshold_res_int(InputWidthPerBlock * (InputWidthBlocks + 1) - 1 downto InputWidthPerBlock);
			end if;

			if cnt = 0 then
				ctrl_reg_flags <= threshold_res(1 * InputWidthPerBlock - 1 downto 0 * InputWidthPerBlock);
				tmp_tobs       <= inp(1 * InputWidthPerBlock - 1 downto 0 * InputWidthPerBlock);
			else
				ctrl_reg_flags <= alignToLeftStdLogicVector(ctrl_reg_flags, current_threshold, reg_ctrl_num);
				tmp_tobs       <= alignToLeftTobArray(tmp_tobs, current_block, reg_ctrl_num);
			end if;
		end if;

	end process;

	process(clkFast_int)
	begin
		if rising_edge(clkFast_int) then
			toggle160 <= toggle40;
		end if;
	end process;

	process(clk40_int)
	begin
		if rising_edge(clk40_int) then
			toggle40 <= not toggle40;
		end if;
	end process;

	process(clk40_int)
	begin
		if rising_edge(clk40_int) then
			oup      <= alignToLeftTobArray(tmp_tobs, current_block, reg_ctrl_num);
			overflow <= overflowReg;
		end if;
	end process;

	resetClk    <= '1' when (toggle40 /= toggle160) else '0';
	overflowTmp <= '1' when ((overflowSum + unsigned('0' & ('0' & currentTobCount))) > InputWidthPerBlock) else '0';
	overflowReg <= overflowTmp;

end Behavioral;
