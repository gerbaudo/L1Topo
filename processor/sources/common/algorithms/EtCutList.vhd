------------------------------------------------------------------------------------------
-- Author/Modified by	: sebastian.artz@uni-mainz.de
-- Description			: 
-- Number of registers	: 
------------------------------------------------------------------------------------------

--------------
-- includes --
--------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use work.L1TopoDataTypes.all;
use work.L1TopoFunctions.all;

------------------------
-- entity declaration --
------------------------

entity EtCutList is
	generic(InputWidth : integer := 16
	);
	Port(ClockBus   : in  std_logic_vector(2 downto 0);
		 MinEt      : in  std_logic_vector(GenericEtBitWidth - 1 downto 0);
		 MinEta     : in  std_logic_vector(GenericAbsoluteEtaBitWidth - 1 downto 0);
		 MaxEta     : in  std_logic_vector(GenericAbsoluteEtaBitWidth - 1 downto 0);
		 --		 IsoMask    : std_logic_vector(ClusterIsolBitWidth - 1 downto 0);
		 TobArrayIn : in  TobArray(InputWidth - 1 downto 0);
		 Result     : out std_logic
	);
end EtCutList;

-----------------------------------------
-- behavioral description of algorithm --
-----------------------------------------

architecture Behavioral of EtCutList is
	signal cutFlag : std_logic_vector(InputWidth - 1 downto 0);

begin
	sel_eta_iso : process(MinEta, MaxEta, TobArrayIn) --IsoMask,
		variable resultBit : std_logic := '0';
	begin
		resultBit := '0';
		for i in 0 to InputWidth - 1 loop
			if (
				-- Select Eta range
				(is_in_eta_range_tob(TobArrayIn(i), MinEta, MaxEta)) and
				-- Select Et threshold
				(TobArrayIn(i).Et >= MinEt)
			) then
				resultBit := resultBit or '1';
			end if;
		end loop;
		Result <= resultBit;
	end process;

end Behavioral;