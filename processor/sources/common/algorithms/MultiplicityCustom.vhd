------------------------------------------------------------------------------------------
-- Author/Modified by	: manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de
-- Description			: applies Et cut on Tob list
-- Number of registers	: 0
------------------------------------------------------------------------------------------

--------------
-- includes --
--------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.L1TopoDataTypes.ALL;
use IEEE.numeric_std.all;
use work.L1TopoFunctions.ALL;

------------------------
-- entity declaration --
------------------------

entity MultiplicityCustom is
	generic(
		InputWidth    : integer := 1;   --number of input TOBs
		NumResultBits : integer := 1
	);
	Port(ClockBus   : std_logic_vector(2 downto 0);
		 Parameters : in  ParameterArray;
		 Tob        : in  TOBArray(InputWidth - 1 downto 0);
		 Results    : out std_logic_vector(NumResultBits - 1 downto 0);
		 Overflow   : out std_logic_vector(NumResultBits - 1 downto 0)
	);
end MultiplicityCustom;

-----------------------------------------
-- behavioral description of algorithm --
-----------------------------------------

architecture Behavioral of MultiplicityCustom is

	----------------------
	-- type definitions --
	----------------------
	--	type threshold_array is array (integer range <>) of std_logic_vector(GenericEtBitWidth - 1 downto 0);
	type MultiplicityArray is array (integer range <>) of std_logic_vector(MultiplicityCustomBitWidth - 1 downto 0);

	-------------
	-- signals --
	-------------
	-- parameters:
	signal MinEt               : std_logic_vector(GenericEtBitWidth - 1 downto 0); --threshold_array(NumResultBits - 1 downto 0);
	signal MinEta, MaxEta      : std_logic_vector(GenericEtaBitWidth - 1 downto 0);
	signal MinMultiplicity     : MultiplicityArray(NumResultBits - 1 downto 0);
	-- temporary results:
	signal tobs_over_threshold : std_logic_vector(InputWidth - 1 downto 0);
	signal tmp_mult            : std_logic_vector(MultiplicityCustomBitWidth - 1 downto 0);
	signal results_out         : std_logic_vector(NumResultBits - 1 downto 0);
	signal overflow_out        : std_logic_vector(NumResultBits - 1 downto 0);

begin

	-- read parameters

	MinEt  <= Parameters(0)(GenericEtBitWidth - 1 downto 0);
	MinEta <= Parameters(1)(GenericEtaBitWidth - 1 downto 0);
	MaxEta <= Parameters(2)(GenericEtaBitWidth - 1 downto 0);
	set_parameters : for i in 0 to (NumResultBits - 1) generate
		MinMultiplicity(i) <= Parameters(i + 3)(MultiplicityCustomBitWidth - 1 downto 0);
	end generate;

	genAcceptTemp : for i in 0 to InputWidth - 1 generate
		tobs_over_threshold(i) <= '1' when (Tob(i).Et > MinEt and is_in_eta_range_tob(Tob(i), minEta, maxEta)) else '0';
	end generate;

	tmp_mult <= std_logic_vector(to_unsigned(count_ones(tobs_over_threshold), MultiplicityCustomBitWidth));

	gen_results : for i in 0 to (NumResultBits - 1) generate
		results_out(i) <= '1' when (tmp_mult >= MinMultiplicity(i)) else '0';
	end generate;

	Results <= results_out;

	-----------------------------
	-- generate overflow bits  --
	-----------------------------

	genOverflows : for result in 0 to NumResultBits - 1 generate
		overflow_out(result) <= Tob(0).Overflow;
	end generate;

	Overflow <= overflow_out;

end Behavioral;