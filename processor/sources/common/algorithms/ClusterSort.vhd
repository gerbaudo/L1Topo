------------------------------------------------------------------------------------------
-- Author/Modified by	: manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de
-- Description			: applies isolation and eta cut to ClusterTOB list, sorts TOBs,
--						  converts them to GenericTOBs and returns short sorted list
-- Number of registers	: 2
------------------------------------------------------------------------------------------

--------------
-- includes --
--------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use work.L1TopoDataTypes.all;
use work.L1TopoFunctions.all;

------------------------
-- entity declaration --
------------------------

entity ClusterSort is
	generic(InputWidth         : integer := InputWidthEM; --number of input TOBs
		    InputWidth1stStage : integer := InputWidth1stStageSortEM; --number of TOBs in 1st stage
		    OutputWidth        : integer := OutputWidthSortEM; --number of sorted output TOBs
		    DoIsoCut           : integer := 1
	);
	Port(ClockBus        : in  std_logic_vector(2 downto 0);
		 Parameters      : in  ParameterArray;
		 ClusterTobArray : in  ClusterArray(InputWidth - 1 downto 0);
		 TobArrayOut     : out TOBArray(OutputWidth - 1 downto 0)
	);
end ClusterSort;

-----------------------------------------
-- behavioral description of algorithm --
-----------------------------------------

architecture Behavioral of ClusterSort is
begin
	genWithoutIsoCut : if (DoIsoCut = 0) generate
		genSortWithoutIsoCut : entity work.ClusterSortNoIso
			generic map(
				InputWidth         => InputWidth,
				InputWidth1stStage => InputWidth1stStage,
				OutputWidth        => OutputWidth
			)
			port map(
				ClockBus        => ClockBus,
				Parameters      => Parameters,
				ClusterTobArray => ClusterTobArray,
				TobArrayOut     => TobArrayOut
			);
	end generate;

	genWithIsoCut : if (DoIsoCut /= 0) generate
		genSortWithIsoCut : entity work.ClusterSortIso
			generic map(
				InputWidth         => InputWidth,
				InputWidth1stStage => InputWidth1stStage,
				OutputWidth        => OutputWidth
			)
			port map(
				ClockBus        => ClockBus,
				Parameters      => Parameters,
				ClusterTobArray => ClusterTobArray,
				TobArrayOut     => TobArrayOut
			);
	end generate;

end Behavioral;
