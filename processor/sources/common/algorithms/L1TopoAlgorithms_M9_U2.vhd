library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use work.l1topo_package.all;
use work.L1TopoDataTypes.all;

entity L1TopoAlgorithms_M9_U2 is
	Port(ClockBus     : in  std_logic_vector(2 downto 0);

		 EmTobArray   : in  ClusterArray(InputWidthEM - 1 downto 0);
		 TauTobArray  : in  ClusterArray(InputWidthTAU - 1 downto 0);
		 JetTobArray  : in  JetArray(InputWidthJET - 1 downto 0);
		 MuonTobArray : in  MuonArray(InputWidthMU - 1 downto 0);
		 MetTobArray  : in  MetArray(0 downto 0);
		 
		 Parameters   : in  ParameterSpace(NumberOfAlgorithms - 1 downto 0);
		 SortParameters     : in  ParameterSpace(NumberOfSortAlgorithms - 1 downto 0);
		 
		 Results      : out std_logic_vector(NumberOfResultBits - 1 downto 0);
		 Overflow     : out std_logic_vector(NumberOfResultBits - 1 downto 0)
	);
end L1TopoAlgorithms_M9_U2;

architecture Behavioral of L1TopoAlgorithms_M9_U2 is

-- Menu Name   : Topo_pp_vX
-- Menu Version: 1

-- Module/FPGA : 0/1
-- General configuration (<TopoConfig>):

  constant AlgoOffset0 : integer := 0;   --   2INVM999-2MU4ab
  constant AlgoOffset1 : integer := 1;   --   2INVM999-MU6ab-MU4ab
  constant AlgoOffset2 : integer := 2;   --   2INVM999-2MU6ab
  constant AlgoOffset3 : integer := 3;   --   4INVM8-2MU4ab
  constant AlgoOffset4 : integer := 4;   --   4INVM8-MU6ab-MU4ab
  constant AlgoOffset5 : integer := 5;   --   4INVM8-2MU6ab
  constant AlgoOffset6 : integer := 6;   --   2DR99-2MU4ab
  constant AlgoOffset11 : integer := 7;   --   5DETA99-5DPHI99-2MU4ab
  constant AlgoOffset12 : integer := 8;   --   5DETA99-5DPHI99-MU6ab-MU4ab
  constant AlgoOffset13 : integer := 9;   --   5DETA99-5DPHI99-2MU6ab
  constant AlgoOffset7 : integer := 10;   --   0DR10-MU10ab-MU6ab
  constant AlgoOffset8 : integer := 11;   --   2DR15-2MU4ab
  constant AlgoOffset9 : integer := 12;   --   2DR15-2MU6ab
  constant AlgoOffset15 : integer := 13;   --   0DR28-TAU20abi-TAU12abi
  constant AlgoOffset10 : integer := 14;   --   2DR15-MU6ab-MU4ab
  constant AlgoOffset14 : integer := 15;   --   0DR28-MU10ab-TAU12abi
 -- Ordered list of sorted TOBArrays:
  signal TAUabi  :    TOBArray ((OutputWidthSelectTAU - 1)  downto 0);
  signal EMs  :    TOBArray ((OutputWidthSortEM - 1)  downto 0);
  signal AJall  :    TOBArray ((InputWidthJET - 1)  downto 0);
  signal AJs  :    TOBArray ((OutputWidthSortJET - 1)  downto 0);
  signal Js  :    TOBArray ((OutputWidthSortJET - 1)  downto 0);
  signal MUab  :    TOBArray ((OutputWidthSelectMU - 1)  downto 0);

begin

sortalgo0 : entity work.ClusterSelect --  Algorithm name: TAUabi
  generic map (
     InputWidth => InputWidthTAU,
     InputWidth1stStage => InputWidth1stStageSelectTAU,
     OutputWidth => OutputWidthSelectTAU )
  port map (
     ClusterTobArray => TauTobArray,
     TobArrayOut =>  TAUabi,
     ClockBus => ClockBus,
     Parameters => SortParameters(0)
 );

sortalgo1 : entity work.MuonSelect --  Algorithm name: MUab
  generic map (
     InputWidth => InputWidthMU,
     InputWidth1stStage => InputWidth1stStageSelectMU,
     OutputWidth => OutputWidthSelectMU )
  port map (
  	 ClockBus => ClockBus,
     Parameters => SortParameters(1),
     MuonTobArray => MuonTobArray,
     TobArrayOut =>  MUab
 );

decisionalgo0 : entity work.InvariantMassInclusive1 --  Algorithm name: 2INVM999-2MU4ab
  generic map (
     InputWidth => OutputWidthSelectMU,
     MaxTob => OutputWidthSelectMU,
     RequireOneBarrel => 0,
     NumResultBits => 1 )
  port map (
     Tob => MUab,
     Results => Results(AlgoOffset0+0 downto AlgoOffset0), 
     Overflow => Overflow(AlgoOffset0+0 downto AlgoOffset0), 
     ClockBus => ClockBus,
     Parameters => Parameters(0)
 );

decisionalgo1 : entity work.InvariantMassInclusive1 --  Algorithm name: 2INVM999-MU6ab-MU4ab
  generic map (
     InputWidth => OutputWidthSelectMU,
     MaxTob => OutputWidthSelectMU,
     RequireOneBarrel => 0,
     NumResultBits => 1 )
  port map (
     Tob => MUab,
     Results => Results(AlgoOffset1+0 downto AlgoOffset1), 
     Overflow => Overflow(AlgoOffset1+0 downto AlgoOffset1), 
     ClockBus => ClockBus,
     Parameters => Parameters(1)
 );

decisionalgo2 : entity work.InvariantMassInclusive1 --  Algorithm name: 2INVM999-2MU6ab
  generic map (
     InputWidth => OutputWidthSelectMU,
     MaxTob => OutputWidthSelectMU,
     RequireOneBarrel => 0,
     NumResultBits => 1 )
  port map (
     Tob => MUab,
     Results => Results(AlgoOffset2+0 downto AlgoOffset2), 
     Overflow => Overflow(AlgoOffset2+0 downto AlgoOffset2), 
     ClockBus => ClockBus,
     Parameters => Parameters(2)
 );

decisionalgo3 : entity work.InvariantMassInclusive1 --  Algorithm name: 4INVM8-2MU4ab
  generic map (
     InputWidth => OutputWidthSelectMU,
     MaxTob => OutputWidthSelectMU,
     RequireOneBarrel => 0,
     NumResultBits => 1 )
  port map (
     Tob => MUab,
     Results => Results(AlgoOffset3+0 downto AlgoOffset3), 
     Overflow => Overflow(AlgoOffset3+0 downto AlgoOffset3), 
     ClockBus => ClockBus,
     Parameters => Parameters(3)
 );

decisionalgo4 : entity work.InvariantMassInclusive1 --  Algorithm name: 4INVM8-MU6ab-MU4ab
  generic map (
     InputWidth => OutputWidthSelectMU,
     MaxTob => OutputWidthSelectMU,
     RequireOneBarrel => 0,
     NumResultBits => 1 )
  port map (
     Tob => MUab,
     Results => Results(AlgoOffset4+0 downto AlgoOffset4), 
     Overflow => Overflow(AlgoOffset4+0 downto AlgoOffset4), 
     ClockBus => ClockBus,
     Parameters => Parameters(4)
 );

decisionalgo5 : entity work.InvariantMassInclusive1 --  Algorithm name: 4INVM8-2MU6ab
  generic map (
     InputWidth => OutputWidthSelectMU,
     MaxTob => OutputWidthSelectMU,
     RequireOneBarrel => 0,
     NumResultBits => 1 )
  port map (
     Tob => MUab,
     Results => Results(AlgoOffset5+0 downto AlgoOffset5), 
     Overflow => Overflow(AlgoOffset5+0 downto AlgoOffset5), 
     ClockBus => ClockBus,
     Parameters => Parameters(5)
 );

decisionalgo6 : entity work.DeltaRSqrIncl1 --  Algorithm name: 2DR99-2MU4ab
  generic map (
     InputWidth => OutputWidthSelectMU,
     MaxTob => OutputWidthSelectMU,
     RequireOneBarrel => 0,
     NumResultBits => 1 )
  port map (
     Tob => MUab,
     Results => Results(AlgoOffset6+0 downto AlgoOffset6), 
     Overflow => Overflow(AlgoOffset6+0 downto AlgoOffset6), 
     ClockBus => ClockBus,
     Parameters => Parameters(6)
 );

decisionalgo11 : entity work.DeltaEtaPhiIncl1 --  Algorithm name: 5DETA99-5DPHI99-2MU4ab
  generic map (
     NumResultBits => 1,
     InputWidth => OutputWidthSelectMU,
     MaxTob => OutputWidthSelectMU )
  port map (
     Tob => MUab,
     Results => Results(AlgoOffset11+0 downto AlgoOffset11), 
     Overflow => Overflow(AlgoOffset11+0 downto AlgoOffset11), 
     ClockBus => ClockBus,
     Parameters => Parameters(11)
 );

decisionalgo12 : entity work.DeltaEtaPhiIncl1 --  Algorithm name: 5DETA99-5DPHI99-MU6ab-MU4ab
  generic map (
     NumResultBits => 1,
     InputWidth => OutputWidthSelectMU,
     MaxTob => OutputWidthSelectMU )
  port map (
     Tob => MUab,
     Results => Results(AlgoOffset12+0 downto AlgoOffset12), 
     Overflow => Overflow(AlgoOffset12+0 downto AlgoOffset12), 
     ClockBus => ClockBus,
     Parameters => Parameters(12)
 );

decisionalgo13 : entity work.DeltaEtaPhiIncl1 --  Algorithm name: 5DETA99-5DPHI99-2MU6ab
  generic map (
     NumResultBits => 1,
     InputWidth => OutputWidthSelectMU,
     MaxTob => OutputWidthSelectMU )
  port map (
     Tob => MUab,
     Results => Results(AlgoOffset13+0 downto AlgoOffset13), 
     Overflow => Overflow(AlgoOffset13+0 downto AlgoOffset13), 
     ClockBus => ClockBus,
     Parameters => Parameters(13)
 );

decisionalgo7 : entity work.DeltaRSqrIncl1 --  Algorithm name: 0DR10-MU10ab-MU6ab
  generic map (
     InputWidth => OutputWidthSelectMU,
     MaxTob => OutputWidthSelectMU,
     RequireOneBarrel => 0,
     NumResultBits => 1 )
  port map (
     Tob => MUab,
     Results => Results(AlgoOffset7+0 downto AlgoOffset7), 
     Overflow => Overflow(AlgoOffset7+0 downto AlgoOffset7), 
     ClockBus => ClockBus,
     Parameters => Parameters(7)
 );

decisionalgo8 : entity work.DeltaRSqrIncl1 --  Algorithm name: 2DR15-2MU4ab
  generic map (
     InputWidth => OutputWidthSelectMU,
     MaxTob => OutputWidthSelectMU,
     RequireOneBarrel => 0,
     NumResultBits => 1 )
  port map (
     Tob => MUab,
     Results => Results(AlgoOffset8+0 downto AlgoOffset8), 
     Overflow => Overflow(AlgoOffset8+0 downto AlgoOffset8), 
     ClockBus => ClockBus,
     Parameters => Parameters(8)
 );

decisionalgo9 : entity work.DeltaRSqrIncl1 --  Algorithm name: 2DR15-2MU6ab
  generic map (
     InputWidth => OutputWidthSelectMU,
     MaxTob => OutputWidthSelectMU,
     RequireOneBarrel => 0,
     NumResultBits => 1 )
  port map (
     Tob => MUab,
     Results => Results(AlgoOffset9+0 downto AlgoOffset9), 
     Overflow => Overflow(AlgoOffset9+0 downto AlgoOffset9), 
     ClockBus => ClockBus,
     Parameters => Parameters(9)
 );

decisionalgo15 : entity work.DeltaRSqrIncl1 --  Algorithm name: 0DR28-TAU20abi-TAU12abi
  generic map (
     InputWidth => OutputWidthSelectTAU,
     MaxTob => OutputWidthSelectTAU,
     NumResultBits => 1 )
  port map (
     Tob => TAUabi,
     Results => Results(AlgoOffset15+0 downto AlgoOffset15), 
     Overflow => Overflow(AlgoOffset15+0 downto AlgoOffset15), 
     ClockBus => ClockBus,
     Parameters => Parameters(15)
 );

decisionalgo10 : entity work.DeltaRSqrIncl1 --  Algorithm name: 2DR15-MU6ab-MU4ab
  generic map (
     InputWidth => OutputWidthSelectMU,
     MaxTob => OutputWidthSelectMU,
     RequireOneBarrel => 0,
     NumResultBits => 1 )
  port map (
     Tob => MUab,
     Results => Results(AlgoOffset10+0 downto AlgoOffset10), 
     Overflow => Overflow(AlgoOffset10+0 downto AlgoOffset10), 
     ClockBus => ClockBus,
     Parameters => Parameters(10)
 );

decisionalgo14 : entity work.DeltaRSqrIncl2 --  Algorithm name: 0DR28-MU10ab-TAU12abi
  generic map (
     InputWidth1 => OutputWidthSelectMU,
     InputWidth2 => OutputWidthSelectTAU,
     MaxTob1 => OutputWidthSelectMU,
     MaxTob2 => OutputWidthSelectTAU,
     NumResultBits => 1 )
  port map (
     Tob1 => MUab,
     Tob2 => TAUabi,
     Results => Results(AlgoOffset14+0 downto AlgoOffset14), 
     Overflow => Overflow(AlgoOffset14+0 downto AlgoOffset14), 
     ClockBus => ClockBus,
     Parameters => Parameters(14)
 );

end Behavioral;