------------------------------------------------------------------------------------------
-- Author/Modified by	: sebastian.artz@uni-mainz.de
-- Description			: dummy algorithm. only forwards MetTob
-- Number of registers	: 0
------------------------------------------------------------------------------------------

--------------
-- includes --
--------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use work.L1TopoDataTypes.all;
use work.L1TopoFunctions.all;

------------------------
-- entity declaration --
------------------------

entity METNoSort is
	generic(InputWidth   : integer := InputWidthMET; --number of input missing energies (1)
		    OutputWidth  : integer := OutputWidthMET; --number of output missing energies (MET, MET_corrected)
		    NumRegisters : integer := 2 -- number of registers (0 or 2)
	);
	Port(ClockBus    : in  std_logic_vector(2 downto 0);
		 Parameters  : in  ParameterArray; --unused!
		 MetTobArray : in  MetArray(InputWidth - 1 downto 0);
		 TobArrayOut : out MetArray(OutputWidth - 1 downto 0)
	);
end METNoSort;

-----------------------------------------
-- behavioral description of algorithm --
-----------------------------------------

architecture Behavioral of METNoSort is
	signal MET_in, MET_reg1, MET_reg2 : MetArray(InputWidth - 1 downto 0);
	signal clock                      : std_logic;

begin
	clock <= ClockBus(0);

	MET_in <= MetTobArray;

	gen_reg : if (NumRegisters = 2) generate
		gen_reg1 : entity work.METTobRegister
			generic map(InputWidth => InputWidth)
			port map(clk     => clock,
				     enable  => '1',
				     reg_in  => MET_in,
				     reg_out => MET_reg1);

		gen_reg2 : entity work.METTobRegister
			generic map(InputWidth => InputWidth)
			port map(clk     => clock,
				     enable  => '1',
				     reg_in  => MET_reg1,
				     reg_out => MET_reg2);
	end generate;

	gen_no_reg : if (NumRegisters = 0) generate
		MET_reg2 <= MetTobArray;
	end generate;

	TobArrayOut <= MET_reg2;

end Behavioral;