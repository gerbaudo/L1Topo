------------------------------------------------------------------------------------------
-- Author/Modified by	: manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de
-- Description			: applies eta cut to JetTOB list, sorts TOBs,
--						  converts them to GenericTOBs and returns short sorted list
-- Number of registers	: 2
------------------------------------------------------------------------------------------

--------------
-- includes --
--------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use work.L1TopoDataTypes.all;
use work.L1TopoFunctions.all;

------------------------
-- entity declaration --
------------------------

entity JetSort is
	generic(InputWidth         : integer := 64; --number of input TOBs
		    InputWidth1stStage : integer := InputWidth1stStageSortJet; --number of TOBs in 1st stage
		    OutputWidth        : integer := 10; --number of sorted output TOBs
		    JetSize            : integer := DefaultJetSize; --jet Et bit width (1 = 9 bit, 2 = 10 b
		    DoEtaCut           : integer := 1
	);
	Port(ClockBus    : in  std_logic_vector(2 downto 0);
		 Parameters  : in  ParameterArray;
		 JetTobArray : in  JetArray(InputWidth - 1 downto 0);
		 TobArrayOut : out TOBArray(OutputWidth - 1 downto 0)
	--		 Overflow        : out std_logic
	);
end JetSort;

-----------------------------------------
-- behavioral description of algorithm --
-----------------------------------------

architecture Behavioral of JetSort is
begin
	genWithoutEtaCut : if (DoEtaCut = 0) generate
		genSortWithoutEtaCut : entity work.JetSortNoEta
			generic map(
				InputWidth         => InputWidth,
				InputWidth1stStage => InputWidth1stStage,
				OutputWidth        => OutputWidth,
				JetSize            => JetSize
			)
			port map(
				ClockBus    => ClockBus,
				Parameters  => Parameters,
				JetTobArray => JetTobArray,
				TobArrayOut => TobArrayOut
			);
	end generate;

	genWithEtaCut : if (DoEtaCut /= 0) generate
		genSortWithEtaCut : entity work.JetSortEta
			generic map(
				InputWidth         => InputWidth,
				InputWidth1stStage => InputWidth1stStage,
				OutputWidth        => OutputWidth,
				JetSize            => JetSize
			)
			port map(
				ClockBus    => ClockBus,
				Parameters  => Parameters,
				JetTobArray => JetTobArray,
				TobArrayOut => TobArrayOut
			);
	end generate;

end Behavioral;
