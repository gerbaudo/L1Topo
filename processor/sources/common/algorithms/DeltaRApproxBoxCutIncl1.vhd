------------------------------------------------------------------------------------------
-- Author/Modified by	: manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de
-- Description			: applies Et cut on TOB list; calculates delta eta and delta phi of all 
--						  TOB combinations in this list and applies approximated box cut
-- Number of registers	: 0
------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.L1TopoDataTypes.ALL;

entity DeltaRApproxBoxCutIncl1 is
	generic(InputWidth    : integer := 3;
		    NumResultBits : integer := 1
	);
	Port(ClockBus   : in  std_logic_vector(2 downto 0);
		 Parameters : in  ParameterArray; --! [threshold1, max_R, min_R, ...]
		 Tob        : in  TOBArray(InputWidth - 1 downto 0);
		 Results    : out std_logic_vector(NumResultBits - 1 downto 0);
		 Overflow   : out std_logic_vector(NumResultBits - 1 downto 0)
	);
end DeltaRApproxBoxCutIncl1;

architecture Behavioral of DeltaRApproxBoxCutIncl1 is
	type acceptarr is array (integer range <>) of std_logic;
	type acceptmat is array (integer range <>) of acceptarr(2 * InputWidth - 2 downto 0);
	type acceptmat_list is array (integer range <>) of acceptmat(2 * InputWidth - 2 downto 0);

	type deltaarr is array (integer range <>) of std_logic_vector(4 downto 0);
	type deltamat is array (integer range <>) of deltaarr(2 * InputWidth - 2 downto 0);

	type Rthreshold is array (integer range <>) of std_logic_vector(4 downto 0);

	signal accept_tmp  : acceptmat_list(NumResultBits - 1 downto 0);
	signal accept_tmp2 : acceptmat_list(NumResultBits - 1 downto 0);

	signal thr_flag : std_logic_vector(InputWidth - 1 downto 0);

	signal deltaEta : deltamat(2 * InputWidth - 2 downto 0);
	signal deltaPhi : deltamat(2 * InputWidth - 2 downto 0);

	signal MinEt1 : std_logic_vector(9 downto 0);
	signal MaxR   : Rthreshold(NumResultBits - 1 downto 0);
	signal MinR   : Rthreshold(NumResultBits - 1 downto 0);

	signal results_out  : std_logic_vector(NumResultBits - 1 downto 0);
	signal overflow_out : std_logic_vector(NumResultBits - 1 downto 0);

begin
	MinEt1 <= Parameters(0)(9 downto 0);

	genThrArr : for i in 0 to NumResultBits - 1 generate
		MinR(i) <= Parameters(i * 2 + 1)(4 downto 0);
		MaxR(i) <= Parameters(i * 2 + 2)(4 downto 0); --TODO: does this work with 5 bits?
	end generate;

	genThrFlag : for i in 0 to InputWidth - 1 generate
		thr_flag(i) <= '1' when Tob(i).Et > MinEt1 else '0';
	end generate;

	gen1 : for i in 0 to InputWidth - 1 generate
		gen2 : for j in 0 to InputWidth - 1 generate
			gen3 : if j < i generate
				dEta_inst : entity work.DeltaEtaCalc
					port map(
						eta1In      => Tob(i).Eta,
						eta2In      => Tob(j).Eta,
						deltaEtaOut => deltaEta(i)(j)
					);
				dPhi_inst : entity work.DeltaPhiCalc
					port map(phi1In      => Tob(i).Phi,
						     phi2In      => Tob(j).Phi,
						     deltaPhiOut => deltaPhi(i)(j));
			end generate;
		end generate;
	end generate;

	genResults : for k in 0 to NumResultBits - 1 generate
		gen1 : for i in 0 to InputWidth - 1 generate
			gen2 : for j in 0 to InputWidth - 1 generate
				gen3 : if j < i generate
					accept_tmp(k)(i)(j) <= '1' when ((deltaEta(i)(j) < MaxR(k)) AND (deltaEta(i)(j) > MinR(k) AND deltaPhi(i)(j) < MaxR(k)) AND (deltaPhi(i)(j) > MinR(k))) else '0';
				end generate;
				gen4 : if j >= i generate
					accept_tmp(k)(i)(j) <= '0';
				end generate;
			end generate;
		end generate;

		genAcceptTmp2a : for i in 0 to InputWidth - 1 generate
			genAcceptTmp2b : for j in 0 to InputWidth - 1 generate
				accept_tmp2(k)(i)(j) <= accept_tmp(k)(i)(j) AND thr_flag(i) AND thr_flag(j);
			end generate;
		end generate;

		genOutAccept1 : for i in 0 to InputWidth - 1 generate
			genx : for j in 0 to InputWidth - 2 generate
				accept_tmp2(k)(i)(InputWidth + j) <= accept_tmp2(k)(i)(2 * j) or accept_tmp2(k)(i)(2 * j + 1);
			end generate;
		end generate;

		genOutAccept2 : for i in 0 to InputWidth - 2 generate
			accept_tmp2(k)(InputWidth + i)(2 * InputWidth - 2) <= accept_tmp2(k)(2 * i)(2 * InputWidth - 2) or accept_tmp2(k)(2 * i + 1)(2 * InputWidth - 2);
		end generate;

		results_out(k) <= accept_tmp2(k)(2 * InputWidth - 2)(2 * InputWidth - 2);

	end generate;

	Results <= results_out;

	-----------------------------
	-- generate overflow bits  --
	-----------------------------
	genOverflows : for result in 0 to NumResultBits - 1 generate
		-- get overflow from input list
		overflow_out(result) <= Tob(0).Overflow;
	end generate;

	Overflow <= overflow_out;

end Behavioral;