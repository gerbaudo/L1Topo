------------------------------------------------------------------------------------------
-- Author/Modified by	: manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de
-- Description			: merges selected lists using a faster clock
------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.L1TopoDataTypes.all;
use work.L1TopoFunctions.all;

entity SelectMergeMuon is
	generic(InputWidthBlocks   : integer := 4;
		    InputWidthPerBlock : integer := 8;
		    OutputWidth        : integer := 6;
		    DoPtCut            : integer := 0
	);
	Port(clk40              : in  std_logic;
		 clkFast            : in  std_logic;
		 Parameters         : in  ParameterArray;
		 inp                : in  MuonArray(InputWidthPerBlock * (InputWidthBlocks) - 1 downto 0);
		 oup                : out MuonArray(OutputWidth - 1 downto 0);
		 select_pattern_out : out std_logic_vector(OutputWidth - 1 downto 0);
		 NumTobsOut         : out integer range 0 to OutputWidth;
		 overflow           : out std_logic
	);
end SelectMergeMuon;

architecture Behavioral of SelectMergeMuon is
	constant MaxNumOutputTob : integer := Maximum(OutputWidth, InputWidthPerBlock);

	type MuonInputArray is array (integer range <>) of MuonArray(InputWidthPerBlock * (InputWidthBlocks) - 1 downto 0);

	signal packedTOBs             : MuonArray(OutputWidth - 1 downto 0);
	signal ctrl_reg_flags         : std_logic_vector(OutputWidth - 1 downto 0);
	signal reg_ctrl_num           : integer range 0 to 15;
	signal clk40_int, clkFast_int : std_logic;
	signal inp_reg                : MuonInputArray(4 - 1 downto 0);
	signal cnt                    : integer range 0 to InputWidthBlocks := 3; -- 2 for 4 input lists
	constant cntStart             : integer range 0 to InputWidthBlocks := 1;
	signal toggle40               : std_logic                           := '0';
	signal toggle160              : std_logic                           := '0';
	signal resetClk               : std_logic                           := '0';

	signal tobSelectIn           : MuonArray(InputWidthPerBlock - 1 downto 0); --block of tobs to be processed in parallel
	signal tobSelectInCut        : MuonArray(InputWidthPerBlock - 1 downto 0);
	signal tobSelectOut          : MuonArray(InputWidthPerBlock - 1 downto 0);
	signal tobSelectOut_reg      : MuonArray(InputWidthPerBlock - 1 downto 0);
	signal tobSelectOut_resized  : MuonArray(MaxNumOutputTob - 1 downto 0);
	signal generic_in            : TOBArray(InputWidthPerBlock - 1 downto 0);
	signal selectBitsIn          : std_logic_vector(InputWidthPerBlock - 1 downto 0);
	signal selectBitsOut         : std_logic_vector(InputWidthPerBlock - 1 downto 0);
	signal selectBitsOut_reg     : std_logic_vector(InputWidthPerBlock - 1 downto 0);
	signal selectBitsOut_resized : std_logic_vector(MaxNumOutputTob - 1 downto 0);

	signal overflowSumFinal         : unsigned(6 - 1 downto 0) := (others => '0');
	signal currentTobCount          : std_logic_vector(4 - 1 downto 0);
	signal currentTobCountReg       : std_logic_vector(4 - 1 downto 0);
	signal overflowTmp, overflowReg : std_logic;
	signal MinEt                    : std_logic_vector(GenericEtBitWidth - 1 downto 0); --TODO: unused - include! --10
	signal MinEta                   : std_logic_vector(GenericAbsoluteEtaBitWidth - 1 downto 0); --6
	signal MaxEta                   : std_logic_vector(GenericAbsoluteEtaBitWidth - 1 downto 0); --6
	signal testSelectBits           : Std_logic_vector(OutputWidth - 1 downto 0);

begin
	clk40_int   <= clk40;
	clkFast_int <= clkFast;

	gen_param1 : if (DoPtCut = 1) generate
		-- read parameters
		MinEt  <= Parameters(0)(GenericEtBitWidth - 1 downto 0); --TODO: unused - include!
		MinEta <= Parameters(1)(GenericAbsoluteEtaBitWidth - 1 downto 0);
		MaxEta <= Parameters(2)(GenericAbsoluteEtaBitWidth - 1 downto 0);
	end generate;

	gen_param2 : if (DoPtCut = 0) generate
		-- read parameters
		MinEta <= Parameters(0)(GenericAbsoluteEtaBitWidth - 1 downto 0);
		MaxEta <= Parameters(1)(GenericAbsoluteEtaBitWidth - 1 downto 0);
	end generate;

	inp_reg(0) <= inp;

	gen_input_reg : for i in 2 downto 0 generate
		inp_reg(i + 1) <= inp_reg(i) when rising_edge(clkFast_int);
	end generate;

	tobSelectIn <= inp_reg(0)((0 + 1) * InputWidthPerBlock - 1 downto (0 + 0) * InputWidthPerBlock) when (cnt = 0) else inp_reg(1)((1 + 1) * InputWidthPerBlock - 1 downto (1 + 0) * InputWidthPerBlock) when (cnt = 1) else inp_reg(2)((2 + 1) * InputWidthPerBlock - 1 downto (2 +
				0) * InputWidthPerBlock) when (cnt = 2) else inp_reg(3)((3 + 1) * InputWidthPerBlock - 1 downto (3 + 0) * InputWidthPerBlock) when (cnt = 3) else (others => emptyMuonTOB);

	-- apply eta cut
	sel_eta_range : process(clkFast_int, tobSelectIn)
	begin
		for i in InputWidthPerBlock - 1 downto 0 loop
			generic_in(i) <= to_GenericTOB(tobSelectIn(i));
		end loop;

		if (rising_edge(clkFast_int)) then
			if (DoPtCut = 1) then
				for i in InputWidthPerBlock - 1 downto 0 loop
					if (is_in_eta_range_tob(generic_in(i), MinEta, MaxEta) and (unsigned(generic_in(i).Et) > unsigned(MinEt)) and (tobSelectIn(i).no_candidate = '0')
					) then
						tobSelectInCut(i) <= tobSelectIn(i);
						selectBitsIn(i)   <= '1';
					else
						tobSelectInCut(i) <= emptyMuonTOB;
						selectBitsIn(i)   <= '0';
					end if;
				end loop;
			else
				for i in InputWidthPerBlock - 1 downto 0 loop
					if (is_in_eta_range_tob(generic_in(i), MinEta, MaxEta) and (tobSelectIn(i).no_candidate = '0')) then
						tobSelectInCut(i) <= tobSelectIn(i);
						selectBitsIn(i)   <= '1';
					else
						tobSelectInCut(i) <= emptyMuonTOB;
						selectBitsIn(i)   <= '0';
					end if;
				end loop;
			end if;
		end if;
	end process;

	reg_ctrl_num <= reg_ctrl(ctrl_reg_flags);

	-- align tobs in block
	gen_tob_select : entity work.TobSelectMuon
		generic map(
			InputWidth  => InputWidthPerBlock,
			OutputWidth => InputWidthPerBlock
		)
		port map(
			Tob         => tobSelectInCut,
			cmp_res     => selectBitsIn,
			selTOBs     => tobSelectOut,
			end_cmp_res => selectBitsOut
		);

	count_ones : process(selectBitsIn, selectBitsOut_reg)
		variable sum : integer;
	begin
		currentTobCount <= std_logic_vector(to_unsigned(reg_ctrl(selectBitsOut_reg), currentTobCount'length));
	end process;

	gen_reg_count_overflow : entity work.GenericRegister
		generic map(
			reg_width => 4
		)
		port map(
			clk     => clkFast_int,
			enable  => '1',
			reg_in  => currentTobCount,
			reg_out => currentTobCountReg
		);

	tobSelectOut_reg <= tobSelectOut when (rising_edge(clkFast_int));

	gen_tobSelectResize1 : for i in MaxNumOutputTob - 1 downto InputWidthPerBlock generate
		tobSelectOut_resized(i)  <= emptyMuonTOB;
		selectBitsOut_resized(i) <= '0';
	end generate;

	gen_tobSelectResize2 : for i in InputWidthPerBlock - 1 downto 0 generate
		tobSelectOut_resized(i)  <= tobSelectOut_reg(i);
		selectBitsOut_resized(i) <= selectBitsOut_reg(i);
	end generate;

	selectBitsOut_reg <= selectBitsOut when (rising_edge(clkFast_int));

	fill_final_array : process(clkFast_int)
		variable overflowSum : unsigned(6 - 1 downto 0) := (others => '0');
	begin
		if rising_edge(clkFast_int) then
			if (resetClk = '1') then
				cnt <= cntStart;
			else
				if (cnt = InputWidthBlocks - 1) then
					cnt <= 0;
				else
					cnt <= cnt + 1;
				end if;
			end if;

			case (cnt) is
				when 1 =>
					overflowSum      := overflowSum + unsigned('0' & ('0' & currentTobCount));
					overflowSumFinal <= overflowSum;
				when 2 =>
					overflowSum      := unsigned('0' & ('0' & currentTobCount));
					overflowSumFinal <= overflowSumFinal;
				when others =>
					overflowSum      := overflowSum + unsigned('0' & ('0' & currentTobCount));
					overflowSumFinal <= overflowSumFinal;
			end case;

			if (cnt = 2) then
				packedTOBs     <= tobSelectOut_resized(OutputWidth - 1 downto 0);
				ctrl_reg_flags <= selectBitsOut_resized(OutputWidth - 1 downto 0);
			else
				if reg_ctrl_num /= OutputWidth then
					for reg_ctrl_num_2 in 0 to OutputWidth - 1 loop
						if (reg_ctrl_num = reg_ctrl_num_2) then
							ctrl_reg_flags(OutputWidth - 1 downto reg_ctrl_num_2) <= selectBitsOut_resized(OutputWidth - reg_ctrl_num_2 - 1 downto 0);
							packedTOBs(OutputWidth - 1 downto reg_ctrl_num_2)     <= tobSelectOut_resized(OutputWidth - reg_ctrl_num_2 - 1 downto 0);
							if (cnt = 1) then
								oup                <= tobSelectOut_resized(OutputWidth - reg_ctrl_num_2 - 1 downto 0) & packedTOBs(reg_ctrl_num_2 - 1 downto 0);
								select_pattern_out <= selectBitsOut_resized(OutputWidth - reg_ctrl_num_2 - 1 downto 0) & ctrl_reg_flags(reg_ctrl_num_2 - 1 downto 0);

								testSelectBits <= selectBitsOut_resized(OutputWidth - reg_ctrl_num_2 - 1 downto 0) & ctrl_reg_flags(reg_ctrl_num_2 - 1 downto 0);
								NumTobsOut     <= reg_ctrl(selectBitsOut_resized(OutputWidth - reg_ctrl_num_2 - 1 downto 0) & ctrl_reg_flags(reg_ctrl_num_2 - 1 downto 0));
							end if;
						end if;
					end loop;
				else
					if (cnt = 1) then
						oup                <= packedTOBs;
						select_pattern_out <= ctrl_reg_flags;
						NumTobsOut         <= OutputWidth;
					end if;
				end if;

			end if;
		end if;

	end process;

	process(clkFast_int)
	begin
		if rising_edge(clkFast_int) then
			toggle160 <= toggle40;
		end if;
	end process;

	process(clk40_int)
	begin
		if rising_edge(clk40_int) then
			toggle40 <= not toggle40;
		end if;
	end process;

	resetClk <= '1' when (toggle40 /= toggle160) else '0';

	overflowTmp <= '1' when (to_integer(overflowSumFinal) > OutputWidth) else '0';

	overflowReg <= overflowTmp;

	overflow <= overflowReg;

end Behavioral;
