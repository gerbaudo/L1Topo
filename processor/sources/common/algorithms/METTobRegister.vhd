------------------------------------------------------------------------------------------
-- Author/Modified by	: manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de
-- Description			: register for METTOB list
-- Number of registers	: 1
------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.L1TopoDataTypes.all;

entity METTobRegister is
	generic(InputWidth : integer := 16  -- Width of the register (in generic TOB)
	);
	Port(clk     : in  std_logic;
		 enable  : in  std_logic;
		 reg_in  : in  MetArray(InputWidth - 1 downto 0);
		 reg_out : out MetArray(InputWidth - 1 downto 0)
	);
end METTobRegister;

architecture Behavioral of METTobRegister is
	type Energy_arr is array (integer range <>) of std_logic_vector(METEnergyBitWidth - 1 downto 0);
	type overflowarr is array (integer range <>) of std_logic;

	signal reg_Ex_tmp, reg_Ey_tmp, reg_Et_tmp : Energy_arr(InputWidth - 1 downto 0);
	signal reg_overflow_tmp                   : overflowarr(InputWidth - 1 downto 0);

	signal reg_Ex, reg_Ey, reg_Et : Energy_arr(InputWidth - 1 downto 0);
	signal reg_overflow           : overflowarr(InputWidth - 1 downto 0);
	signal clock                  : std_logic;

begin
	clock <= clk;                       -- this signal is needed for the simulation

	genIn : for i in 0 to InputWidth - 1 generate
		reg_Ex_tmp(i)       <= reg_in(i).Ex;
		reg_Ey_tmp(i)       <= reg_in(i).Ey;
		reg_Et_tmp(i)       <= reg_in(i).Et;
		reg_overflow_tmp(i) <= reg_in(i).Overflow;
	end generate;

	process(clock)
	begin
		if (rising_edge(clock)) then
			if (enable = '1') then
				reg_Ex       <= reg_Ex_tmp;
				reg_Ey       <= reg_Ey_tmp;
				reg_Et       <= reg_Et_tmp;
				reg_overflow <= reg_overflow_tmp;
			end if;
		end if;
	end process;

	genOut : for i in 0 to InputWidth - 1 generate
		reg_out(i).Ex       <= reg_Ex(i);
		reg_out(i).Ey       <= reg_Ey(i);
		reg_out(i).Et       <= reg_Et(i);
		reg_out(i).Overflow <= reg_overflow(i);
	end generate;

end Behavioral;