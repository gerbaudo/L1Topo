----------------------------------------------------------------------------------
-- Company: ETAP university Mainz
-- Engineer: Michael Haft (mhaft@students.uni-mainz.de)
-- 
-- Design Name: simpleCone
-- Module Name: simpleCone - Behavioral
--
-- Create Date: 16.10.2015 15:03:07
-- Last Modification: 06.04.2016
-- Project Name: 
-- Target Devices: Virtex-7 xc7vx690tffg1927-3
-- Tool Versions: Vivado v2015.3 (64-bit) SW Build 1368829 on Mon Sep 28 20:06:39 MDT 2015 IP Build 1367837 on Mon Sep 28 08:56:14 MDT 2015
-- Description: This implements the simpleCone algorithm. 
-- 
-- Dependencies:    L1TopoDataTypes
--                  DeltaRSqrCalc
--                  ieee.std_logic_1164
--                  ieee.numeric_std
-- 
-- Revision:
-- Revision 0.9 - Beta
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

--------------
-- includes --
--------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.L1TopoDataTypes.all;
use work.DeltaRSqrCalc;

------------------------
-- entity declaration --
------------------------
entity SimpleCone is
    Generic (
            InputWidth              : integer := 10;
            NumResultBits           : integer := 1
            );
    Port ( 
    -- input
            Tob         : in TOBArray (InputWidth - 1 downto 0);
            Parameters  : in ParameterArray;
            ClockBus    : in STD_LOGIC_VECTOR (2 downto 0);
    -- ouput
            Results     : out STD_LOGIC_VECTOR(NumResultBits - 1 downto 0);
            Overflow    : out STD_LOGIC_VECTOR(NumResultBits - 1 downto 0)
            );
end SimpleCone;


-----------------------------------------
-- behavioral description of algorithm --
-----------------------------------------

architecture Behavioral of SimpleCone is

    ---------------
    -- constants --
    ---------------
    
    constant    EnergyTresholdBitWidth  : integer := 14;
    constant    MaxEnergyBitWidth       : integer := 14;
    constant    MaxDistanceBitWitdh     : integer := 15;
    constant    DeltaRBitWitdh          : integer := 8;
    
    constant    BC_TIME_8               : time    := 3.125 ns;
    
    ----------------------
    -- type definitions --
    ----------------------
    
    type deltaarrEta       is array (integer range <>) of std_logic_vector(GenericEtaBitWidth - 1 downto 0);
    type deltamatEta       is array (integer range <>) of deltaarrEta(InputWidth - 1 downto 0);
    
    type deltaarrPhi       is array (integer range <>) of std_logic_vector(GenericPhiBitWidth - 1 downto 0);
    type deltamatPhi       is array (integer range <>) of deltaarrPhi(InputWidth - 1 downto 0);
    
    type DistanceMatrix    is array (InputWidth - 1 downto 0, InputWidth - 1 downto 0) of std_logic_vector(MaxDistanceBitWitdh - 1 downto 0);
    
    type EnergyMatrix      is array (InputWidth - 1 downto 0, InputWidth - 1 downto 0) of std_logic_vector(GenericEtBitWidth - 1 downto 0);
    type EnergySumVector   is array (InputWidth - 1 downto 0) of std_logic_vector(MaxEnergyBitWidth - 1 downto 0);
    
    type sumArray            is array (natural range <>, natural range <>) of unsigned(MaxEnergyBitWidth + 1 - 1 downto 0);

    -------------
    -- signals --
    -------------

     -- parameters:
    signal MinEt           : std_logic_vector(GenericEtBitWidth - 1 downto 0);
    signal MinSumEt           : std_logic_vector(EnergyTresholdBitWidth - 1 downto 0);
    signal thresholdDeltaR          : std_logic_vector(DeltaRBitWitdh - 1 downto 0);
    signal MaxRSqr    : std_logic_vector((2 * DeltaRBitWitdh) - 1 downto 0);
    
     -- local values for calculating and temporary value storages
    signal inputTOBs       : TOBArray(InputWidth - 1 downto 0);

    signal deltaEta        : deltamatEta(InputWidth - 1 downto 0);
    signal deltaPhi        : deltamatPhi(InputWidth - 1 downto 0);

    signal distances       : DistanceMatrix;
    
    signal energies        : EnergyMatrix;
        
    signal tempSum         : sumArray(InputWidth - 1 downto 0, (2 * InputWidth) - 2 downto 0);
    signal energySum       : EnergySumVector;
    
    signal resultVector    : std_logic_vector(InputWidth - 1 downto 0);
    
begin

    ---------------------
    -- read parameters --
    ---------------------

    MinEt <= Parameters(0)(GenericEtBitWidth - 1 downto 0);
    MinSumEt <= Parameters(1)(EnergyTresholdBitWidth - 1 downto 0);
    MaxRSqr <= Parameters(2)(2*DeltaRBitWitdh - 1 downto 0);
    -- calculate the square of the Threshold form deltaR, so we done have to do the expensive calculation of the squareroot 
--    thresholdDeltaRSquare <= std_logic_vector(unsigned(thresholdDeltaR) * unsigned(thresholdDeltaR));

    ----------------------
    -- get input values --
    ----------------------
    
    inputTOBs <= Tob;

    ---------------------------------
    -- step 1: calculate distances --
    ---------------------------------
    
    calcDistanaceMatrixHorizontal : for i in 0 to InputWidth - 1 generate
        -- we only need an upper/lower triangular matrix because its symmetric
        calcDistanceMatrixVertical : for j in 0 to i generate
        
            -- calculate deltaEta for TOBs i and j
            dEta_inst : entity work.DeltaEtaCalc
                port map(
                    eta1In      => inputTOBs(i).Eta,
                    eta2In      => inputTOBs(j).Eta,
                    deltaEtaOut => deltaEta(i)(j)
                );

            -- calculate deltaPhi for TOBs i and j
            dPhi_inst : entity work.DeltaPhiCalc
                port map(
                    phi1In      => inputTOBs(i).Phi,
                    phi2In      => inputTOBs(j).Phi,
                    deltaPhiOut => deltaPhi(i)(j)
                );

            -- calculate deltaR² for TOBs i and j
            DeltaRSqrCalc_inst : entity work.DeltaRSqrCalc
                port map(
                    deltaEta  => deltaEta(i)(j),
                    deltaPhi  => deltaPhi(i)(j),
                    deltaRSqr => distances(i,j)
                );
        end generate; 
    end generate;
    
    -------------------------------------------------------
    -- step 2: Et cut, deltaR cut and fill energy matrix --
    -------------------------------------------------------
    -- compare deltaR and Et with thresholds for all TOB pairs, if passed _both_ thresholds fill actual energiematrixcell with Et else with 0
   compareAgainstThresholdsAndFillEnergyMatrix : process(distances, MaxRSqr, inputTOBs, MinEt) 
   begin
        for k in 0 to (InputWidth - 1) loop
            for l in 0 to (InputWidth - 1) loop
            
                -- apply Et Cut
                if (unsigned(inputTOBs(l).Et) > unsigned(MinEt)) then
                    if (k > l) then
                        -- apply deltaR Cut
                        if (unsigned(distances(k,l)) <= unsigned(MaxRSqr)) then
                            energies(k,l) <= inputTOBs(l).Et;
                        else
                            energies(k,l) <= (others => '0');
                        end if;
                    elsif (k = l) then
                            -- for this case we don't need the deltaR Cut, because it's the TOB compared to itself, so deltaR = 0
                            energies(k,l) <= inputTOBs(l).Et;
                    elsif (k < l) then
                        -- apply deltaR Cut
                        if (unsigned(distances(l,k)) <= unsigned(MaxRSqr)) then
                            energies(k,l) <= inputTOBs(l).Et;
                        else
                            energies(k,l) <= (others => '0');
                        end if;
                    end if;
                else
                    energies(k,l) <= (others => '0');
                end if;            
            end loop; 
        end loop;
    end process;
    
    -----------------------------
    -- step 3: sum up energies --
    -----------------------------
    generateEnergySums : for o in 0 to InputWidth - 1 generate
        genTmpSum : for m in 0 to InputWidth - 1 generate
            tempSum(o,m)(GenericEtBitWidth - 1 downto 0) <= unsigned(energies(o,m));
            tempSum(o,m)(MaxEnergyBitWidth downto GenericEtBitWidth) <= (others => '0');
        end generate;
            
        genSumTree : for n in 0 to InputWidth - 2 generate
            tempSum(o, InputWidth + n) <= tempSum(o, 2 * n) + tempSum(o, (2 * n) + 1);
        end generate;
        
        energySum(o) <= std_logic_vector(tempSum(o, (2 * InputWidth) - 2)(MaxEnergyBitWidth - 1 downto 0));
    end generate;
    
    ---------------------------------------------------------------------------
    -- step 4: check if energysums from step 3 greater then energy treshhold --
    ---------------------------------------------------------------------------
    compareSummedEnergiesAgainstThreshold : process(energySum, MinSumEt)
    begin
        for p in 0 to InputWidth - 1 loop
            if (unsigned(energySum(p)) > unsigned(MinSumEt)) then
                resultVector(p) <= '1';
            else 
                resultVector(p) <= '0';
            end if;
        end loop;
    end process;
    
    ---------------------------------------------------------
    -- step 5: "generate" overflow and return the results --
    ---------------------------------------------------------
    genOverflowAndResultBits : for result in 0 to NumResultBits - 1 generate
        -- get overflow from input list
        Overflow(result) <= inputTOBs(0).Overflow;
    end generate;

    -- or all entries from a vector of arbitrary length
    Results <= (others => '0') when resultVector = (resultVector'range => '0') else (others => '1');

end Behavioral;
