------------------------------------------------------------------------------------------
-- Author/Modified by	: manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de
-- Description			: applies Et cut on TOB list; calculates the transverse 
--						  mass squared for the list using MET and applies cut
-- Number of registers	: 0
------------------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use work.L1TopoDataTypes.ALL;
use work.L1TopoFunctions.all;

entity TransverseMassInclusive1 is
	generic(InputWidth    : integer := 25;
		    MaxTob        : integer := 0; -- number of TOBs which should be considered
		    NumResultBits : integer := 1
	);
	Port(ClockBus   : in  std_logic_vector(2 downto 0);
		 Parameters : in  ParameterArray;
		 Tob1       : in  TOBArray(InputWidth - 1 downto 0);
		 Tob2       : in  TOBArray(0 downto 0); -- MET Tob
		 Results    : out std_logic_vector(NumResultBits - 1 downto 0);
		 Overflow   : out std_logic_vector(NumResultBits - 1 downto 0)
	);
end TransverseMassInclusive1;

architecture Behavioral of TransverseMassInclusive1 is

	-- constants

	constant MaxCount : integer := getMaxCount(InputWidth, MaxTob);

	type etThreshArray is array (integer range <>) of std_logic_vector(GenericEtBitWidth - 1 downto 0);
	type thrAcceptArray is array (NumResultBits - 1 downto 0) of std_logic_vector(InputWidth - 1 downto 0);
	type Resultsarr is array (integer range <>) of std_logic_vector(TransverseMassSqrBitWidth - 1 downto 0);
	type Mthreshold is array (integer range <>) of std_logic_vector(TransverseMassSqrBitWidth - 1 downto 0);

	signal mass_sqr_results       : Resultsarr(InputWidth - 1 downto 0);
	signal results_out            : std_logic_vector(NumResultBits - 1 downto 0);
	signal overflow_out           : std_logic_vector(NumResultBits - 1 downto 0);
	signal MinEt1                 : etThreshArray(NumResultBits - 1 downto 0);
	signal MinEt2                 : etThreshArray(NumResultBits - 1 downto 0);
	signal MinTransvMSqr          : Mthreshold(NumResultBits - 1 downto 0);
	signal thraccept1, thraccept2 : thrAcceptArray;
	signal tob1_in                : TOBArray(InputWidth - 1 downto 0);
	signal tobMet_in              : GenericTOB;

begin
	tobMet_in <= Tob2(0);
	tob1_in   <= Tob1;

	genThrArr : for i in 0 to NumResultBits - 1 generate
		MinEt1(i)        <= Parameters(3 * i + 0)(GenericEtBitWidth - 1 downto 0);
		MinEt2(i)        <= Parameters(3 * i + 1)(GenericEtBitWidth - 1 downto 0);
		MinTransvMSqr(i) <= Parameters(3 * i + 2)(TransverseMassSqrBitWidth - 1 downto 0); --convert 31 to 34 bit (3 more digits, after decimal)
	end generate;

	min_tob_Et : process(tob1_in, tobMet_in, MinEt1, MinEt2)
	begin
		for k in 0 to (NumResultBits - 1) loop
			for i in 0 to (maxCount - 1) loop
				if (tob1_in(i).Et > MinEt1(k)) then
					thraccept1(k)(i) <= '1';
				else
					thraccept1(k)(i) <= '0';
				end if;
			end loop;
			if (tobMet_in.Et > MinEt2(k)) then
				thraccept2(k)(0) <= '1';
			else
				thraccept2(k)(0) <= '0';
			end if;
		end loop;
	end process;

	gen1 : for i in 0 to maxCount - 1 generate
		Transverse_mass_inst : entity work.TransverseMassCalc
			port map(phi1     => tob1_in(i).Phi,
				     METPhi   => tobMet_in.Phi,
				     energy1  => tob1_in(i).Et,
				     MET      => tobMet_in.Et,
				     mass_sqr => mass_sqr_results(i)
			);
	end generate;

	compare_thresholds : process(mass_sqr_results, thraccept1, thraccept2, MinTransvMSqr) -- Compare delta eta with thresholds for all TOB pairs -- TODO: BUGGY?
		variable hits : std_logic_vector(NumResultBits - 1 downto 0);
	begin
		hits := (others => '0');
		for i in 0 to (maxCount - 1) loop -- loop over TOBs i
			for k in 0 to NumResultBits - 1 loop -- loop over thresholds
				if ((unsigned(mass_sqr_results(i)) >= unsigned(MinTransvMSqr(k))) and (thraccept1(k)(i) = '1') and (thraccept2(k)(0) = '1')) then
					hits(k) := '1';
				end if;
			end loop;
		end loop;
		results_out <= hits;
	end process;

	Results <= results_out;

	-----------------------------
	-- generate overflow bits  --
	-----------------------------
	genOverflows : for result in 0 to NumResultBits - 1 generate
		-- get overflow from input list
		overflow_out(result) <= Tob1(0).Overflow or Tob2(0).Overflow;
	end generate;

	Overflow <= overflow_out;

end Behavioral;