------------------------------------------------------------------------------------------
-- Author/Modified by	: manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de
-- Description			: applies Et cut on two TOB lists; calculates delta eta and delta phi of all 
--						  TOB combinations and applies rSquare cut
-- Number of registers	: 0
------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use work.L1TopoDataTypes.ALL;
use work.L1TopoFunctions.ALL;

entity DeltaRSqrIncl2 is
	generic(InputWidth1   : integer := 9;
		    InputWidth2   : integer := 9;
		    MaxTob1       : integer := 0; -- number of TOBs which should be considered
		    MaxTob2       : integer := 0; -- number of TOBs which should be considered
		    NumResultBits : integer := 1
	);
	Port(ClockBus   : std_logic_vector(2 downto 0);
		 Parameters : in  ParameterArray;
		 Tob1       : in  TOBArray(InputWidth1 - 1 downto 0);
		 Tob2       : in  TOBArray(InputWidth2 - 1 downto 0);
		 Results    : out std_logic_vector(NumResultBits - 1 downto 0);
		 Overflow   : out std_logic_vector(NumResultBits - 1 downto 0)
	);
end DeltaRSqrIncl2;

architecture Behavioral of DeltaRSqrIncl2 is

	-- constants

	constant MaxCount1 : integer := getMaxCount(InputWidth1, MaxTob1);
	constant MaxCount2 : integer := getMaxCount(InputWidth2, MaxTob2);

	type deltaarrEta is array (integer range <>) of std_logic_vector(GenericEtaBitWidth - 1 downto 0);
	type deltaarrPhi is array (integer range <>) of std_logic_vector(GenericPhiBitWidth - 1 downto 0);
	type deltaarr11 is array (integer range <>) of std_logic_vector(2 * GenericEtaBitWidth downto 0);
	type deltamatEta is array (integer range <>) of deltaarrEta(InputWidth1 - 1 downto 0);
	type deltamatPhi is array (integer range <>) of deltaarrPhi(InputWidth1 - 1 downto 0);
	type deltamatRSqr is array (integer range <>) of deltaarr11(InputWidth1 - 1 downto 0);
	type thrAcceptArray1 is array (NumResultBits - 1 downto 0) of std_logic_vector(InputWidth1 - 1 downto 0);
	type thrAcceptArray2 is array (NumResultBits - 1 downto 0) of std_logic_vector(InputWidth2 - 1 downto 0);
	type etThreshArray is array (NumResultBits - 1 downto 0) of std_logic_vector(GenericEtBitWidth - 1 downto 0);

	type RSqrThreshold is array (integer range <>) of std_logic_vector(2 * GenericEtaBitWidth downto 0);

	signal thr_flag1 : thrAcceptArray1;
	signal thr_flag2 : thrAcceptArray2;

	signal deltaEta  : deltamatEta(InputWidth1 - 1 downto 0);
	signal deltaPhi  : deltamatPhi(InputWidth1 - 1 downto 0);
	signal deltaRSqr : deltamatRSqr(InputWidth1 - 1 downto 0);

	signal MinEt1  : etThreshArray;
	signal MinEt2  : etThreshArray;
	signal MaxRSqr : RSqrThreshold(NumResultBits - 1 downto 0);
	signal MinRSqr : RSqrThreshold(NumResultBits - 1 downto 0);

	signal results_out  : std_logic_vector(NumResultBits - 1 downto 0);
	signal overflow_out : std_logic_vector(NumResultBits - 1 downto 0);

begin
	genThrArr : for i in 0 to NumResultBits - 1 generate
		MinEt1(i)  <= Parameters(i * 4 + 0)(GenericEtBitWidth - 1 downto 0);
		MinEt2(i)  <= Parameters(i * 4 + 1)(GenericEtBitWidth - 1 downto 0);
		MinRSqr(i) <= Parameters(i * 4 + 2)(2 * GenericEtaBitWidth downto 0);
		MaxRSqr(i) <= Parameters(i * 4 + 3)(2 * GenericEtaBitWidth downto 0);
	end generate;

	gen_parameter_sets : for k in 0 to (NumResultBits - 1) generate
		genThrFlag1 : for i in 0 to MaxCount1 - 1 generate
			thr_flag1(k)(i) <= '1' when Tob1(i).Et > MinEt1(k) else '0';
		end generate;

		genThrFlag2 : for i in 0 to MaxCount2 - 1 generate
			thr_flag2(k)(i) <= '1' when Tob2(i).Et > MinEt2(k) else '0';
		end generate;
	end generate;

	gen1 : for i in 0 to MaxCount1 - 1 generate
		gen2 : for j in 0 to MaxCount2 - 1 generate
			dEta_inst : entity work.DeltaEtaCalc
				port map(
					eta1In      => Tob1(i).Eta,
					eta2In      => Tob2(j).Eta,
					deltaEtaOut => deltaEta(i)(j)
				);
			dPhi_inst : entity work.DeltaPhiCalc
				port map(phi1In      => Tob1(i).Phi,
					     phi2In      => Tob2(j).Phi,
					     deltaPhiOut => deltaPhi(i)(j));
			DeltaRSqrCalc_inst : entity work.DeltaRSqrCalc
				Port map(deltaEta  => deltaEta(i)(j),
					     deltaPhi  => deltaPhi(i)(j),
					     deltaRSqr => deltaRSqr(i)(j)
				);
		end generate;
	end generate;

	compare_thresholds : process(deltaRSqr, MinRSqr, MaxRSqr, thr_flag1, thr_flag2)
		variable hits : std_logic_vector(NumResultBits - 1 downto 0);
	begin
		hits := (others => '0');
		for i in 0 to (maxCount1 - 1) loop -- loop over TOBs i and j
			for j in 0 to (maxCount2 - 1) loop
				for k in 0 to NumResultBits - 1 loop -- loop over thresholds
					if (((unsigned(deltaRSqr(i)(j)) <= unsigned(MaxRSqr(k))) AND (unsigned(deltaRSqr(i)(j)) >= unsigned(MinRSqr(k)))) and (thr_flag1(k)(i) = '1') and (thr_flag2(k)(j) = '1')) then
						hits(k) := '1';
					end if;
				end loop;
			end loop;
		end loop;
		results_out <= hits;
	end process;

	Results <= results_out;

	-----------------------------
	-- generate overflow bits  --
	-----------------------------
	genOverflows : for result in 0 to NumResultBits - 1 generate
		-- get overflow from input list
		overflow_out(result) <= Tob1(0).Overflow or Tob2(0).Overflow;
	end generate;

	Overflow <= overflow_out;

end Behavioral;
