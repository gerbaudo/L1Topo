------------------------------------------------------------------------------------------
-- Author/Modified by	: manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de, 
--                        aschulte@cern.ch
-- Description			: DisambiguationDR: Checks if dRSqr > min_drSqr and <= max_drSqr
-- Number of registers	: 0 
------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.L1TopoDataTypes.all;
use work.L1TopoFunctions.all;

entity DisambiguationDRIncl2 is
	generic(
		InputWidth1   : integer := 8;
		InputWidth2   : integer := 8;
		MaxTob1       : integer := 0;   -- number of TOBs which should be considered
		MaxTob2       : integer := 0;   -- number of TOBs which should be considered
		NumResultBits : integer := 1;   -- Number of result bits
		ClusterOnly   : integer := 0    -- "0": checks for deltaRSqr > DisambDRSqr, "1": checks for identical coordinates
	);
	port(
		Tob1       : in  TOBArray(InputWidth1 - 1 downto 0); -- Two arrays of TOBs
		Tob2       : in  TOBArray(InputWidth2 - 1 downto 0);
		Parameters : in  ParameterArray;
		ClockBus   : in  std_logic_vector(2 downto 0);
		Results    : out std_logic_vector(NumResultBits - 1 downto 0);
		Overflow   : out std_logic_vector(NumResultBits - 1 downto 0)
	);
end DisambiguationDRIncl2;

architecture Behavioral of DisambiguationDRIncl2 is
begin
	DisambiguationIncl2_inst : entity work.DisambiguationIncl2
		generic map(
			InputWidth1   => InputWidth1,
			InputWidth2   => InputWidth2,
			MaxTob1       => MaxTob1,
			MaxTob2       => MaxTob2,
			NumResultBits => NumResultBits,
			ClusterOnly   => ClusterOnly,
			ApplyDR       => 1
		)
		port map(
			Tob1       => Tob1,
			Tob2       => Tob2,
			Parameters => Parameters,
			ClockBus   => ClockBus,
			Results    => Results,
			Overflow   => Overflow
		);

end Behavioral;
