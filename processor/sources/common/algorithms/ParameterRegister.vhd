------------------------------------------------------------------------------------------
-- Author/Modified by	: manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de
-- Description			: (deprecated) register for parameters
-- Number of registers	: 1
------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.L1TopoDataTypes.all;

entity ParameterRegister is
	port(clk     : in  std_logic;
		 enable  : in  std_logic;
		 reg_in  : in  ParameterArray;
		 reg_out : out ParameterArray
	);
end ParameterRegister;

architecture behavioral of ParameterRegister is
	signal sr : ParameterArray;
	signal clock : std_logic;

begin

clock <= clk;	-- this signal is needed for the simulation

	process(clock)
	begin
		if (rising_edge(clock)) then
			if (enable = '1') then
				sr <= reg_in;
			end if;
		end if;
	end process;

	reg_out <= sr;

end behavioral;