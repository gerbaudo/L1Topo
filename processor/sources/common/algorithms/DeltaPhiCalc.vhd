------------------------------------------------------------------------------------------
-- Author/Modified by	: manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de
-- Description			: calculates delta phi for two TOBs
-- Number of registers	: 0
------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.L1TopoDataTypes.all;

entity DeltaPhiCalc is
	port(
		phi1In       : in  std_logic_vector(GenericPhiBitWidth -1 downto 0);
		phi2In       : in  std_logic_vector(GenericPhiBitWidth -1 downto 0);
		deltaPhiOut   : out std_logic_vector(GenericPhiBitWidth -1 downto 0)
	);
end DeltaPhiCalc;

library ieee;
use ieee.numeric_std.all;
architecture Behavioral of DeltaPhiCalc is

	signal phi1, phi2, deltaPhi	: std_logic_vector(GenericPhiBitWidth -1 downto 0);

begin     

phi1 <= phi1In;
phi2 <= phi2In;

calcDeltaPhi : process(phi1, phi2)
	variable delta : unsigned(GenericPhiBitWidth - 1 downto 0);
begin
	if unsigned(phi1) > unsigned(phi2) then
		delta := unsigned(phi1) - unsigned(phi2);
	else
		delta := unsigned(phi2) - unsigned(phi1);
	end if;
	if delta > 32 then
		delta := 64 - delta;
	end if;
	deltaPhi <= std_logic_vector(delta);
end process;
	
	deltaPhiOut <= deltaPhi;

end Behavioral;