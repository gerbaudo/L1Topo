------------------------------------------------------------------------------------------
-- Author/Modified by	: manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de
-- Description			: applies Et cut on TOB list; calculates delta phi of all 
--						  TOB combinations in this list and applies MinDeltaPhi cut
-- Number of registers	: 0
-- behavioral simulation : ok
------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.L1TopoFunctions.all;
use work.L1TopoDataTypes.all;

entity DeltaPhiIncl1 is
	generic(
		InputWidth    : integer := 8;
		MaxTob		  : integer := 0;	-- number of TOBs which should be considered
		NumResultBits : integer := 1       -- Number of result bits
	);
	port(
		Tob        : in  TOBArray(InputWidth - 1 downto 0); -- Single array of TOBs (same type)
		Parameters : in  ParameterArray;
		ClockBus   : in  std_logic_vector(2 downto 0);
		Results    : out std_logic_vector(NumResultBits - 1 downto 0);
		Overflow   : out std_logic_vector(NumResultBits - 1 downto 0)
	);
end DeltaPhiIncl1;

architecture Behavioral of DeltaPhiIncl1 is

	-- constants

	constant maxCount : integer := getMaxCount(InputWidth, MaxTob);

	-- Type declarations

	type dphi is array (maxCount - 1 downto 1) of std_logic_vector(GenericPhiBitWidth - 1 downto 0);
	type dphiarray is array (maxCount - 2 downto 0) of dphi;
	type phiThreshArray is array (NumResultBits - 1 downto 0) of std_logic_vector(GenericPhiBitWidth - 1 downto 0);
	type etThreshArray is array (NumResultBits - 1 downto 0) of std_logic_vector(GenericEtBitWidth - 1 downto 0);
	type thrAcceptArray is array (NumResultBits - 1 downto 0) of std_logic_vector(maxCount - 1 downto 0);

	-- Internal signal declarations

	signal deltaPhi                 : dphiarray;
	signal MinDeltaPhi, MaxDeltaPhi : phiThreshArray;
	signal MinEt1, MinEt2           : etThreshArray;
	signal thraccept1               : thrAcceptArray;
	signal thraccept2               : thrAcceptArray;
	
    signal results_out  : std_logic_vector(NumResultBits - 1 downto 0);
    signal overflow_out : std_logic_vector(NumResultBits - 1 downto 0);
    signal tob_in       : TOBArray(InputWidth - 1 downto 0);
    signal clock        : std_logic;

begin

	-- Behavioral

	clock <= ClockBus(0);
	tob_in <= Tob;
	
	genParameters : for i in 0 to (NumResultBits - 1) generate -- Extract delta phi cuts
		MinEt1(i) <= Parameters(0 + (4 * i))(GenericEtBitWidth - 1 downto 0);
		MinEt2(i) <= Parameters(1 + (4 * i))(GenericEtBitWidth - 1 downto 0);
		MinDeltaPhi(i)  <= Parameters(2 + (4 * i))(GenericPhiBitWidth-1 downto 0);
		MaxDeltaPhi(i) <= Parameters(3 + (4 * i))(GenericPhiBitWidth-1 downto 0);
	end generate;

	min_tob_Et : process(tob_in, MinEt1, MinEt2) -- Flag TOBs with Et over threshold
	begin
		for k in 0 to (NumResultBits - 1) loop
			for i in 0 to (maxCount - 1) loop
				if (tob_in(i).Et > MinEt1(k)) then
					thraccept1(k)(i) <= '1';
				else
					thraccept1(k)(i) <= '0';
				end if;
			end loop;
			for i in 0 to (maxCount - 1) loop
				if (tob_in(i).Et > MinEt2(k)) then
					thraccept2(k)(i) <= '1';
				else
					thraccept2(k)(i) <= '0';
				end if;
			end loop;
		end loop;
	end process;
	
	deltaPhi_calc1 : for i in 0 to (maxCount - 2) generate
		deltaPhi_calc2 : for j in (i + 1) to (maxCount - 1) generate
			dphiCalc_inst : entity work.DeltaPhiCalc
				port map(
					phi1In      => tob_in(i).phi,
					phi2In      => tob_in(j).phi,
					deltaPhiOut => deltaPhi(i)(j)
				);
		end generate;
	end generate;

	compare_thresholds : process(deltaPhi, thraccept1, thraccept2, MinDeltaPhi, MaxDeltaPhi) -- Compare delta phi with thresholds for all TOB pairs
		variable hits : std_logic_vector(NumResultBits - 1 downto 0);
	begin
		hits := (others => '0');
		for i in 0 to (maxCount - 2) loop -- loop over TOBs i and j
			for j in (i + 1) to (maxCount - 1) loop
				for k in 0 to NumResultBits - 1 loop -- loop over thresholds
					if ((unsigned(deltaPhi(i)(j)) >= unsigned(MinDeltaPhi(k)))
						and (unsigned(deltaPhi(i)(j)) <= unsigned(MaxDeltaPhi(k)))
						and (((thraccept1(k)(i) = '1') and (thraccept2(k)(j) = '1')) or ((thraccept1(k)(j) = '1') and (thraccept2(k)(i) = '1')))
					) then
						hits(k) := '1';
					end if;
				end loop;
			end loop;
		end loop;
		results_out <= hits;
	end process;
	
	Results <= results_out;
	
	-----------------------------
	-- generate overflow bits  --
	-----------------------------
	genOverflows : for result in 0 to NumResultBits - 1 generate
		-- get overflow from input list
		overflow_out(result) <= Tob(0).Overflow;
	end generate;
	
	Overflow <= overflow_out;

end Behavioral;