------------------------------------------------------------------------------------------
-- Author/Modified by	: manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de
-- Description			: register for std_logic_vector
-- Number of registers	: 1
------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.L1TopoDataTypes.all;

entity StdLogicRegister is
	port(clk     : in  std_logic;
		 enable  : in  std_logic;
		 reg_in  : in  std_logic;
		 reg_out : out std_logic
	);
end StdLogicRegister;

architecture behavioral of StdLogicRegister is
	signal sr, tmpBit : std_logic;
	signal clock      : std_logic;

begin
	clock  <= clk;
	tmpBit <= reg_in;

	process(clock)
	begin
		if (rising_edge(clock)) then
			if (enable = '1') then
				sr <= tmpBit;
			end if;
		end if;
	end process;

	reg_out <= sr;

end behavioral;