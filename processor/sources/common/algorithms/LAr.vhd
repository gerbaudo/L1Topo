------------------------------------------------------------------------------------------
-- Author/Modified by	: manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de
-- Description			: applies Et cut on TOB list; calculates delta phi of all 
--						  TOB combinations in this list and applies MinDeltaPhi cut
-- Number of registers	: 0
------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
--use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use work.L1TopoFunctions.all;
use work.L1TopoDataTypes.all;

entity LAr is
	generic(
		InputWidth    : integer := 8;
		MaxTob        : integer := 0;   -- number of TOBs which should be considered
		NumResultBits : integer := 1    -- Number of result bits
	);
	port(
		Tob        : in  TOBArray(InputWidth - 1 downto 0); -- Single array of TOBs (same type)
		Parameters : in  ParameterArray;
		ClockBus   : in  std_logic_vector(2 downto 0);
		Results    : out std_logic_vector(NumResultBits - 1 downto 0);
		Overflow   : out std_logic_vector(NumResultBits - 1 downto 0)
	);
end LAr;

architecture Behavioral of Lar is

	-- constants

	constant maxCount : integer := getMaxCount(InputWidth, MaxTob);

	-- Type declarations

	type phiThreshArray is array (NumResultBits - 1 downto 0) of std_logic_vector(GenericPhiBitWidth - 1 downto 0);
	type etaThreshArray is array (NumResultBits - 1 downto 0) of std_logic_vector(GenericEtaBitWidth - 1 downto 0);
	type etThreshArray is array (NumResultBits - 1 downto 0) of std_logic_vector(GenericEtBitWidth - 1 downto 0);
	type thrAcceptArray is array (NumResultBits - 1 downto 0) of std_logic_vector(maxCount - 1 downto 0);

	-- Internal signal declarations

	signal MinPhi, MaxPhi : phiThreshArray;
	signal MinEta, MaxEta : etaThreshArray;
	signal MinEt          : etThreshArray;
	signal thraccept1     : thrAcceptArray;

	signal results_out  : std_logic_vector(NumResultBits - 1 downto 0);
	signal overflow_out : std_logic_vector(NumResultBits - 1 downto 0);
	signal tob_in       : TOBArray(InputWidth - 1 downto 0);
	signal clock        : std_logic;

begin
	clock  <= ClockBus(0);
	tob_in <= Tob;

	genParameters : for i in 0 to (NumResultBits - 1) generate -- Extract delta phi cuts
		MinEt(i)  <= Parameters(0 + (5 * i))(GenericEtBitWidth - 1 downto 0);
		MinEta(i) <= Parameters(1 + (5 * i))(GenericEtaBitWidth - 1 downto 0);
		MaxEta(i) <= Parameters(2 + (5 * i))(GenericEtaBitWidth - 1 downto 0);
		MinPhi(i) <= Parameters(3 + (5 * i))(GenericPhiBitWidth - 1 downto 0);
		MaxPhi(i) <= Parameters(4 + (5 * i))(GenericPhiBitWidth - 1 downto 0);
	end generate;

	min_tob_Et : process(tob_in, MinEt) -- Flag TOBs with Et over threshold
	begin
		for k in 0 to (NumResultBits - 1) loop
			for i in 0 to (maxCount - 1) loop
				if (unsigned(tob_in(i).Et) > unsigned(MinEt(k))) then
					thraccept1(k)(i) <= '1';
				else
					thraccept1(k)(i) <= '0';
				end if;
			end loop;
		end loop;
	end process;

	compare_thresholds : process(thraccept1, tob_in, MinPhi, MinEta, MaxPhi, MaxEta) -- Compare delta phi with thresholds for all TOB pairs
		variable hits : std_logic_vector(NumResultBits - 1 downto 0);
	begin
		hits := (others => '0');
		for i in 0 to (maxCount - 1) loop -- loop over TOBs i and j
			for k in 0 to NumResultBits - 1 loop -- loop over thresholds
				if (thraccept1(k)(i) = '1' AND (unsigned(tob_in(i).Phi) > unsigned(MinPhi(k)) AND unsigned(tob_in(i).Phi) < unsigned(MaxPhi(k))) AND (signed(tob_in(i).Eta) > signed(MinEta(k)) AND signed(tob_in(i).Eta) < signed(MaxEta(k)))) then
					hits(k) := '1';
				end if;
			end loop;
		end loop;
		results_out <= hits;
	end process;

	Results <= results_out;

	-----------------------------
	-- generate overflow bits  --
	-----------------------------
	genOverflows : for result in 0 to NumResultBits - 1 generate
		-- get overflow from input list
		overflow_out(result) <= Tob(0).Overflow;
	end generate;

	Overflow <= overflow_out;

end Behavioral;