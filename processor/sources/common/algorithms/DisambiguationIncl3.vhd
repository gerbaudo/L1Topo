------------------------------------------------------------------------------------------
-- Author/Modified by	: manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de
-- Description			: Checks if coordinates between ClusterTOBs at Tob1,Tob2 are
--                        identical + DR of each of them with the third TOB type
-- Number of registers	: 0
-- Simulation: ok 
-- automatisches Testen: 
------------------------------------------------------------------------------------------

--TODO: prüfe 3 bedingungen: over threshold + cluster nicht identische koordinaten + 3.tob dR > parameter

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.L1TopoDataTypes.ALL;
use work.L1TopoFunctions.all;

entity DisambiguationIncl3 is
	generic(InputWidth1   : integer := 5; --EM or TAU (possible DR applied)
		    InputWidth2   : integer := 3; --TAU (possible DR applied)
		    InputWidth3   : integer := 3; --here Jet !
		    MaxTob1       : integer := 0; -- number of TOBs which should be considered
		    MaxTob2       : integer := 0; -- number of TOBs which should be considered
		    MaxTob3       : integer := 0; -- number of TOBs which should be considered
		    NumResultBits : integer := 1;
		    ApplyDR       : integer := 0
	);
	Port(ClockBus   : std_logic_vector(2 downto 0);
		 Parameters : in  ParameterArray; --! [threshold1, threshold2, threshold3, min_EtaPhi, ...]
		 Tob1       : in  TOBArray(InputWidth1 - 1 downto 0); --! ClusterTOBs only (EM/TAU) !!!
		 Tob2       : in  TOBArray(InputWidth2 - 1 downto 0); --! ClusterTOBs only (EM/TAU) !!!
		 Tob3       : in  TOBArray(InputWidth3 - 1 downto 0);
		 Results    : out std_logic_vector(NumResultBits - 1 downto 0);
		 Overflow   : out std_logic_vector(NumResultBits - 1 downto 0)
	);
end DisambiguationIncl3;

architecture Behavioral of DisambiguationIncl3 is

	-- constants

	constant maxCount1 : integer := getMaxCount(InputWidth1, MaxTob1);
	constant maxCount2 : integer := getMaxCount(InputWidth2, MaxTob2);
	constant maxCount3 : integer := getMaxCount(InputWidth3, MaxTob3);

	-- Type declarations

	type deta_3 is array (InputWidth3 - 1 downto 0) of std_logic_vector(GenericEtaBitWidth - 1 downto 0);
	type deta_2 is array (InputWidth2 - 1 downto 0) of std_logic_vector(GenericEtaBitWidth - 1 downto 0);
	type detaarray_13 is array (InputWidth1 - 1 downto 0) of deta_3; --which InputWidth (from Jet or ClusterTOBS)
	type detaarray_23 is array (InputWidth2 - 1 downto 0) of deta_3;
	type detaarray_12 is array (InputWidth1 - 1 downto 0) of deta_2; --which InputWidth
	type dphi_3 is array (InputWidth3 - 1 downto 0) of std_logic_vector(GenericPhiBitWidth - 1 downto 0);
	type dphi_2 is array (InputWidth2 - 1 downto 0) of std_logic_vector(GenericPhiBitWidth - 1 downto 0);
	type dphiarray_13 is array (InputWidth1 - 1 downto 0) of dphi_3;
	type dphiarray_23 is array (InputWidth2 - 1 downto 0) of dphi_3;
	type dphiarray_12 is array (InputWidth1 - 1 downto 0) of dphi_2; --which InputWidth
	type drsqr_3 is array (InputWidth3 - 1 downto 0) of std_logic_vector(GenericRSqrBitWidth - 1 downto 0);
	type drsqr_2 is array (InputWidth2 - 1 downto 0) of std_logic_vector(GenericRSqrBitWidth - 1 downto 0);
	type drsqrarray_13 is array (InputWidth1 - 1 downto 0) of drsqr_3;
	type drsqrarray_23 is array (InputWidth2 - 1 downto 0) of drsqr_3;
	type drsqrarray_12 is array (InputWidth1 - 1 downto 0) of drsqr_2; --which InputWidth
	type etThreshArray is array (NumResultBits - 1 downto 0) of std_logic_vector(GenericEtBitWidth - 1 downto 0);
	type rsqrThreshArray is array (NumResultBits - 1 downto 0) of std_logic_vector(GenericRSqrBitWidth - 1 downto 0);
	type thrAcceptArray1 is array (NumResultBits - 1 downto 0) of std_logic_vector(InputWidth1 - 1 downto 0);
	type thrAcceptArray2 is array (NumResultBits - 1 downto 0) of std_logic_vector(InputWidth2 - 1 downto 0);
	type thrAcceptArray3 is array (NumResultBits - 1 downto 0) of std_logic_vector(InputWidth3 - 1 downto 0);

	-- Internal signal declarations

	signal clock   : std_logic;
	signal tob_in1 : TOBArray(InputWidth1 - 1 downto 0);
	signal tob_in2 : TOBArray(InputWidth2 - 1 downto 0);
	signal tob_in3 : TOBArray(InputWidth3 - 1 downto 0);

	signal MinEt1, MinEt2, MinEt3 : etThreshArray;
	signal DisambDRSqr            : rsqrThreshArray;
	signal DisambDRSqrMin         : rsqrThreshArray;
	signal DisambDRSqrMax         : rsqrThreshArray;

	signal deltaEta_13  : detaarray_13;
	signal deltaEta_23  : detaarray_23;
	signal deltaEta_12  : detaarray_12; --for DR requirement
	signal deltaPhi_13  : dphiarray_13;
	signal deltaPhi_23  : dphiarray_23;
	signal deltaPhi_12  : dphiarray_12; --for DR requirement
	signal deltaRSqr_13 : drsqrarray_13;
	signal deltaRSqr_23 : drsqrarray_23;
	signal deltaRSqr_12 : drsqrarray_12; --for DR requirement
	signal thraccept_1  : thrAcceptArray1;
	signal thraccept_2  : thrAcceptArray2;
	signal thraccept_3  : thrAcceptArray3;

	signal results_out  : std_logic_vector(NumResultBits - 1 downto 0);
	signal overflow_out : std_logic_vector(NumResultBits - 1 downto 0);

begin

	-- Behavioral

	clock   <= ClockBus(0);
	tob_in1 <= Tob1;
	tob_in2 <= Tob2;
	tob_in3 <= Tob3;

	ifDR_0 : if ApplyDR = 0 generate
		set_ranges_1 : for k in 0 to (NumResultBits - 1) generate
			MinEt1(k)      <= Parameters(0 + (4 * k))(GenericEtBitWidth - 1 downto 0);
			MinEt2(k)      <= Parameters(1 + (4 * k))(GenericEtBitWidth - 1 downto 0);
			MinEt3(k)      <= Parameters(2 + (4 * k))(GenericEtBitWidth - 1 downto 0);
			DisambDRSqr(k) <= Parameters(3 + (4 * k))(GenericRSqrBitWidth - 1 downto 0); --max value

		end generate;
	end generate;

	ifDR_1 : if ApplyDR = 1 generate
		set_ranges_1 : for k in 0 to (NumResultBits - 1) generate
			MinEt1(k)         <= Parameters(0 + (6 * k))(GenericEtBitWidth - 1 downto 0);
			MinEt2(k)         <= Parameters(1 + (6 * k))(GenericEtBitWidth - 1 downto 0);
			MinEt3(k)         <= Parameters(2 + (6 * k))(GenericEtBitWidth - 1 downto 0);
			DisambDRSqrMin(k) <= Parameters(3 + (6 * k))(GenericRSqrBitWidth - 1 downto 0);
			DisambDRSqrMax(k) <= Parameters(4 + (6 * k))(GenericRSqrBitWidth - 1 downto 0);
			DisambDRSqr(k)    <= Parameters(5 + (6 * k))(GenericRSqrBitWidth - 1 downto 0); --max value
		end generate;
	end generate;

	min_tob_Et : process(tob_in1, tob_in2, tob_in3, MinEt1, MinEt2, MinEt3) -- Flag TOBs with Et over threshold
	begin
		for k in 0 to (NumResultBits - 1) loop -- k: results
			for i in 0 to (maxCount1 - 1) loop -- i: tob1
				if (tob_in1(i).Et > MinEt1(k)) then
					thraccept_1(k)(i) <= '1';
				else
					thraccept_1(k)(i) <= '0';
				end if;
			end loop;
			for j in 0 to (maxCount2 - 1) loop -- j: tob2
				if (tob_in2(j).Et > MinEt2(k)) then
					thraccept_2(k)(j) <= '1';
				else
					thraccept_2(k)(j) <= '0';
				end if;
			end loop;
			for l in 0 to (maxCount3 - 1) loop -- l: tob3
				if (tob_in3(l).Et > MinEt3(k)) then
					thraccept_3(k)(l) <= '1';
				else
					thraccept_3(k)(l) <= '0';
				end if;
			end loop;
		end loop;
	end process;

	DeltaRSqrCalc_13_1 : for i in 0 to (maxCount1 - 1) generate -- Calculate DeltaRSqr_13
		DeltaRSqrCalc_13_2 : for l in 0 to (maxCount3 - 1) generate
			DeltaEtaCalc_inst_1 : entity work.DeltaEtaCalc
				port map(eta1In      => Tob1(i).Eta,
					     eta2In      => Tob3(l).Eta, --here tob2 -> tob3
					     deltaEtaOut => deltaEta_13(i)(l));
			DeltaPhiCalc_inst_1 : entity work.DeltaPhiCalc
				port map(phi1In      => Tob1(i).Phi,
					     phi2In      => Tob3(l).Phi,
					     deltaPhiOut => deltaPhi_13(i)(l));
			DeltaRSqrCalc_inst_1 : entity work.DeltaRSqrCalc
				Port map(deltaEta  => deltaEta_13(i)(l),
					     deltaPhi  => deltaPhi_13(i)(l),
					     deltaRSqr => deltaRSqr_13(i)(l));
		end generate;
	end generate;

	DeltaRSqrCalc_23_1 : for j in 0 to (maxCount2 - 1) generate -- Calculate DeltaRSqr_23
		DeltaRSqrCalc_23_2 : for l in 0 to (maxCount3 - 1) generate
			DeltaEtaCalc_inst_2 : entity work.DeltaEtaCalc
				port map(eta1In      => Tob2(j).Eta, --here tob1 -> tob2
					     eta2In      => Tob3(l).Eta, --here tob2 -> tob3
					     deltaEtaOut => deltaEta_23(j)(l));
			DeltaPhiCalc_inst_2 : entity work.DeltaPhiCalc
				port map(phi1In      => Tob2(j).Phi,
					     phi2In      => Tob3(l).Phi,
					     deltaPhiOut => deltaPhi_23(j)(l));
			DeltaRSqrCalc_inst_2 : entity work.DeltaRSqrCalc
				Port map(deltaEta  => deltaEta_23(j)(l),
					     deltaPhi  => deltaPhi_23(j)(l),
					     deltaRSqr => deltaRSqr_23(j)(l));
		end generate;
	end generate;

	ifDR_2 : if ApplyDR = 1 generate
		DeltaRSqrCalc_12_1 : for j in 0 to (maxCount1 - 1) generate -- Calculate DeltaRSqr_12
			DeltaRSqrCalc_12_2 : for l in 0 to (maxCount2 - 1) generate
				DeltaEtaCalc_inst_2 : entity work.DeltaEtaCalc
					port map(eta1In      => Tob1(j).Eta,
						     eta2In      => Tob2(l).Eta,
						     deltaEtaOut => deltaEta_12(j)(l));
				DeltaPhiCalc_inst_2 : entity work.DeltaPhiCalc
					port map(phi1In      => Tob1(j).Phi,
						     phi2In      => Tob2(l).Phi,
						     deltaPhiOut => deltaPhi_12(j)(l));
				DeltaRSqrCalc_inst_2 : entity work.DeltaRSqrCalc
					Port map(deltaEta  => deltaEta_12(j)(l),
						     deltaPhi  => deltaPhi_12(j)(l),
						     deltaRSqr => deltaRSqr_12(j)(l));
			end generate;
		end generate;
	end generate;

	-- Compare coordinates (accept if not equal) for TOB pairs over threshold
	compare_thresholds : process(Tob1, Tob2, deltaRSqr_13, DisambDRSqr, deltaRSqr_23, thraccept_1, thraccept_2, thraccept_3, deltaRSqr_12, DisambDRSqrMin, DisambDRSqrMax)
		variable hits : std_logic_vector(NumResultBits - 1 downto 0);
	begin
		hits := (others => '0');
		for i in 0 to (maxCount1 - 1) loop -- loop over TOBs i,j and l
			for j in 0 to (maxCount2 - 1) loop
				for l in 0 to (maxCount3 - 1) loop
					for k in 0 to NumResultBits - 1 loop -- loop over thresholds
						-- coordinates of Tob1 and Tob2 not identical + DR(Tob1,Tob3)>param. + DR(Tob2,Tob3)>param. + Tob1,Tob2,Tob3 over threshold:
						-- here if else DR
						if ApplyDR = 0 then
							if (((Tob1(i).Eta /= Tob2(j).Eta) or (Tob1(i).Phi /= Tob2(j).Phi)) and (deltaRSqr_13(i)(l) > DisambDRSqr(k)) and (deltaRSqr_23(j)(l) > DisambDRSqr(k)) and (thraccept_1(k)(i) = '1') and (thraccept_2(k)(j) = '1') and (thraccept_3(k)(l) = '1')
							) then
								hits(k) := '1';
							end if;

						end if;

						if ApplyDR = 1 then
							if (((Tob1(i).Eta /= Tob2(j).Eta) or (Tob1(i).Phi /= Tob2(j).Phi)) and (deltaRSqr_13(i)(l) > DisambDRSqr(k)) and (deltaRSqr_23(j)(l) > DisambDRSqr(k)) and (thraccept_1(k)(i) = '1') and (thraccept_2(k)(j) = '1') and (thraccept_3(k)(l) = '1')
								and deltaRSqr_12(i)(j) <= DisambDRSqrMax(k) and deltaRSqr_12(i)(j) > DisambDRSqrMin(k)
							) then
								hits(k) := '1';
							end if;

						end if;
					end loop;
				end loop;
			end loop;
		end loop;
		results_out <= hits;
	end process;

	Results <= results_out;

	-----------------------------
	-- generate overflow bits  --
	-----------------------------
	genOverflows : for result in 0 to NumResultBits - 1 generate
		-- get overflow from input list
		overflow_out(result) <= Tob1(0).Overflow or Tob2(0).Overflow or Tob3(0).Overflow;
	end generate;

	Overflow <= overflow_out;

end Behavioral;
