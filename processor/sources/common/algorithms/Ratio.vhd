------------------------------------------------------------------------------------------
-- Author/Modified by	: sebastian.artz@uni-mainz.de
-- Description			: calculates the ratio of MET (or MET squared) and JetHT calculated on the
--						  second input list and applies cut
-- IMPORTANT : if Ht saturates, the resultbit will be 0 and overflow will be 1
-- Number of registers	: 0
------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.L1TopoDataTypes.all;
use work.L1TopoFunctions.all;

entity Ratio is
	generic(InputWidth1   : integer := 1; -- MET
		    InputWidth2   : integer := InputWidthJET; -- jets for HT
		    MaxTob1       : integer := 0; -- number of TOBs which should be considered
		    MaxTob2       : integer := 0; -- number of TOBs which should be considered
		    NumResultBits : integer := 1;
		    isXE2         : integer := 0
	);
	port(
		Tob1       : in  TOBArray(InputWidth1 - 1 downto 0); -- MET
		Tob2       : in  TOBArray(InputWidth2 - 1 downto 0); -- jets for HT
		Parameters : in  ParameterArray;
		ClockBus   : in  std_logic_vector(2 downto 0);
		Results    : out std_logic_vector(NumResultBits - 1 downto 0);
		Overflow   : out std_logic_vector(NumResultBits - 1 downto 0)
	);
end Ratio;

architecture Behavioral of Ratio is

	-- constants

	constant MaxCount1 : integer := getMaxCount(InputWidth1, MaxTob1);
	constant MaxCount2 : integer := getMaxCount(InputWidth2, MaxTob2);

	-- Type declarations

	type ratioArr is array (integer range <>) of std_logic_vector(RatioBitWidth - 1 downto 0);

	-- Internal signal declarations

	signal ratioParam : ratioArr(NumResultBits - 1 downto 0);

	signal tob_in1  : TOBArray(InputWidth1 - 1 downto 0);
	signal tob_in2  : TOBArray(InputWidth2 - 1 downto 0);
	signal clock    : std_logic;
	signal MinEtHt  : std_logic_vector(JetHtBitWidth - 1 downto 0);
	signal MinEtMet : std_logic_vector(GenericEtBitWidth - 1 downto 0);

	signal thr_flag1    : std_logic_vector(InputWidth1 - 1 downto 0);
	signal thr_flag2    : std_logic;
	signal results_out  : std_logic_vector(NumResultBits - 1 downto 0);
	signal overflow_out : std_logic_vector(NumResultBits - 1 downto 0);

	signal ParametersHt : ParameterArray;
	signal HtSaturation : std_logic;
	signal HtSum        : std_logic_vector(JetHtBitWidth - 1 downto 0);
	signal HtOverflow   : std_logic;

	constant TEN : unsigned(3 downto 0) := "1010";

begin
	clock   <= ClockBus(0);
	tob_in1 <= Tob1;
	tob_in2 <= Tob2;

	ParametersHt(0) <= Parameters(0);   -- minEt (Ht)
	ParametersHt(1) <= Parameters(1);   -- minEta (Ht)
	ParametersHt(2) <= Parameters(2);   -- maxEta (Ht)
	MinEtMet        <= Parameters(3)(GenericEtBitWidth - 1 downto 0);
	MinEtHt         <= Parameters(4)(JetHtBitWidth - 1 downto 0);

	genThrArr : for i in 0 to NumResultBits - 1 generate
		ratioParam(i) <= Parameters(5 + i)(RatioBitWidth - 1 downto 0); -- can be interpreted as unsigned int with 1 digit decimal comma shift
	end generate;

	gen_Ht : entity work.JetHtInternal
		generic map(InputWidth => InputWidth2,
			        MaxTob     => MaxTob2,
			        HtBitWidth => JetHtBitWidth
		)
		Port Map(ClockBus   => ClockBus,
			     Parameters => ParametersHt,
			     Tob        => Tob2,
			     Saturation => HtSaturation,
			     SumValue   => HtSum,
			     Overflow   => HtOverflow
		);

	genThrFlag1 : for i in 0 to MaxCount1 - 1 generate
		thr_flag1(i) <= '1' when tob_in1(i).Et > MinEtMet else '0';
	end generate;

	thr_flag2 <= '1' when HtSum > MinEtHt else '0';

	genRatio : if (isXE2 = 0) generate
		compare_thresholds : process(HtSum, tob_in1, thr_flag1, thr_flag2, HtSaturation, ratioParam) -- Compare delta eta with thresholds for all TOB pairs -- TODO: BUGGY?
			variable hits : std_logic_vector(NumResultBits - 1 downto 0);
		begin
			hits := (others => '0');
			for i in 0 to (MaxCount1 - 1) loop -- loop over TOBs i and j
				for k in 0 to NumResultBits - 1 loop -- loop over thresholds
					if (unsigned(tob_in1(i).Et) * TEN >= unsigned(ratioParam(k)) * unsigned(HtSum) and (HtSaturation = '0') and (thr_flag1(i) = '1') and (thr_flag2 = '1')
					) then
						hits(k) := '1';
					end if;
				end loop;
			end loop;
			results_out <= hits;
		end process;
	end generate;

	genRatio2 : if (isXE2 = 1) generate
		compare_thresholds : process(HtSum, tob_in1, thr_flag1, thr_flag2, HtSaturation, ratioParam) -- Compare delta eta with thresholds for all TOB pairs -- TODO: BUGGY?
			variable hits : std_logic_vector(NumResultBits - 1 downto 0);
		begin
			hits := (others => '0');
			for i in 0 to (MaxCount1 - 1) loop -- loop over TOBs i and j
				for k in 0 to NumResultBits - 1 loop -- loop over thresholds
					if (unsigned(tob_in1(i).Et) * unsigned(tob_in1(i).Et) * TEN >= unsigned(ratioParam(k)) * unsigned(HtSum) and (HtSaturation = '0') and (thr_flag1(i) = '1') and (thr_flag2 = '1')
					) then
						hits(k) := '1';
					end if;
				end loop;
			end loop;
			results_out <= hits;
		end process;
	end generate;

	Results <= results_out;

	-----------------------------
	-- generate overflow bits  --
	-----------------------------
	genOverflows : for result in 0 to NumResultBits - 1 generate
		-- get overflow from input list
		overflow_out(result) <= Tob1(0).Overflow or Tob2(0).Overflow or HtOverflow or HtSaturation;
	end generate;

	Overflow <= overflow_out;

end Behavioral;