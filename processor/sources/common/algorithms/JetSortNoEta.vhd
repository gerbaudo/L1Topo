------------------------------------------------------------------------------------------
-- Author/Modified by	: manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de
-- Description			: applies eta cut to JetTOB list, sorts TOBs,
--						  converts them to GenericTOBs and returns short sorted list
-- Number of registers	: 2
------------------------------------------------------------------------------------------

--------------
-- includes --
--------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use work.L1TopoDataTypes.all;
use work.L1TopoFunctions.all;

------------------------
-- entity declaration --
------------------------

entity JetSortNoEta is
	generic(InputWidth         : integer := 64; --number of input TOBs
		    InputWidth1stStage : integer := InputWidth1stStageSortJet; --number of TOBs in 1st stage
		    OutputWidth        : integer := 10; --number of sorted output TOBs
		    JetSize            : integer := DefaultJetSize --jet Et bit width (1 = 9 bit, 2 = 10 b
	);
	Port(ClockBus    : in  std_logic_vector(2 downto 0);
		 Parameters  : in  ParameterArray;
		 JetTobArray : in  JetArray(InputWidth - 1 downto 0);
		 TobArrayOut : out TOBArray(OutputWidth - 1 downto 0)
	);
end JetSortNoEta;

-----------------------------------------
-- behavioral description of algorithm --
-----------------------------------------

architecture Behavioral of JetSortNoEta is
	constant NumSortedLists : integer := 2;

	-------------
	-- signals --
	-------------
	-- parameters:
	-- selected input TOBs (Eta & isolation):
	signal tob_in                                                        : JetArray(OutputWidth * NumSortedLists - 1 downto 0);
	signal generic_in, generic_in_reg1, generic_in_reg2, generic_in_reg3 : TOBArray(OutputWidth * NumSortedLists - 1 downto 0);
	-- sorted TOBs after first stage (Et):
	signal out_1stReg                                                    : TOBArray(OutputWidth * NumSortedLists - 1 downto 0);
	signal out_1stStage                                                  : TOBArray(OutputWidth - 1 downto 0);
	signal reg_out_1stStage                                              : TOBArray(OutputWidth - 1 downto 0);
	-- sorted TOBs after second stage
	signal out_2ndReg                                                    : TOBArray(OutputWidth - 1 downto 0);
	signal overflow_reg1, overflow_reg2                                  : std_logic;

begin
	gen_input_1 : for j in NumSortedLists - 1 downto 0 generate
		gen_input_2 : for i in OutputWidth - 1 downto 0 generate
			tob_in((NumSortedLists - 1 - j + 1) * OutputWidth - i - 1) <= JetTobArray(j * (InputWidth / NumSortedLists) + i);
		end generate;
	end generate;

	REG160 : process(ClockBus(2))
	begin
		if rising_edge(ClockBus(2)) then
			generic_in_reg1 <= generic_in;
			generic_in_reg2 <= generic_in_reg1;
			generic_in_reg3 <= generic_in_reg2;
		end if;
	end process;

	sel_eta_iso : process(tob_in)
	begin
		for i in 0 to OutputWidth * NumSortedLists - 1 loop
			-- translate to generic TOB
			generic_in(i) <= to_GenericTOB(tob_in(i), JetSize);
		end loop;
	end process;

	reg_1 : entity work.TobRegister
		generic map(InputWidth => OutputWidth * NumSortedLists
		)
		Port map(clk     => ClockBus(0),
			     enable  => '1',
			     reg_in  => generic_in_reg3,
			     reg_out => out_1stReg
		);

	-- sort presorted lists

	sortPresorted : entity work.TobSort
		generic map(OutputWidth => OutputWidth,
			        InputWidth  => OutputWidth * NumSortedLists,
			        DoPresort   => 0
		)
		port map(Tob         => out_1stReg,
			     TobArrayOut => out_1stStage
		);

	---------------------------------------------
	-- register the output of the second stage --
	---------------------------------------------

	reg_2 : entity work.TobRegister
		generic map(InputWidth => OutputWidth
		)
		Port map(clk     => ClockBus(0),
			     enable  => '1',
			     reg_in  => out_1stStage,
			     reg_out => out_2ndReg
		);

	--set Overflow
	gen_overflow_reg1 : entity work.StdLogicRegister
		port map(clk     => ClockBus(0),
			     enable  => '1',
			     reg_in  => JetTobArray(0).Overflow,
			     reg_out => overflow_reg1
		);

	gen_overflow_reg2 : entity work.StdLogicRegister
		port map(clk     => ClockBus(0),
			     enable  => '1',
			     reg_in  => overflow_reg1,
			     reg_out => overflow_reg2
		);

	-- workaround to set overflow bit and reordering of TOBs
	gen_set_overflow : for i in 0 to OutputWidth - 1 generate
		TobArrayOut(i).Et  <= out_2ndReg(OutputWidth - 1 - i).Et;
		TobArrayOut(i).Eta <= out_2ndReg(OutputWidth - 1 - i).Eta;
		TobArrayOut(i).Phi <= out_2ndReg(OutputWidth - 1 - i).Phi;
	end generate;

	TobArrayOut(0).Overflow <= overflow_reg2;

	-- only needed for simulation
	gen_set_dummy_overflow : for i in 1 to OutputWidth - 1 generate
		TobArrayOut(i).Overflow <= '0';
	end generate;

end Behavioral;
