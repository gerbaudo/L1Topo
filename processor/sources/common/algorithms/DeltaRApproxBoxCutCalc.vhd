library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.L1TopoDataTypes.ALL;

entity DeltaRApproxBoxCutCalc is
	Port(eta1     : in  std_logic_vector(GenericEtaBitWidth - 1 downto 0);
		 eta2     : in  std_logic_vector(GenericEtaBitWidth - 1 downto 0);
		 phi1     : in  std_logic_vector(GenericPhiBitWidth - 1 downto 0);
		 phi2     : in  std_logic_vector(GenericPhiBitWidth - 1 downto 0);
		 deltaEta : out std_logic_vector(GenericEtaBitWidth - 1 downto 0);
		 deltaPhi : out std_logic_vector(GenericPhiBitWidth - 1 downto 0)
	);
end DeltaRApproxBoxCutCalc;

architecture Behavioral of DeltaRApproxBoxCutCalc is
	signal eta1_sig, eta2_sig : signed(GenericEtaBitWidth - 1 downto 0);
	signal phi1_sig, phi2_sig : signed(GenericPhiBitWidth - 1 downto 0);

	signal delta_eta_tmp : unsigned(GenericEtaBitWidth - 1 downto 0);
	signal delta_phi_tmp : unsigned(GenericPhiBitWidth - 1 downto 0);

begin

	--std_logic_vector to signed
	eta1_sig <= signed(eta1);
	eta2_sig <= signed(eta2);
	phi1_sig <= signed(phi1);
	phi2_sig <= signed(phi2);

	--delta calculation
	delta_eta_tmp <= unsigned(abs (eta1_sig - eta2_sig));
	delta_phi_tmp <= unsigned(abs (phi1_sig - phi2_sig));
	--TODO: 2s-complement subtraction: ignoring the overflow bit == subtracting 64 if result > 32, ... then remove sign (abs) :)

	--remove unused bits, convert to std_logic_vector
	deltaEta <= std_logic_vector(delta_eta_tmp(GenericEtaBitWidth - 1 downto 0));
	deltaPhi <= std_logic_vector(delta_phi_tmp(GenericPhiBitWidth - 1 downto 0));

end Behavioral;
