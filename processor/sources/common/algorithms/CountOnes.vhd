------------------------------------------------------------------------------------------
-- Author/Modified by	: manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de
-- Description			: calculates cos via lookup table
-- Number of registers	: 0
------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.L1TopoFunctions.count_ones;

entity CountOnes is
	Generic(
		InputWidth : integer := 10
	);
	Port(input  : in  std_logic_vector(InputWidth - 1 downto 0);
		 output : out std_logic_vector(4 - 1 downto 0));
end CountOnes;

architecture Behavioral of CountOnes is
	type countLut is array (integer range 0 to 31) of std_logic;

	signal tmp1, tmp2 : std_logic_vector(3 - 1 downto 0);
	signal int1, int2 : integer range 0 to 31;

	constant countLutBit0 : countLut := ('0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '1', '0', '1', '1', '1');
	constant countLutBit1 : countLut := ('0', '0', '0', '1', '0', '1', '1', '1', '0', '1', '1', '1', '1', '1', '1', '0', '0', '1', '1', '1', '1', '1', '1', '0', '1', '1', '1', '0', '1', '0', '0', '0');
	constant countLutBit2 : countLut := ('0', '1', '1', '0', '1', '0', '0', '1', '1', '0', '0', '1', '0', '1', '1', '0', '1', '0', '0', '1', '0', '1', '1', '0', '0', '1', '1', '0', '1', '0', '0', '1');

begin

	-- optimized for 10 and 11 bits
	gen_10_11bit : if ((InputWidth = 10) or (InputWidth = 11)) generate
		int1 <= to_integer(unsigned(input(10 - 1 downto 5)));
		int2 <= to_integer(unsigned(input(5 - 1 downto 0)));

		tmp1 <= countLutBit0(int1) & countLutBit1(int1) & countLutBit2(int1);
		tmp2 <= countLutBit0(int2) & countLutBit1(int2) & countLutBit2(int2);

		gen_out_10 : if (InputWidth = 10) generate
			output <= std_logic_vector(unsigned('0' & tmp1) + unsigned('0' & tmp2));
		end generate;

		gen_out_11 : if (InputWidth = 11) generate
			output <= std_logic_vector(unsigned('0' & tmp1) + unsigned('0' & tmp2) + unsigned("000" & input(10 downto 10)));
		end generate;
	end generate;

	gen_any_bit : if ((InputWidth /= 10) and (InputWidth /= 11)) generate
		output <= std_logic_vector(to_unsigned(count_ones(input), output'length));
	end generate;

end Behavioral;