library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use work.L1TopoDataTypes.all;
use work.L1TopoFunctions.all;

entity L1TopoAlgorithms_debug_jets2 is
	Port(ClockBus    : in  std_logic_vector(2 downto 0);
		 
--		 ClusterIn   : in  ClusterArray(InputWidthEM - 1 downto 0);
--		 TauIn: in  ClusterArray(InputWidthTAU - 1 downto 0);
--		 JetIn : in  JetArray(InputWidthJET - 1 downto 0);
--		 MuonIn : in  MuonArray(InputWidthMU - 1 downto 0);

		 EmTobArray  : in  ClusterArray(InputWidthEM - 1 downto 0);
		 TauTobArray : in  ClusterArray(InputWidthTAU - 1 downto 0);
		 JetTobArray : in  JetArray(InputWidthJET - 1 downto 0);
--		 MuonTobArray  : in  MuonArray(InputWidthMU - 1 downto 0); --TODO: NYI! use it!
--		 signedEx    : in  std_logic_vector(14 downto 0);
--		 signedEy    : in  std_logic_vector(14 downto 0);
--		 ExEyArray	 : in  MissingEnergyArray(1 downto 0);
		 MetTobArray : in  MetArray(0 downto 0);
		 
		 Parameters  : in  ParameterSpace(NumberOfAlgorithms - 1 downto 0);
		 SortParameters     : in  ParameterSpace(NumberOfSortAlgorithms - 1 downto 0);
		 
		 Results     : out std_logic_vector(NumberOfResultBits - 1 downto 0);
		 Overflow    : out std_logic_vector(NumberOfResultBits - 1 downto 0);
		 
		 DebugOutput : out BitArray128(256 - 1 downto 0)
	);
end L1TopoAlgorithms_debug_jets2;

architecture Behavioral of L1TopoAlgorithms_debug_jets2 is

--signal ExEyArray	 : MissingEnergyArray(1 downto 0);
	
-- Menu Name   : Topo_pp_vX
-- Menu Version: 1

-- Module/FPGA : 0/0
-- General configuration (<TopoConfig>):
--    global_em_scale: 2
--    global_jet_scale: 1
  
 -- Ordered list of sorted TOBArrays:
  signal Jall : TOBArray ((InputWidthJet - 1)  downto 0);
  signal DebugOutput_inputTOBs : BitArray128(15 downto 0);
  
begin

sortalgo0 : entity work.JetNoSort_debug
  generic map (
     InputWidth => InputWidthJet,
     OutputWidth => InputWidthJet )
  port map (
     JetTobArray => JetTobArray,
     TobArrayOut =>  Jall,
     ClockBus => ClockBus,
     Parameters => SortParameters(0),
     DebugOut => DebugOutput(12 downto 0)
 );

-- JetTobArray to DebugOutput
-- JetTobArray = JetArray(64 - 1 downto 0) is array (natural range <>) of JetTOB is record:
-- Et1 : std_logic_vector(JetEt1BitWidth-1 downto 0); (9)
-- Et2 : std_logic_vector(JetEt2BitWidth-1 downto 0); (10)
-- Eta : std_logic_vector(JetEtaBitWidth-1 downto 0); (5)
-- Phi : std_logic_vector(JetPhiBitWidth-1 downto 0); (5)
-- number bits  per JetTOB = 29
-- ==> Array of 64 x 29 bits
-- 4 x 29 = 116 --> 4 JetTobs in 128 bits
-- 16 x 4 JetTobs

	debug_gen : for i in 0 to 15 generate
		DebugOutput_inputTOBs(i) <= JetArray_to128BitArray4(JetTobArray(4*i+3 downto 4*i));
	end generate;

	DebugOutput(28 downto 13) <= DebugOutput_inputTOBs(15 downto 0);
 
end Behavioral;