------------------------------------------------------------------------------------------
-- Author/Modified by	: manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de
-- Description			: applies eta cut to JetTOB list and converts
--						  it to GenericTOB list
-- Number of registers	: 0
------------------------------------------------------------------------------------------

--------------
-- includes --
--------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use work.L1TopoDataTypes.all;
use work.L1TopoFunctions.all;

------------------------
-- entity declaration --
------------------------

entity JetNoSort_debug is
	generic(InputWidth  : integer := InputWidthJET; --number of input TOBs
		    OutputWidth : integer := InputWidthJET; --number of output TOBs
		    JetSize     : integer := DefaultJetSize --jet Et bit width (1 = 9 bit, 2 = 10 bit)
	);
	Port(ClockBus    : in  std_logic_vector(2 downto 0);
		 Parameters  : in  ParameterArray;
		 JetTobArray : in  JetArray(InputWidth - 1 downto 0);
		 TobArrayOut : out TOBArray(OutputWidth - 1 downto 0);
		 DebugOut	 : out BitArray128(12 downto 0)
--		 Overflow    : out std_logic
	);
end JetNoSort_debug;

-----------------------------------------
-- behavioral description of algorithm --
-----------------------------------------

architecture Behavioral of JetNoSort_debug is

	-------------
	-- signals --
	-------------
	-- parameters:
--	signal threshold : std_logic_vector(GenericEtBitWidth-1 downto 0); --TODO: unused - include!
--	signal MinEta   : std_logic_vector(ClusterEtaBitWidth-1 downto 0); --TODO: unused - include!
--	signal MaxEta   : std_logic_vector(ClusterEtaBitWidth-1 downto 0); --TODO: unused - include!
	-- selected input TOBs (no cut):
	signal generic_in : TOBArray(InputWidth - 1 downto 0);
	
	----------------
	-- Attributes --
	----------------
	-- Preserve the hierarchy of instances/signals
	--attribute DONT_TOUCH : string;

begin

	debug_gen : for i in 0 to 11 generate
		DebugOut(i) <= TobArray_to128BitArray5(generic_in(5*i+4 downto 5*i));
	end generate;

	DebugOut(12) <= TobArray_to128BitArray4(generic_in(63 downto 5*12));

	

	-- read parameters
--	MinEt   <= Parameters(0)(GenericEtBitWidth-1 downto 0); --TODO: unused - include!
--	MinEta  <= Parameters(2)(GenericAbsoluteEtaBitWidth-1 downto 0); --TODO: unused - include!
--	MaxEta  <= Parameters(3)(GenericAbsoluteEtaBitWidth-1 downto 0); --TODO: unused - include!
	
	------------------------------
	-- translate to generic TOB --
	------------------------------

	no_sel : process(JetTobArray)
	begin
		for i in 0 to InputWidth - 1 loop
			-- translate to generic TOB
			generic_in(i) <= to_GenericTOB(JetTobArray(i),JetSize);
		end loop;
	end process;
	
	TobArrayOut <= generic_in;
	
	--set Overflow to 0 (Sorting has no Overflow)
	TobArrayOut(0).Overflow <= '0';

end Behavioral;
