------------------------------------------------------------------------------------------
-- Author/Modified by	: manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de
-- Description			: applies isolation and eta cut to ClusterTOB list, selects TOBs
--						  above an Et threshold and converts them to GenericTOBs
-- Number of registers	: 2
------------------------------------------------------------------------------------------

--------------
-- includes --
--------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use work.L1TopoDataTypes.all;
use work.L1TopoFunctions.all;

------------------------
-- entity declaration --
------------------------

entity ClusterSelect_debug is
	generic(InputWidth         : integer := InputWidthEM;               --number of input TOBs
		    InputWidth1stStage : integer := InputWidth1stStageSelectEM; --number of TOBs in 1st stage
		    OutputWidth        : integer := OutputWidthSelectEM         --number of selected output TOBs
	);
	Port(ClockBus        : in  std_logic_vector(2 downto 0);
		 Parameters      : in  ParameterArray;
		 ClusterTobArray : in  clusterArray(InputWidth - 1 downto 0);
		 TobArrayOut     : out TOBArray(OutputWidth - 1 downto 0);
		 DebugOut	 : out BitArray128(1 downto 0)
--		 Overflow        : out std_logic
	);
end ClusterSelect_debug;

-----------------------------------------
-- behavioral description of algorithm --
-----------------------------------------

architecture Behavioral of ClusterSelect_debug is

	-------------
	-- signals --
	-------------
	-- parameters:
	signal MinEt                           : std_logic_vector(GenericEtBitWidth - 1 downto 0);
	signal IsoMask                         : std_logic_vector(ClusterIsolBitWidth - 1 downto 0);
	signal MinEta                          : std_logic_vector(GenericAbsoluteEtaBitWidth - 1 downto 0);
	signal MaxEta                          : std_logic_vector(GenericAbsoluteEtaBitWidth - 1 downto 0);
	-- selected input TOBs (Eta & isolation):
	signal generic_in                      : TOBArray(InputWidth - 1 downto 0);
	signal generic_inEtaRange              : TOBArray(InputWidth - 1 downto 0);
	-- selected TOBs after first stage (Et threshold):
	signal out_1stStage                    : TOBArray((OutputWidth * (InputWidth / InputWidth1stStage)) - 1 downto 0);
	signal reg_out_1stStage                : TOBArray((OutputWidth * (InputWidth / InputWidth1stStage)) - 1 downto 0);
	-- intermediate results from first stage (used for second stage):
	signal cmp_res_1stStage                : std_logic_vector((OutputWidth * (InputWidth / InputWidth1stStage)) - 1 downto 0);
	signal cmp_res_1stStage_reg            : std_logic_vector((OutputWidth * (InputWidth / InputWidth1stStage)) - 1 downto 0);
	-- dummy (helper) signals:
	signal cmp_res_1stStage_reg_dmy        : std_logic_vector((OutputWidth * (InputWidth / InputWidth1stStage)) - 1 + OutputWidth downto 0);
	signal cmp_res_1stStage_reg_dmy_zeroes : std_logic_vector(OutputWidth - 1 downto 0) := (others => '0');
	signal reg_out_1stStage_dmy            : TOBArray((OutputWidth * (InputWidth / InputWidth1stStage)) - 1 + OutputWidth downto 0);
	signal reg_out_1stStage_dmy_empty      : TOBArray(OutputWidth - 1 downto 0);
	-- selected TOBs after second stage
	signal reg_out_2ndStage                : TOBArray(OutputWidth - 1 downto 0);
	-- overflow bit after second stage
	signal reg_over_2ndStage               : std_logic;
	signal reg_over_final                  : std_logic;
	
	signal generic_out : TOBArray(OutputWidth - 1 downto 0);

	----------------
	-- Attributes --
	----------------
	-- Preserve the hierarchy of instances/signals
	--attribute DONT_TOUCH : string;
	--attribute DONT_TOUCH of out_1stStage : signal is "TRUE";
	--attribute DONT_TOUCH of reg_out_1stStage : signal is "TRUE";
	--attribute DONT_TOUCH of cmp_res_1stStage : signal is "TRUE";
	--attribute DONT_TOUCH of cmp_res_1stStage_reg : signal is "TRUE";

begin

	DebugOut(0) <= TobArray_to128BitArray5(generic_out(4 downto 0));
	DebugOut(1) <= TobArray_to128BitArray1(generic_out(OutputWidthSelectEM -1  downto 5));

	-- read parameters
	MinEt   <= Parameters(0)(GenericEtBitWidth-1 downto 0);
	IsoMask <= Parameters(1)(ClusterIsolBitWidth-1 downto 0);
	MinEta  <= Parameters(2)(GenericAbsoluteEtaBitWidth-1 downto 0);
	MaxEta  <= Parameters(3)(GenericAbsoluteEtaBitWidth-1 downto 0);
	
	---------------------------------------------------------------------------
	-- translate to generic TOB and select TOBs (Eta range & isolation mask) --
	---------------------------------------------------------------------------
	-- If IsoMask is zero, then all TOBs are used (no isolation).
	-- Otherwise the isolation bits are ANDed with IsoMask.
	sel_eta_iso : process(IsoMask, MinEta, MaxEta, ClusterTobArray, generic_in)
	begin
		for i in 0 to InputWidth - 1 loop
			-- Select isolation
			if (IsoMask = "00000" or ((ClusterTobArray(i).Isol and IsoMask) /= "00000")) then
				-- translate to generic TOB			
				generic_in(i) <= to_GenericTOB(ClusterTobArray(i));
			else
				-- replace by empty dummy TOB if not matching selection criteria
				generic_in(i) <= empty_tob;
			end if;
			-- Select Eta range
			if ( is_in_eta_range_tob(generic_in(i), MinEta, MaxEta) ) then
				generic_inEtaRange(i) <= generic_in(i);
			else
				-- replace by empty dummy TOB if not matching selection criteria
				generic_inEtaRange(i) <= empty_tob;
			end if;
		end loop;
	end process;

	------------------------------------------------------------------------
	-- first stage: select the input TOBs in groups of InputWidth1stStage --
	------------------------------------------------------------------------
	
	gen1stStage : for i in 0 to InputWidth / InputWidth1stStage - 1 generate
		selection_1stStage : entity work.TobSelect
			generic map(OutputWidth => OutputWidth,
				        InputWidth  => InputWidth1stStage
			)
			port map(threshold   => MinEt,
				     Tob         => generic_inEtaRange((i + 1) * InputWidth1stStage - 1 downto i * InputWidth1stStage),
				     selTOBs     => out_1stStage((i + 1) * (OutputWidth) - 1 downto i * (OutputWidth)),
				     end_cmp_res => cmp_res_1stStage((i + 1) * (OutputWidth) - 1 downto i * (OutputWidth))
			);
	end generate;
	
	---------------------------------------------
	-- register the outputs of the first stage --
	---------------------------------------------
	
	cmp_res_reg : entity work.GenericRegister
		generic map(reg_width => (OutputWidth * (InputWidth / InputWidth1stStage))
		)
		port map(clk     => ClockBus(0),
			     enable  => '1',
			     reg_in  => cmp_res_1stStage,
			     reg_out => cmp_res_1stStage_reg
		);

	reg_1stStage : entity work.TobRegister
		generic map(InputWidth => (OutputWidth * (InputWidth / InputWidth1stStage))
		)
		Port map(clk     => ClockBus(0),
			     enable  => '1',
			     reg_in  => out_1stStage,
			     reg_out => reg_out_1stStage
		);

	--------------------------
	-- prepare second stage --
	--------------------------
	
	cmp_res_1stStage_reg_dmy <= cmp_res_1stStage_reg_dmy_zeroes & cmp_res_1stStage_reg(((OutputWidth) * (InputWidth / InputWidth1stStage)) - 1 downto 0);

	--create TOBArray of empty_tob with a number of OutputWidth elements
	genEmptyTOBs : for i in 0 to OutputWidth - 1 generate
		reg_out_1stStage_dmy_empty(i) <= empty_tob;
	end generate;

	reg_out_1stStage_dmy <= reg_out_1stStage_dmy_empty & reg_out_1stStage(((OutputWidth) * (InputWidth / InputWidth1stStage)) - 1 downto 0);

	-------------------------------------------------
	-- second stage: merge the first-stage outputs --
	-------------------------------------------------
	
	merge_ins : entity work.SelectMerge
		generic map(InputWidthBlocks   => InputWidth / InputWidth1stStage,
			        InputWidthPerBlock => OutputWidth
		)
		Port map(clk40         => ClockBus(0),
			     clkFast       => ClockBus((InputWidth / InputWidth1stStage) / 2), --4:160 MHz, 2:80 MHz
			     threshold_res => cmp_res_1stStage_reg_dmy,
			     inp           => reg_out_1stStage_dmy,
			     oup           => reg_out_2ndStage,
			     overflow      => reg_over_2ndStage
		);

	----------------------------------------------------------
	-- register the output and overflow of the second stage --
	----------------------------------------------------------

	reg_out_2nd_stage : entity work.TobRegister
		generic map(InputWidth => OutputWidth
		)
		Port map(clk     => ClockBus(0),
			     enable  => '1',
			     reg_in  => reg_out_2ndStage,
			     reg_out => generic_out
		);
		
	reg_over_2nd_stage : entity work.StdLogicRegister
		port map(clk     => ClockBus(0),
			     enable  => '1',
			     reg_in  => reg_over_2ndStage,
			     reg_out => reg_over_final
		);
		
	TobArrayOut <= generic_out;	
	
	-- Set overflow bit in element (0) of TOBArray
	TobArrayOut(0).Overflow <= reg_over_final;

end Behavioral;
