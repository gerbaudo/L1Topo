library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use work.L1TopoDataTypes.all;

entity L1TopoAlgorithms_debug_tau is
	Port(ClockBus    : in  std_logic_vector(2 downto 0);
		 
--		 ClusterIn   : in  ClusterArray(InputWidthEM - 1 downto 0);
--		 TauIn: in  ClusterArray(InputWidthTAU - 1 downto 0);
--		 JetIn : in  JetArray(InputWidthJET - 1 downto 0);
--		 MuonIn : in  MuonArray(InputWidthMU - 1 downto 0);

		 EmTobArray  : in  ClusterArray(InputWidthEM - 1 downto 0);
		 TauTobArray : in  ClusterArray(InputWidthTAU - 1 downto 0);
		 JetTobArray : in  JetArray(InputWidthJET - 1 downto 0);
--		 MuonTobArray  : in  MuonArray(InputWidthMU - 1 downto 0); --TODO: NYI! use it!
--		 signedEx    : in  std_logic_vector(14 downto 0);
--		 signedEy    : in  std_logic_vector(14 downto 0);
--		 ExEyArray	 : in  MissingEnergyArray(1 downto 0);
		 MetTobArray : in  MetArray(0 downto 0);
		 
		 Parameters  : in  ParameterSpace(NumberOfAlgorithms - 1 downto 0);
		 SortParameters     : in  ParameterSpace(NumberOfSortAlgorithms - 1 downto 0);
		 
		 Results     : out std_logic_vector(NumberOfResultBits - 1 downto 0);
		 Overflow    : out std_logic_vector(NumberOfResultBits - 1 downto 0);
		 
		 DebugOutput : out BitArray128(256 - 1 downto 0)
	);
end L1TopoAlgorithms_debug_tau;

architecture Behavioral of L1TopoAlgorithms_debug_tau is

--signal ExEyArray	 : MissingEnergyArray(1 downto 0);
	
-- Menu Name   : Topo_pp_vX
-- Menu Version: 1

-- Module/FPGA : 0/0
-- General configuration (<TopoConfig>):
--    global_em_scale: 2
--    global_jet_scale: 1

  constant AlgoOffset0 : integer := 0;
  
 -- Ordered list of sorted TOBArrays:
  signal TAUall : TOBArray ((InputWidthTAU - 1)  downto 0);
  signal TAUs   : TOBArray ((OutputWidthSortTAU - 1)  downto 0);
  signal TAUab  : TOBArray ((OutputWidthSelectTAU - 1)  downto 0);
  
begin

sortalgo0 : entity work.ClusterNoSort_debug
  generic map (
     InputWidth => InputWidthTAU,
     OutputWidth => InputWidthTAU )
  port map (
     ClusterTobArray => TauTobArray,
     TobArrayOut =>  TAUall,
     ClockBus => ClockBus,
     Parameters => SortParameters(0),
     DebugOut => DebugOutput(23 downto 0)
 );

sortalgo1 : entity work.ClusterSort_debug
  generic map (
     InputWidth => InputWidthTAU,
     InputWidth1stStage => InputWidth1stStageSortTAU,
     OutputWidth => OutputWidthSortTAU)
  port map (
     ClusterTobArray => TauTobArray,
     TobArrayOut =>  TAUs,
     ClockBus => ClockBus,
     Parameters => SortParameters(1),
     DebugOut => DebugOutput(25 downto 24)
 );
 
sortalgo2 : entity work.ClusterSelect_debug
  generic map (
     InputWidth => InputWidthTAU,
     InputWidth1stStage => InputWidth1stStageSelectTAU,
     OutputWidth => OutputWidthSelectTAU)
  port map (
     ClusterTobArray => TauTobArray,
     TobArrayOut =>  TAUab,
     ClockBus => ClockBus,
     Parameters => SortParameters(2),
     DebugOut => DebugOutput(27 downto 26)
 );

decisionalgo0 : entity work.DeltaPhiIncl1
	generic map (
		InputWidth => OutputWidthSortTAU,
		NumResultBits => 1 )
	port map (
		Tob => TAUs,
		Parameters => Parameters(0),
		ClockBus => ClockBus,
		Results => Results(AlgoOffset0+0 downto AlgoOffset0),
		Overflow => Overflow(AlgoOffset0+0 downto AlgoOffset0)
);
 
end Behavioral;