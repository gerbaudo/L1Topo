------------------------------------------------------------------------------------------
-- Author/Modified by	: manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de
-- Description			: applies isolation and eta cut to ClusterTOB list and converts
--						  it to GenericTOB list
-- Number of registers	: 0
------------------------------------------------------------------------------------------

--------------
-- includes --
--------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use work.L1TopoDataTypes.all;
use work.L1TopoFunctions.all;

------------------------
-- entity declaration --
------------------------

entity ClusterNoSort_debug is
	generic(InputWidth  : integer := InputWidthEM; --number of input TOBs
		    OutputWidth : integer := InputWidthEM  --number of output TOBs
	);
	Port(ClockBus        : in  std_logic_vector(2 downto 0);
		 Parameters      : in  ParameterArray;
		 ClusterTobArray : in  ClusterArray(InputWidth - 1 downto 0);
		 TobArrayOut     : out TOBArray(OutputWidth - 1 downto 0);
		 DebugOut	     : out BitArray128(23 downto 0)
--		 Overflow        : out std_logic
	);
end ClusterNoSort_debug;

-----------------------------------------
-- behavioral description of algorithm --
-----------------------------------------

architecture Behavioral of ClusterNoSort_debug is
	
	-------------
	-- signals --
	-------------
	-- parameters:
--	signal MinEt   : std_logic_vector(GenericEtBitWidth-1 downto 0); --TODO: unused - include!
	signal IsoMask : std_logic_vector(ClusterIsolBitWidth-1 downto 0);
--	signal MinEta  : std_logic_vector(ClusterEtaBitWidth-1 downto 0); --TODO: unused - include!
--	signal MaxEta  : std_logic_vector(ClusterEtaBitWidth-1 downto 0); --TODO: unused - include!
	-- selected input TOBs (isolation):
	signal generic_in : TOBArray(InputWidth - 1 downto 0);

	----------------
	-- Attributes --
	----------------
	-- Preserve the hierarchy of instances/signals
	--attribute DONT_TOUCH : string;

begin

	debug_gen : for i in 0 to 23 generate
		DebugOut(i) <= TobArray_to128BitArray5(generic_in(5*i+4 downto 5*i));
	end generate;

	--DebugOut(23) <= TobArray_to128BitArray5(generic_in(InputWidthEM - 1 downto 5*22));

	-- read parameters
--	MinEt   <= Parameters(0)(GenericEtBitWidth-1 downto 0); --TODO: unused - include!
	IsoMask <= Parameters(0)(ClusterIsolBitWidth-1 downto 0);
--	MinEta  <= Parameters(2)(GenericAbsoluteEtaBitWidth-1 downto 0); --TODO: unused - include!
--	MaxEta  <= Parameters(3)(GenericAbsoluteEtaBitWidth-1 downto 0); --TODO: unused - include!

	---------------------------------------------------------------
	-- select TOBs (isolation mask) and translate to generic TOB --
	---------------------------------------------------------------
	-- If IsoMask is zero, then all TOBs are used (no isolation).
	-- Otherwise the isolation bits are ANDed with IsoMask.
	sel_iso : process(IsoMask, ClusterTobArray)
	begin
		for i in 0 to InputWidth - 1 loop
			if (IsoMask = "00000" or ((ClusterTobArray(i).Isol and IsoMask) /= "00000")) then -- Isolation
				-- translate to generic TOB
				generic_in(i) <= to_GenericTOB(ClusterTobArray(i));
			else
				-- replace by empty dummy TOB if not matching selection criteria
				generic_in(i) <= empty_tob;
			end if;
		end loop;
	end process;
	
	TobArrayOut <= generic_in;
	
	--set Overflow to 0 (Sorting has no Overflow)
	TobArrayOut(0).Overflow <= '0';

end Behavioral;
