------------------------------------------------------------------------------------------
-- Author/Modified by	: manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de
-- Description			: applies eta cut to JetTOB list, sorts TOBs,
--						  converts them to GenericTOBs and returns short sorted list
-- Number of registers	: 2
------------------------------------------------------------------------------------------

--------------
-- includes --
--------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use work.L1TopoDataTypes.all;
use work.L1TopoFunctions.all;

------------------------
-- entity declaration --
------------------------

entity JetSort_debug is
	generic(InputWidth         : integer := InputWidthJET;             --number of input TOBs
		    InputWidth1stStage : integer := InputWidth1stStageSortJET; --number of TOBs in 1st stage
		    OutputWidth        : integer := OutputWidthSortJET;        --number of sorted output TOBs
		    JetSize            : integer := DefaultJetSize             --jet Et bit width (1 = 9 bit, 2 = 10 bit)
	);
	Port(ClockBus    : in  std_logic_vector(2 downto 0);
		 Parameters  : in  ParameterArray;
		 JetTobArray : in  JetArray(InputWidth - 1 downto 0);
		 TobArrayOut : out TOBArray(OutputWidth - 1 downto 0);
		 DebugOut	 : out BitArray128(1 downto 0)
--		 Overflow    : out std_logic
	);
end JetSort_debug;

-----------------------------------------
-- behavioral description of algorithm --
-----------------------------------------

architecture Behavioral of JetSort_debug is

	-------------
	-- signals --
	-------------
	-- parameters:
--	signal MinEt              : std_logic_vector(GenericEtBitWidth - 1 downto 0); --TODO: unused - include!
	signal MinEta             : std_logic_vector(GenericAbsoluteEtaBitWidth - 1 downto 0);
	signal MaxEta             : std_logic_vector(GenericAbsoluteEtaBitWidth - 1 downto 0);
	-- selected input TOBs (Eta):
	signal generic_in         : TOBArray(InputWidth - 1 downto 0);
	signal generic_inEtaRange : TOBArray(InputWidth - 1 downto 0);
	-- sorted TOBs after first stage (Et):
	signal out_1stStage       : TOBArray((InputWidth / InputWidth1stStage) * OutputWidth - 1 downto 0);
	signal reg_out_1stStage   : TOBArray((InputWidth / InputWidth1stStage) * OutputWidth - 1 downto 0);
	-- sorted TOBs after second stage
	signal reg_out_2ndStage   : TOBArray(OutputWidth - 1 downto 0);

	signal generic_out : TOBArray(OutputWidth - 1 downto 0);

	----------------
	-- Attributes --
	----------------
	-- preserve the hierarchy of instances/signals
	--attribute DONT_TOUCH : string;
	--attribute DONT_TOUCH of out_1stStage : signal is "TRUE";
	--attribute DONT_TOUCH of reg_out_1stStage : signal is "TRUE";

begin

	DebugOut(0) <= TobArray_to128BitArray5(generic_out(4 downto 0));
	DebugOut(1) <= TobArray_to128BitArray1(generic_out(OutputWidthSortJET - 1 downto 5));

	-- read parameters
--	MinEt   <= Parameters(0)(GenericEtBitWidth-1 downto 0); --TODO: unused - include!
	MinEta  <= Parameters(0)(GenericAbsoluteEtaBitWidth-1 downto 0);
	MaxEta  <= Parameters(1)(GenericAbsoluteEtaBitWidth-1 downto 0);

	----------------------------------------------------------
	-- translate to generic TOB and select TOBs (Eta range) --
	----------------------------------------------------------

	sel_eta_range : process(MinEta, MaxEta, JetTobArray, generic_in)
	begin
		for i in 0 to InputWidth - 1 loop
			-- translate to generic TOB			
			generic_in(i) <= to_GenericTOB(JetTobArray(i),JetSize);
			-- Select Eta range
			if ( is_in_eta_range_tob(generic_in(i), MinEta, MaxEta) ) then
				generic_inEtaRange(i) <= generic_in(i);
			else
				-- replace by empty dummy TOB if not matching selection criteria
				generic_inEtaRange(i) <= empty_tob;
			end if;
		end loop;
	end process;

	----------------------------------------------------------------------
	-- first stage: sort the input TOBs in groups of InputWidth1stStage --
	----------------------------------------------------------------------

	gen1stStage : for i in 0 to InputWidth / InputWidth1stStage - 1 generate
		sort1stStage : entity work.TobSort
			generic map(OutputWidth => OutputWidth,
				        InputWidth  => InputWidth1stStage,
				        DoPresort   => 1
			)
			port map(
				Tob         => generic_inEtaRange((i + 1) * InputWidth1stStage - 1 downto i * InputWidth1stStage),
				TobArrayOut => out_1stStage((i + 1) * OutputWidth - 1 downto i * OutputWidth)
			);
	end generate;

	--------------------------------------------
	-- register the output of the first stage --
	--------------------------------------------

	reg_1st_stage : entity work.TobRegister
		generic map(InputWidth => (InputWidth / InputWidth1stStage) * OutputWidth
		)
		Port map(clk     => ClockBus(0),
			     enable  => '1',
			     reg_in  => out_1stStage,
			     reg_out => reg_out_1stStage
		);

	------------------------------------------------
	-- second stage: sort the first-stage outputs --
	------------------------------------------------

	sort2ndStage : entity work.TobSort
		generic map(OutputWidth => OutputWidth,
			        InputWidth  => (InputWidth / InputWidth1stStage) * OutputWidth,
			        DoPresort   => 0
		)
		port map(Tob         => reg_out_1stStage,
			     TobArrayOut => reg_out_2ndStage
		);

	---------------------------------------------
	-- register the output of the second stage --
	---------------------------------------------

	reg_2nd_stage : entity work.TobRegister
		generic map(InputWidth => OutputWidth
		)
		Port map(clk     => ClockBus(0),
			     enable  => '1',
			     reg_in  => reg_out_2ndStage,
			     reg_out => TobArrayOut
		);
	
	TobArrayOut <= generic_out;
	
	--set Overflow to 0 (Sorting has no Overflow)
	TobArrayOut(0).Overflow <= '0';

end Behavioral;
