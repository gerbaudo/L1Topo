------------------------------------------------------------------------------------------
-- Author/Modified by	: manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de
-- Description			: applies isolation and eta cut to ClusterTOB list, sorts TOBs,
--						  converts them to GenericTOBs and returns short sorted list
-- Number of registers	: 2
------------------------------------------------------------------------------------------

--------------
-- includes --
--------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use work.L1TopoDataTypes.all;
use work.L1TopoFunctions.all;

------------------------
-- entity declaration --
------------------------

entity ClusterSort_debug is
	generic(InputWidth         : integer := InputWidthEM;             --number of input TOBs
		    InputWidth1stStage : integer := InputWidth1stStageSortEM; --number of TOBs in 1st stage
		    OutputWidth        : integer := OutputWidthSortEM         --number of sorted output TOBs
	);
	Port(ClockBus        : in  std_logic_vector(2 downto 0);
		 Parameters      : in  ParameterArray;
		 ClusterTobArray : in  ClusterArray(InputWidth - 1 downto 0);
		 TobArrayOut     : out TOBArray(OutputWidth - 1 downto 0);
		 DebugOut	     : out BitArray128(1 downto 0)
--		 Overflow        : out std_logic
	);
end ClusterSort_debug;

-----------------------------------------
-- behavioral description of algorithm --
-----------------------------------------

architecture Behavioral of ClusterSort_debug is

	-------------
	-- signals --
	-------------
	-- parameters:
--	signal MinEt              : std_logic_vector(GenericEtBitWidth - 1 downto 0); --TODO: unused - include!
	signal IsoMask            : std_logic_vector(ClusterIsolBitWidth - 1 downto 0);
	signal MinEta             : std_logic_vector(GenericAbsoluteEtaBitWidth - 1 downto 0);
	signal MaxEta             : std_logic_vector(GenericAbsoluteEtaBitWidth - 1 downto 0);
	-- selected input TOBs (Eta & isolation):
	signal generic_in         : TOBArray(InputWidth - 1 downto 0);
	signal generic_inEtaRange : TOBArray(InputWidth - 1 downto 0);
	-- sorted TOBs after first stage (Et):
	signal out_1stStage       : TOBArray((InputWidth / InputWidth1stStage) * OutputWidth - 1 downto 0);
	signal reg_out_1stStage   : TOBArray((InputWidth / InputWidth1stStage) * OutputWidth - 1 downto 0);
	-- sorted TOBs after second stage
	signal reg_out_2ndStage   : TOBArray(OutputWidth - 1 downto 0);

	signal generic_out : TOBArray(OutputWidth - 1 downto 0);

	----------------
	-- Attributes --
	----------------
	-- preserve the hierarchy of instances/signals
	--attribute DONT_TOUCH : string;
	--attribute DONT_TOUCH of out_1stStage : signal is "TRUE";
	--attribute DONT_TOUCH of reg_out_1stStage : signal is "TRUE";

begin
	
	DebugOut(0) <= TobArray_to128BitArray5(generic_out(4 downto 0));
	DebugOut(1) <= TobArray_to128BitArray1(generic_out(OutputWidthSortEM - 1 downto 5));
	
	-- read parameters
--	MinEt   <= Parameters(0)(GenericEtBitWidth-1 downto 0); --TODO: unused - include!
	IsoMask <= Parameters(0)(ClusterIsolBitWidth-1 downto 0);
	MinEta  <= Parameters(1)(GenericAbsoluteEtaBitWidth-1 downto 0);
	MaxEta  <= Parameters(2)(GenericAbsoluteEtaBitWidth-1 downto 0);
	
	---------------------------------------------------------------------------
	-- translate to generic TOB and select TOBs (Eta range & isolation mask) --
	---------------------------------------------------------------------------
	-- If IsoMask is zero, then all TOBs are used (no isolation).
	-- Otherwise the isolation bits are ANDed with IsoMask.
	sel_eta_iso : process(IsoMask, MinEta, MaxEta, ClusterTobArray, generic_in)
	begin
		for i in 0 to InputWidth - 1 loop
			-- Select isolation
			if (IsoMask = "00000" or ((ClusterTobArray(i).Isol and IsoMask) /= "00000")) then
				-- translate to generic TOB			
				generic_in(i) <= to_GenericTOB(ClusterTobArray(i));
			else
				-- replace by empty dummy TOB if not matching selection criteria
				generic_in(i) <= empty_tob;
			end if;
			-- Select Eta range
			if ( is_in_eta_range_tob(generic_in(i), MinEta, MaxEta) ) then
				generic_inEtaRange(i) <= generic_in(i);
			else
				-- replace by empty dummy TOB if not matching selection criteria
				generic_inEtaRange(i) <= empty_tob;
			end if;
		end loop;
	end process;

	----------------------------------------------------------------------
	-- first stage: sort the input TOBs in groups of InputWidth1stStage --
	----------------------------------------------------------------------
	
	gen1stStage : for i in 0 to InputWidth / InputWidth1stStage - 1 generate
		sort1stStage : entity work.TobSort
			generic map(OutputWidth => OutputWidth,
				        InputWidth  => InputWidth1stStage,
				        DoPresort   => 1
			)
			port map(
				Tob         => generic_inEtaRange((i + 1) * InputWidth1stStage - 1 downto i * InputWidth1stStage),
				TOBArrayOut => out_1stStage((i + 1) * OutputWidth - 1 downto i * OutputWidth)
			);
	end generate;

	--------------------------------------------
	-- register the output of the first stage --
	--------------------------------------------
	
	reg_1st_stage : entity work.TobRegister
		generic map(InputWidth => (InputWidth / InputWidth1stStage) * OutputWidth
		)
		Port map(clk     => ClockBus(0),
			     enable  => '1',
			     reg_in  => out_1stStage,
			     reg_out => reg_out_1stStage
		);

	------------------------------------------------
	-- second stage: sort the first-stage outputs --
	------------------------------------------------
	
	sort2ndStage : entity work.TobSort
		generic map(OutputWidth => OutputWidth,
			        InputWidth  => (InputWidth / InputWidth1stStage) * OutputWidth,
			        DoPresort   => 0
		)
		port map(Tob         => reg_out_1stStage,
			     TobArrayOut => reg_out_2ndStage
		);

	---------------------------------------------
	-- register the output of the second stage --
	---------------------------------------------

	reg_2nd_stage : entity work.TobRegister
		generic map(InputWidth => OutputWidth
		)
		Port map(clk     => ClockBus(0),
			     enable  => '1',
			     reg_in  => reg_out_2ndStage,
			     reg_out => generic_out
		);
	
	TobArrayOut <= generic_out;	
	
	--set Overflow to 0 (Sorting has no Overflow)
	TobArrayOut(0).Overflow <= '0';

end Behavioral;
