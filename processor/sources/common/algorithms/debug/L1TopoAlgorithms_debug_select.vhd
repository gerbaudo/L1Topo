library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use work.L1TopoDataTypes.all;

entity L1TopoAlgorithms_debug_select is
	Port(ClockBus       : in  std_logic_vector(2 downto 0);

		 EmTobArray     : in  ClusterArray(InputWidthEM - 1 downto 0);
		 TauTobArray    : in  ClusterArray(InputWidthTAU - 1 downto 0);
		 JetTobArray    : in  JetArray(InputWidthJET - 1 downto 0);
		 MuonTobArray   : in  MuonArray(InputWidthMU - 1 downto 0);
		 MetTobArray    : in  MetArray(0 downto 0);

		 Parameters     : in  ParameterSpace(NumberOfAlgorithms - 1 downto 0);
		 SortParameters : in  ParameterSpace(NumberOfSortAlgorithms - 1 downto 0);

		 Results        : out std_logic_vector(NumberOfResultBits - 1 downto 0);
		 Overflow       : out std_logic_vector(NumberOfResultBits - 1 downto 0);

		 DebugOutput    : out BitArray128(256 - 1 downto 0)
	);
end L1TopoAlgorithms_debug_select;

architecture Behavioral of L1TopoAlgorithms_debug_select is

	-- Menu Name   : Topo_pp_vX
	-- Menu Version: 1

	-- Module/FPGA : 0/0
	-- General configuration (<TopoConfig>):
	--    global_em_scale: 2
	--    global_jet_scale: 1

	constant AlgoOffset0 : integer := 0;

	-- Ordered list of sorted TOBArrays:
	signal AJab : TOBArray((OutputWidthSelectJET - 1) downto 0);

begin
	sortalgo0 : entity work.JetSelect_debug
		generic map(
			InputWidth         => InputWidthJET,
			InputWidth1stStage => InputWidth1stStageSelectJET,
			OutputWidth        => OutputWidthSelectJET,
			JetSize            => 2)
		port map(
			JetTobArray => JetTobArray,
			TobArrayOut => AJab,
			ClockBus    => ClockBus,
			Parameters  => SortParameters(0),
			DebugOut    => DebugOutput(1 downto 0)
		);

	decisionalgo0 : entity work.JetHT
		generic map(
			InputWidth    => OutputWidthSelectJET,
			MaxTob        => 0,
			NumResultBits => 1,
			NumRegisters  => 0)
		port map(
			ClockBus   => ClockBus,
			Parameters => Parameters(0),
			Tob        => AJab,
			Results    => Results(AlgoOffset0 + 0 downto AlgoOffset0),
			Overflow   => Overflow(AlgoOffset0 + 0 downto AlgoOffset0)
		);

end Behavioral;