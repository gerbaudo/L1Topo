------------------------------------------------------------------------------------------
-- Author/Modified by	: manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de
-- Description			: calculates MET Tob
-- Number of registers	: 2
------------------------------------------------------------------------------------------

--------------
-- includes --
--------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use work.L1TopoDataTypes.all;
use work.L1TopoFunctions.all;

------------------------
-- entity declaration --
------------------------

entity METSort_debug is
	generic(InputWidth         : integer := InputWidthMET; --number of input missing energies (1)
		    OutputWidth        : integer := OutputWidthMET --number of output missing energies (MET, MET_corrected)
	);
	Port(ClockBus    : in  std_logic_vector(2 downto 0);
		 Parameters  : in  ParameterArray; --unused!
		 MetTobArray : in  MetArray(InputWidth - 1 downto 0); --make this algorithm flexible (now input width can only be 1)
		 TobArrayOut : out TOBArray(OutputWidth - 1 downto 0);
		 DebugOut	 : out BitArray128(0 downto 0)
--		 Overflow    : out std_logic
	);
end METSort_debug;

-----------------------------------------
-- behavioral description of algorithm --
-----------------------------------------

architecture Behavioral of METSort_debug is

	----------------------
	-- type definitions --
	----------------------

	type METPhiArray is array (natural range <>) of std_logic_vector(GenericPhiBitWidth - 1 downto 0);
	type METEnergyArray is array (natural range <>) of std_logic_vector(Arctan2InputEnergyBitWidth - 1 downto 0);

	-------------
	-- signals --
	-------------
	-- parameters:
--	signal threshold : std_logic_vector(GenericEtBitWidth-1 downto 0); --TODO: unused - include!
	-- MET Ex and Ey with matching number of bits for Arctan2 function
	signal Ex_in, Ey_in, reg_Ex_in, reg_Ey_in : METEnergyArray(InputWidth - 1 downto 0);
	-- phi value of MET object
	signal out_METPhi, reg_out_METPhi : METPhiArray(InputWidth - 1 downto 0);
	-- buffered input
	signal reg_MetTobArray : MetArray(InputWidth - 1 downto 0);
	-- selected input TOBs (no cut):
	signal generic_in : TOBArray(InputWidth - 1 downto 0);
	signal generic_out : TOBArray(OutputWidth - 1 downto 0);
	
begin
	
	-- read parameters
--	MinEt   <= Parameters(0)(GenericEtBitWidth-1 downto 0); --TODO: unused - include!
	
	DebugOut(0) <= TobArray_to128BitArray1(generic_out(0 downto 0));
	
	

	-- calculate phi and register output for all input METs
	genInputWidth : for i in 0 to InputWidth - 1 generate
	
		-------------------------
		-- MET Phi calculation --
		-------------------------
	
		-- make MET energies fit Artan2 function arguments (take highest bits)
		Ex_in(i) <= MetTobArray(i).Ex(METEnergyBitWidth - 1 downto METEnergyBitWidth - Arctan2InputEnergyBitWidth);
		Ey_in(i) <= MetTobArray(i).Ey(METEnergyBitWidth - 1 downto METEnergyBitWidth - Arctan2InputEnergyBitWidth);

		Arctan_inst : entity work.Arctan2 Port map(Ex       => Ex_in(i),
				                                   Ey       => Ey_in(i),
				                                   Phi      => out_METPhi(i)
			);

		--------------------------
		-- register the outputs --
		--------------------------

		reg_METPhi : entity work.GenericRegister
		generic map(reg_width => GenericPhiBitWidth
	       )
		port map(clk => ClockBus(0),
			enable  => '1',
	    	reg_in  => out_METPhi(i),
	    	reg_out => reg_out_METPhi(i)
	    );
	    
	    reg_Ex : entity work.GenericRegister
		generic map(reg_width => Arctan2InputEnergyBitWidth
	       )
		port map(clk => ClockBus(0),
			enable  => '1',
	    	reg_in  => Ex_in(i),
	    	reg_out => reg_Ex_in(i)
	    );
	    
	    reg_Ey : entity work.GenericRegister
		generic map(reg_width => Arctan2InputEnergyBitWidth
	       )
		port map(clk => ClockBus(0),
			enable  => '1',
	    	reg_in  => Ey_in(i),
	    	reg_out => reg_Ey_in(i)
	    );
	    
	    reg_MetTobArray(i).Ex <= reg_Ex_in(i);
		reg_MetTobArray(i).Ey <= reg_Ey_in(i);
		--reg_MetTobArray(i).Et <= (others => '0');
	
	end generate;

	------------------------------------
	-- Kalman Filter MET calculations --
	------------------------------------
	
	--TODO: put to TobArrayOut(1):
	--	KalmanMETCorrection_inst : entity work.KalmanMETCorrection
--		Port map(ClockBus            => ClockBus,
--			     Parameters          => Parameters(30),
--			     JetTobArray         => JetTobArray,
--			     signedEx            => signedEx,
--			     signedEy            => signedEy,
--			     lookupTableEtEta_in => KalmanMETCorrectionLUT,
--			     METSqrCorrected     => METSqr,
--			     METPhiCorrected     => METPhi,
--			     Overflow_out        => Overflow(31)
--		);

	------------------------------
	-- translate to generic TOB --
	------------------------------

	no_sel : process(reg_MetTobArray,reg_out_METPhi)
	begin
		for i in 0 to InputWidth - 1 loop
			-- translate to generic TOB
			generic_in(i) <= to_GenericTOB(reg_MetTobArray(i),reg_out_METPhi(i));
		end loop;
	end process;
	
	-------------------------
	-- register the output --
	-------------------------
	
	reg_out : entity work.TobRegister
		generic map(InputWidth => OutputWidth
		)
		Port map(clk     => ClockBus(0),
			     enable  => '1',
			     reg_in  => generic_in,
			     reg_out => generic_out
		);
	
	--set Overflow to 0 (Sorting has no Overflow)
	TobArrayOut(0).Overflow <= '0';
	
	TobArrayOut <= generic_out;

end Behavioral;