------------------------------------------------------------------------------------------
-- Author/Modified by	: sebastian.artz@uni-mainz.de
-- Description			: checks that there are no matching objects between the two lists
-- Number of registers	: 0
------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use work.L1TopoDataTypes.ALL;
use work.L1TopoFunctions.all;

entity NotMatch is
	generic(InputWidth1   : integer := 9;
		    InputWidth2   : integer := 9;
		    MaxTob1       : integer := 0; -- number of TOBs which should be considered
		    MaxTob2       : integer := 0; -- number of TOBs which should be considered
		    NumResultBits : integer := 1
	);
	Port(ClockBus   : std_logic_vector(2 downto 0);
		 Parameters : in  ParameterArray;
		 Tob1       : in  TOBArray(InputWidth1 - 1 downto 0);
		 Tob2       : in  TOBArray(InputWidth2 - 1 downto 0);
		 Results    : out std_logic_vector(NumResultBits - 1 downto 0);
		 Overflow   : out std_logic_vector(NumResultBits - 1 downto 0)
	);
end NotMatch;

architecture Behavioral of NotMatch is

	-- constants

	constant MaxCount1 : integer := getMaxCount(InputWidth1, MaxTob1);
	constant MaxCount2 : integer := getMaxCount(InputWidth2, MaxTob2);

	type acceptarr is array (integer range <>) of std_logic;
	type acceptmat is array (integer range <>) of acceptarr(2 * InputWidth2 - 2 downto 0);
	type acceptmat_list is array (integer range <>) of acceptmat(2 * InputWidth1 - 2 downto 0);

	type deltaarrEta is array (integer range <>) of std_logic_vector(GenericEtaBitWidth - 1 downto 0);
	type deltaarrPhi is array (integer range <>) of std_logic_vector(GenericPhiBitWidth - 1 downto 0);
	type deltaarr11 is array (integer range <>) of std_logic_vector(2 * GenericEtaBitWidth downto 0);
	type deltamatEta is array (integer range <>) of deltaarrEta(2 * InputWidth2 - 2 downto 0);
	type deltamatPhi is array (integer range <>) of deltaarrPhi(2 * InputWidth2 - 2 downto 0);
	type deltamatRSqr is array (integer range <>) of deltaarr11(2 * InputWidth2 - 2 downto 0);

	type EtThreshold is array (integer range <>) of std_logic_vector(GenericEtBitWidth - 1 downto 0);
	type etaThreshArray is array (NumResultBits - 1 downto 0) of std_logic_vector(GenericEtaBitWidth - 1 downto 0);
	type RSqrThreshold is array (integer range <>) of std_logic_vector(2 * GenericEtaBitWidth downto 0);

	type thrAcceptArray1 is array (NumResultBits - 1 downto 0) of std_logic_vector(InputWidth1 - 1 downto 0);
	type thrAcceptArray2 is array (NumResultBits - 1 downto 0) of std_logic_vector(InputWidth2 - 1 downto 0);

	signal not_accept_tmp  : acceptmat_list(NumResultBits - 1 downto 0);
	signal not_accept_tmp2 : acceptmat_list(NumResultBits - 1 downto 0);

	signal thr_flag1 : thrAcceptArray1;
	signal thr_flag2 : thrAcceptArray2;

	signal deltaEta  : deltamatEta(2 * InputWidth1 - 2 downto 0);
	signal deltaPhi  : deltamatPhi(2 * InputWidth1 - 2 downto 0);
	signal deltaRSqr : deltamatRSqr(2 * InputWidth1 - 2 downto 0);

	signal MinEt1                             : EtThreshold(NumResultBits - 1 downto 0);
	signal MinEt2                             : EtThreshold(NumResultBits - 1 downto 0);
	signal MinEta1, MaxEta1, MinEta2, MaxEta2 : etaThreshArray;
	signal MaxRSqr                            : RSqrThreshold(NumResultBits - 1 downto 0);

	signal reg_Tobs1 : TOBArray(InputWidth1 - 1 downto 0);
	signal reg_Tobs2 : TOBArray(InputWidth2 - 1 downto 0);

	signal reg_parameters : ParameterArray;

	signal results_out  : std_logic_vector(NumResultBits - 1 downto 0);
	signal overflow_out : std_logic_vector(NumResultBits - 1 downto 0);

	signal anyAcc1, anyAcc2 : std_logic;

begin
	reg_Tobs1      <= Tob1;
	reg_Tobs2      <= Tob2;
	reg_parameters <= Parameters;

	genThrArr : for i in 0 to NumResultBits - 1 generate
		MinEt1(i)  <= reg_parameters(7 * i + 0)(GenericEtBitWidth - 1 downto 0);
		MinEt2(i)  <= reg_parameters(7 * i + 1)(GenericEtBitWidth - 1 downto 0);
		MinEta1(i) <= reg_parameters(7 * i + 2)(GenericEtaBitWidth - 1 downto 0);
		MaxEta1(i) <= reg_parameters(7 * i + 3)(GenericEtaBitWidth - 1 downto 0);
		MinEta2(i) <= reg_parameters(7 * i + 4)(GenericEtaBitWidth - 1 downto 0);
		MaxEta2(i) <= reg_parameters(7 * i + 5)(GenericEtaBitWidth - 1 downto 0);
		MaxRSqr(i) <= reg_parameters(7 * i + 6)(2 * GenericEtaBitWidth downto 0);
	end generate;

	min_tob_Et : process(reg_Tobs1, reg_Tobs2, MaxEta1, MaxEta2, MinEt1, MinEt2, MinEta1, MinEta2) -- Flag TOB with Et over threshold -- TODO: BUGGY?
		variable anyAccTmp1, anyAccTmp2 : std_logic;
	begin
		anyAccTmp1 := '0';
		anyAccTmp2 := '0';

		for k in 0 to (NumResultBits - 1) loop
			for i in 0 to (maxCount1 - 1) loop
				if (reg_Tobs1(i).Et > MinEt1(k) and is_in_eta_range_tob(reg_Tobs1(i), minEta1(k), maxEta1(k))) then
					thr_flag1(k)(i) <= '1';
					anyAccTmp1      := '1';
				else
					thr_flag1(k)(i) <= '0';
				end if;
			end loop;
			for i in 0 to (maxCount2 - 1) loop
				if (reg_Tobs2(i).Et > MinEt2(k) and is_in_eta_range_tob(reg_Tobs2(i), minEta2(k), maxEta2(k))) then
					thr_flag2(k)(i) <= '1';
					anyAccTmp2      := '1';
				else
					thr_flag2(k)(i) <= '0';
				end if;
			end loop;
		end loop;

		anyAcc1 <= anyAccTmp1;
		anyAcc2 <= anyAccTmp2;
	end process;

	gen1 : for i in 0 to MaxCount1 - 1 generate
		gen2 : for j in 0 to MaxCount2 - 1 generate
			dEta_inst : entity work.DeltaEtaCalc
				port map(
					eta1In      => reg_Tobs1(i).Eta,
					eta2In      => reg_Tobs2(j).Eta,
					deltaEtaOut => deltaEta(i)(j)
				);
			dPhi_inst : entity work.DeltaPhiCalc
				port map(phi1In      => reg_Tobs1(i).Phi,
					     phi2In      => reg_Tobs2(j).Phi,
					     deltaPhiOut => deltaPhi(i)(j));
			DeltaRSqrCalc_inst : entity work.DeltaRSqrCalc
				Port map(deltaEta  => deltaEta(i)(j),
					     deltaPhi  => deltaPhi(i)(j),
					     deltaRSqr => deltaRSqr(i)(j)
				);
		end generate;
	end generate;

	genResults : for k in 0 to NumResultBits - 1 generate
		gen1 : for i in 0 to MaxCount1 - 1 generate
			gen2 : for j in 0 to MaxCount2 - 1 generate
				not_accept_tmp(k)(i)(j) <= '1' when unsigned(deltaRSqr(i)(j)) <= unsigned(MaxRSqr(k)) else '0';
			end generate;
		end generate;

		genAcceptTmp2a : for i in 0 to MaxCount1 - 1 generate
			genAcceptTmp2b : for j in 0 to MaxCount2 - 1 generate
				not_accept_tmp2(k)(i)(j) <= not_accept_tmp(k)(i)(j) AND thr_flag1(k)(i) AND thr_flag2(k)(j);
			end generate;
		end generate;

		genOutAccept1 : for i in 0 to MaxCount1 - 1 generate
			genx : for j in 0 to MaxCount2 - 2 generate
				not_accept_tmp2(k)(i)(MaxCount2 + j) <= not_accept_tmp2(k)(i)(2 * j) OR not_accept_tmp2(k)(i)(2 * j + 1);
			end generate;
		end generate;

		genOutAccept2 : for i in 0 to MaxCount1 - 2 generate
			not_accept_tmp2(k)(MaxCount1 + i)(2 * MaxCount2 - 2) <= not_accept_tmp2(k)(2 * i)(2 * MaxCount2 - 2) OR not_accept_tmp2(k)(2 * i + 1)(2 * MaxCount2 - 2);
		end generate;

		results_out(k) <= not not_accept_tmp2(k)(2 * MaxCount1 - 2)(2 * MaxCount2 - 2) and anyAcc1 and anyAcc2;

	end generate;

	Results <= results_out;

	-----------------------------
	-- generate overflow bits  --
	-----------------------------
	genOverflows : for result in 0 to NumResultBits - 1 generate
		-- get overflow from input list
		overflow_out(result) <= Tob1(0).Overflow or Tob2(0).Overflow;
	end generate;

	Overflow <= overflow_out;

end Behavioral;
