------------------------------------------------------------------------------------------
-- Author/Modified by	: manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de
-- Description			: applies Et cu on two TOB lists; calculates the invariant masses 
--						  and applies cut on these
-- Number of registers	: 0
-- behavioral simulation : ok (~ 0.1% - 0.2% errors)
------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use work.L1TopoDataTypes.ALL;
use work.L1TopoFunctions.all;

entity InvariantMassInclusive2 is
	generic(InputWidth1   : integer := OutputWidthSelectEM;
		    InputWidth2   : integer := OutputWidthSelectEM;
		    MaxTob1       : integer := 0; -- number of TOBs which should be considered
		    MaxTob2       : integer := 0; -- number of TOBs which should be considered
		    ApplyEtaCut	  : integer := 0;
		    NumResultBits : integer := 1;
		    NumRegisters  : integer := 0 -- number of registers (0 or 1)
	);
	Port(ClockBus   : in  std_logic_vector(2 downto 0);
		 Tob1       : in  TOBArray(InputWidth1 - 1 downto 0);
		 Tob2       : in  TOBArray(InputWidth2 - 1 downto 0);
		 Parameters : in  ParameterArray;
		 Results    : out std_logic_vector(NumResultBits - 1 downto 0);
		 Overflow   : out std_logic_vector(NumResultBits - 1 downto 0)
	);
end InvariantMassInclusive2;

architecture Behavioral of InvariantMassInclusive2 is

	-- constants

	constant MaxCount1 : integer := getMaxCount(InputWidth1, MaxTob1);
	constant MaxCount2 : integer := getMaxCount(InputWidth2, MaxTob2);

	type Resultsarr is array (integer range <>) of std_logic_vector(InvariantMassSqrBitWidth - 1 downto 0);
	type Resultsmat is array (integer range <>) of Resultsarr(InputWidth2 - 1 downto 0);
	type thrAcceptArray1 is array (integer range <>) of std_logic_vector(InputWidth1 - 1 downto 0);
	type thrAcceptArray2 is array (integer range <>) of std_logic_vector(InputWidth2 - 1 downto 0);

	type etThreshArray is array (integer range <>) of std_logic_vector(GenericEtBitWidth - 1 downto 0);
	type Mthreshold is array (integer range <>) of std_logic_vector(InvariantMassSqrBitWidth - 1 downto 0);

	signal mass_sqr_results : Resultsmat(InputWidth1 - 1 downto 0);

	signal tob_in1 : TOBArray(InputWidth1 - 1 downto 0);
	signal tob_in2 : TOBArray(InputWidth2 - 1 downto 0);

	signal results_out  : std_logic_vector(NumResultBits - 1 downto 0);
	signal overflow_out : std_logic_vector(NumResultBits - 1 downto 0);

	signal MinEt1     : etThreshArray(NumResultBits - 1 downto 0);
	signal MinEt2     : etThreshArray(NumResultBits - 1 downto 0);
	signal MinEta1 : std_logic_vector(GenericAbsoluteEtaBitWidth - 1 downto 0);
	signal MaxEta1 : std_logic_vector(GenericAbsoluteEtaBitWidth - 1 downto 0);
	signal MinEta2 : std_logic_vector(GenericAbsoluteEtaBitWidth - 1 downto 0);
	signal MaxEta2 : std_logic_vector(GenericAbsoluteEtaBitWidth - 1 downto 0);
	signal MinMSqr    : Mthreshold(NumResultBits - 1 downto 0);
	signal MaxMSqr    : Mthreshold(NumResultBits - 1 downto 0);
	signal thraccept1 : thrAcceptArray1(NumResultBits - 1 downto 0);
	signal thraccept2 : thrAcceptArray2(NumResultBits - 1 downto 0);

begin
	tob_in1 <= Tob1;
	tob_in2 <= Tob2;

	gen_without_eta_cut : if ApplyEtaCut = 0 generate
		genThrArr : for i in 0 to NumResultBits - 1 generate
			MinEt1(i)  <= Parameters(4 * i + 0)(GenericEtBitWidth - 1 downto 0);
			MinEt2(i)  <= Parameters(4 * i + 1)(GenericEtBitWidth - 1 downto 0);
			MinMSqr(i) <= Parameters(4 * i + 2)(InvariantMassSqrBitWidth - 1 downto 0);
			MaxMSqr(i) <= Parameters(4 * i + 3)(InvariantMassSqrBitWidth - 1 downto 0);
		end generate;
	end generate;
	
	gen_with_eta_cut : if ApplyEtaCut = 1 generate
		genThrArr : for i in 0 to NumResultBits - 1 generate
			MinEt1(i)  <= Parameters(8 * i + 0)(GenericEtBitWidth - 1 downto 0);
			MinEt2(i)  <= Parameters(8 * i + 1)(GenericEtBitWidth - 1 downto 0);
			MinMSqr(i) <= Parameters(8 * i + 2)(InvariantMassSqrBitWidth - 1 downto 0);
			MaxMSqr(i) <= Parameters(8 * i + 3)(InvariantMassSqrBitWidth - 1 downto 0);
			MinEta1 <= Parameters(8 * i + 4)(GenericAbsoluteEtaBitWidth - 1 downto 0);
			MaxEta1 <= Parameters(8 * i + 5)(GenericAbsoluteEtaBitWidth - 1 downto 0);
			MinEta2 <= Parameters(8 * i + 6)(GenericAbsoluteEtaBitWidth - 1 downto 0);
			MaxEta2 <= Parameters(8 * i + 7)(GenericAbsoluteEtaBitWidth - 1 downto 0);
		end generate;
	end generate;

	min_tob_Et : process(tob_in1, tob_in2, MinEt1, MinEt2, MaxEta1, MaxEta2, MinEta1, MinEta2) -- Flag TOB with Et over threshold -- TODO: BUGGY?
	begin
		if (ApplyEtaCut = 0) then
			for k in 0 to (NumResultBits - 1) loop
				for i in 0 to (maxCount1 - 1) loop
					if (tob_in1(i).Et > MinEt1(k)) then
						thraccept1(k)(i) <= '1';
					else
						thraccept1(k)(i) <= '0';
					end if;
				end loop;
				for i in 0 to (maxCount2 - 1) loop
					if (tob_in2(i).Et > MinEt2(k)) then
						thraccept2(k)(i) <= '1';
					else
						thraccept2(k)(i) <= '0';
					end if;
				end loop;
			end loop;
		end if;
		if (ApplyEtaCut = 1) then
			for k in 0 to (NumResultBits - 1) loop
				for i in 0 to (maxCount1 - 1) loop
					if (tob_in1(i).Et > MinEt1(k) and is_in_eta_range_tob(tob_in1(i), MinEta1, MaxEta1)) then
						thraccept1(k)(i) <= '1';
					else
						thraccept1(k)(i) <= '0';
					end if;
				end loop;
				for i in 0 to (maxCount2 - 1) loop
					if (tob_in2(i).Et > MinEt2(k) and is_in_eta_range_tob(tob_in1(i), MinEta2, MaxEta2)) then
						thraccept2(k)(i) <= '1';
					else
						thraccept2(k)(i) <= '0';
					end if;
				end loop;
			end loop;
		end if;
	end process;

	gen1 : for i in 0 to MaxCount1 - 1 generate
		gen2 : for j in 0 to MaxCount2 - 1 generate
			Inv_mass_inst : entity work.InvMassCalc
				generic map(
					NumRegisters => NumRegisters
				)
				port map(ClockBus => ClockBus,
					     eta1     => tob_in1(i).Eta,
					     eta2     => tob_in2(j).Eta,
					     phi1     => tob_in1(i).Phi,
					     phi2     => tob_in2(j).Phi,
					     energy1  => tob_in1(i).Et,
					     energy2  => tob_in2(j).Et,
					     mass_sqr => mass_sqr_results(i)(j)
				);
		end generate;
	end generate;

	compare_thresholds : process(mass_sqr_results, thraccept1, thraccept2, MinMSqr, MaxMSqr) -- Compare delta eta with thresholds for all TOB pairs -- TODO: BUGGY?
		variable hits : std_logic_vector(NumResultBits - 1 downto 0);
	begin
		hits := (others => '0');
		for i in 0 to (maxCount1 - 1) loop -- loop over TOBs i and j
			for j in 0 to (maxCount2 - 1) loop
				for k in 0 to NumResultBits - 1 loop -- loop over thresholds
					if ((unsigned(mass_sqr_results(i)(j)) >= unsigned(MinMSqr(k))) and (unsigned(mass_sqr_results(i)(j)) <= unsigned(MaxMSqr(k))) and (thraccept1(k)(i) = '1') and (thraccept2(k)(j) = '1')) then
						hits(k) := '1';
					end if;
				end loop;
			end loop;
		end loop;
		results_out <= hits;
	end process;

	Results <= results_out;

	-----------------------------
	-- generate overflow bits  --
	-----------------------------
	genOverflows : for result in 0 to NumResultBits - 1 generate
		-- get overflow from input list
		overflow_out(result) <= Tob1(0).Overflow or Tob2(0).Overflow;
	end generate;

	Overflow <= overflow_out;

end Behavioral;

