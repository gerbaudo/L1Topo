------------------------------------------------------------------------------------------
-- Author/Modified by	: sebastian.artz@uni-mainz.de
-- Description			: calculates the ratio of all TOB combinations of applies an cut
-- Number of registers	: 0
------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use work.L1TopoDataTypes.ALL;
use work.L1TopoFunctions.all;

entity RatioMatch is
	generic(InputWidth1   : integer := 2;
		    InputWidth2   : integer := 120;
		    MaxTob1       : integer := 0; -- number of TOBs which should be considered
		    MaxTob2       : integer := 0; -- number of TOBs which should be considered
		    NumResultBits : integer := 1
	);
	Port(ClockBus   : std_logic_vector(2 downto 0);
		 Parameters : in  ParameterArray;
		 Tob1       : in  TOBArray(InputWidth1 - 1 downto 0);
		 Tob2       : in  TOBArray(InputWidth2 - 1 downto 0);
		 Results    : out std_logic_vector(NumResultBits - 1 downto 0);
		 Overflow   : out std_logic_vector(NumResultBits - 1 downto 0)
	);
end RatioMatch;

architecture Behavioral of RatioMatch is

	-- constants

	constant MaxCount1 : integer := getMaxCount(InputWidth1, MaxTob1);
	constant MaxCount2 : integer := getMaxCount(InputWidth2, MaxTob2);

	type acceptarr is array (integer range <>) of std_logic;
	type acceptmat is array (integer range <>) of acceptarr(2 * InputWidth2 - 2 downto 0);
	type acceptmat_list is array (integer range <>) of acceptmat(2 * InputWidth1 - 2 downto 0);

	type ratioArr is array (integer range <>) of std_logic_vector(RatioBitWidth - 1 downto 0);

	signal accept_tmp  : acceptmat_list(NumResultBits - 1 downto 0);
	signal accept_tmp2 : acceptmat_list(NumResultBits - 1 downto 0);
	signal ratioAccept : acceptmat_list(NumResultBits - 1 downto 0);

	signal thr_flag1 : std_logic_vector(InputWidth1 - 1 downto 0);
	signal thr_flag2 : std_logic_vector(InputWidth2 - 1 downto 0);

	signal MinEt1 : std_logic_vector(GenericEtBitWidth - 1 downto 0);
	signal MinEt2 : std_logic_vector(GenericEtBitWidth - 1 downto 0);

	signal tob_in1    : TOBArray(InputWidth1 - 1 downto 0);
	signal tob_in2    : TOBArray(InputWidth2 - 1 downto 0);
	signal ratioParam : ratioArr(NumResultBits - 1 downto 0);

	signal reg_parameters : ParameterArray;

	signal results_out  : std_logic_vector(NumResultBits - 1 downto 0);
	signal overflow_out : std_logic_vector(NumResultBits - 1 downto 0);

	constant TEN : unsigned(3 downto 0) := "1010";

begin
	tob_in1        <= Tob1;
	tob_in2        <= Tob2;
	reg_parameters <= Parameters;

	MinEt1 <= reg_parameters(0)(GenericEtBitWidth - 1 downto 0);
	MinEt2 <= reg_parameters(1)(GenericEtBitWidth - 1 downto 0);

	genThrArr : for i in 0 to NumResultBits - 1 generate
		ratioParam(i) <= reg_parameters(i + 2)(RatioBitWidth - 1 downto 0); -- can be interpreted as unsigned int with 1 digit decimal comma shift
	end generate;

	genThrFlag1 : for i in 0 to MaxCount1 - 1 generate
		thr_flag1(i) <= '1' when tob_in1(i).Et > MinEt1 else '0';
	end generate;

	genThrFlag2 : for i in 0 to MaxCount2 - 1 generate
		thr_flag2(i) <= '1' when tob_in2(i).Et > MinEt2 else '0';
	end generate;

	genResults : for k in 0 to NumResultBits - 1 generate
		genRatio1 : for i in 0 to MaxCount1 - 1 generate
			genRatio2 : for j in 0 to MaxCount2 - 1 generate
				ratioAccept(k)(i)(j) <= '1' when unsigned(tob_in1(i).Et) * TEN >= unsigned(ratioParam(k)) * unsigned(tob_in2(j).Et) else '0';
			end generate;
		end generate;

		gen1 : for i in 0 to MaxCount1 - 1 generate
			gen2 : for j in 0 to MaxCount2 - 1 generate
				accept_tmp(k)(i)(j) <= '1' when ((tob_in1(i).Eta = tob_in2(j).Eta) and (tob_in1(i).Phi = tob_in2(j).Phi)) else '0';
			end generate;
		end generate;

		genAcceptTmp2a : for i in 0 to MaxCount1 - 1 generate
			genAcceptTmp2b : for j in 0 to MaxCount2 - 1 generate
				accept_tmp2(k)(i)(j) <= accept_tmp(k)(i)(j) AND ratioAccept(k)(i)(j) AND thr_flag1(i) AND thr_flag2(j);
			end generate;
		end generate;

		genOutAccept1 : for i in 0 to MaxCount1 - 1 generate
			genx : for j in 0 to MaxCount2 - 2 generate
				accept_tmp2(k)(i)(MaxCount2 + j) <= accept_tmp2(k)(i)(2 * j) or accept_tmp2(k)(i)(2 * j + 1);
			end generate;
		end generate;

		genOutAccept2 : for i in 0 to MaxCount1 - 2 generate
			accept_tmp2(k)(MaxCount1 + i)(2 * MaxCount2 - 2) <= accept_tmp2(k)(2 * i)(2 * MaxCount2 - 2) or accept_tmp2(k)(2 * i + 1)(2 * MaxCount2 - 2);
		end generate;

		results_out(k) <= accept_tmp2(k)(2 * MaxCount1 - 2)(2 * MaxCount2 - 2);

	end generate;

	Results <= results_out;

	-----------------------------
	-- generate overflow bits  --
	-----------------------------
	genOverflows : for result in 0 to NumResultBits - 1 generate
		-- get overflow from input list
		overflow_out(result) <= Tob1(0).Overflow or Tob2(0).Overflow;
	end generate;

	Overflow <= overflow_out;

end Behavioral;
