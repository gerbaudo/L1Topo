------------------------------------------------------------------------------------------
-- Author/Modified by	: manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de
-- Description			: register for GenericTOB list
-- Number of registers	: 1
------------------------------------------------------------------------------------------

-- TODO: GenericTOB type now has more entries (EtSqr, Overflow, ...) -> update registers??? -> MET-TOB register???

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.L1TopoDataTypes.all;

entity TobRegister is
	generic(InputWidth : integer := 16  -- Width of the register (in generic TOB)
	);
	Port(clk     : in  std_logic;
		 enable  : in  std_logic;
		 reg_in  : in  TOBArray(InputWidth - 1 downto 0);
		 reg_out : out TOBArray(InputWidth - 1 downto 0)
	);
end TobRegister;

architecture Behavioral of TobRegister is
	type Et_arr is array (integer range <>) of std_logic_vector(GenericEtBitWidth - 1 downto 0);
	type etaarr is array (integer range <>) of std_logic_vector(GenericEtaBitWidth - 1 downto 0);
	type phiarr is array (integer range <>) of std_logic_vector(GenericPhiBitWidth - 1 downto 0);
	type overflowarr is array (integer range <>) of std_logic;

	signal reg_Et_tmp       : Et_arr(InputWidth - 1 downto 0);
	signal reg_eta_tmp      : etaarr(InputWidth - 1 downto 0);
	signal reg_phi_tmp      : phiarr(InputWidth - 1 downto 0);
	signal reg_overflow_tmp : overflowarr(InputWidth - 1 downto 0);

	signal reg_Et       : Et_arr(InputWidth - 1 downto 0);
	signal reg_eta      : etaarr(InputWidth - 1 downto 0);
	signal reg_phi      : phiarr(InputWidth - 1 downto 0);
	signal reg_overflow : overflowarr(InputWidth - 1 downto 0);
	signal clock        : std_logic;

begin
	clock <= clk;                       -- this signal is needed for the simulation

	genIn : for i in 0 to InputWidth - 1 generate
		reg_Et_tmp(i)       <= reg_in(i).Et;
		reg_eta_tmp(i)      <= reg_in(i).Eta;
		reg_phi_tmp(i)      <= reg_in(i).Phi;
		reg_overflow_tmp(i) <= reg_in(i).Overflow;
	end generate;

	process(clock)
	begin
		if (rising_edge(clock)) then
			if (enable = '1') then
				reg_Et       <= reg_Et_tmp;
				reg_eta      <= reg_eta_tmp;
				reg_phi      <= reg_phi_tmp;
				reg_overflow <= reg_overflow_tmp;
			end if;
		end if;
	end process;

	genOut : for i in 0 to InputWidth - 1 generate
		reg_out(i).Et       <= reg_Et(i);
		reg_out(i).Eta      <= reg_eta(i);
		reg_out(i).Phi      <= reg_phi(i);
		reg_out(i).Overflow <= reg_overflow(i);
	end generate;

end Behavioral;