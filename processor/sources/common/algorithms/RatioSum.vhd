------------------------------------------------------------------------------------------
-- Author/Modified by	: sebastian.artz@uni-mainz.de
-- Description			: calculates the ratio of MET (or MET squared) and JetHT calculated on the
--						  second input list and applies cut
-- IMPORTANT : if Ht saturates, the resultbit will be 0 and overflow will be 1
-- Number of registers	: 0
------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.L1TopoDataTypes.all;
use work.L1TopoFunctions.all;

entity RatioSum is
	generic(InputWidth1             : integer := 1; -- MET
		    InputWidth2             : integer := InputWidthJET; -- jets for HT
		    InputWidth3             : integer := 1; -- cluster TOBs
		    MaxTob1                 : integer := 0; -- number of TOBs which should be considered
		    MaxTob2                 : integer := 0; -- number of TOBs which should be considered
		    MaxTob3                 : integer := 0;
		    NumResultBits           : integer := 1;
		    UseCluster05Granularity : integer := 0
	);
	port(
		Tob1       : in  TOBArray(InputWidth1 - 1 downto 0); -- MET
		Tob2       : in  TOBArray(InputWidth2 - 1 downto 0); -- jets for HT
		Tob3       : in  TOBArray(InputWidth3 - 1 downto 0); -- TOBs to be added to the Ht
		Parameters : in  ParameterArray;
		ClockBus   : in  std_logic_vector(2 downto 0);
		Results    : out std_logic_vector(NumResultBits - 1 downto 0);
		Overflow   : out std_logic_vector(NumResultBits - 1 downto 0)
	);
end RatioSum;

architecture Behavioral of RatioSum is

	-- constants

	constant MaxCount1 : integer := getMaxCount(InputWidth1, MaxTob1);
	constant MaxCount2 : integer := getMaxCount(InputWidth2, MaxTob2);
	constant MaxCount3 : integer := getMaxCount(InputWidth3, MaxTob3);

	-- Type declarations

	type ratioArr is array (integer range <>) of std_logic_vector(RatioBitWidth - 1 downto 0);
	type METArray is array (integer range <>) of std_logic_vector(GenericEtBitWidth + 1 - 1 downto 0);

	-- Internal signal declarations

	signal ratioParam : ratioArr(NumResultBits - 1 downto 0);

	signal tob_in1  : TOBArray(InputWidth1 - 1 downto 0);
	signal tob_in2  : TOBArray(InputWidth2 - 1 downto 0);
	signal tob_in3  : TOBArray(InputWidth3 - 1 downto 0);
	signal clock    : std_logic;
	signal MinEtHt  : std_logic_vector(JetHtBitWidth - 1 downto 0);
	signal MinEtMet : std_logic_vector(GenericEtBitWidth - 1 downto 0);

	signal thr_flag1                 : std_logic_vector(InputWidth1 - 1 downto 0);
	signal thr_flag2, AcceptFinalSum : std_logic;

	signal results_out  : std_logic_vector(NumResultBits - 1 downto 0);
	signal overflow_out : std_logic_vector(NumResultBits - 1 downto 0);

	signal ParametersHt, ParametersSumTob3 : ParameterArray;
	signal HtSaturation, SumTob3Saturation : std_logic;
	signal HtSum, Tob3Sum                  : std_logic_vector(JetHtBitWidth - 1 downto 0);
	signal MinFinalSum                     : std_logic_vector(JetHtBitWidth + 1 - 1 downto 0);
	signal finalSum                        : std_logic_vector(JetHtBitWidth + 1 - 1 downto 0);
	signal HtOverflow, SumTob3Overflow     : std_logic;
	signal MET                             : METArray(InputWidth1 - 1 downto 0);

	constant TEN : unsigned(3 downto 0) := "1010";

begin
	clock   <= ClockBus(0);
	tob_in1 <= Tob1;
	tob_in2 <= Tob2;
	tob_in3 <= Tob3;

	ParametersHt(0)      <= Parameters(0); -- minEt (Ht)
	ParametersHt(1)      <= Parameters(1); -- minEta (Ht)
	ParametersHt(2)      <= Parameters(2); -- maxEta (Ht)
	ParametersSumTob3(0) <= Parameters(3); -- minEt (SumTob3)
	ParametersSumTob3(1) <= Parameters(4); -- minEta (SumTob3)
	ParametersSumTob3(2) <= Parameters(5); -- maxEta (SumTob3)
	MinEtMet             <= Parameters(6)(GenericEtBitWidth - 1 downto 0);
	MinEtHt              <= Parameters(7)(JetHtBitWidth - 1 downto 0);
	MinFinalSum          <= Parameters(8)(JetHtBitWidth + 1 - 1 downto 0);

	genThrArr : for i in 0 to NumResultBits - 1 generate
		ratioParam(i) <= Parameters(9 + i)(RatioBitWidth - 1 downto 0); -- can be interpreted as unsigned int with 1 digit decimal comma shift
	end generate;

	-- sum of TOB2 list
	gen_Ht : entity work.JetHtInternal
		generic map(InputWidth => InputWidth2,
			        MaxTob     => MaxCount2,
			        HtBitWidth => JetHtBitWidth
		)
		Port Map(ClockBus   => ClockBus,
			     Parameters => ParametersHt,
			     Tob        => tob_in2,
			     Saturation => HtSaturation,
			     SumValue   => HtSum,
			     Overflow   => HtOverflow
		);

	-- sum of TOB3 list
	gen_Sum_Tob3 : entity work.JetHtInternal
		generic map(InputWidth => InputWidth3,
			        MaxTob     => MaxCount3,
			        HtBitWidth => JetHtBitWidth
		)
		Port Map(ClockBus   => ClockBus,
			     Parameters => ParametersSumTob3,
			     Tob        => tob_in3,
			     Saturation => SumTob3Saturation,
			     SumValue   => Tob3Sum,
			     Overflow   => SumTob3Overflow
		);

	gen_granularity1 : if (UseCluster05Granularity = 0) generate
		finalSum <= std_logic_vector(unsigned('0' & HtSum) + unsigned('0' & Tob3Sum));
		gen_met_1 : for i in 0 to InputWidth1 - 1 generate
			MET(i) <= '0' & tob_in1(i).Et;
		end generate;
	end generate;

	gen_granularity05 : if (UseCluster05Granularity = 1) generate
		finalSum <= std_logic_vector(unsigned(HtSum & '0') + unsigned('0' & Tob3Sum));
		gen_met_2 : for i in 0 to InputWidth1 - 1 generate
			MET(i) <= tob_in1(i).Et & '0';
		end generate;
	end generate;

	genThrFlag1 : for i in 0 to MaxCount1 - 1 generate
		thr_flag1(i) <= '1' when tob_in1(i).Et > MinEtMet else '0';
	end generate;

	thr_flag2 <= '1' when HtSum > MinEtHt else '0';

	AcceptFinalSum <= '1' when finalSum > MinFinalSum else '0';

	compare_thresholds : process(finalSum, MET, HtSaturation, SumTob3Saturation, thr_flag1, thr_flag2, AcceptFinalSum, ratioParam)
		variable hits : std_logic_vector(NumResultBits - 1 downto 0);
	begin
		hits := (others => '0');
		for i in 0 to (MaxCount1 - 1) loop -- loop over TOBs i and j
			for k in 0 to NumResultBits - 1 loop -- loop over thresholds
				if (unsigned(MET(i)) * TEN >= unsigned(ratioParam(k)) * unsigned(finalSum) and (HtSaturation = '0') and (SumTob3Saturation = '0') and (thr_flag1(i) = '1') and (thr_flag2 = '1') and (AcceptFinalSum = '1') and (unsigned(Tob3Sum) > 0)
				) then
					hits(k) := '1';
				end if;
			end loop;
		end loop;
		results_out <= hits;
	end process;

	Results <= results_out;

	-----------------------------
	-- generate overflow bits  --
	-----------------------------
	genOverflows : for result in 0 to NumResultBits - 1 generate
		-- get overflow from input list
		overflow_out(result) <= Tob1(0).Overflow or Tob2(0).Overflow or HtOverflow or HtSaturation or SumTob3Overflow or SumTob3Saturation;
	end generate;

	Overflow <= overflow_out;

end Behavioral;