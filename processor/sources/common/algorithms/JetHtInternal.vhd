-----------------------------------------------------------------------------------------
-- Author/Modified by	: manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de
-- Description			: calculates the sum of input list; only for internal use
-- Number of registers	: 0
------------------------------------------------------------------------------------------

--------------
-- includes --
--------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.L1TopoDataTypes.all;
use work.L1TopoFunctions.all;

------------------------
-- entity declaration --
------------------------

entity JetHtInternal is
	generic(InputWidth : integer := InputWidthJET; --number of input TOBs
		    MaxTob     : integer := 0;  -- number of TOBs which should be considered
		    HtBitWidth : integer := 14
	);
	Port(ClockBus   : in  std_logic_vector(2 downto 0);
		 Parameters : in  ParameterArray;
		 Tob        : in  TOBArray(InputWidth - 1 downto 0);
		 Saturation : out std_logic;
		 SumValue   : out std_logic_vector(HtBitWidth - 1 downto 0);
		 Overflow   : out std_logic
	);
end JetHtInternal;

-----------------------------------------
-- behavioral description of algorithm --
-----------------------------------------

architecture Behavioral of JetHtInternal is

	-- constants

	constant maxCount : integer := getMaxCount(InputWidth, MaxTob);
	--	constant HtBitWidth : integer := 14;

	----------------------
	-- type definitions --
	----------------------

	type sumarr is array (natural range <>) of unsigned(HtBitWidth + 1 - 1 downto 0);

	-------------
	-- signals --
	-------------
	-- parameters:
	signal MinEt          : std_logic_vector(GenericEtBitWidth - 1 downto 0);
	signal MinEta, MaxEta : std_logic_vector(GenericEtaBitWidth - 1 downto 0);
	signal generic_in     : TOBArray(InputWidth - 1 downto 0);
	signal sel_jets       : TOBArray(InputWidth - 1 downto 0);
	signal tmp_sum        : sumarr(2 * InputWidth - 2 downto 0);
	signal sum            : std_logic_vector(HtBitWidth - 1 downto 0);
	signal overMaxEt      : std_logic_vector(2 * InputWidth - 2 downto 0);

begin

	-- read parameters

	MinEt  <= Parameters(0)(GenericEtBitWidth - 1 downto 0);
	MinEta <= Parameters(1)(GenericEtaBitWidth - 1 downto 0);
	MaxEta <= Parameters(2)(GenericEtaBitWidth - 1 downto 0);

	generic_in <= Tob;

	----------------------
	-- select TOBs (Et) --
	----------------------
	genInp : for i in 0 to maxCount - 1 generate
		sel_jets(i) <= generic_in(i) when (generic_in(i).Et > MinEt and is_in_eta_range_tob(generic_in(i), minEta, maxEta)) else empty_tob;
	end generate;

	-------------------
	-- calculate sum --
	-------------------
	genTmpSum : for i in 0 to maxCount - 1 generate
		tmp_sum(i)(GenericEtBitWidth - 1 downto 0)      <= unsigned(sel_jets(i).Et);
		tmp_sum(i)(HtBitWidth downto GenericEtBitWidth) <= (others => '0');
	end generate;

	genSumTree : for i in 0 to maxCount - 2 generate
		tmp_sum(maxCount + i) <= tmp_sum(2 * i) + tmp_sum(2 * i + 1);
	end generate;

	-- use summed values as saturation indicator
	genOverMax1 : for i in 0 to maxCount - 2 generate
		overMaxEt(i) <= tmp_sum(maxCount + i)(HtBitWidth);
	end generate;
	overMaxEt(maxCount - 1) <= '0';     -- to have an even number of saturation bits

	-- OR all the saturation indicators
	genOverMax2 : for i in 0 to maxCount - 2 generate
		overMaxEt(maxCount + i) <= overMaxEt(2 * i) OR overMaxEt(2 * i + 1);
	end generate;

	sum <= std_logic_vector(tmp_sum(2 * maxCount - 2)(HtBitWidth - 1 downto 0));

	Saturation <= overMaxEt(2 * maxCount - 2);

	Overflow <= Tob(0).Overflow;

	SumValue <= sum;

end Behavioral;
