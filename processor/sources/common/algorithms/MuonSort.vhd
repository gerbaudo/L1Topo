------------------------------------------------------------------------------------------
-- Author/Modified by	: manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de
-- Description			: applies eta cut to muonOB list, sorts TOBs,
--						  converts them to GenericTOBs and returns short sorted list
-- Number of registers	: 2
------------------------------------------------------------------------------------------

--------------
-- includes --
--------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use work.l1topo_package.all;
use work.L1TopoDataTypes.all;
use work.L1TopoFunctions.all;

------------------------
-- entity declaration --
------------------------

entity MuonSort is
	generic(InputWidth         : integer := InputWidthMU; --number of input TOBs
		    InputWidth1stStage : integer := InputWidth1stStageSortMU; --number of TOBs in 1st stage
		    OutputWidth        : integer := OutputWidthSortMU --number of sorted output TOBs
	);
	Port(ClockBus     : in  std_logic_vector(2 downto 0);
		 Parameters   : in  ParameterArray;
		 MuonTobArray : in  MuonArray(InputWidth - 1 downto 0);
		 TobArrayOut  : out TOBArray(OutputWidth - 1 downto 0)
	);
end MuonSort;

-----------------------------------------
-- behavioral description of algorithm --
-----------------------------------------

architecture Behavioral of MuonSort is
	constant NumSublists : integer := 4;

	type MuonMatrix is array (3 - 1 downto 0) of MuonArray(InputWidth - 1 downto 0);
	type MuonMatrixOut is array (3 - 1 downto 0) of MuonArray(OutputWidth - 1 downto 0);
	type SelectPatternMatrix is array (3 - 1 downto 0) of std_logic_vector(OutputWidth - 1 downto 0);
	type IntArray is array (3 - 1 downto 0) of integer range 0 to OutputWidth - 1;
	type ShiftedTobsArray is array (3 - 1 downto 0) of MuonArray(3 * OutputWidth - 1 downto 0);

	-------------
	-- signals --
	-------------
	-- parameters:
	signal MinEta        : std_logic_vector(GenericAbsoluteEtaBitWidth - 1 downto 0);
	signal MaxEta        : std_logic_vector(GenericAbsoluteEtaBitWidth - 1 downto 0);
	-- selected input TOBs (Eta):
	signal generic_in    : TOBArray(InputWidth - 1 downto 0);
	signal muons_sorted  : MuonMatrix;
	signal muons_aligned : MuonMatrixOut;
	-- sorted TOBs after first stage (Et):

	-- sorted TOBs after second stage
	signal TobsShifted                  : ShiftedTobsArray;
	signal TobsShiftedMerged            : MuonArray(3 * OutputWidth - 1 downto 0);
	signal reg_out_3                    : TOBArray(OutputWidth - 1 downto 0);
	signal overflow_reg1, overflow_reg2 : std_logic;
	signal select_pattern_out           : SelectPatternMatrix;
	signal NumTobsOut                   : IntArray;

	signal clkFast_int     : std_logic;
	signal clk40_int       : std_logic;
	signal overflowPtLists : std_logic_vector(3 - 1 downto 0);
	signal overflow_octant : std_logic;

begin
	clk40_int   <= ClockBus(0);
	clkFast_int <= ClockBus(2);

	-- read parameters
	MinEta <= Parameters(0)(MuonEtaBitWidth - 1 downto 0);
	MaxEta <= Parameters(1)(MuonEtaBitWidth - 1 downto 0);

	----------------------------------------------------------
	-- translate to generic TOB and select TOBs (Eta range) --
	----------------------------------------------------------

	sel_eta_range : process(MinEta, MaxEta, MuonTobArray, generic_in)
		variable tmpOverflow : std_logic;
	begin
		tmpOverflow := '0';

		for j in 0 to InputWidth - 1 loop
			for k in 0 to 3 - 1 loop
				muons_sorted(k)(j) <= emptyMuonTOB;
			end loop;
		end loop;

		for i in 0 to InputWidth - 1 loop
			-- translate to generic TOB			
			if (MuonTobArray(i).Pt /= "11") then
				muons_sorted(to_integer(unsigned(MuonTobArray(i).Pt)))(i) <= MuonTobArray(i);
			else
				tmpOverflow := '1';
			end if;
		end loop;
		overflow_octant <= tmpOverflow;
	end process;

	----------------------------------------------------------------------
	-- first stage: sort the input TOBs in groups of InputWidth1stStage --
	----------------------------------------------------------------------

	gen_merge_i : for i in 3 - 1 downto 0 generate
		gen_select_merge_muon : entity work.SelectMergeMuon
			generic map(
				InputWidthBlocks   => NumSublists,
				InputWidthPerBlock => InputWidth / NumSublists,
				OutputWidth        => OutputWidth,
				DoPtCut            => 0
			)
			port map(
				clk40              => ClockBus(0),
				clkFast            => ClockBus(2),
				Parameters         => Parameters,
				inp                => muons_sorted(i),
				oup                => muons_aligned(i),
				--				select_pattern_in  => select_pattern_in,
				select_pattern_out => select_pattern_out(i),
				NumTobsOut         => NumTobsOut(i),
				overflow           => overflowPtLists(i)
			);

	end generate;

	gen_shift_process : process(NumTobsOut(2), muons_aligned(2), NumTobsOut(1), muons_aligned(0), muons_aligned(1))
		variable shift1, shift0 : integer;
	begin
		shift1 := integer(NumTobsOut(2));
		shift0 := integer(NumTobsOut(2)) + integer(NumTobsOut(1));

		for i in 0 to 3 * OutputWidth - 1 loop
			if (i >= OutputWidth) then
				TobsShifted(2)(i) <= emptyMuonTOB;
			else
				TobsShifted(2)(i) <= muons_aligned(2)(i);
			end if;

			if ((i < shift1) or (i >= shift1 + OutputWidth)) then
				TobsShifted(1)(i) <= emptyMuonTOB;
			else
				TobsShifted(1)(i) <= muons_aligned(1)(i - shift1);
			end if;

			if ((i < shift0) or (i >= shift0 + OutputWidth)) then
				TobsShifted(0)(i) <= emptyMuonTOB;
			else
				TobsShifted(0)(i) <= muons_aligned(0)(i - shift0);
			end if;
		end loop;
	end process;

	gen_or : for i in 0 to 3 * OutputWidth - 1 generate
		TobsShiftedMerged(i).Pt           <= TobsShifted(0)(i).Pt or TobsShifted(1)(i).Pt or TobsShifted(2)(i).Pt;
		TobsShiftedMerged(i).Eta          <= TobsShifted(0)(i).Eta or TobsShifted(1)(i).Eta or TobsShifted(2)(i).Eta;
		TobsShiftedMerged(i).Phi          <= TobsShifted(0)(i).Phi or TobsShifted(1)(i).Phi or TobsShifted(2)(i).Phi;
		TobsShiftedMerged(i).no_candidate <= TobsShifted(0)(i).no_candidate and TobsShifted(1)(i).no_candidate and TobsShifted(2)(i).no_candidate;
	end generate;

	gen_generic : process(ClockBus)
	begin
		if (rising_edge(ClockBus(0))) then
			for i in OutputWidth - 1 downto 0 loop
				if (TobsShiftedMerged(i).no_candidate = '1') then
					reg_out_3(i) <= empty_tob;
				else
					reg_out_3(i) <= to_GenericTOB(TobsShiftedMerged(i));
				end if;
			end loop;
		end if;
	end process;

	--	--set Overflow
	gen_overflow_reg1 : entity work.StdLogicRegister
		port map(clk     => ClockBus(0),
			     enable  => '1',
			     reg_in  => MuonTobArray(0).Overflow or overflow_octant,
			     reg_out => overflow_reg1
		);

	gen_overflow_reg2 : entity work.StdLogicRegister
		port map(clk     => ClockBus(0),
			     enable  => '1',
			     reg_in  => overflow_reg1,
			     reg_out => overflow_reg2
		);

	-- workaround to set overflow bit and reordering of TOBs
	gen_set_overflow : for i in 0 to OutputWidth - 1 generate
		TobArrayOut(i).Et  <= reg_out_3(i).Et;
		TobArrayOut(i).Eta <= reg_out_3(i).Eta;
		TobArrayOut(i).Phi <= reg_out_3(i).Phi;
	end generate;

	TobArrayOut(0).Overflow <= overflow_reg2; -- or overflowAllLists;

	-- only needed for simulation
	gen_set_dummy_overflow : for i in 1 to OutputWidth - 1 generate
		TobArrayOut(i).Overflow <= '0';
	end generate;

end Behavioral;
