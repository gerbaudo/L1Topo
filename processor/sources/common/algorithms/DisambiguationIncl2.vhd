------------------------------------------------------------------------------------------
-- Author/Modified by	: manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de, 
--                        aschulte@cern.ch
-- Description			: Disambiguation: Checks if coordinates are identical
--                        (ClusterTOBs) or if dRSqr > parameter (JetTOBs)
-- Number of registers	: 0 
-- Simulation: ok
-- Python test:
------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.L1TopoDataTypes.all;
use work.L1TopoFunctions.all;

entity DisambiguationIncl2 is
	generic(
		InputWidth1   : integer := 8;
		InputWidth2   : integer := 8;
		MaxTob1       : integer := 0;   -- number of TOBs which should be considered
		MaxTob2       : integer := 0;   -- number of TOBs which should be considered
		NumResultBits : integer := 1;   -- Number of result bits
		ClusterOnly   : integer := 0;   -- "0": checks for deltaRSqr > DisambDRSqr, "1": checks for identical coordinates 
		ApplyDR       : integer := 0    -- "0": no DR requirement, "1": DR requirement (only for ClusterTOBs)
	);
	port(
		Tob1       : in  TOBArray(InputWidth1 - 1 downto 0); -- Two arrays of TOBs
		Tob2       : in  TOBArray(InputWidth2 - 1 downto 0);
		Parameters : in  ParameterArray;
		ClockBus   : in  std_logic_vector(2 downto 0);
		Results    : out std_logic_vector(NumResultBits - 1 downto 0);
		Overflow   : out std_logic_vector(NumResultBits - 1 downto 0)
	);
end DisambiguationIncl2;

architecture Behavioral of DisambiguationIncl2 is

	-- constants

	constant maxCount1 : integer := getMaxCount(InputWidth1, MaxTob1);
	constant maxCount2 : integer := getMaxCount(InputWidth2, MaxTob2);

	-- Type declarations

	type deta is array (InputWidth2 - 1 downto 0) of std_logic_vector(GenericEtaBitWidth - 1 downto 0);
	type detaarray is array (InputWidth1 - 1 downto 0) of deta;
	type dphi is array (InputWidth2 - 1 downto 0) of std_logic_vector(GenericPhiBitWidth - 1 downto 0);
	type dphiarray is array (InputWidth1 - 1 downto 0) of dphi;
	type drsqr is array (InputWidth2 - 1 downto 0) of std_logic_vector(GenericRSqrBitWidth - 1 downto 0);
	type drsqrarray is array (InputWidth1 - 1 downto 0) of drsqr;
	type etThreshArray is array (NumResultBits - 1 downto 0) of std_logic_vector(GenericEtBitWidth - 1 downto 0);
	type rsqrThreshArray is array (NumResultBits - 1 downto 0) of std_logic_vector(GenericRSqrBitWidth - 1 downto 0);
	type thrAcceptArray1 is array (NumResultBits - 1 downto 0) of std_logic_vector(InputWidth1 - 1 downto 0);
	type thrAcceptArray2 is array (NumResultBits - 1 downto 0) of std_logic_vector(InputWidth2 - 1 downto 0);

	-- Internal signal declarations

	signal clock   : std_logic;
	signal tob_in1 : TOBArray(InputWidth1 - 1 downto 0);
	signal tob_in2 : TOBArray(InputWidth2 - 1 downto 0);

	signal MinEt1, MinEt2 : etThreshArray;
	signal DisambDRSqrMin : rsqrThreshArray;
	signal DisambDRSqrMax : rsqrThreshArray;

	signal deltaEta    : detaarray;
	signal deltaPhi    : dphiarray;
	signal deltaRSqr   : drsqrarray;
	signal thraccept_1 : thrAcceptArray1;
	signal thraccept_2 : thrAcceptArray2;

	signal results_out  : std_logic_vector(NumResultBits - 1 downto 0);
	signal overflow_out : std_logic_vector(NumResultBits - 1 downto 0);

begin

	-- Behavioral

	clock   <= ClockBus(0);
	tob_in1 <= Tob1;
	tob_in2 <= Tob2;

	ClusterOnly_1 : if ClusterOnly = 1 and ApplyDR = 0 generate -- extract 2 parameters (if only ClusterTOBs)
		set_ranges_1 : for k in 0 to (NumResultBits - 1) generate
			MinEt1(k) <= Parameters(0 + (2 * k))(GenericEtBitWidth - 1 downto 0);
			MinEt2(k) <= Parameters(1 + (2 * k))(GenericEtBitWidth - 1 downto 0);
		end generate;
	end generate;

	noClusterOnly_1 : if ClusterOnly = 0 and ApplyDR = 0 generate -- extract 3 parameters (if also other TOBs --> dR parameter needed)
		set_ranges_2 : for k in 0 to (NumResultBits - 1) generate
			MinEt1(k)         <= Parameters(0 + (3 * k))(GenericEtBitWidth - 1 downto 0);
			MinEt2(k)         <= Parameters(1 + (3 * k))(GenericEtBitWidth - 1 downto 0);
			DisambDRSqrMin(k) <= Parameters(2 + (3 * k))(GenericRSqrBitWidth - 1 downto 0);
		end generate;
	end generate;

	DR_1 : if ApplyDR = 1 generate      -- extract 4 parameters (if DR cut is applied)                                                                                                                  
		set_ranges_3 : for k in 0 to (NumResultBits - 1) generate
			MinEt1(k)         <= Parameters(0 + (4 * k))(GenericEtBitWidth - 1 downto 0);
			MinEt2(k)         <= Parameters(1 + (4 * k))(GenericEtBitWidth - 1 downto 0);
			DisambDRSqrMin(k) <= Parameters(2 + (4 * k))(GenericRSqrBitWidth - 1 downto 0);
			DisambDRSqrMax(k) <= Parameters(3 + (4 * k))(GenericRSqrBitWidth - 1 downto 0);
		end generate;
	end generate;

	min_tob_Et : process(tob_in1, tob_in2, MinEt1, MinEt2) -- Flag TOBs with Et over threshold
	begin
		for k in 0 to (NumResultBits - 1) loop
			for i in 0 to (maxCount1 - 1) loop
				if (tob_in1(i).Et > MinEt1(k)) then
					thraccept_1(k)(i) <= '1';
				else
					thraccept_1(k)(i) <= '0';
				end if;
			end loop;
			for j in 0 to (maxCount2 - 1) loop
				if (tob_in2(j).Et > MinEt2(k)) then
					thraccept_2(k)(j) <= '1';
				else
					thraccept_2(k)(j) <= '0';
				end if;
			end loop;
		end loop;
	end process;

	noClusterOnly_2_or_DR_2 : if (ClusterOnly = 0 and ApplyDR = 0) or ApplyDR = 1 generate -- Calculate DeltaRSqr
		DeltaRSqrCalc_1 : for i in 0 to (maxCount1 - 1) generate
			DeltaRSqrCalc_2 : for j in 0 to (maxCount2 - 1) generate
				DeltaEtaCalc_inst : entity work.DeltaEtaCalc
					port map(eta1In      => Tob1(i).Eta,
						     eta2In      => Tob2(j).Eta,
						     deltaEtaOut => deltaEta(i)(j));
				DeltaPhiCalc_inst : entity work.DeltaPhiCalc
					port map(phi1In      => Tob1(i).Phi,
						     phi2In      => Tob2(j).Phi,
						     deltaPhiOut => deltaPhi(i)(j));
				DeltaRSqrCalc_inst : entity work.DeltaRSqrCalc
					Port map(deltaEta  => deltaEta(i)(j),
						     deltaPhi  => deltaPhi(i)(j),
						     deltaRSqr => deltaRSqr(i)(j));
			end generate;
		end generate;
	end generate;

	ClusterOnly_noDR : if ClusterOnly = 1 and ApplyDR = 0 generate -- Compare coordinates (accept if not equal) for TOB pairs over threshold
		compare_thresholds_1 : process(Tob1, Tob2, thraccept_1, thraccept_2)
			variable hits : std_logic_vector(NumResultBits - 1 downto 0);
		begin
			hits := (others => '0');
			for i in 0 to (maxCount1 - 1) loop -- loop over TOBs i and j
				for j in 0 to (maxCount2 - 1) loop
					for k in 0 to NumResultBits - 1 loop -- loop over thresholds
						if (((Tob1(i).Eta /= Tob2(j).Eta) or (Tob1(i).Phi /= Tob2(j).Phi)) and (thraccept_1(k)(i) = '1') and (thraccept_2(k)(j) = '1')) then
							hits(k) := '1';
						end if;
					end loop;
				end loop;
			end loop;
			results_out <= hits;
		end process;
	end generate;

	ClusterOnly_DR : if ClusterOnly = 1 and ApplyDR = 1 generate -- Compare coordinates (accept if not equal and dR<=max) for TOB pairs over threshold
		compare_thresholds_1 : process(Tob1, Tob2, deltaRSqr, DisambDRSqrMax, DisambDRSqrMin, thraccept_1, thraccept_2)
			variable hits : std_logic_vector(NumResultBits - 1 downto 0);
		begin
			hits := (others => '0');
			for i in 0 to (maxCount1 - 1) loop -- loop over TOBs i and j
				for j in 0 to (maxCount2 - 1) loop
					for k in 0 to NumResultBits - 1 loop -- loop over thresholds
						if (((Tob1(i).Eta /= Tob2(j).Eta) or (Tob1(i).Phi /= Tob2(j).Phi)) and (deltaRSqr(i)(j) <= DisambDRSqrMax(k)) and (deltaRSqr(i)(j) > DisambDRSqrMin(k)) and (thraccept_1(k)(i) = '1') and (thraccept_2(k)(j) = '1')) then
							hits(k) := '1';
						end if;
					end loop;
				end loop;
			end loop;
			results_out <= hits;
		end process;
	end generate;

	noClusterOnly_noDR : if ClusterOnly = 0 and ApplyDR = 0 generate -- Compare deltaRSqr with parameter (accept if dR>min) for TOB pairs over threshold
		compare_thresholds_2 : process(deltaRSqr, DisambDRSqrMin, thraccept_1, thraccept_2)
			variable hits : std_logic_vector(NumResultBits - 1 downto 0);
		begin
			hits := (others => '0');
			for i in 0 to (maxCount1 - 1) loop -- loop over TOBs i and j
				for j in 0 to (maxCount2 - 1) loop
					for k in 0 to NumResultBits - 1 loop -- loop over thresholds
						if ((deltaRSqr(i)(j) > DisambDRSqrMin(k)) and (thraccept_1(k)(i) = '1') and (thraccept_2(k)(j) = '1')) then
							hits(k) := '1';
						end if;
					end loop;
				end loop;
			end loop;
			results_out <= hits;
		end process;
	end generate;

	noClusterOnly_DR : if ClusterOnly = 0 and ApplyDR = 1 generate -- Compare deltaRSqr with parameters (accept if dR>min and dR<=max) for TOB pairs over threshold
		compare_thresholds_3 : process(deltaRSqr, DisambDRSqrMin, DisambDRSqrMax, thraccept_1, thraccept_2)
			variable hits : std_logic_vector(NumResultBits - 1 downto 0);
		begin
			hits := (others => '0');
			for i in 0 to (maxCount1 - 1) loop -- loop over TOBs i and j
				for j in 0 to (maxCount2 - 1) loop
					for k in 0 to NumResultBits - 1 loop -- loop over thresholds
						if ((deltaRSqr(i)(j) > DisambDRSqrMin(k)) and (deltaRSqr(i)(j) <= DisambDRSqrMax(k)) and (thraccept_1(k)(i) = '1') and (thraccept_2(k)(j) = '1')) then
							hits(k) := '1';
						end if;
					end loop;
				end loop;
			end loop;
			results_out <= hits;
		end process;
	end generate;

	Results <= results_out;

	-----------------------------
	-- generate overflow bits  --
	-----------------------------
	genOverflows : for result in 0 to NumResultBits - 1 generate
		-- get overflow from input list
		overflow_out(result) <= Tob1(0).Overflow or Tob2(0).Overflow;
	end generate;

	Overflow <= overflow_out;

end Behavioral;
