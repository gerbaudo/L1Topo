------------------------------------------------------------------------------------------
-- Author/Modified by	: manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de
-- Description			: merges selected lists using a faster clock
-- Number of registers	: 1
------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.L1TopoDataTypes.all;
use work.L1TopoFunctions.all;

entity SelectMergePresorted is
	generic(InputWidthBlocks   : integer := 4;
		    InputWidthPerBlock : integer := 10
	);
	Port(clk40              : in  std_logic;
		 clkFast            : in  std_logic;
		 inp                : in  TOBArray(InputWidthPerBlock * (InputWidthBlocks) - 1 downto 0);
		 oup                : out TOBArray(InputWidthPerBlock - 1 downto 0);
		 select_pattern_in  : in  std_logic_vector(InputWidthPerBlock * (InputWidthBlocks) - 1 downto 0);
		 select_pattern_out : out std_logic_vector(InputWidthPerBlock - 1 downto 0);
		 overflow           : out std_logic
	);
end SelectMergePresorted;

architecture Behavioral of SelectMergePresorted is
	signal packedTOBs, oup_int : TOBArray(InputWidthPerBlock - 1 downto 0);
	signal ctrl_reg_flags      : std_logic_vector(InputWidthPerBlock - 1 downto 0);

	signal reg_ctrl_num : integer range 0 to 15;

	signal clk40_int, clkFast_int : std_logic;
	signal inp_int                : TOBArray(InputWidthPerBlock * (InputWidthBlocks) - 1 downto 0);

	signal cnt                       : integer range 0 to InputWidthBlocks := 3; -- 2 for 4 input lists
	constant cntStart                : integer range 0 to InputWidthBlocks := 2;
	signal toggle40                  : std_logic                           := '0';
	signal toggle160, toggle160synch : std_logic                           := '0';
	signal resetClk                  : std_logic                           := '0';

	signal tobSelectIn        : TOBArray(InputWidthPerBlock - 1 downto 0);
	signal tobSelectOut       : TOBArray(InputWidthPerBlock - 1 downto 0);
	signal tobSelectOut_reg   : TOBArray(InputWidthPerBlock - 1 downto 0);
	signal selectBitsIn       : std_logic_vector(InputWidthPerBlock - 1 downto 0);
	signal selectBitsOut      : std_logic_vector(InputWidthPerBlock - 1 downto 0);
	signal selectBitsOut_reg  : std_logic_vector(InputWidthPerBlock - 1 downto 0);
	signal select_pattern_int : std_logic_vector(InputWidthPerBlock * (InputWidthBlocks) - 1 downto 0);

	signal overflowSumFinal                                      : unsigned(6 - 1 downto 0) := (others => '0');
	signal currentTobCount                                       : std_logic_vector(4 - 1 downto 0);
	signal overflowTmp, overflowReg                              : std_logic;
	signal toggle40_v, toggle160_v, toggle160synch_v, resetClk_v : std_logic_vector(0 downto 0);

	type overflowSumArray is array (integer range <>) of unsigned(6 - 1 downto 0);
	signal overflowSums : overflowSumarray(4 - 1 downto 0) := (others => (others => '0'));

begin
	oup                 <= oup_int;
	toggle40_v(0)       <= toggle40;
	toggle160_v(0)      <= toggle160;
	toggle160synch_v(0) <= toggle160synch;
	resetClk_v(0)       <= resetClk;

	clk40_int   <= clk40;
	clkFast_int <= clkFast;

	inp_int            <= inp;
	select_pattern_int <= select_pattern_in;

	reg_ctrl_num <= reg_ctrl(ctrl_reg_flags);

	gen_tob_select : entity work.TobSelect_presorted
		generic map(
			InputWidth  => InputWidthPerBlock,
			OutputWidth => InputWidthPerBlock
		)
		port map(
			Tob         => tobSelectIn,
			cmp_res     => selectBitsIn,
			selTOBs     => tobSelectOut,
			end_cmp_res => selectBitsOut
		);

	currentTobCount <= std_logic_vector(to_unsigned(count_ones(selectBitsIn), currentTobCount'length));

	gen_reg_tob : entity work.TobRegister
		generic map(
			InputWidth => InputWidthPerBlock
		)
		port map(
			clk     => clkFast_int,
			enable  => '1',
			reg_in  => tobSelectOut,
			reg_out => tobSelectOut_reg
		);

	gen_reg_select_pattern : entity work.GenericRegister
		generic map(
			reg_width => InputWidthPerBlock
		)
		port map(
			clk     => clkFast_int,
			enable  => '1',
			reg_in  => selectBitsOut,
			reg_out => selectBitsOut_reg
		);

	fill_final_array : process(clkFast_int, inp_int, select_pattern_int, cnt)
		variable overflowSum : unsigned(6 - 1 downto 0) := (others => '0');
	begin
		if rising_edge(clkFast_int) then
			if (resetClk = '1') then
				cnt <= cntStart;
			else
				if (cnt = InputWidthBlocks) then
					cnt <= 0;
				else
					cnt <= cnt + 1;
				end if;
			end if;

			if (cnt = 0) then
				overflowSum := unsigned('0' & ('0' & currentTobCount));
			else
				overflowSum := overflowSum + unsigned('0' & ('0' & currentTobCount));
			end if;

			overflowSumFinal <= overflowSum;

			if (cnt = 1) then
				packedTOBs     <= tobSelectOut_reg(InputWidthPerBlock - 1 downto 0);
				ctrl_reg_flags <= selectBitsOut_reg(InputWidthPerBlock - 1 downto 0);
			else
				if reg_ctrl_num /= InputWidthPerBlock then
					for reg_ctrl_num_2 in 0 to InputWidthPerBlock - 1 loop
						if (reg_ctrl_num = reg_ctrl_num_2) then
							ctrl_reg_flags(InputWidthPerBlock - 1 downto reg_ctrl_num_2) <= selectBitsOut_reg(InputWidthPerBlock - reg_ctrl_num_2 - 1 downto 0);
							packedTOBs(InputWidthPerBlock - 1 downto reg_ctrl_num_2)     <= tobSelectOut_reg(InputWidthPerBlock - reg_ctrl_num_2 - 1 downto 0);
							if (cnt = 3) then
								oup_int            <= tobSelectOut_reg(InputWidthPerBlock - reg_ctrl_num_2 - 1 downto 0) & packedTOBs(reg_ctrl_num_2 - 1 downto 0);
								select_pattern_out <= selectBitsOut_reg(InputWidthPerBlock - reg_ctrl_num_2 - 1 downto 0) & ctrl_reg_flags(reg_ctrl_num_2 - 1 downto 0);
								overflow           <= overflowReg;
							end if;
						end if;
					end loop;
				else
					ctrl_reg_flags <= ctrl_reg_flags;
					packedTOBs     <= packedTOBs;

					if (cnt = 3) then
						oup_int            <= packedTOBs;
						select_pattern_out <= ctrl_reg_flags;
						overflow           <= overflowReg;
					end if;
				end if;
			end if;
		end if;

	end process;

	tobSelectIn <= (others => empty_tob) when cnt = 3 else inp_int((0 + 1) * InputWidthPerBlock - 1 downto (0 + 0) * InputWidthPerBlock) when (cnt = 0) else inp_int((1 + 1) * InputWidthPerBlock - 1 downto (1 + 0) * InputWidthPerBlock) when (cnt = 1) else inp_int((2 + 1) *
			InputWidthPerBlock - 1 downto (2 + 0) * InputWidthPerBlock) when (cnt = 2) else (others => empty_tob);

	selectBitsIn <= (others => '0') when cnt = 3 else select_pattern_int((0 + 1) * InputWidthPerBlock - 1 downto (0 + 0) * InputWidthPerBlock) when (cnt = 0) else select_pattern_int((1 + 1) * InputWidthPerBlock - 1 downto (1 + 0) * InputWidthPerBlock) when (cnt = 1) else
		select_pattern_int((2 + 1) * InputWidthPerBlock - 1 downto (2 + 0) * InputWidthPerBlock) when (cnt = 2) else (others => '0');

	process(clkFast_int)
	begin
		if rising_edge(clkFast_int) then
			toggle160      <= toggle40;
			toggle160synch <= toggle160;
		end if;
	end process;

	process(clk40_int)
	begin
		if rising_edge(clk40_int) then
			toggle40 <= not toggle40;
		end if;
	end process;

	resetClk    <= '1' when (toggle160synch /= toggle160) else '0';
	overflowTmp <= '1' when (overflowSumFinal > InputWidthPerBlock) else '0';
	overflowReg <= overflowTmp;

end Behavioral;
