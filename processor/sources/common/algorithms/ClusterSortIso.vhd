------------------------------------------------------------------------------------------
-- Author/Modified by	: manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de
-- Description			: applies isolation and eta cut to ClusterTOB list, sorts TOBs,
--						  converts them to GenericTOBs and returns short sorted list
-- Number of registers	: 2
------------------------------------------------------------------------------------------

--------------
-- includes --
--------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use work.L1TopoDataTypes.all;
use work.L1TopoFunctions.all;

------------------------
-- entity declaration --
------------------------

entity ClusterSortIso is
	generic(InputWidth         : integer := InputWidthEM; --number of input TOBs
		    InputWidth1stStage : integer := InputWidth1stStageSortEM; --number of TOBs in 1st stage
		    OutputWidth        : integer := OutputWidthSortEM --number of sorted output TOBs
	);
	Port(ClockBus        : in  std_logic_vector(2 downto 0);
		 Parameters      : in  ParameterArray;
		 ClusterTobArray : in  ClusterArray(InputWidth - 1 downto 0);
		 TobArrayOut     : out TOBArray(OutputWidth - 1 downto 0)
	);
end ClusterSortIso;

-----------------------------------------
-- behavioral description of algorithm --
-----------------------------------------

architecture Behavioral of ClusterSortIso is

	---------------
	-- constants --
	---------------
	constant ClusterPresortedLength       : integer := 30;
	constant OutputWidth1stStagePresorted : integer := 10;

	-----------
	-- types --
	-----------
	type vector_output_width is array (integer range <>) of std_logic_vector(OutputWidth1stStagePresorted - 1 downto 0);

	-------------
	-- signals --
	-------------
	-- parameters:
	signal IsoMask                      : std_logic_vector(ClusterIsolBitWidth - 1 downto 0);
	-- selected input TOBs (Eta & isolation):
	signal generic_in                   : TOBArray(InputWidth - 1 downto 0);
	signal select_pattern               : std_logic_vector(InputWidth - 1 downto 0);
	signal generic_in_cut               : ClusterArray(InputWidth - 1 downto 0);
	-- sorted TOBs after first stage (Et):
	signal out_1stStage                 : TOBArray((InputWidth / ClusterPresortedLength) * OutputWidth - 1 downto 0);
	signal out_1stStage_selected        : TOBArray((InputWidth / ClusterPresortedLength) * OutputWidth - 1 downto 0);
	signal reg_out_1stStage             : TOBArray((InputWidth / ClusterPresortedLength) * OutputWidth - 1 downto 0);
	-- sorted TOBs after second stage
	signal reg_out_2ndStage             : TOBArray(OutputWidth - 1 downto 0);
	signal reg_out_3                    : TOBArray(OutputWidth - 1 downto 0);
	signal overflow_reg1, overflow_reg2 : std_logic;
	signal select_pattern_out           : vector_output_width((InputWidth / ClusterPresortedLength) - 1 downto 0);

	signal ParametersRemap : ParameterArray;

begin

	-- read parameters
	ParametersRemap(0) <= (others => '0');
	ParametersRemap(1) <= Parameters(0);

	----------------------------------------------
	-- select TOBs and translate to generic TOB --
	----------------------------------------------

	generic_in_cut <= ClusterTobArray;

	---------------------------------------------------------------
	-- first stage: restore pre-sorting of input lists after cut --
	---------------------------------------------------------------
	gen1stStage : for i in 0 to InputWidth / ClusterPresortedLength - 1 generate
		selection_1stStage : entity work.SelectMergePresortedCluster
			generic map(
				InputWidthBlocks   => ClusterPresortedLength / OutputWidth1stStagePresorted,
				InputWidthPerBlock => OutputWidth1stStagePresorted,
				OutputWidth        => OutputWidth,
				DoIsoCut           => 1,
				UseOutClk40        => 1
			)
			port map(
				clk40              => ClockBus(0),
				clkFast            => ClockBus(2),
				Parameters         => ParametersRemap,
				inp                => generic_in_cut((i + 1) * ClusterPresortedLength - 1 downto i * ClusterPresortedLength),
				select_pattern_in  => select_pattern((i + 1) * ClusterPresortedLength - 1 downto i * ClusterPresortedLength),
				oup                => out_1stStage((i + 1) * (OutputWidth) - 1 downto i * (OutputWidth)),
				select_pattern_out => open,
				overflow           => open
			);
	end generate;

	gen_select1stStage_1 : for j in (InputWidth / ClusterPresortedLength) - 1 downto 0 generate
		gen_select1stStage_2 : for i in OutputWidth - 1 downto 0 generate
			out_1stStage_selected(((InputWidth / ClusterPresortedLength - 1) - j + 1) * OutputWidth - i - 1) <= out_1stStage(j * OutputWidth + i);
		end generate;
	end generate;

	reg_out_1stStage <= out_1stStage_selected;

	------------------------------------------------
	-- second stage: sort the first-stage outputs --
	------------------------------------------------
	sort2ndStage : entity work.TobSort
		generic map(OutputWidth => OutputWidth,
			        InputWidth  => (InputWidth / ClusterPresortedLength) * OutputWidth,
			        DoPresort   => 0
		)
		port map(Tob         => reg_out_1stStage,
			     TobArrayOut => reg_out_2ndStage
		);

	---------------------------------------------
	-- register the output of the second stage --
	---------------------------------------------
	reg_2nd_stage : entity work.TobRegister
		generic map(InputWidth => OutputWidth
		)
		Port map(clk     => ClockBus(0),
			     enable  => '1',
			     reg_in  => reg_out_2ndStage,
			     reg_out => reg_out_3
		);

	--set Overflow
	gen_overflow_reg1 : entity work.StdLogicRegister
		port map(clk     => ClockBus(0),
			     enable  => '1',
			     reg_in  => ClusterTobArray(0).Overflow,
			     reg_out => overflow_reg1
		);

	gen_overflow_reg2 : entity work.StdLogicRegister
		port map(clk     => ClockBus(0),
			     enable  => '1',
			     reg_in  => overflow_reg1,
			     reg_out => overflow_reg2
		);

	-- workaround to set overflow bit and reordering of TOBs
	gen_set_overflow : for i in 0 to OutputWidth - 1 generate
		TobArrayOut(i).Et  <= reg_out_3(OutputWidth - 1 - i).Et;
		TobArrayOut(i).Eta <= reg_out_3(OutputWidth - 1 - i).Eta;
		TobArrayOut(i).Phi <= reg_out_3(OutputWidth - 1 - i).Phi;
	end generate;

	TobArrayOut(0).Overflow <= overflow_reg2;

	-- only needed for simulation
	gen_set_dummy_overflow : for i in 1 to OutputWidth - 1 generate
		TobArrayOut(i).Overflow <= '0';
	end generate;

end Behavioral;
