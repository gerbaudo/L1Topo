------------------------------------------------------------------------------------------
-- Author/Modified by	: manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de
-- Description			: applies Et cut on two TOB lists; calculates delta eta of all 
--						  TOB combinations and applies MinDeltaEta cut
-- Number of registers	: 0
-- behavioral simulation : ok
------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.L1TopoDataTypes.all;
use work.L1TopoFunctions.all;

entity DeltaEtaPhiIncl2 is
	generic(
		InputWidth1   : integer   := 9; -- length of first input list
		InputWidth2   : integer   := 9;-- length of second input list
		MaxTob1		  : integer := 0;	-- number of TOBs which should be considered
		MaxTob2		  : integer := 0;	-- number of TOBs which should be considered
		NumResultBits : integer   := 1 -- Number of result bits
	);
	port(
		Tob1       : in  TOBArray(InputWidth1 - 1 downto 0); -- first TOB array
		Tob2       : in  TOBArray(InputWidth2 - 1 downto 0); -- second TOB array
		Parameters : in  ParameterArray;
		ClockBus   : in  std_logic_vector(2 downto 0);
		Results    : out std_logic_vector(NumResultBits - 1 downto 0);
		Overflow   : out std_logic_vector(NumResultBits - 1 downto 0)
	);
end DeltaEtaPhiIncl2;

architecture Behavioral of DeltaEtaPhiIncl2 is
	
	-- constants

	constant maxCount1 : integer := getMaxCount(InputWidth1, MaxTob1);
	constant maxCount2 : integer := getMaxCount(InputWidth2, MaxTob2);

	-- Type declarations

	type deta is array (InputWidth2 - 1 downto 0) of std_logic_vector(GenericEtaBitWidth - 1 downto 0);
	type dphi is array (InputWidth2 - 1 downto 0) of std_logic_vector(GenericPhiBitWidth - 1 downto 0);
	type detaarray is array (InputWidth1 - 1 downto 0) of deta;
	type etThreshArray is array (NumResultBits - 1 downto 0) of std_logic_vector(GenericEtBitWidth - 1 downto 0);
	type etaThreshArray is array (NumResultBits - 1 downto 0) of std_logic_vector(GenericEtaBitWidth - 1 downto 0);
	type phiThreshArray is array (NumResultBits - 1 downto 0) of std_logic_vector(GenericPhiBitWidth - 1 downto 0);
	type dphiarray is array (InputWidth1 - 1 downto 0) of dphi;
	type thrAcceptArray1 is array (NumResultBits - 1 downto 0) of std_logic_vector(InputWidth1 - 1 downto 0);
	type thrAcceptArray2 is array (NumResultBits - 1 downto 0) of std_logic_vector(InputWidth2 - 1 downto 0);

	-- Internal signal declarations

	signal deltaeta                           : detaarray;
	signal deltaPhi                 : dphiarray;
	signal MinDeltaEta, MaxDeltaEta           : etaThreshArray;
	signal MinDeltaPhi, MaxDeltaPhi : phiThreshArray;
	signal MinEt1, MinEt2                     : etThreshArray;
	signal thraccept_1                        : thrAcceptArray1;
	signal thraccept_2                        : thrAcceptArray2;

	signal clock        : std_logic;
	signal results_out  : std_logic_vector(NumResultBits - 1 downto 0);
	signal overflow_out : std_logic_vector(NumResultBits - 1 downto 0);
	signal tob1_in      : TOBArray(InputWidth1 - 1 downto 0);
	signal tob2_in      : TOBArray(InputWidth2 - 1 downto 0);

begin                                   -- Behavioral

	clock   <= ClockBus(0);
	tob1_in <= Tob1;
	tob2_in <= Tob2;

	set_ranges : for i in 0 to (NumResultBits - 1) generate -- extract parameters
		MinEt1(i)  <= Parameters(0 + (6 * i))(GenericEtBitWidth - 1 downto 0);
		MinEt2(i)  <= Parameters(1 + (6 * i))(GenericEtBitWidth - 1 downto 0);
		MinDeltaEta(i) <= Parameters(2 + (6 * i))(GenericEtaBitWidth - 1 downto 0);
		MaxDeltaEta(i) <= Parameters(3 + (6 * i))(GenericEtaBitWidth - 1 downto 0);
		MinDeltaPhi(i) <= Parameters(4 + (6 * i))(GenericPhiBitWidth - 1 downto 0);
		MaxDeltaPhi(i) <= Parameters(5 + (6 * i))(GenericPhiBitWidth - 1 downto 0);
	end generate;

	min_tob_Et : process(tob1_in, tob2_in, MinEt1, MinEt2) -- Flag TOB with Et over threshold -- TODO: BUGGY?
	begin
		for k in 0 to (NumResultBits - 1) loop
			for i in 0 to (maxCount1 - 1) loop
				if (tob1_in(i).Et > MinEt1(k)) then
					thraccept_1(k)(i) <= '1';
				else
					thraccept_1(k)(i) <= '0';
				end if;
			end loop;
			for i in 0 to (maxCount2 - 1) loop
				if (tob2_in(i).Et > MinEt2(k)) then
					thraccept_2(k)(i) <= '1';
				else
					thraccept_2(k)(i) <= '0';
				end if;
			end loop;
		end loop;
	end process;

	deltaEtaPhi_calc1 : for i in 0 to (maxCount1 - 1) generate
		deltaEtaPhi_calc2 : for j in 0 to (maxCount2 - 1) generate
			detadphiCalc_inst : entity work.DeltaRApproxBoxCutCalc
				port map(eta1     => tob1_in(i).Eta,
					     eta2     => tob2_in(j).Eta,
					     phi1     => tob1_in(i).Phi,
					     phi2     => tob2_in(j).Phi,
					     deltaEta => deltaEta(i)(j),
					     deltaPhi => deltaPhi(i)(j)
				);
		end generate;
	end generate;

	compare_thresholds : process(deltaeta, MaxDeltaEta, MaxDeltaPhi, MinDeltaEta, MinDeltaPhi, deltaPhi, thraccept_1, thraccept_2) -- Compare delta eta with thresholds for all TOB pairs -- TODO: BUGGY?
		variable hits : std_logic_vector(NumResultBits - 1 downto 0);
	begin
		hits := (others => '0');
		for i in 0 to (maxCount1 - 1) loop -- loop over TOBs i and j
			for j in 0 to (maxCount2 - 1) loop
				for k in 0 to NumResultBits - 1 loop -- loop over thresholds
					if ( ((unsigned(deltaEta(i)(j)) >= unsigned(MinDeltaEta(k))) OR (unsigned(deltaPhi(i)(j)) >= unsigned(MinDeltaPhi(k))))
						AND (unsigned(deltaEta(i)(j)) <= unsigned(MaxDeltaEta(k))) AND  (unsigned(deltaPhi(i)(j)) <= unsigned(MaxDeltaPhi(k)))
						AND (thraccept_1(k)(i) = '1') AND (thraccept_2(k)(j) = '1')
					) then
						hits(k) := '1';
					end if;
				end loop;
			end loop;
		end loop;
		results_out  <= hits;
	end process;

	Results  <= results_out;
	
	-----------------------------
	-- generate overflow bits  --
	-----------------------------
	genOverflows : for result in 0 to NumResultBits - 1 generate
		-- get overflow from input list
		overflow_out(result) <= Tob1(0).Overflow or Tob2(0).Overflow;
	end generate;
	
	Overflow <= overflow_out;

end Behavioral;
