------------------------------------------------------------------------------------------
-- Author/Modified by	: manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de
-- Description			: calcuates the transverse mass for a given TOB using MET
-- Number of registers	: 0
------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use IEEE.NUMERIC_STD.ALL;
use work.L1TopoDataTypes.all;

entity TransverseMassCalc is
	Port(phi1     : in  std_logic_vector(GenericPhiBitWidth - 1 downto 0);
		 METPhi   : in  std_logic_vector(GenericPhiBitWidth - 1 downto 0); --MET
		 energy1  : in  std_logic_vector(GenericEtBitWidth - 1 downto 0);
		 MET      : in  std_logic_vector(GenericEtBitWidth - 1 downto 0); -- MET phi
		 mass_sqr : out std_logic_vector(TransverseMassSqrBitWidth - 1 downto 0)
	);
end TransverseMassCalc;

architecture Behavioral of TransverseMassCalc is
	attribute S : string;

	signal delta_phi : std_logic_vector(GenericPhiBitWidth - 1 downto 0);
	attribute S of delta_phi : signal is "true";

	signal phi1_sig   : std_logic_vector(GenericPhiBitWidth - 1 downto 0);
	signal phi2_sig   : std_logic_vector(GenericPhiBitWidth - 1 downto 0);
	signal cos_result : std_logic_vector(CosSinBitWidth - 1 downto 0);

	signal energymult  : unsigned(2 * GenericEtBitWidth - 1 downto 0);
	signal energymult2 : unsigned(2 * GenericEtBitWidth + 1 - 1 downto 0);

	signal cos_diff   : std_logic_vector(CosSinBitWidth + 1 - 1 downto 0);
	signal massSqrTmp : std_logic_vector(TransverseMassSqrBitWidth + 7 - 1 downto 0);

	constant one : std_logic_vector(CosSinBitWidth + 1 - 1 downto 0) := "0010000000";

begin
	phi1_sig <= phi1;
	phi2_sig <= METPhi;

	DPhi_calc : entity work.DeltaPhiCalc
		port map(
			phi1In      => phi1_sig,
			phi2In      => phi2_sig,
			deltaPhiOut => delta_phi
		);

	Cos_module : entity work.Cos
		port map(inp => delta_phi,
			     oup => cos_result
		);

	energymult <= unsigned(energy1) * unsigned(MET);

	energymult2 <= energymult & '0';

	cos_diff <= std_logic_vector(signed(one) - signed(cos_result(cos_result'length - 1) & cos_result));

	massSqrTmp <= std_logic_vector(energymult2 * unsigned(cos_diff)); -- TODO: possible to reduce width of "mass" by removing precision of result (by keeping only the highest bits)

	mass_sqr <= massSqrTmp(TransverseMassSqrBitWidth + 7 - 1 downto 7);

end Behavioral;