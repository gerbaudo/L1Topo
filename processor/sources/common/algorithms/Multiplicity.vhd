------------------------------------------------------------------------------------------
-- Author/Modified by	: manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de
-- Description			: applies Et cut on Tob list
-- Number of registers	: 0
------------------------------------------------------------------------------------------

--------------
-- includes --
--------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.L1TopoDataTypes.ALL;
use IEEE.numeric_std.all;
use work.L1TopoFunctions.ALL;

------------------------
-- entity declaration --
------------------------

entity Multiplicity is
	generic(InputWidth    : integer := 1; --number of input TOBs
		    NumResultBits : integer := 2 --number of output bits
	);
	Port(ClockBus   : std_logic_vector(2 downto 0);
		 Parameters : in  ParameterArray;
		 Tob        : in  TOBArray(InputWidth - 1 downto 0);
		 Results    : out std_logic_vector(NumResultBits - 1 downto 0); --2 bits?
		 Overflow   : out std_logic_vector(NumResultBits - 1 downto 0)
	);
end Multiplicity;

-----------------------------------------
-- behavioral description of algorithm --
-----------------------------------------

architecture Behavioral of Multiplicity is

	----------------------
	-- type definitions --
	----------------------
	type threshold_array is array (integer range <>) of std_logic_vector(GenericEtBitWidth - 1 downto 0);

	-------------
	-- signals --
	-------------
	-- parameters:
	signal MinEt               : threshold_array(NumResultBits - 1 downto 0);
	-- temporary results:
	signal tobs_over_threshold : std_logic_vector(InputWidth - 1 downto 0);
	signal results_out         : std_logic_vector(NumResultBits - 1 downto 0);
	signal overflow_out        : std_logic_vector(NumResultBits - 1 downto 0);

begin
	MinEt(0) <= Parameters(0)(GenericEtBitWidth - 1 downto 0);

	genAcceptTemp : for i in 0 to InputWidth - 1 generate
		tobs_over_threshold(i) <= '1' when (Tob(i).Et > MinEt(0)) else '0';
	end generate;

	results_out <= std_logic_vector(to_unsigned(count_ones_twoBits(tobs_over_threshold), 2));

	Results <= results_out;

	-----------------------------
	-- generate overflow bits  --
	-----------------------------
	genOverflows : for result in 0 to NumResultBits - 1 generate
		-- get overflow from input list
		overflow_out(result) <= Tob(0).Overflow;
	end generate;

	Overflow <= overflow_out;

end Behavioral;