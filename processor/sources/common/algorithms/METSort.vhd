------------------------------------------------------------------------------------------
-- Author/Modified by	: manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de
-- Description			: calculates MET Tob
-- Number of registers	: 2
------------------------------------------------------------------------------------------

--------------
-- includes --
--------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use work.L1TopoDataTypes.all;
use work.L1TopoFunctions.all;

------------------------
-- entity declaration --
------------------------

entity METSort is
	generic(InputWidth  : integer := InputWidthMET; --number of input missing energies (1)
		    OutputWidth : integer := OutputWidthMET --number of output missing energies (MET, MET_corrected)
	);
	Port(ClockBus    : in  std_logic_vector(2 downto 0);
		 Parameters  : in  ParameterArray;
		 MetTobArray : in  MetArray(InputWidth - 1 downto 0);
		 TobArrayOut : out TOBArray(OutputWidth - 1 downto 0)
	);
end METSort;

-----------------------------------------
-- behavioral description of algorithm --
-----------------------------------------

architecture Behavioral of METSort is

	----------------------
	-- type definitions --
	----------------------

	type METPhiArray is array (natural range <>) of std_logic_vector(GenericPhiBitWidth - 1 downto 0);
	type METEnergyArray is array (natural range <>) of std_logic_vector(Arctan2InputEnergyBitWidth - 1 downto 0);

	-------------
	-- signals --
	-------------

	signal out_METPhi                                                              : METPhiArray(InputWidth - 1 downto 0);
	signal MetTobArray_reg40, MetTobArray_reg1, MetTobArray_reg2, MetTobArray_reg3 : MetArray(InputWidth - 1 downto 0);
	signal generic_in                                                              : TOBArray(InputWidth - 1 downto 0);
	signal generic_out                                                             : TOBArray(OutputWidth - 1 downto 0);
	signal overflow_reg1, overflow_reg2                                            : std_logic;

begin

	-- calculate phi and register output for all input METs
	genInputWidth : for i in 0 to InputWidth - 1 generate

		-------------------------
		-- MET Phi calculation --
		-------------------------

		-- make MET energies fit Artan2 function arguments (take highest bits)

		-- calculate the phi angle starting from the x-axis which is horizontal and points to the center of the LHC
		Arctan_inst : entity work.Arctan2 Port map(
				Ex  => MetTobArray_reg40(i).Ex(METEnergyBitWidth - 1 downto METEnergyBitWidth - Arctan2InputEnergyBitWidth),
				Ey  => MetTobArray_reg40(i).Ey(METEnergyBitWidth - 1 downto METEnergyBitWidth - Arctan2InputEnergyBitWidth),
				Phi => out_METPhi(i)
			);

		REG160_PROC : process(Clockbus(2))
		begin
			if rising_edge(ClockBus(2)) then
				MetTobArray_reg1(i) <= MetTobArray(i);
				MetTobArray_reg2(i) <= MetTobArray_reg1(i);
				MetTobArray_reg3(i) <= MetTobArray_reg2(i);
			end if;
		end process;

		reg_Ex : entity work.GenericRegister
			generic map(reg_width => METEnergyBitWidth
			)
			port map(clk     => ClockBus(0),
				     enable  => '1',
				     reg_in  => MetTobArray_reg3(i).Ex,
				     reg_out => MetTobArray_reg40(i).Ex
			);

		reg_Ey : entity work.GenericRegister
			generic map(reg_width => METEnergyBitWidth
			)
			port map(clk     => ClockBus(0),
				     enable  => '1',
				     reg_in  => MetTobArray_reg3(i).Ey,
				     reg_out => MetTobArray_reg40(i).Ey
			);

	end generate;

	------------------------------
	-- translate to generic TOB --
	------------------------------

	gen_generic_tob : for i in 0 to InputWidth - 1 generate
		-- translate to generic TOB
		generic_in(i) <= to_GenericTOB(MetTobArray_reg40(i), out_METPhi(i));
	end generate;

	-------------------------
	-- register the output --
	-------------------------

	reg_out : entity work.TobRegister
		generic map(InputWidth => OutputWidth
		)
		Port map(clk     => ClockBus(0),
			     enable  => '1',
			     reg_in  => generic_in,
			     reg_out => generic_out
		);

	gen_overflow_reg1 : entity work.StdLogicRegister
		port map(clk     => ClockBus(0),
			     enable  => '1',
			     reg_in  => MetTobArray(0).Overflow,
			     reg_out => overflow_reg1
		);

	gen_overflow_reg2 : entity work.StdLogicRegister
		port map(clk     => ClockBus(0),
			     enable  => '1',
			     reg_in  => overflow_reg1,
			     reg_out => overflow_reg2
		);

	--set Overflow to 0 (Sorting has no Overflow)

	TobArrayOut(0).Et       <= generic_out(0).Et;
	TobArrayOut(0).Eta      <= generic_out(0).Eta;
	TobArrayOut(0).Phi      <= generic_out(0).Phi;
	TobArrayOut(0).Overflow <= overflow_reg2;

end Behavioral;