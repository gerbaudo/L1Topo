-----------------------------------------------------------------------------------------
-- Author/Modified by	: manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de
-- Description			: calculates the sum of a jet list and applies a cut on the sum 
-- Number of registers	: 2
------------------------------------------------------------------------------------------

--------------
-- includes --
--------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.L1TopoDataTypes.all;
use work.L1TopoFunctions.all;

------------------------
-- entity declaration --
------------------------

entity JetHT is
	generic(InputWidth    : integer := InputWidthJET; --number of input TOBs
		    MaxTob        : integer := 0; -- number of TOBs which should be considered
		    NumResultBits : integer := 1; --number of output bits
		    NumRegisters  : integer := 2 -- deprecated; does not have any effect
	);
	Port(ClockBus   : in  std_logic_vector(2 downto 0);
		 Parameters : in  ParameterArray;
		 Tob        : in  TOBArray(InputWidth - 1 downto 0);
		 Results    : out std_logic_vector(NumResultBits - 1 downto 0);
		 Overflow   : out std_logic_vector(NumResultBits - 1 downto 0)
	);
end JetHT;

-----------------------------------------
-- behavioral description of algorithm --
-----------------------------------------

architecture Behavioral of JetHT is

	-- constants

	constant maxCount               : integer := getMaxCount(InputWidth, MaxTob);
	constant NumRegistersWorkaround : integer := 0;

	----------------------
	-- type definitions --
	----------------------

	type thresholdArray is array (integer range <>) of std_logic_vector(JetHtBitWidth - 1 downto 0);

	-------------
	-- signals --
	-------------
	-- parameters:

	signal MinHt                                      : thresholdArray(NumResultBits - 1 downto 0);
	signal sum                                        : std_logic_vector(JetHtBitWidth - 1 downto 0);
	signal accept, accept_reg1, accept_reg2           : std_logic_vector(NumResultBits - 1 downto 0);
	signal overMaxEt                                  : std_logic;
	signal overflow_1bit                              : std_logic;
	signal overflow_tmp, overflow_reg1, overflow_reg2 : std_logic_vector(NumResultBits - 1 downto 0);

begin
	jetHt_cal : entity work.JetHtInternal
		generic map(InputWidth => InputWidth, --number of input TOBs
			        MaxTob     => MaxTob, -- number of TOBs which should be considered
			        HtBitWidth => JetHtBitWidth
		)
		Port Map(ClockBus   => ClockBus,
			     Parameters => Parameters,
			     Tob        => Tob,
			     Saturation => overMaxEt,
			     SumValue   => sum,
			     Overflow   => overflow_1bit
		);

	--------------------------------------------------
	-- generate result bits (jet HT over threshold) --
	--------------------------------------------------

	gen_param : for i in 0 to NumResultBits - 1 generate
		MinHt(i) <= Parameters(i + 3)(JetHtBitWidth - 1 downto 0);
	end generate;

	genAccept : for result in 0 to NumResultBits - 1 generate
		accept(result) <= '1' when unsigned(sum) > unsigned(MinHt(result)) OR overMaxEt = '1' else '0';
	end generate;

	-----------------------------
	-- generate overflow bits  --
	-----------------------------
	genOverflows : for result in 0 to NumResultBits - 1 generate
		-- get overflow from input list
		overflow_tmp(result) <= overflow_1bit;
	end generate;

	-------------------------------
	-- buffer and output results --
	-------------------------------

	-- output without registers
	gen_output_0reg : if (NumRegistersWorkaround = 0) generate
		Results  <= accept;
		Overflow <= overflow_tmp;
	end generate;

	-- output with 2 registers
	gen_output_2reg : if (NumRegistersWorkaround = 2) generate
		Oup_reg1 : entity work.GenericRegister
			generic map(reg_width => NumResultBits
			)
			port map(clk     => ClockBus(0),
				     enable  => '1',
				     reg_in  => accept,
				     reg_out => accept_reg1
			);

		Over_reg1 : entity work.GenericRegister
			generic map(reg_width => NumResultBits
			)
			port map(clk     => ClockBus(0),
				     enable  => '1',
				     reg_in  => overflow_tmp,
				     reg_out => overflow_reg1
			);

		Oup_reg2 : entity work.GenericRegister
			generic map(reg_width => NumResultBits
			)
			port map(clk     => ClockBus(0),
				     enable  => '1',
				     reg_in  => accept_reg1,
				     reg_out => accept_reg2
			);

		Over_reg2 : entity work.GenericRegister
			generic map(reg_width => NumResultBits
			)
			port map(clk     => ClockBus(0),
				     enable  => '1',
				     reg_in  => overflow_reg1,
				     reg_out => overflow_reg2
			);

		Results  <= accept_reg2;
		Overflow <= overflow_reg2;
	end generate;

end Behavioral;
