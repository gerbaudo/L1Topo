------------------------------------------------------------------------------------------
-- Author/Modified by	: manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de
-- Description			: applies Et cut on TOB list; calculates delta eta of all 
--						  TOB combinations in this list and applies MinDeltaEta cut
-- Number of registers	: 0
-- behavioral simulation : ok
------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.L1TopoDataTypes.all;
use work.L1TopoFunctions.all;

entity DeltaEtaIncl1 is
	generic(
		InputWidth    : integer := 6;
		MaxTob        : integer := 0;   -- number of TOBs which should be considered
		NumResultBits : integer := 1    -- Number of result bits
	);
	port(
		Tob        : in  TOBArray(InputWidth - 1 downto 0);
		Parameters : in  ParameterArray;
		ClockBus   : in  std_logic_vector(2 downto 0);
		Results    : out std_logic_vector(NumResultBits - 1 downto 0);
		Overflow   : out std_logic_vector(NumResultBits - 1 downto 0)
	);
end DeltaEtaIncl1;

architecture Behavioral of DeltaEtaIncl1 is

	-- constants

	constant maxCount : integer := getMaxCount(InputWidth, MaxTob);

	-- Type declarations

	type deta is array (InputWidth - 1 downto 1) of std_logic_vector(GenericEtaBitWidth - 1 downto 0);
	type detaarray is array (InputWidth - 2 downto 0) of deta;
	type etThreshArray is array (NumResultBits - 1 downto 0) of std_logic_vector(GenericEtBitWidth - 1 downto 0);
	type etaThreshArray is array (NumResultBits - 1 downto 0) of std_logic_vector(GenericEtaBitWidth - 1 downto 0);
	type thrAcceptArray is array (NumResultBits - 1 downto 0) of std_logic_vector(InputWidth - 1 downto 0);

	-- Internal signal declarations

	signal deltaeta                 : detaarray;
	signal MinDeltaEta, MaxDeltaEta : etaThreshArray;
	signal MinEt1, MinEt2           : etThreshArray;
	signal thraccept1               : thrAcceptArray;
	signal thraccept2               : thrAcceptArray;

	signal clock        : std_logic;
	signal tob_in       : TOBArray(InputWidth - 1 downto 0);
	signal results_out  : std_logic_vector(NumResultBits - 1 downto 0);
	signal overflow_out : std_logic_vector(NumResultBits - 1 downto 0);

begin

	-- Behavioral

	clock  <= ClockBus(0);
	tob_in <= Tob;

	set_ranges : for i in 0 to (NumResultBits - 1) generate -- extract parameters
		MinEt1(i)      <= Parameters(0 + (4 * i))(GenericEtBitWidth - 1 downto 0);
		MinEt2(i)      <= Parameters(1 + (4 * i))(GenericEtBitWidth - 1 downto 0);
		MinDeltaEta(i) <= Parameters(2 + (4 * i))(GenericEtaBitWidth - 1 downto 0);
		MaxDeltaEta(i) <= Parameters(3 + (4 * i))(GenericEtaBitWidth - 1 downto 0);
	end generate;

	min_tob_Et : process(tob_in, MinEt1, MinEt2) -- Flag TOB with Et over threshold
	begin
		for k in 0 to (NumResultBits - 1) loop
			for i in 0 to (maxCount - 1) loop
				if (tob_in(i).Et > MinEt1(k)) then
					thraccept1(k)(i) <= '1';
				else
					thraccept1(k)(i) <= '0';
				end if;
			end loop;
			for i in 0 to (maxCount - 1) loop
				if (tob_in(i).Et > MinEt2(k)) then
					thraccept2(k)(i) <= '1';
				else
					thraccept2(k)(i) <= '0';
				end if;
			end loop;
		end loop;
	end process;

	deltaEta_calc1 : for i in 0 to (maxCount - 2) generate
		deltaEta_calc2 : for j in (i + 1) to (maxCount - 1) generate
			dEtaCalc_inst : entity work.DeltaEtaCalc
				port map(
					eta1In      => tob_in(i).eta,
					eta2In      => tob_in(j).eta,
					deltaEtaOut => deltaEta(i)(j)
				);
		end generate;
	end generate;

	compare_thresholds : process(deltaeta, MaxDeltaEta, MinDeltaEta, thraccept1, thraccept2) -- Compare delta eta with thresholds for all TOB pairs
		variable hits : std_logic_vector(NumResultBits - 1 downto 0);
	begin
		hits := (others => '0');
		for i in 0 to (maxCount - 2) loop -- loop over TOBs i and j
			for j in (i + 1) to (maxCount - 1) loop
				for k in 0 to NumResultBits - 1 loop -- loop over thresholds
					if ((unsigned(deltaeta(i)(j)) >= unsigned(MinDeltaEta(k))) and (unsigned(deltaeta(i)(j)) <= unsigned(MaxDeltaEta(k))) and (((thraccept1(k)(i) = '1') and (thraccept2(k)(j) = '1')) or ((thraccept1(k)(j) = '1') and (thraccept2(k)(i) = '1')))
					) then
						hits(k) := '1';
					end if;
				end loop;
			end loop;
		end loop;
		results_out <= hits;
	end process;

	Results <= results_out;

	-----------------------------
	-- generate overflow bits  --
	-----------------------------
	genOverflows : for result in 0 to NumResultBits - 1 generate
		-- get overflow from input list
		overflow_out(result) <= Tob(0).Overflow;
	end generate;

	Overflow <= overflow_out;

end Behavioral;
