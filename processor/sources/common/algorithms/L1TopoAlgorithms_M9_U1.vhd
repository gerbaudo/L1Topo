library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use work.L1TopoDataTypes.all;

entity L1TopoAlgorithms_M9_U1 is
	Port(ClockBus     : in  std_logic_vector(2 downto 0);

		 EmTobArray   : in  ClusterArray(InputWidthEM - 1 downto 0);
		 TauTobArray  : in  ClusterArray(InputWidthTAU - 1 downto 0);
		 JetTobArray  : in  JetArray(InputWidthJET - 1 downto 0);
		 MuonTobArray : in  MuonArray(InputWidthMU - 1 downto 0);
		 MetTobArray  : in  MetArray(0 downto 0);
		 
		 Parameters   : in  ParameterSpace(NumberOfAlgorithms - 1 downto 0);
		 SortParameters     : in  ParameterSpace(NumberOfSortAlgorithms - 1 downto 0);
		 
		 Results      : out std_logic_vector(NumberOfResultBits - 1 downto 0);
		 Overflow     : out std_logic_vector(NumberOfResultBits - 1 downto 0)
	);
end L1TopoAlgorithms_M9_U1;

architecture Behavioral of L1TopoAlgorithms_M9_U1 is

-- Menu Name   : Topo_pp_vX
-- Menu Version: 1

-- Module/FPGA : 0/0
-- General configuration (<TopoConfig>):

  constant AlgoOffset0 : integer := 0;   --   INVM_AJ
  constant AlgoOffset1 : integer := 4;   --   INVM_J
  constant AlgoOffset2 : integer := 8;   --   HT
  constant AlgoOffset3 : integer := 10;   --   HT1-AJ0all.ETA49
  constant AlgoOffset4 : integer := 11;   --   INVM_EMs
  constant AlgoOffset5 : integer := 14;   --   0DETA10-Js1-Js2
 -- Ordered list of sorted TOBArrays:
  signal TAUabi  :    TOBArray ((OutputWidthSelectTAU - 1)  downto 0);
  signal EMs  :    TOBArray ((OutputWidthSortEM - 1)  downto 0);
  signal AJall  :    TOBArray ((InputWidthJET - 1)  downto 0);
  signal AJs  :    TOBArray ((OutputWidthSortJET - 1)  downto 0);
  signal Js  :    TOBArray ((OutputWidthSortJET - 1)  downto 0);
  signal MUab  :    TOBArray ((OutputWidthSelectMU - 1)  downto 0);

begin

sortalgo0 : entity work.ClusterSort --  Algorithm name: EMs
  generic map (
     InputWidth => InputWidthEM,
     InputWidth1stStage => InputWidth1stStageSortEM,
     OutputWidth => OutputWidthSortEM )
  port map (
     ClusterTobArray => EmTobArray,
     TobArrayOut =>  EMs,
     ClockBus => ClockBus,
     Parameters => SortParameters(0)
 );

sortalgo1 : entity work.JetNoSort --  Algorithm name: AJall
  generic map (
     InputWidth => InputWidthJET,
     OutputWidth => InputWidthJET,
     JetSize => DefaultJetSize )
  port map (
     JetTobArray => JetTobArray,
     TobArrayOut =>  AJall,
     ClockBus => ClockBus,
     Parameters => SortParameters(1)
 );

sortalgo2 : entity work.JetSort --  Algorithm name: AJs
  generic map (
     InputWidth => InputWidthJET,
     InputWidth1stStage => InputWidth1stStageSortJET,
     OutputWidth => OutputWidthSortJET,
     JetSize => 2 )
  port map (
     JetTobArray => JetTobArray,
     TobArrayOut =>  AJs,
     ClockBus => ClockBus,
     Parameters => SortParameters(2)
 );

sortalgo3 : entity work.JetSort --  Algorithm name: Js
  generic map (
     InputWidth => InputWidthJET,
     InputWidth1stStage => InputWidth1stStageSortJET,
     OutputWidth => OutputWidthSortJET,
     JetSize => 2 )
  port map (
     JetTobArray => JetTobArray,
     TobArrayOut =>  Js,
     ClockBus => ClockBus,
     Parameters => SortParameters(3)
 );

decisionalgo0 : entity work.InvariantMassInclusive1 --  Algorithm name: INVM_AJ
  generic map (
     InputWidth => OutputWidthSortJET,
     MaxTob => 6,
     NumResultBits => 4 )
  port map (
     Tob => AJs,
     Results => Results(AlgoOffset0+3 downto AlgoOffset0), 
     Overflow => Overflow(AlgoOffset0+3 downto AlgoOffset0), 
     ClockBus => ClockBus,
     Parameters => Parameters(0)
 );

decisionalgo1 : entity work.InvariantMassInclusive1 --  Algorithm name: INVM_J
  generic map (
     InputWidth => OutputWidthSortJET,
     MaxTob => 6,
     NumResultBits => 4 )
  port map (
     Tob => Js,
     Results => Results(AlgoOffset1+3 downto AlgoOffset1), 
     Overflow => Overflow(AlgoOffset1+3 downto AlgoOffset1), 
     ClockBus => ClockBus,
     Parameters => Parameters(1)
 );

decisionalgo2 : entity work.JetHT --  Algorithm name: HT
  generic map (
     InputWidth => OutputWidthSortJET,
     MaxTob => 5,
     NumRegisters => 0,
     NumResultBits => 2 )
  port map (
     Tob => AJs,
     Results => Results(AlgoOffset2+1 downto AlgoOffset2), 
     Overflow => Overflow(AlgoOffset2+1 downto AlgoOffset2), 
     ClockBus => ClockBus,
     Parameters => Parameters(2)
 );

decisionalgo3 : entity work.JetHT --  Algorithm name: HT1-AJ0all.ETA49
  generic map (
     InputWidth => InputWidthJET,
     MaxTob => InputWidthJET,
     NumRegisters => 2,
     NumResultBits => 1 )
  port map (
     Tob => AJall,
     Results => Results(AlgoOffset3+0 downto AlgoOffset3), 
     Overflow => Overflow(AlgoOffset3+0 downto AlgoOffset3), 
     ClockBus => ClockBus,
     Parameters => Parameters(3)
 );

decisionalgo4 : entity work.InvariantMassInclusive2 --  Algorithm name: INVM_EMs
  generic map (
     InputWidth1 => OutputWidthSortEM,
     InputWidth2 => OutputWidthSortEM,
     MaxTob1 => 2,
     MaxTob2 => OutputWidthSortEM,
     NumResultBits => 3 )
  port map (
     Tob1 => EMs,
     Tob2 => EMs,
     Results => Results(AlgoOffset4+2 downto AlgoOffset4), 
     Overflow => Overflow(AlgoOffset4+2 downto AlgoOffset4), 
     ClockBus => ClockBus,
     Parameters => Parameters(4)
 );

decisionalgo5 : entity work.DeltaEtaIncl1 --  Algorithm name: 0DETA10-Js1-Js2
  generic map (
     InputWidth => OutputWidthSortJET,
     MaxTob => 2,
     NumResultBits => 1 )
  port map (
     Tob => Js,
     Results => Results(AlgoOffset5+0 downto AlgoOffset5), 
     Overflow => Overflow(AlgoOffset5+0 downto AlgoOffset5), 
     ClockBus => ClockBus,
     Parameters => Parameters(5)
 );

end Behavioral;