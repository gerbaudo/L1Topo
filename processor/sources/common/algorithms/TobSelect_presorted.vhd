------------------------------------------------------------------------------------------
-- Author/Modified by	: sebastian.artz@uni-mainz.de
-- Description			: aligns all TOBs at the lower part of the array
--						  and creates a bitmask for this list
--						  supports input and output lists up to a length of 10
-- Number of registers	: 0
------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.L1TopoDataTypes.all;
use work.L1TopoFunctions.all;
use IEEE.math_real.all;

entity TobSelect_presorted is
	generic(InputWidth  : integer := 10;--5; --number of input channels
		    OutputWidth : integer := 10 --2  --number of selected TOBs
	);
	Port(Tob         : in  TOBArray(InputWidth - 1 downto 0); --input TOBs
		 cmp_res     : in  std_logic_vector(InputWidth - 1 downto 0); --list of selected TOBs ('1' if selected, '0' if not)
		 selTOBs     : out TOBArray(OutputWidth - 1 downto 0); --selected TOBs
		 end_cmp_res : out std_logic_vector(OutputWidth - 1 downto 0) --threshold flags for the next stage
	);
end TobSelect_presorted;

library ieee;
use ieee.numeric_std.all;
architecture Behavioral of TobSelect_presorted is

	constant InputWidthBase2 : integer := 2 ** integer(ceil(log2(real(InputWidth))));

	type TobCountArray is array (integer range <>) of integer range 0 to 16;
	type TobMatrix2 is array (integer range <>) of TOBArray(1 downto 0);
	type TobMatrix4 is array (integer range <>) of TOBArray(3 downto 0);
	type TobMatrix8 is array (integer range <>) of TOBArray(7 downto 0);
	type TobMatrix16 is array (integer range <>) of TOBArray(15 downto 0);

	signal selectBitsInBase2		: std_logic_vector(InputWidthBase2 - 1 downto 0);
	signal inputTOBsBase2			: TOBArray(InputWidthBase2 - 1 downto 0);
	signal tobCount2				: TobCountArray(8 - 1 downto 0);
	signal tobCount4				: TobCountArray(4 - 1 downto 0);
	signal tobCount8				: TobCountArray(2 - 1 downto 0);
	signal tobCount16				: TobCountArray(1 - 1 downto 0);
	
	signal mergedTobsMat2			: TobMatrix2(InputWidthBase2 - 1 downto 0);
	signal mergedTobsMat4			: TobMatrix4(InputWidthBase2 / 2 - 1 downto 0);
	signal mergedTobsMat8			: TobMatrix8(InputWidthBase2 / 4 - 1 downto 0);
	signal mergedTobsMat16			: TobMatrix16(InputWidthBase2 / 8 - 1 downto 0);
	signal mergedTobsMat16Final		: TobMatrix16(InputWidthBase2 / 16 - 1 downto 0);
	signal out_bits_tmp8 			: std_logic_vector(8 - 1 downto 0);
	signal out_bits_tmp16 			: std_logic_vector(10 - 1 downto 0);

begin

	GEN_INPUT_CUT : for i in InputWidth - 1 downto 0 generate
		inputTOBsBase2(i) <= Tob(i) when (cmp_res(i) = '1') else empty_tob;
	end generate;
	inputTOBsBase2(InputWidthBase2 - 1 downto InputWidth) <= (others => empty_tob);
	
	selectBitsInBase2(InputWidth - 1 downto 0) <= cmp_res;
	selectBitsInBase2(InputWidthBase2 - 1 downto InputWidth) <= (others => '0');

	GEN_MAT2 : for i in InputWidthBase2 - 1 downto 0 generate
		mergedTobsMat2(i)(0) <= inputTOBsBase2(i);
		mergedTobsMat2(i)(1) <= empty_tob;
	end generate;

	-- merge into list of length 2
	GEN_MERGE_2 : for i in (InputWidthBase2 / 2) - 1 downto 0 generate
		mergedTobsMat4(i)(3 downto 2) <= (others => empty_tob);
		mergedTobsMat4(i)(1 downto 0) <= 
			alignToLeftTobArray(mergedTobsMat2(2*i), mergedTobsMat2(2*i+1), to_integer(unsigned(selectBitsInBase2(2*i downto 2*i))));
		tobCount2(i) <= to_integer(unsigned(selectBitsInBase2(2*i downto 2*i))) + to_integer(unsigned(selectBitsInBase2(2*i+1 downto 2*i+1)));
	end generate;

	-- merge into list of length 4
	GEN_MERGE_4 : for i in (InputWidthBase2 / 4) - 1 downto 0 generate
		mergedTobsMat8(i)(7 downto 4) <= (others => empty_tob);
		mergedTobsMat8(i)(3 downto 0) <= 
			alignToLeftTobArray(mergedTobsMat4(2*i), mergedTobsMat4(2*i+1), tobCount2(2*i));
		tobCount4(i) <= tobCount2(2*i) + tobCount2(2*i + 1);
	end generate;
--
	-- merge into list of length 8
	GEN_MERGE_8 : for i in (InputWidthBase2 / 8) - 1 downto 0 generate
		mergedTobsMat16(i)(15 downto 8) <= (others => empty_tob);
		mergedTobsMat16(i)(7 downto 0) <= 
			alignToLeftTobArray(mergedTobsMat8(2*i), mergedTobsMat8(2*i+1), tobCount4(2*i));
		tobCount8(i) <= tobCount4(2*i) + tobCount4(2*i + 1);
	end generate;

	GEN_OUTP_8 : if (InputWidthBase2 = 8) generate
		selTOBs <= mergedTobsMat16(0)(OutputWidth - 1 downto 0);

		out_bits_tmp8 <=	"00000000" when (tobCount8(0) = 0) else
							"00000001" when (tobCount8(0) = 1) else
							"00000011" when (tobCount8(0) = 2) else
							"00000111" when (tobCount8(0) = 3) else
							"00001111" when (tobCount8(0) = 4) else
							"00011111" when (tobCount8(0) = 5) else
							"00111111" when (tobCount8(0) = 6) else
							"01111111" when (tobCount8(0) = 7) else
							"11111111";
							
		end_cmp_res <= std_logic_vector(resize(unsigned(out_bits_tmp8), OutputWidth));
	end generate;

	-- use one more step if input list length > 8 and <=16
	GEN_OUTP_16 : if (InputWidthBase2 = 16) generate

		-- merge into list of length 16
		GEN_MERGE_16 : for i in (InputWidthBase2 / 16) - 1 downto 0 generate
		mergedTobsMat16Final(i)(15 downto 0) <= 
			alignToLeftTobArray(mergedTobsMat16(2*i), mergedTobsMat16(2*i+1), tobCount8(2*i));
			tobCount16(i) <= tobCount8(2*i) + tobCount8(2*i + 1);
		end generate;
	
		selTOBs <= mergedTobsMat16Final(0)(OutputWidth - 1 downto 0);
		-- create bit mask depending on the number of TOBs
		out_bits_tmp16 <=	"0000000000" when (tobCount16(0) = 0) else
							"0000000001" when (tobCount16(0) = 1) else
							"0000000011" when (tobCount16(0) = 2) else
							"0000000111" when (tobCount16(0) = 3) else
							"0000001111" when (tobCount16(0) = 4) else
							"0000011111" when (tobCount16(0) = 5) else
							"0000111111" when (tobCount16(0) = 6) else
							"0001111111" when (tobCount16(0) = 7) else
							"0011111111" when (tobCount16(0) = 8) else
							"0111111111" when (tobCount16(0) = 9) else
							"1111111111";
							
		end_cmp_res <= std_logic_vector(resize(unsigned(out_bits_tmp16), OutputWidth));
	end generate;

end Behavioral;