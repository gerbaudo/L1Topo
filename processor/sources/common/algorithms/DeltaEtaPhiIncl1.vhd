------------------------------------------------------------------------------------------
-- Author/Modified by	: manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de
-- Description			: applies Et cut on one TOB list; calculates delta eta and delta phi
--						  of all TOB combinations and applies cut:
--						  ((deltaEta >= dEtaMin) OR (deltaPhi >= dPhiMin)) AND (deltaEta <= dEtaMax) AND  (deltaPhi <= dPhiMax)
-- Number of registers	: 0
------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use work.L1TopoDataTypes.ALL;
use work.L1TopoFunctions.ALL;

entity DeltaEtaPhiIncl1 is
	generic(InputWidth    : integer := 3;
		    MaxTob        : integer := 0; -- number of TOBs which should be considered
		    NumResultBits : integer := 1
	);
	Port(ClockBus   : in  std_logic_vector(2 downto 0);
		 Parameters : in  ParameterArray; --! [MinEt1, MinDeltaEta, MaxDeltaEta, MinDeltaPhi, MaxDeltaPhi]
		 Tob        : in  TOBArray(InputWidth - 1 downto 0);
		 Results    : out std_logic_vector(NumResultBits - 1 downto 0);
		 Overflow   : out std_logic_vector(NumResultBits - 1 downto 0)
	);
end DeltaEtaPhiIncl1;

architecture Behavioral of DeltaEtaPhiIncl1 is

	-- constants

	constant maxCount : integer := getMaxCount(InputWidth, MaxTob);

	-- Type declarations

	type deta is array (maxCount - 1 downto 1) of std_logic_vector(GenericEtaBitWidth - 1 downto 0);
	type detaarray is array (maxCount - 2 downto 0) of deta;
	type dphi is array (maxCount - 1 downto 1) of std_logic_vector(GenericPhiBitWidth - 1 downto 0);
	type dphiarray is array (maxCount - 2 downto 0) of dphi;
	type etaThreshArray is array (NumResultBits - 1 downto 0) of std_logic_vector(GenericEtaBitWidth - 1 downto 0);
	type phiThreshArray is array (NumResultBits - 1 downto 0) of std_logic_vector(GenericPhiBitWidth - 1 downto 0);
	type etThreshArray is array (NumResultBits - 1 downto 0) of std_logic_vector(GenericEtBitWidth - 1 downto 0);
	type thrAcceptArray is array (NumResultBits - 1 downto 0) of std_logic_vector(maxCount - 1 downto 0);

	-- Internal signal declarations

	signal deltaEta : detaarray;
	signal deltaPhi : dphiarray;

	signal MinDeltaEta, MaxDeltaEta : etaThreshArray;
	signal MinDeltaPhi, MaxDeltaPhi : phiThreshArray;
	signal MinEt1, MinEt2           : etThreshArray;
	signal thraccept1               : thrAcceptArray;
	signal thraccept2               : thrAcceptArray;

	signal results_out  : std_logic_vector(NumResultBits - 1 downto 0);
	signal overflow_out : std_logic_vector(NumResultBits - 1 downto 0);
	signal tob_in       : TOBArray(InputWidth - 1 downto 0);
	signal clock        : std_logic;

begin

	-- Behavioral

	clock  <= ClockBus(0);
	tob_in <= Tob;

	genParameters : for i in 0 to (NumResultBits - 1) generate
		MinEt1(i)      <= Parameters(0 + (6 * i))(GenericEtBitWidth - 1 downto 0);
		MinEt2(i)      <= Parameters(1 + (6 * i))(GenericEtBitWidth - 1 downto 0);
		MinDeltaEta(i) <= Parameters(2 + (6 * i))(GenericEtaBitWidth - 1 downto 0);
		MaxDeltaEta(i) <= Parameters(3 + (6 * i))(GenericEtaBitWidth - 1 downto 0);
		MinDeltaPhi(i) <= Parameters(4 + (6 * i))(GenericPhiBitWidth - 1 downto 0);
		MaxDeltaPhi(i) <= Parameters(5 + (6 * i))(GenericPhiBitWidth - 1 downto 0);
	end generate;

	min_tob_Et : process(tob_in, MinEt1, MinEt2) -- Flag TOBs with Et over threshold
	begin
		for k in 0 to (NumResultBits - 1) loop
			for i in 0 to (maxCount - 1) loop
				if (tob_in(i).Et > MinEt1(k)) then
					thraccept1(k)(i) <= '1';
				else
					thraccept1(k)(i) <= '0';
				end if;
			end loop;
			for i in 0 to (maxCount - 1) loop
				if (tob_in(i).Et > MinEt2(k)) then
					thraccept2(k)(i) <= '1';
				else
					thraccept2(k)(i) <= '0';
				end if;
			end loop;
		end loop;
	end process;

	deltaEtaPhi_calc1 : for i in 0 to (maxCount - 2) generate
		deltaEtaPhi_calc2 : for j in (i + 1) to (maxCount - 1) generate
			detadphiCalc_inst : entity work.DeltaRApproxBoxCutCalc
				port map(eta1     => tob_in(i).Eta,
					     eta2     => tob_in(j).Eta,
					     phi1     => tob_in(i).Phi,
					     phi2     => tob_in(j).Phi,
					     deltaEta => deltaEta(i)(j),
					     deltaPhi => deltaPhi(i)(j)
				);
		end generate;
	end generate;

	compare_thresholds : process(deltaEta, deltaPhi, thraccept1, thraccept2, MinDeltaEta, MaxDeltaEta, MinDeltaPhi, MaxDeltaPhi) -- Compare delta eta and delta phi with thresholds for all TOB pairs
		variable hits : std_logic_vector(NumResultBits - 1 downto 0);
	begin
		hits := (others => '0');
		for i in 0 to (maxCount - 2) loop -- loop over TOBs i and j
			for j in (i + 1) to (maxCount - 1) loop
				for k in 0 to NumResultBits - 1 loop -- loop over thresholds
					if (((unsigned(deltaEta(i)(j)) >= unsigned(MinDeltaEta(k))) OR (unsigned(deltaPhi(i)(j)) >= unsigned(MinDeltaPhi(k)))) AND (unsigned(deltaEta(i)(j)) <= unsigned(MaxDeltaEta(k))) AND (unsigned(deltaPhi(i)(j)) <= unsigned(MaxDeltaPhi(k))) AND ((
								(thraccept1(k)(i) = '1') and (thraccept2(k)(j) = '1')) or ((thraccept1(k)(j) = '1') and (thraccept2(k)(i) = '1')))
					) then
						hits(k) := '1';
					end if;
				end loop;
			end loop;
		end loop;
		results_out <= hits;
	end process;

	Results <= results_out;

	-----------------------------
	-- generate overflow bits  --
	-----------------------------
	genOverflows : for result in 0 to NumResultBits - 1 generate
		-- get overflow from input list
		overflow_out(result) <= Tob(0).Overflow;
	end generate;

	Overflow <= overflow_out;

end Behavioral;