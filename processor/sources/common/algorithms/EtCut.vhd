------------------------------------------------------------------------------------------
-- Author/Modified by	: manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de
-- Description			: applies Et cut on TOB list
-- Number of registers	: 0
------------------------------------------------------------------------------------------

--------------
-- includes --
--------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.L1TopoDataTypes.ALL;
use work.L1TopoFunctions.ALL;

------------------------
-- entity declaration --
------------------------

entity EtCut is
	generic(InputWidth    : integer := 1; --number of input TOBs
			MaxTob	      : integer := 0;
		    NumResultBits : integer := 1 --number of output bits
	);
	Port(ClockBus   : std_logic_vector(2 downto 0);
		 Parameters : in  ParameterArray;
		 Tob        : in  TOBArray(InputWidth - 1 downto 0);
		 Results    : out std_logic_vector(NumResultBits - 1 downto 0);
		 Overflow   : out std_logic_vector(NumResultBits - 1 downto 0)
	);
end EtCut;

-----------------------------------------
-- behavioral description of algorithm --
-----------------------------------------

architecture Behavioral of EtCut is

	constant maxCount : integer := getMaxCount(InputWidth, MaxTob);

	----------------------
	-- type definitions --
	----------------------
	type threshold_array is array (integer range <>) of std_logic_vector(GenericEtBitWidth - 1 downto 0);
	type accept_array is array (integer range <>) of std_logic;
	type accept_matrix is array (integer range <>) of accept_array(2 * InputWidth - 2 downto 0);

	-------------
	-- signals --
	-------------
	-- parameters:
	signal MinEt        : threshold_array(NumResultBits - 1 downto 0);
	-- temporary results:
	signal accept_tmp   : accept_matrix(NumResultBits - 1 downto 0);
	signal results_out  : std_logic_vector(NumResultBits - 1 downto 0);
	signal overflow_out : std_logic_vector(NumResultBits - 1 downto 0);

begin

	-- read parameters
	genReadParameters : for result in 0 to NumResultBits - 1 generate
		MinEt(result) <= Parameters(result)(GenericEtBitWidth - 1 downto 0);
	end generate;

	-- generate result bits (Et over threshold)
	genResults : for k in 0 to NumResultBits - 1 generate
		-- store for all TOBs if over threshold:
		genAcceptTemp : for i in 0 to maxCount - 1 generate
			accept_tmp(k)(i) <= '1' when (Tob(i).Et >= MinEt(k)) else '0';
		end generate;
		-- get result bit (OR of all TOBs):
		genAccept : for i in 0 to maxCount - 2 generate
			accept_tmp(k)(maxCount + i) <= accept_tmp(k)(2 * i) OR accept_tmp(k)(2 * i + 1);
		end generate;
		-- result is last element:
		results_out(k) <= accept_tmp(k)(2 * maxCount - 2);
	end generate;

	Results <= results_out;

	-----------------------------
	-- generate overflow bits  --
	-----------------------------
	genOverflows : for result in 0 to NumResultBits - 1 generate
		-- get overflow from input list
		overflow_out(result) <= Tob(0).Overflow;
	end generate;

	Overflow <= overflow_out;

end Behavioral;
