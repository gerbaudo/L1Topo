------------------------------------------------------------------------------------------
-- Author/Modified by	: manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de, 
--                        aschulte@cern.ch
-- Description			: DisambiguationDR: Checks if dRSqr > min_drSqr and <= max_drSqr
-- Number of registers	: 0
------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.L1TopoDataTypes.all;
use work.L1TopoFunctions.all;

entity DisambiguationDRIncl3 is
	generic(InputWidth1   : integer := 5; --EM or TAU (possible DR applied)
		    InputWidth2   : integer := 3; --TAU (possible DR applied)
		    InputWidth3   : integer := 3; --here Jet !
		    MaxTob1       : integer := 0; -- number of TOBs which should be considered
		    MaxTob2       : integer := 0; -- number of TOBs which should be considered
		    MaxTob3       : integer := 0; -- number of TOBs which should be considered
		    NumResultBits : integer := 1
	);
	Port(ClockBus   : std_logic_vector(2 downto 0);
		 Parameters : in  ParameterArray; --! [threshold1, threshold2, threshold3, min_EtaPhi, ...]
		 Tob1       : in  TOBArray(InputWidth1 - 1 downto 0); --! ClusterTOBs only (EM/TAU) !!!
		 Tob2       : in  TOBArray(InputWidth2 - 1 downto 0); --! ClusterTOBs only (EM/TAU) !!!
		 Tob3       : in  TOBArray(InputWidth3 - 1 downto 0);
		 Results    : out std_logic_vector(NumResultBits - 1 downto 0);
		 Overflow   : out std_logic_vector(NumResultBits - 1 downto 0)
	);
end DisambiguationDRIncl3;

architecture Behavioral of DisambiguationDRIncl3 is
begin
	DisambiguationIncl3_inst : entity work.DisambiguationIncl3
		generic map(
			InputWidth1   => InputWidth1,
			InputWidth2   => InputWidth2,
			InputWidth3   => InputWidth3,
			MaxTob1       => MaxTob1,
			MaxTob2       => MaxTob2,
			MaxTob3       => MaxTob3,
			NumResultBits => NumResultBits,
			ApplyDR       => 1
		)
		port map(
			Tob1       => Tob1,
			Tob2       => Tob2,
			Tob3       => Tob3,
			Parameters => Parameters,
			ClockBus   => ClockBus,
			Results    => Results,
			Overflow   => Overflow
		);

end Behavioral;
