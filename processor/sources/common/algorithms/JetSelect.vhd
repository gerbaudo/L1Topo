------------------------------------------------------------------------------------------
-- Author/Modified by	: manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de
-- Description			: applies eta cut to JetTOB list, selects TOBs
--						  above an Et threshold and converts them to GenericTOBs
-- Number of registers	: 2
------------------------------------------------------------------------------------------

--------------
-- includes --
--------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use work.L1TopoDataTypes.all;
use work.L1TopoFunctions.all;

------------------------
-- entity declaration --
------------------------

entity JetSelect is
	generic(InputWidth         : integer := InputWidthJET; --number of input TOBs
		    InputWidth1stStage : integer := InputWidth1stStageSelectJET; --number of TOBs in 1st stage
		    OutputWidth        : integer := 10; --number of selected output TOBs
		    JetSize            : integer := DefaultJetSize; --jet Et bit width (1 = 9 bit, 2 = 10 bit)
		    DoEtaCut           : integer := 1
	);
	Port(ClockBus    : in  std_logic_vector(2 downto 0);
		 Parameters  : in  ParameterArray;
		 JetTobArray : in  JetArray(InputWidth - 1 downto 0);
		 TobArrayOut : out TOBArray(OutputWidth - 1 downto 0)
	--		 Overflow    : out std_logic
	);
end JetSelect;

-----------------------------------------
-- behavioral description of algorithm --
-----------------------------------------

architecture Behavioral of JetSelect is

	---------------
	-- constants --
	---------------
	constant JetPresortedLength                   : integer := 30;
	constant OutputWidth1stStagePresorted         : integer := 10;
	constant JetPresortedLengthExtended           : integer := 33;
	constant OutputWidth1stStagePresortedExtended : integer := 11;
	constant InputWidthExtended                   : integer := InputWidth + 2;

	-----------
	-- types --
	-----------
	type vector_output_width is array (integer range <>) of std_logic_vector(OutputWidth - 1 downto 0);
	type vector_TOBArray is array (integer range <>) of TOBArray(OutputWidth - 1 downto 0);

	-------------
	-- signals --
	-------------
	-- parameters:tob_in
	signal MinEt                           : std_logic_vector(GenericEtBitWidth - 1 downto 0);
	signal MinEta                          : std_logic_vector(GenericAbsoluteEtaBitWidth - 1 downto 0);
	signal MaxEta                          : std_logic_vector(GenericAbsoluteEtaBitWidth - 1 downto 0);
	-- selected input TOBs (Eta):
	signal tob_in                          : JetArray(InputWidth - 1 downto 0);
	signal generic_in                      : TOBArray(InputWidth - 1 downto 0);
	signal select_pattern                  : std_logic_vector(InputWidth - 1 downto 0);
	signal select_pattern_extended         : std_logic_vector(InputWidthExtended - 1 downto 0);
	signal generic_in_cut                  : JetArray(InputWidth - 1 downto 0);
	signal generic_in_cut_extended         : JetArray(InputWidthExtended - 1 downto 0);
	-- selected TOBs after first stage (Et threshold):
	signal out_1stStage                    : TOBArray((OutputWidth1stStagePresortedExtended * ((InputWidthExtended) / JetPresortedLengthExtended)) - 1 downto 0);
	signal out_1stStage_short              : TOBArray((OutputWidth * ((InputWidthExtended) / JetPresortedLengthExtended)) - 1 downto 0);
	signal out_1stStage_preparation        : vector_TOBArray((InputWidthExtended / JetPresortedLengthExtended) - 1 downto 0);
	signal reg_out_1stStage                : TOBArray((OutputWidth * ((InputWidthExtended) / JetPresortedLengthExtended)) - 1 downto 0);
	-- intermediate results from first stage (used for second stage):
	signal cmp_res_1stStage                : std_logic_vector((OutputWidth * ((InputWidthExtended) / JetPresortedLengthExtended)) - 1 downto 0);
	signal cmp_res_1stStage_reg            : std_logic_vector((OutputWidth * ((InputWidthExtended) / JetPresortedLengthExtended)) - 1 downto 0);
	-- dummy (helper) signals:
	signal cmp_res_1stStage_reg_dmy        : std_logic_vector((OutputWidth * ((InputWidthExtended) / JetPresortedLengthExtended)) - 1 + OutputWidth downto 0);
	signal cmp_res_1stStage_reg_dmy_zeroes : std_logic_vector(OutputWidth - 1 downto 0) := (others => '0');
	signal reg_out_1stStage_dmy            : TOBArray((OutputWidth * (InputWidthExtended / JetPresortedLengthExtended)) - 1 + OutputWidth downto 0);
	signal reg_out_1stStage_dmy_empty      : TOBArray(OutputWidth - 1 downto 0);
	-- selected TOBs after second stage
	signal reg_out_2ndStage                : TOBArray(OutputWidth - 1 downto 0);
	-- overflow bit after second stage
	signal reg_over_2ndStage               : std_logic;
	signal reg_over_final                  : std_logic;
	signal reg_out_3                       : TOBArray(OutputWidth - 1 downto 0);
	signal overflow_reg1, overflow_reg2    : std_logic;
	signal overflow_all_cmx                : std_logic;
	signal overflow_all_cmx_1              : std_logic;
	signal overflow_all_cmx_2              : std_logic;
	signal overflow_all_cmx_3              : std_logic;
	signal overflow_all_cmx_4              : std_logic;
	signal overflow1stStage                : std_logic_vector(((InputWidthExtended) / JetPresortedLengthExtended) - 1 downto 0);
	signal select_pattern_out              : vector_output_width(((InputWidthExtended) / JetPresortedLengthExtended) - 1 downto 0);
	signal overflowOutputWidth             : std_logic;

begin
	tob_in <= JetTobArray;

	-- read parameters
	MinEt  <= Parameters(0)(GenericEtBitWidth - 1 downto 0);
	MinEta <= Parameters(1)(GenericAbsoluteEtaBitWidth - 1 downto 0);
	MaxEta <= Parameters(2)(GenericAbsoluteEtaBitWidth - 1 downto 0);

	generic_in_cut <= tob_in;

	gen_extend_1 : for i in 31 downto 0 generate
		generic_in_cut_extended(i) <= generic_in_cut(i);
		select_pattern_extended(i) <= select_pattern(i);
	end generate;

	gen_extend_2 : for i in 31 downto 0 generate
		generic_in_cut_extended(33 + i) <= generic_in_cut(32 + i);
		select_pattern_extended(33 + i) <= select_pattern(32 + i);
	end generate;

	generic_in_cut_extended(32) <= emptyJetTOB;
	select_pattern_extended(32) <= '0';
	generic_in_cut_extended(65) <= emptyJetTOB;
	select_pattern_extended(65) <= '0';

	------------------------------------------------------------------------
	-- first stage: select the input TOBs in groups of InputWidth1stStage --
	------------------------------------------------------------------------

	gen1stStage : for i in 0 to InputWidthExtended / JetPresortedLengthExtended - 1 generate
		selection_1stStage : entity work.SelectMergePresortedJet
			generic map(
				InputWidthBlocks   => 4, --JetPresortedLengthExtended / OutputWidth1stStagePresortedExtended,
				InputWidthPerBlock => 8, --OutputWidth1stStagePresortedExtended,
				OutputWidth        => OutputWidth,
				JetSize            => JetSize,
				DoEtaCut           => DoEtaCut
			)
			port map(
				clk40              => ClockBus(0),
				clkFast            => ClockBus(2),
				Parameters         => Parameters,
				inp                => generic_in_cut((i + 1) * 32 - 1 downto i * 32),
				select_pattern_in  => select_pattern((i + 1) * 32 - 1 downto i * 32),
				oup                => out_1stStage_preparation(i),
				select_pattern_out => select_pattern_out(i),
				overflow           => overflow1stStage(i)
			);

		overflowOutputWidth <= '1' when ((reg_ctrl(select_pattern_out(0)) + reg_ctrl(select_pattern_out(1))) > OutputWidth) else '0';

		--only take "OutputWidth" elements of the 10 that come out of this module
		out_1stStage_short((i + 1) * OutputWidth - 1 downto i * OutputWidth) <= out_1stStage_preparation(i)(OutputWidth - 1 downto 0);
		cmp_res_1stStage((i + 1) * OutputWidth - 1 downto i * OutputWidth)   <= select_pattern_out(i)(OutputWidth - 1 downto 0);
	end generate;

	overflow_all_cmx <= '1' when (((overflow1stStage(0) or overflow1stStage(1) or overflowOutputWidth) = '1'
			)) else '0';

	REG_OV_CMX_160 : process(ClockBus(2))
	begin
		if rising_edge(ClockBus(2)) then
			overflow_all_cmx_1 <= overflow_all_cmx;
			overflow_all_cmx_2 <= overflow_all_cmx_1;
			overflow_all_cmx_3 <= overflow_all_cmx_2;
			overflow_all_cmx_4 <= overflow_all_cmx_3;
		end if;
	end process;

	---------------------------------------------
	-- register the outputs of the first stage --
	---------------------------------------------

	cmp_res_1stStage_reg <= cmp_res_1stStage;
	reg_out_1stStage     <= out_1stStage_short;

	--------------------------
	-- prepare second stage --
	--------------------------

	cmp_res_1stStage_reg_dmy <= cmp_res_1stStage_reg_dmy_zeroes & cmp_res_1stStage_reg(((OutputWidth) * (InputWidthExtended / JetPresortedLengthExtended)) - 1 downto 0);

	--create TOBArray of empty_tob with a number of OutputWidth elements
	genEmptyTOBs : for i in 0 to OutputWidth - 1 generate
		reg_out_1stStage_dmy_empty(i) <= empty_tob;
	end generate;

	reg_out_1stStage_dmy <= reg_out_1stStage_dmy_empty & reg_out_1stStage(((OutputWidth) * (InputWidthExtended / JetPresortedLengthExtended)) - 1 downto 0);

	-------------------------------------------------
	-- second stage: merge the first-stage outputs --
	-------------------------------------------------

	merge_ins : entity work.SelectMerge
		generic map(InputWidthBlocks   => InputWidthExtended / JetPresortedLengthExtended,
			        InputWidthPerBlock => OutputWidth
		)
		Port map(clk40         => ClockBus(0),
			     clkFast       => ClockBus((InputWidthExtended / JetPresortedLengthExtended) / 2), --4:160 MHz, 2:80 MHz
			     threshold_res => cmp_res_1stStage_reg_dmy,
			     inp           => reg_out_1stStage_dmy,
			     oup           => reg_out_2ndStage,
			     overflow      => reg_over_2ndStage
		);

	----------------------------------------------------------
	-- register the output and overflow of the second stage --
	----------------------------------------------------------

	reg_out_3 <= reg_out_2ndStage;

	-- workaround to set overflow bit
	gen_set_overflow : for i in 0 to OutputWidth - 1 generate
		TobArrayOut(i).Et  <= reg_out_3(i).Et;
		TobArrayOut(i).Eta <= reg_out_3(i).Eta;
		TobArrayOut(i).Phi <= reg_out_3(i).Phi;
	end generate;

	--set Overflow
	gen_overflow_reg1 : entity work.StdLogicRegister
		port map(clk     => ClockBus(0),
			     enable  => '1',
			     reg_in  => JetTobArray(0).Overflow,
			     reg_out => overflow_reg1
		);

	gen_overflow_reg2 : entity work.StdLogicRegister
		port map(clk     => ClockBus(0),
			     enable  => '1',
			     reg_in  => overflow_reg1,
			     reg_out => overflow_reg2
		);

	TobArrayOut(0).Overflow <= reg_over_2ndStage or overflow_reg2 or overflow_all_cmx_4;

	-- only needed for simulation
	gen_set_dummy_overflow : for i in 1 to OutputWidth - 1 generate
		TobArrayOut(i).Overflow <= '0';
	end generate;

end Behavioral;
