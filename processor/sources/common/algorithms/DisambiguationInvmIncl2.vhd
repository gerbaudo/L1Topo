------------------------------------------------------------------------------------------
-- Author/Modified by	: sebastian.artz@uni-mainz.de
-- Description			: Disambiguation: Checks if coordinates are identical
--                        and applies cut on invariant mass
-- Number of registers	: 0 
-- created: 26.09.2016
-- Simulation: 
-- Python test:
------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.L1TopoDataTypes.all;
use work.L1TopoFunctions.all;

entity DisambiguationInvmIncl2 is
	generic(
		InputWidth1   : integer := 8;
		InputWidth2   : integer := 8;
		MaxTob1       : integer := 0;   -- number of TOBs which should be considered
		MaxTob2       : integer := 0;   -- number of TOBs which should be considered
		NumResultBits : integer := 1   -- Number of result bits
	);
	port(
		Tob1       : in  TOBArray(InputWidth1 - 1 downto 0); -- Two arrays of TOBs
		Tob2       : in  TOBArray(InputWidth2 - 1 downto 0);
		Parameters : in  ParameterArray;
		ClockBus   : in  std_logic_vector(2 downto 0);
		Results    : out std_logic_vector(NumResultBits - 1 downto 0);
		Overflow   : out std_logic_vector(NumResultBits - 1 downto 0)
	);
end DisambiguationInvmIncl2;

architecture Behavioral of DisambiguationInvmIncl2 is

	-- constants

	constant maxCount1 : integer := getMaxCount(InputWidth1, MaxTob1);
	constant maxCount2 : integer := getMaxCount(InputWidth2, MaxTob2);

	-- Type declarations

	type etThreshArray is array (NumResultBits - 1 downto 0) of std_logic_vector(GenericEtBitWidth - 1 downto 0);
	type thrAcceptArray1 is array (NumResultBits - 1 downto 0) of std_logic_vector(InputWidth1 - 1 downto 0);
	type thrAcceptArray2 is array (NumResultBits - 1 downto 0) of std_logic_vector(InputWidth2 - 1 downto 0);
	type Resultsarr is array (integer range <>) of std_logic_vector(InvariantMassSqrBitWidth - 1 downto 0);
	type Resultsmat is array (integer range <>) of Resultsarr(InputWidth2 - 1 downto 0);
	type Mthreshold is array (integer range <>) of std_logic_vector(InvariantMassSqrBitWidth - 1 downto 0);
	
	-- Internal signal declarations

	signal clock   : std_logic;
	signal tob_in1 : TOBArray(InputWidth1 - 1 downto 0);
	signal tob_in2 : TOBArray(InputWidth2 - 1 downto 0);

	signal MinEt1, MinEt2 : etThreshArray;
	signal thraccept_1 : thrAcceptArray1;
	signal thraccept_2 : thrAcceptArray2;
	signal mass_sqr_results : Resultsmat(InputWidth1 - 1 downto 0);
	signal MinMSqr    : Mthreshold(NumResultBits - 1 downto 0);
	signal MaxMSqr    : Mthreshold(NumResultBits - 1 downto 0);

	signal results_out  : std_logic_vector(NumResultBits - 1 downto 0);
	signal overflow_out : std_logic_vector(NumResultBits - 1 downto 0);

begin

	-- Behavioral

	clock   <= ClockBus(0);
	tob_in1 <= Tob1;
	tob_in2 <= Tob2;

	set_ranges_1 : for k in 0 to (NumResultBits - 1) generate
		MinEt1(k) <= Parameters(0 + (4 * k))(GenericEtBitWidth - 1 downto 0);
		MinEt2(k) <= Parameters(1 + (4 * k))(GenericEtBitWidth - 1 downto 0);
		MinMSqr(k) <= Parameters(2 + (4 * k))(InvariantMassSqrBitWidth - 1 downto 0);
		MaxMSqr(k) <= Parameters(3 + (4 * k))(InvariantMassSqrBitWidth - 1 downto 0);
	end generate;



	min_tob_Et : process(tob_in1, tob_in2, MinEt1, MinEt2) -- Flag TOBs with Et over threshold
	begin
		for k in 0 to (NumResultBits - 1) loop
			for i in 0 to (maxCount1 - 1) loop
				if (tob_in1(i).Et > MinEt1(k)) then
					thraccept_1(k)(i) <= '1';
				else
					thraccept_1(k)(i) <= '0';
				end if;
			end loop;
			for j in 0 to (maxCount2 - 1) loop
				if (tob_in2(j).Et > MinEt2(k)) then
					thraccept_2(k)(j) <= '1';
				else
					thraccept_2(k)(j) <= '0';
				end if;
			end loop;
		end loop;
	end process;

	gen1 : for i in 0 to MaxCount1 - 1 generate
		gen2 : for j in 0 to MaxCount2 - 1 generate
			Inv_mass_inst : entity work.InvMassCalc
				generic map(
					NumRegisters => 0
				)
				port map(ClockBus => ClockBus,
					     eta1     => tob_in1(i).Eta,
					     eta2     => tob_in2(j).Eta,
					     phi1     => tob_in1(i).Phi,
					     phi2     => tob_in2(j).Phi,
					     energy1  => tob_in1(i).Et,
					     energy2  => tob_in2(j).Et,
					     mass_sqr => mass_sqr_results(i)(j)
				);
		end generate;
	end generate;

	compare_thresholds_1 : process(Tob1, Tob2, thraccept_1, thraccept_2, MaxMSqr, MinMSqr, mass_sqr_results)
		variable hits : std_logic_vector(NumResultBits - 1 downto 0);
	begin
		hits := (others => '0');
		for i in 0 to (maxCount1 - 1) loop -- loop over TOBs i and j
			for j in 0 to (maxCount2 - 1) loop
				for k in 0 to NumResultBits - 1 loop -- loop over thresholds
					if (((Tob1(i).Eta /= Tob2(j).Eta) or (Tob1(i).Phi /= Tob2(j).Phi)) and (thraccept_1(k)(i) = '1') and (thraccept_2(k)(j) = '1')
						and (unsigned(mass_sqr_results(i)(j)) >= unsigned(MinMSqr(k))) and (unsigned(mass_sqr_results(i)(j)) <= unsigned(MaxMSqr(k)))
					) then
						hits(k) := '1';
					end if;
				end loop;
			end loop;
		end loop;
		results_out <= hits;
	end process;

	Results <= results_out;

	-----------------------------
	-- generate overflow bits  --
	-----------------------------
	genOverflows : for result in 0 to NumResultBits - 1 generate
		-- get overflow from input list
		overflow_out(result) <= Tob1(0).Overflow or Tob2(0).Overflow;
	end generate;

	Overflow <= overflow_out;

end Behavioral;
