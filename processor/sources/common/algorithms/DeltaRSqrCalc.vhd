------------------------------------------------------------------------------------------
-- Author/Modified by	: manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de
-- Description			: calculates delta r squared for two TOBs
-- Number of registers	: 0
------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.L1TopoDataTypes.ALL;

entity DeltaRSqrCalc is
	Port(deltaEta  : in  std_logic_vector(GenericEtaBitWidth - 1 downto 0);
		 deltaPhi  : in  std_logic_vector(GenericPhiBitWidth - 1 downto 0);
		 deltaRSqr : out std_logic_vector(2 * GenericEtaBitWidth downto 0) --! GenericEtaBitWidth needs to be >= GenericPhiBitWidth
	);
end DeltaRSqrCalc;

architecture Behavioral of DeltaRSqrCalc is
	signal deltaEta_us : unsigned(GenericEtaBitWidth - 1 downto 0);
	signal deltaPhi_us : unsigned(GenericPhiBitWidth - 1 downto 0);

	signal deltaEta_sqr : unsigned(2 * GenericEtaBitWidth - 1 downto 0);
	signal deltaPhi_sqr : unsigned(2 * GenericPhiBitWidth - 1 downto 0);

	signal deltaRsqr_tmp : unsigned(2 * GenericEtaBitWidth downto 0);

begin

	--std_logic_vector to unsigned
	deltaEta_us <= unsigned(deltaEta);
	deltaPhi_us <= unsigned(deltaPhi);

	--squares
	deltaEta_sqr <= deltaEta_us * deltaEta_us;
	deltaPhi_sqr <= deltaPhi_us * deltaPhi_us;

	--calculate deltaR
	deltaRsqr_tmp <= ('0' & deltaEta_sqr) + ("00" & deltaPhi_sqr);

	--unsigned to std_logic_vector
	deltaRSqr <= std_logic_vector(deltaRsqr_tmp);

end Behavioral;
