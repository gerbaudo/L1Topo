------------------------------------------------------------------------------------------
-- Author/Modified by	: manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de
-- Description			: applies Et cut on two TOB lists; calculates delta eta and delta phi of all 
--						  TOB combinations and applies approximated box cut
-- Number of registers	: 0
------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.L1TopoDataTypes.ALL;

entity DeltaRApproxBoxCutIncl2 is
	generic(InputWidth1   : integer := 5;
		    InputWidth2   : integer := 3;
		    NumResultBits : integer := 1
	);
	Port(ClockBus   : std_logic_vector(2 downto 0);
		 Parameters : in  ParameterArray; --! [threshold1, threshold2, max_R, min_R, ...]
		 Tob1       : in  TOBArray(InputWidth1 - 1 downto 0);
		 Tob2       : in  TOBArray(InputWidth2 - 1 downto 0);
		 Results    : out std_logic_vector(NumResultBits - 1 downto 0);
		 Overflow   : out std_logic_vector(NumResultBits - 1 downto 0)
	);
end DeltaRApproxBoxCutIncl2;

architecture Behavioral of DeltaRApproxBoxCutIncl2 is
	type acceptarr is array (integer range <>) of std_logic;
	type acceptmat is array (integer range <>) of acceptarr(2 * InputWidth2 - 2 downto 0);
	type acceptmat_list is array (integer range <>) of acceptmat(2 * InputWidth1 - 2 downto 0);

	type deltaarr is array (integer range <>) of std_logic_vector(4 downto 0);
	type deltamat is array (integer range <>) of deltaarr(2 * InputWidth2 - 2 downto 0);

	type Rthreshold is array (integer range <>) of std_logic_vector(4 downto 0);

	signal accept_tmp  : acceptmat_list(NumResultBits - 1 downto 0);
	signal accept_tmp2 : acceptmat_list(NumResultBits - 1 downto 0);

	signal thr_flag1 : std_logic_vector(InputWidth1 - 1 downto 0);
	signal thr_flag2 : std_logic_vector(InputWidth2 - 1 downto 0);

	signal deltaEta : deltamat(2 * InputWidth1 - 2 downto 0);
	signal deltaPhi : deltamat(2 * InputWidth1 - 2 downto 0);

	signal minEt1 : std_logic_vector(9 downto 0);
	signal MinEt2 : std_logic_vector(9 downto 0);
	signal MaxR   : Rthreshold(NumResultBits - 1 downto 0);
	signal MinR   : Rthreshold(NumResultBits - 1 downto 0);

	signal results_out  : std_logic_vector(NumResultBits - 1 downto 0);
	signal overflow_out : std_logic_vector(NumResultBits - 1 downto 0);

begin
	MinEt1 <= Parameters(0)(9 downto 0);
	MinEt2 <= Parameters(1)(9 downto 0);

	genThrArr : for i in 0 to NumResultBits - 1 generate
		MinR(i) <= Parameters(i * 2 + 2)(4 downto 0);
		MaxR(i) <= Parameters(i * 2 + 3)(4 downto 0); --TODO: does this work with 5 bits?
	end generate;

	genThrFlag1 : for i in 0 to InputWidth1 - 1 generate
		thr_flag1(i) <= '1' when Tob1(i).Et > MinEt1 else '0';
	end generate;

	genThrFlag2 : for i in 0 to InputWidth2 - 1 generate
		thr_flag2(i) <= '1' when Tob2(i).Et > MinEt2 else '0';
	end generate;

	gen1 : for i in 0 to InputWidth1 - 1 generate
		gen2 : for j in 0 to InputWidth2 - 1 generate
			dEta_inst : entity work.DeltaEtaCalc
				port map(eta1In      => Tob1(i).Eta,
					     eta2In      => Tob2(j).Eta,
					     deltaEtaOut => deltaEta(i)(j));
			dPhi_inst : entity work.DeltaPhiCalc
				port map(phi1In      => Tob1(i).Phi,
					     phi2In      => Tob2(j).Phi,
					     deltaPhiOut => deltaPhi(i)(j));
		end generate;
	end generate;

	genResults : for k in 0 to NumResultBits - 1 generate
		gen1 : for i in 0 to InputWidth1 - 1 generate
			gen2 : for j in 0 to InputWidth2 - 1 generate
				accept_tmp(k)(i)(j) <= '1' when ((deltaEta(i)(j) < MaxR(k)) AND (deltaEta(i)(j) > MinR(k) AND deltaPhi(i)(j) < MaxR(k)) AND (deltaPhi(i)(j) > MinR(k))) else '0';
			end generate;
		end generate;

		genAcceptTmp2a : for i in 0 to InputWidth1 - 1 generate
			genAcceptTmp2b : for j in 0 to InputWidth2 - 1 generate
				accept_tmp2(k)(i)(j) <= accept_tmp(k)(i)(j) AND thr_flag1(i) AND thr_flag2(j);
			end generate;
		end generate;

		genOutAccept1 : for i in 0 to InputWidth1 - 1 generate
			genx : for j in 0 to InputWidth2 - 2 generate
				accept_tmp2(k)(i)(InputWidth2 + j) <= accept_tmp2(k)(i)(2 * j) or accept_tmp2(k)(i)(2 * j + 1);
			end generate;
		end generate;

		genOutAccept2 : for i in 0 to InputWidth1 - 2 generate
			accept_tmp2(k)(InputWidth1 + i)(2 * InputWidth2 - 2) <= accept_tmp2(k)(2 * i)(2 * InputWidth2 - 2) or accept_tmp2(k)(2 * i + 1)(2 * InputWidth2 - 2);
		end generate;

		results_out(k) <= accept_tmp2(k)(2 * InputWidth1 - 2)(2 * InputWidth2 - 2);

	end generate;

	Results <= results_out;

	-----------------------------
	-- generate overflow bits  --
	-----------------------------
	genOverflows : for result in 0 to NumResultBits - 1 generate
		-- get overflow from input list
		overflow_out(result) <= Tob1(0).Overflow or Tob2(0).Overflow;
	end generate;

	Overflow <= overflow_out;

end Behavioral;
