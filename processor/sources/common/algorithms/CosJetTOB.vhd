------------------------------------------------------------------------------------------
-- Author/Modified by	: manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de
-- Description			: ?
-- Number of registers	: 0
------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.L1TopoDataTypes.all;

entity CosJetTOB is
	Port(inp : in  STD_LOGIC_VECTOR(GenericPhiBitWidth - 1 downto 0); -- signed, interval [0; 2*pi], LSB = 2*PI/32
		 oup : out STD_LOGIC_VECTOR(CosSinBitWidth - 1 downto 0)); -- interpreted as signed number in format (+0.0000000)
end CosJetTOB;

architecture Behavioral of CosJetTOB is
	type triglut is array (integer range 0 to 31) of std_logic_vector(CosSinBitWidth - 1 downto 0);
	signal intad : integer range 0 to 31;

	-- 1 sign bit + 1 integer bit(s) + 7 decimal bit(s); (32 entries)
	constant cosJettab : triglut := (
		"010000000",                    -- 1.0
		"001111110",                    -- 0.9807852804032304
		"001110110",                    -- 0.9238795325112867
		"001101010",                    -- 0.8314696123025452
		"001011011",                    -- 0.7071067811865476
		"001000111",                    -- 0.5555702330196023
		"000110001",                    -- 0.38268343236508984
		"000011001",                    -- 0.19509032201612833
		"000000000",                    -- 6.123233995736766e-17
		"111100111",                    -- -0.1950903220161282
		"111001111",                    -- -0.3826834323650897
		"110111001",                    -- -0.555570233019602
		"110100101",                    -- -0.7071067811865475
		"110010110",                    -- -0.8314696123025453
		"110001010",                    -- -0.9238795325112867
		"110000010",                    -- -0.9807852804032304
		"110000000",                    -- -1.0
		"110000010",                    -- -0.9807852804032304
		"110001010",                    -- -0.9238795325112868
		"110010110",                    -- -0.8314696123025455
		"110100101",                    -- -0.7071067811865477
		"110111001",                    -- -0.5555702330196022
		"111001111",                    -- -0.38268343236509034
		"111100111",                    -- -0.19509032201612866
		"000000000",                    -- -1.8369701987210297e-16
		"000011001",                    -- 0.1950903220161283
		"000110001",                    -- 0.38268343236509
		"001000111",                    -- 0.5555702330196018
		"001011011",                    -- 0.7071067811865474
		"001101010",                    -- 0.8314696123025452
		"001110110",                    -- 0.9238795325112865
		"001111110"                     -- 0.9807852804032303
	);

begin
	intad <= to_integer(unsigned(inp));
	oup   <= cosJettab(intad);

end Behavioral;