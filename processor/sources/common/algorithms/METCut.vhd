------------------------------------------------------------------------------------------
-- Author/Modified by	: manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de
-- Description			: applies cut on MET
-- Number of registers	: 0
------------------------------------------------------------------------------------------
--------------
-- includes --
--------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.L1TopoDataTypes.ALL;

------------------------
-- entity declaration --
------------------------

entity METCut is
	generic(NumResultBits : integer := 1 --number of output bits
	);
	port(ClockBus   : std_logic_vector(2 downto 0);
		 Parameters : in  ParameterArray; -- [min_Sqr]
		 Tob        : in  TOBArray((OutputWidthMET - 1) downto 0);
		 Results    : out std_logic_vector(NumResultBits - 1 downto 0);
		 Overflow   : out std_logic_vector(NumResultBits - 1 downto 0)
	);
end METCut;

-----------------------------------------
-- behavioral description of algorithm --
-----------------------------------------

architecture Behavioral of METCut is

	----------------------
	-- type definitions --
	----------------------
	type threshold_array is array (integer range <>) of std_logic_vector(METEnergyBitWidth - 1 downto 0);

	-------------
	-- signals --
	-------------
	-- parameters:
	signal thresholds   : threshold_array(NumResultBits - 1 downto 0);
	-- temporary results:
	signal results_out  : std_logic_vector(NumResultBits - 1 downto 0);
	signal overflow_out : std_logic_vector(NumResultBits - 1 downto 0);
	signal MET_in       : std_logic_vector(GenericEtBitWidth - 1 downto 0);

begin
	MET_in <= Tob(0).Et;                -- TODO: make MET object selectable (uncorrected=Tob(0).Et, corrected=Tob(1).Et)

	-- read parameters
	genReadParameters : for result in 0 to NumResultBits - 1 generate
		thresholds(result) <= Parameters(result)(METEnergyBitWidth - 1 downto 0);
	end generate;

	-- generate result bits (MET over threshold)
	genResults : for result in 0 to NumResultBits - 1 generate
		results_out(result) <= '1' when (unsigned(MET_in) >= unsigned(thresholds(result))) else '0';
	end generate;

	Results <= results_out;

	-----------------------------
	-- generate overflow bits  --
	-----------------------------
	genOverflows : for result in 0 to NumResultBits - 1 generate
		-- get overflow from input list
		overflow_out(result) <= Tob(0).Overflow;
	end generate;

	Overflow <= overflow_out;

end Behavioral;