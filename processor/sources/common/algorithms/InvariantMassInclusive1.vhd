------------------------------------------------------------------------------------------
-- Author/Modified by	: manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de
-- Description			: applies Et cu on one TOB list; calculates the invariant masses 
--						  and applies cut on these
-- Number of registers	: 0
-- behavioral simulation : ok (~ 0.02% errors)
------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use work.L1TopoDataTypes.ALL;
use work.L1TopoFunctions.ALL;

entity InvariantMassInclusive1 is
	generic(InputWidth       : integer := 3;
		    MaxTob           : integer := 0;
		    NumResultBits    : integer := 1;
		    RequireOneBarrel : integer := 0;
		    NumRegisters     : integer := 0
	);
	Port(ClockBus   : in  std_logic_vector(2 downto 0);
		 Tob        : in  TOBArray(InputWidth - 1 downto 0);
		 Parameters : in  ParameterArray;
		 Results    : out std_logic_vector(NumResultBits - 1 downto 0);
		 Overflow   : out std_logic_vector(NumResultBits - 1 downto 0)
	);
end InvariantMassInclusive1;

architecture Behavioral of InvariantMassInclusive1 is
	constant maxCount : integer := getMaxCount(InputWidth, MaxTob);

	type thrAcceptArray is array (NumResultBits - 1 downto 0) of std_logic_vector(InputWidth - 1 downto 0);
	type Resultsarr is array (integer range <>) of std_logic_vector(InvariantMassSqrBitWidth - 1 downto 0);
	type Resultsmat is array (integer range <>) of Resultsarr(InputWidth - 1 downto 0);

	type etThreshArray is array (integer range <>) of std_logic_vector(GenericEtBitWidth - 1 downto 0);
	type Mthreshold is array (integer range <>) of std_logic_vector(2 * GenericEtBitWidth + CoshBitWidth + 1 - 7 - 1 downto 0);

	signal mass_sqr_results : Resultsmat(InputWidth - 1 downto 0);

	signal tob_in : TOBArray(InputWidth - 1 downto 0);

	signal results_out  : std_logic_vector(NumResultBits - 1 downto 0);
	signal overflow_out : std_logic_vector(NumResultBits - 1 downto 0);

	signal MinEt1       : etThreshArray(NumResultBits - 1 downto 0);
	signal MinEt2       : etThreshArray(NumResultBits - 1 downto 0);
	signal MinMSqr      : Mthreshold(NumResultBits - 1 downto 0);
	signal MaxMSqr      : Mthreshold(NumResultBits - 1 downto 0);
	signal thraccept1   : thrAcceptArray;
	signal thraccept2   : thrAcceptArray;
	signal InsideBarrel : std_logic_vector(InputWidth - 1 downto 0);

begin
	tob_in <= Tob;

	genThrArr : for i in 0 to NumResultBits - 1 generate
		MinEt1(i)  <= Parameters(4 * i + 0)(GenericEtBitWidth - 1 downto 0);
		MinEt2(i)  <= Parameters(4 * i + 1)(GenericEtBitWidth - 1 downto 0);
		MinMSqr(i) <= Parameters(4 * i + 2)(InvariantMassSqrBitWidth - 1 downto 0);
		MaxMSqr(i) <= Parameters(4 * i + 3)(InvariantMassSqrBitWidth - 1 downto 0);
	end generate;

	min_tob_Et : process(tob_in, MinEt1, MinEt2) -- Flag TOB with Et over threshold -- TODO: BUGGY?
	begin
		for k in 0 to (NumResultBits - 1) loop
			for i in 0 to (maxCount - 1) loop
				if (tob_in(i).Et > MinEt1(k)) then
					thraccept1(k)(i) <= '1';
				else
					thraccept1(k)(i) <= '0';
				end if;
			end loop;
			for i in 0 to (maxCount - 1) loop
				if (tob_in(i).Et > MinEt2(k)) then
					thraccept2(k)(i) <= '1';
				else
					thraccept2(k)(i) <= '0';
				end if;
			end loop;
		end loop;
	end process;

	genInsideBarrel : for i in 0 to maxCount - 1 generate
		InsideBarrel(i) <= '1' when abs (signed(Tob(i).Eta)) <= 10 else '0';
	end generate;

	deltaEta_calc1 : for i in 0 to (maxCount - 2) generate
		deltaEta_calc2 : for j in (i + 1) to (maxCount - 1) generate
			Inv_mass_inst : entity work.InvMassCalc
				generic map(
					NumRegisters => NumRegisters
				)
				port map(ClockBus => ClockBus,
					     eta1     => tob_in(i).Eta,
					     eta2     => tob_in(j).Eta,
					     phi1     => tob_in(i).Phi,
					     phi2     => tob_in(j).Phi,
					     energy1  => tob_in(i).Et,
					     energy2  => tob_in(j).Et,
					     mass_sqr => mass_sqr_results(i)(j)
				);
		end generate;
	end generate;

	compare_thresholds : process(mass_sqr_results, thraccept1, thraccept2, MinMSqr, MaxMSqr, InsideBarrel) -- Compare delta eta with thresholds for all TOB pairs -- TODO: BUGGY?
		variable hits : std_logic_vector(NumResultBits - 1 downto 0);
	begin
		hits := (others => '0');
		for i in 0 to (maxCount - 2) loop -- loop over TOBs i and j
			for j in (i + 1) to (maxCount - 1) loop
				for k in 0 to NumResultBits - 1 loop -- loop over thresholds
					if RequireOneBarrel = 1 then
						if ((unsigned(mass_sqr_results(i)(j)) >= unsigned(MinMSqr(k))) and (unsigned(mass_sqr_results(i)(j)) <= unsigned(MaxMSqr(k))) and (((thraccept1(k)(i) = '1') and (thraccept2(k)(j) = '1')) or ((thraccept1(k)(j) = '1') and (thraccept2(k)(i) =
										'1'))) AND ((InsideBarrel(i) = '1') OR (InsideBarrel(j) = '1'))) then
							hits(k) := '1';
						end if;
					end if;
					if RequireOneBarrel = 0 then
						if ((unsigned(mass_sqr_results(i)(j)) >= unsigned(MinMSqr(k))) and (unsigned(mass_sqr_results(i)(j)) <= unsigned(MaxMSqr(k))) and (((thraccept1(k)(i) = '1') and (thraccept2(k)(j) = '1')) or ((thraccept1(k)(j) = '1') and (thraccept2(k)(i) =
										'1')))
						) then
							hits(k) := '1';
						end if;
					end if;
				end loop;
			end loop;
		end loop;
		results_out <= hits;
	end process;

	Results <= results_out;

	-----------------------------
	-- generate overflow bits  --
	-----------------------------
	genOverflows : for result in 0 to NumResultBits - 1 generate
		-- get overflow from input list
		overflow_out(result) <= Tob(0).Overflow;
	end generate;

	Overflow <= overflow_out;

end Behavioral;
