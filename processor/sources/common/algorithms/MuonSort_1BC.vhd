------------------------------------------------------------------------------------------
-- Author/Modified by	: manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de
-- Description			: applies eta cut to muonTOB list, sorts TOBs,
--						  converts them to GenericTOBs and returns short sorted list
-- Number of registers	: 1
------------------------------------------------------------------------------------------

--------------
-- includes --
--------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use work.l1topo_package.all;
use work.L1TopoDataTypes.all;
use work.L1TopoFunctions.all;

------------------------
-- entity declaration --
------------------------

entity MuonSort_1BC is
	generic(InputWidth    : integer := InputWidthMU; --number of input TOBs
		    OutputWidth   : integer := OutputWidthSortMU; --number of sorted output TOBs
		    nDelayedMuons : integer := NumberOfDelayedMuons --number of output delayed muons
	);
	Port(ClockBus           : in  std_logic_vector(2 downto 0);
		 Parameters         : in  ParameterArray;
		 MuonTobArray       : in  MuonArray(InputWidth - 1 downto 0);
		 TobArrayOut        : out TOBArray(nDelayedMuons - 1 downto 0)
	);
end MuonSort_1BC;

-----------------------------------------
-- behavioral description of algorithm --
-----------------------------------------

architecture Behavioral of MuonSort_1BC is

	-------------
	-- signals --
	-------------
	signal MinEta                          : std_logic_vector(GenericAbsoluteEtaBitWidth - 1 downto 0);
	signal MaxEta                          : std_logic_vector(GenericAbsoluteEtaBitWidth - 1 downto 0);
	signal generic_in, generic_in_cut    : TOBArray(InputWidth - 1 downto 0);
	signal generic_out : TOBArray(nDelayedMuons - 1 downto 0);
	signal overflow_octant : std_logic := '0';

begin

	-- read parameters
	MinEta <= Parameters(0)(GenericAbsoluteEtaBitWidth - 1 downto 0);
	MaxEta <= Parameters(1)(GenericAbsoluteEtaBitWidth - 1 downto 0);

	----------------------------------------------------------
	-- translate to generic TOB and select TOBs (Eta range) --
	----------------------------------------------------------

	sel_eta_range : process(MinEta, MaxEta, MuonTobArray, generic_in)
		variable tmpOverflow : std_logic;
	begin
		tmpOverflow := '0';
		
		for i in 0 to InputWidth - 1 loop
		
			generic_in(i) <= to_GenericTOB(MuonTobArray(i));
			
			-- discard empty TOBs and apply eta cut
			if (MuonTobArray(i).no_candidate = '0' and is_in_eta_range_tob(generic_in(i), MinEta, MaxEta) and MuonTobArray(i).Pt /= "11") then		
				generic_in_cut(i) <= generic_in(i);
			else
				generic_in_cut(i) <= empty_tob;
			end if;
						
			if (MuonTobArray(i).Pt = "11") then
				tmpOverflow := '1';
			end if;
		end loop;
		overflow_octant <= tmpOverflow;
	end process;

	-------------------------
	-- sort the input TOBs --
	-------------------------

	sortSingleStage : entity work.TobSort
		generic map(OutputWidth => nDelayedMuons,
			        InputWidth  => InputWidth,
			        DoPresort   => 1
		)
		port map(Tob         => generic_in_cut,
			     TobArrayOut => generic_out
		);

	-------------------------
	-- register the output --
	-------------------------

	proc_reg_out : process (ClockBus(0))
	begin
		if rising_edge(ClockBus(0)) then
			for i in nDelayedMuons - 1 downto 0 loop
			TobArrayOut(i).Et <= generic_out(nDelayedMuons - i - 1).Et;
			TobArrayOut(i).Eta <= generic_out(nDelayedMuons - i - 1).Eta;
			TobArrayOut(i).Phi <= generic_out(nDelayedMuons - i - 1).Phi;
		end loop;
		
		TobArrayOut(0).Overflow <= MuonTobArray(0).Overflow or overflow_octant;
		
		for i in nDelayedMuons - 1 downto 1 loop
			TobArrayOut(i).Overflow <= '0';
		end loop;
		end if;
	end process;

end Behavioral;
