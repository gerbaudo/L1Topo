------------------------------------------------------------------------------------------
-- Author/Modified by	: manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de
-- Description			: apples Et cut on two lists; calcs delta phi and returns
--						  true if all delta phi pass the cut
-- Number of registers	: 0
------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
--use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use work.L1TopoDataTypes.all;
use work.L1TopoFunctions.all;

entity MinDeltaPhiIncl2 is
	generic(
		InputWidth1   : integer := 8;
		InputWidth2   : integer := 8;
		MaxTob1       : integer := 0;   -- number of TOBs which should be considered
		MaxTob2       : integer := 0;   -- number of TOBs which should be considered
		NumResultBits : integer := 1    -- Number of result bits
	);
	port(
		Tob1       : in  TOBArray(InputWidth1 - 1 downto 0); -- Two arrays of TOBs
		Tob2       : in  TOBArray(InputWidth2 - 1 downto 0);
		Parameters : in  ParameterArray;
		ClockBus   : in  std_logic_vector(2 downto 0);
		Results    : out std_logic_vector(NumResultBits - 1 downto 0);
		Overflow   : out std_logic_vector(NumResultBits - 1 downto 0)
	);
end MinDeltaPhiIncl2;

library ieee;
use ieee.numeric_std.all;
architecture Behavioral of MinDeltaPhiIncl2 is

	-- constants

	constant MaxCount1 : integer := getMaxCount(InputWidth1, MaxTob1);
	constant MaxCount2 : integer := getMaxCount(InputWidth2, MaxTob2);

	-- Type declarations

	type etThreshArray is array (NumResultBits - 1 downto 0) of std_logic_vector(GenericEtBitWidth - 1 downto 0);
	type dphi is array (InputWidth2 - 1 downto 0) of std_logic_vector(GenericPhiBitWidth - 1 downto 0);
	type dphiarray is array (InputWidth1 - 1 downto 0) of dphi;
	type thresharray is array (NumResultBits - 1 downto 0) of std_logic_vector(GenericPhiBitWidth - 1 downto 0);
	type thrAcceptArray1 is array (NumResultBits - 1 downto 0) of std_logic_vector(InputWidth1 - 1 downto 0);
	type thrAcceptArray2 is array (NumResultBits - 1 downto 0) of std_logic_vector(InputWidth2 - 1 downto 0);

	-- Internal signal declarations

	signal deltaphi       : dphiarray;
	signal MinPhi         : thresharray;
	signal MinEt1, MinEt2 : etThreshArray;
	signal thraccept_1    : thrAcceptArray1;
	signal thraccept_2    : thrAcceptArray2;

	signal results_out  : std_logic_vector(NumResultBits - 1 downto 0);
	signal overflow_out : std_logic_vector(NumResultBits - 1 downto 0);
	signal tob_in1      : TOBArray(InputWidth1 - 1 downto 0);
	signal tob_in2      : TOBArray(InputWidth2 - 1 downto 0);
	signal clock        : std_logic;

begin                                   -- Behavioral

	clock   <= ClockBus(0);
	tob_in1 <= Tob1;
	tob_in2 <= Tob2;

	set_ranges : for i in 0 to (NumResultBits - 1) generate -- Extract delta phi cuts
		MinEt1(i) <= Parameters(0 + (3 * i))(GenericEtBitWidth - 1 downto 0);
		MinEt2(i) <= Parameters(1 + (3 * i))(GenericEtBitWidth - 1 downto 0);
		MinPhi(i) <= Parameters(2 + (3 * i))(GenericPhiBitWidth - 1 downto 0);
	end generate;

	min_tob_Et : process(tob_in1, tob_in2, MinEt1, MinEt2) -- Flag TOB with Et over threshold -- TODO: BUGGY?
	begin
		for k in 0 to (NumResultBits - 1) loop
			for i in 0 to (MaxCount1 - 1) loop
				if (tob_in1(i).Et > MinEt1(k)) then
					thraccept_1(k)(i) <= '1';
				else
					thraccept_1(k)(i) <= '0';
				end if;
			end loop;
			for i in 0 to (MaxCount2 - 1) loop
				if (tob_in2(i).Et > MinEt2(k)) then
					thraccept_2(k)(i) <= '1';
				else
					thraccept_2(k)(i) <= '0';
				end if;
			end loop;
		end loop;
	end process;

	deltaPhi_calc1 : for i in 0 to (MaxCount1 - 1) generate
		deltaPhi_calc2 : for j in 0 to (MaxCount2 - 1) generate
			dphiCalc_inst : entity work.DeltaPhiCalc
				port map(
					phi1In      => tob_in1(i).phi,
					phi2In      => tob_in2(j).phi,
					deltaPhiOut => deltaphi(i)(j)
				);
		end generate;
	end generate;

	compare_thresholds : process(deltaphi, MinPhi, thraccept_1, thraccept_2) -- Compare delta phi with thresholds for all TOB pairs
		variable hits             : std_logic_vector(NumResultBits - 1 downto 0);
		variable anyAcc1, anyAcc2 : std_logic_vector(NumResultBits - 1 downto 0);
	begin

		-- check if there is any combination surviving the Et cuts
		anyAcc1 := (others => '0');
		anyAcc2 := (others => '0');
		for k in 0 to NumResultBits - 1 loop
			for i in 0 to (MaxCount1 - 1) loop
				anyAcc1(k) := anyAcc1(k) or thraccept_1(k)(i);
			end loop;
			for j in 0 to (MaxCount2 - 1) loop
				anyAcc2(k) := anyAcc2(k) or thraccept_2(k)(j);
			end loop;
		end loop;

		hits := anyAcc1 and anyAcc2;
		for i in 0 to (MaxCount1 - 1) loop -- loop over TOBs i and j
			for j in 0 to (MaxCount2 - 1) loop
				for k in 0 to NumResultBits - 1 loop -- loop over thresholds
					if (((unsigned(deltaphi(i)(j)) <= unsigned(MinPhi(k)))) and (thraccept_1(k)(i) = '1') and (thraccept_2(k)(j) = '1')
					) then
						hits(k) := '0';
					end if;
				end loop;
			end loop;
		end loop;

		results_out <= hits;
	end process;

	Results <= results_out;

	-----------------------------
	-- generate overflow bits  --
	-----------------------------
	genOverflows : for result in 0 to NumResultBits - 1 generate
		-- get overflow from input list
		overflow_out(result) <= Tob1(0).Overflow or Tob2(0).Overflow;
	end generate;

	Overflow <= overflow_out;

end Behavioral;