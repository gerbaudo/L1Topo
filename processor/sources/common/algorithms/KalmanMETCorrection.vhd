------------------------------------------------------------------------------------------
-- Author/Modified by	: manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de
-- Description	:	Calculates a corrected MET using correction factors calculated by a
--				Kalman filter (proposed by Antonia Strubig, Sascha Caron, Jeroen Schouwenberg)
-- Number of registers	: 1
------------------------------------------------------------------------------------------

--------------
-- includes --
--------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.l1topo_package.all;            -- needed for function ld
use work.L1TopoDataTypes.all;
use work.L1TopoFunctions.all;

------------------------
-- entity declaration --
------------------------

entity KalmanMETCorrection is
	generic(InputWidth    : integer := InputWidthJET;
		    NumResultBits : integer := 1 --number of output bits
	);
	Port(ClockBus   : in  std_logic_vector(2 downto 0);
		 Parameters : in  ParameterArray;
		 Tob1       : in  MetArray(0 downto 0);
		 Tob2       : in  TOBArray(InputWidth - 1 downto 0);
		 Results    : out std_logic_vector(NumResultBits - 1 downto 0);
		 Overflow   : out std_logic_vector(NumResultBits - 1 downto 0)
--		 ;sum_METSqrOut             : out std_logic_vector(2 * (16 + 1) - 1 downto 0) -- simulation only
	);
end KalmanMETCorrection;

-----------------------------------------
-- behavioral description of algorithm --
-----------------------------------------

architecture Behavioral of KalmanMETCorrection is

	---------------
	-- constants --
	---------------
	constant SumBitWidth                : integer := 16;
	constant JetEtCorrectedBitWidth     : integer := GenericEtBitWidth + KalmanMETCorrection_correctionBitWidth; -- -1 for minus 1 sign bit
	constant JetEtComponentLongBitWidth : integer := JetEtCorrectedBitWidth + CosSinBitWidth - 1;
    constant lookupTableEtEta_in : std_logic_vector(KalmanMETCorrection_numberOfEtBins * KalmanMETCorrection_numberOfEtaBins * KalmanMETCorrection_correctionBitWidth - 1 downto 0) := "111111011111110001111101101111101111111101101111101110111101110111110110111111111111110110111110111111110110111110010111110001111110001111110001111111011111110001111101101111101111111101101111101110111101110111110110111111111111110110111110111111110110111110010111110001111110001111110001111111011111110001111101101111101111111101101111101110111101110111110110111111111111110110111110111111110110111110010111110001111110001111110001000000110111111100111111000111111100111111000111111010111111011000000101000001111000000100000000100000000011111111111111111101111111101111111111000010110000001111000001100000001111000001010000001100000001110000011011000100100000010111000011000000010110000010001000001110000001110000010001000100100000110001000101101000101010000010010000010100000011000000101110000110111000100110000100110000100011000011010000010111000010110000011000000101010001001111001001100001000011000010011000010100000011000000110001000110110000100110000100110000100001000010110000001111000001111000010010000101010001001111001001100001000011000010011000010100000011000000110001000110110000100110000100110000100001000010110000001111000001111000010010000101010001001111001001100001000011000010011000010100000011000000110001000110110000100110000100110000100001000010110000001111000001111000010010000101010001001111001001100001000011000010011000010100000011000000110001000110110000100110000100110000100001000010110000001111000001111000010010";

	----------------------
	-- type definitions --
	----------------------
	type lookupCorrEtaArray is array (natural range <>) of signed(KalmanMETCorrection_correctionBitWidth - 1 downto 0);
	type lookupTableEtEtaArray is array (natural range <>) of lookupCorrEtaArray(KalmanMETCorrection_numberOfEtaBins - 1 downto 0);
	type intArray is array (natural range <>) of natural;
	type corrFactorArray is array (natural range <>) of signed(KalmanMETCorrection_correctionBitWidth - 1 downto 0);
	type jetEtCorrectedArray is array (natural range <>) of std_logic_vector(JetEtCorrectedBitWidth - 1 downto 0); -- minus one sign bit
	type jetEtCorrectedLongArray is array (natural range <>) of std_logic_vector(JetEtCorrectedBitWidth + 1 - 1 downto 0); -- minus one sign bit
	type triangResultArray is array (natural range <>) of std_logic_vector(CosSinBitWidth - 1 downto 0); -- sin/cos result array
	type sumarr is array (integer range <>) of std_logic_vector((SumBitWidth + KalmanMETCorrection_correctionBitWidth - 2 + 1) - 1 downto 0); -- "+1": saturation
	type sumarrWithSaturation is array (integer range <>) of std_logic_vector((SumBitWidth + KalmanMETCorrection_correctionBitWidth + 1 - 2 + 1) - 1 downto 0); -- "+1": saturation
	type sumarr_long is array (integer range <>) of std_logic_vector(JetEtComponentLongBitWidth - 1 downto 0);
	type minMetArray is array (integer range <>) of std_logic_vector(SumBitWidth + 1 - 1 downto 0);
	type etaArray is array (integer range <>) of signed(GenericEtaBitWidth - 1 downto 0);
	type etaArrayShort is array (integer range <>) of unsigned(4 - 1 downto 0);

	-------------
	-- signals --
	-------------
	-- parameters:
	signal MinEt                  : std_logic_vector(GenericEtBitWidth - 1 downto 0); -- threshold to decide minimal energies used in the sum
	signal MinMET                 : minMetArray(NumResultBits - 1 downto 0);
	signal etBin                  : intArray(InputWidth - 1 downto 0);
	signal etaBin                 : intArray(InputWidth - 1 downto 0);
	signal jetEtCorrected_long    : jetEtCorrectedLongArray(InputWidth - 1 downto 0);
	signal jetEtCorrected         : jetEtCorrectedArray(InputWidth - 1 downto 0);
	signal cos_phi                : triangResultArray(InputWidth - 1 downto 0);
	signal sin_phi                : triangResultArray(InputWidth - 1 downto 0);
	signal corrFactor             : corrFactorArray(InputWidth - 1 downto 0);
	signal saturationCorrect      : std_logic_vector(2 * InputWidth - 2 downto 0);
	signal saturationCorrect2     : std_logic;
	signal overflow_out_tmp       : std_logic_vector(0 downto 0);
	signal lookupTableEtEta       : lookupTableEtEtaArray(KalmanMETCorrection_numberOfEtBins - 1 downto 0);
	signal tmp_Ex                 : sumarr(InputWidth - 1 downto 0);
	signal tmp_Ey                 : sumarr(InputWidth - 1 downto 0);
	signal tmp_sum_1stStage       : sumarr(2 * InputWidth - 2 - 1 downto 0);
	signal tmp_sum_Ex_1stStage    : sumarr(InputWidth - 1 downto 0);
	signal tmp_sum_Ey_1stStage    : sumarr(InputWidth - 1 downto 0);
	signal tmp_sum_Ex_2ndStage    : sumarrWithSaturation(2 * InputWidth - 2 downto 0);
	signal tmp_sum_Ey_2ndStage    : sumarrWithSaturation(2 * InputWidth - 2 downto 0);
	signal saturation_sum         : std_logic_vector(2 * InputWidth - 2 downto 0);
	signal tmp_Ex_long            : sumarr_long(InputWidth - 1 downto 0);
	signal tmp_Ey_long            : sumarr_long(InputWidth - 1 downto 0);
	signal sum_Ex                 : std_logic_vector(SumBitWidth + 1 - 1 downto 0);
	signal sum_Ey                 : std_logic_vector(SumBitWidth + 1 - 1 downto 0);
	signal sum_Ex_inverse         : std_logic_vector(SumBitWidth + 1 - 1 downto 0);
	signal sum_Ey_inverse         : std_logic_vector(SumBitWidth + 1 - 1 downto 0);
	signal signed_Ex_corrected    : std_logic_vector(SumBitWidth + 1 - 1 downto 0);
	signal signed_Ey_corrected    : std_logic_vector(SumBitWidth + 1 - 1 downto 0);
	signal sum_METSqr             : std_logic_vector(2 * (SumBitWidth + 1) - 1 downto 0);
	signal phi_corrected          : std_logic_vector(GenericPhiBitWidth - 1 downto 0);
	signal generic_in             : TOBArray(InputWidth - 1 downto 0);
	signal sel_jets               : TOBArray(InputWidth - 1 downto 0);
	signal METSqrCorrected        : std_logic_vector(METSqrEnergyBitWidth - 1 downto 0);
	signal METPhiCorrected        : std_logic_vector(GenericPhiBitWidth - 1 downto 0);
	signal result_1, result_2     : std_logic_vector(NumResultBits - 1 downto 0);
	signal tmpOver1, tmpOver2     : std_logic_vector(0 downto 0);
	signal tmpEta                 : etaArray(InputWidth - 1 downto 0);
	signal tmpEtaShort			  : etaArrayShort(InputWidth - 1 downto 0);
	signal tmpEtaHighBin		  : std_logic_vector(InputWidth - 1 downto 0);
	signal test                   : signed(4 downto 0);
	signal metTobReg1, metTobReg2 : MetArray(0 downto 0);

begin
	generic_in <= Tob2 when rising_edge(ClockBus(0));
	metTobReg1 <= Tob1;

--	sum_METSqrOut <= sum_METSqr; -- simulation only

	-- read parameters
	MinEt <= Parameters(0)(GenericEtBitWidth - 1 downto 0);
	gen_read_param_MinMet : for i in 0 to NumResultBits - 1 generate
		MinMET(i) <= Parameters(1 + i)(MinMET(0)'length - 1 downto 0);
	end generate;

	-- build lookup table from input signal "lookupTableEtEta_in"
	lookup_gen1 : for Eta in 0 to KalmanMETCorrection_numberOfEtaBins - 1 generate
		lookup_gen2 : for Et in 0 to KalmanMETCorrection_numberOfEtBins - 1 generate
			lookupTableEtEta(Et)(Eta) <= signed(lookupTableEtEta_in((Et * KalmanMETCorrection_numberOfEtaBins + Eta + 1) * KalmanMETCorrection_correctionBitWidth - 1 downto (Et * KalmanMETCorrection_numberOfEtaBins + Eta) * KalmanMETCorrection_correctionBitWidth));
		end generate;
	end generate;

	-- use threshold for input jets
	genInp : for i in 0 to InputWidth - 1 generate
		sel_jets(i) <= generic_in(i) when generic_in(i).Et >= MinEt else empty_tob;
	end generate;

	-- create array of sin(Phi) values for all jets
	genSin : for i in 0 to InputWidth - 1 generate
		genSin_i : entity work.Sin
			Port Map(inp => sel_jets(i).Phi,
				     oup => sin_phi(i)
			);
	end generate;

	-- create array of cos(Phi) values for all jets
	genCos : for i in 0 to InputWidth - 1 generate
		genCos_i : entity work.Cos
			Port Map(inp => sel_jets(i).Phi,
				     oup => cos_phi(i)
			);
	end generate;

	-- determine Et and Eta bin and look up correction factor
	genCorrection : for i in 0 to InputWidth - 1 generate
		-- select most significant bits of Et to determine Et bin
		etBin(i)      <= find_msb_1(sel_jets(i).Et);
		-- select most significant bits of Eta to determine Eta bin
		tmpEta(i)     <= abs (signed(sel_jets(i).Eta));
		tmpEtaHighBin(i) <= tmpEta(i)(tmpEta(0)'length - 2);
		tmpEtaShort(i) <= unsigned(tmpEta(i)(tmpEta(0)'length - 3 downto tmpEta(0)'length - ld(KalmanMETCorrection_numberOfEtaBins) - 2));
		etaBin(i)     <=  15 when (tmpEtaHighBin(i) = '1') else to_integer(tmpEtaShort(i));
		corrFactor(i) <= lookupTableEtEta(etBin(i))(etaBin(i));
	end generate;

	-- apply correction factor to Et and calculate Ex and Ey
	genCalcExEy : for i in 0 to InputWidth - 1 generate
		jetEtCorrected_long(i) <= std_logic_vector(signed('0' & sel_jets(i).Et) * corrFactor(i));
		jetEtCorrected(i)      <= jetEtCorrected_long(i)(jetEtCorrected_long(0)'length - 2 downto 0); -- discard 2nd sign bit

		tmp_Ex_long(i) <= std_logic_vector(resize(signed(jetEtCorrected(i)) * signed(cos_phi(i)), tmp_Ex_long(0)'length)); -- discard 2nd sign bit
		tmp_Ey_long(i) <= std_logic_vector(resize(signed(jetEtCorrected(i)) * signed(sin_phi(i)), tmp_Ex_long(0)'length));

		tmp_Ex(i) <= std_logic_vector(resize(signed(tmp_Ex_long(i)(tmp_Ex_long(0)'length - 1 downto 7)), tmp_Ex(0)'length)); -- discard the last 7 digits behind comma (-> keep 6 for max ??? error)
		tmp_Ey(i) <= std_logic_vector(resize(signed(tmp_Ey_long(i)(tmp_Ey_long(0)'length - 1 downto 7)), tmp_Ey(0)'length));

		saturationCorrect(i) <= (tmp_Ex(i)(tmp_Ex(i)'length - 1) xor tmp_Ex(i)(tmp_Ex(i)'length - 2)) -- set overflow bit considering the sign of tmp_sum_Ex
			or (tmp_Ey(i)(tmp_Ey(i)'length - 1) xor tmp_Ey(i)(tmp_Ey(i)'length - 2)); -- set overflow bit considering the sign of tmp_sum_Ey

	end generate;

	gen_register_stage : for i in 0 to InputWidth - 1 generate
		tmp_sum_Ex_2ndStage(i) <= tmp_sum_Ex_1stStage(i)(tmp_sum_Ex_1stStage(0)'length - 1) & tmp_sum_Ex_1stStage(i);
		tmp_sum_Ey_2ndStage(i) <= tmp_sum_Ey_1stStage(i)(tmp_sum_Ey_1stStage(0)'length - 1) & tmp_sum_Ey_1stStage(i);
	end generate;

	tmp_sum_Ex_1stStage <= tmp_Ex when rising_edge(ClockBus(0));
	tmp_sum_Ey_1stStage <= tmp_Ey when rising_edge(ClockBus(0));

	-- calculate saturation by correcting energy values
	genSumTree : for i in 0 to InputWidth - 2 generate
		saturationCorrect(InputWidth + i) <= saturationCorrect(2 * i) or saturationCorrect(2 * i + 1);
	end generate;

	saturationCorrect2 <= saturationCorrect(saturationCorrect'length - 1);

	-- calculate the sums Ex and Ey of 64 jet-Et's in two steps (use register after first stage for timing/delay issues)
	-- 1st Stage
	genSum1stStage : for i in 0 to InputWidth - 1 generate
	end generate;
	-- 2nd Stage
	genSum2ndStage : for i in 0 to InputWidth - 2 generate
		tmp_sum_Ex_2ndStage(InputWidth + i) <= std_logic_vector(signed(tmp_sum_Ex_2ndStage(2 * i)) + signed(tmp_sum_Ex_2ndStage(2 * i + 1)));
		tmp_sum_Ey_2ndStage(InputWidth + i) <= std_logic_vector(signed(tmp_sum_Ey_2ndStage(2 * i)) + signed(tmp_sum_Ey_2ndStage(2 * i + 1)));
	end generate;

	genSumSaturation : for i in 0 to InputWidth - 2 generate
		saturation_sum(i) <= (tmp_sum_Ex_2ndStage(InputWidth + i)(tmp_sum_Ex_2ndStage(0)'length - 2) xor tmp_sum_Ex_2ndStage(InputWidth + i)(tmp_sum_Ex_2ndStage(0)'length - 1)) or (tmp_sum_Ey_2ndStage(InputWidth + i)(tmp_sum_Ey_2ndStage(0)'length - 2) xor tmp_sum_Ey_2ndStage(
					InputWidth + i)(tmp_sum_Ey_2ndStage(0)'length - 1));
	end generate;
	saturation_sum(InputWidth - 1) <= '0';

	-- OR all the saturation indicators
	genOverMax2 : for i in 0 to InputWidth - 2 generate
		saturation_sum(InputWidth + i) <= saturation_sum(2 * i) OR saturation_sum(2 * i + 1);
	end generate;

	-- get the element containing the whole sum (and the sign, first)
	sum_Ex <= tmp_sum_Ex_2ndStage(2 * InputWidth - 2)(tmp_sum_Ex_2ndStage(0)'length - 1) & tmp_sum_Ex_2ndStage(2 * InputWidth - 2)(tmp_sum_Ex_2ndStage(0)'length - 2 - 1 downto (tmp_sum_Ex_2ndStage(0)'length - SumBitWidth - 2)); -- discard saturation bit
	sum_Ey <= tmp_sum_Ey_2ndStage(2 * InputWidth - 2)(tmp_sum_Ex_2ndStage(0)'length - 1) & tmp_sum_Ey_2ndStage(2 * InputWidth - 2)(tmp_sum_Ex_2ndStage(0)'length - 2 - 1 downto (tmp_sum_Ex_2ndStage(0)'length - SumBitWidth - 2)); -- discard saturation bit

	-- invert vector to get correct direction of MET correction
	sum_Ex_inverse <= std_logic_vector(-signed(sum_Ex));
	sum_Ey_inverse <= std_logic_vector(-signed(sum_Ey));

	-- get the corrected Ex and Ey values from old MET and correction
	signed_Ex_corrected <= std_logic_vector(signed(metTobReg1(0).Ex) + signed(sum_Ex_inverse)); -- TODO: overflow not treated!
	signed_Ey_corrected <= std_logic_vector(signed(metTobReg1(0).Ey) + signed(sum_Ey_inverse)); -- TODO: overflow not treated!

	sum_METSqr <= std_logic_vector(signed(signed_Ex_corrected) * signed(signed_Ex_corrected) + signed(signed_Ey_corrected) * signed(signed_Ey_corrected));

	-- do final cut and include saturation bit
	gen_result_1 : for i in 0 to NumResultBits - 1 generate
		result_1(i) <= '1' when unsigned(MinMET(i)) * unsigned(MinMET(i)) < unsigned(sum_METSqr) or (saturation_sum(2 * InputWidth - 2) = '1') or (saturationCorrect2 = '1') else '0';
	end generate;

	gen_result_2 : for i in 0 to NumResultBits - 1 generate
		Results(i) <= result_2(i);      -- or overflow_out_tmp(0);
	end generate;

	gen_overflow : for i in 0 to NumResultBits - 1 generate
		Overflow(i) <= tmpOver2(0);     --overflow_out_tmp;
	end generate;

	METPhiCorrected <= phi_corrected;
	tmpOver1(0)     <= generic_in(0).Overflow;
	tmpOver2(0)     <= tmpOver1(0);
	result_2        <= result_1;

end Behavioral;