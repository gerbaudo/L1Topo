------------------------------------------------------------------------------------------
-- Author/Modified by	: manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de
-- Description			: calculates delta eta for two TOBs
-- Number of registers	: 0
------------------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.L1TopoDataTypes.all;

entity DeltaEtaCalc is
	port(
		eta1In      : in  std_logic_vector(GenericEtaBitWidth - 1 downto 0);
		eta2In      : in  std_logic_vector(GenericEtaBitWidth - 1 downto 0);
		deltaEtaOut : out std_logic_vector(GenericEtaBitWidth - 1 downto 0)
	);
end DeltaEtaCalc;

library ieee;
use ieee.numeric_std.all;
architecture Behavioral of DeltaEtaCalc is
	signal eta1, eta2, deltaEta : std_logic_vector(GenericEtaBitWidth - 1 downto 0);

begin
	eta1 <= eta1In;
	eta2 <= eta2In;

	calcDeltaEta : process(eta1, eta2)
		variable delta : unsigned(GenericEtaBitWidth - 1 downto 0);
	begin
		if signed(eta1) > signed(eta2) then
			delta := unsigned(signed(eta1) - signed(eta2));
		else
			delta := unsigned(signed(eta2) - signed(eta1));
		end if;
		deltaEta <= std_logic_vector(delta);
	end process;

	deltaEtaOut <= deltaEta;

end Behavioral;