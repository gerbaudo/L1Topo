------------------------------------------------------------------------------------------
-- Author/Modified by	: manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de
-- Description			: applies isolation and eta cut to ClusterTOB list and converts
--						  it to GenericTOB list
-- Number of registers	: 0
------------------------------------------------------------------------------------------

--------------
-- includes --
--------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use work.L1TopoDataTypes.all;
use work.L1TopoFunctions.all;

------------------------
-- entity declaration --
------------------------

entity ClusterNoSort is
	generic(InputWidth   : integer := InputWidthEM; --number of input TOBs
		    OutputWidth  : integer := InputWidthEM; --number of output TOBs
		    NumRegisters : integer := 2 -- number of registers (0 or 2)
	);
	Port(ClockBus        : in  std_logic_vector(2 downto 0);
		 Parameters      : in  ParameterArray;
		 ClusterTobArray : in  ClusterArray(InputWidth - 1 downto 0);
		 TobArrayOut     : out TOBArray(OutputWidth - 1 downto 0)
	);
end ClusterNoSort;

-----------------------------------------
-- behavioral description of algorithm --
-----------------------------------------

architecture Behavioral of ClusterNoSort is

	-------------
	-- signals --
	-------------
	-- parameters:
	signal IsoMask                                                                                   : std_logic_vector(ClusterIsolBitWidth - 1 downto 0);
	-- selected input TOBs (isolation):
	signal generic_in, generic_in_reg1, generic_in_reg2, generic_in_reg3, generic_reg1, generic_reg2 : TOBArray(InputWidth - 1 downto 0);
	signal cluster_in                                                                                : ClusterArray(InputWidth - 1 downto 0);
	signal clock                                                                                     : std_logic;
	signal overflow_reg1, overflow_reg2                                                              : std_logic;

begin
	clock <= ClockBus(0);

	cluster_in <= ClusterTobArray;

	-- read parameters
	IsoMask <= Parameters(0)(ClusterIsolBitWidth - 1 downto 0);

	----------------------------------------------
	-- select TOBs and translate to generic TOB --
	----------------------------------------------
	-- If IsoMask is zero, then all TOBs are used (no isolation).
	-- Otherwise the isolation bits are ANDed with IsoMask.
	sel_iso : process(IsoMask, cluster_in)
	begin
		for i in 0 to InputWidth - 1 loop
			if (IsoMask = "00000" or ((cluster_in(i).Isol and IsoMask) = IsoMask)) then -- Isolation
				-- translate to generic TOB
				generic_in(i) <= to_GenericTOB(cluster_in(i));
			else
				-- replace by empty dummy TOB if not matching selection criteria
				generic_in(i) <= empty_tob;
			end if;
		end loop;
	end process;

	gen_reg : if (NumRegisters = 2) generate
		REG160 : process(ClockBus(2))
		begin
			if rising_edge(ClockBus(2)) then
				generic_in_reg1 <= generic_in;
				generic_in_reg2 <= generic_in_reg1;
				generic_in_reg3 <= generic_in_reg2;
			end if;
		end process;

		gen_reg1 : entity work.TobRegister
			generic map(InputWidth => InputWidth)
			port map(clk     => clock,
				     enable  => '1',
				     reg_in  => generic_in_reg3,
				     reg_out => generic_reg1);

		gen_reg2 : entity work.TobRegister
			generic map(InputWidth => InputWidth)
			port map(clk     => clock,
				     enable  => '1',
				     reg_in  => generic_reg1,
				     reg_out => generic_reg2);
	end generate;

	gen_no_reg : if (NumRegisters = 0) generate
		generic_reg2 <= generic_in;
	end generate;

	-- workaround to set overflow bit
	gen_set_overflow : for i in 0 to OutputWidth - 1 generate
		TobArrayOut(i).Et  <= generic_reg2(i).Et;
		TobArrayOut(i).Eta <= generic_reg2(i).Eta;
		TobArrayOut(i).Phi <= generic_reg2(i).Phi;
	end generate;

	gen_overflow_reg : if (NumRegisters = 2) generate
		gen_overflow_reg1 : entity work.StdLogicRegister
			port map(clk     => clock,
				     enable  => '1',
				     reg_in  => cluster_in(0).Overflow,
				     reg_out => overflow_reg1);

		gen_overflow_reg2 : entity work.StdLogicRegister
			port map(clk     => clock,
				     enable  => '1',
				     reg_in  => overflow_reg1,
				     reg_out => overflow_reg2);
	end generate;

	gen_no_overflow_reg : if (NumRegisters = 0) generate
		overflow_reg2 <= cluster_in(0).Overflow;
	end generate;

	TobArrayOut(0).Overflow <= overflow_reg2;

	-- only needed for simulation
	gen_set_dummy_overflow : for i in 1 to OutputWidth - 1 generate
		TobArrayOut(i).Overflow <= '0';
	end generate;

end Behavioral;
