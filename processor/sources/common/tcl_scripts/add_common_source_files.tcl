set common_dir [get_property DIRECTORY [current_project]]/../../sources/common/
set ip_dir [get_property DIRECTORY [current_project]]/../../ip/
set hist_dir [get_property DIRECTORY [current_project]]/../../sources/common/histograms

#set_param project.enableVHDL2008 1
#set_property enable_vhdl_2008 1 [current_project]

#set_param project.enableVHDL2008 1
#set_property enable_vhdl_2008 1 [current_project]
add_files -fileset sources_1 \
    $common_dir/L1TopoProcessor_690_common_top.vhd\
    $common_dir/infrastructure/infrastructure.vhd\
    $common_dir/xadc/read_out_xadc.vhd\
    $common_dir/ttcBridge/ttcBridge.vhd\
    $common_dir/controlPath/controlPath.vhd\
    $common_dir/controlPath/ipbusBridge/ipbusBridge.vhd\
    $common_dir/controlPath/ipbusBridge/ipbusBridge_deserializer.vhd\
    $common_dir/controlPath/ipbusBridge/ipbusBridge_serializer.vhd\
    $common_dir/controlPath/slaves.vhd\
    $common_dir/controlPath/slaves/ipbus_fabric.vhd\
    $common_dir/controlPath/slaves/ipbus_slave_status.vhd\
    $common_dir/controlPath/slaves/ipbus_slave_control.vhd\
    $common_dir/controlPath/slaves/ipbus_slave_reg_pulse.vhd\
    $common_dir/controlPath/slaves/ipbus_slave_xadc.vhd\
    $common_dir/controlPath/slaves/ipbus_slave_testram.vhd\
    $common_dir/controlPath/slaves/ipbus_slave_control_N.vhd\
    $common_dir/controlPath/slaves/ipbus_slave_status_N.vhd\
    $common_dir/controlPath/slaves/ipbus_slave_topoInput_playbackspy.vhd\
    $common_dir/controlPath/slaves/ipbus_slave_playbackspy32.vhd\
    $common_dir/controlPath/slaves/playbackspy_blockram.vhd\
    $common_dir/controlPath/slaves/ipbus_slave_parameters.vhd\
    $common_dir/realTimePath/realTimeDataPath.vhd\
    $common_dir/realTimePath/gth32QuadMuons.vhd\
    $common_dir/realTimePath/gth32Quad.vhd\
    $common_dir/realTimePath/muctpi2topo/L1TopoReceiver/muon_gth32_common.vhd\
    $common_dir/realTimePath/muctpi2topo/L1TopoReceiver/L1TopoReceiver.vhd\
    $common_dir/realTimePath/muctpi2topo/L1TopoReceiver/DataSorter.vhd\
    $common_dir/realTimePath/muctpi2topo/L1TopoReceiver/crc.vhd\
    $common_dir/realTimePath/gth16Quad.vhd\
    $common_dir/realTimePath/gtwizard_gth16quad_init.vhd\
    $common_dir/realTimePath/gtwizard_gth16quad_multi_gt.vhd\
    $common_dir/realTimePath/gtwizard_gth16quad_gt.vhd\
    $common_dir/realTimePath/gtwizard_gth16quad_cpll_railing.vhd\
    $common_dir/realTimePath/gtwizard_gth16quad_rx_startup_fsm.vhd\
    $common_dir/realTimePath/gtwizard_gth16quad_sync_block.vhd\
    $common_dir/realTimePath/CRC_CHECK_32b.vhd\
    $common_dir/realTimePath/muctpi2topo/muon_types.vhd\
    $common_dir/realTimePath/gth16QuadROD.vhd\
    $common_dir/rod/l1topo_to_ddr.vhd\
    $common_dir/rod/TransmitterWrapper.vhd\
    $common_dir/rod/delay.vhd\
    $common_dir/rod/ddr_enc_transmitter.vhd\
    $common_dir/rod/output_module_v2.vhd\
    $common_dir/rod/link_select_rom.coe\
    $common_dir/tcl_scripts/tcl_pre_write_bitstream.tcl\
    $common_dir/realTimePath/muctpi2topo/L1TopoReceiver/crc_package.vhd\
    $common_dir/libraries/L1TopoDataTypes.vhd\
    $common_dir/libraries/L1TopoFunctions.vhd\
    $common_dir/libraries/L1TopoLUTvalues.vhd\
    $common_dir/libraries/L1TopoGT_Configuration.vhd\
    $common_dir/libraries/crc_lut.coe\
    $common_dir/libraries/ipbus_addr_decode.vhd\
    $common_dir/../../../common/ipbus_package.vhd\
    $common_dir/../../../common/decode_8b10b_lut_base.vhd\
    $common_dir/../../../common/decode_8b10b_pkg.vhd\
    $common_dir/../../../common/encode_8b10b_lut_base.vhd\
    $common_dir/../../../common/encode_8b10b_pkg.vhd\
    $common_dir/../../../common/rod_l1topo_pkg.vhd\
    $common_dir/../../../common/l1topo_package.vhd\
    $hist_dir/HistogrammingConstants.vhd\
    $hist_dir/HistogrammingTop.vhd\
    $hist_dir/EventInfo.vhd\
    $hist_dir/HistogramBram.vhd\
    $hist_dir/HistogramBram2D.vhd\
    $hist_dir/JetSortDijet.vhd\
    $hist_dir/JetSortEtaDijet.vhd\
    $hist_dir/SelectMergePresortedJetDijet.vhd\
    $hist_dir/TobSelect_presortedDijet.vhd\
    $hist_dir/TobSortDijet.vhd\
    $hist_dir/HistogrammingCutsConstants.vhd\
    $hist_dir/HistogrammingCuts.vhd


set_property FILE_TYPE "VHDL 2008" [get_files DataSorter.vhd]
set_property FILE_TYPE "VHDL 2008" [get_files gth32QuadMuons.vhd]
set_property FILE_TYPE "VHDL 2008" [get_files L1TopoReceiver.vhd] 
set_property FILE_TYPE "VHDL 2008" [get_files gth16QuadROD.vhd]
set_property FILE_TYPE "VHDL 2008" [get_files realTimeDataPath.vhd]
set_property FILE_TYPE "VHDL 2008" [get_files gth32Quad.vhd]

add_files -fileset sources_1 \
    $ip_dir/clk_wiz_crystalclk/clk_wiz_crystalclk.xci\
    $ip_dir/clk_wiz_sysclk/clk_wiz_sysclk.xci\
    $ip_dir/xadc_temp/xadc_temp.xci\
    $ip_dir/ila_ttcBridge/ila_ttcBridge.xci\
    $ip_dir/rod_clk/rod_clk.xci\
    $ip_dir/ila_ipbusBridge1/ila_ipbusBridge1.xci\
    $ip_dir/ipbus_playbackspy_bram128/ipbus_playbackspy_bram128.xci\
    $ip_dir/muon_gth32/muon_gth32.xci\
    $ip_dir/muon_gth32/muon_gth32/example_design/muon_gth32_auto_phase_align.vhd\
    $ip_dir/muon_gth32/muon_gth32/example_design/muon_gth32_rx_manual_phase_align.vhd\
    $ip_dir/muon_gth32/muon_gth32/example_design/muon_gth32_rx_startup_fsm.vhd\
    $ip_dir/muon_gth32/muon_gth32/example_design/muon_gth32_sync_block.vhd\
    $ip_dir/muon_gth32/muon_gth32/example_design/muon_gth32_tx_startup_fsm.vhd\
    $ip_dir/muon_gth32/muon_gth32_cpll_railing.vhd\
    $ip_dir/muon_gth32/muon_gth32_gt.vhd\
    $ip_dir/muon_gth32/muon_gth32_init.vhd\
    $ip_dir/muon_gth32/muon_gth32_multi_gt.vhd\
    $ip_dir/gtwizard_gth16QuadROD/gtwizard_gth16quadrod_gt.vhd\
    $ip_dir/gtwizard_gth16QuadROD/gtwizard_gth16quadrod_rx_startup_fsm.vhd\
    $ip_dir/gtwizard_gth16QuadROD/gtwizard_gth16quadrod_tx_startup_fsm.vhd\
    $ip_dir/gtwizard_gth16QuadROD/gtwizard_gth16QuadROD.xci\
    $ip_dir/gtwizard_gth16QuadROD/gtwizard_gth16quadrod_init.vhd\
    $ip_dir/gtwizard_gth16QuadROD/gtwizard_gth16quadrod_sync_block.vhd\
    $ip_dir/gtwizard_gth16QuadROD/gtwizard_gth16quadrod.vhd\
    $ip_dir/gtwizard_gth16QuadROD/gtwizard_gth16quadrod_multi_gt.vhd\
    $ip_dir/gtwizard_gth16QuadROD/gtwizard_gth16quadrod_cpll_railing.vhd\
    $ip_dir/rod_gtx_ila/rod_gtx_ila.xci\
    $ip_dir/fast_to_gth_clk_fifo/fast_to_gth_clk_fifo.xci\
    $ip_dir/muon_test_ila/muon_test_ila.xci\
    $ip_dir/muon_mem/muon_mem.xci\
    $ip_dir/muon_coord_ila/muon_coord_ila.xci\
    $ip_dir/muon_rec_ila/muon_rec_ila.xci\
    $ip_dir/dist_mem_gen_0/dist_mem_gen_0.xci\
    $ip_dir/ila_rtdp_quad/ila_rtdp_quad.xci\
    $ip_dir/ila_rtdp_infr/ila_rtdp_infr.xci\
    $ip_dir/ila_cluster_decode/ila_cluster_decode.xci\
    $ip_dir/ila_jet_decode/ila_jet_decode.xci\
    $ip_dir/ila_rtdp_sumet_decode/ila_rtdp_sumet_decode.xci\
    $ip_dir/l1topo_to_ddr_ila/l1topo_to_ddr_ila.xci\
    $ip_dir/ring_buffer_128b_512W/ring_buffer_128b_512W.xci\
    $ip_dir/ring_buffer_128b_1024W/ring_buffer_128b_1024W.xci\
    $ip_dir/l0a_trigger_fifo/l0a_trigger_fifo.xci\
    $ip_dir/fast_to_slow_clk_fifo/fast_to_slow_clk_fifo.xci\
    $ip_dir/event_cntr_fifo/event_cntr_fifo.xci\
    $ip_dir/ila_ctp_output_1/ila_ctp_output.xci\
    $ip_dir/muon_link_ila/muon_link_ila.xci\
    $ip_dir/muon_dataflow_ila/muon_dataflow_ila.xci\
    $ip_dir/processor_rod_ddr/processor_rod_ddr.xci\
    $ip_dir/delay_rtdp/delay_rtdp.xci\
    $ip_dir/gth32QuadIP/gth32QuadIP/example_design/gth32quadip_rx_startup_fsm.vhd\
    $ip_dir/gth32QuadIP/gth32QuadIP/example_design/gth32quadip_sync_block.vhd\
    $ip_dir/gth32QuadIP/gth32QuadIP/example_design/gth32quadip_tx_startup_fsm.vhd\
    $ip_dir/gth32QuadIP/gth32QuadIP.xci\
    $ip_dir/gth32QuadIP/gth32quadip_cpll_railing.vhd\
    $ip_dir/gth32QuadIP/gth32quadip_gt.vhd\
    $ip_dir/gth32QuadIP/gth32quadip_init.vhd\
    $ip_dir/gth32QuadIP/gth32quadip_multi_gt.vhd\
    $ip_dir/hist102/hist102.xci\
    $ip_dir/hist1024tdp/hist1024tdp.xci\
    $ip_dir/hist120/hist120.xci\
    $ip_dir/hist128/hist128.xci\
    $ip_dir/hist294/hist294.xci


upgrade_ip [get_ips *]
    
    
add_files -fileset constrs_1 $common_dir/L1TopoProcessor_690_constraints.xdc
