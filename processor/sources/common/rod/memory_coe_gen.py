#!/usr/bin/python

count = 0
count_tmp = 0
comp = 1


while (count < 256):
   base = 15
   counter = 0
   tmp_number = 0

   for i in range(0,8):
      count_tmp = count
      count_tmp = count_tmp >> i
      count_tmp = count_tmp & comp
      if count_tmp > 0:
          base = base << 4     
          tmp_number = tmp_number + (i << counter*4)
          counter = counter + 1
   for j in range(counter,8):
      tmp_number = tmp_number | base
      base = base << 4   
#   print 'c:', bin(count)
   print '', "0x%x" % tmp_number
   count = count + 1

print "Good bye!"
