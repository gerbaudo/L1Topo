library IEEE;
--use IEEE.STD_LOGIC_UNSIGNED.ALL;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--library work;
--use work.all;
--use ieee.std_logic_arith.all;

library UNISIM;
use UNISIM.VCOMPONENTS.all;
use work.rod_l1_topo_types_const.all;
use work.L1TopoDataTypes.all;

entity l1topo_to_ddr is
  generic (
    MAKE_SYNCH_INPUT : integer := 0;
    SIMULATION : boolean := FALSE;
    DEBUG: boolean := FALSE;
    GTH_VIRTEX_TO_KINTEX: boolean :=TRUE
    );    
  port (
    RESET                 : in  std_logic;
    DATA_IN_CLK           : in  std_logic;
    DATA_OUT_CLK          : in  std_logic;
    DDR_CLK               : in  std_logic;
    GTH_CLK               : in  std_logic;
    
    NUMBER_OF_SLICES      : in  slice_parameters_array_u;
    --NUMBER_OF_SLICES after ineteresting event (minus global offset)
    LVL0_ACCEPTED         : in  std_logic;
    LVL0_VALID            : in  std_logic;
    LVL0_GLOBAL_OFFSET    : in  std_logic_vector(7 downto 0);
    LVL0_FULL_THR         : in  std_logic_vector(7 downto 0);
    LVL0_OFFSET           : in  slice_parameters_array_u;
    ROS_ROI_IN_DATA       : in  in_data_array;
    CRC_ERR_IN            : in  std_logic_vector(NUMBER_OF_ROS_ROI_INPUT_BUSES-1 downto 0);
    MAX_OFFSET            : in  std_logic_vector(3 downto 0);
    SEND_ON_CRC           : in  std_logic_vector(NUMBER_OF_ROS_ROI_INPUT_BUSES-1 downto 0);
    DATA_VALID_IN         : in  std_logic_vector(0 downto 0);
    OUT_DATA              : out std_logic_vector(OUTPUT_DATA_WIDTH-1 downto 0);
    DATA_VALID_OUT        : out std_logic_vector(NUMBER_OF_ROS_OUTPUT_BUSES-1 downto 0);
    L0_BUSY               : out std_logic;
    SPECIAL_CHARACTER_OUT : out std_logic_vector(NUMBER_OF_ROS_OUTPUT_BUSES-1 downto 0);
--    TEST_IN               : in  std_logic;
    ROD_DBG               : out std_logic_vector(31 downto 0)
    );
end l1topo_to_ddr;


architecture l1topo_to_ddr of l1topo_to_ddr is

  component l1topo_to_ddr_ila
    port (
      clk    : IN STD_LOGIC;
      probe0 : IN STD_LOGIC_VECTOR(255 DOWNTO 0));
  end component;
  
  component delay
    generic (
      VECTOR_WIDTH : natural;
      DELAY_INT    : integer range 0 to 31);
    port (
      CLK              : in  std_logic;
      DELAY_VECTOR_IN  : in  std_logic_vector(VECTOR_WIDTH - 1 downto 0);
      DELAY_VECTOR_OUT : out std_logic_vector(VECTOR_WIDTH - 1 downto 0));
  end component;

  --component ring_buffer_128b_512W
  --  port (
  --    WEA   : in  std_logic_vector(0 downto 0);
  --    ADDRA : in  std_logic_vector(in_memory_address_range);
  --    DINA  : in  std_logic_vector(127 downto 0);
  --    CLKA  : in  std_logic;
  --    ADDRB : in  std_logic_vector(in_memory_address_range);
  --    DOUTB : out std_logic_vector(127 downto 0);
  --    CLKB  : in  std_logic);
  --end component;

  component ring_buffer_128b_1024W --$%^
    port (
      WEA   : in  std_logic_vector(0 downto 0);
      ADDRA : in  std_logic_vector(in_memory_address_range);
      DINA  : in  std_logic_vector(127 downto 0);
      CLKA  : in  std_logic;
      ADDRB : in  std_logic_vector(in_memory_address_range);
      DOUTB : out std_logic_vector(127 downto 0);
      CLKB  : in  std_logic);
  end component;

  component l0a_trigger_fifo
  	port(
  		rst              : in  std_logic;
  		wr_clk           : in  std_logic;
  		rd_clk           : in  std_logic;
  		din              : in  std_logic_vector(31 downto 0);
  		wr_en            : in  std_logic;
  		rd_en            : in  std_logic;
  		prog_full_thresh : in  std_logic_vector(7 downto 0);
  		dout             : out std_logic_vector(31 downto 0);
  		full             : out std_logic;
  		empty            : out std_logic;
  		prog_full        : out std_logic);
  end component;

  COMPONENT fast_to_slow_clk_fifo
  	PORT(
  		rst              : IN  STD_LOGIC;
  		wr_clk           : IN  STD_LOGIC;
  		rd_clk           : IN  STD_LOGIC;
  		din              : IN  STD_LOGIC_VECTOR(129 DOWNTO 0);
  		wr_en            : IN  STD_LOGIC;
  		rd_en            : IN  STD_LOGIC;
  		prog_full_thresh : IN  STD_LOGIC_VECTOR(11 DOWNTO 0);
  		dout             : OUT STD_LOGIC_VECTOR(64 DOWNTO 0);
  		full             : OUT STD_LOGIC;
  		empty            : OUT STD_LOGIC;
  		rd_data_count    : OUT STD_LOGIC_VECTOR(12 DOWNTO 0);
  		prog_full        : OUT STD_LOGIC
  	);
  END COMPONENT;

  COMPONENT event_cntr_fifo
  	PORT(
  		rst           : IN  STD_LOGIC;
  		wr_clk        : IN  STD_LOGIC;
  		rd_clk        : IN  STD_LOGIC;
  		din           : IN  STD_LOGIC_VECTOR(0 DOWNTO 0);
  		wr_en         : IN  STD_LOGIC;
  		rd_en         : IN  STD_LOGIC;
  		dout          : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
  		full          : OUT STD_LOGIC;
  		empty         : OUT STD_LOGIC;
  		rd_data_count : OUT STD_LOGIC_VECTOR(6 DOWNTO 0)
  	);
  END COMPONENT;

  signal number_of_slices_synch, lvl0_offset_synch      : slice_parameters_array_u;
  signal addr_input_data_cntr                           : unsigned(in_memory_address_range) := (others => '0');
  signal crc_for_fifo, crc_data_out                     : std_logic_vector(127 downto 0);
  signal memory_data                                    : memory_array := (others => (others => '0'));
  signal mem_addr_cntr, send_conf_cntr, conf_wait_cntr  : unsigned(3 downto 0)          := (others => '0');
  signal mem_addr_cntr_rst, mem_addr_cntr_rst_fsm       : std_logic;
  signal l0_trigger_read, l0_trigger_read_fsm           : std_logic                     := '0';
  signal l0_fifo_empty, l0_fifo_full                    : std_logic                     := '0';
  signal l0_trigger_value_in, l0_trigger_value_out      : std_logic_vector(31 downto 0) := (others => '0');
  signal l0_full_thr_l                                  : std_logic_vector(7 downto 0);
  signal read_crc_s, read_crc_s_fsm, read_crc_s_delayed : std_logic;
  signal conf_wait_cntr_rst, conf_wait_cntr_rst_fsm     : std_logic;
  signal send_crc_cntr                                  : unsigned(3 downto 0);
  signal send_conf_cntr_rst, send_conf_cntr_rst_fsm     : std_logic;
  signal fsm_dbg, fsm_dbg_fsm                           : std_logic_vector(3 downto 0);
  signal data_to_fifo_valid, data_to_fifo_valid_fsm     : std_logic;
  signal data_to_fifo, data_to_fifo_fsm                 : std_logic_vector(127 downto 0);
  signal l1_finished, l1_finished_a                     : std_logic;
  signal l1_finished_c, l1_finished_d, l1_finished_e    : std_logic;
  signal l1_finished_b, l1_finished_fsm                 : std_logic;

  type actual_memory_address_array is array (0 to 8) of unsigned(in_memory_address_range);
  signal actual_memory_address : actual_memory_address_array;
  type SEND_DATA_AND_CONF_FSM is (IDLE, READ_CRC, SEND_CONF_WAIT, SEND_CONF, SEND_CRC, SEND_DATA,
                                  SEND_DATA_NEXT_SLICE, SEND_DATA_NEXT_SLICE_WAIT_A, SEND_DATA_NEXT_SLICE_WAIT_B, SEND_DATA_NEXT_SLICE_CHECK_A);
  signal SEND_DATA_AND_CONF_FSM_CURRENT, SEND_DATA_AND_CONF_FSM_NEXT : SEND_DATA_AND_CONF_FSM;
  type input_link_anable_array is array (0 to 6) of std_logic_vector(NUMBER_OF_ROS_ROI_INPUT_BUSES-1 downto 0);

  signal input_link_enable   : input_link_anable_array;
  signal input_scan_cntr     : unsigned(7 downto 0) := (others => '0');
  signal actual_offset       : unsigned(3 downto 0);
  signal crc_cntr            : unsigned(3 downto 0);
  signal crc_saved           : input_link_anable_array;
  
  type link_select_rom_array is array (0 to 10) of std_logic_vector(31 downto 0);
  signal link_select_rom_out : link_select_rom_array;
  type inputs_to_read_array is array (0 to NUMBER_OF_ROS_ROI_INPUT_BUSES - 1) of std_logic_vector(7 downto 0);
  signal inputs_to_read_a, link_select_rom_tmp_a : inputs_to_read_array;
  type input_link_rom_array is array (0 to 10) of std_logic_vector(7 downto 0);
  signal input_link_rom_a : input_link_rom_array;
  
  
  signal l1a_finished_cntr : unsigned(15 downto 0);
  signal data_to_ddr_wr, data_to_ddr_rd, data_to_ddr_full, data_to_ddr_empty : std_logic;
  signal data_to_ddr_fast : std_logic_vector(135 downto 0) := (others => '0');
  signal data_to_ddr_slow : std_logic_vector(64 downto 0) := (others => '0');
  signal data_to_gth: std_logic_vector(16 downto 0) := (others => '0');
  constant data_to_ddr_thr : std_logic_vector(11 downto 0) := x"dff";
  signal data_to_ddr_cntr : std_logic_vector(12 downto 0);
  signal  data_to_ddr_prog_full : std_logic;
  signal event_rd_data_count : std_logic_vector(6 downto 0);
  signal event_fifo_empty, event_fifo_rd : std_logic;
  signal data_valid_synch : std_logic;
  signal probe0                                                      : std_logic_vector(255 downto 0);
  signal reset_cntr_data_in : unsigned(7 downto 0);
  signal fifo_reset_data_in: std_logic;
  signal reset_cntr_data_out : unsigned(7 downto 0);
  signal fifo_reset_data_out: std_logic;
  attribute keep : string;
  attribute keep of l0_fifo_empty  : signal is "true"; 
  
begin

  crc_for_fifo <= std_logic_vector(to_unsigned(0,47)) & CRC_ERR_IN;
  RING_BUFF_CRC : ring_buffer_128b_1024W --512W $%^
--  RING_BUFF_CRC : ring_buffer_128b_512W 
    port map (
      WEA(0)   => '1',--DATA_VALID_IN,
      ADDRA => std_logic_vector(addr_input_data_cntr),
      DINA  => crc_for_fifo,
      CLKA  => DATA_IN_CLK,
      ADDRB => std_logic_vector(actual_memory_address(8)),
      DOUTB => crc_data_out,
      CLKB  => DATA_OUT_CLK);

  SAVE_INPUT_ROS_ROI_DATA : for i in 0 to NUMBER_OF_ROS_ROI_INPUT_BUSES-1 generate
    SELECT_ACTIVE_LINKS : if active_channels(i) = '1' or i=77 or i=78 generate
    RING_BUFF_128B_512W_ROS_INST : ring_buffer_128b_1024W--512W $%^
--    RING_BUFF_128B_512W_ROS_INST : ring_buffer_128b_512W 
      port map (
        WEA(0)   => '1',--DATA_VALID_IN,
        ADDRA => std_logic_vector(addr_input_data_cntr),
        DINA  => ROS_ROI_IN_DATA(i),
        CLKA  => DATA_IN_CLK,
        ADDRB => std_logic_vector(actual_memory_address(i/9)),
        DOUTB => memory_data(i),
        CLKB  => DATA_OUT_CLK);
    end generate SELECT_ACTIVE_LINKS;
    CALCULATE_ACTUAL_ADDRESS : process (DATA_OUT_CLK, RESET)
    begin
      if rising_edge(DATA_OUT_CLK) then
        actual_memory_address(i/9) <= unsigned(l0_trigger_value_out(addr_input_data_cntr'range)) + unsigned(MAX_OFFSET) - mem_addr_cntr; 
      end if;
    end process CALCULATE_ACTUAL_ADDRESS;
  end generate SAVE_INPUT_ROS_ROI_DATA;
  
  INCREASE_INPUT_BUFFER_ADDRESS : process (DATA_IN_CLK, RESET) --continous writing into ring buffer
  begin
    if rising_edge(DATA_IN_CLK) then
      if RESET = '1' then
        addr_input_data_cntr <= (others => '0');
        l0_trigger_value_in(addr_input_data_cntr'range) <= (others => '0');
      else
        addr_input_data_cntr <= addr_input_data_cntr + 1;
        l0_trigger_value_in(addr_input_data_cntr'range) <= std_logic_vector(unsigned(addr_input_data_cntr) - unsigned(LVL0_GLOBAL_OFFSET));
      end if;
    end if;
  end process INCREASE_INPUT_BUFFER_ADDRESS;

  -----------------------------------------------------------------------------
  -- sending data out - saving incomming l1A
  -----------------------------------------------------------------------------

  l0_trigger_value_in(31) <= LVL0_ACCEPTED;
  SET_L1A_THRESHOLD : process (DATA_OUT_CLK)
  begin
    if rising_edge(DATA_OUT_CLK) then
      if LVL0_FULL_THR = x"00" then
        l0_full_thr_l <= x"0A";
      else
        l0_full_thr_l <= LVL0_FULL_THR;
      end if;
    end if;
  end process SET_L1A_THRESHOLD;

  RESET_CNTR_DATA_IN_PROC: process(DATA_IN_CLK)
  begin
    if rising_edge(DATA_IN_CLK) then
      if RESET = '1' then 
        reset_cntr_data_in <= (others => '0');
      elsif reset_cntr_data_in < x"ff" then
        reset_cntr_data_in <= reset_cntr_data_in + 1;
      else
        reset_cntr_data_in <= reset_cntr_data_in;
      end if;
    end if;
  end process RESET_CNTR_DATA_IN_PROC;

  SET_RESET_DATA_IN: process(DATA_IN_CLK)
  begin
    if rising_edge(DATA_IN_CLK) then
      if reset_cntr_data_in < 40 and reset_cntr_data_in > 20 then
        fifo_reset_data_in <= '1';
      else
        fifo_reset_data_in <= '0';
      end if;
    end if;
  end process SET_RESET_DATA_IN;      
          
  L0A_TRIGGER_FIFO_INST : l0a_trigger_fifo  --to record incoming triggers -
                                            --data flow can be slower than triggers
    port map (
      rst              => fifo_reset_data_in,--RESET,
      wr_clk           => DATA_IN_CLK,
      rd_clk           => DATA_OUT_CLK,
      din              => l0_trigger_value_in,
      wr_en            => LVL0_ACCEPTED,
      rd_en            => l0_trigger_read,
      prog_full_thresh => l0_full_thr_l,
      dout             => l0_trigger_value_out,
      full             => open,
      empty            => l0_fifo_empty,
      prog_full        => l0_fifo_full);
  
  L0_BUSY <= l0_fifo_full or data_to_ddr_prog_full;
  ROD_DBG(14) <= l0_fifo_full;
  ROD_DBG(15) <= l0_fifo_empty;

  -----------------------------------------------------------------------------
  -- sending data out - reading from individual fifos after l1a and saving to
  -- one fifo (with 200MHz), 
  -----------------------------------------------------------------------------
  SEND_DATA_AND_CONF_FSM_CLK : process (DATA_OUT_CLK, RESET)
  begin
    if rising_edge(DATA_OUT_CLK) then
      if RESET = '1' then
        SEND_DATA_AND_CONF_FSM_CURRENT <= IDLE;
        l0_trigger_read                <= '0';
        read_crc_s                     <= '0';
        mem_addr_cntr_rst              <= '1';
        conf_wait_cntr_rst             <= '1';
        send_conf_cntr_rst             <= '1';
        fsm_dbg                        <= (others => '0');
        data_to_fifo_valid             <= '0';
        data_to_fifo                   <= (others => '0');
        l1_finished                    <= '0';
        l1_finished_a                  <= l1_finished;
        l1_finished_b                  <= l1_finished_a;
        l1_finished_c                  <= l1_finished_b;
        l1_finished_d                  <= l1_finished_c;
        l1_finished_e                  <= l1_finished_d;
      else
        SEND_DATA_AND_CONF_FSM_CURRENT <= SEND_DATA_AND_CONF_FSM_NEXT;
        l0_trigger_read                <= l0_trigger_read_fsm;
        read_crc_s                     <= read_crc_s_fsm;
        mem_addr_cntr_rst              <= mem_addr_cntr_rst_fsm;
        conf_wait_cntr_rst             <= conf_wait_cntr_rst_fsm;
        send_conf_cntr_rst             <= send_conf_cntr_rst_fsm;
        fsm_dbg                        <= fsm_dbg_fsm;
        data_to_fifo_valid             <= data_to_fifo_valid_fsm;
        data_to_fifo                   <= data_to_fifo_fsm;
        l1_finished                    <= l1_finished_fsm;
        l1_finished_a                  <= l1_finished;
        l1_finished_b                  <= l1_finished_a;
        l1_finished_c                  <= l1_finished_b;
        l1_finished_d                  <= l1_finished_c;
        l1_finished_e                  <= l1_finished_d;
      end if;
    end if;
  end process SEND_DATA_AND_CONF_FSM_CLK;
 
  SEND_DATA_AND_CONF_FSM_PROC : process (DATA_OUT_CLK)
  begin
    l0_trigger_read_fsm   <= '0';
    read_crc_s_fsm         <= '0';
    mem_addr_cntr_rst_fsm  <= '0';
    conf_wait_cntr_rst_fsm  <= '1';
    send_conf_cntr_rst_fsm  <= '1';
    fsm_dbg_fsm           <= x"f";
    data_to_fifo_valid_fsm  <= '0';
    data_to_fifo_fsm      <= (others => '1');
    l1_finished_fsm      <= '0';
    case (SEND_DATA_AND_CONF_FSM_CURRENT) is
      when IDLE => --1S
        mem_addr_cntr_rst_fsm   <= '1';
        fsm_dbg_fsm           <= x"1";
        if l0_fifo_empty = '0' then
          l0_trigger_read_fsm <= '1';
          SEND_DATA_AND_CONF_FSM_NEXT <= READ_CRC;
        else
          SEND_DATA_AND_CONF_FSM_NEXT <= IDLE;
        end if;
      when READ_CRC => --6S
        fsm_dbg_fsm             <= x"2";
        read_crc_s_fsm          <= '1';
        if mem_addr_cntr < 5 then
          SEND_DATA_AND_CONF_FSM_NEXT <= READ_CRC;
        else
          SEND_DATA_AND_CONF_FSM_NEXT <= SEND_CONF_WAIT;
        end if;
      when SEND_CONF_WAIT => --12S
        fsm_dbg_fsm             <= x"3";
        mem_addr_cntr_rst_fsm   <= '1';
        conf_wait_cntr_rst_fsm  <= '0';
        if conf_wait_cntr = 6 then
          send_conf_cntr_rst_fsm  <= '0';
          SEND_DATA_AND_CONF_FSM_NEXT <= SEND_CONF;
        else
          SEND_DATA_AND_CONF_FSM_NEXT <= SEND_CONF_WAIT;
        end if;
      when SEND_CONF => --18S
        fsm_dbg_fsm             <= x"4";
        mem_addr_cntr_rst_fsm   <= '1';
        send_conf_cntr_rst_fsm  <= '0';
        if unsigned(input_link_enable(to_integer(send_conf_cntr))) /= 0 then
          data_to_fifo_valid_fsm  <= '1';
        else
          data_to_fifo_valid_fsm  <= '0';
        end if;
        data_to_fifo_fsm(127 downto 124) <= x"8";
        data_to_fifo_fsm(123 downto 120) <= std_logic_vector(send_conf_cntr);
        data_to_fifo_fsm(NUMBER_OF_ROS_ROI_INPUT_BUSES-1 downto 0) <= input_link_enable(to_integer(send_conf_cntr));
        --for i in 0 to 15 loop
        --  data_to_fifo_fsm((i+1)*8-1 downto i*8) <=  std_logic_vector(to_unsigned(i,8));
        --end loop;
        if send_conf_cntr < 6 then
          SEND_DATA_AND_CONF_FSM_NEXT <= SEND_CONF;
        else
          
          SEND_DATA_AND_CONF_FSM_NEXT <= SEND_CRC;
        end if;
      when SEND_CRC => --24S
        fsm_dbg_fsm             <= x"5";
        if unsigned(input_link_enable(to_integer(send_crc_cntr))) /= 0 then
          data_to_fifo_valid_fsm  <= '1';
        else
          data_to_fifo_valid_fsm  <= '0';
        end if;
        data_to_fifo_fsm(127 downto 124) <= x"7";
        data_to_fifo_fsm(123 downto 120) <= std_logic_vector(send_crc_cntr);
        data_to_fifo_fsm(NUMBER_OF_ROS_ROI_INPUT_BUSES-1 downto 0) <= crc_saved(to_integer(send_crc_cntr));
        --for i in 0 to 15 loop
        --  data_to_fifo_fsm((i+1)*8-1 downto i*8) <=  std_logic_vector(to_unsigned(i,8));
        --end loop;
        if send_crc_cntr < 6 then
          SEND_DATA_AND_CONF_FSM_NEXT <= SEND_CRC;
        else
          mem_addr_cntr_rst_fsm   <= '1';
          SEND_DATA_AND_CONF_FSM_NEXT <= SEND_DATA;
        end if;
      when SEND_DATA => --81S*number of slices (NoS) - all before is only once
        fsm_dbg_fsm           <= x"6";
        if input_link_enable(to_integer(actual_offset))(to_integer(input_scan_cntr)) = '1' then --inputs_to_read_a(to_integer(input_scan_cntr))(7) = '0' then
          data_to_fifo_valid_fsm <= '1';
          data_to_fifo_fsm <= memory_data(to_integer(input_scan_cntr));--(to_integer(unsigned(inputs_to_read_a(to_integer(input_scan_cntr)))));
        else
          data_to_fifo_valid_fsm <= '0';
        end if;
        if input_scan_cntr = NUMBER_OF_ROS_ROI_INPUT_BUSES-1 then
          SEND_DATA_AND_CONF_FSM_NEXT <= SEND_DATA_NEXT_SLICE;    
        else
          SEND_DATA_AND_CONF_FSM_NEXT <= SEND_DATA;
        end if;

      when SEND_DATA_NEXT_SLICE =>--82*Nos
        fsm_dbg_fsm           <= x"7";
        if actual_offset < 5 then
          SEND_DATA_AND_CONF_FSM_NEXT <= SEND_DATA_NEXT_SLICE_WAIT_A;
        else
          l1_finished_fsm <= '1';
          SEND_DATA_AND_CONF_FSM_NEXT <= IDLE;
        end if;
      when SEND_DATA_NEXT_SLICE_WAIT_A => --83*NoS
        fsm_dbg_fsm           <= x"8";
        SEND_DATA_AND_CONF_FSM_NEXT <=  SEND_DATA_NEXT_SLICE_WAIT_B;
      when SEND_DATA_NEXT_SLICE_WAIT_B =>
        fsm_dbg_fsm           <= x"A";
        SEND_DATA_AND_CONF_FSM_NEXT <=  SEND_DATA_NEXT_SLICE_CHECK_A;  
      when SEND_DATA_NEXT_SLICE_CHECK_A => --84*NoS
        fsm_dbg_fsm           <= x"9";
        if unsigned(input_link_enable(to_integer(actual_offset))) /= 0 then
          SEND_DATA_AND_CONF_FSM_NEXT <=  SEND_DATA;
        else
          SEND_DATA_AND_CONF_FSM_NEXT <=  SEND_DATA_NEXT_SLICE;
        end if;
      when others =>
        fsm_dbg_fsm           <= x"e";
        SEND_DATA_AND_CONF_FSM_NEXT <= IDLE;
    end case;
  end process SEND_DATA_AND_CONF_FSM_PROC;
  
  MEM_ADDR_CNTR_PROC : process (DATA_OUT_CLK)
  begin
    if rising_edge(DATA_OUT_CLK) then
      if mem_addr_cntr_rst = '1' then
        mem_addr_cntr <= (others => '0');
      elsif SEND_DATA_AND_CONF_FSM_CURRENT = READ_CRC or SEND_DATA_AND_CONF_FSM_CURRENT = SEND_DATA_NEXT_SLICE then
        mem_addr_cntr <= mem_addr_cntr + 1;
      else
        mem_addr_cntr <= mem_addr_cntr;
      end if;
    end if;
  end process MEM_ADDR_CNTR_PROC;

  CONF_WAIT_CNTR_PROC : process (DATA_OUT_CLK)
  begin
    if rising_edge(DATA_OUT_CLK) then
      if conf_wait_cntr_rst = '1' then
       conf_wait_cntr <= (others => '0');
      else
       conf_wait_cntr <=  conf_wait_cntr + 1;
      end if;
    end if;
  end process CONF_WAIT_CNTR_PROC;

  SEND_CONF_CNTR_PROC : process (DATA_OUT_CLK)
  begin
    if rising_edge(DATA_OUT_CLK) then
      if send_conf_cntr_rst = '1' then
       send_conf_cntr <= (others => '0');
      else
       send_conf_cntr <=  send_conf_cntr + 1;
      end if;
    end if;
  end process SEND_CONF_CNTR_PROC;

  INPUT_SCAN_CNTR_PROC : process (DATA_OUT_CLK)
  begin
    if rising_edge(DATA_OUT_CLK) then
      if SEND_DATA_AND_CONF_FSM_CURRENT = SEND_DATA then
       input_scan_cntr <= input_scan_cntr + 1;
      else
       input_scan_cntr <= (others => '0');
      end if;
    end if;
  end process INPUT_SCAN_CNTR_PROC;

  ACTUAL_OFFSET_PROC : process (DATA_OUT_CLK)
  begin
    if rising_edge(DATA_OUT_CLK) then
      if SEND_DATA_AND_CONF_FSM_CURRENT = IDLE then
        actual_offset <= (others => '0');
      elsif SEND_DATA_AND_CONF_FSM_CURRENT = SEND_DATA_NEXT_SLICE then
        actual_offset <= actual_offset + 1;
      else
        actual_offset <= actual_offset;
      end if;
    end if;
  end process ACTUAL_OFFSET_PROC;
  
  -- save crc for given l1A for further processing
  DELAY_CRC_READ: delay
    generic map (
      VECTOR_WIDTH => 1,
      DELAY_INT    => 6)
    port map (
      CLK              => DATA_OUT_CLK,
      DELAY_VECTOR_IN(0)  => read_crc_s,
      DELAY_VECTOR_OUT(0) => read_crc_s_delayed);

  CRC_CNTR_PROC : process (DATA_OUT_CLK)
  begin
    if rising_edge(DATA_OUT_CLK) then
      if read_crc_s_delayed = '1' then
        crc_cntr <= crc_cntr + 1;
      else
        crc_cntr <= (others => '0');
      end if;
    end if;
  end process CRC_CNTR_PROC;

  SAVE_CRC : process (DATA_OUT_CLK)
  begin
    if rising_edge(DATA_OUT_CLK) then
      if read_crc_s_delayed = '1' then
        crc_saved(to_integer(crc_cntr)) <= crc_data_out(NUMBER_OF_ROS_ROI_INPUT_BUSES-1 downto 0);
      end if;
    end if;
  end process SAVE_CRC;

  SEND_CRC_CNTR_PROC : process (DATA_OUT_CLK)
  begin
    if rising_edge(DATA_OUT_CLK) then
      if SEND_DATA_AND_CONF_FSM_CURRENT = SEND_CRC and send_crc_cntr < 6 then
        send_crc_cntr <= send_crc_cntr + 1;
      else
        send_crc_cntr <= (others => '0');
      end if;
    end if;
  end process SEND_CRC_CNTR_PROC;

  
  --set which channels should be read out - based on offset, number of slices
  --and crc
  SET_INDIVIDUAL_BITS_OF_DV_FOR_GIVEN_OFFSET: for j in 0 to 5 generate
    SET_INDIVIDUAL_BITS_OF_DV: for i in 0 to NUMBER_OF_ROS_ROI_INPUT_BUSES - 1 generate
      SET_INPUT_ENABLE: process (DATA_OUT_CLK) is
      begin 
        if rising_edge(DATA_OUT_CLK) then
          if (crc_data_out(i) = '1' or SEND_ON_CRC(i) = '0') and NUMBER_OF_SLICES(i) /= 0 and 
             signed(LVL0_OFFSET(i)) - signed(NUMBER_OF_SLICES(i)) + 1 <= signed(MAX_OFFSET) - j and
             signed(LVL0_OFFSET(i)) >= signed(MAX_OFFSET) - j 
          then
            input_link_enable(j)(i) <= '1';
          else
            input_link_enable(j)(i) <= '0';
          end if;
        end if;
      end process SET_INPUT_ENABLE;
    end generate SET_INDIVIDUAL_BITS_OF_DV;
  end generate SET_INDIVIDUAL_BITS_OF_DV_FOR_GIVEN_OFFSET;
  
 --calc conseq fifo numbers to read-out (dist. ROM based) - now not used !!!
  --SELECT_CONSEQ_INPUTS_WITH_ROM: for i in 0 to 10 generate
  --  CMX_INPUTS: if i < 10 generate
  --    input_link_rom_a(i) <= input_link_enable(to_integer(actual_offset))((i+1)*8-1 downto i*8);
  --    LINK_SELECT_ROM_INST: entity work.link_select_rom
  --      port map (
  --        a   => input_link_rom_a(i),
  --        spo => link_select_rom_out(i));
  --  end generate CMX_INPUTS;
  --  TOPO_DATA: if i = 10 generate
  --    input_link_rom_a(10)(0) <= input_link_enable(to_integer(actual_offset))(80);
  --    input_link_rom_a(10)(7 downto 1) <= (others => '0');
  --    LINK_SELECT_ROM_INST: entity work.link_select_rom
  --      port map (
  --        a   => input_link_rom_a(i),
  --        spo => link_select_rom_out(i));
      
  --  end generate TOPO_DATA;
  --end generate SELECT_CONSEQ_INPUTS_WITH_ROM;
  
  CALC_INPUTS_NUMBER_TO_READ: for i in 0 to NUMBER_OF_ROS_ROI_INPUT_BUSES-1 generate
    --ROM output is  for example : fffff6421, max number is 8, f is resereved
    --for not active channel
    link_select_rom_tmp_a(i) <= x"0" & link_select_rom_out(i/10)(((i mod 8)+1)*4-1 downto (i mod 8)*4);
    SET_INPUTS_TO_READ: process (DATA_OUT_CLK) is
    begin  
      if rising_edge(DATA_OUT_CLK) then
        if link_select_rom_out(i/10)(((i mod 8)+1)*4-1 downto (i mod 8)*4) /= x"f" then
          inputs_to_read_a(i) <= std_logic_vector(unsigned(link_select_rom_tmp_a(i)) +  i/10*8);
        else
          inputs_to_read_a(i) <= (others => '1');
        end if;
      end if;
    end process SET_INPUTS_TO_READ;
  end generate CALC_INPUTS_NUMBER_TO_READ;
  
  L1A_FINISHED_PROC : process (DATA_OUT_CLK, RESET)
  begin
    if rising_edge(DATA_OUT_CLK) then
      if  RESET = '1' then
        l1a_finished_cntr <= (others => '0');
      elsif l1_finished = '1' then
        l1a_finished_cntr <= l1a_finished_cntr  + 1;
      else
        l1a_finished_cntr <= l1a_finished_cntr;
      end if;
    end if;
  end process L1A_FINISHED_PROC;
  ROD_DBG(31 downto 28) <= std_logic_vector(l1a_finished_cntr(3 downto 0));
  ROD_DBG(27 downto 24) <= fsm_dbg;
  ROD_DBG(23)           <= l0_fifo_empty;
  ROD_DBG(22)           <= data_to_ddr_prog_full;
  ROD_DBG(21 downto 16) <= event_rd_data_count(5 downto 0);
  ROD_DBG(13)           <= data_to_ddr_empty;
  ROD_DBG(12 downto 0)  <= data_to_ddr_cntr;
  -----------------------------------------------------------------------------
  -- GTH_VIRTEX_TO_KINTEX_EN
  -----------------------------------------------------------------------------
  RESET_CNTR_DATA_OUT_PROC: process(DATA_OUT_CLK)
  begin
    if rising_edge(DATA_OUT_CLK) then
      if RESET = '1' then 
        reset_cntr_data_out <= (others => '0');
      elsif reset_cntr_data_out < 255 then
        reset_cntr_data_out <= reset_cntr_data_out + 1;
      else
        reset_cntr_data_out <= reset_cntr_data_out;
      end if;
    end if;
  end process RESET_CNTR_DATA_OUT_PROC;

  SET_RESET_DATA_OUT: process(DATA_OUT_CLK)
  begin
    if rising_edge(DATA_OUT_CLK) then
      if reset_cntr_data_out < 60 and reset_cntr_data_out > 40 then
        fifo_reset_data_out <= '1';
      else
        fifo_reset_data_out <= '0';
      end if;
    end if;
  end process SET_RESET_DATA_OUT;     

  
  GTH_VIRTEX_TO_KINTEX_EN : if GTH_VIRTEX_TO_KINTEX = TRUE generate
    SYNCH_WR_TO_FIFO: process(DATA_OUT_CLK)
    begin
      if rising_edge(DATA_OUT_CLK) then
        data_to_ddr_wr <= data_to_fifo_valid or l1_finished or l1_finished_a or l1_finished_b
                          or l1_finished_c or l1_finished_d or l1_finished_e;--was only till b
      else
        data_to_ddr_wr <= data_to_ddr_wr;
      end if;
    end process SYNCH_WR_TO_FIFO;
    
    CONNECT_FAST_TO_GTH_DATA: for i in 0 to 7 generate          
      SYNCH_GTH_DATA_TO_FIFO: process(DATA_OUT_CLK)
      begin
        if rising_edge(DATA_OUT_CLK) then
          data_to_ddr_fast((i+1)*17-2 downto i*17) <= data_to_fifo((i+1)*16-1 downto i*16);
          data_to_ddr_fast((i+1)*17-1) <= data_to_fifo_valid;
        end if;
      end process SYNCH_GTH_DATA_TO_FIFO;
    end generate CONNECT_FAST_TO_GTH_DATA;

    SET_READ_WHEN_EVENT_READY_GTH: process(GTH_CLK)
    begin
      if rising_edge(GTH_CLK) then
        if event_fifo_empty = '0' then
          data_to_ddr_rd <= not data_to_ddr_empty;
        else
          data_to_ddr_rd <= '0';
        end if;
      end if;
    end process SET_READ_WHEN_EVENT_READY_GTH;

    FAST_TO_GTH_CLK_FIFO_INST: entity work.fast_to_gth_clk_fifo
      port map (
        rst              => fifo_reset_data_out,--RESET,
        wr_clk           => DATA_OUT_CLK,
        rd_clk           => GTH_CLK,
        din              => data_to_ddr_fast,
        wr_en            => data_to_ddr_wr,
        rd_en            => data_to_ddr_rd,
        dout             => data_to_gth,
        rd_data_count    => data_to_ddr_cntr,
        prog_full_thresh => data_to_ddr_thr,
        prog_full        => data_to_ddr_prog_full,
        full             => data_to_ddr_full,
        empty            => data_to_ddr_empty);

    SPECIAL_CHARACTER_OUT <= (others => '0');
    SYNCH_DATA_OUT:process (GTH_CLK)
    begin
      if rising_edge(GTH_CLK) then
        OUT_DATA(15 downto 0) <= data_to_gth(15 downto 0) ;
        DATA_VALID_OUT <= (others => data_to_gth(16));
      end if;
    end process SYNCH_DATA_OUT;
    
    READ_EVENT_CNTR_FIFO:process(GTH_CLK)
    begin
      if rising_edge(GTH_CLK) then
        if data_valid_synch = '1' and data_to_gth(16) = '0' then
          data_valid_synch <= data_to_gth(16);
          event_fifo_rd <= '1';
        else
          data_valid_synch <= data_to_gth(16);
          event_fifo_rd <= '0';
        end if;
      end if;
    end process READ_EVENT_CNTR_FIFO;
    
   end generate GTH_VIRTEX_TO_KINTEX_EN;

    EVENT_CNTR_FIFO_INST: event_cntr_fifo
    port map (
      rst           => fifo_reset_data_out,--RESET,
      wr_clk        => DATA_OUT_CLK,
      rd_clk        => GTH_CLK,
      din           => data_to_fifo(0 downto 0),
      wr_en         => l1_finished,
      rd_en         => event_fifo_rd,
      dout          => open,
      full          => open,
      empty         => event_fifo_empty,
      rd_data_count => event_rd_data_count);

--    DEBUG_ROD_INST: entity work.debug_rod
--      port map (
--        clk    => GTH_CLK,
--        probe0(0) => fifo_reset_data_out,
--       probe1(0) => event_fifo_empty,
--        probe2(0) => data_to_ddr_empty,
--        probe3(0) => data_to_gth(16),
--        probe4(0) => fifo_reset_data_in,
--        probe5(0) => LVL0_ACCEPTED
--        );
      
end l1topo_to_ddr;
