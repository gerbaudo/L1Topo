----------------------------------------------------------------------------------
-- Company: Johannes Gutenberg-Universitaet Mainz, Jagiellonian University
-- Engineer: Christian Kahra, Maek Palka
-- 
-- Create Date: 14.07.2014 16:06:32
-- Design Name: L1TopoProcessor
-- Module Name: top_L1TopoProcessor - Behavioral
-- Project Name: Atlas Level-1 Topological Processor
-- Target Devices: 
-- Tool Versions: Vivado 14.2
-- Description: top module
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.l1topo_package.all;
use work.L1TopoDataTypes.all;
use work.L1TopoFunctions.all;
use work.rod_l1_topo_types_const.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity top_L1TopoProcessor is
    generic(
        LINES_NUMBER      : integer := NUMBER_OF_ROS_OUTPUT_BUSES;
        SIMULATION        : boolean := false;
        PROCESSOR_NUMBER  : integer := 0;
        GTH_VIRTEX_TO_KINTEX : boolean := TRUE;
        DEBUG: integer := 0
  );
    Port ( 
        GCK1_P, GCK1_N: in std_logic;
        GCK2_P, GCK2_N: in std_logic;
        
        MGT2_CLK_P, MGT2_CLK_N: in std_logic_vector(11 downto 1);
        MGT4_CLK_P, MGT4_CLK_N: in std_logic_vector(11 downto 1);
        
        RxP : in  std_logic_vector(numberOfChannels-1 downto 0);
        RxN : in  std_logic_vector(numberOfChannels-1 downto 0);
        TxP : out std_logic_vector(3 downto 0);
        TxN : out std_logic_vector(3 downto 0);
        
--        CTRLBUS_IN_P,  CTRLBUS_IN_N:  in  std_logic_vector( 6 downto 0);
--        CTRLBUS_OUT_P, CTRLBUS_OUT_N: out std_logic_vector(20 downto 9);
        
        ipbBridgeRequest_p, ipbBridgeRequest_n:      in  std_logic_vector(4 downto 0);
        ipbBridgeResponse_p, ipbBridgeResponse_n:    out std_logic_vector(2 downto 0);
        ttcBridge_p, ttcBridge_n:                    in  std_logic;
        rodBridge_p, rodBridge_n:                    out std_logic_vector(7 downto 0);
        l1_accepted_in_p, l1_accepted_in_n:          in  std_logic;
        l0_busy_p, l0_busy_n:                        out std_logic;
        sysclk40_p, sysclk40_n:                      out std_logic;
        crystalclk40_p, crystalclk40_n:              out std_logic;
        
        EXT_V7_P, EXT_V7_N: out std_logic_vector(16+ctpOutputLinesOffset(PROCESSOR_NUMBER)-1 downto ctpOutputLinesOffset(PROCESSOR_NUMBER))
        
--        MMCX_U56: out std_logic
--        MMCX_U57: out std_logic
    
        
    );
end top_L1TopoProcessor;

architecture Behavioral of top_L1TopoProcessor is
	
	constant FirmwareVersion:  std_logic_vector(31 downto 0) := X"00050200";
	constant FirmwareDate:     std_logic_vector(31 downto 0) := X"26052017";
	constant FirmwareRevision: std_logic_vector(31 downto 0) := X"00003124";
	
	signal ProcessorID: std_logic_vector(31 downto 0);
	signal usr_access: std_logic_vector(31 downto 0);

    signal gck1, gck1_bufg:     std_logic;
    signal gck2, gck2_bufg:     std_logic;
    signal sysclk40:            std_logic;
    signal sysclk80:            std_logic;
    signal sysclk160:           std_logic;
    signal sysclk320:           std_logic;
    signal sysclk400:           std_logic;
    signal rodclk400_io:        std_logic;
    signal rodclk400_r:         std_logic;
    signal rodclk80:            std_logic;
    signal sysclk_pll_locked:   std_logic;
    signal sysclk40_ddr:        std_logic;
    signal gth_rod_clk:         std_logic;
        
    signal crystalclk40:            std_logic;
    signal crystalclk62_5:          std_logic;
    signal crystalclk200:           std_logic;
    signal crystalclk_pll_locked:   std_logic;
    signal crystalclk40_ddr:        std_logic;

    signal pll_reset:               std_logic;    
        
    signal idelayctrlReset:         std_logic;
    signal idelayctrlReady:         std_logic;

    signal clk_control:             std_logic;
    signal clk_status:              std_logic_vector(3 downto 0);

    signal xadc_control: std_logic_vector(25  downto 0);
    signal xadc_status:  std_logic_vector(22  downto 0);


--    signal MgtRefClk0P, MgtRefClk0N: std_logic_vector(QuadHigh downto QuadLow);
	signal MgtRefClk1P, MgtRefClk1N: std_logic_vector(numberOfQuads-1 downto 0);
    signal MGTRefClk: std_logic_vector(numberOfQuads-1 downto 0);

--    signal RxP, RxN: std_logic_vector(MGTHigh downto MGTLow);
--    signal TxP, TxN: std_logic_vector(TXMGTHigh downto TXMGTLow);


    signal ipbusBridgeBusIn:    std_logic_vector(4 downto 0);
--   ipbusBridgeBusIn_p,  ipbusBridgeBusIn_n : std_logic_vector(4 downto 0);
    signal ipbusBridgeBusOut:    std_logic_vector(2 downto 0);
--  signal ipbusBridgeBusOut_p, ipbusBridgeBusOut_n: std_logic_vector(2 downto 0);


    signal ttcBridge: std_logic;
--    signal ttcBridge_p, ttcBridge_n: std_logic;
    signal ttcBridge_control: std_logic_vector( 5 downto 0);
    signal ttcBridge_status : std_logic_vector(14 downto 0);
    signal ttcBroadcast, ttcL1Accept, ttcBunchCounterReset: std_logic;
     
     --ROD INTERFACE
--    signal ROD_DATA_LINES_P : std_logic_vector(LINES_NUMBER-1 downto 0); 
--    signal ROD_DATA_LINES_N : std_logic_vector(LINES_NUMBER-1 downto 0);
    
--    signal BUSY_TO_KINTEX_P : std_logic;
--    signal BUSY_TO_KINTEX_N : std_logic;
    
--    signal L1_ACCEPTED_IN_P : std_logic;
--    signal L1_ACCEPTED_IN_N : std_logic;
    
    signal rod_reset: std_logic := '0';

    signal l1_accepted_in: std_logic := '0';
    signal l0_busy : std_logic := '0';
       
    signal number_of_slices : slice_parameters_array_u;
    signal lvl0_offset      : slice_parameters_array_u;
 
    signal TEMP_data_for_ros_roi_bus, data_for_ros_roi_bus, data_for_ros_roi_bus_synch : in_data_array := (others => (others => '1'));  --dummy cntr for tests
    signal data_for_ros_roi_bus_algo : std_logic_vector(127 downto 0);
    signal out_data              : std_logic_vector(OUTPUT_DATA_WIDTH-1 downto 0)          := (others => '0');
    signal data_valid_out        : std_logic_vector(NUMBER_OF_ROS_OUTPUT_BUSES-1 downto 0) := (others => '0');
    signal special_character_out : std_logic_vector(NUMBER_OF_ROS_OUTPUT_BUSES-1 downto 0) := (others => '0');
    signal global_reset_cnt      : unsigned(15 downto 0)                                   := (others => '0');
    signal send_on_crc, crc_err  : std_logic_vector(NUMBER_OF_ROS_ROI_INPUT_BUSES-1 downto 0) := (others => '0');
    
    signal rod_control_register: rod_control_registers_array := (others => (others => '0'));
    signal rod_status_register: rod_status_registers_array;
    signal l1_accepted_in_pulse, l1_accepted_in_synch_a, l1_accepted_in_synch_b : std_logic := '0';
    attribute IOB : string;
    attribute IOB of l1_accepted_in_synch_a : signal is "TRUE";
    
    signal rod_dataToAlgorithm128, muxedData128BitToAlgorithms_reg_shifted: arraySLV128(numberOfChannels-1 downto 0);
        

    signal ChannelControl  : arraySLV32(numberOfChannels-1 downto 0);
	signal ChannelStatus   : arraySLV32(numberOfChannels-1 downto 0);
    signal GeneralControl:	std_logic_vector(31 downto 0);
    
    signal topoInput_enablePlayback:    std_logic;
    signal topoInput_playbackData128:   arraySLV128(numberOfChannels-1 downto 0);
    signal topoInput_spyData128:        arraySLV128(numberOfChannels-1 downto 0);
    
    
    signal SortParameters: ParameterSpace(NumberOfSortAlgorithms - 1 downto 0);
    signal AlgoParameters: ParameterSpace(NumberOfAlgorithms - 1 downto 0);
    
    
    --algo ports
    
    signal AlgoResults:  std_logic_vector(NumberOfResultBits - 1 downto 0);
    signal AlgoOverflow: std_logic_vector(NumberOfResultBits - 1 downto 0);

    signal rateCounter:  arraySLV32(numberOfResultBits-1 downto 0);
    signal rateCounterOv:  arraySLV32(numberOfResultBits-1 downto 0);
    signal rateCounter_valid: std_logic;

    signal AlgoResults32bit, AlgoOverflow32bit : std_logic_vector(31 downto 0):=(others => '0');
    signal muxedAlgoOutputCTP: std_logic_vector(NumberOfOutputBits-1 downto 0);
    
    signal spyData64BitFromAlgos: std_logic_vector(2*NumberOfResultBits-1 downto 0);
    signal enablePlaybackOfAlgos: std_logic;
    signal playbackData32BitFakingAlgos: std_logic_vector(NumberOfOutputBits-1 downto 0);
    
    signal ctpOutputControl: std_logic_vector(1 downto 0);
    signal ctpOutputMask: std_logic_vector(31 downto 0);
    
    signal ctp_counter: std_logic_vector(4 downto 0);
    signal ctp_runningOne, ctp_runningOne_remap: std_logic_vector(31 downto 0);
    
    signal ctp_data:            std_logic_vector(31 downto 0); 
    signal ctp_data_delayed:    std_logic_vector(31 downto 0);
    signal ctp_data_ddr, ctp_data_p, ctp_data_n: std_logic_vector(15 downto 0);
    
    signal ctp_odelay_control: std_logic_vector(5 downto 0);
    signal ctp_odelay_pulse:   std_logic_vector(1 downto 0);
    signal ctp_odelay_status:  arraySLV5(15 downto 0);
    
    
    signal data_is_corrupted, data_is_corrupted_delayed, data_is_corrupted_sim : std_logic_vector(numberOfChannels-1 downto 0) := (others => '0');
    signal err_in : err_in_array :=(others => (others => '0'));
    signal err_flag : std_logic_vector(6 downto 0) :=(others => '0');
    type err_flag_array is array (3 downto 0) of std_logic_vector(6 downto 0);
    signal err_flag_delay : err_flag_array :=(others => (others => '0'));
    signal rxoutclk, rxoutclk_bufg: std_logic;
    signal delayed_muons_delay: std_logic_vector(3 downto 0);
        
    signal JetTOBs: JetArray (InputWidthJet - 1 downto 0);
    signal met    : MetArray (InputWidthMET - 1 downto 0);
    signal BCID: std_logic_vector(11 downto 0);
    signal eventCounterReset : std_logic;
    signal eventCounterReset_in_p, eventCounterReset_in_n: std_logic; -- DUMMY
--    attribute IODELAY_GROUP: string;
--	attribute IODELAY_GROUP of BANK14_IDELAYCTRL:  label is "bank14_iodelaygroup";
	
	

begin
	
-- set ProcessorID
ProcessorID <= std_logic_vector(to_unsigned(PROCESSOR_NUMBER, ProcessorID'length));
rod_status_register(0) <= x"16051017";        -- date - yymmddhh

--------------------------------------------------------
--input mapping
--------------------------------------------------------
    MgtRefClk1P(0)  <= MGT2_CLK_P(1);
    MgtRefClk1N(0)  <= MGT2_CLK_N(1);
    MgtRefClk1P(1)  <= MGT4_CLK_P(1);
    MgtRefClk1N(1)  <= MGT4_CLK_N(1);
    MgtRefClk1P(2)  <= MGT2_CLK_P(2);
    MgtRefClk1N(2)  <= MGT2_CLK_N(2);
    MgtRefClk1P(3)  <= MGT4_CLK_P(2);
    MgtRefClk1N(3)  <= MGT4_CLK_N(2);
    MgtRefClk1P(4)  <= MGT2_CLK_P(3);
    MgtRefClk1N(4)  <= MGT2_CLK_N(3);
    MgtRefClk1P(5)  <= MGT4_CLK_P(3);
    MgtRefClk1N(5)  <= MGT4_CLK_N(3);
    MgtRefClk1P(6)  <= MGT2_CLK_P(4);
    MgtRefClk1N(6)  <= MGT2_CLK_N(4);
    MgtRefClk1P(7)  <= MGT4_CLK_P(4);
    MgtRefClk1N(7)  <= MGT4_CLK_N(4);
    MgtRefClk1P(8)  <= MGT2_CLK_P(5);
    MgtRefClk1N(8)  <= MGT2_CLK_N(5);
    MgtRefClk1P(9)  <= MGT4_CLK_P(5);
    MgtRefClk1N(9)  <= MGT4_CLK_N(5);
    MgtRefClk1P(10) <= MGT2_CLK_P(7);
    MgtRefClk1N(10) <= MGT2_CLK_N(7);
    MgtRefClk1P(11) <= MGT4_CLK_P(7);
    MgtRefClk1N(11) <= MGT4_CLK_N(7);
    MgtRefClk1P(12) <= MGT2_CLK_P(8);
    MgtRefClk1N(12) <= MGT2_CLK_N(8);
    MgtRefClk1P(13) <= MGT4_CLK_P(8);
    MgtRefClk1N(13) <= MGT4_CLK_N(8);
    MgtRefClk1P(14) <= MGT2_CLK_P(9);
    MgtRefClk1N(14) <= MGT2_CLK_N(9);
    MgtRefClk1P(15) <= MGT4_CLK_P(9);
    MgtRefClk1N(15) <= MGT4_CLK_N(9);
    MgtRefClk1P(16) <= MGT2_CLK_P(10);
    MgtRefClk1N(16) <= MGT2_CLK_N(10);
    MgtRefClk1P(17) <= MGT4_CLK_P(10);
    MgtRefClk1N(17) <= MGT4_CLK_N(10);
    MgtRefClk1P(18) <= MGT2_CLK_P(11);
    MgtRefClk1N(18) <= MGT2_CLK_N(11);
    MgtRefClk1P(19) <= MGT4_CLK_P(11);
    MgtRefClk1N(19) <= MGT4_CLK_N(11);
    
--------------------------------------------------------
--input buffer
--------------------------------------------------------

GCK1_IBUFGDS: IBUFGDS
	port map(
		I  => GCK1_P,
		IB => GCK1_N,
		O  => gck1
);

GCK1_GBUF: BUFG port map(I => gck1, O => gck1_bufg);

GCK2_IBUFGDS: IBUFGDS
	port map(
		I  => GCK2_P,
		IB => GCK2_N,
		O  => gck2
);

GCK2_GBUF: BUFG port map(I => gck2, O => gck2_bufg);

MGTREFCLK_IBUFDS_GTE2_GEN: for i in numberOfQuads-1 downto 0 generate
begin
  MGTREFCLK_IBUFDS_GTE2: IBUFDS_GTE2
    generic map(
    CLKRCV_TRST     => true,
    CLKCM_CFG       => true,
    CLKSWING_CFG    => "11"
    )
    port map(
    I       => MgtRefClk1P(i), 
    IB      => MgtRefClk1N(i),
    O       => MGTRefClk(i),
    ODIV2   => open,
    CEB     => '0'
);
end generate;

IPBUSBRIDGE_IN_IBUFDS_GEN: for i in 4 downto 0 generate
IPBUSBRIDGE_IN_IBUFDS: IBUFDS
port map(
    I       => ipbBridgeRequest_p(i),
   IB      => ipbBridgeRequest_n(i),
    O       => ipbusBridgeBusIn(i)
);
end generate IPBUSBRIDGE_IN_IBUFDS_GEN;

TTC_CTRLBUS_IBUFDS: IBUFDS
	port map(
		I     => ttcBridge_p,
		IB    => ttcBridge_n,
		O     => ttcBridge
	);

BANK14_IDELAYCTRL: IDELAYCTRL
	 port map (
		  REFCLK => crystalclk200,
		  RST    => idelayctrlReset,
		  RDY    => idelayctrlReady
	  );

process(sysclk40, idelayctrlReady) begin
if idelayctrlReady='0' then clk_status(3) <= '1';
elsif rising_edge(sysclk40) and clk_control='1' then clk_status(3) <= '0';
end if;
end process;


--sub modules

--------------------------------------------------------
-- processor infrastructure
--------------------------------------------------------
INFR: entity work.infrastructure
    port map(
        pll_reset               => pll_reset,
        gck1	                => gck1,
        gck2_bufg               => gck2_bufg,
        sysclk40                => sysclk40,
        sysclk80                => sysclk80,
        sysclk160               => sysclk160,
        sysclk320               => sysclk320,
        sysclk400               => sysclk400,
        rodclk400_io            => rodclk400_io,
        rodclk400_r             => rodclk400_r,
        rodclk80                => rodclk80,
        sysclk_pll_locked       => sysclk_pll_locked,
        crystalclk40            => crystalclk40,
        crystalclk62_5          => crystalclk62_5,
        crystalclk200           => crystalclk200,
        crystalclk_pll_locked   => crystalclk_pll_locked,
        idelayctrlReset         => idelayctrlReset,
        xadc_control            => xadc_control,
        xadc_status             => xadc_status,
        ttcBridge               => ttcBridge,
        ttcBridge_control       => ttcBridge_control,
        ttcBridge_status        => ttcBridge_status,
        ttcL1Accept             => ttcL1Accept,
        ttcBroadcast            => ttcBroadcast,
        ttcBunchCounterReset    => ttcBunchCounterReset,
        usr_access              => usr_access,
        clk_control             => clk_control,
        clk_status              => clk_status(2 downto 0)
    );

--------------------------------------------------------
-- ipbus
--------------------------------------------------------

CTRL: entity work.controlPath
  generic map(
    PROCESSOR_NUMBER => PROCESSOR_NUMBER
    )
port map(
    sysclk40    => sysclk40,
    sysclk80    => sysclk80,
    sysclk160   => sysclk160,
    sysclk400   => sysclk400,
    
    sysclk_pll_locked               => sysclk_pll_locked,
    
    ipbBridgeBusIn                  => ipbusBridgeBusIn,
    ipbBridgeBusOut                 => ipbusBridgeBusOut,

    clk_control                     => clk_control,
    clk_status                      => clk_status,

    xadc_control                    => xadc_control,
    xadc_status                     => xadc_status,

    pll_reset                       => pll_reset,
    
    ttcBridge_control               => ttcBridge_control,
    ttcBridge_status                => ttcBridge_status,
    ttcBroadcast                    => ttcBroadcast,

    rod_control_register            => rod_control_register,
    rod_status_register             => rod_status_register,


    ChannelControl                  => ChannelControl,
    ChannelStatus                   => ChannelStatus,
    GeneralControl					=> GeneralControl,
    
    topoInput_enablePlayback        => topoInput_enablePlayback,
    topoInput_playbackData128       => topoInput_playbackData128,
    topoInput_spyData128            => topoInput_spyData128,

    
    SortParameters                  => SortParameters,
    AlgoParameters                  => AlgoParameters,
    
    spyData64BitFromAlgos           => spyData64BitFromAlgos,
    enablePlaybackOfAlgos           => enablePlaybackOfAlgos,
    playbackData32BitFakingAlgos    => playbackData32BitFakingAlgos,
    
    rateCounter                     => rateCounter,
    rateCounterOv                     => rateCounterOv,
    rateCounter_valid               => rateCounter_valid,
    
    ctpOutputControl                => ctpOutputControl,
    ctpOutputMask                   => ctpOutputMask,
    
    ctp_odelay_control              => ctp_odelay_control,
    ctp_odelay_pulse                => ctp_odelay_pulse,
    ctp_odelay_status               => ctp_odelay_status,
  
    ProcessorID                     => ProcessorID, 
    FirmwareVersion                 => FirmwareVersion,
    FirmwareDate                    => FirmwareDate,
    FirmwareRevision                => FirmwareRevision,
    usr_access                      => usr_access,

    JetTOBs                         => JetTOBs,
    Met                             => met,
    EventCounterReset               => eventCounterReset,
    L1Accept                        => l1_accepted_in, --ttcL1Accept,
--    BunchCounterReset               => ttcBunchCounterReset
    BCID                            => BCID
);

GEN_PROCESSOR1_ECR_IN : if PROCESSOR_NUMBER = 1 generate

--IBUFDS_ECR_IN: IBUFDS
--  generic map (DIFF_TERM => true, IBUF_LOW_PWR => false, IOSTANDARD => "DEFAULT")
--  port map (O            => eventCounterReset, I => eventCounterReset_in_p, IB => eventCounterReset_in_n);

end generate;

RTDP: entity work.realTimeDataPath
generic map(
  PROCESSOR_NUMBER => PROCESSOR_NUMBER,
    GTH_VIRTEX_TO_KINTEX => GTH_VIRTEX_TO_KINTEX,
    DEBUG => DEBUG
)
port map(
    sysclk40                    => sysclk40,
    sysclk80                    => sysclk80,
    sysclk160                   => sysclk160,
    sysclk320                   => sysclk320,
    sysclk_pll_locked           => sysclk_pll_locked,   
    gtrefclk                    => MGTRefClk,
    gtrxp                       => RxP,
    gtrxn                       => RxN,
    
    ChannelControl              => ChannelControl,
    ChannelStatus               => ChannelStatus,
    GeneralControl				=> GeneralControl,
    
    topoInput_enablePlayback    => topoInput_enablePlayback,
    topoInput_playbackData128   => topoInput_playbackData128,
    topoInput_spyData128        => topoInput_spyData128,
    rod_dataToAlgorithm128      => rod_dataToAlgorithm128,
    
    SortParameters              => SortParameters,
    AlgoParameters              => AlgoParameters,
    
    AlgoResults                 => AlgoResults,
    AlgoOverflow                => AlgoOverflow,
    
    rateCounter_out             => rateCounter,
    rateCounterOv_out             => rateCounterOv,
    rateCounter_valid_out       => rateCounter_valid,
    --ROD
    ROD_DATA                    => out_data(15 downto 0),
    ROD_DATA_VALID              => data_valid_out(0),
    gttxp                       => TxP,
    gttxn                       => TxN,
    GTH_ROD_CLK_OUT             => GTH_ROD_CLK,
    DATA_IS_CORRUPTED_OUT       => data_is_corrupted,
    
    jetTobs_out                 => JetTOBs,
    met_out                     => met,
    bcid                        => bcid
);

    spydata64bitfromalgos <= AlgoOverflow & AlgoResults when rising_edge(sysclk40);
    muxedAlgoOutputCTP    <= (AlgoResults or AlgoOverflow) when enablePlaybackOfAlgos='0' else playbackData32BitFakingAlgos;

-----------------------------------------------------------------------------
-- ROD
-----------------------------------------------------------------------------
  delayed_muons_delay <= std_logic_vector(to_unsigned(std_int(rod_control_register(25)(3 downto 0))-1,4));
  SHIFT_DATA_FROM_CMX: for i in 0 to NUMBER_OF_ROS_ROI_INPUT_BUSES-2  generate
    DISABLE_DELAYED_MUONS: if i /= 77 and i /= 78 generate
      ITERATE_WITH_SRL: for j in 0 to 127 generate
        SRL16E_inst : SRL16E
          generic map (
            INIT => X"0000")
          port map (
            Q => data_for_ros_roi_bus_synch(i)(j),--muxedData128BitToAlgorithms_reg_shifted(i)(j),       
            A0 => rod_control_register(25)(0),     
            A1 => rod_control_register(25)(1),     
            A2 => rod_control_register(25)(2),     
            A3 => rod_control_register(25)(3),     
            CE => '1',     
            CLK => sysclk40,   
            D => rod_dataToAlgorithm128(i)(j)    
            );
      end generate ITERATE_WITH_SRL;
    end generate DISABLE_DELAYED_MUONS;

    ENABLE_DELAYED_MUONS: if i = 77 or i = 78 generate
      DELAYED_ITERATE_WITH_SRL: for j in 0 to 127 generate
        SRL16E_inst : SRL16E
          generic map (
            INIT => X"0000")
          port map (
            Q => data_for_ros_roi_bus_synch(i)(j),--muxedData128BitToAlgorithms_reg_shifted(i)(j),       
            A0 => delayed_muons_delay(0),     
            A1 => delayed_muons_delay(1),
            A2 => delayed_muons_delay(2),
            A3 => delayed_muons_delay(3),
            CE => '1',     
            CLK => sysclk40,   
            D => rod_dataToAlgorithm128(76+(i-77)*3)(j)    
            );
      end generate DELAYED_ITERATE_WITH_SRL;
    end generate ENABLE_DELAYED_MUONS;
    
    
  end generate SHIFT_DATA_FROM_CMX;

  --SHIFT_ERROR_FROM_CMX: for i in 0 to NUMBER_OF_ROS_ROI_INPUT_BUSES-2  generate
  --    SRL16E_inst : SRL16E
  --      generic map (
  --        INIT => X"0000")
  --      port map (
  --        Q => data_is_corrupted_delayed(i),--muxedData128BitToAlgorithms_reg_shifted(i)(j),       
  --        A0 => rod_control_register(25)(0),     
  --        A1 => rod_control_register(25)(1),     
  --        A2 => rod_control_register(25)(2),     
  --        A3 => rod_control_register(25)(3),     
  --        CE => '1',     
  --        CLK => sysclk40,   
  --        D => data_is_corrupted(i)    
  --        );
  --end generate SHIFT_ERROR_FROM_CMX;

--  rod_reset <= (not sysclk_pll_locked) or  rod_control_register(0)(0);
  rod_reset <= rod_control_register(0)(0);
    
  SELECT_SETTINGS_FOR_OP: if SIMULATION = false generate

    GENERATE_DATA_CONNECTIONS: for i in 0 to NUMBER_OF_ROS_ROI_INPUT_BUSES-2  generate
      data_for_ros_roi_bus(i) <= data_for_ros_roi_bus_synch(i);
    end generate GENERATE_DATA_CONNECTIONS;  

    AlgoResults32bit(AlgoResults'length-1 downto 0) <= AlgoResults;
    AlgoOverflow32bit(AlgoOverflow'length-1 downto 0) <= AlgoOverflow;
    
    DATA_FROM_ALGO : for i in 0 to 3 generate
      WRITE_ALGO_RESULTS_TO_ROD : process(sysclk40)
      begin
        if rising_edge(sysclk40) then
          if enablePlaybackOfAlgos='0' then
            data_for_ros_roi_bus_algo(i*2*8+7 downto i*2*8) <= AlgoResults32bit(i*8+7 downto i*8);
            data_for_ros_roi_bus_algo(i*2*8+15 downto i*2*8+8) <= AlgoOverflow32bit(i*8+7 downto i*8);
          else
            data_for_ros_roi_bus_algo(i*2*8+7 downto i*2*8) <= playbackData32BitFakingAlgos(i*8+7 downto i*8);
            data_for_ros_roi_bus_algo(i*2*8+15 downto i*2*8+8) <= playbackData32BitFakingAlgos(i*8+7 downto i*8);
          end if;   
        end if;
      end process WRITE_ALGO_RESULTS_TO_ROD;
    end generate DATA_FROM_ALGO;

    SYNCH_ALGO_RESULTS: process(sysclk40)
    begin
      if rising_edge(sysclk40) then
        data_for_ros_roi_bus(80)(0) <= '0'; 
        data_for_ros_roi_bus(80)(64 downto 1) <= data_for_ros_roi_bus_algo(63 downto 0);
        data_for_ros_roi_bus(80)(69 downto 65) <= err_flag(6) & err_flag(4) & ('0' or err_flag(2)) & err_flag(1 downto 0);
        data_for_ros_roi_bus(80)(127 downto 70) <= (others => '0');
      end if;
    end process SYNCH_ALGO_RESULTS;
    
    ASSIGN_NUMBER_OF_SLICES_OP : for i in 0 to NUMBER_OF_SLICES'length-1 generate
      CMX_INPUTS: if i /= 80 generate
        number_of_slices(i) <= '0' & unsigned(rod_control_register(3 + (i/8))(((i mod 8)+1)*4-2 downto (i mod 8)*4));
        lvl0_offset(i)      <= '0' & unsigned(rod_control_register(14 + (i/8))(((i mod 8)+1)*4-2 downto (i mod 8)*4));
      end generate CMX_INPUTS;

      TOPO_INPUT: if i = 80 generate
        number_of_slices(i) <= '0' & unsigned(rod_control_register(3 + (i/8))(((i mod 8)+1)*4-2 downto (i mod 8)*4)) when unsigned(rod_control_register(3 + (i/8))(((i mod 8)+1)*4-2 downto (i mod 8)*4)) /= 0 else x"1";
        lvl0_offset(i)      <= '0' & unsigned(rod_control_register(14 + (i/8))(((i mod 8)+1)*4-2 downto (i mod 8)*4));
      end generate TOPO_INPUT;
    end generate ASSIGN_NUMBER_OF_SLICES_OP;
  end generate SELECT_SETTINGS_FOR_OP;

  SET_ALL_CORRUPTED_FLAGS: process(sysclk40)
  begin
    if rising_edge(sysclk40) then
      for channel in 0 to NUMBER_OF_ROS_ROI_INPUT_BUSES-2 loop
        report "i:" & integer'image(channel);
        report "tob type: " & integer'image(to_integer(unsigned(TOB_MAP((PROCESSOR_NUMBER+1) mod 2)(NUMBER_OF_ROS_ROI_INPUT_BUSES - channel))));
        assert TOB_MAP(0)(NUMBER_OF_ROS_ROI_INPUT_BUSES-79) /= MUON_TOB_TYPE  severity failure;
        --here problem with mixing to and downto, maybe problem in different version
        err_in(to_integer(unsigned (TOB_MAP((PROCESSOR_NUMBER+1) mod 2)(NUMBER_OF_ROS_ROI_INPUT_BUSES - channel))))(channel) <= data_is_corrupted(channel);
        --    when EM_TOB_TYPE =>
        --      err_in(0)(channel) <= data_is_corrupted(channel);
        --    when TAU_TOB_TYPE =>
        --      err_in(1)(channel) <= data_is_corrupted(channel);
        --    when MUON_TOB_TYPE =>
        --      err_in(2)(channel) <= data_is_corrupted(channel);
        --    when JET_TOB_TYPE =>
        --      err_in(4)(channel) <= data_is_corrupted(channel);
        --    when ENERGY_TOB_TYPE =>
        --      err_in(6)(channel) <= data_is_corrupted(channel);
        --    when others =>
        --      err_in <= err_in;
        --end case;
      end loop;
    end if;
  end process SET_ALL_CORRUPTED_FLAGS;

  ITERATE_FLAVOURS: for flavour in 0 to 6 generate
    SET_FLAVOUR_ERR: process(sysclk40)
    begin
      if rising_edge(sysclk40) then
        if unsigned(err_in(flavour)) /= 0 and rod_control_register(29)(25 + flavour) = '0' then
          err_flag_delay(0)(flavour) <= '1';
        else
          err_flag_delay(0)(flavour) <= '0';
        end if;
      end if;
    end process SET_FLAVOUR_ERR;
  end generate ITERATE_FLAVOURS;

  DELAY_ERR_FLAGS:process(sysclk40)
  begin
    if rising_edge(sysclk40) then
      err_flag_delay(1) <= err_flag_delay(0);
      err_flag_delay(2) <= err_flag_delay(1);
      err_flag_delay(3) <= err_flag_delay(2);
    end if;
  end process DELAY_ERR_FLAGS;

  CON_ERR: for i in 0 to 6 generate
    OTHER_ERR_FLAGS: if i /= 3 generate
      err_flag(i) <= err_flag_delay(0)(i) when unsigned(rod_control_register(25)(3 downto 0)) < 3
                  else err_flag_delay(std_int(rod_control_register(25)(3 downto 0))-2)(i);
    end generate OTHER_ERR_FLAGS;
    MUON_ERR_FLAG: if i = 3 generate
      err_flag(3) <= err_flag_delay(0)(2) when unsigned(rod_control_register(25)(3 downto 0)) < 3
                  else err_flag_delay(std_int(rod_control_register(25)(3 downto 0))-3)(i);
    --it will not work when Delay_CMX_Data is less than 3 !, then whole path
    --needs to be fixed
    end generate MUON_ERR_FLAG;
  end generate CON_ERR;
    
  
  --ILA_ERROR_FLAG_INST : entity work.error_flag_ila
  --  PORT MAP (
  --      clk => sysclk40,
  --      probe0 => data_is_corrupted
  --      );
                          
                          
  SELECT_SETTINGS_FOR_SIM: if SIMULATION = true generate
    SET_DATA_IS_CORRUPTED_SIM: process 
    begin
      wait for 1000 ns;
      data_is_corrupted_sim <= x"0000000000000000000f";
      wait for 1000 ns;
      data_is_corrupted_sim <= x"00000000000000000f00";
      wait for 1000 ns;
      data_is_corrupted_sim <= x"000000f0000000000000";
      wait for 1000 ns;
      data_is_corrupted_sim <= x"000ff000000000000000";
      wait for 1000 ns;
      data_is_corrupted_sim <= x"f0000000000000000000";
    end process SET_DATA_IS_CORRUPTED_SIM;
    
    DATA_FOR_ROS_ROI_BUS_PROC_SIM : process (sysclk40)
    begin
      if rising_edge(sysclk40) then
        for i in 0 to 79 loop 
          data_for_ros_roi_bus(i) <= x"fedcba98765432100123456789abcdef"; 
        end loop;
        data_for_ros_roi_bus(80)(0) <= '0'; 
        data_for_ros_roi_bus(80)(64 downto 1) <= x"0123456789abcdef";
        data_for_ros_roi_bus(80)(69 downto 65) <= err_flag(6) & err_flag(4) & ('0' or err_flag(2)) & err_flag(1 downto 0);
        data_for_ros_roi_bus(80)(127 downto 70) <= (others => '0');
      end if;
    end process DATA_FOR_ROS_ROI_BUS_PROC_SIM;
    ASSIGN_NUMBER_OF_SLICES_SIM : for i in 0 to NUMBER_OF_SLICES'length-1 generate
      number_of_slices(i) <= to_unsigned(5, NUMBER_OF_SLICES(0)'length);  
      lvl0_offset(i)      <= to_unsigned(i mod 6, LVL0_OFFSET(0)'length); 
    end generate ASSIGN_NUMBER_OF_SLICES_SIM;
  end generate SELECT_SETTINGS_FOR_SIM;
                           
  IBUFDS_L1ACCPETED_IN: IBUFDS
  generic map (DIFF_TERM => true, IBUF_LOW_PWR => false, IOSTANDARD => "DEFAULT")
  port map (O            => l1_accepted_in, I => l1_accepted_in_p, IB => l1_accepted_in_n);
--  port map (O            => l1_accepted_in, I => CTRLBUS_IN_P(6), IB => CTRLBUS_IN_N(6));


    L1A_PULSE_PROC : process (sysclk40 )
    begin
      if rising_edge(sysclk40) then
        if l1_accepted_in_synch_a  = '1' and l1_accepted_in_synch_b = '0' then
          l1_accepted_in_pulse <= '1';
          l1_accepted_in_synch_a <= l1_accepted_in;
          l1_accepted_in_synch_b <= l1_accepted_in_synch_a;
        else
          l1_accepted_in_pulse <= '0';
          l1_accepted_in_synch_a <= l1_accepted_in;
          l1_accepted_in_synch_b <= l1_accepted_in_synch_a;
        end if;
      end if;
    end process L1A_PULSE_PROC;

    send_on_crc <= rod_control_register(29)(NUMBER_OF_ROS_ROI_INPUT_BUSES-1-64 downto 0) & rod_control_register(28) & rod_control_register(27);
    
    L1TOPO_TO_DDR_INST : entity work.l1topo_to_ddr
      generic map (
        MAKE_SYNCH_INPUT => 0,
        SIMULATION => SIMULATION,
        DEBUG      => FALSE,
        GTH_VIRTEX_TO_KINTEX => GTH_VIRTEX_TO_KINTEX
        )
      port map (
        RESET                 => rod_reset,
        DATA_IN_CLK           => sysclk40,
        DATA_OUT_CLK          => sysclk160,
        DDR_CLK               => rodclk80,
        GTH_CLK               => gth_rod_clk,
        NUMBER_OF_SLICES      => number_of_slices,
        LVL0_ACCEPTED         => l1_accepted_in_pulse,
        LVL0_VALID            => '1',
--        LVL0_GLOBAL_OFFSET    => rod_rw_register(5)(7 downto 0),
        LVL0_GLOBAL_OFFSET    => rod_control_register(1)(7 downto 0),
        LVL0_FULL_THR         => rod_control_register(2)(7 downto 0),
        LVL0_OFFSET           => lvl0_offset,
        ROS_ROI_IN_DATA       => data_for_ros_roi_bus,--TEMP_data_for_ros_roi_bus,
        CRC_ERR_IN            => crc_err,
        MAX_OFFSET            => rod_control_register(26)(3 downto 0),
        SEND_ON_CRC           => send_on_crc,
        DATA_VALID_IN         => "1",
        OUT_DATA              => out_data,
        DATA_VALID_OUT        => data_valid_out,
        L0_BUSY               => l0_busy,
        SPECIAL_CHARACTER_OUT => special_character_out,
--        ROD_DBG               => rod_rw_register(8)
--        TEST_IN               => playbackData32BitFakingAlgos(0),
        ROD_DBG               => rod_status_register(2)
    );

    OBUFDS_BUSY_OUT: OBUFDS
      generic map (IOSTANDARD => "DEFAULT")
      port map (I => l0_busy, O => l0_busy_p, OB => l0_busy_n);
    
    OLD_DDR_ROD_GEN: for i in 0 to 7 generate
      ROD_OBUFDS: OBUFDS
        port map(
          I  => '0',
          O  => rodBridge_p(i),
          OB => rodBridge_n(i) 
          );
    end generate;
                     
----------------------------------------------------------------------------
-- CTP Output
-----------------------------------------------------------------------------
    
process(sysclk40) begin
if rising_edge(sysclk40) then
    ctp_counter <= std_logic_vector(unsigned(ctp_counter)+1);
    if ctp_counter="00000" then ctp_runningOne <= x"00000001";
    elsif ctp_counter="11111" then ctp_runningOne <= x"ffffffff";
    else ctp_runningOne(31 downto 0) <= ctp_runningOne(30 downto 0) & '0';
    end if;
    
    for i in 15 downto 0 loop
    	ctp_runningOne_remap(i) <= ctp_runningOne(2*i);
    	ctp_runningOne_remap(i+16) <= ctp_runningOne(2*i+1);
    end loop;
end if;
end process;



    ctp_data <= muxedAlgoOutputCTP                     when ctpOutputControl="00"
           else (muxedAlgoOutputCTP and ctpOutputMask) when ctpOutputControl="01"
           else ctpOutputMask                          when ctpOutputControl="10" 
           else ctp_runningOne_remap;



DEBUG_GEN: if DEBUG=1 generate
ILA_CTP_OUTPUT : entity work.ila_ctp_output
PORT MAP (
	clk => sysclk40,
	probe0 => ctp_data, 
	probe1 => ctpOutputControl,
	probe2 => ctpOutputMask
);
end generate;

CTP_DATA_ODDR_GEN: for l in 15 downto 0 generate begin
    CTP_DATA_ODDR: ODDR
    generic map(
        DDR_CLK_EDGE    => "SAME_EDGE", 
        INIT            => '0',
        SRTYPE          => "SYNC"
    )
    port map (
        C   => sysclk40,
        CE  => '1',   
        D1  => ctp_data(l),--(2*l),
        D2  => ctp_data(l+16),--(2*l+1),
        Q   => ctp_data_ddr(l),
    --unused ports
        R   => '0',
        S   => '0'
    );
end generate;

SYSCLK40_ODDR: ODDR
generic map(
    DDR_CLK_EDGE => "SAME_EDGE", 
    INIT => '0',
    SRTYPE => "SYNC")
port map (
    Q => sysclk40_ddr,
    C => gck1_bufg,
    CE => '1',   
    D1 => '1',
    D2 => '0',
    R => '0',
    S => '0'
);

CRYSTALCLK40_ODDR: ODDR
generic map(
    DDR_CLK_EDGE => "SAME_EDGE", 
    INIT => '0',
    SRTYPE => "SYNC")
port map (
    Q => crystalclk40_ddr,
    C => crystalclk40,
    CE => '1',   
    D1 => '1',
    D2 => '0',
    R => '0',
    S => '0'
);

--------------------------------------------------------
--output delays and buffer
--------------------------------------------------------

SYSLCK40_OBUFDS: OBUFDS
port map(
    I  => sysclk40_ddr,
    O  => sysclk40_p,
    OB => sysclk40_n
);


CRYSTALCLK40_OBUFDS: OBUFDS
port map(
    I  => crystalclk40_ddr,
    O  => crystalclk40_p,
    OB => crystalclk40_n
);

IPBUSBRIDGE_OUT_OBUFDS_GEN: for i in 2 downto 0 generate
IPBUSBRIDGE_OUT_OBUFDS: OBUFDS
port map(
    I  => ipbusBridgeBusOut(i),
    O  => ipbBridgeResponse_p(i),
    OB => ipbBridgeResponse_n(i)
);
end generate;


CTP_ODELAY_GEN: for l in 15 downto 0 generate
--    attribute IODELAY_GROUP of CTP_ODELAY:         label is "bank14_iodelaygroup";
    begin
    CTP_ODELAY: ODELAYE2
    generic map (
        CINVCTRL_SEL            => "FALSE",
        DELAY_SRC               => "ODATAIN",
        HIGH_PERFORMANCE_MODE   => "TRUE",
        ODELAY_TYPE             => "VAR_LOAD",
        ODELAY_VALUE            => 0,
        PIPE_SEL                => "FALSE",
        REFCLK_FREQUENCY        => 200.4,
        SIGNAL_PATTERN          => "DATA"
    )
    port map(
        ODATAIN		   => ctp_data_ddr(l),
        CNTVALUEIN	   => ctp_odelay_control(4 downto 0),
        LD		       => ctp_odelay_pulse(0),
        INC		       => ctp_odelay_control(5),
        C		       => sysclk40,
        CE		       => ctp_odelay_pulse(1),
        CNTVALUEOUT    => ctp_odelay_status(l),
        DATAOUT        => ctp_data_delayed(l),
    --unused ports
        CINVCTRL	   => '0',
        LDPIPEEN	   => '0',
        REGRST		   => '0',
        CLKIN		   => '0'
    );
end generate CTP_ODELAY_GEN;

  
  
CTP_OBUFDS_GEN: for l in 15 downto 0 generate
    CTP_OBUFDS: OBUFDS
    port map(
        I   => ctp_data_delayed(l),
        O   => ctp_data_p(l),
        OB  => ctp_data_n(l)
    );
end generate CTP_OBUFDS_GEN;
	 
	 

EXT_V7_MAP: for l in 15 downto 0 generate
    
    EXT_V7_P(l+ctpOutputLinesOffset(PROCESSOR_NUMBER)) <= ctp_data_p(l);
    EXT_V7_N(l+ctpOutputLinesOffset(PROCESSOR_NUMBER)) <= ctp_data_n(l);
    
end generate EXT_V7_MAP;
    
--IPBUSBRIDGE_OUT_OBUFDS_GEN: for i in 2 downto 0 generate
--    IPBUSBRIDGE_OUT_OBUFDS: OBUFDS
--    port map(
--        I  => ipbusBridgeBusOut(i),
--        O  => ipbusBridgeBusOut_p(i),
--        OB => ipbusBridgeBusOut_n(i)
--    );
--end generate;

--    CTRLBUS_OUT_P(18) <= ipbusBridgeBusOut_p(0);
--	CTRLBUS_OUT_N(18) <= ipbusBridgeBusOut_n(0);
--	CTRLBUS_OUT_P(19) <= ipbusBridgeBusOut_p(1);
--	CTRLBUS_OUT_N(19) <= ipbusBridgeBusOut_n(1);
--	CTRLBUS_OUT_P(20) <= ipbusBridgeBusOut_p(2);
--	CTRLBUS_OUT_N(20) <= ipbusBridgeBusOut_n(2);
 
end Behavioral;
