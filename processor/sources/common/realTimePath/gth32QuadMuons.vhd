----------------------------------------------------------------------------------
-- Company:        
-- Engineer:        Marek Palka
-- 
-- Create Date:     03/06/2015 08:11:02 PM
-- Module Name:     gth32QuadMuons - Behavioral
-- Target Devices:  xc7vx690tffg1927-3
-- Tool Versions:   Xilinx Vivado 2014.4
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

use work.l1topo_package.all;


entity gth32QuadMuons is
  generic(
          DEBUG: integer := 1
          );

  port(

    sysclk40:           in  std_logic;
    sysclk80:           in  std_logic;
    sysclk160:          in  std_logic;
    sysclk320:          in  std_logic;
    sysclk_pll_locked:  in  std_logic;
    gtrefclk:           in  std_logic;
    
    gtrxp, gtrxn:       in  std_logic_vector(3 downto 0);
    
    cplllock:           out std_logic_vector(3 downto 0);
    rxfsmresetdone:     out std_logic_vector(3 downto 0);
    rxresetdone:        out std_logic_vector(3 downto 0);
    rxdata32:           out arraySLV32(3 downto 0);
    rxcharisk4:         out arraySLV4(3 downto 0);
    rxchariscomma4:     out arraySLV4(3 downto 0);
    
    rxdisperr4:         out arraySLV4(3 downto 0);
    rxdisperr1:         out std_logic_vector(3 downto 0);
    
    rxnotintable4:      out arraySLV4(3 downto 0);
    rxnotintable1:      out std_logic_vector(3 downto 0);
    
    rxbufstatus3:       out arraySLV3(3 downto 0);
    rxbufstatus1:       out std_logic_vector(3 downto 0);

    rxbyteisaligned:    out std_logic_vector(3 downto 0);
    rxbyterealign:      out std_logic_vector(3 downto 0);

    delay:              in arraySLV4(3 downto 0);
    gth_error_out:      out arraySLV4(3 downto 0);
    
    cpllpd:             in  std_logic_vector(3 downto 0);
    soft_reset:         in  std_logic_vector(3 downto 0);
    rxbufreset:         in  std_logic_vector(3 downto 0);
    rxpolarity:         in  std_logic_vector(3 downto 0);
    
    
    gt_drp_addr:        in  std_logic_vector(8 downto 0);
    gt_drp_dataIn:      in  std_logic_vector(15 downto 0);
    gt_drp_dataOut:     out arraySLV16(3 downto 0);
    gt_drp_enable:      in  std_logic_vector(3 downto 0);
    gt_drp_ready:       out std_logic_vector(3 downto 0);
    gt_drp_writeEnable: in  std_logic_vector(3 downto 0)
    
);

end gth32QuadMuons;


architecture Behavioral of gth32QuadMuons is
  
  signal mem_data_in, mem_data_out : arraySLV38(3 downto 0);
  signal rxdata32_out, rxdata32_reg : arraySLV32(3 downto 0);
  signal rxcharisk4_out : arraySLV4(3 downto 0);
  signal probe : arraySLV128(7 downto 0);
  signal write_addr, read_addr : arrayUNS5(3 downto 0);
  signal delay_cntr : arrayUNS4(3 downto 0);
  signal rxoutclk_out, rxoutclk_out_buf : std_logic_vector(3 downto 0);
  signal gt0_qplloutclk_i  : std_logic;
  signal gt0_qplloutrefclk_i : std_logic;
  signal local_reset_160 : std_logic_vector(3 downto 0);
  --debug
  signal cplllock_l:            std_logic_vector(3 downto 0);
  signal rxfsmresetdone_l:      std_logic_vector(3 downto 0);
  signal rxresetdone_l:         std_logic_vector(3 downto 0);
  signal rxdata32_l:            arraySLV32(3 downto 0);
  signal rxcharisk4_reg:          arraySLV4(3 downto 0);
--  signal rxchariscomma4:      arraySLV4(3 downto 0);
  signal local_reset_cntr:      arrayUNS8(3 downto 0);
  signal local_reset_rx:        std_logic_vector(3 downto 0);
  signal rxdisperr4_l:          arraySLV4(3 downto 0);
  signal rxnotintable4_l:       arraySLV4(3 downto 0);
  signal rxbufstatus3_l:        arraySLV3(3 downto 0);
  signal rxbyteisaligned_l:     std_logic_vector(3 downto 0);
  signal rxbyterealign_l:       std_logic_vector(3 downto 0);
  signal data_corrupted, data_corrupted_tmp, data_corrupted_l:        std_logic_vector(3 downto 0);
  type data_corrupted_array is array (0 to 3) of std_logic_vector(8 downto 0);
  signal data_corrupted_a : data_corrupted_array;
  attribute DONT_TOUCH : string;
  attribute MARK_DEBUG : string;
  attribute DONT_TOUCH of cplllock_l, rxfsmresetdone_l, rxresetdone_l, rxdata32_l : signal is "TRUE";
  attribute DONT_TOUCH of rxdisperr4_l, rxnotintable4_l, rxbufstatus3_l, rxbyteisaligned_l, rxbyterealign_l: signal is "TRUE";
  attribute MARK_DEBUG of cplllock_l, rxfsmresetdone_l, rxresetdone_l, rxdata32_l: signal is "TRUE";
  attribute MARK_DEBUG of rxdisperr4_l, rxnotintable4_l, rxbufstatus3_l, rxbyteisaligned_l, rxbyterealign_l: signal is "TRUE";

  signal vio_soft_reset, gt0_gtrxreset_in, gt1_gtrxreset_in, gt0_rxbufreset, gt1_rxbufreset : std_logic;
    
begin

  
  --VIO_GTX: entity work.soft_reset
  --port map (
  --  CLK => sysclk160,
  --  probe_out0(0) => vio_soft_reset,
  --  probe_out1(0) => gt0_rxbufreset,
  --  probe_out2(0) => gt1_rxbufreset,
  --  probe_out3(0) => gt0_gtrxreset_in,
  --  probe_out4(0) => gt1_gtrxreset_in
    
  --);


MUON_GTH_INST: entity work.muon_gth32_init--_v5_init
port map
(
  SYSCLK_IN                       =>      sysclk80,
  SOFT_RESET_RX_IN                =>      soft_reset(0),
  DONT_RESET_ON_DATA_ERROR_IN     =>      '1',
        
    GT0_TX_FSM_RESET_DONE_OUT => open,
    GT0_RX_FSM_RESET_DONE_OUT => rxfsmresetdone(0),
    GT0_DATA_VALID_IN => '1',
 
--    GT0_RX_MMCM_RESET_OUT => GT0_RX_MMCM_RESET_OUT,

    GT1_TX_FSM_RESET_DONE_OUT => open,
    GT1_RX_FSM_RESET_DONE_OUT => rxfsmresetdone(1),
    GT1_DATA_VALID_IN => '1',
--    GT1_RX_MMCM_LOCK_IN => sysclk_pll_locked,
--    GT1_RX_MMCM_RESET_OUT => GT1_RX_MMCM_RESET_OUT,

    GT2_TX_FSM_RESET_DONE_OUT => open,
    GT2_RX_FSM_RESET_DONE_OUT => rxfsmresetdone(2),
    GT2_DATA_VALID_IN => '1',
----    GT2_RX_MMCM_LOCK_IN => sysclk_pll_locked,
----    GT2_RX_MMCM_RESET_OUT => GT2_RX_MMCM_RESET_OUT,

    GT3_TX_FSM_RESET_DONE_OUT => open,
    GT3_RX_FSM_RESET_DONE_OUT => rxfsmresetdone(3),
    GT3_DATA_VALID_IN => '1',
----    GT3_RX_MMCM_LOCK_IN => sysclk_pll_locked,
----    GT3_RX_MMCM_RESET_OUT => GT3_RX_MMCM_RESET_OUT,


    --_________________________________________________________________________
    --GT0  (X0Y0)
    --____________________________CHANNEL PORTS________________________________
    --------------------------------- CPLL Ports -------------------------------
        gt0_cpllfbclklost_out           =>      open,
        gt0_cplllock_out                =>      cplllock(0),
        gt0_cplllockdetclk_in           =>      sysclk40,
        gt0_cpllpd_in                   =>      '0',--cpllpd(0),
        gt0_cpllreset_in                =>      '0',
    -------------------------- Channel - Clocking Ports ------------------------
        gt0_gtrefclk0_in                =>      '0',
        gt0_gtrefclk1_in                =>      gtrefclk,
    ---------------------------- Channel - DRP Ports  --------------------------
        gt0_drpaddr_in                  =>      "000000000",
        gt0_drpclk_in                   =>      sysclk80,
        gt0_drpdi_in                    =>      x"0000",
        gt0_drpdo_out                   =>      open,
        gt0_drpen_in                    =>      '0',
        gt0_drprdy_out                  =>      open,
        gt0_drpwe_in                    =>      '0',   
    --------------------- RX Initialization and Reset Ports --------------------
        gt0_eyescanreset_in             =>      '0',
        gt0_rxuserrdy_in                =>      sysclk_pll_locked,
    -------------------------- RX Margin Analysis Ports ------------------------
        gt0_eyescandataerror_out        =>      open,
        gt0_eyescantrigger_in           =>      '0',
    ------------------- Receive Ports - Digital Monitor Ports ------------------
        gt0_dmonitorout_out             =>      open,
    ------------------ Receive Ports - FPGA RX Interface Ports -----------------
        gt0_rxusrclk_in                 =>      rxoutclk_out_buf(0),
        gt0_rxusrclk2_in                =>      rxoutclk_out_buf(0),
    ------------------ Receive Ports - FPGA RX interface Ports -----------------
        gt0_rxdata_out                  =>      rxdata32_out(0),
    ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
        gt0_rxdisperr_out               =>      rxdisperr4(0),
        gt0_rxnotintable_out            =>      rxnotintable4(0),
    ------------------------ Receive Ports - RX AFE Ports ----------------------
        gt0_gthrxn_in                   =>      gtrxn(0),
    ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
        gt0_rxbufreset_in               =>      gt0_rxbufreset,
        gt0_rxbufstatus_out             =>      open,--rxbufstatus3(0),
        --gt0_rxphmonitor_out             =>      open,--
        --gt0_rxmonitorsel_in             =>      (others => '0'),
        --gt0_gtrxreset_in                 =>      '0',
--      gt0_rxphslipmonitor_out         =>      open,--
    -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
        gt0_rxbyteisaligned_out         =>      rxbyteisaligned(0),
        gt0_rxbyterealign_out           =>      rxbyterealign(0),
    --------------------- Receive Ports - RX Equalizer Ports -------------------
        gt0_rxmonitorout_out            =>      open,
        gt0_rxmonitorsel_in             =>      "00",
    --------------- Receive Ports - RX Fabric Output Control Ports -------------
        gt0_rxoutclk_out                =>      rxoutclk_out(0),
    ------------- Receive Ports - RX InitializGGation and Reset Ports ------------
        gt0_gtrxreset_in                =>      gt0_gtrxreset_in,--'0',
    ----------------- Receive Ports - RX Polarity Control Ports ----------------
        gt0_rxpolarity_in               =>      rxpolarity(0),
    ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
        gt0_rxchariscomma_out           =>      rxchariscomma4(0),
        gt0_rxcharisk_out               =>      rxcharisk4_out(0),
    ------------------------ Receive Ports -RX AFE Ports -----------------------
        gt0_gthrxp_in                   =>      gtrxp(0),
    -------------- Receive Ports -RX Initialization and Reset Ports ------------
        gt0_rxresetdone_out             =>      rxresetdone(0),
    --------------------- TX Initialization and Reset Ports --------------------
        gt0_gttxreset_in                =>      '0',

    --GT1  (X0Y1)
    --____________________________CHANNEL PORTS________________________________
    --------------------------------- CPLL Ports -------------------------------
        gt1_cpllfbclklost_out           =>      open,
        gt1_cplllock_out                =>      cplllock(1),
        gt1_cplllockdetclk_in           =>      sysclk40,
        gt1_cpllpd_in                   =>      '0',--cpllpd(1),
        gt1_cpllreset_in                =>      '0',
    -------------------------- Channel - Clocking Ports ------------------------
        gt1_gtrefclk0_in                =>      '0',
        gt1_gtrefclk1_in                =>      gtrefclk,--'0',
    ---------------------------- Channel - DRP Ports  --------------------------
        gt1_drpaddr_in                  =>      "000000000",
        gt1_drpclk_in                   =>      sysclk80,
        gt1_drpdi_in                    =>      x"0000",
        gt1_drpdo_out                   =>      open,
        gt1_drpen_in                    =>      '0', 
        gt1_drprdy_out                  =>      open,
        gt1_drpwe_in                    =>      '0', 
    --------------------- RX Initialization and Reset Ports --------------------
        gt1_eyescanreset_in             =>      '0',
        gt1_rxuserrdy_in                =>      sysclk_pll_locked,
    -------------------------- RX Margin Analysis Ports ------------------------
        gt1_eyescandataerror_out        =>      open,
        gt1_eyescantrigger_in           =>      '0',
    ------------------- Receive Ports - Digital Monitor Ports ------------------
        gt1_dmonitorout_out             =>      open,
    ------------------ Receive Ports - FPGA RX Interface Ports -----------------
        gt1_rxusrclk_in                 =>      rxoutclk_out_buf(0),
        gt1_rxusrclk2_in                =>      rxoutclk_out_buf(0),
    ------------------ Receive Ports - FPGA RX interface Ports -----------------
        gt1_rxdata_out                  =>      rxdata32_out(1),
    ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
        gt1_rxdisperr_out               =>      rxdisperr4(1),
        gt1_rxnotintable_out            =>      rxnotintable4(1),
    ------------------------ Receive Ports - RX AFE Ports ----------------------
        gt1_gthrxn_in                   =>      gtrxn(1),
    ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
        gt1_rxbufreset_in               =>      gt1_rxbufreset,
        gt1_rxbufstatus_out             =>      open,--rxbufstatus3(1),
--        gt1_rxphmonitor_out             =>      open,--
        --gt1_rxphslipmonitor_out         =>      open,--
    -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
        gt1_rxbyteisaligned_out         =>      rxbyteisaligned(1),
        gt1_rxbyterealign_out           =>      rxbyterealign(1),
    --------------------- Receive Ports - RX Equalizer Ports -------------------
        gt1_rxmonitorout_out            =>      open,
        gt1_rxmonitorsel_in             =>      "00",
    --------------- Receive Ports - RX Fabric Output Control Ports -------------
        gt1_rxoutclk_out                =>      rxoutclk_out(1),
    ------------- Receive Ports - RX Initialization and Reset Ports ------------
        gt1_gtrxreset_in                =>      gt1_gtrxreset_in,--'0',
    ----------------- Receive Ports - RX Polarity Control Ports ----------------
        gt1_rxpolarity_in               =>      rxpolarity(1),
    ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
        gt1_rxchariscomma_out           =>      rxchariscomma4(1),
        gt1_rxcharisk_out               =>      rxcharisk4_out(1),
    ------------------------ Receive Ports -RX AFE Ports -----------------------
        gt1_gthrxp_in                   =>      gtrxp(1),
    -------------- Receive Ports -RX Initialization and Reset Ports ------------
        gt1_rxresetdone_out             =>      rxresetdone(1),
    --------------------- TX Initialization and Reset Ports --------------------
        gt1_gttxreset_in                =>      '0',

    --GT2  (X0Y2)
    --____________________________CHANNEL PORTS________________________________
    --------------------------------- CPLL Ports -------------------------------
        gt2_cpllfbclklost_out           =>      open,
        gt2_cplllock_out                =>      cplllock(2),
        gt2_cplllockdetclk_in           =>      sysclk40,
        gt2_cpllpd_in                   =>      '0',
        gt2_cpllreset_in                =>      '0',
    ------------------------ Channel - Clocking Ports ------------------------
        gt2_gtrefclk0_in                =>      '0',
        gt2_gtrefclk1_in                =>      gtrefclk,--'0',
    ---------------------------- Channel - DRP Ports  --------------------------
        gt2_drpaddr_in                  =>      "000000000",
        gt2_drpclk_in                   =>      sysclk80,
        gt2_drpdi_in                    =>      x"0000",
        gt2_drpdo_out                   =>      open,
        gt2_drpen_in                    =>      '0', 
        gt2_drprdy_out                  =>      open,
        gt2_drpwe_in                    =>      '0', 
    ------------------- RX Initialization and Reset Ports --------------------
        gt2_eyescanreset_in             =>      '0',
        gt2_rxuserrdy_in                =>      sysclk_pll_locked,
    ------------------------ RX Margin Analysis Ports ------------------------
        gt2_eyescandataerror_out        =>      open,
        gt2_eyescantrigger_in           =>      '0',
    ----------------- Receive Ports - Digital Monitor Ports ------------------
      gt2_dmonitorout_out             =>      open,
  ---------------- Receive Ports - FPGA RX Interface Ports -----------------
      gt2_rxusrclk_in                 =>      rxoutclk_out_buf(0),
      gt2_rxusrclk2_in                =>      rxoutclk_out_buf(0),
  ---------------- Receive Ports - FPGA RX interface Ports -----------------
      gt2_rxdata_out                  =>      rxdata32_out(2),
  ---------------- Receive Ports - RX 8B/10B Decoder Ports -----------------
      gt2_rxdisperr_out               =>      rxdisperr4(2),
      gt2_rxnotintable_out            =>      rxnotintable4(2),
  ---------------------- Receive Ports - RX AFE Ports ----------------------
      gt2_gthrxn_in                   =>      gtrxn(2),
  ----------------- Receive Ports - RX Buffer Bypass Ports -----------------
      gt2_rxbufreset_in               =>      rxbufreset(2),
      gt2_rxbufstatus_out             =>      open,--rxbufstatus3(2),
  ------------ Receive Ports - RX Byte and Word Alignment Ports ------------
      gt2_rxbyteisaligned_out         =>      rxbyteisaligned(2),
      gt2_rxbyterealign_out           =>      rxbyterealign(2),
--      gt2_rxphmonitor_out             =>      open,--
--      gt2_rxphslipmonitor_out         =>      open,-- 
  ------------------- Receive Ports - RX Equalizer Ports -------------------
      gt2_rxmonitorout_out            =>      open,
      gt2_rxmonitorsel_in             =>      "00",
  ------------- Receive Ports - RX Fabric Output Control Ports -------------
      gt2_rxoutclk_out                =>      rxoutclk_out(2),
  ----------- Receive Ports - RX Initialization and Reset Ports ------------
      gt2_gtrxreset_in                =>      '0',
  --------------- Receive Ports - RX Polarity Control Ports ----------------
      gt2_rxpolarity_in               =>      rxpolarity(2),
  ----------------- Receive Ports - RX8B/10B Decoder Ports -----------------
      gt2_rxchariscomma_out           =>      rxchariscomma4(2),
      gt2_rxcharisk_out               =>      rxcharisk4_out(2),
  ---------------------- Receive Ports -RX AFE Ports -----------------------
      gt2_gthrxp_in                   =>      gtrxp(2),
  ------------ Receive Ports -RX Initialization and Reset Ports ------------
      gt2_rxresetdone_out             =>      rxresetdone(2),
  ------------------- TX Initialization and Reset Ports --------------------
      gt2_gttxreset_in                =>      '0',

  --GT3  (X0Y3)
  --____________________________CHANNEL PORTS________________________________
  --------------------------------- CPLL Ports -------------------------------
        gt3_cpllfbclklost_out           =>      open,
        gt3_cplllock_out                =>      cplllock(3),
        gt3_cplllockdetclk_in           =>      sysclk40,
        gt3_cpllpd_in                   =>      '0',--cpllpd(3),
        gt3_cpllreset_in                =>      '0',
  -------------------------- Channel - Clocking Ports ------------------------
        gt3_gtrefclk0_in                =>      '0',
        gt3_gtrefclk1_in                =>      gtrefclk,--'0',
  ---------------------------- Channel - DRP Ports  --------------------------
        gt3_drpaddr_in                  =>      "000000000",
        gt3_drpclk_in                   =>      sysclk80,
        gt3_drpdi_in                    =>      x"0000",
        gt3_drpdo_out                   =>      open,
        gt3_drpen_in                    =>      '0', 
       gt3_drprdy_out                  =>      open,
        gt3_drpwe_in                    =>      '0', 
  --------------------- RX Initialization and Reset Ports --------------------
        gt3_eyescanreset_in             =>      '0',
        gt3_rxuserrdy_in                =>      sysclk_pll_locked,
  -------------------------- RX Margin Analysis Ports ------------------------
        gt3_eyescandataerror_out        =>      open,
        gt3_eyescantrigger_in           =>      '0',
  ------------------- Receive Ports - Digital Monitor Ports ------------------
        gt3_dmonitorout_out             =>      open,
  ------------------ Receive Ports - FPGA RX Interface Ports -----------------
        gt3_rxusrclk_in                 =>      rxoutclk_out_buf(0),
        gt3_rxusrclk2_in                =>      rxoutclk_out_buf(0),
  ------------------ Receive Ports - FPGA RX interface Ports -----------------
        gt3_rxdata_out                  =>      rxdata32_out(3),
  ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
        gt3_rxdisperr_out               =>      rxdisperr4(3),
        gt3_rxnotintable_out            =>      rxnotintable4(3),
  ------------------------ Receive Ports - RX AFE Ports ----------------------
        gt3_gthrxn_in                   =>      gtrxn(3),
  ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
        gt3_rxbufreset_in               =>      rxbufreset(3),
        gt3_rxbufstatus_out             =>      open,--rxbufstatus3(3),
  -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
        gt3_rxbyteisaligned_out         =>      rxbyteisaligned(3),
        gt3_rxbyterealign_out           =>      rxbyterealign(3),
  --------------------- Receive Ports - RX Equalizer Ports -------------------
        gt3_rxmonitorout_out            =>      open,
        gt3_rxmonitorsel_in             =>      "00",
      --gt3_rxphmonitor_out             =>      open,--
      --gt3_rxphslipmonitor_out         =>      open,--
  --------------- Receive Ports - RX Fabric Output Control Ports -------------
        gt3_rxoutclk_out                =>      rxoutclk_out(3),
  ------------- Receive Ports - RX Initialization and Reset Ports ------------
        gt3_gtrxreset_in                =>      '0',
  ----------------- Receive Ports - RX Polarity Control Ports ----------------
        gt3_rxpolarity_in               =>      rxpolarity(3),
  ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
        gt3_rxchariscomma_out           =>      rxchariscomma4(3),
        gt3_rxcharisk_out               =>      rxcharisk4_out(3),
  ------------------------ Receive Ports -RX AFE Ports -----------------------
        gt3_gthrxp_in                   =>      gtrxp(3),
  -------------- Receive Ports -RX Initialization and Reset Ports ------------
        gt3_rxresetdone_out             =>      rxresetdone(3),
  --------------------- TX Initialization and Reset Ports --------------------
        gt3_gttxreset_in                =>      '0',


    --____________________________COMMON PORTS________________________________
     GT0_QPLLOUTCLK_IN  => gt0_qplloutclk_i,
     GT0_QPLLOUTREFCLK_IN => gt0_qplloutrefclk_i
    );

  --to keep generics for gth32
 -- COMMON0_INST: entity work.muon_gth32_common 
 -- generic map
 -- (
 --  WRAPPER_SIM_GTRESET_SPEEDUP => "TRUE",--EXAMPLE_SIM_GTRESET_SPEEDUP,
 --  SIM_QPLLREFCLK_SEL => "001"
 -- )
 --port map
 --  (
 --   QPLLREFCLKSEL_IN    => "001",
 --   GTREFCLK0_IN      => '0',
 --   GTREFCLK1_IN      => gtrefclk,
 --   QPLLLOCK_OUT =>  open,
 --   QPLLLOCKDETCLK_IN => sysclk80,
 --   QPLLOUTCLK_OUT => gt0_qplloutclk_i,
 --   QPLLOUTREFCLK_OUT => gt0_qplloutrefclk_i,
 --   QPLLREFCLKLOST_OUT => open,    
 --   QPLLRESET_IN => '1'

 -- );

  GTH_RX_BUFH: BUFH
    port map (
      I => rxoutclk_out(0),
      O => rxoutclk_out_buf(0)
      ); 

  INDIVIDUAL_INPUTS: for i in 0 to 3 generate

    --GTH_RX_BUFH: BUFH
    --  port map (
    --    I => rxoutclk_out(i),
    --    O => rxoutclk_out_buf(i)
    --    );

    ENABLE_DEBUG : if DEBUG = 1 generate
      ONLY_DATA_LINKS: if i = 0 or i = 3 generate
        muon_test_ila_RX: entity work.muon_test_ila
          port map (
            clk    => rxoutclk_out_buf(0),--i
            probe0 => mem_data_in(i)(35 downto 0),
            probe1(0) => rxresetdone(i),
            probe2 => std_logic_vector(write_addr(i)),
            probe3 => rxdisperr4(i)
            );
        muon_test_ila_RX_OUT: entity work.muon_test_ila
          port map (
            clk    => sysclk160,
            probe0 => mem_data_out(i)(35 downto 0),
            probe1(0) => local_reset_160(i),
            probe2 => std_logic_vector(read_addr(i)),
            probe3 => std_logic_vector(delay_cntr(i))
            );
      end generate ONLY_DATA_LINKS;
    end generate ENABLE_DEBUG;

    UPDATE_RESET_CNTR:process(rxoutclk_out_buf(0))
    begin
      if rising_edge(rxoutclk_out_buf(0)) then
        if rxresetdone(i) = '0' then
          local_reset_cntr(i) <= x"00";
          local_reset_rx(i) <= '1';
        elsif local_reset_cntr(i) < x"fd" and rxresetdone(i) = '1' then
          local_reset_cntr(i) <= local_reset_cntr(i) + 1;
          local_reset_rx(i) <= '1';
        else
          local_reset_cntr(i) <= local_reset_cntr(i);
          local_reset_rx(i) <= '0';
        end if;
      end if;
    end process UPDATE_RESET_CNTR;
        
    WRITE_ADDR_PROC:process(rxoutclk_out_buf(0))--i
     begin
       if rising_edge(rxoutclk_out_buf(0)) then --i
         --if rxbufreset(i) = '1' then
         -- if !! change reset since maybe the clock is crap after soft reset !!! soft_reset(0) = '1' then --soft_reset must be long. - at least
         -- if cplllock(i) = '0' then
         --         if rxfsmresetdone(i) = '0' then
         if local_reset_rx(i) = '1' then  
                                     --33*3.125ns. It is not an internal
                                     --pulse, it is set by software. 
           write_addr(i) <= (others => '0');
         else
           write_addr(i) <= write_addr(i) + 1;
         end if;
       end if;
     end process WRITE_ADDR_PROC;

     DELAY_READ: process(sysclk160)
     begin
       if rising_edge(sysclk160) then
         if local_reset_160(i) = '1'  then
           delay_cntr(i) <= (others => '0');
           read_addr(i) <= (others => '0');
         elsif delay_cntr(i) < unsigned(DELAY(i)) + 1 then
           delay_cntr(i) <= delay_cntr(i) + 1;
           read_addr(i) <= (others => '0');
         else
           delay_cntr(i) <= delay_cntr(i);
           read_addr(i) <= read_addr(i) + 1;
         end if;
       end if;
     end process DELAY_READ;

--     DATA_CORRUPTED_PROC: process(rxoutclk_out_buf(0))
--     begin
--       if rising_edge(rxoutclk_out_buf(0)) then
--         if rxresetdone(i) = '0' or rxfsmresetdone(i) = '0' or rxbyteisaligned(i) = '0' or
--           rxbyterealign(i) = '1' or rxnotintable4(i) /=x"0" or rxdisperr4 /= x"0" or
--           cplllock(i) = '0' then
--           data_corrupted(i) <= '1';
--         elsif mem_data_in(7 downto 0) = x"bc" then
--           data_corrupted(i) <= '0';
--         end if;
--       end if;
--     end process DATA_CORRUPTED_PROC;
     data_corrupted(i) <= '1' when rxresetdone(i) = '0' or rxfsmresetdone(i) = '0' or
                       rxbyteisaligned(i) = '0' or rxbyterealign(i) = '1'
                       or rxnotintable4(i) /=x"0" or rxdisperr4(i) /= x"0" or cplllock(i) = '0'
     else '0';
     --mem_data_in(i) <= data_corrupted(i) & rxbufreset(i) & rxcharisk4_out(i) & rxdata32_out(i);
     mem_data_in(i) <= data_corrupted(i) & local_reset_rx(i) & rxcharisk4_out(i) & rxdata32_out(i);
     
     muon_mem: entity work.muon_mem
       port map (
         clka  => rxoutclk_out_buf(0),--i
         wea(0)   => '1',
         addra => std_logic_vector(write_addr(i)),
         dina  => mem_data_in(i),
         clkb  => sysclk160,
         addrb => std_logic_vector(read_addr(i)),
         doutb => mem_data_out(i));


     RELAX_TIMING: process(sysclk160)
     begin
       if rising_edge(sysclk160) then
         rxdata32_reg(i)    <= mem_data_out(i)(31 downto 0);
         rxcharisk4_reg(i)  <=  mem_data_out(i)(35 downto 32);
         rxdata32(i) <= rxdata32_reg(i);
         rxcharisk4(i) <= rxcharisk4_reg(i);  
       end if;
     end process RELAX_TIMING;
     
     local_reset_160(i) <= mem_data_out(i)(36);
     
     SET_DATA_CORUPTED_TMP: process(sysclk160)
     begin
       if rising_edge(sysclk160) then
         if mem_data_out(i)(7 downto 0) = x"bc" and mem_data_out(i)(37) = '0' then
           data_corrupted_tmp(i) <= '0';
         elsif mem_data_out(i)(37) = '1' then
           data_corrupted_tmp(i) <= '1';
         else
           data_corrupted_tmp(i) <= data_corrupted_tmp(i);
         end if;
       end if;
     end process SET_DATA_CORUPTED_TMP;

     SET_DATA_CORUPTED_4BC: process(sysclk160)
     begin
       if rising_edge(sysclk160) then
         if mem_data_out(i)(7 downto 0) = x"bc" and data_corrupted_tmp(i) = '1'  then
           data_corrupted_a(i)(0) <= '1';
         elsif mem_data_out(i)(7 downto 0) = x"bc" and data_corrupted_tmp(i)  = '0' then
           data_corrupted_a(i)(0) <= '0';
         else
           data_corrupted_a(i)(0) <= data_corrupted_a(i)(0);
         end if;
       end if;
     end process SET_DATA_CORUPTED_4BC;

     DELAY_ERROR_FLAG: process(sysclk160) --to synchronize with 128bit data
                                          --after muon DataSorter
     begin
       if rising_edge(sysclk160) then
         for pipe in 1 to 8 loop
           data_corrupted_a(i)(pipe) <= data_corrupted_a(i)(pipe-1);
         end loop;
       end if;
     end process DELAY_ERROR_FLAG;
     
     gth_error_out(i)(3 downto 0) <= (others => data_corrupted_a(i)(6)); 
     
  end generate INDIVIDUAL_INPUTS;

-------------------------------------------------------------------------------
--debug
-------------------------------------------------------------------------------
    --cplllock:           out std_logic_vector(3 downto 0);
    --rxfsmresetdone:     out std_logic_vector(3 downto 0);
    --rxresetdone:        out std_logic_vector(3 downto 0);
    --rxdata32:           out arraySLV32(3 downto 0);
    --rxcharisk4:         out arraySLV4(3 downto 0);
    --rxchariscomma4:     out arraySLV4(3 downto 0);
    --rxdisperr4:         out arraySLV4(3 downto 0);
    --rxnotintable4:      out arraySLV4(3 downto 0);
    --rxbufstatus3:       out arraySLV3(3 downto 0);
    --rxbyteisaligned:    out std_logic_vector(3 downto 0);
    --rxbyterealign:      out std_logic_vector(3 downto 0);

  SET_ERROR_BITS: process(sysclk160)
  begin
    for i in 0 to 3 loop
      rxdisperr1(i) <= '1' when rxdisperr4(i) /= x"0" else '0';
      rxnotintable1(i) <= '1' when rxnotintable4(i) /= x"0" else '0';
      rxbufstatus1(i) <= '0';-- when rxbufstatus3(i) /= "000" else '0';
      rxbufstatus3(i) <= "000";
    end loop;
  end process SET_ERROR_BITS;

--cplllock(x) <= '1';
--rxresetdone(x) <= '1';
--rxfsmresetdone(x) <= '1';
--rxbyteisaligned(x) <= '1';
--rxbyterealign(x) <= '0';
--rxnotintable4(x) <= '0';
--rxdisperr4(x) <= '0';


  
          
cplllock_l         <= cplllock;
rxfsmresetdone_l   <= rxfsmresetdone;
rxresetdone_l      <= rxresetdone;
rxdisperr4_l(0)    <= rxdisperr4(0);
rxdisperr4_l(1)    <= rxdisperr4(1);
rxdisperr4_l(2)    <= rxdisperr4(2);
rxdisperr4_l(3)    <= rxdisperr4(3);
rxnotintable4_l(0) <= rxnotintable4(0);
rxnotintable4_l(1) <= rxnotintable4(1);
rxnotintable4_l(2) <= rxnotintable4(2);
rxnotintable4_l(3) <= rxnotintable4(3);
rxbufstatus3_l(0)  <= rxbufstatus3(0);
rxbufstatus3_l(1)  <= rxbufstatus3(1);
rxbufstatus3_l(2)  <= rxbufstatus3(2);
rxbufstatus3_l(3)  <= rxbufstatus3(3);
rxbyteisaligned_l  <= rxbyteisaligned;
rxbyterealign_l    <= rxbyterealign;
  
--ILA_GTX: entity work.muon_link_ila
--  port map (
--    CLK => sysclk160,
--    probe0 => cplllock_l,
--    probe1 => rxfsmresetdone_l,
--    probe2 => rxresetdone_l,
--    probe3 => local_reset_rx,--data_corrupted_a(3)(8),--rxdisperr4_l(0),
--    probe4 => local_reset_160,--rxdisperr4_l(1),
--    probe5 => std_logic_vector(write_addr(0)(3 downto 0)),--rxcharisk4(0),--rxdisperr4_l(2),
--    probe6 => std_logic_vector(read_addr(0)(3 downto 0)),--rxdisperr4_l(3),
--    probe7 => std_logic_vector(read_addr(1)(3 downto 0)),
--    probe8 => std_logic_vector(write_addr(3)(3 downto 0)),--rxnotintable4_l(1),
--    probe9 => std_logic_vector(read_addr(3)(3 downto 0)),--rxnotintable4_l(2),
--    probe10 => std_logic_vector(delay_cntr(0)),--rxnotintable4_l(3),
--    probe11 => std_logic_vector(delay_cntr(1)(2 downto 0)),--rxbufstatus3_l(0),
--    probe12 => std_logic_vector(delay_cntr(2)(2 downto 0)),--rxbufstatus3_l(1),
--    probe13 => std_logic_vector(delay_cntr(3)(2 downto 0)),--rxbufstatus3_l(2),
--    probe14 => "000",--std_logic_vector(write_addr(3)(2 downto 0)),
--    probe15 => mem_data_in(0)(3 downto 0),
--    probe16 => mem_data_in(3)(3 downto 0),
--    probe17 => rxdata32(0),--_out(0),
--    probe18 =>  std_logic_vector(local_reset_cntr(3)) & std_logic_vector(local_reset_cntr(2)) & std_logic_vector(local_reset_cntr(1)) & std_logic_vector(local_reset_cntr(0)),
--    probe19 => rxdata32(2),--_out(2),
--    probe20 => rxdata32(3)--_out(3)
  
--  );


end Behavioral;
