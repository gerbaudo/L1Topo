----------------------------------------------------------------------------------
-- Company:     Johannes Gutenberg-Universitaet Mainz
-- Engineer:    Christian Kahra
-- 
-- Create Date: 02/08/2015 01:12:09 PM
-- Design Name: 
-- Module Name: receiver_gth16_v3_4 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.NUMERIC_STD.ALL;

--library UNISIM;
--use UNISIM.VComponents.all;

use work.l1topo_package.all;
use work.L1TopoGTConfiguration.all;



entity receiver_gth16_v3_4 is port(

    sysclk40:              in std_logic;
    sysclk80:              in std_logic;
    sysclk320:             in std_logic;
	sysclk_pll_locked:     in std_logic; 
	
--	crystalclk62_5:        in std_logic;
--	crystalclk_pll_locked: in std_logic;
	
    gtrefclk:              in  std_logic_vector(QuadHigh downto QuadLow);
    gt_rxp, gt_rxn:        in  std_logic_vector(MGTHigh downto MGTLow);                                       
    
    rxdata32:              out arraySLV32(MGTHigh downto MGTLow);                                       
    rxcharisk4:            out arraySLV4(MGTHigh downto MGTLow);
	
	QuadControl:           in  arraySLV32(QuadHigh downto QuadLow);
	QuadStatus:            out arraySLV32(QuadHigh downto QuadLow);
	ChannelControl:        in  arraySLV32(MGTHigh downto MGTLow);
	ChannelStatus20bit:    out arraySLV20(MGTHigh downto MGTLow)
	 
);

end receiver_gth16_v3_4;

architecture Behavioral of receiver_gth16_v3_4 is

    signal rxdata16:        arraySLV16(MGTHigh downto MGTLow);
    signal rxdata16_reg:    arraySLV16(MGTHigh downto MGTLow);
    signal rxdata32_reg:    arraySLV32(MGTHigh downto MGTLow);
    signal rxcharisk2:      arraySLV2(MGTHigh downto MGTLow);
    signal rxcharisk2_reg:  arraySLV2(MGTHigh downto MGTLow);
    signal rxcharisk4_reg:  arraySLV4(MGTHigh downto MGTLow);
    
    signal rxchariscomma2:  arraySLV2(MGTHigh downto MGTLow);
    signal toggle:          std_logic_vector(MGTHigh downto MGTLow);

begin


GTH16_Quad_Gen: for q in QuadHigh downto QuadLow generate begin

GTH16_Quad: entity work.gth16cpll_Quad
generic map(
    debugQuad => "00000000000000000000",
    quad => q
)
port map(
    sysclk40                => sysclk40,
    sysclk80                => sysclk80,
    sysclk320               => sysclk320,
    sysclk_pll_locked       => sysclk_pll_locked,
--    crystalclk62_5          => crystalclk62_5,
--    crystalclk_pll_locked   => crystalclk_pll_locked,
    gtrefclk                => gtrefclk(q),
    gt_rxp                  => gt_rxp(4*q+3 downto 4*q),
    gt_rxn                  => gt_rxn(4*q+3 downto 4*q),
    QuadControl             => QuadControl(q),
    QuadStatus              => QuadStatus(q), 
    ChannelControl          => ChannelControl(4*q+3 downto 4*q),
    ChannelStatus20bit      => ChannelStatus20bit(4*q+3 downto 4*q),
    rxdata                  => rxdata16(4*q+3 downto 4*q),
    rxcharisk               => rxcharisk2(4*q+3 downto 4*q),
    rxchariscomma           => rxchariscomma2(4*q+3 downto 4*q)
);

end generate;


process(sysclk320) begin
    if rising_edge(sysclk320) then
        for ch in MGTHigh downto MGTLow loop
            
            if rxchariscomma2(ch)(0)='1' then toggle(ch) <= '0';
            else toggle(ch) <= not toggle(ch);
            end if;
            
            rxdata16_reg(ch)    <= rxdata16(ch);
            rxcharisk2_reg(ch)  <= rxcharisk2(ch);
            
            if toggle(ch)='1' then
                rxdata32_reg(ch)    <= rxdata16_reg(ch)     & rxdata16(ch);
                rxcharisk4_reg(ch)  <= rxcharisk2_reg(ch)   & rxcharisk2(ch);
            end if;
            
        end loop;
    end if;
end process;


rxdata32    <= rxdata32_reg;
rxcharisk4  <= rxcharisk4_reg;



end Behavioral;
