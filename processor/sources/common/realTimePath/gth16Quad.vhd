----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Christian Kahra, Marek Palka
-- 
-- Create Date: 05/23/2015 09:28:44 PM
-- Design Name: 
-- Module Name: gth16Quad - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.NUMERIC_STD.ALL;

--library UNISIM;
--use UNISIM.VComponents.all;

use work.l1topo_package.all;



entity gth16Quad is port(
    sysclk40:               in  std_logic;
    sysclk80:               in  std_logic;
    sysclk160:              in  std_logic;
    sysclk320:              in  std_logic;
    sysclk_pll_locked:      in  std_logic;
    gtrefclk:               in  std_logic;
    gt_rxp, gt_rxn:         in  std_logic_vector( 3 downto 0);
    
    cpllpd:             in  std_logic_vector(3 downto 0);
    soft_reset:         in  std_logic_vector(3 downto 0);
    rxbufreset:         in  std_logic_vector(3 downto 0);
    rxpolarity:         in  std_logic_vector(3 downto 0);
    
    gt_drp_addr:        in  std_logic_vector(8 downto 0);
    gt_drp_dataIn:      in  std_logic_vector(15 downto 0);
    gt_drp_dataOut:     out arraySLV16(3 downto 0);
    gt_drp_enable:      in  std_logic_vector(3 downto 0);
    gt_drp_ready:       out std_logic_vector(3 downto 0);
    gt_drp_writeEnable: in  std_logic_vector(3 downto 0);
    
    rxdata32:           out arraySLV32(3 downto 0);
    rxcharisk4:         out arraySLV4(3 downto 0);
    rxchariscomma4:     out arraySLV4(3 downto 0);
    rxdisperr4:         out arraySLV4(3 downto 0);
    rxdisperr1:         out std_logic_vector(3 downto 0); --new
    rxnotintable4:      out arraySLV4(3 downto 0);
    rxnotintable1:      out std_logic_vector(3 downto 0); --new
    rxbufstatus3:       out arraySLV3(3 downto 0);
    rxbufstatus1:       out std_logic_vector(3 downto 0); --new
    rxbyteisaligned:    out std_logic_vector(3 downto 0);
    rxbyterealign:      out std_logic_vector(3 downto 0);
        
    cplllock:           out std_logic_vector(3 downto 0);
    rxfsmresetdone:     out std_logic_vector(3 downto 0);
    rxresetdone:        out std_logic_vector(3 downto 0)
);

end gth16Quad;



architecture Behavioral of gth16Quad is

    signal rxdata16:                arraySLV16(3 downto 0);
    signal rxcharisk2:              arraySLV2( 3 downto 0);
    signal rxchariscomma2:          arraySLV2( 3 downto 0);
    signal rxnotintable2:           arraySLV2( 3 downto 0);
    signal rxdisperr2:              arraySLV2( 3 downto 0);
    

    signal rxdata16_buffer, rxdata16_buffer2:          arraySLV16(3 downto 0);
    signal rxcharisk2_buffer, rxcharisk2_buffer2:        arraySLV2( 3 downto 0);
    signal rxchariscomma2_buffer, rxchariscomma2_buffer2:    arraySLV2( 3 downto 0);
    signal rxnotintable2_buffer, rxnotintable2_buffer2:     arraySLV2( 3 downto 0);
    signal rxdisperr2_buffer, rxdisperr2_buffer2:        arraySLV2( 3 downto 0);

    signal rxdata32_reg, rxdata32_reg2:          arraySLV32(3 downto 0);
    signal rxcharisk4_reg, rxcharisk4_reg2:        arraySLV4( 3 downto 0);
    signal rxchariscomma4_reg, rxchariscomma4_reg2:    arraySLV4( 3 downto 0);
    signal rxnotintable4_reg, rxnotintable4_reg2:     arraySLV4( 3 downto 0);
    signal rxdisperr4_reg, rxdisperr4_reg2:        arraySLV4( 3 downto 0);

    signal rxbufstatus3_i:       arraySLV3(3 downto 0);
    signal rxbyteisaligned_i:    std_logic_vector(3 downto 0);
    signal rxbyterealign_i:      std_logic_vector(3 downto 0);
    signal rxresetdone_i:        std_logic_vector(3 downto 0);

    signal rxbufstatus3_buffer, rxbufstatus3_buffer2:     arraySLV3(3 downto 0);
    signal rxbyteisaligned_buffer, rxbyteisaligned_buffer2:  std_logic_vector(3 downto 0);
    signal rxbyterealign_buffer, rxbyterealign_buffer2:    std_logic_vector(3 downto 0);
    signal rxresetdone_buffer, rxresetdone_buffer2:      std_logic_vector(3 downto 0);
    
    signal rxbufstatus3_reg:        arraySLV3(3 downto 0);
    signal rxbyteisaligned_reg:     std_logic_vector(3 downto 0);
    signal rxbyterealign_reg:       std_logic_vector(3 downto 0);
    signal rxresetdone_reg:         std_logic_vector(3 downto 0);

    signal soft_reset_ored:         std_logic;

begin


    soft_reset_ored <= soft_reset(0) or soft_reset(1) or soft_reset(2) or soft_reset(3);


gtwizard_gth16Quad_init: entity work.gtwizard_gth16Quad_init
port map
(
        SYSCLK_IN                       => sysclk80,
        SOFT_RESET_RX_IN                => soft_reset_ored,
        DONT_RESET_ON_DATA_ERROR_IN     => '1',
        
        GT0_TX_FSM_RESET_DONE_OUT       => open,
        GT0_RX_FSM_RESET_DONE_OUT       => rxfsmresetdone(0),
        GT0_DATA_VALID_IN               => '1',
        
        GT1_TX_FSM_RESET_DONE_OUT       => open,
        GT1_RX_FSM_RESET_DONE_OUT       => rxfsmresetdone(1),
        GT1_DATA_VALID_IN               => '1',
        
        GT2_TX_FSM_RESET_DONE_OUT       => open,
        GT2_RX_FSM_RESET_DONE_OUT       => rxfsmresetdone(2),
        GT2_DATA_VALID_IN               => '1',
        
        GT3_TX_FSM_RESET_DONE_OUT       => open,
        GT3_RX_FSM_RESET_DONE_OUT       => rxfsmresetdone(3),
        GT3_DATA_VALID_IN               => '1',

    --_________________________________________________________________________
    --GT0  (X0Y0)
    --____________________________CHANNEL PORTS________________________________
    --------------------------------- CPLL Ports -------------------------------
        gt0_cpllfbclklost_out           =>      open, --cpllfbclklost(0),
        gt0_cplllock_out                =>      cplllock(0),
        gt0_cplllockdetclk_in           =>      sysclk40,
        gt0_cpllpd_in                   =>      cpllpd(0),
        gt0_cpllreset_in                =>      '0', --cpllreset(0),
    -------------------------- Channel - Clocking Ports ------------------------
        gt0_gtrefclk0_in                =>      '0',
        gt0_gtrefclk1_in                =>      gtrefclk,
    ---------------------------- Channel - DRP Ports  --------------------------
        gt0_drpaddr_in                  =>      "000000000",
        gt0_drpclk_in                   =>      sysclk80,
        gt0_drpdi_in                    =>      x"0000",
        gt0_drpdo_out                   =>      open,
        gt0_drpen_in                    =>      '0',
        gt0_drprdy_out                  =>      open,
        gt0_drpwe_in                    =>      '0',
    --------------------- RX Initialization and Reset Ports --------------------
        gt0_eyescanreset_in             =>      '0',
        gt0_rxuserrdy_in                =>      sysclk_pll_locked,
    -------------------------- RX Margin Analysis Ports ------------------------
        gt0_eyescandataerror_out        =>      open,
        gt0_eyescantrigger_in           =>      '0',
    ------------------- Receive Ports - Digital Monitor Ports ------------------
        gt0_dmonitorout_out             =>      open,
    ------------------ Receive Ports - FPGA RX Interface Ports -----------------
        gt0_rxusrclk_in                 =>      sysclk320,
        gt0_rxusrclk2_in                =>      sysclk320,
    ------------------ Receive Ports - FPGA RX interface Ports -----------------
        gt0_rxdata_out                  =>      rxdata16(0),
    ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
        gt0_rxdisperr_out               =>      rxdisperr2(0),
        gt0_rxnotintable_out            =>      rxnotintable2(0),
    ------------------------ Receive Ports - RX AFE Ports ----------------------
        gt0_gthrxn_in                   =>      gt_rxn(0),
    ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
        gt0_rxbufreset_in               =>      rxbufreset(0),
        gt0_rxbufstatus_out             =>      rxbufstatus3_i(0),
    -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
        gt0_rxbyteisaligned_out         =>      rxbyteisaligned_i(0),
        gt0_rxbyterealign_out           =>      rxbyterealign_i(0),
    --------------------- Receive Ports - RX Equalizer Ports -------------------
        gt0_rxmonitorout_out            =>      open,
        gt0_rxmonitorsel_in             =>      "00",
    --------------- Receive Ports - RX Fabric Output Control Ports -------------
        gt0_rxoutclk_out                =>      open, --rxoutclk(0),
    ------------- Receive Ports - RX Initialization and Reset Ports ------------
        gt0_gtrxreset_in                =>      '0',
    ----------------- Receive Ports - RX Polarity Control Ports ----------------
        gt0_rxpolarity_in               =>      rxpolarity(0),
    ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
        gt0_rxchariscomma_out           =>      rxchariscomma2(0),
        gt0_rxcharisk_out               =>      rxcharisk2(0),
    ------------------------ Receive Ports -RX AFE Ports -----------------------
        gt0_gthrxp_in                   =>      gt_rxp(0),
    -------------- Receive Ports -RX Initialization and Reset Ports ------------
        gt0_rxresetdone_out             =>      rxresetdone_i(0),
    --------------------- TX Initialization and Reset Ports --------------------
        gt0_gttxreset_in                =>      '0',

    --GT1  (X0Y1)
    --____________________________CHANNEL PORTS________________________________
    --------------------------------- CPLL Ports -------------------------------
        gt1_cpllfbclklost_out           =>      open, --cpllfbclklost(1),
        gt1_cplllock_out                =>      cplllock(1),
        gt1_cplllockdetclk_in           =>      sysclk40,
        gt1_cpllpd_in                   =>      cpllpd(1),
        gt1_cpllreset_in                =>      '0', ---cpllreset(1),
    -------------------------- Channel - Clocking Ports ------------------------
        gt1_gtrefclk0_in                =>      '0',
        gt1_gtrefclk1_in                =>      gtrefclk,
    ---------------------------- Channel - DRP Ports  --------------------------
        gt1_drpaddr_in                  =>      "000000000",
        gt1_drpclk_in                   =>      sysclk80,
        gt1_drpdi_in                    =>      x"0000",
        gt1_drpdo_out                   =>      open,
        gt1_drpen_in                    =>      '0',
        gt1_drprdy_out                  =>      open,
        gt1_drpwe_in                    =>      '0',
    --------------------- RX Initialization and Reset Ports --------------------
        gt1_eyescanreset_in             =>      '0',
        gt1_rxuserrdy_in                =>      sysclk_pll_locked,
    -------------------------- RX Margin Analysis Ports ------------------------
        gt1_eyescandataerror_out        =>      open,
        gt1_eyescantrigger_in           =>      '0',
    ------------------- Receive Ports - Digital Monitor Ports ------------------
        gt1_dmonitorout_out             =>      open,
    ------------------ Receive Ports - FPGA RX Interface Ports -----------------
        gt1_rxusrclk_in                 =>      sysclk320,
        gt1_rxusrclk2_in                =>      sysclk320,
    ------------------ Receive Ports - FPGA RX interface Ports -----------------
        gt1_rxdata_out                  =>      rxdata16(1),
    ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
        gt1_rxdisperr_out               =>      rxdisperr2(1),
        gt1_rxnotintable_out            =>      rxnotintable2(1),
    ------------------------ Receive Ports - RX AFE Ports ----------------------
        gt1_gthrxn_in                   =>      gt_rxn(1),
    ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
        gt1_rxbufreset_in               =>      rxbufreset(1), --'0',
        gt1_rxbufstatus_out             =>      rxbufstatus3_i(1),
    -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
        gt1_rxbyteisaligned_out         =>      rxbyteisaligned_i(1),
        gt1_rxbyterealign_out           =>      rxbyterealign_i(1),
    --------------------- Receive Ports - RX Equalizer Ports -------------------
        gt1_rxmonitorout_out            =>      open,
        gt1_rxmonitorsel_in             =>      "00",
    --------------- Receive Ports - RX Fabric Output Control Ports -------------
        gt1_rxoutclk_out                =>      open, --rxoutclk(1),
    ------------- Receive Ports - RX Initialization and Reset Ports ------------
        gt1_gtrxreset_in                =>      '0',
    ----------------- Receive Ports - RX Polarity Control Ports ----------------
        gt1_rxpolarity_in               =>      rxpolarity(1),
    ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
        gt1_rxchariscomma_out           =>      rxchariscomma2(1),
        gt1_rxcharisk_out               =>      rxcharisk2(1),
    ------------------------ Receive Ports -RX AFE Ports -----------------------
        gt1_gthrxp_in                   =>      gt_rxp(1),
    -------------- Receive Ports -RX Initialization and Reset Ports ------------
        gt1_rxresetdone_out             =>      rxresetdone_i(1),
    --------------------- TX Initialization and Reset Ports --------------------
        gt1_gttxreset_in                =>      '0',

    --GT2  (X0Y2)
    --____________________________CHANNEL PORTS________________________________
    --------------------------------- CPLL Ports -------------------------------
        gt2_cpllfbclklost_out           =>      open, --cpllfbclklost(2),
        gt2_cplllock_out                =>      cplllock(2),
        gt2_cplllockdetclk_in           =>      sysclk40,
        gt2_cpllpd_in                   =>      cpllpd(2),
        gt2_cpllreset_in                =>      '0', --cpllreset(2),
    -------------------------- Channel - Clocking Ports ------------------------
        gt2_gtrefclk0_in                =>      '0',
        gt2_gtrefclk1_in                =>      gtrefclk,
    ---------------------------- Channel - DRP Ports  --------------------------
        gt2_drpaddr_in                  =>      "000000000",
        gt2_drpclk_in                   =>      sysclk80,
        gt2_drpdi_in                    =>      x"0000",
        gt2_drpdo_out                   =>      open,
        gt2_drpen_in                    =>      '0',
        gt2_drprdy_out                  =>      open,
        gt2_drpwe_in                    =>      '0',
    --------------------- RX Initialization and Reset Ports --------------------
        gt2_eyescanreset_in             =>      '0',
        gt2_rxuserrdy_in                =>      sysclk_pll_locked,
    -------------------------- RX Margin Analysis Ports ------------------------
        gt2_eyescandataerror_out        =>      open,
        gt2_eyescantrigger_in           =>      '0',
    ------------------- Receive Ports - Digital Monitor Ports ------------------
        gt2_dmonitorout_out             =>      open,
    ------------------ Receive Ports - FPGA RX Interface Ports -----------------
        gt2_rxusrclk_in                 =>      sysclk320,
        gt2_rxusrclk2_in                =>      sysclk320,
    ------------------ Receive Ports - FPGA RX interface Ports -----------------
        gt2_rxdata_out                  =>      rxdata16(2),
    ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
        gt2_rxdisperr_out               =>      rxdisperr2(2),
        gt2_rxnotintable_out            =>      rxnotintable2(2),
    ------------------------ Receive Ports - RX AFE Ports ----------------------
        gt2_gthrxn_in                   =>      gt_rxn(2),
    ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
        gt2_rxbufreset_in               =>      rxbufreset(2),
        gt2_rxbufstatus_out             =>      rxbufstatus3_i(2),
    -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
        gt2_rxbyteisaligned_out         =>      rxbyteisaligned_i(2),
        gt2_rxbyterealign_out           =>      rxbyterealign_i(2),
    --------------------- Receive Ports - RX Equalizer Ports -------------------
        gt2_rxmonitorout_out            =>      open,
        gt2_rxmonitorsel_in             =>      "00",
    --------------- Receive Ports - RX Fabric Output Control Ports -------------
        gt2_rxoutclk_out                =>      open, --rxoutclk(2),
    ------------- Receive Ports - RX Initialization and Reset Ports ------------
        gt2_gtrxreset_in                =>      '0',
    ----------------- Receive Ports - RX Polarity Control Ports ----------------
        gt2_rxpolarity_in               =>      rxpolarity(2),
    ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
        gt2_rxchariscomma_out           =>      rxchariscomma2(2),
        gt2_rxcharisk_out               =>      rxcharisk2(2),
    ------------------------ Receive Ports -RX AFE Ports -----------------------
        gt2_gthrxp_in                   =>      gt_rxp(2),
    -------------- Receive Ports -RX Initialization and Reset Ports ------------
        gt2_rxresetdone_out             =>      rxresetdone_i(2),
    --------------------- TX Initialization and Reset Ports --------------------
        gt2_gttxreset_in                =>      '0',
    --GT3  (X0Y3)
    --____________________________CHANNEL PORTS________________________________
    --------------------------------- CPLL Ports -------------------------------
        gt3_cpllfbclklost_out           =>      open, --cpllfbclklost(3),
        gt3_cplllock_out                =>      cplllock(3),
        gt3_cplllockdetclk_in           =>      sysclk40,
        gt3_cpllpd_in                   =>      cpllpd(3),
        gt3_cpllreset_in                =>      '0', --cpllreset(3),
    -------------------------- Channel - Clocking Ports ------------------------
        gt3_gtrefclk0_in                =>      '0',
        gt3_gtrefclk1_in                =>      gtrefclk,
    ---------------------------- Channel - DRP Ports  --------------------------
        gt3_drpaddr_in                  =>      "000000000",
        gt3_drpclk_in                   =>      sysclk80,
        gt3_drpdi_in                    =>      x"0000",
        gt3_drpdo_out                   =>      open,
        gt3_drpen_in                    =>      '0',
        gt3_drprdy_out                  =>      open,
        gt3_drpwe_in                    =>      '0',
    --------------------- RX Initialization and Reset Ports --------------------
        gt3_eyescanreset_in             =>      '0',
        gt3_rxuserrdy_in                =>      sysclk_pll_locked,
    -------------------------- RX Margin Analysis Ports ------------------------
        gt3_eyescandataerror_out        =>      open,
        gt3_eyescantrigger_in           =>      '0',
    ------------------- Receive Ports - Digital Monitor Ports ------------------
        gt3_dmonitorout_out             =>      open,
    ------------------ Receive Ports - FPGA RX Interface Ports -----------------
        gt3_rxusrclk_in                 =>      sysclk320,
        gt3_rxusrclk2_in                =>      sysclk320,
    ------------------ Receive Ports - FPGA RX interface Ports -----------------
        gt3_rxdata_out                  =>      rxdata16(3),
    ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
        gt3_rxdisperr_out               =>      rxdisperr2(3),
        gt3_rxnotintable_out            =>      rxnotintable2(3),
    ------------------------ Receive Ports - RX AFE Ports ----------------------
        gt3_gthrxn_in                   =>      gt_rxn(3),
    ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
        gt3_rxbufreset_in               =>      rxbufreset(3), --'0',
        gt3_rxbufstatus_out             =>      rxbufstatus3_i(3),
    -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
        gt3_rxbyteisaligned_out         =>      rxbyteisaligned_i(3),
        gt3_rxbyterealign_out           =>      rxbyterealign_i(3),
    --------------------- Receive Ports - RX Equalizer Ports -------------------
        gt3_rxmonitorout_out            =>      open,
        gt3_rxmonitorsel_in             =>      "00",
    --------------- Receive Ports - RX Fabric Output Control Ports -------------
        gt3_rxoutclk_out                =>      open, --rxoutclk(3),
    ------------- Receive Ports - RX Initialization and Reset Ports ------------
        gt3_gtrxreset_in                =>      '0',
    ----------------- Receive Ports - RX Polarity Control Ports ----------------
        gt3_rxpolarity_in               =>      rxpolarity(3),
    ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
        gt3_rxchariscomma_out           =>      rxchariscomma2(3),
        gt3_rxcharisk_out               =>      rxcharisk2(3),
    ------------------------ Receive Ports -RX AFE Ports -----------------------
        gt3_gthrxp_in                   =>      gt_rxp(3),
    -------------- Receive Ports -RX Initialization and Reset Ports ------------
        gt3_rxresetdone_out             =>      rxresetdone_i(3),
    --------------------- TX Initialization and Reset Ports --------------------
        gt3_gttxreset_in                =>      '0',

    --____________________________COMMON PORTS________________________________
     GT0_QPLLOUTCLK_IN  => '0',
     GT0_QPLLOUTREFCLK_IN => '0'


--    cpllrefclklost_out  => open --cpllrefclklost
    
);


process(sysclk320) begin
if rising_edge(sysclk320) then
    rxdata16_buffer          <= rxdata16;
    rxchariscomma2_buffer    <= rxchariscomma2;
    rxcharisk2_buffer        <= rxcharisk2;
    rxdisperr2_buffer        <= rxdisperr2;
    rxnotintable2_buffer     <= rxnotintable2;
    
    rxbufstatus3_buffer      <= rxbufstatus3_i;
    rxbyteisaligned_buffer   <= rxbyteisaligned_i;
    rxbyterealign_buffer     <= rxbyterealign_i;
    rxresetdone_buffer       <= rxresetdone_i;

    rxdata16_buffer2          <= rxdata16_buffer;         
    rxchariscomma2_buffer2    <= rxchariscomma2_buffer;   
    rxcharisk2_buffer2        <= rxcharisk2_buffer;       
    rxdisperr2_buffer2        <= rxdisperr2_buffer;       
    rxnotintable2_buffer2     <= rxnotintable2_buffer;    
                                                         
    rxbufstatus3_buffer2      <= rxbufstatus3_buffer;     
    rxbyteisaligned_buffer2   <= rxbyteisaligned_buffer;  
    rxbyterealign_buffer2     <= rxbyterealign_buffer;    
    rxresetdone_buffer2       <= rxresetdone_buffer;      
      
end if;
end process;



process(sysclk160) begin
if rising_edge(sysclk160) then
    for ch in 3 downto 0 loop
        --rxdata32_reg(ch)              <= rxdata16_buffer(ch) & rxdata16(ch);
        rxdata32_reg(ch)              <= rxdata16_buffer2(ch) & rxdata16_buffer(ch);
        --rxchariscomma4_reg(ch)        <= rxchariscomma2_buffer(ch) & rxchariscomma2(ch);
        rxchariscomma4_reg(ch)        <= rxchariscomma2_buffer2(ch) & rxchariscomma2_buffer(ch);
        --rxcharisk4_reg(ch)            <= rxcharisk2_buffer(ch) & rxcharisk2(ch);
        rxcharisk4_reg(ch)            <= rxcharisk2_buffer2(ch) & rxcharisk2_buffer(ch);
        --rxdisperr4_reg(ch)            <= rxdisperr2_buffer(ch) & rxdisperr2(ch);
        rxdisperr4_reg(ch)            <= rxdisperr2_buffer2(ch) & rxdisperr2_buffer(ch);
        --rxnotintable4_reg(ch)         <= rxnotintable2_buffer(ch) & rxnotintable2(ch);
        rxnotintable4_reg(ch)         <= rxnotintable2_buffer2(ch) & rxnotintable2_buffer(ch);
    end loop;
    
    for ch in 3 downto 0 loop
        
        --if rxbufstatus3_i(ch) = "000" then rxbufstatus3_reg(ch) <= rxbufstatus3_buffer(ch);
        if rxbufstatus3_buffer(ch) = "000" then rxbufstatus3_reg(ch) <= rxbufstatus3_buffer2(ch);
        --else rxbufstatus3_reg(ch) <= rxbufstatus3_i(ch);
        else rxbufstatus3_reg(ch) <= rxbufstatus3_buffer(ch);
        end if; 

        --rxbyteisaligned_reg(ch) <= rxbyteisaligned_buffer(ch) and rxbyteisaligned_i(ch);
        rxbyteisaligned_reg(ch) <= rxbyteisaligned_buffer2(ch) and rxbyteisaligned_buffer(ch);
        --rxbyterealign_reg(ch)   <= rxbyterealign_buffer(ch)   or  rxbyterealign_i(ch);
        rxbyterealign_reg(ch)   <= rxbyterealign_buffer2(ch)   or  rxbyterealign_buffer(ch);
    
        --if rxresetdone_i(ch) = '0' then rxresetdone_reg(ch) <= rxresetdone_buffer(ch);
        if rxresetdone_buffer(ch) = '0' then rxresetdone_reg(ch) <= rxresetdone_buffer2(ch);
        --else rxresetdone_reg(ch) <= rxresetdone_i(ch);
        else rxresetdone_reg(ch) <= rxresetdone_buffer(ch); 
        end if;
        
    end loop;
    
end if;
end process;

process(sysclk160) begin
if rising_edge(sysclk160) then
    rxdata32          <= rxdata32_reg;
    rxchariscomma4    <= rxchariscomma4_reg;
    rxcharisk4        <= rxcharisk4_reg;
    
    rxdisperr4        <= rxdisperr4_reg;
    rxnotintable4     <= rxnotintable4_reg;
    rxbufstatus3      <= rxbufstatus3_reg; 
    
    rxbyteisaligned   <= rxbyteisaligned_reg; 
    rxbyterealign     <= rxbyterealign_reg;
    rxresetdone       <= rxresetdone_reg;
    end if;
end process;

STAT_ERR_GEN : for i in 0 to 3 generate
process(sysclk160) begin
if rising_edge(sysclk160) then
  rxdisperr1(i)        <= rxdisperr4_reg(i)(3) or rxdisperr4_reg(i)(2) or rxdisperr4_reg(i)(1) or rxdisperr4_reg(i)(0);
  rxnotintable1(i)     <= rxnotintable4_reg(i)(3) or rxnotintable4_reg(i)(2) or rxnotintable4_reg(i)(1) or rxnotintable4_reg(i)(0);
  rxbufstatus1(i)      <= rxbufstatus3_reg(i)(2) or rxbufstatus3_reg(i)(1) or rxbufstatus3_reg(i)(0); 
end if;  
end process;
end generate STAT_ERR_GEN;

end Behavioral;
