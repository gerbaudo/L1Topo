library ieee ;
use ieee.std_logic_1164.ALL ;
use ieee.numeric_std.all ;

package CRC_package is

function ToIndirectInitVal(Direct:std_logic_vector; CRC_Width: positive; Poly: std_logic_vector) return std_logic_vector;

end package; --of Package CRC_package

package body CRC_package is

	function ToIndirectInitVal(Direct:std_logic_vector; CRC_Width: positive; Poly: std_logic_vector) return std_logic_vector is 


		variable InDirect: std_logic_vector(Direct'high downto Direct'low);
	begin
		for k in 0 to CRC_Width loop

			if(k = 0) then
				InDirect := Direct;
			else
				if(InDirect(0)='1') then
					InDirect := (('0'&InDirect(CRC_Width-1 downto 1)) xor ('1'&Poly(CRC_Width-1 downto 1)));
				else
					InDirect := '0'&InDirect(CRC_Width-1 downto 1);
				end if;
			end if;

		end loop;
		return InDirect; 
	end ToIndirectInitVal; -- end function

end CRC_package;
