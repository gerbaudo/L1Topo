--******************************************************************************
--*                                                                            *
--*           Provides a delayed channel reset for the GT Transceivers         *
--*                                                                            *
--*           Frans Schreuder (Nikhef) franss@nikhef.nl                        *
--*                                                                            *
--******************************************************************************


library UNISIM, ieee, work;
use UNISIM.VCOMPONENTS.all;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

entity reset_delay is
  port (
    clk_sys_i      : in     std_logic;
    gtx_reset      : out    std_logic;
    gtx_soft_reset : in     std_logic;
    locked         : out    std_logic;
    reset_in       : in     std_logic;
    reset_out      : out    std_logic);
end entity reset_delay;


architecture rtl of reset_delay is
	signal reset_int: std_logic;
begin
	reset_int <= reset_in or gtx_soft_reset;
	gtx_reset <= reset_int;
	delay: process(clk_sys_i, reset_int) is
		variable count: natural range 0 to 2000;
	begin       
		if(reset_int = '1') then
			count := 1999;
			reset_out <= '0';
		else       
			if (rising_edge(clk_sys_i)) then
				locked <= '0';
				if(count > 800) then
					count := count - 1;
					reset_out <= '0';
				else
					if(count > 0) then
						count := count - 1;
						reset_out <= '1';
					else
						reset_out <= '0';
						locked <= '1';
					end if;
				end if;
			end if;
		end if;
	end process;

end architecture rtl ; -- of reset_delay



