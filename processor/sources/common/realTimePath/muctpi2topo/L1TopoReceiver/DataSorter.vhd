

--******************************************************************************
--*                                                                            *
--*           Checks CRC16-CCITT over the data and arranges the data           *
--*           into two 128-bit std_logic_vector's                              *
--*                                                                            *
--*           Frans Schreuder (Nikhef) franss@nikhef.nl                        *
--*                                                                            *
--******************************************************************************
 
 
library UNISIM, ieee, work;
use UNISIM.VCOMPONENTS.all;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
 
entity DataSorter is
  port (
    RxCharisK0  : in     std_logic_vector(3 downto 0);
    RxCharisK1  : in     std_logic_vector(3 downto 0);
    RxDataIn0   : in     std_logic_vector(31 downto 0);
    RxDataIn1   : in     std_logic_vector(31 downto 0);
    RxDataOut0  : out    std_logic_vector(127 downto 0);
    RxDataOut1  : out    std_logic_vector(127 downto 0);
    clk160m32   : in     std_logic;
    crc16_in    : in     std_logic_vector(31 downto 0);
    crc_charisK : in     std_logic_vector(3 downto 0);
    crc_valid   : out    std_logic;
    data_valid  : out    std_logic;
    reset       : in     std_logic);
end entity DataSorter;
 
 
architecture rtl of DataSorter is
 
component CRC_Gen is
   generic(
     Nbits :  positive  := 32;
     CRC_Width            :  positive   := 16;
     G_Poly: Std_Logic_Vector :=x"1021";
     G_InitVal: std_logic_vector:=x"ffff"
     );
   port(
     CRC   : out    std_logic_vector(CRC_Width-1 downto 0);
     Calc  : in     std_logic;
     Clk   : in     std_logic;
     DIn   : in     std_logic_vector(Nbits-1 downto 0);
     Reset : in     std_logic);
end component;
 
  --IDLE = <K28.5>   "0011111010" or "1100000101"
  constant K28_5 : std_logic_vector (7 downto 0) := x"BC";
  --IDLE = <K28.1>   "0011111001" or "1100000110"
  constant K28_1 : std_logic_vector (7 downto 0) := x"3C";
 
 signal sync_counter: std_logic_vector(1 downto 0):= (others => '0');
 signal srst : std_logic:= '0';
 signal crc_out0, crc_out1: std_logic_vector(15 downto 0):= (others => '0');
 signal crc_out0_pipe, crc_out1_pipe: std_logic_vector(15 downto 0):= (others => '0');
 signal crc_in0, crc_in1: std_logic_vector(31 downto 0):= (others => '0');
 signal crc_checkval0, crc_checkval1: std_logic_vector(15 downto 0);
 signal crc_valid_i0, crc_valid_i1: std_logic;
 
begin
 
    crc0: CRC_Gen
    generic map(
        Nbits       => 32,
        CRC_Width   => 16,
        G_Poly        => x"1021",
        G_InitVal     => x"ffff"
     )
    port map
    (
        CRC => crc_out0,
        Calc => '1',
        Clk => clk160m32,
        Din => crc_in0,
        Reset => srst
 
    );
 
 
    crc1: CRC_Gen
    generic map(
        Nbits       => 32,
        CRC_Width   => 16,
        G_Poly        => x"1021",
        G_InitVal     => x"ffff"
     )
    port map
    (
        CRC => crc_out1,
        Calc => '1',
        Clk => clk160m32,
        Din => crc_in1,
        Reset => srst
 
    );  
 
    CommaReplace0: process(clk160m32) is
        variable v_crc_in0, v_crc_in1: std_logic_vector(31 downto 0):= (others => '0');
        variable v_RxDataOut0, v_RxDataOut1 : std_logic_vector(127 downto 0):=(others =>'0');
        variable v_RxDataOut0_reversed, v_RxDataOut1_reversed : std_logic_vector(127 downto 0):=(others =>'0');
       
    begin
        if (rising_edge(clk160m32)) then
            if(RxCharisK0(0)= '1' and RxDataIn0(7 downto 0)=K28_5) then --last word can only contain K28_5, other commas are always K28_1
                sync_counter <= "00"; --will be 00 on next round of 4 clock cycles.
            else
                sync_counter <= sync_counter + 1;
            end if;
            if(sync_counter = "00") then
                srst <= '1';
            else
                srst <= '0';
            end if;
           
            if(RxCharisK0(0) = '1') then
                v_crc_in0(31 downto 24) := x"00";
            else
                v_crc_in0(31 downto 24) := RxDataIn0(7 downto 0);
            end if;
 
            if(RxCharisK0(1) = '1') then
                v_crc_in0(23 downto 16) := x"00";
            else
                v_crc_in0(23 downto 16) := RxDataIn0(15 downto 8);
            end if;
 
            if(RxCharisK0(2) = '1') then
                v_crc_in0(15 downto 8) := x"00";
            else
                v_crc_in0(15 downto 8) := RxDataIn0(23 downto 16);
            end if;
 
            if(RxCharisK0(3) = '1') then
                v_crc_in0(7 downto 0) := x"00";
            else
                v_crc_in0(7 downto 0) := RxDataIn0(31 downto 24);
            end if;
 
 
            if(RxCharisK1(0) = '1') then
                v_crc_in1(31 downto 24) := x"00";
            else
                v_crc_in1(31 downto 24) := RxDataIn1(7 downto 0);
            end if;
 
            if(RxCharisK1(1) = '1') then
                v_crc_in1(23 downto 16) := x"00";
            else
                v_crc_in1(23 downto 16) := RxDataIn1(15 downto 8);
            end if;
 
            if(RxCharisK1(2) = '1') then
                v_crc_in1(15 downto 8) := x"00";
            else
                v_crc_in1(15 downto 8) := RxDataIn1(23 downto 16);
            end if;
 
            if(RxCharisK1(3) = '1') then
                v_crc_in1(7 downto 0) := x"00";
            else
                v_crc_in1(7 downto 0) := RxDataIn1(31 downto 24);
            end if;
 
            for i in 0 to 15 loop
           
                v_RxDataOut0((i*8)+(to_integer(unsigned(sync_counter))*2)) := v_crc_in0(i+16);
                v_RxDataOut0((i*8)+1+(to_integer(unsigned(sync_counter))*2)) := v_crc_in0(i);
                v_RxDataOut1((i*8)+(to_integer(unsigned(sync_counter))*2)) := v_crc_in1(i+16);
                v_RxDataOut1((i*8)+1+(to_integer(unsigned(sync_counter))*2)) := v_crc_in1(i);
                                                               
               
            end loop;
 
            for i in 0 to 7 loop    --reverse bit order in bytes in output vector
                for j in 0 to 15 loop
                    v_RxDataOut0_reversed(i+(j*8)) := v_RxDataOut0((7-i)+(j*8));
                    v_RxDataOut1_reversed(i+(j*8)) := v_RxDataOut1((7-i)+(j*8));
                end loop;
            end loop;
           
            if(sync_counter = "11") then        --output the data when looped through 1 BC, every 25ns
                RxDataOut0 <= v_RxDataOut0_reversed;
                RxDataOut1 <= v_RxDataOut1_reversed;
                data_valid <= '1';
            else
                data_valid <= '0';
            end if;
 
            if(crc_charIsK = "0000") then
                crc_checkval0 <= crc16_in(23 downto 16) & crc16_in(31 downto 24);
                crc_checkval1 <= crc16_in( 7 downto  0) & crc16_in(15 downto 8);
            else
                crc_checkval0 <= crc_checkval0;
                crc_checkval1 <= crc_checkval1;
            end if;
 
            crc_in0 <= v_crc_in0;
            crc_in1 <= v_crc_in1;
           
           
               
 
 
            if(srst = '1') then --time to check CRC
                crc_out0_pipe <= crc_out0;
                crc_out1_pipe <= crc_out1;
                if(crc_out0_pipe = crc_checkval0) then
                    crc_valid_i0 <= '1';
                else
                    crc_valid_i0 <= '0';
                end if;
                if(crc_out1_pipe = crc_checkval1) then
                    crc_valid_i1 <= '1';
                else
                    crc_valid_i1 <= '0';
                end if;
            else
                crc_valid_i0 <= crc_valid_i0;
                crc_valid_i1 <= crc_valid_i1;
            end if;
           
           
        end if;
       
 
    end process;
 
    crc_valid <= crc_valid_i0 and crc_valid_i1;
 
 
 
end architecture rtl ; -- of DataSorter
     

