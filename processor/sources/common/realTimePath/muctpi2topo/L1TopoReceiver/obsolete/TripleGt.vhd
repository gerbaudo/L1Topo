--******************************************************************************
--*                                                                            *
--*           Wrapper for slightly modified Xilinx Transceiver wizard          *
--*            -RX/TX buffers bypassed                                         *
--*            -Locks on 4 byte boundry                                        *
--*            -All clocks set to 160.32MHz                                    *
--*            -32/40 bit interface                                            *
--*           *Modifications to save global clock resources:                   *
--*            -All clocks are single ended (internal) clocks                  *
--*            -Clocks are tied together (GtRefCLK / Sysclk)                   *
--*                                                                            *
--*           Frans Schreuder (Nikhef) franss@nikhef.nl                        *
--*                                                                            *
--******************************************************************************


library UNISIM, ieee, work;
use UNISIM.VCOMPONENTS.all;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

entity triple_gt is
  port (
    channel_reset      : in     std_logic;
    clk160m32          : in     std_logic;
    gt0_gtxrxn_in      : in     std_logic;
    gt0_gtxrxp_in      : in     std_logic;
    gt0_gtxtxn_out     : out    std_logic;
    gt0_gtxtxp_out     : out    std_logic;
    gt0_rxcharisk_out  : out    std_logic_vector(3 downto 0);
    gt0_rxdata_out     : out    std_logic_vector(31 downto 0);
    gt0_txcharisk_in   : in     std_logic_vector(3 downto 0);
    gt0_txdata_in      : in     std_logic_vector(31 downto 0);
    gt1_gtxrxn_in      : in     std_logic;
    gt1_gtxrxp_in      : in     std_logic;
    gt1_gtxtxn_out     : out    std_logic;
    gt1_gtxtxp_out     : out    std_logic;
    gt1_rxcharisk_out  : out    std_logic_vector(3 downto 0);
    gt1_rxdata_out     : out    std_logic_vector(31 downto 0);
    gt1_txcharisk_in   : in     std_logic_vector(3 downto 0);
    gt1_txdata_in      : in     std_logic_vector(31 downto 0);
    gt2_gtxrxn_in      : in     std_logic;
    gt2_gtxrxp_in      : in     std_logic;
    gt2_gtxtxn_out     : out    std_logic;
    gt2_gtxtxp_out     : out    std_logic;
    gt2_rxcharisk_out  : out    std_logic_vector(3 downto 0);
    gt2_rxdata_out     : out    std_logic_vector(31 downto 0);
    gt2_txcharisk_in   : in     std_logic_vector(3 downto 0);
    gt2_txdata_in      : in     std_logic_vector(31 downto 0);
    recoveredclk160m32 : out    std_logic;
    reset              : in     std_logic;
    reset_done         : out    std_logic;
    userrdy            : in     std_logic);
end entity triple_gt;

library UNISIM;
use UNISIM.VCOMPONENTS.all;
architecture rtl of triple_gt is


component gtwizard_triplet_support
generic
(
    EXAMPLE_SIM_GTRESET_SPEEDUP             : string    := "TRUE";     -- simulation setting for GT SecureIP model
    STABLE_CLOCK_PERIOD                     : integer   := 6  

);
port
(
    SOFT_RESET_IN                           : in   std_logic;
    DONT_RESET_ON_DATA_ERROR_IN             : in   std_logic;
    
    GT0_TX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT0_RX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT0_DATA_VALID_IN                       : in   std_logic;
    GT1_TX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT1_RX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT1_DATA_VALID_IN                       : in   std_logic;
    GT2_TX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT2_RX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT2_DATA_VALID_IN                       : in   std_logic;
 
    GT0_TXUSRCLK_OUT                        : out  std_logic;
    GT0_TXUSRCLK2_OUT                       : out  std_logic;
    GT0_RXUSRCLK_OUT                        : out  std_logic;
    GT0_RXUSRCLK2_OUT                       : out  std_logic;
 
    GT1_TXUSRCLK_OUT                        : out  std_logic;
    GT1_TXUSRCLK2_OUT                       : out  std_logic;
    GT1_RXUSRCLK_OUT                        : out  std_logic;
    GT1_RXUSRCLK2_OUT                       : out  std_logic;
 
    GT2_TXUSRCLK_OUT                        : out  std_logic;
    GT2_TXUSRCLK2_OUT                       : out  std_logic;
    GT2_RXUSRCLK_OUT                        : out  std_logic;
    GT2_RXUSRCLK2_OUT                       : out  std_logic;

    --_________________________________________________________________________
    --GT0  (X1Y0)
    --____________________________CHANNEL PORTS________________________________
    ---------------------------- Channel - DRP Ports  --------------------------
    gt0_drpaddr_in                          : in   std_logic_vector(8 downto 0);
    gt0_drpdi_in                            : in   std_logic_vector(15 downto 0);
    gt0_drpdo_out                           : out  std_logic_vector(15 downto 0);
    gt0_drpen_in                            : in   std_logic;
    gt0_drprdy_out                          : out  std_logic;
    gt0_drpwe_in                            : in   std_logic;
    --------------------------- Digital Monitor Ports --------------------------
    gt0_dmonitorout_out                     : out  std_logic_vector(7 downto 0);
    --------------------- RX Initialization and Reset Ports --------------------
    gt0_eyescanreset_in                     : in   std_logic;
    gt0_rxuserrdy_in                        : in   std_logic;
    -------------------------- RX Margin Analysis Ports ------------------------
    gt0_eyescandataerror_out                : out  std_logic;
    gt0_eyescantrigger_in                   : in   std_logic;
    ------------------ Receive Ports - FPGA RX interface Ports -----------------
    gt0_rxdata_out                          : out  std_logic_vector(31 downto 0);
    ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
    gt0_rxdisperr_out                       : out  std_logic_vector(3 downto 0);
    gt0_rxnotintable_out                    : out  std_logic_vector(3 downto 0);
    --------------------------- Receive Ports - RX AFE -------------------------
    gt0_gtxrxp_in                           : in   std_logic;
    ------------------------ Receive Ports - RX AFE Ports ----------------------
    gt0_gtxrxn_in                           : in   std_logic;
    ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
    gt0_rxphmonitor_out                     : out  std_logic_vector(4 downto 0);
    gt0_rxphslipmonitor_out                 : out  std_logic_vector(4 downto 0);
    --------------------- Receive Ports - RX Equalizer Ports -------------------
    gt0_rxdfelpmreset_in                    : in   std_logic;
    gt0_rxmonitorout_out                    : out  std_logic_vector(6 downto 0);
    gt0_rxmonitorsel_in                     : in   std_logic_vector(1 downto 0);
    ------------- Receive Ports - RX Initialization and Reset Ports ------------
    gt0_gtrxreset_in                        : in   std_logic;
    gt0_rxpmareset_in                       : in   std_logic;
    ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
    gt0_rxcharisk_out                       : out  std_logic_vector(3 downto 0);
    -------------- Receive Ports -RX Initialization and Reset Ports ------------
    gt0_rxresetdone_out                     : out  std_logic;
    --------------------- TX Initialization and Reset Ports --------------------
    gt0_gttxreset_in                        : in   std_logic;
    gt0_txuserrdy_in                        : in   std_logic;
    ------------------ Transmit Ports - TX Data Path interface -----------------
    gt0_txdata_in                           : in   std_logic_vector(31 downto 0);
    ---------------- Transmit Ports - TX Driver and OOB signaling --------------
    gt0_gtxtxn_out                          : out  std_logic;
    gt0_gtxtxp_out                          : out  std_logic;
    ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
    gt0_txoutclkfabric_out                  : out  std_logic;
    gt0_txoutclkpcs_out                     : out  std_logic;
    --------------------- Transmit Ports - TX Gearbox Ports --------------------
    gt0_txcharisk_in                        : in   std_logic_vector(3 downto 0);
    ------------- Transmit Ports - TX Initialization and Reset Ports -----------
    gt0_txresetdone_out                     : out  std_logic;

    --GT1  (X1Y1)
    --____________________________CHANNEL PORTS________________________________
    ---------------------------- Channel - DRP Ports  --------------------------
    gt1_drpaddr_in                          : in   std_logic_vector(8 downto 0);
    gt1_drpdi_in                            : in   std_logic_vector(15 downto 0);
    gt1_drpdo_out                           : out  std_logic_vector(15 downto 0);
    gt1_drpen_in                            : in   std_logic;
    gt1_drprdy_out                          : out  std_logic;
    gt1_drpwe_in                            : in   std_logic;
    --------------------------- Digital Monitor Ports --------------------------
    gt1_dmonitorout_out                     : out  std_logic_vector(7 downto 0);
    --------------------- RX Initialization and Reset Ports --------------------
    gt1_eyescanreset_in                     : in   std_logic;
    gt1_rxuserrdy_in                        : in   std_logic;
    -------------------------- RX Margin Analysis Ports ------------------------
    gt1_eyescandataerror_out                : out  std_logic;
    gt1_eyescantrigger_in                   : in   std_logic;
    ------------------ Receive Ports - FPGA RX interface Ports -----------------
    gt1_rxdata_out                          : out  std_logic_vector(31 downto 0);
    ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
    gt1_rxdisperr_out                       : out  std_logic_vector(3 downto 0);
    gt1_rxnotintable_out                    : out  std_logic_vector(3 downto 0);
    --------------------------- Receive Ports - RX AFE -------------------------
    gt1_gtxrxp_in                           : in   std_logic;
    ------------------------ Receive Ports - RX AFE Ports ----------------------
    gt1_gtxrxn_in                           : in   std_logic;
    ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
    gt1_rxphmonitor_out                     : out  std_logic_vector(4 downto 0);
    gt1_rxphslipmonitor_out                 : out  std_logic_vector(4 downto 0);
    --------------------- Receive Ports - RX Equalizer Ports -------------------
    gt1_rxdfelpmreset_in                    : in   std_logic;
    gt1_rxmonitorout_out                    : out  std_logic_vector(6 downto 0);
    gt1_rxmonitorsel_in                     : in   std_logic_vector(1 downto 0);
    ------------- Receive Ports - RX Initialization and Reset Ports ------------
    gt1_gtrxreset_in                        : in   std_logic;
    gt1_rxpmareset_in                       : in   std_logic;
    ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
    gt1_rxcharisk_out                       : out  std_logic_vector(3 downto 0);
    -------------- Receive Ports -RX Initialization and Reset Ports ------------
    gt1_rxresetdone_out                     : out  std_logic;
    --------------------- TX Initialization and Reset Ports --------------------
    gt1_gttxreset_in                        : in   std_logic;
    gt1_txuserrdy_in                        : in   std_logic;
    ------------------ Transmit Ports - TX Data Path interface -----------------
    gt1_txdata_in                           : in   std_logic_vector(31 downto 0);
    ---------------- Transmit Ports - TX Driver and OOB signaling --------------
    gt1_gtxtxn_out                          : out  std_logic;
    gt1_gtxtxp_out                          : out  std_logic;
    ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
    gt1_txoutclkfabric_out                  : out  std_logic;
    gt1_txoutclkpcs_out                     : out  std_logic;
    --------------------- Transmit Ports - TX Gearbox Ports --------------------
    gt1_txcharisk_in                        : in   std_logic_vector(3 downto 0);
    ------------- Transmit Ports - TX Initialization and Reset Ports -----------
    gt1_txresetdone_out                     : out  std_logic;

    --GT2  (X1Y2)
    --____________________________CHANNEL PORTS________________________________
    ---------------------------- Channel - DRP Ports  --------------------------
    gt2_drpaddr_in                          : in   std_logic_vector(8 downto 0);
    gt2_drpdi_in                            : in   std_logic_vector(15 downto 0);
    gt2_drpdo_out                           : out  std_logic_vector(15 downto 0);
    gt2_drpen_in                            : in   std_logic;
    gt2_drprdy_out                          : out  std_logic;
    gt2_drpwe_in                            : in   std_logic;
    --------------------------- Digital Monitor Ports --------------------------
    gt2_dmonitorout_out                     : out  std_logic_vector(7 downto 0);
    --------------------- RX Initialization and Reset Ports --------------------
    gt2_eyescanreset_in                     : in   std_logic;
    gt2_rxuserrdy_in                        : in   std_logic;
    -------------------------- RX Margin Analysis Ports ------------------------
    gt2_eyescandataerror_out                : out  std_logic;
    gt2_eyescantrigger_in                   : in   std_logic;
    ------------------ Receive Ports - FPGA RX interface Ports -----------------
    gt2_rxdata_out                          : out  std_logic_vector(31 downto 0);
    ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
    gt2_rxdisperr_out                       : out  std_logic_vector(3 downto 0);
    gt2_rxnotintable_out                    : out  std_logic_vector(3 downto 0);
    --------------------------- Receive Ports - RX AFE -------------------------
    gt2_gtxrxp_in                           : in   std_logic;
    ------------------------ Receive Ports - RX AFE Ports ----------------------
    gt2_gtxrxn_in                           : in   std_logic;
    ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
    gt2_rxphmonitor_out                     : out  std_logic_vector(4 downto 0);
    gt2_rxphslipmonitor_out                 : out  std_logic_vector(4 downto 0);
    --------------------- Receive Ports - RX Equalizer Ports -------------------
    gt2_rxdfelpmreset_in                    : in   std_logic;
    gt2_rxmonitorout_out                    : out  std_logic_vector(6 downto 0);
    gt2_rxmonitorsel_in                     : in   std_logic_vector(1 downto 0);
    ------------- Receive Ports - RX Initialization and Reset Ports ------------
    gt2_gtrxreset_in                        : in   std_logic;
    gt2_rxpmareset_in                       : in   std_logic;
    ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
    gt2_rxcharisk_out                       : out  std_logic_vector(3 downto 0);
    -------------- Receive Ports -RX Initialization and Reset Ports ------------
    gt2_rxresetdone_out                     : out  std_logic;
    --------------------- TX Initialization and Reset Ports --------------------
    gt2_gttxreset_in                        : in   std_logic;
    gt2_txuserrdy_in                        : in   std_logic;
    ------------------ Transmit Ports - TX Data Path interface -----------------
    gt2_txdata_in                           : in   std_logic_vector(31 downto 0);
    ---------------- Transmit Ports - TX Driver and OOB signaling --------------
    gt2_gtxtxn_out                          : out  std_logic;
    gt2_gtxtxp_out                          : out  std_logic;
    ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
    gt2_txoutclkfabric_out                  : out  std_logic;
    gt2_txoutclkpcs_out                     : out  std_logic;
    --------------------- Transmit Ports - TX Gearbox Ports --------------------
    gt2_txcharisk_in                        : in   std_logic_vector(3 downto 0);
    ------------- Transmit Ports - TX Initialization and Reset Ports -----------
    gt2_txresetdone_out                     : out  std_logic;

    --____________________________COMMON PORTS________________________________
    GT0_QPLLLOCK_OUT : out std_logic;
    GT0_QPLLREFCLKLOST_OUT  : out std_logic;
     GT0_QPLLOUTCLK_OUT  : out std_logic;
     GT0_QPLLOUTREFCLK_OUT : out std_logic;
       SYSCLK_IN                             : in   std_logic

);

end component;



signal rxoutclk: std_logic;
signal userclk: std_logic;
signal gt0_rxresetdone: std_logic;
signal gt1_rxresetdone: std_logic;
signal gt2_rxresetdone: std_logic;
signal gt0_txresetdone: std_logic;
signal gt1_txresetdone: std_logic;
signal gt2_txresetdone: std_logic;

begin   
	recoveredclk160m32 <= userclk;

    reset_done <=(gt0_rxresetdone and
                  gt1_rxresetdone and
                  gt2_rxresetdone and
                  gt0_txresetdone and
                  gt1_txresetdone and
                  gt2_txresetdone);

buf0: BUFG port map(
    I=>rxoutclk,
    O=>userclk
    );

    
gt_triplet0 : gtwizard_triplet_support
port map
(
    SOFT_RESET_IN => reset,
    DONT_RESET_ON_DATA_ERROR_IN => '1',
    
     GT0_TX_FSM_RESET_DONE_OUT => open,
     GT0_RX_FSM_RESET_DONE_OUT => open,
     GT0_DATA_VALID_IN => userrdy,
     GT1_TX_FSM_RESET_DONE_OUT => open,
     GT1_RX_FSM_RESET_DONE_OUT => open,
     GT1_DATA_VALID_IN => userrdy,
     GT2_TX_FSM_RESET_DONE_OUT => open,
     GT2_RX_FSM_RESET_DONE_OUT => open,
     GT2_DATA_VALID_IN => userrdy,
     GT0_RXUSRCLK_OUT => rxoutclk,
     GT0_RXUSRCLK2_OUT => open,
     GT1_RXUSRCLK_OUT => open,
     GT1_RXUSRCLK2_OUT => open,
     GT2_RXUSRCLK_OUT => open,
     GT2_RXUSRCLK2_OUT => open,
     GT0_TXUSRCLK_OUT => open,
     GT0_TXUSRCLK2_OUT => open,
     GT1_TXUSRCLK_OUT => open,
     GT1_TXUSRCLK2_OUT => open,
     GT2_TXUSRCLK_OUT => open,
     GT2_TXUSRCLK2_OUT => open,
 
     
    --_________________________________________________________________________
    --GT0  (X0Y0)
    --____________________________CHANNEL PORTS________________________________
    ---------------------------- Channel - DRP Ports  --------------------------
        gt0_drpaddr_in                  =>      (others => '0'),
        gt0_drpdi_in                    =>      (others => '0'),
        gt0_drpdo_out                   =>      open,
        gt0_drpen_in                    =>      '0',
        gt0_drprdy_out                  =>      open,
        gt0_drpwe_in                    =>      '0',
    --------------------------- Digital Monitor Ports --------------------------
        gt0_dmonitorout_out             =>      open,
       
    --------------------- RX Initialization and Reset Ports --------------------
        gt0_eyescanreset_in             =>      reset,
        gt0_rxuserrdy_in                =>      userrdy,
    -------------------------- RX Margin Analysis Ports ------------------------
        gt0_eyescandataerror_out        =>      open,
        gt0_eyescantrigger_in           =>      '0',
    ------------------ Receive Ports - FPGA RX interface Ports -----------------
        gt0_rxdata_out                  =>      gt0_rxdata_out,
    ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
        gt0_rxdisperr_out               =>      open,
        gt0_rxnotintable_out            =>      open,
    --------------------------- Receive Ports - RX AFE -------------------------
        gt0_gtxrxp_in                   =>      gt0_gtxrxp_in,
    ------------------------ Receive Ports - RX AFE Ports ----------------------
        gt0_gtxrxn_in                   =>      gt0_gtxrxn_in,
    ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
        gt0_rxphmonitor_out             =>      open,
        gt0_rxphslipmonitor_out         =>      open,      
    --------------------- Receive Ports - RX Equalizer Ports -------------------
        gt0_rxdfelpmreset_in            =>      reset,
        gt0_rxmonitorout_out            =>      open,
        gt0_rxmonitorsel_in             =>      "00",
    ------------- Receive Ports - RX Initialization and Reset Ports ------------
        gt0_gtrxreset_in                =>      channel_reset,
        gt0_rxpmareset_in               =>      reset,
    ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
        gt0_rxcharisk_out               =>      gt0_rxcharisk_out,
    -------------- Receive Ports -RX Initialization and Reset Ports ------------
        gt0_rxresetdone_out             =>      gt0_rxresetdone,
    --------------------- TX Initialization and Reset Ports --------------------
        gt0_gttxreset_in                =>      channel_reset,
        gt0_txuserrdy_in                =>      userrdy,
    ------------------ Transmit Ports - TX Data Path interface -----------------
        gt0_txdata_in                   =>      gt0_txdata_in,
    ---------------- Transmit Ports - TX Driver and OOB signaling --------------
        gt0_gtxtxn_out                  =>      gt0_gtxtxn_out,
        gt0_gtxtxp_out                  =>      gt0_gtxtxp_out,
    ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
        gt0_txoutclkfabric_out          =>      open,
        gt0_txoutclkpcs_out             =>      open,
    --------------------- Transmit Ports - TX Gearbox Ports --------------------
        gt0_txcharisk_in                =>      gt0_txcharisk_in,
    ------------- Transmit Ports - TX Initialization and Reset Ports -----------
        gt0_txresetdone_out             =>      gt0_txresetdone,       
 
    --GT1  (X0Y1)
    --____________________________CHANNEL PORTS________________________________
    ---------------------------- Channel - DRP Ports  --------------------------
        gt1_drpaddr_in                  =>      (others => '0'),
        gt1_drpdi_in                    =>      (others => '0'),
        gt1_drpdo_out                   =>      open,
        gt1_drpen_in                    =>      '0',
        gt1_drprdy_out                  =>      open,
        gt1_drpwe_in                    =>      '0',
        --------------------------- Digital Monitor Ports --------------------------
        gt1_dmonitorout_out             =>      open,
    --------------------- RX Initialization and Reset Ports --------------------
        gt1_eyescanreset_in             =>      reset,
        gt1_rxuserrdy_in                =>      userrdy,
    -------------------------- RX Margin Analysis Ports ------------------------
        gt1_eyescandataerror_out        =>      open,
        gt1_eyescantrigger_in           =>      '0',
    ------------------ Receive Ports - FPGA RX interface Ports -----------------
        gt1_rxdata_out                  =>      gt1_rxdata_out,
    ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
        gt1_rxdisperr_out               =>      open,
        gt1_rxnotintable_out            =>      open,
    --------------------------- Receive Ports - RX AFE -------------------------
        gt1_gtxrxp_in                   =>      gt1_gtxrxp_in,
    ------------------------ Receive Ports - RX AFE Ports ----------------------
        gt1_gtxrxn_in                   =>      gt1_gtxrxn_in,
    ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
        gt1_rxphmonitor_out             =>      open,
        gt1_rxphslipmonitor_out         =>      open,         
    --------------------- Receive Ports - RX Equalizer Ports -------------------
        gt1_rxdfelpmreset_in            =>      reset,
        gt1_rxmonitorout_out            =>      open,
        gt1_rxmonitorsel_in             =>      "00",
    ------------- Receive Ports - RX Initialization and Reset Ports ------------
        gt1_gtrxreset_in                =>      channel_reset,
        gt1_rxpmareset_in               =>      reset,
    ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
        gt1_rxcharisk_out               =>      gt1_rxcharisk_out,
    -------------- Receive Ports -RX Initialization and Reset Ports ------------
        gt1_rxresetdone_out             =>      gt1_rxresetdone,
    --------------------- TX Initialization and Reset Ports --------------------
        gt1_gttxreset_in                =>      channel_reset,
        gt1_txuserrdy_in                =>      userrdy,
    ------------------ Transmit Ports - TX Data Path interface -----------------
        gt1_txdata_in                   =>      gt1_txdata_in,
    ---------------- Transmit Ports - TX Driver and OOB signaling --------------
        gt1_gtxtxn_out                  =>      gt1_gtxtxn_out,
        gt1_gtxtxp_out                  =>      gt1_gtxtxp_out,
    ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
        gt1_txoutclkfabric_out          =>      open,
        gt1_txoutclkpcs_out             =>      open,
    --------------------- Transmit Ports - TX Gearbox Ports --------------------
        gt1_txcharisk_in                =>      gt1_txcharisk_in,
    ------------- Transmit Ports - TX Initialization and Reset Ports -----------
        gt1_txresetdone_out             =>      gt1_txresetdone,           
    --GT2  (X0Y2)
    --____________________________CHANNEL PORTS________________________________
    ---------------------------- Channel - DRP Ports  --------------------------
        gt2_drpaddr_in                  =>      (others => '0'),
        gt2_drpdi_in                    =>      (others => '0'),
        gt2_drpdo_out                   =>      open,
        gt2_drpen_in                    =>      '0',
        gt2_drprdy_out                  =>      open,
        gt2_drpwe_in                    =>      '0',
    --------------------------- Digital Monitor Ports --------------------------
        gt2_dmonitorout_out             =>      open,
    --------------------- RX Initialization and Reset Ports --------------------
        gt2_eyescanreset_in             =>      reset,
        gt2_rxuserrdy_in                =>      userrdy,
    -------------------------- RX Margin Analysis Ports ------------------------
        gt2_eyescandataerror_out        =>      open,
        gt2_eyescantrigger_in           =>      '0',
    ------------------ Receive Ports - FPGA RX interface Ports -----------------
        gt2_rxdata_out                  =>      gt2_rxdata_out,
    ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
        gt2_rxdisperr_out               =>      open,
        gt2_rxnotintable_out            =>      open,
    --------------------------- Receive Ports - RX AFE -------------------------
        gt2_gtxrxp_in                   =>      gt2_gtxrxp_in,
    ------------------------ Receive Ports - RX AFE Ports ----------------------
        gt2_gtxrxn_in                   =>      gt2_gtxrxn_in,
    ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
        gt2_rxphmonitor_out             =>      open,
        gt2_rxphslipmonitor_out         =>      open,         
    --------------------- Receive Ports - RX Equalizer Ports -------------------
        gt2_rxdfelpmreset_in            =>      reset,
        gt2_rxmonitorout_out            =>      open,
        gt2_rxmonitorsel_in             =>      "00",
    ------------- Receive Ports - RX Initialization and Reset Ports ------------
        gt2_gtrxreset_in                =>      channel_reset,
        gt2_rxpmareset_in               =>      reset,
    ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
        gt2_rxcharisk_out               =>      gt2_rxcharisk_out,
    -------------- Receive Ports -RX Initialization and Reset Ports ------------
        gt2_rxresetdone_out             =>      gt2_rxresetdone,
    --------------------- TX Initialization and Reset Ports --------------------
        gt2_gttxreset_in                =>      channel_reset,
        gt2_txuserrdy_in                =>      userrdy,
    ------------------ Transmit Ports - TX Data Path interface -----------------
        gt2_txdata_in                   =>      gt2_txdata_in,
    ---------------- Transmit Ports - TX Driver and OOB signaling --------------
        gt2_gtxtxn_out                  =>      gt2_gtxtxn_out,
        gt2_gtxtxp_out                  =>      gt2_gtxtxp_out,
    ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
        gt2_txoutclkfabric_out          =>      open,
        gt2_txoutclkpcs_out             =>      open,
    --------------------- Transmit Ports - TX Gearbox Ports --------------------
        gt2_txcharisk_in                =>      gt2_txcharisk_in,
    ------------- Transmit Ports - TX Initialization and Reset Ports -----------
        gt2_txresetdone_out             =>      gt2_txresetdone,           

            --____________________________COMMON PORTS________________________________
        GT0_QPLLLOCK_OUT                =>      open,
        GT0_QPLLREFCLKLOST_OUT          =>      open,
        GT0_QPLLOUTCLK_OUT              =>      open,
        GT0_QPLLOUTREFCLK_OUT           =>      open,
        SYSCLK_IN                       =>      clk160m32

);


end architecture rtl ; -- of triple_gt
