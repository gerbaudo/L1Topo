--******************************************************************************
--*                                                                            *
--*           Toplevel structure for the MuCTPiToTopo receiver in the          *
--*           L1Topo Processor                                                 *
--*                                                                            *
--*           Frans Schreuder (Nikhef) franss@nikhef.nl                        *
--*                                                                            *
--******************************************************************************

library UNISIM, ieee, work;
use UNISIM.VCOMPONENTS.all;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use work.rod_l1_topo_types_const.all;
use work.muon_types.all;
use work.l1topo_package.all;

entity L1TopoReceiver is
  
  port (
  --160.32MHz input
    clkin160m32     : in     std_logic;
    sysclk40 : in std_logic;
  --RxDataOut0/1 contain the received TOB's in a 128 bit vector, updated every 4 clockticks, registered when data_valid = '1'. (clock: clk160m32)  
    RxDataOut0      : out    std_logic_vector(127 downto 0);
    RxDataOut1      : out    std_logic_vector(127 downto 0);
    crc_valid       : out    std_logic;
    data_valid      : out    std_logic;
  --GT Inputs:
    gt0_rxcharisk_in: in     std_logic_vector(3 downto 0);
    gt0_rxdata_in   : in     std_logic_vector(31 downto 0);
    gt1_rxcharisk_in: in     std_logic_vector(3 downto 0);
    gt1_rxdata_in   : in     std_logic_vector(31 downto 0);
    gt2_rxcharisk_in: in     std_logic_vector(3 downto 0);
    gt2_rxdata_in   : in     std_logic_vector(31 downto 0);
  --Reset input
    reset           : in     std_logic);
end entity L1TopoReceiver;


architecture structure of L1TopoReceiver is

  signal muon_data_out : arraySLV128(2 downto 0);
  signal muon_crc_valid, muon_data_valid : std_logic;

begin

  DataSorterInst: entity work.DataSorter
    port map(
      RxCharisK0  => gt0_rxcharisk_in,
      RxCharisK1  => gt1_rxcharisk_in,
      RxDataIn0   => gt0_rxdata_in,
      RxDataIn1   => gt1_rxdata_in,
      RxDataOut0  => muon_data_out(1),--RxDataOut0,
      RxDataOut1  => muon_data_out(0),--RxDataOut1,
      crc16_in    => gt2_rxdata_in,
      crc_charisK => gt2_rxcharisk_in,
      crc_valid   => muon_crc_valid,
      data_valid  => muon_data_valid,
      reset       => reset,
      clk160m32   => clkin160m32);
  
PIPELINE_MUON_RAW: process(sysclk40) 
  begin
    if rising_edge(sysclk40) then
      RxDataOut0 <= muon_data_out(0);
      RxDataOut1 <= muon_data_out(1);
      data_valid <= muon_data_valid;

    end if;
  end process;



ILA_MUON: entity work.muon_rec_ila  
  port map(
    clk => clkin160m32,
    probe0 => muon_data_out(0),
    probe1 => muon_data_out(1),
    probe2(0) => '0',
    probe3(0) => '0',
    probe4 => (others => '0'),
    probe5 => (others => '0'),
    probe6 => (others => '0')
    );

  
  crc_valid <= muon_crc_valid;
  
end architecture structure ; -- of L1TopoReceiver

