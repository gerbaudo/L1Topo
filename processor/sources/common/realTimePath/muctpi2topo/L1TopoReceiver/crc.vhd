
--******************************************************************************
--*                                                                            *
--*           Calculates a CRC-16-CCITT over the data at Din, data may         *
--*           already arrive when Reset is high                                *
--*                                                                            *
--*           Frans Schreuder (Nikhef) franss@nikhef.nl                        *
--*                                                                            *
--******************************************************************************


library ieee;
use ieee.std_logic_1164.all;
library work;
use work.crc_package.all;

entity CRC_Gen is
   generic(
     Nbits :  positive 	:= 32;
     CRC_Width		  :  positive 	:= 16;
     G_Poly: Std_Logic_Vector :=x"1021";
     G_InitVal: std_logic_vector:=x"ffff"
     );
   port(
     CRC   : out    std_logic_vector(CRC_Width-1 downto 0);
     Calc  : in     std_logic;
     Clk   : in     std_logic;
     DIn   : in     std_logic_vector(Nbits-1 downto 0);
     Reset : in     std_logic);
end CRC_Gen;
 
 


architecture rtl of CRC_Gen is
  constant Poly: Std_Logic_Vector(CRC_Width-1 downto 0) := G_Poly;
  constant InitVal: Std_Logic_Vector(CRC_Width-1 downto 0) := G_InitVal;
  
  constant InDirectInitVal: std_logic_vector(CRC_Width-1 downto 0):=ToIndirectInitVal(InitVal, CRC_Width, Poly);
  signal Reg1_out: std_logic_vector(CRC_Width-1 downto 0);
BEGIN
   
  
   Process (Clk)

      Variable Reg, Reg2: Std_Logic_Vector (CRC_Width-1 Downto 0);
      Variable ApplyPoly: std_logic;
   Begin
      
      If Rising_Edge(Clk) Then
	 If Reset = '1' Then
	    if(Calc = '1') then
	       For k In 0 to Nbits Loop
		  if(k = 0) then
		     Reg := (InDirectInitVal);--(CRC_Width-1 downto 0)&dinP(k))xor ('0'&Poly);
		  else
		     If Reg(CRC_Width-1) = '1' then
			Reg := (Reg(CRC_Width-2 downto 0)&din(Nbits - k))xor (Poly);
		     else
			Reg := Reg(CRC_Width-2 downto 0)&din(Nbits - k);
		     End If;
		  end if;
	       End Loop;
	    else
	       Reg := InDirectInitVal;
	    end if;
	 else
	    If Calc = '1' Then
	       For k In 1 to Nbits Loop
		  If Reg(CRC_Width-1) = '1' then
		     Reg := (Reg(CRC_Width-2 downto 0)&din(Nbits - k))xor (Poly);
		  else
		     Reg := Reg(CRC_Width-2 downto 0)&din(Nbits - k);
		  End If;
	       End Loop;
	    else
	       Reg := Reg;
	    End If;
	 end if;

	 --we need one more loop to output the CRC register to the output.
	 For k In 0 to CRC_Width Loop
	    if(k = 0) then
	       Reg2 := Reg;
	    else
	       If Reg2(CRC_Width-1) = '1' then
		  Reg2 := (Reg2(CRC_Width-2 downto 0)&'0')xor (Poly);
	       else
		  Reg2:= Reg2(CRC_Width-2 downto 0)&'0';
	       End If;
	    end if;
	 End Loop;
	 
	 
      End If;

      CRC <= Reg2;--(CRC_width-1 downto 0);
      Reg1_out <= Reg;
   End Process;
end architecture rtl ; -- of CRC_Gen
