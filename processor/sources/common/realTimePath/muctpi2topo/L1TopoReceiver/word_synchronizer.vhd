--******************************************************************************
--*                                                                            *
--*           Checks whether the words are aligned (comma character at         *
--*           the same time) and if not, provides a soft_reset                 *
--*                                                                            *
--*           Frans Schreuder (Nikhef) franss@nikhef.nl                        *
--*                                                                            *
--******************************************************************************


library UNISIM, ieee, work;
use UNISIM.VCOMPONENTS.all;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;


entity word_synchronizer is
port (
  clk160m32           : in    std_logic;
  reset_in            : in    std_logic;
  reset_done_in		  : in	  std_logic;
  gt0_rxdata          : in    std_logic_vector(31 downto 0);
  gt0_rxcharisk       : in    std_logic_vector(3 downto 0);
  gt1_rxdata          : in    std_logic_vector(31 downto 0);
  gt1_rxcharisk       : in    std_logic_vector(3 downto 0);
  reset_out           : out    std_logic);
end entity word_synchronizer;


architecture rtl of word_synchronizer is
	signal delay_counter : std_logic_vector(23 downto 0);
	signal synchronized  : std_logic;
	
  --IDLE = <K28.5>   "0011111010" or "1100000101"
  constant K28_5 : std_logic_vector (7 downto 0) := x"BC";
  --IDLE = <K28.1>   "0011111001" or "1100000110"
  constant K28_1 : std_logic_vector (7 downto 0) := x"3C";
begin

	sync: process ( reset_in, clk160m32)
	begin
		if(reset_in = '1') then
			delay_counter <= (others => '1');
			reset_out <= '0';
			synchronized <= '0';
		elsif(rising_edge(clk160m32)) then
			if(reset_done_in = '1') then
				if(synchronized = '1') then
					reset_out <= '0';
					synchronized <= '1';
				else
					
					if(delay_counter > 0) then
						delay_counter <= delay_counter - 1;
					else
						delay_counter <= delay_counter;
					end if;
						
					reset_out <= '0';
					if(gt0_rxcharisk(0)= '1' and gt0_rxdata(7 downto 0)=K28_5 and
					   gt1_rxcharisk(0)= '1' and gt1_rxdata(7 downto 0)=K28_5) then  --the two channels are locked, and properly aligned together
						
						synchronized <= '1';
					else
						synchronized <= '0';
					end if;
					if(delay_counter < 10) then
						reset_out <= '1';
						
					end if;
					
				end if;
			else
				synchronized <= synchronized ; --0';
				reset_out <= '0';
				delay_counter <= (others => '1');
			end if;
		end if;
	end process;


end architecture rtl;
