#scan individual MIOCT ID and search for max, min eta/phi for 
#given combination of eta and phi code

import argparse

parser = argparse.ArgumentParser(description='This python script printouts muon_type package. Please redirect output to "muon_types.vhd" used in vivado project. Usage: python MioctGeometryToLUT.py > muon_types.vhd In case there is no xml file defining local coordinates please download it from: https://svnweb.cern.ch/cern/wsvn/atlasoff/Trigger/TrigConfiguration/TrigConfMuctpi/tags/TrigConfMuctpi-00-00-14/?')

args = parser.parse_args()


import xml.etree.cElementTree as ET
tree = ET.parse('TestMioctGeometry.xml')
root = tree.getroot()
root.tag
root.attrib
count_octants = 0.


print "library IEEE;"
print "use ieee.std_logic_1164.all;"
print "use ieee.numeric_std.all;"

print "package muon_types is"

print "type MuonEtaLut_a is array (0 to 7) of integer range -31 to 31;"
print "type MuonEtaLut_aa is array (0 to 6) of MuonEtaLut_a;"
print "type MuonEtaLut is array (0 to 15) of MuonEtaLut_aa;"

print "constant MuonEtaTab : MuonEtaLut :="
print "("
for Decode in root.iter('Decode'):
    etacode_saved = "newDecode"
    print "("
    print "(",
    for TopoCell in Decode.findall('TopoCell'):
        ieta = TopoCell.get('ieta')
        etacode = TopoCell.get('etacode')
        phicode = TopoCell.get('phicode')
        if etacode != etacode_saved and etacode_saved != "newDecode":
            print "),"
            print "(",
        etacode_saved = etacode
        if phicode != "0x7":
            print ieta,',', 
       #     print phicode
        else:
            print ieta,
        if phicode == "0x7" and etacode == "0x6":
            print ")",
    if count_octants < 15:
        print "),"
    else:
        print ")"
    count_octants += 1
print ");"

count_octants = 0
print "type MuonPhiLut_a is array (0 to 7) of integer range 0 to 63;"
print "type MuonPhiLut_aa is array (0 to 6) of MuonPhiLut_a;"
print "type MuonPhiLut is array (0 to 15) of MuonPhiLut_aa;"
print "constant MuonPhiTab : MuonPhiLut :="
print "("
for Decode in root.iter('Decode'):
    etacode_saved = "newDecode"
    print "("
    print "(",
    for TopoCell in Decode.findall('TopoCell'):
        ieta = TopoCell.get('ieta')
        iphi = TopoCell.get('iphi')
        etacode = TopoCell.get('etacode')
        phicode = TopoCell.get('phicode')
        if etacode != etacode_saved and etacode_saved != "newDecode":
            print "),"
            print "(",
        etacode_saved = etacode
        if phicode != "0x7":
            print iphi,',', 
       #     print phicode
        else:
            print iphi,
        if phicode == "0x7" and etacode == "0x6":
            print ")",
    if count_octants < 15:
        print "),"
    else:
        print ")"
    count_octants += 1
print ");"


print "type MuonPtLut is array (0 to 5) of integer range 0 to 20;"
print "constant MuonPtTab : MuonPtLut :="
print "(",
for PtEncoding in root.iter('PtEncoding'):
    for PtCodeElement in PtEncoding.findall('PtCodeElement'):
        value = PtCodeElement.get('value')
        pt = PtCodeElement.get('pt')
        if pt != "6":
            print value,',', 
        else:
            print value, 
        
print ");"

print "end muon_types;"
