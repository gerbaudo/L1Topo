----------------------------------------------------------------------------------
-- Company:         Johannes Gutenberg - Universitaet Mainz, Jagiellonian University
-- Engineer:        Christian Kahra, Marek Palka
-- 
-- Create Date:     02/05/2015 07:06:37 PM
-- Module Name:     realTimePath - Behavioral
-- Target Devices:  xc7vx690tffg1927-3
-- Tool Versions:   Xilinx Vivado 2014.4
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

library UNISIM;
use UNISIM.VComponents.all;


use work.l1topo_package.all;
use work.L1TopoDataTypes.all;
use work.L1TopoFunctions.all;
use work.rod_l1_topo_types_const.all;
use work.muon_types.all;

entity realTimeDataPath is
  generic(
    PROCESSOR_NUMBER : integer := 0;
    DEBUG            : integer := 0;
    GTH              : natural := 16;
    GTH_VIRTEX_TO_KINTEX : boolean := TRUE
    );
  port(
    sysclk40          : in std_logic;
    sysclk80          : in std_logic;
    sysclk160         : in std_logic;
    sysclk320         : in std_logic;
    sysclk_pll_locked : in std_logic;
    gtrefclk          : in std_logic_vector(numberOfQuads-1 downto 0);
    gtrxp, gtrxn      : in std_logic_vector(numberOfChannels-1 downto 0);

    ChannelControl : in  arraySLV32(numberOfChannels-1 downto 0);
    ChannelStatus  : out arraySLV32(numberOfChannels-1 downto 0);
    GeneralControl:	in std_logic_vector(31 downto 0);

    topoInput_enablePlayback  : in  std_logic;
    topoInput_playbackData128 : in  arraySLV128(numberOfChannels-1 downto 0);
    topoInput_spyData128      : out arraySLV128(numberOfChannels-1 downto 0);
    rod_dataToAlgorithm128    : out arraySLV128(numberOfChannels-1 downto 0);

    SortParameters : in ParameterSpace(NumberOfSortAlgorithms-1 downto 0);
    AlgoParameters : in ParameterSpace(NumberOfAlgorithms-1 downto 0);

    AlgoResults           : out std_logic_vector(numberOfResultBits-1 downto 0);
    AlgoOverflow          : out std_logic_vector(numberOfResultBits-1 downto 0);
    rateCounter_out       : out arraySLV32(numberOfResultBits-1 downto 0);
    rateCounterOv_out       : out arraySLV32(numberOfResultBits-1 downto 0);
--    rateOvCounter_out:          out arraySLV32(numberOfResultBits-1 downto 0);
    rateCounter_valid_out : out std_logic;
    --ROD
    ROD_DATA          : in std_logic_vector(15 downto 0);
    ROD_DATA_VALID    : in std_logic;
    gttxp, gttxn      : out std_logic_vector(3 downto 0);
    GTH_ROD_CLK_OUT   : out std_logic;
    DATA_IS_CORRUPTED_OUT : out std_logic_vector(numberOfChannels-1 downto 0);
    jetTobs_out       : out JetArray (InputWidthJet - 1 downto 0);
    met_out           : out MetArray (InputWidthMET - 1 downto 0);
    BCID            : out  std_logic_vector(11 downto 0)
    );

end realTimeDataPath;


architecture Behavioral of realTimeDataPath is

--- !! WARNING !! each stage consists of ~10000 (80*128) registers !!------------------------------------------------------------------------ 
  constant playbackSpyDataPipe_numberOfStages : natural := 1;  --constant for the playback AND spy pipe, so x * 2 * 10000 registers
  constant rodDataPipe_numberOfStages         : natural := 1;
---------------------------------------------------------------------------------------------------------------------------------------------
  attribute keep                              : string;
  attribute SHREG_EXTRACT                     : string;

  signal local_reset                     : std_logic;
  signal muon_data_out                   : arraySLV128(2 downto 0);
  signal muon_crc_valid, muon_data_valid : std_logic;

  signal ClockBus       : std_logic_vector(2 downto 0);
  signal toggle         : arraySLV2(numberOfChannels-1 downto 0);
  signal subtickCounter : arraySLV2(numberOfChannels-1 downto 0);


  signal cpllLock       : std_logic_vector(numberOfChannels-1 downto 0);
  signal rxresetdone    : std_logic_vector(numberOfChannels-1 downto 0);
  signal rxfsmresetdone : std_logic_vector(numberOfChannels-1 downto 0);

  signal rxdata32, rxdata32_sel : arraySLV32(numberOfChannels-1 downto 0) := (others => (others => '0'));
  signal rxcharisk4     : arraySLV4(numberOfChannels-1 downto 0);
  signal rxchariscomma4 : arraySLV4(numberOfChannels-1 downto 0);
  signal rxdisperr4     : arraySLV4(numberOfChannels-1 downto 0);
  signal rxdisperr1     : std_logic_vector(numberOfChannels-1 downto 0);
  signal rxnotintable4  : arraySLV4(numberOfChannels-1 downto 0);
  signal rxnotintable1  : std_logic_vector(numberOfChannels-1 downto 0);
  signal rxbufstatus3   : arraySLV3(numberOfChannels-1 downto 0);
  signal rxbufstatus1   : std_logic_vector(numberOfChannels-1 downto 0);

  signal rxbyteisaligned : std_logic_vector(numberOfChannels-1 downto 0);
  signal rxbyterealign   : std_logic_vector(numberOfChannels-1 downto 0);

  signal rxResetError     : std_logic_vector(numberOfChannels-1 downto 0);
  signal rxAlignmentError : std_logic_vector(numberOfChannels-1 downto 0);
  signal rxBufferError    : std_logic_vector(numberOfChannels-1 downto 0);
  signal gthError         : std_logic_vector(numberOfChannels-1 downto 0);
  signal rxCodeError      : std_logic_vector(numberOfChannels-1 downto 0);


  --ChannelControl
  signal ChannelControl_reg    : arraySLV16(numberOfChannels-1 downto 0);
  signal cpllPowerUp_reg       : std_logic_vector(numberOfChannels-1 downto 0);
  signal cpllPowerDown         : std_logic_vector(numberOfChannels-1 downto 0);
  signal softReset_reg         : std_logic_vector(numberOfChannels-1 downto 0);
  signal rxbufreset_reg        : std_logic_vector(numberOfChannels-1 downto 0);
  signal errorCounterReset_reg : std_logic_vector(numberOfChannels-1 downto 0);
  signal manualFineDelay_reg   : arraySLV2(numberOfChannels-1 downto 0);
  signal manualFineDelay_reg_res : arraySLV6(numberOfChannels-1 downto 0);
  signal coarseDelay_reg       : arraySLV4(numberOfChannels-1 downto 0);
  signal coarseDelay_reg_res   : arraySLV6(numberOfChannels-1 downto 0);
  signal rxpolarity_reg        : std_logic_vector(numberOfChannels-1 downto 0);
  signal crateNumber_reg       : arraySLV2(numberOfChannels-1 downto 0);
  signal showChannel_reg       : std_logic_vector(numberOfChannels-1 downto 0);
  signal showComma_reg         : std_logic_vector(numberOfChannels-1 downto 0);


  --ChannelStatus    
  signal ChannelStatus_reg      : arraySLV32(numberOfChannels-1 downto 0);
  signal linkStatus             : std_logic_vector(numberOfChannels-1 downto 0);
  signal cpllLock_reg           : std_logic_vector(numberOfChannels-1 downto 0);
  signal cpllLock_error         : std_logic_vector(numberOfChannels-1 downto 0);
  signal rxresetdone_reg        : std_logic_vector(numberOfChannels-1 downto 0);
  signal rxfsmresetdone_reg     : std_logic_vector(numberOfChannels-1 downto 0);
  signal rxbyteisaligned_reg    : std_logic_vector(numberOfChannels-1 downto 0);
  signal rxbytealignment_error  : std_logic_vector(numberOfChannels-1 downto 0);
  signal rxcodeError_reg        : std_logic_vector(numberOfChannels-1 downto 0);
  signal rxcodeError_error      : std_logic_vector(numberOfChannels-1 downto 0);
  signal rxbufstatus_reg        : arraySLV3(numberOfChannels-1 downto 0);
  signal rxbufstatus_error      : std_logic_vector(numberOfChannels-1 downto 0);
  signal autoFineDelay_reg      : arraySLV2(numberOfChannels-1 downto 0);
  signal fineDelay_error        : std_logic_vector(numberOfChannels-1 downto 0);
  signal fineDelayStateCntr     : arrayUNS5(numberOfChannels-1 downto 0);
  signal subwordRealignment_reg : std_logic_vector(numberOfChannels-1 downto 0);
  signal subwordRealigned_error : std_logic_vector(numberOfChannels-1 downto 0);
  signal gthErrorCounter        : arraySLV8(numberOfChannels-1 downto 0);
  signal crcErrorCounter        : arraySLV8(numberOfChannels-1 downto 0);

  signal rxAlignmentError_shreg : arraySLV4(numberOfChannels-1 downto 0);
  signal rxBufferError_shreg    : arraySLV4(numberOfChannels-1 downto 0);
  signal rxcodeError_shreg      : arraySLV4(numberOfChannels-1 downto 0);


  --PlaybackSpyROD
  signal topoInput_enablePlayback_reg : std_logic_vector(numberOfChannels-1 downto 0);

  signal topoInput_playbackData128_pipe : arrayOfArray80xSLV128(playbackSpyDataPipe_numberOfStages downto 0);
  signal topoInput_spyData128_pipe      : arrayOfArray80xSLV128(playbackSpyDataPipe_numberOfStages downto 0);
  signal rod_dataToAlgorithm128_pipe    : arrayOfArray80xSLV128(0 downto 0);

  attribute SHREG_EXTRACT of topoInput_playbackData128_pipe : signal is "no";
  attribute SHREG_EXTRACT of topoInput_spyData128_pipe      : signal is "no";
  attribute SHREG_EXTRACT of rod_dataToAlgorithm128_pipe    : signal is "no";

  --drp
  signal gt_drp_addr        : std_logic_vector(8 downto 0);
  signal gt_drp_dataIn      : std_logic_vector(15 downto 0);
  signal gt_drp_dataOut     : arraySLV16(numberOfChannels-1 downto 0);
  signal gt_drp_enable      : std_logic_vector(numberOfChannels-1 downto 0);
  signal gt_drp_ready       : std_logic_vector(numberOfChannels-1 downto 0);
  signal gt_drp_writeEnable : std_logic_vector(numberOfChannels-1 downto 0);



  --fineAlignment    
  signal autoFineDelay     : arraySLV2(numberOfChannels-1 downto 0);
  signal autoFineDelay_res : arraySLV6(numberOfChannels-1 downto 0);
  signal totalFineDelay    : arraySLV6(numberOfChannels-1 downto 0);
  signal totalFineDelayUn  : arrayUNS3(numberOfChannels-1 downto 0);
  signal fineDelaySRL_addr : arraySLV3(numberOfChannels-1 downto 0);
  signal fineDelayState    : std_logic_vector(numberOfChannels-1 downto 0);
  signal dv_cntr_rst       : std_logic_vector(numberOfChannels-1 downto 0);
  signal dv_cntr           : arrayUNS2(numberOfChannels-1 downto 0);
    
  signal rxdata32_fineDelayed, rxdata32_fineDelayed_tmp       : arraySLV32(numberOfChannels-1 downto 0);
  signal rxchariscomma4_fineDelayed,rxchariscomma4_fineDelayed_tmp : arraySLV4(numberOfChannels-1 downto 0);
  signal rxcharisk4_fineDelayed, rxcharisk4_fineDelayed_tmp     : arraySLV4(numberOfChannels-1 downto 0);
  signal gthError_fineDelayed, gthError_fineDelayed_tmp       : std_logic_vector(numberOfChannels-1 downto 0);

  signal rxdata32_fineDelay_mux       : arraySLV32(numberOfChannels-1 downto 0);
  signal rxchariscomma4_fineDelay_mux : arraySLV4(numberOfChannels-1 downto 0);
  signal rxcharisk4_fineDelay_mux     : arraySLV4(numberOfChannels-1 downto 0);
  signal gthError_fineDelay_mux       : std_logic_vector(numberOfChannels-1 downto 0);

  signal coarseDelaySRL_addr : arraySLV4(numberOfChannels-1 downto 0);


  signal rxdata128_coarseDelayed       : arraySLV128(numberOfChannels-1 downto 0);
  signal rxchariscomma16_coarseDelayed : arraySLV16(numberOfChannels-1 downto 0);
  signal rxcharisk16_coarseDelayed     : arraySLV16(numberOfChannels-1 downto 0);
  signal data_is_corrupted_coarseDelayed : std_logic_vector(numberOfChannels-1 downto 0);
  
--    signal gthError_coarseDelayed:          std_logic_vector(numberOfChannels-1 downto 0);

  signal rxdata128_coarseDelay_mux       : arraySLV128(numberOfChannels-1 downto 0);
  
  signal rxchariscomma16_coarseDelay_mux : arraySLV16(numberOfChannels-1 downto 0);
  signal rxcharisk16_coarseDelay_mux     : arraySLV16(numberOfChannels-1 downto 0);
  signal data_is_corrupted_coarseDelayed_mux : std_logic_vector(numberOfChannels-1 downto 0);
  
--    signal gthError_coarseDelay_mux:        std_logic_vector(numberOfChannels-1 downto 0);

  signal subwordRealignment                  : std_logic_vector(numberOfChannels-1 downto 0);
  signal rxdata_subwordRealign_buffer        : arraySLV16(numberOfChannels-1 downto 0);
  signal rxchariscomma_subwordRealign_buffer : arraySLV2(numberOfChannels-1 downto 0);
  signal rxcharisk_subwordRealign_buffer     : arraySLV2(numberOfChannels-1 downto 0);
  signal gthError_subwordRealign_buffer      : std_logic_vector(numberOfChannels-1 downto 0);

  signal rxdata32_subwordRealigned       : arraySLV32(numberOfChannels-1 downto 0);
  signal rxchariscomma4_subwordRealigned : arraySLV4(numberOfChannels-1 downto 0);
  signal rxcharisk4_subwordRealigned     : arraySLV4(numberOfChannels-1 downto 0);
  signal gthError_subwordRealigned       : std_logic_vector(numberOfChannels-1 downto 0);
  signal gthError_previous               : std_logic_vector(numberOfChannels-1 downto 0);


  signal rxdata128_deserializerBuffer_shreg : arraySLV128(numberOfChannels-1 downto 0);
  signal rxchariscomma_shreg                : arraySLV16(numberOfChannels-1 downto 0);
  signal rxcharisk_shreg                    : arraySLV16(numberOfChannels-1 downto 0);  --  <-
  signal gthError_shreg                     : arraySLV4(numberOfChannels-1 downto 0);
--    signal gthErrorCounter:                       std_logic_vector(numberOfChannels-1 downto 0);

  signal rxdata128_muon              : arraySLV128(numberOfMuonChannels-1 downto 0);
  signal rxdata128_muon_deserialized : arraySLV128(numberOfMuonChannels-1 downto 0);
  signal muonData_valid              : std_logic;
  signal muonCRC_valid               : std_logic;

  signal rxdata128_deserialized : arraySLV128(numberOfChannels-1 downto 0);
  signal rxdata128_deserialized_muon_before_crc : arraySLV128(numberOfMuonChannels-1 downto 0);
  signal rxchariscomma16        : arraySLV16(numberOfChannels-1 downto 0);
  signal rxcharisk16            : arraySLV16(numberOfChannels-1 downto 0);
  signal gthError4              : arraySLV4(numberOfChannels-1 downto 0);

  signal rxdata128_playbackMux : arraySLV128(numberOfChannels-1 downto 0);

  signal crcError, crcError_reg : std_logic_vector(numberOfChannels-1 downto 0);
  signal data_is_corrupted      : std_logic_vector(numberOfChannels-1 downto 0);
  type data_is_corrupted_array is array (4 downto 0) of std_logic_vector(numberOfChannels-1 downto 0);
  signal data_is_corrupted_pipe: data_is_corrupted_array;
  
  signal showChannel_mux        : std_logic_vector(numberOfChannels-1 downto 0);

  signal emWords  : ClusterWordArray(numberOfEMChannels-1 downto 0);
  signal tauWords : ClusterWordArray(numberOfTauChannels-1 downto 0);
  signal jetWords : JetWordArray(numberOfJetChannels-1 downto 0);

  signal emOverflow  : std_logic_vector(numberOfEMChannels downto 0);
  signal tauOverflow : std_logic_vector(numberOfTauChannels downto 0);
  signal jetOverflow : std_logic_vector(numberOfJetChannels downto 0);
  signal muOverflow : std_logic_vector(InputWidthMU downto 0);

  signal emTOBs, emTOBs_presorted, emTOBs_presorted_ov, emTOBs_presorted_ov_s   : ClusterArray(InputWidthEM - 1 downto 0);
  signal tauTOBs, tauTOBs_presorted, tauTOBs_presorted_ov, tauTOBs_presorted_ov_s : ClusterArray(InputWidthTau - 1 downto 0);
  signal jetTOBs, jetTOBs_presorted, jetTOBs_presorted_ov, jetTOBs_presorted_ov_s : JetArray (InputWidthJet - 1 downto 0);
  signal sumEtTOBs                  : SumEtArray (numberOfSumEtChannels-1 downto 0);
  signal muTOBs                     : MuonArray (InputWidthMU - 1 downto 0);

  signal sumEt_Et : arraySLV16(numberOfSumEtChannels-1 downto 0);
  signal sumEt_Ex : arraySLV16(numberOfSumEtChannels-1 downto 0);
  signal sumEt_Ey : arraySLV16(numberOfSumEtChannels-1 downto 0);

  signal metTOBs, MetTOBs_s : MetArray (InputWidthMET - 1 downto 0);

  signal AlgoResults_net                    : std_logic_vector(numberOfOutputBits-1 downto 0);
  signal AlgoOverflow_net                   : std_logic_vector(numberOfOutputBits-1 downto 0);
  signal algoOutput_pipe, algoOvOutput_pipe : std_logic_vector(numberOfOutputBits-1 downto 0);

  signal timeCounter                : std_logic_vector(27 downto 0);
  signal rateCounter_valid          : std_logic;
  signal rateCounter, rateOvCounter : arraySLV32(numberOfResultBits-1 downto 0);


  signal muon_phi, muon_eta, muon_pt : arraySLV8(31 downto 0);
  signal muon_no_candidate			 : std_logic_vector(31 downto 0);
  
  signal GeneralControl_reg : std_logic_vector(31 downto 0);
  signal disableCRCdataZeroing : std_logic;
  signal txfsmresetdone, txresetdone : std_logic;
  attribute DONT_TOUCH : string;
  attribute MARK_DEBUG : string;
  
  signal muon_phi_ila, muon_eta_ila, muon_pt_ila : arraySLV8(31 downto 0);
  signal qbcid_allign  : arraySLV8(1 downto 0);
  signal bcid_cntr : unsigned(11 downto 0);


  
  attribute DONT_TOUCH of muon_phi_ila, muon_eta_ila, muon_pt_ila : signal is "TRUE";
  attribute MARK_DEBUG of muon_phi_ila, muon_eta_ila, muon_pt_ila : signal is "TRUE";
--to preserve names, its harmless since there are ff before ila and no logic to
--be optimized
begin

  CHANNELCONTROL_REG_GEN : process(sysclk40)
  begin
    if rising_edge(sysclk40) then
      for ch in numberOfChannels-1 downto 0 loop
        ChannelControl_reg(ch)    <= ChannelControl(ch);
        cpllPowerUp_reg(ch)       <= ChannelControl_reg(ch)(0);
        softReset_reg(ch)         <= ChannelControl_reg(ch)(1);
        rxbufreset_reg(ch)        <= ChannelControl_reg(ch)(2);
        errorCounterReset_reg(ch) <= ChannelControl_reg(ch)(3);
        manualFineDelay_reg(ch)   <= ChannelControl_reg(ch)(5 downto 4);
        coarseDelay_reg(ch)       <= ChannelControl_reg(ch)(9 downto 6);
        rxpolarity_reg(ch)        <= ChannelControl_reg(ch)(11);
        crateNumber_reg(ch)       <= ChannelControl_reg(ch)(13 downto 12);
        showChannel_reg(ch)       <= ChannelControl_reg(ch)(14);
        showComma_reg(ch)         <= ChannelControl_reg(ch)(15);
      end loop;
    end if;
  end process;

  CHANNELSTATUS_REG_MAP : process(sysclk40)
  begin
    if rising_edge(sysclk40) then
      for ch in numberOfChannels-1 downto 0 loop
        ChannelStatus_reg(ch)(0)            <= linkStatus(ch);
        ChannelStatus_reg(ch)(1)            <= cpllLock_reg(ch);
        ChannelStatus_reg(ch)(2)            <= cpllLock_error(ch);
        ChannelStatus_reg(ch)(3)            <= rxresetdone_reg(ch) and rxfsmresetdone_reg(ch);
        ChannelStatus_reg(ch)(4)            <= rxbyteisaligned_reg(ch);
        ChannelStatus_reg(ch)(5)            <= rxbytealignment_error(ch);
        ChannelStatus_reg(ch)(6)            <= rxcodeError_reg(ch);
        ChannelStatus_reg(ch)(7)            <= rxcodeError_error(ch);
        ChannelStatus_reg(ch)(10 downto 8)  <= rxbufstatus_reg(ch);
        ChannelStatus_reg(ch)(11)           <= rxbufstatus_error(ch);
        ChannelStatus_reg(ch)(13 downto 12) <= autoFineDelay_reg(ch);
        ChannelStatus_reg(ch)(14)           <= finedelay_error(ch);
        ChannelStatus_reg(ch)(15)           <= subwordRealigned_error(ch);
        ChannelStatus_reg(ch)(23 downto 16) <= gthErrorCounter(ch);
        ChannelStatus_reg(ch)(31 downto 24) <= crcErrorCounter(ch);
      end loop;
    end if;
  end process;

  CPLL_POWERDOWN_GEN : for ch in numberOfChannels-1 downto 0 generate
    cpllPowerDown(ch) <= not cpllPowerUp_reg(ch);
  end generate;

  CHANNELSTATUS_GEN : for ch in numberOfChannels-1 downto 0 generate
    linkStatus(ch) <= '1' when cpllLock_error(ch) = '0'
                      and rxresetdone_reg(ch) = '1'
                      and rxfsmresetdone_reg(ch) = '1'
                      and rxbytealignment_error(ch) = '0'
                      and rxcodeError_error(ch) = '0'
                      and rxbufstatus_error(ch) = '0'
                      and finedelay_error(ch) = '0'
                      and subwordRealigned_error(ch) = '0'
                      and gthErrorCounter(ch) = (7 downto 0 => '0')
                      and crcErrorCounter(ch) = (7 downto 0 => '0')
                      else '0';
    ChannelStatus(ch) <= ChannelStatus_reg(ch);
  end generate;

  process(sysclk160)
  begin
    if rising_edge(sysclk160) then
      for ch in numberOfChannels-1 downto 0 loop
        rxAlignmentError_shreg(ch)(3 downto 1) <= rxAlignmentError_shreg(ch)(2 downto 1) & rxAlignmentError(ch);
        rxBufferError_shreg(ch)(3 downto 1)    <= rxBufferError_shreg(ch)(2 downto 1) & rxBufferError(ch);
        rxCodeError_shreg(ch)(3 downto 1)      <= rxCodeError_shreg(ch)(2 downto 1) & rxcodeError(ch);
      end loop;
    end if;
  end process;

  process(sysclk40)
  begin
    if rising_edge(sysclk40) then
      for ch in numberOfChannels-1 downto 0 loop
        cpllLock_reg(ch)           <= cpllLock(ch);
        rxresetdone_reg(ch)        <= rxresetdone(ch);
        rxfsmresetdone_reg(ch)     <= rxfsmresetdone(ch);
        rxbyteisaligned_reg(ch)    <= rxbyteisaligned(ch);
        rxcodeError_reg(ch)        <= rxCodeError(ch);
        rxbufstatus_reg(ch)        <= rxbufstatus3(ch);
        autoFineDelay_reg(ch)      <= autoFineDelay(ch);
        subwordRealignment_reg(ch) <= subwordRealignment(ch);
        if errorCounterReset_reg(ch) = '1' then
          cpllLock_error(ch)         <= '0';
          rxbytealignment_error(ch)  <= '0';
          rxcodeError_error(ch)      <= '0';
          rxbufstatus_error(ch)      <= '0';
          finedelay_error(ch)        <= '0';
          subwordRealigned_error(ch) <= '0';
        else
          if cpllLock(ch) = '0' then
            cpllLock_error(ch)                                           <= '1'; end if;
          if rxAlignmentError_shreg(ch) /= (3 downto 0 => '0') then
            rxbytealignment_error(ch)     <= '1'; end if;
          if rxBufferError_shreg(ch) /= (3 downto 0    => '0') then
            rxbufstatus_error(ch)         <= '1'; end if;
          if rxcodeError_shreg(ch) /= (3 downto 0      => '0') then
            rxcodeError_error(ch)         <= '1'; end if;
          if autoFineDelay_reg(ch) /= autoFineDelay(ch) and ch < 76 then
            finedelay_error(ch)                  <= '1';
          end if;
          if  ch > 75 then            
            finedelay_error(ch)                  <= '0';
          end if;
          if subwordRealignment_reg(ch) /= subwordRealignment(ch) and ch < 76 then
            subwordRealigned_error(ch) <= '1';
          end if;
          if  ch > 75 then  
            subwordRealigned_error(ch) <= '0';
          end if;     
        end if;
      end loop;
    end if;
  end process;



  QUAD_GEN : for q in numberOfQuads-1 downto 0 generate
    GTH32MUON_GEN : if q = 19 and PROCESSOR_NUMBER /= 1 generate
      QUAD_MUON : entity work.gth32QuadMuons
        generic map (
          DEBUG => 0)
        port map(
          sysclk40          => sysclk40,
          sysclk80          => sysclk80,
          sysclk160         => sysclk160,
          sysclk320         => sysclk320,
          sysclk_pll_locked => sysclk_pll_locked,
          gtrefclk          => gtrefclk(q),

          gtrxp => gtrxp(q*4+3 downto q*4),
          gtrxn => gtrxn(q*4+3 downto q*4),

          rxdata32        => rxdata32(q*4+3 downto q*4),
          rxcharisk4      => rxcharisk4(q*4+3 downto q*4),
          rxchariscomma4  => rxchariscomma4(q*4+3 downto q*4),
          rxnotintable1   => rxnotintable1(q*4+3 downto q*4),
          rxdisperr4      => rxdisperr4(q*4+3 downto q*4),
          rxdisperr1      => rxdisperr1(q*4+3 downto q*4),
          rxnotintable4   => rxnotintable4(q*4+3 downto q*4),
          rxbufstatus3    => rxbufstatus3(q*4+3 downto q*4),
          rxbufstatus1    => rxbufstatus1(q*4+3 downto q*4),
          rxbyteisaligned => rxbyteisaligned(q*4+3 downto q*4),
          rxbyterealign   => rxbyterealign(q*4+3 downto q*4),
          gth_error_out   => gthError4(q*4+3 downto q*4),
          delay(0) =>  coarseDelay_reg(q*4)(1 downto 0) & manualFineDelay_reg(q*4),
          delay(1) =>  coarseDelay_reg(q*4+1)(1 downto 0) & manualFineDelay_reg(q*4+1),
          delay(2) =>  coarseDelay_reg(q*4+2)(1 downto 0) & manualFineDelay_reg(q*4+2),
          delay(3) =>  coarseDelay_reg(q*4+3)(1 downto 0) & manualFineDelay_reg(q*4+3),

          cpllpd   => cpllPowerDown(q*4+3 downto q*4),
          cplllock => cpllLock(q*4+3 downto q*4),

          soft_reset     => softReset_reg(q*4+3 downto q*4),
          rxbufreset     => (others => '0'),--rxbufreset_reg(q*4+3 downto q*4),
          rxpolarity     => rxpolarity_reg(q*4+3 downto q*4),
          rxfsmresetdone => rxfsmresetdone(q*4+3 downto q*4),
          rxresetdone    => rxresetdone(q*4+3 downto q*4),

          gt_drp_addr        => gt_drp_addr,
          gt_drp_dataIn      => gt_drp_dataIn,
          gt_drp_dataOut     => gt_drp_dataOut(q*4+3 downto q*4),
          gt_drp_enable      => gt_drp_enable(q*4+3 downto q*4),
          gt_drp_ready       => gt_drp_ready(q*4+3 downto q*4),
          gt_drp_writeEnable => gt_drp_writeEnable(q*4+3 downto q*4)
          );
    end generate GTH32MUON_GEN;
    
    
    
    GTH16_ROD: if q = 1 and GTH_VIRTEX_TO_KINTEX = TRUE generate
      QUAD : entity work.gth16QuadROD
        port map(
        sysclk40          => sysclk40,
        sysclk80          => sysclk80,
        sysclk160         => sysclk160,
        sysclk320         => sysclk320,
        sysclk_pll_locked => sysclk_pll_locked,
        gtrefclk          => gtrefclk(q),

        gt_rxp => gtrxp(q*4+3 downto q*4),
        gt_rxn => gtrxn(q*4+3 downto q*4),

        cpllpd     => cpllPowerDown(q*4+3 downto q*4),
        soft_reset => softReset_reg(q*4+3 downto q*4),
        rxbufreset => rxbufreset_reg(q*4+3 downto q*4),
        rxpolarity => rxpolarity_reg(q*4+3 downto q*4),

        gt_drp_addr        => gt_drp_addr,
        gt_drp_dataIn      => gt_drp_dataIn,
        gt_drp_dataOut     => gt_drp_dataOut(q*4+3 downto q*4),
        gt_drp_enable      => gt_drp_enable(q*4+3 downto q*4),
        gt_drp_ready       => gt_drp_ready(q*4+3 downto q*4),
        gt_drp_writeEnable => gt_drp_writeEnable(q*4+3 downto q*4),

        rxdata32        => rxdata32(q*4+3 downto q*4),
        rxcharisk4      => rxcharisk4(q*4+3 downto q*4),
        rxchariscomma4  => rxchariscomma4(q*4+3 downto q*4),
        rxdisperr4      => rxdisperr4(q*4+3 downto q*4),
        rxdisperr1      => rxdisperr1(q*4+3 downto q*4),
        rxnotintable4   => rxnotintable4(q*4+3 downto q*4),
        rxnotintable1   => rxnotintable1(q*4+3 downto q*4),
        rxbufstatus3    => rxbufstatus3(q*4+3 downto q*4),
        rxbufstatus1    => rxbufstatus1(q*4+3 downto q*4),
        rxbyteisaligned => rxbyteisaligned(q*4+3 downto q*4),
        rxbyterealign   => rxbyterealign(q*4+3 downto q*4),

        cplllock       => cpllLock(q*4+3 downto q*4),

        rxfsmresetdone => rxfsmresetdone(q*4+3 downto q*4),
        rxresetdone    => rxresetdone(q*4+3 downto q*4),
        --ROD
        tx_data       => ROD_DATA,
        tx_data_valid => ROD_DATA_VALID,
        GTH_ROD_CLK_OUT  => GTH_ROD_CLK_OUT,
        gt_txp => gttxp,
        gt_txn => gttxn,
        txfsmresetdone => txfsmresetdone,
        txresetdone    => txresetdone
        );
    end generate GTH16_ROD;

    GTH16_GEN : if (GTH = 16 and (q /= 19 and q /=9 and q /= 18) and q/= 1) or
                  (GTH = 16 and q = 1 and GTH_VIRTEX_TO_KINTEX = FALSE)
    generate
      QUAD : entity work.gth16Quad port map(
        sysclk40          => sysclk40,
        sysclk80          => sysclk80,
        sysclk160         => sysclk160,
        sysclk320         => sysclk320,
        sysclk_pll_locked => sysclk_pll_locked,
        gtrefclk          => gtrefclk(q),

        gt_rxp => gtrxp(q*4+3 downto q*4),
        gt_rxn => gtrxn(q*4+3 downto q*4),

        cpllpd     => cpllPowerDown(q*4+3 downto q*4),
        soft_reset => softReset_reg(q*4+3 downto q*4),
        rxbufreset => rxbufreset_reg(q*4+3 downto q*4),
        rxpolarity => rxpolarity_reg(q*4+3 downto q*4),

        gt_drp_addr        => gt_drp_addr,
        gt_drp_dataIn      => gt_drp_dataIn,
        gt_drp_dataOut     => gt_drp_dataOut(q*4+3 downto q*4),
        gt_drp_enable      => gt_drp_enable(q*4+3 downto q*4),
        gt_drp_ready       => gt_drp_ready(q*4+3 downto q*4),
        gt_drp_writeEnable => gt_drp_writeEnable(q*4+3 downto q*4),

        rxdata32        => rxdata32(q*4+3 downto q*4),
        rxcharisk4      => rxcharisk4(q*4+3 downto q*4),
        rxchariscomma4  => rxchariscomma4(q*4+3 downto q*4),
        rxdisperr4      => rxdisperr4(q*4+3 downto q*4),
        rxdisperr1      => rxdisperr1(q*4+3 downto q*4),
        rxnotintable4   => rxnotintable4(q*4+3 downto q*4),
        rxnotintable1   => rxnotintable1(q*4+3 downto q*4),
        rxbufstatus3    => rxbufstatus3(q*4+3 downto q*4),
        rxbufstatus1    => rxbufstatus1(q*4+3 downto q*4),
        rxbyteisaligned => rxbyteisaligned(q*4+3 downto q*4),
        rxbyterealign   => rxbyterealign(q*4+3 downto q*4),


        cplllock       => cpllLock(q*4+3 downto q*4),
        rxfsmresetdone => rxfsmresetdone(q*4+3 downto q*4),
        rxresetdone    => rxresetdone(q*4+3 downto q*4)

        );
    end generate GTH16_GEN;

    GTH32_GEN : if (GTH = 32 and (q /= 19 and q /=9 and q /= 18) and q/= 1) or
                  (GTH = 32 and q = 1 and GTH_VIRTEX_TO_KINTEX = FALSE)
    generate
      QUAD : entity work.gth32Quad port map(
        sysclk40          => sysclk40,
        sysclk80          => sysclk80,
        sysclk160         => sysclk160,
        sysclk320         => sysclk320,
        sysclk_pll_locked => sysclk_pll_locked,
        gtrefclk          => gtrefclk(q),

        gt_rxp => gtrxp(q*4+3 downto q*4),
        gt_rxn => gtrxn(q*4+3 downto q*4),

        cpllpd     => cpllPowerDown(q*4+3 downto q*4),
        soft_reset => softReset_reg(q*4+3 downto q*4),
        rxbufreset => rxbufreset_reg(q*4+3 downto q*4),
        rxpolarity => rxpolarity_reg(q*4+3 downto q*4),

        gt_drp_addr        => gt_drp_addr,
        gt_drp_dataIn      => gt_drp_dataIn,
        gt_drp_dataOut     => gt_drp_dataOut(q*4+3 downto q*4),
        gt_drp_enable      => gt_drp_enable(q*4+3 downto q*4),
        gt_drp_ready       => gt_drp_ready(q*4+3 downto q*4),
        gt_drp_writeEnable => gt_drp_writeEnable(q*4+3 downto q*4),

        rxdata32        => rxdata32(q*4+3 downto q*4),
        rxcharisk4      => rxcharisk4(q*4+3 downto q*4),
        rxchariscomma4  => rxchariscomma4(q*4+3 downto q*4),
        rxdisperr4      => rxdisperr4(q*4+3 downto q*4),
        rxdisperr1      => rxdisperr1(q*4+3 downto q*4),
        rxnotintable4   => rxnotintable4(q*4+3 downto q*4),
        rxnotintable1   => rxnotintable1(q*4+3 downto q*4),
        rxbufstatus3    => rxbufstatus3(q*4+3 downto q*4),
        rxbufstatus1    => rxbufstatus1(q*4+3 downto q*4),
        rxbyteisaligned => rxbyteisaligned(q*4+3 downto q*4),
        rxbyterealign   => rxbyterealign(q*4+3 downto q*4),


        cplllock       => cpllLock(q*4+3 downto q*4),
        rxfsmresetdone => rxfsmresetdone(q*4+3 downto q*4),
        rxresetdone    => rxresetdone(q*4+3 downto q*4)

        );
    end generate GTH32_GEN;
    
  end generate QUAD_GEN;

  GEN_MUONS_IF_NOT_TOPO0_U1: if PROCESSOR_NUMBER /= 1 generate 
    L1TopoReceiver_INST : entity work.L1TopoReceiver
      port map (
        clkin160m32      => sysclk160,
        sysclk40         => sysclk40,
        crc_valid        => muon_crc_valid,
        data_valid       => muon_data_valid,
        RxDataOut0       => rxdata128_deserialized(76),--rxdata128_deserialized_muon_before_crc(0),
        RxDataOut1       => rxdata128_deserialized(79),--rxdata128_deserialized_muon_before_crc(1),
        gt0_rxcharisk_in => rxcharisk4(76),
        gt0_rxdata_in    => rxdata32(76),  --A side
        gt1_rxcharisk_in => rxcharisk4(79),
        gt1_rxdata_in    => rxdata32(79),  --C side
        gt2_rxcharisk_in => rxcharisk4(77),
        gt2_rxdata_in    => rxdata32(77),
        reset            => local_reset);
  end generate GEN_MUONS_IF_NOT_TOPO0_U1;

  CREATE_DUMMY_MUON_DATA: if PROCESSOR_NUMBER = 1 generate
     rxdata128_deserialized(76) <= (others => '1');
     rxdata128_deserialized(79) <= (others => '1');
  end generate CREATE_DUMMY_MUON_DATA;
-----------------------------------------------------------
-- Error gen.
-----------------------------------------------------------
  
  GTH_ERRORSIGNALS_GEN : for ch in numberOfChannels-1 downto 0 generate
    EXCLUDE_UNUSED_CH: if active_channels(ch) = '1' or ch = 77 generate --77 is_CRC_for_muons
      rxResetError(ch)     <= '1' when rxresetdone(ch) = '0' or rxfsmresetdone(ch) = '0'    else '0';
      rxAlignmentError(ch) <= '1' when rxbyteisaligned(ch) = '0' or rxbyterealign(ch) = '1' else '0';
      rxCodeError(ch)      <= '1' when rxnotintable1(ch) = '1' or rxdisperr1(ch) = '1'      else '0';
      rxBufferError(ch)    <= '1' when rxbufstatus1(ch) = '1'                               else '0';
      gthError(ch)         <= '1' when rxResetError(ch) = '1' or rxAlignmentError(ch) = '1'
                              or rxCodeError(ch) = '1' or rxBufferError(ch) = '1' or
                              ((rxcharisk4(ch) /= rxchariscomma4(ch) or
                              rxchariscomma4(ch)(1) = '1' or
                              rxchariscomma4(ch)(3) = '1')  and ch <76)                   else '0';
      rxdata32_sel(ch)     <= rxdata32(ch);
    end generate EXCLUDE_UNUSED_CH;
  end generate;


-------------------------------------------------------------------
-- COMMON FINE & COARSE ALIGNMENT
-- -- LATENCY 0 subtick up to coars + fine allignment
-------------------------------------------------------------------
  L1CALO_CHECKFINEDELAY : process(sysclk40)
  begin
    if rising_edge(sysclk40) then
      for ch in numberOfChannels-1 downto 0 loop
        fineDelayStateCntr(ch) <= fineDelayStateCntr(ch) + 1;
        --if fineDelayState(ch) = '0' and (rxchariscomma16(ch)(12) = '1' or rxchariscomma16(ch)(0) = '1' or rxchariscomma16(ch)(4) = '1') then
        --  fineDelayState(ch) <= '1';
        --else
        --  fineDelayState(ch) <= '0';
        --end if;
        
        --if fineDelayState(ch) = '0' then
        if fineDelayStateCntr(ch) = 0 then
          if rxchariscomma16(ch)(12) = '1' then
            autoFineDelay(ch)   <= std_logic_vector(unsigned(autoFineDelay(ch)) + "01");
          elsif rxchariscomma16(ch)(0) = '1' then
            autoFineDelay(ch) <= std_logic_vector(unsigned(autoFineDelay(ch)) + "10");
          elsif rxchariscomma16(ch)(4) = '1' then
            autoFineDelay(ch) <= std_logic_vector(unsigned(autoFineDelay(ch)) + "11");
          else
            autoFineDelay(ch) <= autoFineDelay(ch);
          end if;
        else
          autoFineDelay(ch) <= autoFineDelay(ch);
        end if;
      end loop;
    end if;
  end process;

  DELAY_GEN : for ch in numberOfChannels-1 downto 0 generate
    manualFineDelay_reg_res(ch) <= x"0" & manualFineDelay_reg(ch);
    autoFineDelay_res(ch)       <= x"0" & autoFineDelay(ch);
    coarseDelay_reg_res(ch)     <= coarseDelay_reg(ch) & "00";
    totalFineDelay(ch) <= std_logic_vector(unsigned(manualFineDelay_reg_res(ch)) + unsigned(autoFineDelay_res(ch)) + unsigned(coarseDelay_reg_res(ch)) - 1);
      
    RXDATA_DELAY : entity work.delay_rtdp
        port map (
          Q(31 downto 0)  => rxdata32_fineDelayed(ch),
          Q(35 downto 32) => rxchariscomma4_fineDelayed(ch),
          Q(39 downto 36) => rxcharisk4_fineDelayed(ch),
          Q(40)           => gthError_fineDelayed(ch),             
          A(5 downto 0)   => totalFineDelay(ch),
          CLK => sysclk160,
          D(31 downto 0)  => rxdata32_sel(ch),
          D(35 downto 32) => rxchariscomma4(ch),
          D(39 downto 36) => rxcharisk4(ch),
          D(40)           => gthError(ch)
          );
 
    rxdata32_fineDelay_mux(ch)       <= rxdata32_sel(ch)   when (manualFineDelay_reg(ch)(1 downto 0) = "00" and autoFineDelay(ch)(1 downto 0) = "00" and coarseDelay_reg(ch) = x"0") else rxdata32_fineDelayed(ch);
    rxchariscomma4_fineDelay_mux(ch) <= rxchariscomma4(ch) when (manualFineDelay_reg(ch)(1 downto 0) = "00" and autoFineDelay(ch)(1 downto 0) = "00" and coarseDelay_reg(ch) = x"0") else rxchariscomma4_fineDelayed(ch);
    rxcharisk4_fineDelay_mux(ch)     <= rxcharisk4(ch)     when (manualFineDelay_reg(ch)(1 downto 0) = "00" and autoFineDelay(ch)(1 downto 0) = "00" and coarseDelay_reg(ch) = x"0") else rxcharisk4_fineDelayed(ch);
    gthError_fineDelay_mux(ch)       <= gthError(ch)       when (manualFineDelay_reg(ch)(1 downto 0) = "00" and autoFineDelay(ch)(1 downto 0) = "00" and coarseDelay_reg(ch) = x"0") else gthError_fineDelayed(ch);
  end generate DELAY_GEN;


----------------------------------------------------------------------------------
-- subword realign - here in 16 bit data uperr byte can be mixed with lower -
-- LATENCY 1 + 0.5 subtick
----------------------------------------------------------------------------------
  SUBWORDREALIGN_BUFFER : process(sysclk160)
  begin
    if rising_edge(sysclk160) then
      for ch in numberOfChannels-1 downto 0 loop
        if rxchariscomma4_fineDelay_mux(ch)(2) = '1' then
          subwordRealignment(ch)    <= '1';
        elsif rxchariscomma4_fineDelay_mux(ch)(0) = '1' then
          subwordRealignment(ch) <= '0';
        end if;
        rxdata_subwordRealign_buffer(ch)        <= rxdata32_fineDelay_mux(ch)(15 downto 0);
        rxchariscomma_subwordRealign_buffer(ch) <= rxchariscomma4_fineDelay_mux(ch)(1 downto 0);
        rxcharisk_subwordRealign_buffer(ch)     <= rxcharisk4_fineDelay_mux(ch)(1 downto 0);
        gthError_subwordRealign_buffer(ch)      <= gthError_fineDelay_mux(ch);
      end loop;
    end if;
  end process;

  SUBWORDREALIGN_REG : process(sysclk160)
  begin
    if rising_edge(sysclk160) then
      for ch in numberOfChannels-1 downto 0 loop
        if subwordRealignment(ch) = '1' then
          rxdata32_subwordRealigned(ch)       <= rxdata_subwordRealign_buffer(ch) & rxdata32_fineDelay_mux(ch)(31 downto 16);
          rxchariscomma4_subwordRealigned(ch) <= rxchariscomma_subwordRealign_buffer(ch) & rxchariscomma4_fineDelay_mux(ch)(3 downto 2);
          rxcharisk4_subwordRealigned(ch)     <= rxcharisk_subwordRealign_buffer(ch) & rxcharisk4_fineDelay_mux(ch)(3 downto 2);
          gthError_subwordRealigned(ch)       <= gthError_subwordRealign_buffer(ch);
        else
          rxdata32_subwordRealigned(ch)       <= rxdata32_fineDelay_mux(ch);
          rxchariscomma4_subwordRealigned(ch) <= rxchariscomma4_fineDelay_mux(ch);
          rxcharisk4_subwordRealigned(ch)     <= rxcharisk4_fineDelay_mux(ch);
          gthError_subwordRealigned(ch)       <= gthError_fineDelay_mux(ch);
        end if;
      end loop;
    end if;
  end process;

  
-----------------------------------------------------------------------------------------------------------
-- 32BIT DATA TO 128 BIT
-- LATENCY from max 4+1 subticks
-----------------------------------------------------------------------------------------------------------
  GTHERROR_RISINGEDGE_REG : process(sysclk160)
  begin
    if rising_edge(sysclk160) then
      for ch in numberOfChannels-1 downto 0 loop
        gthError_previous(ch) <= gthError_subwordRealigned(ch);
      end loop;
    end if;
  end process;


  L1CALO_DESERIALIZER_BUFFER_INPUT_GEN : for ch in numberOfChannels-1 downto 0 generate
    rxdata128_deserializerBuffer_shreg(ch)(31 downto 0) <= rxdata32_subwordRealigned(ch);
    rxchariscomma_shreg(ch)(3 downto 0)                 <= rxchariscomma4_subwordRealigned(ch);
    rxcharisk_shreg(ch)(3 downto 0)                     <= rxcharisk4_subwordRealigned(ch);
    gthError_shreg(ch)(0)                               <= '1' when gthError_previous(ch) = '0' and gthError_subwordRealigned(ch) = '1' else '0';
  end generate L1CALO_DESERIALIZER_BUFFER_INPUT_GEN;

  L1CALO_DESERIALIZER_BUFFER : process(sysclk160)
  begin
    if rising_edge(sysclk160) then

      for ch in numberOfChannels-1 downto 0 loop

        rxdata128_deserializerBuffer_shreg(ch)(127 downto 32) <= rxdata128_deserializerBuffer_shreg(ch)(95 downto 0);
        rxchariscomma_shreg(ch)(15 downto 4)                  <= rxchariscomma_shreg(ch)(11 downto 0);
        rxcharisk_shreg(ch)(15 downto 4)                      <= rxcharisk_shreg(ch)(11 downto 0);
        gthError_shreg(ch)(3 downto 1)                        <= gthError_shreg(ch)(2 downto 0);
        
      end loop;

    end if;
  end process;

  L1CALO_DESERIALIZER_REG : process(sysclk40) 
  begin
    if rising_edge(sysclk40) then
      for ch in numberOfChannels-1 downto 0 loop
        crcError_reg(ch) <= crcError(ch);
        topoInput_enablePlayback_reg(ch) <= topoInput_enablePlayback;
        if ch < 76 then
          rxchariscomma16(ch) <= rxchariscomma_shreg(ch);
          rxcharisk16(ch)     <= rxcharisk_shreg(ch);
          gthError4(ch)       <= gthError_shreg(ch);
          if showChannel_reg(ch) = '1' and topoInput_enablePlayback_reg(ch) = '0' then
            rxdata128_deserialized(ch)(127 downto 72) <= rxdata128_deserializerBuffer_shreg(ch)(127 downto 72);
            rxdata128_deserialized(ch)(51 downto 0)   <= rxdata128_deserializerBuffer_shreg(ch)(51 downto 0);
            if showComma_reg(ch) = '0' and rxchariscomma_shreg(ch)(8) = '1' then
              rxdata128_deserialized(ch)(71 downto 52) <= (71 downto 52 => '0');
            else
              rxdata128_deserialized(ch)(71 downto 52) <= rxdata128_deserializerBuffer_shreg(ch)(71 downto 52);
            end if;
            elsif topoInput_enablePlayback_reg(ch) = '1' then
            --dv_cntr_rst(ch) <= '0';
            rxdata128_deserialized(ch) <= topoInput_playbackData128(ch);
          else
            rxdata128_deserialized(ch) <= (others => '0');
          end if;
        end if;
      end loop;
    end if;
  end process;

  DECOUPLE_RTDP_SPY_ROD:  process(sysclk40)
  begin
    if rising_edge(sysclk40) then
      topoInput_spyData128 <= rxdata128_deserialized;
    end if;
  end process DECOUPLE_RTDP_SPY_ROD;
 
----SPY and ROD outputs------------------------------------------------------------------------
  --to alogs
  rod_dataToAlgorithm128 <= rxdata128_deserialized; --here multicyckle
                                                    --constrain to relax timing
                                                    --(see xdc file)
  DATA_IS_CORRUPTED_OUT  <= data_is_corrupted;
  rxdata128_playbackMux  <= rxdata128_deserialized;--rxdata128_coarseDelay_mux;
  data_is_corrupted_coarseDelayed_mux <= (others => '0');--data_is_corrupted(ch);
-------------------------------------------------------------------------------
-- CRC and error counters
-------------------------------------------------------------------------------
  
  L1CALO_CRCCHECK_GEN : for ch in numberOfChannels-1 downto 0 generate
    process(sysclk40)
    begin
      if rising_edge(sysclk40) then
        for ch in numberOfChannels-1 downto 0 loop
          toggle(ch)(0) <= not toggle(ch)(0);
        end loop;
      end if;
    end process;

    process(sysclk160)
    begin
      if rising_edge(sysclk160) then
        for ch in numberOfChannels-1 downto 0 loop
          toggle(ch)(1)                                <= toggle(ch)(0);
          if toggle(ch) = "01" then
            subtickCounter(ch) <= "01";
          else
            subtickCounter(ch) <= std_logic_vector(unsigned(subtickCounter(ch))+1);
          end if;
        end loop;
      end if;
    end process;
    
    CMX_CRC: if ch < 76 generate
      L1CALO_CRCCHECK : entity work.CRC_CHECK_32b
        port map (
          sysclk160          => sysclk160,
          DATA_IN            => rxdata32_subwordRealigned(ch),
          CRC_ERR            => crcError(ch),
--CRC_Just_Done    : out  std_logic:='0';--indicates that CRC_ERR has just been refreshed
--clockBus: in std_logic_vector(3 downto 0);
          rx_subtick_counter => subtickCounter(ch)
          );
    end generate CMX_CRC;
    
    MUON_CRC: if ch > 75 generate
      crcError(ch) <=  not muon_crc_valid; --for time being to check its
                          --behaviuor befor use !
    end generate MUON_CRC;
  end generate L1CALO_CRCCHECK_GEN;

  GeneralControl_reg <= GeneralControl when rising_edge(sysclk160);
  disableCRCdataZeroing <= GeneralControl_reg(0) when rising_edge(sysclk160);
  
  L1CALO_ERRORCOUNTER_REG : process(sysclk40)
  begin
    if rising_edge(sysclk40) then
      for ch in numberOfChannels-1 downto 0 loop
        if errorCounterReset_reg(ch) = '1' then
          gthErrorCounter(ch) <= x"00";
        elsif gthError4(ch) /= "0000" and gthErrorCounter(ch) /= x"ff" then
          gthErrorCounter(ch) <= std_logic_vector(unsigned(gthErrorCounter(ch))+1);
        end if;
        if errorCounterReset_reg(ch) = '1' then
          crcErrorCounter(ch) <= x"00";
        elsif crcError_reg(ch) = '1' and gthError4(ch) = "0000" and crcErrorCounter(ch) /= x"ff" then
          crcErrorCounter(ch) <= std_logic_vector(unsigned(crcErrorCounter(ch))+1);
        end if;
      end loop;
    end if;
  end process;

  L1CALO_DATACORRUPTED_GEN : for ch in numberOfChannels-1 downto 0 generate
    CMX_ERR: if ch <76 generate
      data_is_corrupted(ch) <= '1' when gthError4(ch) /= "0000" or crcError_reg(ch) = '1' 
      or rxchariscomma16(ch)(0) = '1' or rxchariscomma16(ch)(2) = '1' or rxchariscomma16(ch)(4) = '1'
      or rxchariscomma16(ch)(6) = '1' or rxchariscomma16(ch)(10) = '1'
      or rxchariscomma16(ch)(12) = '1' or rxchariscomma16(ch)(14) = '1'
      else '0';
    end generate CMX_ERR;
    MUON_ERR: if ch> 75 generate
      data_is_corrupted(ch) <= '1' when gthError4(ch) /= "0000" or crcError_reg(ch) = '1'
                               else '0';
    end generate MUON_ERR;
  end generate L1CALO_DATACORRUPTED_GEN;



------------------------------------------------------------------------------------------
-- Decoders
------------------------------------------------------------------------------------------
-- EM
  emOverflow(numberOfEMChannels) <= '0';
  L1CALO_EM_DECODER_GEN : for em_ch in numberOfEMChannels-1 downto 0 generate
    emWords(em_ch)                   <= decodeClusterWord(rxdata128_playbackMux(em_channel(PROCESSOR_NUMBER, em_ch)), crateNumber_reg(em_channel(PROCESSOR_NUMBER, em_ch))) 
    	when ((data_is_corrupted_coarseDelayed_mux(em_channel(PROCESSOR_NUMBER, em_ch)) = '0' or
              disableCRCdataZeroing = '1' or
              topoInput_enablePlayback_reg(em_channel(PROCESSOR_NUMBER, em_ch)) = '1')) 
        else emptyClusterWord;
    emTOBs(5*em_ch+4 downto 5*em_ch) <= emWords(em_ch).tobs;
    emOverflow(em_ch)                <= emWords(em_ch).overflow or emOverflow(em_ch+1);  -- generate or'ed overflow of all 4*6 em channels                                               
  end generate L1CALO_EM_DECODER_GEN;

--  PIPELINE_EM : process(sysclk160)
--  begin
--    if rising_edge(sysclk160) then
      emTOBs_presorted             <= remapPresortedClusterTOBs(emTOBs);
      
  L1CALO_EM_INCLOVERFLOW_GEN : for i in emTOBs_presorted'length - 1 downto 0 generate
 	emTOBs_presorted_ov(i).Et <= emTOBs_presorted(i).Et;
 	emTOBs_presorted_ov(i).Eta <= emTOBs_presorted(i).Eta;
 	emTOBs_presorted_ov(i).Phi <= emTOBs_presorted(i).Phi;
 	emTOBs_presorted_ov(i).Isol <= emTOBs_presorted(i).Isol;
  end generate;
      
  emTOBs_presorted_ov(0).Overflow <= emOverflow(0);
--    end if;
--  end process;
  
--TAU
  tauOverflow(numberOfTauChannels) <= '0';

  L1CALO_TAU_DECODER_GEN : for tau_ch in numberOfTauChannels-1 downto 0 generate
    tauWords(tau_ch)                    <= decodeClusterWord(rxdata128_playbackMux(tau_channel(PROCESSOR_NUMBER, tau_ch)), crateNumber_reg(tau_channel(PROCESSOR_NUMBER, tau_ch))) 
    	when (data_is_corrupted_coarseDelayed_mux(tau_channel(PROCESSOR_NUMBER, tau_ch)) = '0' or
              disableCRCdataZeroing = '1' or
              topoInput_enablePlayback_reg(tau_channel(PROCESSOR_NUMBER, tau_ch)) = '1')
        else emptyClusterWord;
    tauTOBs(5*tau_ch+4 downto 5*tau_ch) <= tauWords(tau_ch).tobs;
    tauOverflow(tau_ch)                 <= tauWords(tau_ch).overflow or tauOverflow(tau_ch+1);  -- generate or'ed overflow of all 4*6 tau channels
  end generate L1CALO_TAU_DECODER_GEN;

--  PIPELINE_TAU : process(sysclk160)
--  begin
--    if rising_edge(sysclk160) then
      tauTOBs_presorted             <= remapPresortedClusterTOBs(tauTOBs);
      
  L1CALO_TAU_INCLOVERFLOW_GEN : for i in tauTOBs_presorted'length - 1 downto 0 generate
 	tauTOBs_presorted_ov(i).Et <= tauTOBs_presorted(i).Et;
 	tauTOBs_presorted_ov(i).Eta <= tauTOBs_presorted(i).Eta;
 	tauTOBs_presorted_ov(i).Phi <= tauTOBs_presorted(i).Phi;
 	tauTOBs_presorted_ov(i).Isol <= tauTOBs_presorted(i).Isol;
  end generate;
 
 tauTOBs_presorted_ov(0).Overflow <= tauOverflow(0);
--    end if;
--  end process;

--JET  
  jetOverflow(numberOfJetChannels) <= '0';

  L1CALO_JET_DECODER_GEN : for jet_ch in numberOfJetChannels-1 downto 0 generate
    jetWords(jet_ch)                    <= decodeJetWord(rxdata128_playbackMux(jet_channel(jet_ch)), crateNumber_reg(jet_channel(jet_ch))(0)) 
    	when (data_is_corrupted_coarseDelayed_mux(jet_channel(jet_ch)) = '0' or
              disableCRCdataZeroing = '1' or
              topoInput_enablePlayback_reg(jet_channel(jet_ch)) = '1')
        else emptyJetWord;
    jetTOBs(4*jet_ch+3 downto 4*jet_ch) <= jetWords(jet_ch).tobs;
    jetOverflow(jet_ch)                 <= jetWords(jet_ch).overflow or jetOverflow(jet_ch+1);  -- generate or'ed overflow of all 2*8 jet channels
  end generate L1CALO_JET_DECODER_GEN;

--  PIPELINE_JET : process(sysclk160)
--  begin
--    if rising_edge(sysclk160) then
      jetTOBs_presorted             <= remapPresortedJetTOBs(jetTOBs);
      
  L1CALO_ATU_INCLOVERFLOW_GEN : for i in jetTOBs_presorted'length - 1 downto 0 generate
 	jetTOBs_presorted_ov(i).Et1 <= jetTOBs_presorted(i).Et1;
 	jetTOBs_presorted_ov(i).Et2 <= jetTOBs_presorted(i).Et2;
 	jetTOBs_presorted_ov(i).Eta <= jetTOBs_presorted(i).Eta;
 	jetTOBs_presorted_ov(i).Phi <= jetTOBs_presorted(i).Phi;
  end generate;
      
      jetTOBs_presorted_ov(0).Overflow <= jetOverflow(0);
--    end if;
--  end process;

--SUM ET  
  L1CALO_SUMET_DECODER_GEN : for sumEt_ch in numberOfSumEtChannels-1 downto 0 generate
    sumEtTOBs(sumEt_ch) <= decodeSumEtTOB(rxdata128_playbackMux(sumEt_channel(sumEt_ch))) 
    	when (data_is_corrupted_coarseDelayed_mux(sumEt_channel(sumEt_ch)) = '0' or
              disableCRCdataZeroing = '1' or
              topoInput_enablePlayback_reg(sumEt_channel(sumEt_ch)) = '1')
        else emptySumEtTOB;
  end generate L1CALO_SUMET_DECODER_GEN;

  metTOBs(0) <= calculateMissingEt(sumEtTOBs(0), sumEtTOBs(1));

  BCID <= rxdata128_playbackMux(sumEt_channel(0))(63 downto 52);
  met_out <= metTOBs;

--Muon 
  MUON_SIDES: for j in 0 to 1 generate
    DECODE_MUON_COORDINATES: for i in 0 to 15 generate

-- don't remove, this is to decode real values for PT if reqired now it is only
-- 2 bits so no decoding required to reduce complexity in later stages:
-- std_logic_vector(to_signed(MuonPtTab(to_integer(unsigned(rxdata128_playbackMux(j)(1 + i*8 downto 0 + i*8)))),8));
      
      muon_eta(j*16+i) <= std_logic_vector(to_signed(MuonEtaTab(i/2+8-j*8)(std_int(rxdata128_playbackMux(j*3+76)(7 + i*8 downto 5 + i*8)))(std_int(rxdata128_playbackMux(j*3+76)(4 + i*8 downto 2 + i*8))),8))
                          when ((rxdata128_playbackMux(j*3+76)(7 + i*8 downto 5 + i*8) /= "111") and
                                data_is_corrupted_coarseDelayed_mux(j*3+76) = '0')
                          else (others => '0');
  
      muon_phi(j*16+i) <= std_logic_vector(to_signed(MuonPhiTab(i/2+8-j*8)(std_int(rxdata128_playbackMux(j*3+76)(7 + i*8 downto 5 + i*8)))(std_int(rxdata128_playbackMux(j*3+76)(4 + i*8 downto 2 + i*8))),8))
                          when ((rxdata128_playbackMux(j*3+76)(7 + i*8 downto 5 + i*8) /= "111") and
                                data_is_corrupted_coarseDelayed_mux(j*3+76) = '0')
                          else (others => '0');
      
      muon_pt(j*16+i) <=  x"0" & "00" & rxdata128_playbackMux(j*3+76)((i+1)*8-7 downto i*8)
                          when ((rxdata128_playbackMux(j*3+76)(7 + i*8 downto 5 + i*8) /= "111") and
                                data_is_corrupted_coarseDelayed_mux(j*3+76) = '0')
                          else (others => '0');
      muon_no_candidate(j*16+i) <= '0' when ((rxdata128_playbackMux(j*3+76)(7 + i*8 downto 5 + i*8) /= "111") and
                                data_is_corrupted_coarseDelayed_mux(j*3+76) = '0')
                          else '1';
    end generate DECODE_MUON_COORDINATES;
--muon test
    BC_ID_CNTR: process (sysclk160)
    begin
      if rising_edge(sysclk160) then
        if bcid_cntr = 3563 then
          bcid_cntr <= (others => '0');
        else
          bcid_cntr <= bcid_cntr + 1;
        end if;
      end if;
    end process;
    
    SAVE_BCID_FOR_ALLIGN : process(sysclk160)
    begin
      if rising_edge(sysclk160) then
        if muon_eta(j*16) /= x"00" or muon_eta(j*16+1) /= x"00" then
          qbcid_allign(j) <= std_logic_vector(bcid_cntr(7 downto 0));
        else
          qbcid_allign(j) <= qbcid_allign(j);
        end if;
      end if;
    end process SAVE_BCID_FOR_ALLIGN;
    PIPE_ILA : process(sysclk160)
    begin
      if rising_edge(sysclk160) then
        muon_eta_ila <= muon_eta;
        muon_phi_ila <= muon_phi;
        muon_pt_ila <= muon_pt;
      end if;
    end process PIPE_ILA;
  end generate MUON_SIDES;
  
  gen_mu_tobs : for i in 31 downto 0 generate
    muTOBs(i) <= decodeMuonTOB(muon_pt(i), muon_eta(i), muon_phi(i), muon_no_candidate(i));
  end generate;
  
--------------------------------------------------------
-- algorithms
--------------------------------------------------------

  ClockBus(0) <= sysclk40;
  ClockBus(1) <= sysclk80;
  ClockBus(2) <= sysclk160;

  jetTobs_out <= jetTOBs_presorted_ov;

-------------------------------------------------------------------------------
-- select algorithms top module according to processor number
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

  --SYNCH_ALGO_DATA: process(sysclk40)
  --  begin
  --    if rising_edge(sysclk40) then
  --      emTOBs_presorted_ov_s <= emTOBs_presorted_ov;
  --      tauTOBs_presorted_ov_s <= tauTOBs_presorted_ov;
  --      jetTOBs_presorted_ov_s <= jetTOBs_presorted_ov;
  --      muTOBs_ov_s <= muTOBs_ov;
  --      MetTOBs_s <= MetTOBs;
  --    end if;
  --  end process;
        
  selectAlgoTopModule1 : if PROCESSOR_NUMBER = 1 generate
    algo : entity work.L1TopoAlgorithms_U1
      port map(
        ClockBus       => ClockBus,
        EmTobArray     => emTOBs_presorted_ov,
        TauTobArray    => tauTOBs_presorted_ov,
        JetTobArray    => jetTOBs_presorted_ov,
        MuonTobArray   => muTOBs,
        MetTobArray    => MetTOBs,
        Parameters     => AlgoParameters,
        SortParameters => SortParameters,
        Results        => AlgoResults_net,
        Overflow       => AlgoOverflow_net
        );
  end generate;
  
  selectAlgoTopModule2 : if PROCESSOR_NUMBER = 2 generate
    algo : entity work.L1TopoAlgorithms_U2
      port map(
        ClockBus       => ClockBus,
        EmTobArray     => emTOBs_presorted_ov,
        TauTobArray    => tauTOBs_presorted_ov,
        JetTobArray    => jetTOBs_presorted_ov,
        MuonTobArray   => muTOBs,
        MetTobArray    => MetTOBs,
        Parameters     => AlgoParameters,
        SortParameters => SortParameters,
        Results        => AlgoResults_net,
        Overflow       => AlgoOverflow_net
        );
  end generate;
  
  selectAlgoTopModule3 : if PROCESSOR_NUMBER = 3 generate
    algo : entity work.L1TopoAlgorithms_U3
      port map(
        ClockBus       => ClockBus,
        EmTobArray     => emTOBs_presorted_ov,
        TauTobArray    => tauTOBs_presorted_ov,
        JetTobArray    => jetTOBs_presorted_ov,
        MuonTobArray   => muTOBs,
        MetTobArray    => MetTOBs,
        Parameters     => AlgoParameters,
        SortParameters => SortParameters,
        Results        => AlgoResults_net,
        Overflow       => AlgoOverflow_net
        );
  end generate;
  
  selectAlgoTopModule4 : if PROCESSOR_NUMBER = 4 generate
    algo : entity work.L1TopoAlgorithms_U4
      port map(
        ClockBus       => ClockBus,
        EmTobArray     => emTOBs_presorted_ov,
        TauTobArray    => tauTOBs_presorted_ov,
        JetTobArray    => jetTOBs_presorted_ov,
        MuonTobArray   => muTOBs,
        MetTobArray    => MetTOBs,
        Parameters     => AlgoParameters,
        SortParameters => SortParameters,
        Results        => AlgoResults_net,
        Overflow       => AlgoOverflow_net
        );
  end generate;

  AlgoResults  <= AlgoResults_net;
  AlgoOverflow <= AlgoOverflow_net;

  process(sysclk40)
  begin
    if rising_edge(sysclk40) then
      algoOutput_pipe   <= AlgoResults_net or AlgoOverflow_net;
      algoOvOutput_pipe <= AlgoOverflow_net;
      if rateCounter_valid = '1' then
        timeCounter <= (timeCounter'range => '0');
      else
        timeCounter <= std_logic_vector(unsigned(timeCounter)+1);
      end if;
      for i in numberOfResultBits-1 downto 0 loop
        if rateCounter_valid = '1' then
          rateCounter(i) <= (others => '0');
        elsif algoOutput_pipe(i) = '1' then
          rateCounter(i) <= std_logic_vector(unsigned(rateCounter(i))+1);
        end if;
      end loop;
      for i in numberOfResultBits-1 downto 0 loop
        if rateCounter_valid = '1' then
          rateOvCounter(i) <= (others => '0');
        elsif algoOvOutput_pipe(i) = '1' then
          rateOvCounter(i) <= std_logic_vector(unsigned(rateOvCounter(i))+1);
        end if;
      end loop;
    end if;
  end process;

  rateCounter_valid     <= '1' when timeCounter = B"0010_0110_0011_1001_0010_0111_1111" else '0';
  rateCounter_valid_out <= rateCounter_valid;
  rateCounter_out       <= rateCounter;
  rateCounterOv_out       <= rateOvCounter;


------------------------------------------------------------------------------------------
-- DEBUG ONLY
------------------------------------------------------------------------------------------
      --MUON_COORD_ILA_INST_CA : entity work.muon_coord_ila
    --  port map (
    --    clk     => sysclk160,
    --    probe0  => muon_eta_ila(0 + j*16),
    --    probe1  => muon_eta_ila(1 + j*16),
    --    probe2  => muon_eta_ila(2 + j*16),
    --    probe3  => muon_eta_ila(3 + j*16),
    --    probe4  => muon_eta_ila(4 + j*16),
    --    probe5  => muon_eta_ila(5 + j*16),
    --    probe6  => muon_eta_ila(6 + j*16),
    --    probe7  => muon_eta_ila(7 + j*16),
    --    probe8  => muon_eta_ila(8 + j*16),
    --    probe9  => muon_eta_ila(9 + j*16),
    --    probe10 => muon_eta_ila(10 + j*16),
    --    probe11 => muon_eta_ila(11 + j*16),
    --    probe12 => muon_eta_ila(12 + j*16),
    --    probe13 => muon_eta_ila(13 + j*16),
    --    probe14 => muon_eta_ila(14 + j*16),
    --    probe15 => muon_eta_ila(15 + j*16),
    --    probe16 => muon_phi_ila(0 + j*16),
    --    probe17 => muon_phi_ila(1 + j*16),
    --    probe18 => muon_phi_ila(2 + j*16),
    --    probe19 => muon_phi_ila(3 + j*16),
    --    probe20 => muon_phi_ila(4 + j*16),
    --    probe21 => muon_phi_ila(5 + j*16),
    --    probe22 => muon_phi_ila(6 + j*16),
    --    probe23 => muon_phi_ila(7 + j*16),
    --    probe24 => muon_phi_ila(8 + j*16),
    --    probe25 => muon_phi_ila(9 + j*16),
    --    probe26 => muon_phi_ila(10 + j*16),
    --    probe27 => muon_phi_ila(11 + j*16),
    --    probe28 => muon_phi_ila(12 + j*16),
    --    probe29 => muon_phi_ila(13 + j*16),
    --    probe30 => muon_phi_ila(14 + j*16),
    --    probe31 => muon_phi_ila(15 + j*16),
    --    probe32 => muon_pt_ila(0 + j*16),
    --    probe33 => muon_pt_ila(1 + j*16),
    --    probe34 => muon_pt_ila(2 + j*16),
    --    probe35 => muon_pt_ila(3 + j*16),
    --    probe36 => muon_pt_ila(4 + j*16),
    --    probe37 => muon_pt_ila(5 + j*16),
    --    probe38 => muon_pt_ila(6 + j*16),
    --    probe39 => muon_pt_ila(7 + j*16),
    --    probe40 => muon_pt_ila(8 + j*16),
    --    probe41 => muon_pt_ila(9 + j*16),
    --    probe42 => muon_pt_ila(10 + j*16),
    --    probe43 => muon_pt_ila(11 + j*16),
    --    probe44 => muon_pt_ila(12 + j*16),
    --    probe45 => muon_pt_ila(13 + j*16),
    --    probe46 => muon_pt_ila(14 + j*16),
    --    probe47 => muon_pt_ila(15 + j*16)
    --    );

  --ENEBLE_ILAS_FOR_FINE_DELAY: for ch in 0 to 79 generate
  --  ENEBLE_CMX_IND_ILAS: if ch = 0 or ch = 6 or ch = 12 or ch = 18 or ch = 24 or ch = 30 or ch = 40 or ch = 46 or ch = 52 or ch = 56  generate 
  --    FINE_DELAY_ILA_INST: entity work.fine_delay_ila
  --      port map(
  --        CLK => sysclk40,
  --        probe0 => rxdata128_deserialized(ch),
  --        probe1 => autoFineDelay(ch)(1 downto 0),
  --        probe2 => manualFineDelay_reg(ch)(1 downto 0),
  --        probe3 => coarseDelay_reg(ch)(3 downto 0),
  --        probe4(0) => fineDelayStateCntr(ch)(4),--fineDelayState(ch),
  --        probe5 => rxchariscomma16(ch),
  --        probe6 => totalFineDelay(ch)(5 downto 0)
  --        );
  --    end generate ENEBLE_CMX_IND_ILAS;
  --ENEBLE_MUON_IND_ILAS: if ch = 76 or ch = 79 generate
  --    FINE_DELAY_ILA_INST: entity work.fine_delay_ila
  --      port map(
  --        CLK => sysclk40,
  --        probe0 => rxdata128_deserialized(ch),
  --        probe1 => (others => '0'),
  --        probe2 => manualFineDelay_reg(ch)(1 downto 0),
  --        probe3 => coarseDelay_reg(ch)(3 downto 0),
  --        probe4(0) => '0',
  --        probe5 => (others => '0'),
  --        probe6 => (others => '0')
  --        );
  --    end generate ENEBLE_MUON_IND_ILAS;
  -- end generate ENEBLE_ILAS_FOR_FINE_DELAY;
  

--  ILA_DATA: entity work.muon_dataflow_ila
--  port map (
--    CLK => sysclk160,
--    probe0 => rxdata128_deserialized(76)(7 downto 0),
--    probe1 => rxdata128_deserialized(79)(7 downto 0),
--    probe2 =>rxdata128_coarseDelayed(76)(7 downto 0),
--    probe3 =>rxdata128_coarseDelayed(79)(7 downto 0),
--    probe4 => rxdata128_coarseDelay_mux(76)(7 downto 0),
--    probe5 => rxdata128_coarseDelay_mux(79)(7 downto 0),
--    probe6 => topoInput_spyData128(76)(7 downto 0),
--    probe7 => topoInput_spyData128(79)(7 downto 0)
--  );

  --ILA_RTDP_INFR_CH26 : entity work.ila_rtdp_infr
  --  port map(
  --    clk        => sysclk160,
  --    probe0     => rxdata32(76),
  --    probe1     => rxdata32_fineDelay_mux(76),
  --    probe2     => rxdata128_coarseDelay_mux(76)(31 downto 0),
  --    probe3     => rxdata32_subwordRealigned(76),
  --    probe4     => rxdata128_deserializerBuffer_shreg(76)(127 downto 32),
  --    probe5     => rxdata128_deserialized(76),
  --    probe6     => rxchariscomma4(76),
  --    probe7     => rxchariscomma4_fineDelay_mux(76),
  --    probe8     => rxchariscomma16_coarseDelay_mux(76)(3 downto 0),
  --    probe9(0)  => subwordRealignment(76),
  --    probe10    => rxchariscomma4_subwordRealigned(76),
  --    probe11    => rxchariscomma_shreg(76)(15 downto 4),
  --    probe12    => rxchariscomma16(76),
  --    probe13    => autoFineDelay(76),
  --    probe14    => fineDelaySRL_addr(76),
  --    probe15(0) => fineDelayState(76),
  --    probe16(0) => gthError(76),
  --    probe17(0) => gthError_fineDelay_mux(76),
  --    probe18(0) => '0',              --gthError_coarseDelay_mux(76),
  --    probe19(0) => gthError_subwordRealigned(76),
  --    probe20(0) => gthError_previous(76),
  --    probe21    => gthError_shreg(76)(3 downto 1),
  --    probe22    => gthErrorCounter(76),
  --    probe23(0) => crcError_reg(76),
  --    probe24    => crcErrorCounter(76),
  --    probe25(0) => data_is_corrupted(76)
  --    );

 
  
  DEBUG_GEN : if DEBUG = 1 generate
    ALL_QUADS : for ch in 0 to 80 generate
      SELECT_QUADS: if ch = 5 or ch = 11 or ch = 17 or ch = 23 or ch = 29 or ch = 35 or ch = 45 or ch = 51 or ch = 52 or ch = 59 or ch = 67 generate
    ILA_RTDP_QUAD_CH5 : entity work.ila_rtdp_quad
      port map(
        clk        => sysclk160,
        probe0     => rxdata32(ch),
        probe1     => rxcharisk4(ch),
        probe2     => rxchariscomma4(ch),
        probe3     => rxdisperr4(ch),
        probe4     => rxnotintable4(ch),
        probe5     => rxbufstatus3(ch),
        probe6(0)  => rxbyteisaligned(ch),
        probe7(0)  => rxbyterealign(ch),
        probe8(0)  => cpllPowerDown(ch),
        probe9(0)  => cpllLock(ch),
        probe10(0) => softReset_reg(ch),
        probe11(0) => rxbufreset_reg(ch),
        probe12(0) => rxpolarity_reg(ch),
        probe13(0) => rxfsmresetdone(ch),
        probe14(0) => rxresetdone(ch)
        );
    end generate SELECT_QUADS;
  end generate ALL_QUADS;
      
    ILA_RTDP_INFR_CH26 : entity work.ila_rtdp_infr
      port map(
        clk        => sysclk160,
        probe0     => rxdata32(26),
        probe1     => rxdata32_fineDelay_mux(26),
        probe2     => rxdata128_coarseDelay_mux(26)(31 downto 0),
        probe3     => rxdata32_subwordRealigned(26),
        probe4     => rxdata128_deserializerBuffer_shreg(26)(127 downto 32),
        probe5     => rxdata128_deserialized(26),
        probe6     => rxchariscomma4(26),
        probe7     => rxchariscomma4_fineDelay_mux(26),
        probe8     => rxchariscomma16_coarseDelay_mux(26)(3 downto 0),
        probe9(0)  => subwordRealignment(26),
        probe10    => rxchariscomma4_subwordRealigned(26),
        probe11    => rxchariscomma_shreg(26)(15 downto 4),
        probe12    => rxchariscomma16(26),
        probe13    => autoFineDelay(26),
        probe14    => "000000",--fineDelaySRL_addr(26),
        probe15(0) => fineDelayState(26),
        probe16(0) => gthError(26),
        probe17(0) => gthError_fineDelay_mux(26),
        probe18(0) => '0',              --gthError_coarseDelay_mux(26),
        probe19(0) => gthError_subwordRealigned(26),
        probe20(0) => gthError_previous(26),
        probe21    => gthError_shreg(26)(3 downto 1),
        probe22    => gthErrorCounter(26),
        probe23(0) => crcError_reg(26),
        probe24    => crcErrorCounter(26),
        probe25(0) => data_is_corrupted(26)
        );

    ILA_RTDP_CLUSTER_DECODE_CH17 : entity work.ila_cluster_decode
      port map(
        clk       => sysclk40,
        probe0    => rxdata128_playbackMux(17),
        probe1(0) => topoInput_enablePlayback_reg(17),
        probe2(0) => showChannel_reg(17),
        probe3(0) => data_is_corrupted(17),
        probe4(0) => showComma_reg(17),
        probe5    => crateNumber_reg(17),
        probe6(0) => tauWords(11).overflow,
        probe7    => tauTOBs(55).Et,
        probe8    => tauTOBs(55).Isol,
        probe9    => tauTOBs(55).Eta,
        probe10   => tauTOBs(55).Phi,
        probe11   => tauTOBs(56).Et,
        probe12   => tauTOBs(56).Isol,
        probe13   => tauTOBs(56).Eta,
        probe14   => tauTOBs(56).Phi,
        probe15   => tauTOBs(57).Et,
        probe16   => tauTOBs(57).Isol,
        probe17   => tauTOBs(57).Eta,
        probe18   => tauTOBs(57).Phi,
        probe19   => tauTOBs(58).Et,
        probe20   => tauTOBs(58).Isol,
        probe21   => tauTOBs(58).Eta,
        probe22   => tauTOBs(58).Phi,
        probe23   => tauTOBs(59).Et,
        probe24   => tauTOBs(59).Isol,
        probe25   => tauTOBs(59).Eta,
        probe26   => tauTOBs(59).Phi
        );

    ILA_RTDP_JET_DECODE_CH67 : entity work.ila_jet_decode
      port map(
        clk       => sysclk40,
        probe0    => rxdata128_playbackMux(67),
        probe1(0) => topoInput_enablePlayback_reg(67),
        probe2(0) => showChannel_reg(67),
        probe3(0) => data_is_corrupted(67),
        probe4(0) => showComma_reg(67),
        probe5(0) => crateNumber_reg(67)(0),
        probe6(0) => jetWords(11).overflow,
        probe7    => jetTOBs(44).Et1,
        probe8    => jetTOBs(44).Et2,
        probe9    => jetTOBs(44).Eta,
        probe10   => jetTOBs(44).Phi,
        probe11   => jetTOBs(45).Et1,
        probe12   => jetTOBs(45).Et2,
        probe13   => jetTOBs(45).Eta,
        probe14   => jetTOBs(45).Phi,
        probe15   => jetTOBs(46).Et1,
        probe16   => jetTOBs(46).Et2,
        probe17   => jetTOBs(46).Eta,
        probe18   => jetTOBs(46).Phi,
        probe19   => jetTOBs(47).Et1,
        probe20   => jetTOBs(47).Et2,
        probe21   => jetTOBs(47).Eta,
        probe22   => jetTOBs(47).Phi
        );

    ILA_RTDP_SUMET_DECODE_CH54 : entity work.ila_rtdp_sumet_decode
      port map(
        clk        => sysclk40,
        probe0(0)  => topoInput_enablePlayback_reg(52),
        probe1     => rxdata128_playbackMux(52),
        probe2(0)  => showChannel_reg(52),
        probe3(0)  => data_is_corrupted(52),
        probe4     => rxdata128_playbackMux(54),
        probe5(0)  => showChannel_reg(54),
        probe6(0)  => data_is_corrupted(54),
        probe7     => sumEtTOBs(0).Et,
        probe8     => sumEtTOBs(0).Ex,
        probe9     => sumEtTOBs(0).Ey,
        probe10    => sumEtTOBs(1).Et,
        probe11    => sumEtTOBs(1).Ex,
        probe12    => sumEtTOBs(1).Ey,
        probe13    => metTOBs(0).Et,
        probe14    => metTOBs(0).Ex,
        probe15    => metTOBs(0).Ey,
        probe16(0) => metTOBs(0).Overflow
        );

  end generate;
end Behavioral;

