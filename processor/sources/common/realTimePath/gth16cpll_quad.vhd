----------------------------------------------------------------------------------
-- Company:     Johannes Gutenberg - Universitaet Mainz
-- Engineer:    Christian Kahra
-- 
-- Create Date: 02/08/2015 02:06:05 PM
-- Design Name: 
-- Module Name: gth16_quad - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;


use work.l1topo_package.all;
use work.L1TopoGTConfiguration.all;


entity gth16cpll_quad is 
generic(
    debugQuad: std_logic_vector(19 downto 0) := (19 downto 0 => '0');
    quad: natural := 0
);
port(
    sysclk40:               in  std_logic;
    sysclk80:               in  std_logic;
    sysclk320:              in  std_logic;
    sysclk_pll_locked:      in  std_logic;
--    crystalclk62_5:         in  std_logic;
--    crystalclk_pll_locked:  in  std_logic;
    gtrefclk:               in  std_logic;
    gt_rxp, gt_rxn:         in  std_logic_vector( 3 downto 0); 
    QuadControl:            in  std_logic_vector(31 downto 0);
    QuadStatus:             out std_logic_vector(31 downto 0);
    ChannelControl:         in  arraySLV32(3 downto 0);
    ChannelStatus20bit:     out arraySLV20(3 downto 0);
    rxdata:                 out arraySLV16(3 downto 0);
    rxcharisk:              out arraySLV2( 3 downto 0);
    rxchariscomma:          out arraySLV2( 3 downto 0)
);

end gth16cpll_quad;




architecture Behavioral of gth16cpll_quad is

    signal ChannelControl_reg:                  arraySLV4(3 downto 0);
    signal ChannelStatus_reg:                   arraySLV20(3 downto 0);
    signal QuadControl_reg:                     std_logic_vector(2 downto 0);
    signal QuadStatus_reg:                      std_logic_vector(3 downto 0);

    signal rxoutclk, rxoutclk_bufh:             std_logic_vector(3 downto 0);
    signal rxdata_i:                            arraySLV16(3 downto 0);
    signal rxcharisk_i:                         arraySLV2( 3 downto 0);
    signal rxchariscomma_i:                     arraySLV2( 3 downto 0);

    signal cpllPowerUp_vio:                     std_logic_vector(3 downto 0);
    signal cpllPowerDown:                       std_logic_vector(3 downto 0);
    signal soft_reset,      soft_reset_vio:     std_logic;
    signal errorReset,      errorReset_vio:     std_logic;
    signal rxpolarity,      rxpolarity_vio:     std_logic_vector(3 downto 0);        
--    signal qpllLock:                std_logic;
--    signal qpllRefClkLost:          std_logic;
--    signal qpllReset:               std_logic;
    signal cpllfbclklost:           std_logic_vector(3 downto 0);
    signal cpllrefclklost:           std_logic_vector(3 downto 0);
    signal cplllock:                std_logic_vector(3 downto 0);
    signal cpllpd:                  std_logic_vector(3 downto 0);
    signal cpllreset:               std_logic_vector(3 downto 0);
    
    
    
    signal rxresetdone:             std_logic_vector(3 downto 0);
    signal rxfsmresetdone:          std_logic_vector(3 downto 0);
    signal rxnotintable:            arraySLV2( 3 downto 0);
    signal rxdisperr:               arraySLV2( 3 downto 0);
    signal rxbufstatus:             arraySLV3( 3 downto 0);
    signal rxbyteisaligned:         std_logic_vector(3 downto 0);
    signal rxbyterealign:           std_logic_vector(3 downto 0);
    
    signal cpllLock_reg:            std_logic_vector(3 downto 0);
    signal cpllRefClkLost_reg:      std_logic_vector(3 downto 0);
    signal cpllLock_error:          std_logic_vector(3 downto 0);
    signal cpllRefClkLost_error:    std_logic_vector(3 downto 0);
    signal cpllFbClkLost_reg:       std_logic_vector(3 downto 0);
    signal cpllFbClkLost_error:     std_logic_vector(3 downto 0);
    
    signal rxoutclk_counter:        arraySLV8(3 downto 0);
    signal rxresetdone_reg:         std_logic_vector(3 downto 0);
    signal rxfsmresetdone_reg:   std_logic_vector(3 downto 0);        
    signal rxcodeerr_shreg:         arraySLV8(3 downto 0);
    signal rxcode_error:            arraySLV8(3 downto 0);
    
    signal rxbufstatus_error:       std_logic_vector(3 downto 0);
    signal rxbyteisaligned_reg:     std_logic_vector(3 downto 0);
    signal rxbyterealign_shreg:     arraySLV8(3 downto 0);
    signal rxbyterealign_error:     arraySLV8(3 downto 0);

    signal rxdata_shreg, rxdata128: std_logic_vector(127 downto 0);
    signal rxcharisk_shreg, rxcharisk16: std_logic_vector(15 downto 0);
    signal rxchariscomma_shreg, rxchariscomma16: std_logic_vector(15 downto 0);
    
    signal rxcodeerr8: std_logic_vector(7 downto 0);
    signal rxbufstatus_shreg, rxbufstatus24: std_logic_vector(23 downto 0);
    signal rxbyterealign8: std_logic_vector(7 downto 0);
    
    component ila_gth16cpllQuad
    	Port(
    		clk     : in STD_LOGIC;
    		probe0  : in STD_LOGIC_VECTOR(3 downto 0);
    		probe1  : in STD_LOGIC_VECTOR(3 downto 0);
    		probe2  : in STD_LOGIC_VECTOR(3 downto 0);
    		probe3  : in STD_LOGIC_VECTOR(3 downto 0);
    		probe4  : in STD_LOGIC_VECTOR(3 downto 0);
    		probe5  : in STD_LOGIC_VECTOR(7 downto 0);
    		probe6  : in STD_LOGIC_VECTOR(7 downto 0);
    		probe7  : in STD_LOGIC_VECTOR(7 downto 0);
    		probe8  : in STD_LOGIC_VECTOR(7 downto 0);
    		probe9  : in STD_LOGIC_VECTOR(127 downto 0);
    		probe10 : in STD_LOGIC_VECTOR(15 downto 0);
    		probe11 : in STD_LOGIC_VECTOR(15 downto 0);
    		probe12 : in STD_LOGIC_VECTOR(7 downto 0);
    		probe13 : in STD_LOGIC_VECTOR(23 downto 0);
    		probe14 : in STD_LOGIC_VECTOR(7 downto 0);
    		probe15 : in STD_LOGIC_VECTOR(3 downto 0)
    	);
    end component;

    component vio_0
    	Port(
    		clk        : in  STD_LOGIC;
    		probe_in0  : in  STD_LOGIC_VECTOR(3 downto 0);
    		probe_in1  : in  STD_LOGIC_VECTOR(3 downto 0);
    		probe_in2  : in  STD_LOGIC_VECTOR(0 to 0);
    		probe_in3  : in  STD_LOGIC_VECTOR(3 downto 0);
    		probe_in4  : in  STD_LOGIC_VECTOR(0 to 0);
    		probe_out0 : out STD_LOGIC_VECTOR(3 downto 0);
    		probe_out1 : out STD_LOGIC_VECTOR(3 downto 0);
    		probe_out2 : out STD_LOGIC_VECTOR(0 to 0);
    		probe_out3 : out STD_LOGIC_VECTOR(3 downto 0);
    		probe_out4 : out STD_LOGIC_VECTOR(0 to 0)
    	);
    end component;

begin


    
process(sysclk40) begin
    if rising_edge(sysclk40) then
    
        QuadControl_reg     <= QuadControl(2 downto 0);
    
        
        for ch in 3 downto 0 loop
        
            ChannelControl_reg(ch)              <= ChannelControl(ch)(3 downto 0);
        
            ChannelStatus_reg(ch)(0)            <= rxresetdone_reg(ch);
            ChannelStatus_reg(ch)(1)            <= rxfsmresetdone_reg(ch);
            ChannelStatus_reg(ch)(2)            <= rxbyteisaligned_reg(ch);
            ChannelStatus_reg(ch)(3)            <= rxbufstatus_error(ch);
            ChannelStatus_reg(ch)(11 downto  4) <= rxcode_error(ch);
            ChannelStatus_reg(ch)(19 downto 12) <= rxbyterealign_error(ch);
            
        end loop;
        
    end if;
end process;
    
    
    ChannelStatus20bit      <= ChannelStatus_reg;
    
    
    
    soft_reset      <= '1' when QuadControl(0)='1'      or soft_reset_vio    ='1' else '0';
    errorReset      <= '1' when QuadControl(1)='1'      or errorReset_vio    ='1' else '0';
    
    
ChannelSignals_GEN: for ch in 3 downto 0 generate begin    
    
    cpllPowerDown(ch)      <= '0' when ChannelControl_reg(ch)(0)='1'   or cpllPowerUp_vio(ch)='1' else '1';
    rxpolarity(ch)         <= '1' when ChannelControl_reg(ch)(1)='1'   or rxpolarity_vio(ch) ='1' else '0';   
    
end generate;
    
    
    


QUADINIT: entity work.gtwizard_gth16Quad_init
    generic map
(
        EXAMPLE_SIM_GTRESET_SPEEDUP   => "TRUE",
        EXAMPLE_SIMULATION            => 0,
 
        STABLE_CLOCK_PERIOD           => 12,
        EXAMPLE_USE_CHIPSCOPE         => 0
)
port map
(
        SYSCLK_IN                       =>      sysclk80,
        SOFT_RESET_IN                   =>      soft_reset,
        DONT_RESET_ON_DATA_ERROR_IN     =>      '1',
    GT0_TX_FSM_RESET_DONE_OUT => open,
    GT0_RX_FSM_RESET_DONE_OUT => rxfsmresetdone(0),
    GT0_DATA_VALID_IN => '1',
    GT1_TX_FSM_RESET_DONE_OUT => open,
    GT1_RX_FSM_RESET_DONE_OUT => rxfsmresetdone(1),
    GT1_DATA_VALID_IN => '1',
    GT2_TX_FSM_RESET_DONE_OUT => open,
    GT2_RX_FSM_RESET_DONE_OUT => rxfsmresetdone(2),
    GT2_DATA_VALID_IN => '1',
    GT3_TX_FSM_RESET_DONE_OUT => open,
    GT3_RX_FSM_RESET_DONE_OUT => rxfsmresetdone(3),
    GT3_DATA_VALID_IN => '1',

    --_________________________________________________________________________
    --GT0  (X0Y0)
    --____________________________CHANNEL PORTS________________________________
    --------------------------------- CPLL Ports -------------------------------
        gt0_cpllfbclklost_out           =>      cpllfbclklost(0),
        gt0_cplllock_out                =>      cplllock(0),
        gt0_cplllockdetclk_in           =>      sysclk80,
        gt0_cpllpd_in                   =>      cpllPowerDown(0),
        gt0_cpllreset_in                =>      cpllreset(0),
    -------------------------- Channel - Clocking Ports ------------------------
        gt0_gtrefclk0_in                =>      gtrefclk,
    ---------------------------- Channel - DRP Ports  --------------------------
        gt0_drpaddr_in                  =>      "000000000",
        gt0_drpclk_in                   =>      sysclk80,
        gt0_drpdi_in                    =>      x"0000",
        gt0_drpdo_out                   =>      open,
        gt0_drpen_in                    =>      '0',
        gt0_drprdy_out                  =>      open,
        gt0_drpwe_in                    =>      '0',
    --------------------- RX Initialization and Reset Ports --------------------
        gt0_eyescanreset_in             =>      '0',
        gt0_rxuserrdy_in                =>      sysclk_pll_locked,
    -------------------------- RX Margin Analysis Ports ------------------------
        gt0_eyescandataerror_out        =>      open,
        gt0_eyescantrigger_in           =>      '0',
    ------------------- Receive Ports - Digital Monitor Ports ------------------
        gt0_dmonitorout_out             =>      open,
    ------------------ Receive Ports - FPGA RX Interface Ports -----------------
        gt0_rxusrclk_in                 =>      sysclk320,
        gt0_rxusrclk2_in                =>      sysclk320,
    ------------------ Receive Ports - FPGA RX interface Ports -----------------
        gt0_rxdata_out                  =>      rxdata_i(0),
    ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
        gt0_rxdisperr_out               =>      rxdisperr(0),
        gt0_rxnotintable_out            =>      rxnotintable(0),
    ------------------------ Receive Ports - RX AFE Ports ----------------------
        gt0_gthrxn_in                   =>      gt_rxn(0),
    ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
        gt0_rxbufreset_in               =>      '0',
        gt0_rxbufstatus_out             =>      rxbufstatus(0),
    -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
        gt0_rxbyteisaligned_out         =>      rxbyteisaligned(0),
        gt0_rxbyterealign_out           =>      rxbyterealign(0),
    --------------------- Receive Ports - RX Equalizer Ports -------------------
        gt0_rxmonitorout_out            =>      open,
        gt0_rxmonitorsel_in             =>      "00",
    --------------- Receive Ports - RX Fabric Output Control Ports -------------
        gt0_rxoutclk_out                =>      rxoutclk(0),
    ------------- Receive Ports - RX Initialization and Reset Ports ------------
        gt0_gtrxreset_in                =>      '0',
        gt0_rxpcsreset_in               =>      '0',
        gt0_rxpmareset_in               =>      '0',
    ----------------- Receive Ports - RX Polarity Control Ports ----------------
        gt0_rxpolarity_in               =>      rxpolarity(0),
    ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
        gt0_rxchariscomma_out           =>      rxchariscomma_i(0),
        gt0_rxcharisk_out               =>      rxcharisk_i(0),
    ------------------------ Receive Ports -RX AFE Ports -----------------------
        gt0_gthrxp_in                   =>      gt_rxp(0),
    -------------- Receive Ports -RX Initialization and Reset Ports ------------
        gt0_rxresetdone_out             =>      rxresetdone(0),
    --------------------- TX Initialization and Reset Ports --------------------
        gt0_gttxreset_in                =>      '0',

    --GT1  (X0Y1)
    --____________________________CHANNEL PORTS________________________________
    --------------------------------- CPLL Ports -------------------------------
        gt1_cpllfbclklost_out           =>      cpllfbclklost(1),
        gt1_cplllock_out                =>      cplllock(1),
        gt1_cplllockdetclk_in           =>      sysclk80,
        gt1_cpllpd_in                   =>      cpllPowerDown(1),
        gt1_cpllreset_in                =>      cpllreset(1),
    -------------------------- Channel - Clocking Ports ------------------------
        gt1_gtrefclk0_in                =>      gtrefclk,
    ---------------------------- Channel - DRP Ports  --------------------------
        gt1_drpaddr_in                  =>      "000000000",
        gt1_drpclk_in                   =>      sysclk80,
        gt1_drpdi_in                    =>      x"0000",
        gt1_drpdo_out                   =>      open,
        gt1_drpen_in                    =>      '0',
        gt1_drprdy_out                  =>      open,
        gt1_drpwe_in                    =>      '0',
    --------------------- RX Initialization and Reset Ports --------------------
        gt1_eyescanreset_in             =>      '0',
        gt1_rxuserrdy_in                =>      sysclk_pll_locked,
    -------------------------- RX Margin Analysis Ports ------------------------
        gt1_eyescandataerror_out        =>      open,
        gt1_eyescantrigger_in           =>      '0',
    ------------------- Receive Ports - Digital Monitor Ports ------------------
        gt1_dmonitorout_out             =>      open,
    ------------------ Receive Ports - FPGA RX Interface Ports -----------------
        gt1_rxusrclk_in                 =>      sysclk320,
        gt1_rxusrclk2_in                =>      sysclk320,
    ------------------ Receive Ports - FPGA RX interface Ports -----------------
        gt1_rxdata_out                  =>      rxdata_i(1),
    ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
        gt1_rxdisperr_out               =>      rxdisperr(1),
        gt1_rxnotintable_out            =>      rxnotintable(1),
    ------------------------ Receive Ports - RX AFE Ports ----------------------
        gt1_gthrxn_in                   =>      gt_rxn(1),
    ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
        gt1_rxbufreset_in               =>      '0',
        gt1_rxbufstatus_out             =>      rxbufstatus(1),
    -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
        gt1_rxbyteisaligned_out         =>      rxbyteisaligned(1),
        gt1_rxbyterealign_out           =>      rxbyterealign(1),
    --------------------- Receive Ports - RX Equalizer Ports -------------------
        gt1_rxmonitorout_out            =>      open,
        gt1_rxmonitorsel_in             =>      "00",
    --------------- Receive Ports - RX Fabric Output Control Ports -------------
        gt1_rxoutclk_out                =>      rxoutclk(1),
    ------------- Receive Ports - RX Initialization and Reset Ports ------------
        gt1_gtrxreset_in                =>      '0',
        gt1_rxpcsreset_in               =>      '0',
        gt1_rxpmareset_in               =>      '0',
    ----------------- Receive Ports - RX Polarity Control Ports ----------------
        gt1_rxpolarity_in               =>      rxpolarity(1),
    ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
        gt1_rxchariscomma_out           =>      rxchariscomma_i(1),
        gt1_rxcharisk_out               =>      rxcharisk_i(1),
    ------------------------ Receive Ports -RX AFE Ports -----------------------
        gt1_gthrxp_in                   =>      gt_rxp(1),
    -------------- Receive Ports -RX Initialization and Reset Ports ------------
        gt1_rxresetdone_out             =>      rxresetdone(1),
    --------------------- TX Initialization and Reset Ports --------------------
        gt1_gttxreset_in                =>      '0',

    --GT2  (X0Y2)
    --____________________________CHANNEL PORTS________________________________
    --------------------------------- CPLL Ports -------------------------------
        gt2_cpllfbclklost_out           =>      cpllfbclklost(2),
        gt2_cplllock_out                =>      cplllock(2),
        gt2_cplllockdetclk_in           =>      sysclk80,
        gt2_cpllpd_in                   =>      cpllPowerDown(2),
        gt2_cpllreset_in                =>      cpllreset(2),
    -------------------------- Channel - Clocking Ports ------------------------
        gt2_gtrefclk0_in                =>      gtrefclk,
    ---------------------------- Channel - DRP Ports  --------------------------
        gt2_drpaddr_in                  =>      "000000000",
        gt2_drpclk_in                   =>      sysclk80,
        gt2_drpdi_in                    =>      x"0000",
        gt2_drpdo_out                   =>      open,
        gt2_drpen_in                    =>      '0',
        gt2_drprdy_out                  =>      open,
        gt2_drpwe_in                    =>      '0',
    --------------------- RX Initialization and Reset Ports --------------------
        gt2_eyescanreset_in             =>      '0',
        gt2_rxuserrdy_in                =>      sysclk_pll_locked,
    -------------------------- RX Margin Analysis Ports ------------------------
        gt2_eyescandataerror_out        =>      open,
        gt2_eyescantrigger_in           =>      '0',
    ------------------- Receive Ports - Digital Monitor Ports ------------------
        gt2_dmonitorout_out             =>      open,
    ------------------ Receive Ports - FPGA RX Interface Ports -----------------
        gt2_rxusrclk_in                 =>      sysclk320,
        gt2_rxusrclk2_in                =>      sysclk320,
    ------------------ Receive Ports - FPGA RX interface Ports -----------------
        gt2_rxdata_out                  =>      rxdata_i(2),
    ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
        gt2_rxdisperr_out               =>      rxdisperr(2),
        gt2_rxnotintable_out            =>      rxnotintable(2),
    ------------------------ Receive Ports - RX AFE Ports ----------------------
        gt2_gthrxn_in                   =>      gt_rxn(2),
    ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
        gt2_rxbufreset_in               =>      '0',
        gt2_rxbufstatus_out             =>      rxbufstatus(2),
    -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
        gt2_rxbyteisaligned_out         =>      rxbyteisaligned(2),
        gt2_rxbyterealign_out           =>      rxbyterealign(2),
    --------------------- Receive Ports - RX Equalizer Ports -------------------
        gt2_rxmonitorout_out            =>      open,
        gt2_rxmonitorsel_in             =>      "00",
    --------------- Receive Ports - RX Fabric Output Control Ports -------------
        gt2_rxoutclk_out                =>      rxoutclk(2),
    ------------- Receive Ports - RX Initialization and Reset Ports ------------
        gt2_gtrxreset_in                =>      '0',
        gt2_rxpcsreset_in               =>      '0',
        gt2_rxpmareset_in               =>      '0',
    ----------------- Receive Ports - RX Polarity Control Ports ----------------
        gt2_rxpolarity_in               =>      rxpolarity(2),
    ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
        gt2_rxchariscomma_out           =>      rxchariscomma_i(2),
        gt2_rxcharisk_out               =>      rxcharisk_i(2),
    ------------------------ Receive Ports -RX AFE Ports -----------------------
        gt2_gthrxp_in                   =>      gt_rxp(2),
    -------------- Receive Ports -RX Initialization and Reset Ports ------------
        gt2_rxresetdone_out             =>      rxresetdone(2),
    --------------------- TX Initialization and Reset Ports --------------------
        gt2_gttxreset_in                =>      '0',
    --GT3  (X0Y3)
    --____________________________CHANNEL PORTS________________________________
    --------------------------------- CPLL Ports -------------------------------
        gt3_cpllfbclklost_out           =>      cpllfbclklost(3),
        gt3_cplllock_out                =>      cplllock(3),
        gt3_cplllockdetclk_in           =>      sysclk80,
        gt3_cpllpd_in                   =>      cpllPowerDown(3),
        gt3_cpllreset_in                =>      cpllreset(3),
    -------------------------- Channel - Clocking Ports ------------------------
        gt3_gtrefclk0_in                =>      gtrefclk,
    ---------------------------- Channel - DRP Ports  --------------------------
        gt3_drpaddr_in                  =>      "000000000",
        gt3_drpclk_in                   =>      sysclk80,
        gt3_drpdi_in                    =>      x"0000",
        gt3_drpdo_out                   =>      open,
        gt3_drpen_in                    =>      '0',
        gt3_drprdy_out                  =>      open,
        gt3_drpwe_in                    =>      '0',
    --------------------- RX Initialization and Reset Ports --------------------
        gt3_eyescanreset_in             =>      '0',
        gt3_rxuserrdy_in                =>      sysclk_pll_locked,
    -------------------------- RX Margin Analysis Ports ------------------------
        gt3_eyescandataerror_out        =>      open,
        gt3_eyescantrigger_in           =>      '0',
    ------------------- Receive Ports - Digital Monitor Ports ------------------
        gt3_dmonitorout_out             =>      open,
    ------------------ Receive Ports - FPGA RX Interface Ports -----------------
        gt3_rxusrclk_in                 =>      sysclk320,
        gt3_rxusrclk2_in                =>      sysclk320,
    ------------------ Receive Ports - FPGA RX interface Ports -----------------
        gt3_rxdata_out                  =>      rxdata_i(3),
    ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
        gt3_rxdisperr_out               =>      rxdisperr(3),
        gt3_rxnotintable_out            =>      rxnotintable(3),
    ------------------------ Receive Ports - RX AFE Ports ----------------------
        gt3_gthrxn_in                   =>      gt_rxn(3),
    ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
        gt3_rxbufreset_in               =>      '0',
        gt3_rxbufstatus_out             =>      rxbufstatus(3),
    -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
        gt3_rxbyteisaligned_out         =>      rxbyteisaligned(3),
        gt3_rxbyterealign_out           =>      rxbyterealign(3),
    --------------------- Receive Ports - RX Equalizer Ports -------------------
        gt3_rxmonitorout_out            =>      open,
        gt3_rxmonitorsel_in             =>      "00",
    --------------- Receive Ports - RX Fabric Output Control Ports -------------
        gt3_rxoutclk_out                =>      rxoutclk(3),
    ------------- Receive Ports - RX Initialization and Reset Ports ------------
        gt3_gtrxreset_in                =>      '0',
        gt3_rxpcsreset_in               =>      '0',
        gt3_rxpmareset_in               =>      '0',
    ----------------- Receive Ports - RX Polarity Control Ports ----------------
        gt3_rxpolarity_in               =>      rxpolarity(3),
    ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
        gt3_rxchariscomma_out           =>      rxchariscomma_i(3),
        gt3_rxcharisk_out               =>      rxcharisk_i(3),
    ------------------------ Receive Ports -RX AFE Ports -----------------------
        gt3_gthrxp_in                   =>      gt_rxp(3),
    -------------- Receive Ports -RX Initialization and Reset Ports ------------
        gt3_rxresetdone_out             =>      rxresetdone(3),
    --------------------- TX Initialization and Reset Ports --------------------
        gt3_gttxreset_in                =>      '0',

    --____________________________COMMON PORTS________________________________
     GT0_QPLLOUTCLK_IN  => '0',
     GT0_QPLLOUTREFCLK_IN => '0' ,


    cpllrefclklost_out  => cpllrefclklost
    
);



    rxdata <= rxdata_i;
    rxcharisk <= rxcharisk_i;
    rxchariscomma <= rxchariscomma_i;



CPLLSTATUS_GEN: for ch in 3 downto 0 generate begin

--RXOUTCLK_BUF: BUFH
--port map(
--    I => rxoutclk(ch),
--    O => rxoutclk_bufh(ch)
--);

process(sysclk80, cpllLock(ch)) begin
    if cpllLock(ch)='0' then cpllLock_error(ch) <= '1';
    elsif rising_edge(sysclk80) then
        if errorReset='1' then cpllLock_error(ch) <='0'; end if;
    end if;
end process;


process(sysclk80, cpllRefClkLost(ch)) begin
    if cpllrefclklost(ch)='0' then cpllRefClkLost_error(ch) <= '1';
    elsif rising_edge(sysclk80) then
        if errorReset='1' then cpllRefClkLost_error(ch) <='0'; end if;
    end if;
end process;


process(sysclk80, cpllfbclklost(ch)) begin
    if cpllfbclklost(ch)='0' then cpllfbclklost_error(ch) <= '1';
    elsif rising_edge(sysclk80) then
        if errorReset='1' then cpllfbclklost_error(ch) <='0'; end if;
    end if;
end process;


process(sysclk80) begin
    if rising_edge(sysclk80) then
        cpllLock_reg(ch)            <= cpllLock(ch);
        cpllRefClkLost_reg(ch)      <= cpllRefClkLost(ch);
        cpllFbClkLost_reg(ch)       <= cpllFbClkLost(ch);
        rxresetdone_reg(ch)         <= rxresetdone(ch);
        rxfsmresetdone_reg(ch)      <= rxfsmresetdone(ch);
        rxbyteisaligned_reg(ch)     <= rxbyteisaligned(ch);
    end if;
end process;

end generate;


process(sysclk80) begin
    if rising_edge(sysclk80) then 
        
        for ch in 3 downto 0 loop
        
            if errorReset='1' then rxcode_error(ch) <= x"00";
            elsif rxcode_error(ch)/= x"ff" and rxcodeerr_shreg(ch) /= x"00" then
                rxcode_error(ch) <= std_logic_vector(unsigned(rxcode_error(ch))+1);
            end if;
            
            if errorReset='1' then rxbufstatus_error(ch) <= '0';
            elsif rxbufstatus(ch)/="000" then rxbufstatus_error(ch) <= '1';
            end if;
            
            if errorReset='1' then rxbyterealign_error(ch) <= x"00";
            elsif rxbyterealign_error(ch)/= x"ff" and rxbyterealign_shreg(ch) /= x"00" then
                rxbyterealign_error(ch) <= std_logic_vector(unsigned(rxbyterealign_error(ch))+1);
            end if;
        
        end loop;
    end if;
end process;


process(sysclk320) begin
    if rising_edge(sysclk320) then
        
        for ch in 3 downto 0 loop
            
            if rxnotintable(ch)/="00" or rxdisperr(ch)/="00" then rxcodeerr_shreg(ch)(0) <= '1';
            else rxcodeerr_shreg(ch)(0) <= '0';
            end if;
            rxcodeerr_shreg(ch)(7 downto 1) <= rxcodeerr_shreg(ch)(6 downto 0);
            
            rxbyterealign_shreg(ch)(0) <= rxbyterealign(ch);
            rxbyterealign_shreg(ch)(7 downto 1) <= rxbyterealign_shreg(ch)(6 downto 0);
        
        end loop;
        
    end if;
end process;



--RXOUTCLKCOUNTER_GEN: for i in 3 downto 0 generate begin
--process(rxoutclk_bufh(i)) begin
--    if rising_edge(rxoutclk_bufh(i)) then rxoutclk_counter(i) <= std_logic_vector(unsigned(rxoutclk_counter(i))+1); end if;
--end process;
--end generate;



DEBUG_ON_GEN: if debugQuad(quad)='1' generate begin

process(sysclk320) begin
    if rising_edge(sysclk320) then
    
        rxdata_shreg(15 downto 0) <= rxdata_i(0);
        rxdata_shreg(127 downto 16) <= rxdata_shreg(111 downto 0);
        
        rxcharisk_shreg(1 downto 0) <= rxcharisk_i(0);
        rxcharisk_shreg(15 downto 2) <= rxcharisk_shreg(13 downto 0);
        
        rxchariscomma_shreg(1 downto 0) <= rxchariscomma_i(0);
        rxchariscomma_shreg(15 downto 2) <= rxchariscomma_shreg(13 downto 0);
        
        rxbufstatus_shreg(2 downto 0) <= rxbufstatus(0);
        rxbufstatus_shreg(23 downto 3) <= rxbufstatus_shreg(20 downto 0);
                     
    end if;
end process;





process(sysclk40) begin
    if rising_edge(sysclk40) then
    
        rxdata128 <= rxdata_shreg;
        rxcharisk16 <= rxcharisk_shreg;
        rxchariscomma16 <= rxchariscomma_shreg;
        rxcodeerr8 <= rxcodeerr_shreg(0);
        rxbufstatus24 <= rxbufstatus_shreg;
        rxbyterealign8 <= rxbyterealign_shreg(0);
                        
    end if;
end process;


ila: ila_gth16cpllQuad
PORT MAP (
	clk     => sysclk80, 
	probe0  => cpllLock_reg,
	probe1  => cpllRefClkLost_reg,
	probe2  => cpllFbClkLost_reg,
	probe3  => rxresetdone_reg,
	probe4  => rxfsmresetdone_reg,
	probe5  => x"00", --rxoutclk_counter(0),
	probe6  => x"00", --rxoutclk_counter(1),
	probe7  => x"00", --rxoutclk_counter(2),
	probe8  => x"00", --rxoutclk_counter(3),
	probe9  => rxdata128,
	probe10 => rxcharisk16,
	probe11 => rxchariscomma16,
	probe12 => rxcodeerr8,
	probe13 => rxbufstatus24,
	probe14 => rxbyterealign8,
	probe15 => rxbyteisaligned_reg
	
	
);



vio: vio_0
  PORT MAP (
    clk => sysclk40,
    probe_in0       => cpllPowerDown,
    probe_in1       => cpllReset,
    probe_in2(0)    => soft_reset,
    probe_in3       => rxpolarity,
    probe_in4(0)    => errorReset,
    
    probe_out0     => cpllPowerUp_vio,
    probe_out1      => cpllReset,
    probe_out2(0)   => soft_reset_vio,
    probe_out3      => rxpolarity_vio,
    probe_out4(0)   => errorReset_vio
  );

end generate;



DEBUG_OFF_GEN: if debugQuad(quad)='0' generate begin

    cpllPowerUp_vio <= "0000";
    soft_reset_vio  <= '0';
    rxpolarity_vio  <= "0000";
    errorReset_vio  <= '0';

end generate;



end Behavioral;



