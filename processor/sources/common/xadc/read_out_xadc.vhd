----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12.02.2014 14:37:00
-- Design Name: 
-- Module Name: read_out_xadc - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library UNISIM;
use UNISIM.VComponents.all;
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;
use ieee.std_logic_textio.all;
use work.L1TopoDataTypes.all;
use work.L1TopoFunctions.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity read_out_xadc is
    Port (
    
        drpclkin     : in std_logic; -- 40 Mhz clock
        drpadress    :in STD_LOGIC_vector (6 downto 0); -- Register Adress of DRP
        
        drp_control  : in STD_LOGIC_VECTOR (2 downto 0); -- Control bits from high to low: RESET / WRITE Enable / Readout Enable
       
        drp_ready    :out STD_LOGIC;     -- High when Register data is ready for readout
       
        drp_input    : in STD_LOGIC_VECTOR (15 downto 0); -- DRP Data input for reconfiguration Registers  
        
        drp_output   : out STD_LOGIC_VECTOR (15 downto 0); -- DRP Data output
        
        drp_alarms   : out STD_LOGIC_VECTOR (5 downto 0) -- vector of all allarms from high to low: All Alarms Or�ed/ VBram/VCCint/VCCaux/userTemp/OT(Critical Temp)               
        
    
    
    );
end read_out_xadc;

architecture Behavioral of read_out_xadc is

component xadc_temp is

   port
   (
        daddr_in        : in  STD_LOGIC_VECTOR (6 downto 0);     -- Address bus for the dynamic reconfiguration port
        den_in          : in  STD_LOGIC;                         -- Enable Signal for the dynamic reconfiguration port
        di_in           : in  STD_LOGIC_VECTOR (15 downto 0);    -- Input data bus for the dynamic reconfiguration port
        dwe_in          : in  STD_LOGIC;                         -- Write Enable for the dynamic reconfiguration port
        do_out          : out  STD_LOGIC_VECTOR (15 downto 0);   -- Output data bus for dynamic reconfiguration port
        drdy_out        : out  STD_LOGIC;                        -- Data ready signal for the dynamic reconfiguration port
        dclk_in         : in  STD_LOGIC;                         -- Clock input for the dynamic reconfiguration port
        
        reset_in        : in  STD_LOGIC;                         -- Reset signal for the System Monitor control logic
        busy_out        : out  STD_LOGIC;                        -- ADC Busy signal
        channel_out     : out  STD_LOGIC_VECTOR (4 downto 0);    -- Channel Selection Outputs
        eoc_out         : out  STD_LOGIC;                        -- End of Conversion Signal
        eos_out         : out  STD_LOGIC;                        -- End of Sequence Signal
        
        ot_out          : out  STD_LOGIC;                        -- Over-Temperature alarm output
        vccaux_alarm_out : out  STD_LOGIC;                        -- VCCAUX-sensor alarm output
        vccint_alarm_out : out  STD_LOGIC;                        -- VCCINT-sensor alarm output
        user_temp_alarm_out : out  STD_LOGIC;                        -- Temperature-sensor alarm output
        vbram_alarm_out : out  STD_LOGIC;                        -- VCCINT-sensor alarm output
        alarm_out       : out STD_LOGIC;                         -- OR'ed output of all the Alarms
        
        vp_in           : in  STD_LOGIC;                         -- Dedicated Analog Input Pair
        vn_in           : in  STD_LOGIC
);
end component;





begin




temp_xadc: xadc_temp
port map(

    daddr_in        => drpadress,                       -- Address bus for the dynamic reconfiguration port
    
    den_in          => drp_control(0),                     -- Enable Signal for the dynamic reconfiguration port, High for activation
    dwe_in          => drp_control(1),                                   -- Write data
    reset_in        => drp_control(2),                         -- Reset signal for the System Monitor control logic
    
    di_in           => drp_input,                         -- Input data set to 0, write disabled
    
    do_out          => drp_output,                          -- Output data bus for dynamic reconfiguration port
    drdy_out        => drp_ready,                      -- Data ready signal for the dynamic reconfiguration port
    
    
    dclk_in         => drpclkin,                       -- 100 Mhz Clk input
    
    
    busy_out        => open,                       -- ADC Busy signal /not needed !?!
    channel_out     => open,                           -- Channel Selection Outputs / not used
    eoc_out         => open,                        -- End of Conversion Signal / not neede for drp
    eos_out         => open,                       -- End of Sequence Signal / not needed for drp
    
    ot_out          =>  drp_alarms(0),                       -- Over-Temperature alarm output / system critical temp!
    user_temp_alarm_out => drp_alarms(1),                       -- Temperature-sensor alarm output / not used!
    vccaux_alarm_out => drp_alarms(2),                        -- VCCAUX-sensor alarm output
    vccint_alarm_out => drp_alarms(3),                        -- VCCINT-sensor alarm output
    vbram_alarm_out => drp_alarms(4),                        -- VCCINT-sensor alarm output
    alarm_out       => drp_alarms(5),                      -- OR'ed output of all the Alarms / not used
    
            
    vp_in           => '0' ,                        -- Dedicated Analog Input Pair / set to ground
    vn_in           => '0'
);

end Behavioral;
