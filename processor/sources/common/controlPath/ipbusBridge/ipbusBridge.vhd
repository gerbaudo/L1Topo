----------------------------------------------------------------------------------
-- Company: Johannes Gutenberg-Universitaet Mainz
-- Engineer: Christian Kahra
-- 
-- Create Date:    20:42:46 08/28/2014 
-- Design Name: 
-- Module Name:    ipbusBridge - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 	based on the old ctrlbus module
--
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

use work.ipbus.all;



entity ipbusBridge is 
    generic(
        debug: natural := 1
    );
	port(
		sysclk:            in  std_logic;
		parallelclk:	    in  std_logic;
		serialclk:         in  std_logic;
		
		pll_locked: in  std_logic;
		
		
		ipb_read: in ipb_rbus;
		ipbBridgeBusOut: out std_logic_vector(2 downto 0);
		
		ipbBridgeBusIn:  in  std_logic_vector(4 downto 0);
		ipb_write: out  ipb_wbus;
		
		idelayStatus: out std_logic_vector(24 downto 0);
		alignmentStatus_out: out std_logic_vector(19 downto 0);
		errorCounter_out: out std_logic_vector(29 downto 0);
		errorCounterReset: in std_logic
		
		--ipbr_ack:   in std_logic_vector(55 downto 0)
		
		
	);
end ipbusBridge;


architecture Behavioral of ipbusBridge is

	constant inWidth: natural := 5;
	constant outWidth: natural := 3;


    constant K28_0: std_logic_vector(7 downto 0) := X"1C";
    constant K28_1: std_logic_vector(7 downto 0) := X"3C";
    constant K28_2: std_logic_vector(7 downto 0) := X"5C";
    constant K28_3: std_logic_vector(7 downto 0) := X"7C";
    constant K28_4: std_logic_vector(7 downto 0) := X"9C";
    constant K28_5: std_logic_vector(7 downto 0) := X"BC";
    constant K28_6: std_logic_vector(7 downto 0) := X"DC";
    constant K28_7: std_logic_vector(7 downto 0) := X"FC";
    constant K23_7: std_logic_vector(7 downto 0) := X"F7";
    constant K27_7: std_logic_vector(7 downto 0) := X"FB";
    constant K29_7: std_logic_vector(7 downto 0) := X"FD";
    constant K30_7: std_logic_vector(7 downto 0) := X"FE";
    
    

    attribute dont_touch: string;


	type array4 is array(natural range <>) of std_logic_vector(3 downto 0);


    signal ipb_write_i: ipb_wbus;
    signal ack, err: std_logic;

	signal dataOut: std_logic_vector(outWidth*2*8-1	downto 0);
	signal dataToSerialize: std_logic_vector(outWidth*8-1 downto 0);
	signal charIsKOut: std_logic_vector(outWidth-1 downto 0);
	
	type   dataOutStateType is (dataOutState_IDLE0, dataOutState_IDLE1, dataOutState_BOF, dataOutState_DATA0, dataOutState_DATA1);
	signal dataOutState: dataOutStateType;
	signal dataOutState_slv: std_logic_vector(2 downto 0);
	
	
	signal deserializedData: std_logic_vector(inWidth*8-1 downto 0);
	signal dataIn: std_logic_vector(inWidth*2*8-1 downto 0);
	signal charIsKIn: std_logic_vector(inWidth-1 downto 0);
	
	signal synchronisedData: std_logic_vector(inWidth*10-1 downto 0);
	
	signal codeErr: std_logic_vector(inWidth-1 downto 0);
	signal codeErr_reg: std_logic_vector(inWidth-1 downto 0);
	signal errorCounter: std_logic_vector(29 downto 0);
	
	signal dataIn_codeErr: std_logic_vector(23 downto 0);
	
	signal bitslipErrorCounter: array4(inWidth-1 downto 0);
	signal bitslip: std_logic_vector(inWidth-1 downto 0);
	
	signal charIsKInAnded: std_logic_vector(inWidth downto 0);
	signal charIsKInOred:  std_logic_vector(inWidth downto 0);
	signal codeErrOred:    std_logic_vector(inWidth downto 0);
	signal charIsBofOred:  std_logic_vector(inWidth downto 0);
	signal charIsBofAnded: std_logic_vector(inWidth downto 0);
	signal charIsIdle0Anded: std_logic_vector(inWidth downto 0);
	
	signal dataIsValid:  std_logic;
	
	type   dataInStateType is (dataInState_IDLE0, dataInState_IDLE1, dataInState_DATA0, dataInState_DATA1);
	signal dataInState: dataInStateType;
	signal dataInState_slv: std_logic_vector(1 downto 0);
	
	type array5 is array(natural range <>) of std_logic_vector(4 downto 0);
	signal idelayValue, idelayValue_reg: array5(4 downto 0);
	signal idelayLoad: std_logic_vector(4 downto 0);
	signal idelayCurrentValue: array5(4 downto 0);
	signal alignmentStatus: std_logic_vector(19 downto 0);
	
	signal ipbusBridgeVersion: std_logic;
	
	signal debugIPBusBridge: std_logic_vector(334 downto 0);
--	attribute dont_touch of debugIPBusBridge: signal is "true";
	
COMPONENT ila_ipbusBridge1 PORT (	
	clk : IN STD_LOGIC;
	probe0 : IN STD_LOGIC_VECTOR(9 DOWNTO 0); 
	probe1 : IN STD_LOGIC_VECTOR(9 DOWNTO 0); 
	probe2 : IN STD_LOGIC_VECTOR(9 DOWNTO 0); 
	probe3 : IN STD_LOGIC_VECTOR(9 DOWNTO 0); 
	probe4 : IN STD_LOGIC_VECTOR(9 DOWNTO 0); 
	probe5 : IN STD_LOGIC_VECTOR(7 DOWNTO 0); 
	probe6 : IN STD_LOGIC_VECTOR(7 DOWNTO 0); 
	probe7 : IN STD_LOGIC_VECTOR(7 DOWNTO 0); 
	probe8 : IN STD_LOGIC_VECTOR(7 DOWNTO 0); 
	probe9 : IN STD_LOGIC_VECTOR(7 DOWNTO 0); 
	probe10 : IN STD_LOGIC_VECTOR(4 DOWNTO 0); 
	probe11 : IN STD_LOGIC_VECTOR(4 DOWNTO 0); 
	probe12 : IN STD_LOGIC_VECTOR(3 DOWNTO 0); 
	probe13 : IN STD_LOGIC_VECTOR(3 DOWNTO 0); 
	probe14 : IN STD_LOGIC_VECTOR(3 DOWNTO 0); 
	probe15 : IN STD_LOGIC_VECTOR(3 DOWNTO 0); 
	probe16 : IN STD_LOGIC_VECTOR(3 DOWNTO 0); 
	probe17 : IN STD_LOGIC_VECTOR(29 DOWNTO 0); 
	probe18 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
	probe19 : IN STD_LOGIC_VECTOR(1 DOWNTO 0); 
    probe20 : IN STD_LOGIC_VECTOR(2 DOWNTO 0); 
    probe21 : IN STD_LOGIC_VECTOR(31 DOWNTO 0); 
    probe22 : IN STD_LOGIC_VECTOR(31 DOWNTO 0); 
    probe23 : IN STD_LOGIC_VECTOR(1 DOWNTO 0); 
    probe24 : IN STD_LOGIC_VECTOR(31 DOWNTO 0); 
    probe25 : IN STD_LOGIC_VECTOR(1 DOWNTO 0); 
    probe26 : IN STD_LOGIC_VECTOR(7 DOWNTO 0); 
    probe27 : IN STD_LOGIC_VECTOR(7 DOWNTO 0); 
    probe28 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    probe29 : IN STD_LOGIC_VECTOR(2 DOWNTO 0)
);
END COMPONENT  ;
	
begin





--	dataOut( 0) <= '0';
--	dataOut(32 downto  1) <= ipb_write.ipb_addr;
--	dataOut(39 downto 33) <= ipb_write.ipb_wdata( 6 downto 0);
--	dataOut(40) <= '1';
--	dataOut(65 downto 41) <= ipb_write.ipb_wdata(31 downto 7);
--	dataOut(66) <= ipb_write.ipb_strobe;
--	dataOut(67) <= ipb_write.ipb_write;
--	dataOut(79 downto 68) <= (others => '0');
	
process(sysclk) begin
if rising_edge(sysclk) then
    ack <= ipb_read.ipb_ack;
    err <= ipb_read.ipb_err;
end if;
end process;
	
	dataOut( 0) <= '0';
	dataOut(23 downto  1) <= ipb_read.ipb_rdata(22 downto  0);
	dataOut(24) <= '1';
	dataOut(33 downto 25) <= ipb_read.ipb_rdata(31 downto 23);
	dataOut(34) <= ack; --ipb_read.ipb_ack;
	dataOut(35) <= err; --ipb_read.ipb_err;
	dataOut(47 downto 36) <= (others => '0');
	
	
	
	process(parallelclk) begin
		if rising_edge(parallelclk) then
			
			if pll_locked='0' then dataOutState <= dataOutState_IDLE0;
			else 
				case dataOutState is
				
				when dataOutState_IDLE0 =>
				    dataOutState <= dataOutState_IDLE1;
                    
				
				when dataOutState_IDLE1 =>
					if dataIn(66)='1' and dataIsValid='1' then 
					     dataOutState <= dataOutState_BOF;
					else dataOutState <= dataOutState_IDLE0;
					end if;
				
				when dataOutState_BOF =>
					dataOutState <= dataOutState_DATA0;
				
				when dataOutState_DATA0 =>
					if dataIn(66)='1' and dataIsValid='1' then 
					     dataOutState <= dataOutState_DATA1;
					else dataOutState <= dataOutState_IDLE0;
					end if;
				
				when dataOutState_DATA1 =>
					dataOutState <= dataOutState_DATA0;
					
				end case;
			end if;
			
		end if;
	end process;


    dataOutState_slv <= "000" when dataOutState=dataOutState_IDLE0
                   else "001" when dataOutState=dataOutState_IDLE1
                   else "010" when dataOutState=dataOutState_BOF
                   else "100" when dataOutState=dataOutState_DATA0
                   else "101";


	dataToSerialize <= K28_5 & K28_5 & K28_5 when dataOutState=dataOutState_IDLE0 --or dataIn(66)='0' 
	                  else dataIn_codeErr when dataOutState=dataOutState_IDLE1 or codeErrOred(0)='1'
					  else K27_7 & K27_7 & K27_7 when  dataOutState=dataOutState_BOF
					  else dataOut(outWidth*8-1 downto  0) when  dataOutState=dataOutState_DATA0 
					  else dataOut(outWidth*2*8-1 downto outWidth*8);






SERIALIZER_GEN: for i in outWidth-1 downto 0 generate begin


	charIsKOut(i) <= '1' when dataOutState=dataOutState_IDLE0 or dataOutState=dataOutState_IDLE1 or codeErrOred(0)='1' or dataOutState=dataOutState_BOF else '0'; --or dataIn(66)='0'


	serializer: entity work.ipbusBridge_serializer
		port map(
			parallelclk  => parallelclk,
			serialclk    => serialclk,
			pll_locked  => pll_locked,
			parallelData => dataToSerialize(i*8+7 downto i*8),
			charIsK      => charIsKOut(i),
			serialData   => ipbBridgeBusOut(i)
		);
		
end generate;






	charIsKInAnded(inWidth)   <= '1';
	charIsKInOred(inWidth)    <= '0';
	codeErrOred(inWidth)      <= '0';
	charIsBofAnded(inWidth)   <= '1';
	charIsBofOred(inWidth)    <= '0';
	charIsIdle0Anded(inWidth)  <= '1';




DESERIALIZER_GEN: for i in inWidth-1 downto 0 generate begin



	deserializer: entity work.ipbusBridge_deserializer
		port map(
			sysclk         => sysclk,
			parallelclk    => parallelclk,
			serialclk      => serialclk,
			pll_locked    => pll_locked,
			serialData     => ipbBridgeBusIn(i),
--			idelayValueIn  => idelayValue(i), --control(i*5+4 downto i*5),
--			idelayLoad     => idelayLoad(i),
--			idelayValueOut => idelayCurrentValue(i), --status(i*5+4 downto i*5),
			bitslip        => bitslip(i),
			alignmentStatus => alignmentStatus(i*4+3 downto i*4),
			decodedData    => deserializedData(i*8+7 downto i*8),
			charIsK        => charIsKIn(i),
			codeErr        => codeErr(i),
			synchronisedData_out => synchronisedData(i*10+9 downto i*10)
		);

--	idelayStatus(i*5 + 4 downto i*5) <= idelayCurrentValue(i);
    alignmentStatus_out(i*4+3 downto i*4) <= alignmentStatus(i*4+3 downto i*4);


	charIsKInAnded(i)  <= charIsKInAnded(i+1) and charIsKIn(i);
	charIsKInOred(i)   <= charIsKInOred(i+1)  or  charIsKIn(i);
	codeErrOred(i)     <= codeErrOred(i+1)    or  codeErr(i);
	charIsBofAnded(i)  <= '1' when charIsBofAnded(i+1)='1' and  deserializedData(i*8+7 downto i*8)=K27_7 else '0';
	charIsBofOred(i)   <= '1' when charIsBofOred(i+1) ='1' or   deserializedData(i*8+7 downto i*8)=K27_7 else '0';
	charIsIdle0Anded(i) <= '1' when charIsIdle0Anded(i+1)='1' and deserializedData(i*8+7 downto i*8)=K28_5 else '0';
		
		
		
		
	process(parallelclk) begin
		if rising_edge(parallelclk) then
			
			codeErr_reg(i) <= codeErr(i);
			
			if codeErr(i)='1' then 
				bitslipErrorCounter(i) <= std_logic_vector( unsigned(bitslipErrorCounter(i)) + 1);
            else 
                bitslipErrorCounter(i) <= "0000";
            end if;
				
            if errorCounterReset='1' then
                errorCounter(i*6+5 downto i*6) <= "000000";
            elsif codeErr(i)='1' then
				if   errorCounter(i*6+5 downto i*6) /= "111111" then 
					  errorCounter(i*6+5 downto i*6) <= std_logic_vector( unsigned(errorCounter(i*6+5 downto i*6)) + 1);
				else errorCounter(i*6+5 downto i*6) <= errorCounter(i*6+5 downto i*6);
				end if;
			else
				errorCounter(i*6+5 downto i*6) <= errorCounter(i*6+5 downto i*6);
			end if;
			
		end if;
	end process;
	
	
	dataIn_codeErr(7 downto 0) <= K28_0 when codeErr(1 downto 0)="00" and codeErr_reg(1 downto 0)="00"
	                else K28_1 when (codeErr(0) = '1' or codeErr_reg(0) = '1') and codeErr(1) = '0' and codeErr_reg(1) = '0'
	                else K28_2 when (codeErr(1) = '1' or codeErr_reg(1) = '1') and codeErr(0) = '0' and codeErr_reg(0) = '0'
	                else K28_3;
	
	dataIn_codeErr(15 downto 8) <= K28_0 when codeErr(3 downto 2)="00" and codeErr_reg(3 downto 2)="00"
                    else K28_1 when (codeErr(2) = '1' or codeErr_reg(2) = '1') and codeErr(3) = '0' and codeErr_reg(3) = '0'
                    else K28_2 when (codeErr(3) = '1' or codeErr_reg(3) = '1') and codeErr(2) = '0' and codeErr_reg(2) = '0'
                    else K28_3;
	
	dataIn_codeErr(23 downto 16) <= K28_0 when codeErr(4)='0' and codeErr_reg(4)='0'
                    else K28_1;

	
	
	bitslip(i) <= '1' when bitslipErrorCounter(i)="1111" else '0';
	errorCounter_out <= errorCounter;

end generate;



	dataIsValid <= '1' when ( codeErrOred(0)='0' and charIsKInOred(0)='0' )  else '0';


	


process(parallelclk) begin
		if rising_edge(parallelclk) then
			
			if pll_locked='0' then dataInState <= dataInState_IDLE0;
			else 
				case dataInState is
				
				when dataInState_IDLE0 =>
					if charIsBofAnded(0) = '1' then dataInState <= dataInState_Data0;
					elsif charIsIdle0Anded(0) = '1' then dataInState <= dataInState_IDLE0;
					else dataInState <= dataInState_IDLE1;
					end if;
                when dataInState_IDLE1 =>
                    if charIsBofAnded(0) = '1' then dataInState <= dataInState_Data0;
                    else dataInState <= dataInState_IDLE0;
                    end if;
				when dataInState_Data0 =>
					if dataIsValid='1' then
						dataInState <= dataInState_Data1;
						dataIn(inWidth*8-1 downto 0) <= deserializedData;
					else 
						dataInState <= dataInState_IDLE0;
						dataIn <= (inWidth*2*8-1 downto 0 => '0');
					end if;
				when dataInState_Data1 =>
					dataIn(inWidth*2*8-1 downto inWidth*8) <= deserializedData;
					if dataIsValid='1' then dataInState <= dataInState_Data0;
					else dataInState <= dataInState_IDLE0;
					end if;
				end case;
			end if;
			
		end if;
end process;

    --ipbusBridgeVersion <= '0' when dataInState = dataInState_IDLE0 and charIsIdle0Anded(0) = '1' else '1';
    

    dataInState_slv <= "00" when dataInState=dataInState_IDLE0
                  else "01" when dataInState=dataInState_IDLE1
                  else "11" when dataInState=dataInState_DATA0
                  else "11";

	
	ipb_write_i.ipb_addr <= dataIn(32 downto 1);
	ipb_write_i.ipb_wdata( 6 downto 0) <= dataIn(39 downto 33);
	ipb_write_i.ipb_wdata(31 downto 7) <= dataIn(65 downto 41);
	ipb_write_i.ipb_strobe <= '1' when dataIn(66)='1' and dataIsValid='1' else '0';
	ipb_write_i.ipb_write  <= '1' when dataIn(67)='1' and dataIsValid='1' else '0';

    ipb_write <= ipb_write_i;


--vio_ipbBridgeIdelay: entity work.vio_idelay
--    port map(
--        clk => sysclk,
--        probe_in0  => idelayCurrentValue(0),
--        probe_in1  => idelayCurrentValue(1),
--        probe_in2  => idelayCurrentValue(2),
--        probe_in3  => idelayCurrentValue(3),
--        probe_in4  => idelayCurrentValue(4),
--        probe_in5  => alignmentStatus( 3 downto  0),
--        probe_in6  => alignmentStatus( 7 downto  4),
--        probe_in7  => alignmentStatus(11 downto  8),
--        probe_in8  => alignmentStatus(15 downto 12),
--        probe_in9  => alignmentStatus(19 downto 16),
--        probe_out0 => idelayValue(0),
--        probe_out1 => idelayValue(1),
--        probe_out2 => idelayValue(2),
--        probe_out3 => idelayValue(3),
--        probe_out4 => idelayValue(4)
--    );


--    process(sysclk) begin
--        if rising_edge(sysclk) then
--            for i in 4 downto 0 loop
--                idelayValue_reg(i) <= idelayValue(i);
--                if idelayValue_reg(i) /= idelayValue(i) then idelayLoad(i) <= '1';
--                else idelayLoad(i) <= '0';
--                end if;
--            end loop;
--        end if;
--    end process;


--DEBUG1_GEN: if debug=1 generate begin

--ILA_IPBBRIDGE1: ila_ipbusBridge
--    port map(
--        clk => parallelclk,
--        probe0  => synchronisedData( 9 downto  0),
--        probe1  => synchronisedData(19 downto 10),
--        probe2  => synchronisedData(29 downto 20),
--        probe3  => synchronisedData(39 downto 30),
--        probe4  => synchronisedData(49 downto 40),
--        probe5  => deserializedData( 7 downto  0),
--        probe6  => deserializedData(15 downto  8),
--        probe7  => deserializedData(23 downto 16),
--        probe8  => deserializedData(31 downto 24),
--        probe9  => deserializedData(39 downto 32),
--        probe10 => charIsKIn,
--        probe11 => codeErr,
--        probe12 => alignmentStatus( 3 downto  0),   
--        probe13 => alignmentStatus( 7 downto  4),
--        probe14 => alignmentStatus(11 downto  8),
--        probe15 => alignmentStatus(15 downto 12),
--        probe16 => alignmentStatus(19 downto 16),
--        probe17 => errorCounter,
--        probe18(0) => errorCounterReset,
--        probe19 => dataInState_slv,
--        probe20 => dataOutState_slv,
--        probe21 => ipb_write_i.ipb_addr,
--        probe22 => ipb_write_i.ipb_wdata,
--        probe23 => ipb_write_i.ipb_write & ipb_write_i.ipb_strobe,
--        probe24 => ipb_read.ipb_rdata,  
--        probe25 => ipb_read.ipb_err & ipb_read.ipb_ack,
--        probe26 => dataToSerialize( 7 downto  0),
--        probe27 => dataToSerialize(15 downto  8),
--        probe28 => dataToSerialize(23 downto 16),
--        probe29 => charIsKOut
--    );

--end generate;






--ToDo: ErrorCounter fuer Protokoll-Fehler: z.B. charIsKOred(0) /= charIsKAnded(0)


end Behavioral;

