----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 24.07.2014 16:15:08
-- Design Name: 
-- Module Name: ipbus_slave_playback128 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

use work.l1topo_package.all;
use work.ipbus.all;
use work.L1TopoGTConfiguration.all;

entity ipbus_slave_playbackspy128 is
    generic(
        debug: std_logic := '0'
    );
    Port ( 
        sysclk40: in std_logic;
        sysclk160: in std_logic;
        ipbus_in: in ipb_wbus;
        ipbus_out: out ipb_rbus;
        
        spyData128Bit: in arraySLV128(MGTHigh downto MGTLow);
        playbackData128Bit: out arraySLV128(79 downto 0);
        enableSpy: in std_logic;
        enableAddressIncrement: in std_logic;
        enableAddressCounterSaturation: in std_logic;
        rewindPlaybackSpy: in std_logic;
        fineDelay: in std_logic_vector(1 downto 0)
        
--        debugPlaybackSpy128: out std_logic_vector(432 downto 0)
        
    );
end ipbus_slave_playbackspy128;



architecture Behavioral of ipbus_slave_playbackspy128 is

    attribute shreg_extract: string;
    
    

    signal sel: natural;

    signal state: std_logic;
    signal ipbusPlaybackDataWriteEnable: std_logic_vector(79 downto 0);
    signal ack: std_logic;
    signal addr0_reg, strobe_reg: std_logic;

    

    type   arrayNxSLV128 is array(natural range <>) of arraySLV128(79 downto 0);
    
    signal spyData128Bit_reg: arrayNxSLV128(0 downto 0);
    attribute shreg_extract of spyData128Bit_reg: signal is "no";
    
    signal playbackData128Bit_reg: arrayNxSLV128(0 downto 0);
    attribute shreg_extract of playbackData128Bit_reg: signal is "no";
    
    
    signal ipbusSpyData: arraySLV32(79 downto 0);
    signal serialSpyData32Bit: arraySLV32(79 downto 0);
    signal serialPlaybackData32Bit: arraySLV32(79 downto 0);
    signal playbackDataShiftRegister: arraySLV96(79 downto 0);
    

--    signal toggle40, toggle160_reg: std_logic_vector(79 downto 0);
    
    type arraySLV11 is array(natural range <>) of std_logic_vector(10 downto 0);
    signal addressCounter: arraySLV11(79 downto 0);
     
    signal resetAddressCounter: std_logic_vector(79 downto 0);
    
    signal enableSpy_reg: std_logic_vector(79 downto 0);
    signal enableAddressIncrement_reg: std_logic_vector(79 downto 0);
    signal enableAddressCounterSaturation_reg: std_logic_vector(79 downto 0);
    signal rewindPlaybackSpy_reg: std_logic_vector(79 downto 0);
    signal fineDelay_reg: arraySLV2(79 downto 0);
    
    
    attribute keep: string;
    attribute keep of spyData128Bit_reg: signal is "true";
    attribute keep of playbackData128Bit_reg: signal is "true";
    attribute keep of enableSpy_reg: signal is "true";
    attribute keep of enableAddressIncrement_reg: signal is "true";
    attribute keep of enableAddressCounterSaturation_reg: signal is "true";
    attribute keep of rewindPlaybackSpy_reg: signal is "true";
    attribute keep of fineDelay_reg: signal is "true";
    
    
    attribute dont_touch: string;
    attribute dont_touch of spyData128Bit_reg: signal is "true";
    attribute dont_touch of playbackData128Bit_reg: signal is "true";
    
    
    
    component ila_playbackSpy128
    	Port(
    		clk     : in STD_LOGIC;
    		probe0  : in STD_LOGIC_VECTOR(31 downto 0);
    		probe1  : in STD_LOGIC_VECTOR(31 downto 0);
    		probe2  : in STD_LOGIC_VECTOR(0 to 0);
    		probe3  : in STD_LOGIC_VECTOR(0 to 0);
    		probe4  : in STD_LOGIC_VECTOR(31 downto 0);
    		probe5  : in STD_LOGIC_VECTOR(0 to 0);
    		probe6  : in STD_LOGIC_VECTOR(127 downto 0);
    		probe7  : in STD_LOGIC_VECTOR(127 downto 0);
    		probe8  : in STD_LOGIC_VECTOR(0 to 0);
    		probe9  : in STD_LOGIC_VECTOR(0 to 0);
    		probe10 : in STD_LOGIC_VECTOR(0 to 0);
    		probe11 : in STD_LOGIC_VECTOR(0 to 0);
    		probe12 : in STD_LOGIC_VECTOR(31 downto 0);
    		probe13 : in STD_LOGIC_VECTOR(31 downto 0);
    		probe14 : in STD_LOGIC_VECTOR(9 downto 0)
    	);
    end component;

begin


    sel <= to_integer(unsigned(ipbus_in.ipb_addr(16 downto 10)));


PLAYBACKSPY128_GEN: for ch in 79 downto 0 generate begin

--    playbackData128Bit(ch) <= playbackData128Bit_reg(0)(ch);

    process(sysclk40) begin
        if rising_edge(sysclk40) then
--            toggle40(ch) <= not toggle40(ch);
            
--            spyData128Bit_reg(1)(ch) <= spyData128Bit_reg(0)(ch);
--            spyData128Bit_reg(2)(ch) <= spyData128Bit_reg(1)(ch);
            playbackData128Bit(ch) <= playbackData128Bit_reg(0)(ch);
--            playbackData128Bit_reg(2)(ch) <= playbackData128Bit_reg(1)(ch);
--            playbackData128Bit_reg(1)(ch) <= playbackData128Bit_reg(0)(ch); 

            enableSpy_reg(ch) <= enableSpy;
            enableAddressIncrement_reg(ch) <= enableAddressIncrement;
            enableAddressCounterSaturation_reg(ch) <= enableAddressCounterSaturation;
            rewindPlaybackSpy_reg(ch) <= rewindPlaybackSpy;
            fineDelay_reg(ch) <= fineDelay;
        end if;
    end process;
        

    SRL16E_inst : SRL16E
   generic map (
      INIT => X"0000")
   port map (
      Q => resetAddressCounter(ch),       -- SRL data output
      A0 => fineDelay_reg(ch)(0),--'0',     -- Select[0] input
      A1 => fineDelay_reg(ch)(1),--'1',     -- Select[1] input
      A2 => '0',     -- Select[2] input
      A3 => '0',     -- Select[3] input
      CE => '1',     -- Clock enable input
      CLK => sysclk160,   -- Clock input
      D => rewindPlaybackSpy_reg(ch)        -- SRL data input
   );


    process(sysclk160) begin
        if rising_edge(sysclk160) then
--            toggle160_reg(ch) <= toggle40(ch);
            
            if resetAddressCounter(ch)='1' then 
                addressCounter(ch) <= (10 downto 0 => '0');
                playbackDataShiftRegister(ch) <= (95 downto 0 => '0');
            
            else
                if enableAddressIncrement_reg(ch)='0' or (enableAddressCounterSaturation_reg(ch)='1' and addressCounter(ch)(10) = '1') then 
                    addressCounter(ch) <= addressCounter(ch);
--                elsif toggle40(ch) /= toggle160_reg(ch) then 
--                    addressCounter(ch)(1 downto 0) <= "10";
                else 
                    addressCounter(ch) <= std_logic_vector( unsigned(addressCounter(ch)) + 1);
                end if;
            
                playbackDataShiftRegister(ch)(95 downto 64) <= playbackDataShiftRegister(ch)(63 downto 32);
                playbackDataShiftRegister(ch)(63 downto 32) <= playbackDataShiftRegister(ch)(31 downto  0);
                playbackDataShiftRegister(ch)(31 downto  0) <= serialPlaybackData32Bit(ch)(31 downto 0);
            
            end if;  
            
        end if;
    end process;


ipbusPlaybackDataWriteEnable(ch) <= '1' when sel=ch and state='1' and ipbus_in.ipb_write='1' else '0';
    
    
PlaybackOnlyChannels: if (ch<MGTLow or ch>MGTHigh) generate

        PlaybackOnly: entity work.playbackspy_blockram
            generic map(dataWidth => 32)
            Port map(
                CLKA => sysclk160,
                DIA => (31 downto 0 => '0'),
                ADDRA => addressCounter(ch)(9 downto 0),
                WEA => '0',
                DOA => serialPlaybackData32Bit(ch),
                
                CLKB => sysclk40,
                DIB => ipbus_in.ipb_wdata,
                ADDRB => ipbus_in.ipb_addr(9 downto 0),
                WEB => ipbusPlaybackDataWriteEnable(ch),
                DOB => ipbusSpyData(ch)
            );

    end generate;



NormalPlaybackSpyChannels: if (ch>=MGTLow and ch<=MGTHigh) generate 
    
        process(sysclk40) begin
            if rising_edge(sysclk40) then
                spyData128Bit_reg(0)(ch) <= spyData128Bit(ch);
            end if;
        end process;
    
        serialSpyData32Bit(ch) <= spyData128Bit_reg(0)(ch)( 31 downto  0) when addressCounter(ch)(1 downto 0)="11"
                             else spyData128Bit_reg(0)(ch)( 63 downto 32) when addressCounter(ch)(1 downto 0)="10"
                             else spyData128Bit_reg(0)(ch)( 95 downto 64) when addressCounter(ch)(1 downto 0)="01"
                             else spyData128Bit_reg(0)(ch)(127 downto 96);
        
        
        PlaybackSpy: entity work.playbackspy_blockram
            generic map(dataWidth => 32)
            Port map(
                CLKA => sysclk160,
                DIA => serialSpyData32Bit(ch),
                ADDRA => addressCounter(ch)(9 downto 0),
                WEA => enableSpy_reg(ch),
                DOA => serialPlaybackData32Bit(ch),
                
                CLKB => sysclk40,
                DIB => ipbus_in.ipb_wdata,
                ADDRB => ipbus_in.ipb_addr(9 downto 0),
                WEB => ipbusPlaybackDataWriteEnable(ch),
                DOB => ipbusSpyData(ch)
            );
    
    
    end generate;
    
    playbackData128Bit_reg(0)(ch)(127 downto 96) <= playbackDataShiftRegister(ch)(95 downto 64);
    playbackData128Bit_reg(0)(ch)( 95 downto 64) <= playbackDataShiftRegister(ch)(63 downto 32);
    playbackData128Bit_reg(0)(ch)( 63 downto 32) <= playbackDataShiftRegister(ch)(31 downto  0);
    playbackData128Bit_reg(0)(ch)( 31 downto  0) <= serialPlaybackData32Bit(ch)(31 downto 0);

end generate;    


    ipbus_out.ipb_rdata <= ipbusSpyData(sel);


    process(sysclk40) begin
        if rising_edge(sysclk40) then
            strobe_reg <= ipbus_in.ipb_strobe;
            addr0_reg <= ipbus_in.ipb_addr(0);  
            if ipbus_in.ipb_strobe='1' then
                
                if ipbus_in.ipb_addr(0) /= addr0_reg or strobe_reg='0' then 
                    state <= '1';
                else 
                    state <= '0';
                end if;
                
                ack <= state;
                
            else    
                state <= '0';
                ack <= '0';
            end if;            
        end if;        
    end process;


    ipbus_out.ipb_ack <= ack;
    ipbus_out.ipb_err <= '0';


--    debugPlaybackSpy128(31 downto 0) <= ipbus_in.ipb_addr;
--    debugPlaybackSpy128(63 downto 32) <= ipbus_in.ipb_wdata;
--    debugPlaybackSpy128(64) <= ipbus_in.ipb_strobe;
--    debugPlaybackSpy128(65) <= ipbus_in.ipb_write;
--    debugPlaybackSpy128(97 downto 66) <= ipbusSpyData(MGTLow);
--    debugPlaybackSpy128(98) <= ack;

--    debugPlaybackSpy128(226 downto   99) <= spyData128Bit(MGTLow);
--    debugPlaybackSpy128(354 downto 227) <= playbackData128Bit_reg(MGTLow); 
    
--    debugPlaybackSpy128(355) <= enableSpy;
--    debugPlaybackSpy128(356) <= rewindPlaybackSpy;
--    debugPlaybackSpy128(357) <= state;
--    debugPlaybackSpy128(358) <= ipbusPlaybackDataWriteEnable(MGTLow);
--    debugPlaybackSpy128(390 downto 359) <= serialSpyData32Bit(MGTLow);
--    debugPlaybackSpy128(422 downto 391) <= serialPlaybackData32Bit(MGTLow);
--    debugPlaybackSpy128(432 downto 423) <= addressCounter(MGTLow);    
    
    
DEBUG_ON_GEN: if debug='1' generate begin    
    
    ILA_PBS128: ila_playbackSpy128
        port map(
            clk => sysclk160,
            probe0 => ipbus_in.ipb_addr,
            probe1 => ipbus_in.ipb_wdata,
            probe2(0) => ipbus_in.ipb_strobe,
            probe3(0) => ipbus_in.ipb_write,
            probe4 => ipbusSpyData(12),
            probe5(0) => ack,
            probe6 => spyData128Bit_reg(0)(12),
            probe7 => playbackData128Bit_reg(0)(12),
            probe8(0) => enableSpy,
            probe9(0) => rewindPlaybackSpy,
            probe10(0) => state,
            probe11(0) => ipbusPlaybackDataWriteEnable(12),
            probe12 => serialSpyData32Bit(12),
            probe13 => serialPlaybackData32Bit(12),
            probe14 => addressCounter(12)(9 downto 0)
        );
    
end generate;    
    
end Behavioral;