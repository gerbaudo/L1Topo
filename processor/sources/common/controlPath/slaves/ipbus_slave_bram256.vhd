----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/05/2015 07:45:54 PM
-- Design Name: 
-- Module Name: ipbus_slave_bram256 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

--library UNISIM;
--use UNISIM.VComponents.all;


use work.ipbus.all;


entity ipbus_slave_bram256 is port(
    sysclk40:  in  std_logic;
    ipbus_in:  in  ipb_wbus;
    ipbus_out: out ipb_rbus;
    spyData:   in  std_logic_vector(255 downto 0);
    rewindSpyMemory: in std_logic
);

end ipbus_slave_bram256;



architecture Behavioral of ipbus_slave_bram256 is

    signal addressCounter: std_logic_vector(7 downto 0);
    signal spyWriteEnable: std_logic;
    signal strobe_reg, addr0_reg, ack : std_logic;

    component ipbus_slave_bram256_wiz
    	Port(
    		clka  : in  STD_LOGIC;
    		wea   : in  STD_LOGIC_VECTOR(0 to 0);
    		addra : in  STD_LOGIC_VECTOR(6 downto 0);
    		dina  : in  STD_LOGIC_VECTOR(255 downto 0);
    		douta : out STD_LOGIC_VECTOR(255 downto 0);
    		clkb  : in  STD_LOGIC;
    		web   : in  STD_LOGIC_VECTOR(0 to 0);
    		addrb : in  STD_LOGIC_VECTOR(9 downto 0);
    		dinb  : in  STD_LOGIC_VECTOR(31 downto 0);
    		doutb : out STD_LOGIC_VECTOR(31 downto 0)
    	);
    end component;
    

begin

    process(sysclk40) begin
        if rising_edge(sysclk40) then
            if rewindSpyMemory='1' then 
                addressCounter <= "00000000";
                spyWriteEnable <= '1';
            elsif addressCounter(7)='0' then 
                addressCounter <= std_logic_vector(unsigned(addressCounter)+1);
            else    
                spyWriteEnable <= '0';
            end if;
        end if;
    end process;

    


bram256: ipbus_slave_bram256_wiz
  PORT MAP (
    clka => sysclk40,
    wea(0) => spyWriteEnable,
    addra => addressCounter(6 downto 0),
    dina => spyData,
    douta => open,
    clkb => sysclk40,
    web(0) => '0',
    addrb => ipbus_in.ipb_addr(9 downto 0),
    dinb => x"00000000",
    doutb => ipbus_out.ipb_rdata
  );


process(sysclk40) begin
    if rising_edge(sysclk40) then
        strobe_reg <= ipbus_in.ipb_strobe;
        addr0_reg  <= ipbus_in.ipb_addr(0);
        if ipbus_in.ipb_strobe='1' then
            if strobe_reg='0' or (ipbus_in.ipb_addr(0) /= addr0_reg) then ack <= '1';
            else ack <= '0';
            end if;
        else
            ack <= '0';
        end if;
    end if;
end process;

    ipbus_out.ipb_ack <= ack;
    ipbus_out.ipb_err <= '0';

end Behavioral;
