----------------------------------------------------------------------------------
-- Company:         Johannes Gutenberg - Universitaet Mainz
-- Engineer:        Christian Kahra
-- 
-- Create Date:     05/11/2015 06:36:11 PM
-- Module Name:     ipbus_slave_topoInput_playbackspy - Behavioral
-- Project Name:    L1TopoProcessor
-- Target Devices:  Xilinx Virtex-7 xc7vx690tffg1927-3
-- Tool Versions:   Vivado 2015.1
-- Description: 

----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.l1topo_package.all;
use work.ipbus.all;
use work.L1TopoDataTypes.all;



entity ipbus_slave_topoInput_playbackspy is port(

    sysclk40:                       in std_logic;
    
    ipbus_in:                       in ipb_wbus;
    ipbus_out:                      out ipb_rbus;
    
    topoInput_enablePlayback:       in  std_logic;
    topoInput_enableSpy:            in  std_logic;
    topoInput_spyData128:           in  arraySLV128(numberOfChannels-1 downto 0);
    topoInput_playbackData128:      out arraySLV128(numberOfChannels-1 downto 0);
    
    enableAddressCounterSaturation: in  std_logic;
    disableAutoAddressIncrement:    in  std_logic;
    incrementAddress:               in  std_logic;
    rewindPlaybackSpy:              in  std_logic;
    enable1024Events:               in  std_logic
    
);

end ipbus_slave_topoInput_playbackspy;




architecture Behavioral of ipbus_slave_topoInput_playbackspy is
    
    signal addressCounter:                      arraySLV11(numberOfChannels-1 downto 0);
    signal bram_address:                        arraySLV10(numberOfChannels-1 downto 0);
    
    signal enableAddressCounterSaturation_reg:  std_logic_vector(numberOfChannels-1 downto 0);
    signal disableAutoAddressIncrement_reg:     std_logic_vector(numberOfChannels-1 downto 0);
    signal incrementAddress_reg:                std_logic_vector(numberOfChannels-1 downto 0);
    signal rewindPlaybackSpy_reg:               std_logic_vector(numberOfChannels-1 downto 0);
    signal enable1024Events_reg:                std_logic_vector(numberOfChannels-1 downto 0);
    
    
    signal sel:                                 std_logic_vector(numberOfChannels-1 downto 0);
    
    signal bram_enable:                         std_logic_vector(numberOfChannels-1 downto 0);
    signal bram_writeEnable:                    std_logic_vector(numberOfChannels-1 downto 0);
    signal ipbus_bram_writeEnable:              std_logic_vector(numberOfChannels-1 downto 0);

    signal sel_reg:                             std_logic_vector(numberOfChannels-1 downto 0);
    signal addr0_reg:                           std_logic_vector(numberOfChannels-1 downto 0);
    signal ipbWrite_address:                    arraySLV19(numberOfChannels-1 downto 0);
    signal ipbWrite_wdata:                      arraySLV32(numberOfChannels-1 downto 0);
    signal ipbWrite_strobe:                     std_logic_vector(numberOfChannels-1 downto 0);
    signal ipbWrite_write:                      std_logic_vector(numberOfChannels-1 downto 0);
    signal ipbRead_rdata:                       arraySLV32(numberOfChannels-1 downto 0);
    signal ipbRead_rdata_reg:                   arraySLV32(numberOfChannels-1 downto 0);
    signal ipbRead_ack:                         std_logic_vector(numberOfChannels-1 downto 0);
    
    signal state:                               std_logic_vector(numberOfChannels-1 downto 0);
        
        
        
        
begin



process(sysclk40) begin
if rising_edge(sysclk40) then
    for ch in numberOfChannels-1 downto 0 loop
        enableAddressCounterSaturation_reg(ch)  <= enableAddressCounterSaturation;
        disableAutoAddressIncrement_reg(ch)     <= disableAutoAddressIncrement;
        incrementAddress_reg(ch)                <= incrementAddress;
        rewindPlaybackSpy_reg(ch)               <= rewindPlaybackSpy;
        enable1024Events_reg(ch)                <= enable1024Events;
        
        bram_enable(ch)                         <= topoInput_enablePlayback or topoInput_enableSpy;
        bram_writeEnable(ch)                    <= topoInput_enableSpy;
        
        ipbWrite_wdata(ch)                      <= ipbus_in.ipb_wdata;
        ipbWrite_address(ch)                    <= ipbus_in.ipb_addr(18 downto 0);
        ipbWrite_strobe(ch)                     <= ipbus_in.ipb_strobe;
        ipbWrite_write(ch)                      <= ipbus_in.ipb_write;
        if ipbWrite_strobe(ch)='1' and std_logic_vector(to_unsigned(ch, 7))=ipbWrite_address(ch)(18 downto 12) then sel(ch) <= '1';
        else sel(ch) <= '0';
        end if;
        ipbus_bram_writeEnable(ch)              <= sel(ch) and ipbWrite_write(ch) and state(ch);
        
    end loop;
    
end if;
end process;



process(sysclk40) begin
if rising_edge(sysclk40) then
    for ch in numberOfChannels-1 downto 0 loop
        if rewindPlaybackSpy_reg(ch)='1' then addressCounter(ch) <= (10 downto 0 => '0');
        elsif 
            (disableAutoAddressIncrement_reg(ch)='0' or incrementAddress_reg(ch)='1') 
            and (enableAddressCounterSaturation_reg(ch)='0' 
                or (enable1024Events_reg(ch)='1' and addressCounter(ch)(10)='0')
                or (enable1024Events_reg(ch)='0' and addressCounter(ch)(8)='0')
            ) then addressCounter(ch) <= std_logic_vector(unsigned(addressCounter(ch))+1);
        end if;
    end loop;
end if;
end process;




BRAM_GEN: for ch in numberOfChannels-1 downto 0 generate
  EN_ACTIVE_ONLY : if active_channels(ch) = '1' generate
  
    bram_address(ch) <= addressCounter(ch)(9 downto 0) when enable1024Events_reg(ch)='1' else "00" & addressCounter(ch)(7 downto 0);

    
BRAM: entity work.ipbus_playbackspy_bram128
  PORT MAP (
    clka    => sysclk40,
    ena     => bram_enable(ch),
    wea(0)  => bram_writeEnable(ch),
    addra   => bram_address(ch),
    dina    => topoInput_spyData128(ch),
    douta   => topoInput_playbackData128(ch),
    clkb    => sysclk40,
    enb     => sel(ch),
    web(0)  => ipbus_bram_writeEnable(ch),
    addrb   => ipbWrite_address(ch)(11 downto 0),
    dinb    => ipbWrite_wdata(ch),
    doutb   => ipbRead_rdata(ch)
  );

  end generate EN_ACTIVE_ONLY;
end generate;



process(sysclk40) begin
if rising_edge(sysclk40) then
    
    for ch in numberOfChannels-1 downto 0 loop
        
        sel_reg(ch)     <= sel(ch);
        addr0_reg(ch)   <= ipbWrite_address(ch)(0);
        
        if sel(ch)='1' and (sel_reg(ch)='0' or addr0_reg(ch)/=ipbWrite_address(ch)(0)) then state(ch) <='1';
        else state(ch) <='0';
        end if;
        
        if state(ch)='1' then 
            ipbRead_rdata_reg(ch) <= ipbRead_rdata(ch); 
            ipbRead_ack(ch) <= '1';
        else
            ipbRead_ack(ch) <= '0';
        end if;
        
    end loop;
    
end if;
end process;



    ipbus_out.ipb_rdata <= ipbRead_rdata(to_integer(unsigned(ipbus_in.ipb_addr(18 downto 12))));
    ipbus_out.ipb_ack   <= ipbRead_ack(to_integer(unsigned(ipbus_in.ipb_addr(18 downto 12))));
    ipbus_out.ipb_err   <= '0';



end Behavioral;
