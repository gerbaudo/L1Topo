----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 15.07.2014 12:06:08
-- Design Name: 
-- Module Name: extended_ipbus - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.l1topo_package.all;
use work.ipbus.all;
use work.L1TopoDataTypes.all;
use work.L1TopoGTConfiguration.all;
use work.rod_l1_topo_types_const.all;


-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity controlPath is
    generic (
        PROCESSOR_NUMBER : integer := 1
    );
    Port (
        sysclk40: in std_logic;
        sysclk80: in std_logic;
        sysclk160: in std_logic;
        sysclk400: in std_logic;
        sysclk_pll_locked: in std_logic;
        
        ipbBridgeBusIn: in std_logic_vector(4 downto 0);
        ipbBridgeBusOut: out std_logic_vector(2 downto 0);
        
        
        -- module interfaces
        
        clk_control:  out std_logic;
        clk_status:   in  std_logic_vector(3 downto 0);

        pll_reset: out std_logic;
        
        xadc_control: out std_logic_vector(25 downto 0);
        xadc_status:  in  std_logic_vector(22 downto 0);
        
        ttcBridge_control: out std_logic_vector(5 downto 0);
        ttcBridge_status: in std_logic_vector(14 downto 0);
        ttcBroadcast: in std_logic;

        rod_control_register: out rod_control_registers_array;
        rod_status_register : in  rod_status_registers_array;
                

        -- Real-Time path configuration

        ChannelControl: out arraySLV32(79 downto 0);
        ChannelStatus: in arraySLV32(79 downto 0);
        
        GeneralControl:	out std_logic_vector(31 downto 0);
        
        -- Real-Time path data
        
        topoInput_enablePlayback:   out  std_logic;
        topoInput_playbackData128:   out  arraySLV128(numberOfChannels-1 downto 0);
        topoInput_spyData128:        in   arraySLV128(numberOfChannels-1 downto 0);
    
        
        
        SortParameters: out ParameterSpace(NumberOfSortAlgorithms - 1 downto 0);
        AlgoParameters: out ParameterSpace(NumberOfAlgorithms - 1 downto 0);
        
        spyData64BitFromAlgos: in std_logic_vector(2*NumberOfResultBits-1 downto 0);
        enablePlaybackOfAlgos: out std_logic;
        playbackData32BitFakingAlgos: out std_logic_vector(31 downto 0);
        
        rateCounter:        in arraySLV32(numberOfResultBits-1 downto 0);
        rateCounterOv:      in arraySLV32(numberOfResultBits-1 downto 0);
        rateCounter_valid:  in std_logic;
        
        ctpOutputControl: out std_logic_vector(1 downto 0);
        ctpOutputMask: out std_logic_vector(31 downto 0);
        
        ctp_odelay_control: out std_logic_vector(5 downto 0);
        ctp_odelay_pulse:   out std_logic_vector(1 downto 0);
        ctp_odelay_status:  in  arraySLV5(15 downto 0);
        
        -- MISC
        
        ProcessorID:        in std_logic_vector(31 downto 0);
        FirmwareVersion:    in std_logic_vector(31 downto 0);
        FirmwareDate:       in std_logic_vector(31 downto 0);
        FirmwareRevision:   in std_logic_vector(31 downto 0);
        usr_access:         in std_logic_vector(31 downto 0);
  
        JetTOBs:            in JetArray (InputWidthJet - 1 downto 0);
        met :               in MetArray (InputWidthMET - 1 downto 0);
--        BCID:               in std_logic_vector(11 downto 0)
        EventCounterReset : in  std_logic;
        L1Accept          : in  std_logic;
--        BunchCounterReset : in  std_logic
        BCID            : in  std_logic_vector(11 downto 0)
    );
end controlPath;

architecture Behavioral of controlPath is

    signal ipb_rst: std_logic;
    signal ipb_master_write: ipb_wbus;
    signal ipb_master_read: ipb_rbus;
    
    signal ipbBridge_idelayStatus: std_logic_vector(24 downto 0);
    signal ipbBridge_alignmentStatus: std_logic_vector(19 downto 0);
    signal ipbBridgeErrorCounter: std_logic_vector(29 downto 0);
    signal ipbBridgeErrorCounterReset: std_logic;
    
--    signal ipbr_ack: std_logic_vector(57 downto 0);
    
--    signal idelay_counter_0 :  std_logic_vector(4 downto 0);
--    signal idelay_counter_1 :  std_logic_vector(4 downto 0);
--    signal idelay_counter_2 :  std_logic_vector(4 downto 0);
--    signal idelay_counter_3 :  std_logic_vector(4 downto 0);
--    signal idelay_counter_4 :  std_logic_vector(4 downto 0);
    
begin



ipbBridge: entity work.ipbusBridge
    port map(
        sysclk => sysclk40,
        parallelclk => sysclk80,
        serialclk => sysclk400,
        pll_locked => sysclk_pll_locked,
        
        -- ipbBridge request
        ipbBridgeBusIn => ipbBridgeBusIn,
        ipb_write => ipb_master_write,
        
        -- ipbBridge response
        ipb_read => ipb_master_read,
        ipbBridgeBusOut => ipbBridgeBusOut,
        
        idelayStatus => ipbBridge_idelayStatus, 
        alignmentStatus_out => ipbBridge_alignmentStatus,
        errorCounter_out => ipbBridgeErrorCounter,
        errorCounterReset => ipbBridgeErrorCounterReset--,
        
--        ipbr_ack => ipbr_ack
         
    );







    





slaves: entity work.slaves
  generic map(
    PROCESSOR_NUMBER => PROCESSOR_NUMBER
    )
  port map(
        sysclk40 => sysclk40,
        sysclk80 => sysclk80,
        sysclk160 => sysclk160,
        sysclk_pll_locked => sysclk_pll_locked,
        
        -- ipbBridge
        
        ipb_in => ipb_master_write,
        ipb_out => ipb_master_read,
        
        
        
        ipbBridge_idelayStatus => ipbBridge_idelayStatus,
        ipbBridge_alignmentStatus => ipbBridge_alignmentStatus,
        ipbBridgeErrorCounter => ipbBridgeErrorCounter,
        ipbBridgeErrorCounterReset => ipbBridgeErrorCounterReset,
        
        
        
        -- module interfaces
        
        clk_control                     => clk_control,
        clk_status                      => clk_status,
        
        xadc_control => xadc_control,
        xadc_status  => xadc_status,
        
        pll_reset    => pll_reset,
        
        ttcBridge_control => ttcBridge_control,
        ttcBridge_status => ttcBridge_status,
        ttcBroadcast => ttcBroadcast,

        rod_control_register => rod_control_register,
        rod_status_register  => rod_status_register,


        -- Real-Time path configuration

        ChannelControl => ChannelControl,
        ChannelStatus => ChannelStatus,
        
        GeneralControl => GeneralControl,
        
        -- Real-Time path data
        
        topoInput_enablePlayback_out => topoInput_enablePlayback,
        topoInput_playbackData128    => topoInput_playbackData128,
        topoInput_spyData128         => topoInput_spyData128,
        
        SortParameters => SortParameters,
        AlgoParameters => AlgoParameters,
        
        spyData64BitFromAlgos        => spyData64BitFromAlgos,
        enablePlaybackOfAlgos_out    => enablePlaybackOfAlgos,
        playbackData32BitFakingAlgos => playbackData32BitFakingAlgos,

        rateCounter                 => rateCounter,
        rateCounterOv                 => rateCounterOv,
        rateCounter_valid           => rateCounter_valid,
        
        ctpOutputControl => ctpOutputControl,
        ctpOutputMask => ctpOutputMask,
        
        ctp_odelay_control              => ctp_odelay_control,
        ctp_odelay_pulse                => ctp_odelay_pulse,
        ctp_odelay_status               => ctp_odelay_status,
        
        -- MISC
        
        ProcessorID         => ProcessorID,
        FirmwareVersion     => FirmwareVersion,
        FirmwareDate        => FirmwareDate,
        FirmwareRevision    => FirmwareRevision,
        usr_access          => usr_access,
        
        JetTOBs             => JetTOBs,
        Met                 => Met,
--        BCID                => BCID
        EventCounterReset   => EventCounterReset,
        L1Accept            => L1Accept,
--        BunchCounterReset   => BunchCounterReset
        BCID                => BCID
--        ipbr_ack => ipbr_ack
    );


end Behavioral;
