-- The ipbus slaves live in this entity - modify according to requirements
--
-- Ports can be added to give ipbus slaves access to the chip top level.
--
-- Dave Newbold, February 2011

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

use work.l1topo_package.all;
use work.ipbus.all;
use work.L1TopoDataTypes.all;
use work.L1TopoGTConfiguration.all;
use work.rod_l1_topo_types_const.all;

entity slaves is
  generic (
    PROCESSOR_NUMBER : integer := 1;
    DISABLE_HISTOGRAMS : integer := 0
    );
    port(
      sysclk40          : in std_logic;
      sysclk80          : in std_logic;
      sysclk160         : in std_logic;
      sysclk_pll_locked : in std_logic;

      ipb_in  : in  ipb_wbus;
      ipb_out : out ipb_rbus;


      ipbBridge_idelayStatus     : in  std_logic_vector(24 downto 0);
      ipbBridge_alignmentStatus  : in  std_logic_vector(19 downto 0);
      ipbBridgeErrorCounter      : in  std_logic_vector(29 downto 0);
      ipbBridgeErrorCounterReset : out std_logic;

      clk_control : out std_logic;
      clk_status  : in  std_logic_vector(3 downto 0);

      xadc_control : out std_logic_vector(25 downto 0);
      xadc_status  : in  std_logic_vector(22 downto 0);

      pll_reset : out std_logic;

      ttcBridge_control : out std_logic_vector(5 downto 0);
      ttcBridge_status  : in  std_logic_vector(14 downto 0);
      ttcBroadcast      : in  std_logic;


      rod_control_register : out rod_control_registers_array;
      rod_status_register  : in  rod_status_registers_array;


      ChannelControl : out arraySLV32(79 downto 0);
      ChannelStatus  : in  arraySLV32(79 downto 0);

      GeneralControl : out std_logic_vector(31 downto 0);


      topoInput_enablePlayback_out : out std_logic;
      topoInput_playbackData128    : out arraySLV128(numberOfChannels-1 downto 0);
      topoInput_spyData128         : in  arraySLV128(numberOfChannels-1 downto 0);



      SortParameters : out ParameterSpace(NumberOfSortAlgorithms - 1 downto 0);
      AlgoParameters : out ParameterSpace(NumberOfAlgorithms - 1 downto 0);


      spyData64BitFromAlgos        : in  std_logic_vector(2*NumberOfResultBits-1 downto 0);
      enablePlaybackOfAlgos_out    : out std_logic;
      playbackData32BitFakingAlgos : out std_logic_vector(31 downto 0);

      rateCounter       : in arraySLV32(numberOfResultBits-1 downto 0);
      rateCounterOv     : in arraySLV32(numberOfResultBits-1 downto 0);
      rateCounter_valid : in std_logic;

      ctpOutputControl : out std_logic_vector(1 downto 0);
      ctpOutputMask    : out std_logic_vector(31 downto 0);

      ctp_odelay_control : out std_logic_vector(5 downto 0);
      ctp_odelay_pulse   : out std_logic_vector(1 downto 0);
      ctp_odelay_status  : in  arraySLV5(15 downto 0);

      -- MISC

      ProcessorID      : in std_logic_vector(31 downto 0);
      FirmwareVersion  : in std_logic_vector(31 downto 0);
      FirmwareDate     : in std_logic_vector(31 downto 0);
      FirmwareRevision : in std_logic_vector(31 downto 0);
      usr_access       : in std_logic_vector(31 downto 0);

--        ipbr_ack:        out std_logic_vector(57 downto 0)

      JetTOBs : in JetArray (InputWidthJet - 1 downto 0);
      Met     : in MetArray (InputWidthMET - 1 downto 0);
      EventCounterReset : in  std_logic;
      L1Accept          : in  std_logic;
--      BunchCounterReset : in  std_logic
      BCID            : in  std_logic_vector(11 downto 0)
      );

end slaves;

architecture rtl of slaves is

  constant NSLV : positive := 72;

  attribute shreg_extract : string;

  type ipbw_regArray is array(natural range<>) of ipb_wbus_array(NSLV-1 downto 0);
  type ipbr_regArray is array(natural range<>) of ipb_rbus_array(NSLV-1 downto 0);

  signal ipbw : ipb_wbus_array(NSLV-1 downto 0);
  signal ipbr : ipb_rbus_array(NSLV-1 downto 0);

  signal ipbw_reg                     : ipbw_regArray(1 downto 0);
  attribute shreg_extract of ipbw_reg : signal is "no";

  signal ipbr_reg                     : ipbr_regArray(1 downto 0);
  attribute shreg_extract of ipbr_reg : signal is "no";

  signal ipb_out_int : ipb_rbus;

  signal ipb_rst : std_logic;


  signal GeneralStatus_32bit  : std_logic_vector(31 downto 0);
  signal GeneralControl_32bit : std_logic_vector(31 downto 0);
  signal GeneralPulse_32bit   : std_logic_vector(31 downto 0);


  signal ipbBridge_idelayStatus_32bit     : std_logic_vector(31 downto 0);
  signal ipbBridge_alignmentStatus_32bit  : std_logic_vector(31 downto 0);
  signal ipbBridgeErrorCounter_32bit      : std_logic_vector(31 downto 0);
  signal ipbBridgeErrorCounterReset_32bit : std_logic_vector(31 downto 0);

  signal ttcBridge_status_32bit     : std_logic_vector(31 downto 0);
  signal ttcBridge_control_32bit    : std_logic_vector(31 downto 0);
  signal ttcErrorCounter_32bit      : std_logic_vector(31 downto 0);
  signal ttcErrorCounterReset_32bit : std_logic_vector(31 downto 0);

  signal PlaybackSpyControl             : std_logic_vector(31 downto 0);
  signal topoInput_enableSpy            : std_logic;
  signal algoResult_enableSpy           : std_logic;
  signal topoInput_enablePlayback       : std_logic;
  signal enablePlaybackOfAlgos          : std_logic;
  signal enableAddressCounterSaturation : std_logic;
  signal disableAutoAddressIncrement    : std_logic;
  signal enable1024Events               : std_logic;

  signal QuadControl_vector     : std_logic_vector(20*32-1 downto 0);
  signal QuadStatus_vector      : std_logic_vector(20*32-1 downto 0);
  signal ChannelControl_vector  : std_logic_vector(80*32-1 downto 0);
  signal ChannelStatus_vector   : std_logic_vector(80*32-1 downto 0);
  signal DataShift40MHz_vector  : std_logic_vector((MGTHigh-MGTLow+1)*32-1 downto 0);
  signal CRCErrorCounter_vector : std_logic_vector(20*32-1 downto 0);





  signal ROD_Debug              : std_logic_vector(63 downto 0);
  signal ROD_Slices_vector      : std_logic_vector(11*32-1 downto 0);
  signal ROD_Offsets_vector     : std_logic_vector(11*32-1 downto 0);
  signal ROD_Send_On_CRC_vector : std_logic_vector(3*32-1 downto 0);


  signal fakeDataCounter                           : std_logic_vector(127 downto 0);
  signal fakeData                                  : arraySLV128(MGTHigh downto MGTLow);
  signal playbackData128BitFakingDeserialisers_int : arraySLV128(79 downto 0);
  signal ipbScopeControl                           : std_logic_vector(31 downto 0);
--    signal debugParameters: std_logic_vector(127 downto 0);
--  signal ipbusPlaybackCharIsKFakingMGTs: std_logic_vector(31 downto 0);
--  signal ipbusSpyCharIsKFromMGTs: std_logic_vector(31 downto 0);

  signal AlgoDebug128 : std_logic_vector(127 downto 0);

  signal rewindPlaybackSpy : std_logic;

  signal ctpOutputControl_32bit : std_logic_vector(31 downto 0);
  signal rateCounter_reg        : std_logic_vector(numberOfResultBits*32-1 downto 0);
  signal rateCounterOv_reg      : std_logic_vector(numberOfResultBits*32-1 downto 0);

  signal ctp_odelay_control_32bit : std_logic_vector(31 downto 0);
  signal ctp_odelay_pulse_32bit   : std_logic_vector(31 downto 0);
  signal ctp_odelay_status_512bit : std_logic_vector(511 downto 0);

  signal clk_control_32bit : std_logic_vector(31 downto 0);
  signal clk_status_32bit  : std_logic_vector(31 downto 0);

  -- histogramming
  signal ClockBus                               : std_logic_vector(2 downto 0);
  signal HistTimestamp, histStatus, ECRoverflow : std_logic_vector(31 downto 0);
  signal HistogramNumber                        : unsigned(31 downto 0);
  signal HistControl, HistControl2              : std_logic_vector(31 downto 0);
  signal BcidVeto                               : std_logic_vector(4096 - 1 downto 0);
  signal HistControlReset                       : std_logic;
  signal HistogramPulse                         : std_logic_vector(31 downto 0);
  signal HistResetCtr                           : std_logic_vector(31 downto 0);
  signal NumLhcTurns                            : std_logic_vector(31 downto 0);
begin

  --ipb_rst <= not sysclk_pll_locked; --all initial values aftrer reset are
  --set to 0 (if not inititilised in the code)


  fabric : entity work.ipbus_fabric
    generic map(NSLV => NSLV)
    port map(
      ipb_in          => ipb_in,
      ipb_out         => ipb_out_int,
      ipb_to_slaves   => ipbw_reg(0),
      ipb_from_slaves => ipbr_reg(0)
      );

  ipb_out <= ipb_out_int;



  process(sysclk40)
  begin
    if rising_edge(sysclk40) then
      for i in NSLV-1 downto 0 loop
        for stage in 1 downto 1 loop
          ipbw_reg(stage)   <= ipbw_reg(stage-1);
          ipbr_reg(stage-1) <= ipbr_reg(stage);
        end loop;
        ipbw(i)        <= ipbw_reg(1)(i);
        ipbr_reg(1)(i) <= ipbr(i);
      end loop;
    end if;
  end process;



--IPBR_ACK_GEN: for s in NSLV-1 downto 0 generate
--    ipbr_ack(s) <= ipbr_reg(0)(s).ipb_ack;
--end generate;



----------------------------------------------------------------


  slave0_ProcessorID : entity work.ipbus_slave_status
    generic map(addr_width => 0)
    port map(
      clk       => sysclk40,
      reset     => ipb_rst,
      ipbus_in  => ipbw(0),
      ipbus_out => ipbr(0),
      d         => ProcessorID
      );


----------------------------------------------------------------


  slave1_FirmwareVersion : entity work.ipbus_slave_status
    generic map(addr_width => 0)
    port map(
      clk       => sysclk40,
      reset     => ipb_rst,
      ipbus_in  => ipbw(1),
      ipbus_out => ipbr(1),
      d         => FirmwareVersion
      );


----------------------------------------------------------------


  slave15_FirmwareDate : entity work.ipbus_slave_status
    generic map(addr_width => 0)
    port map(
      clk       => sysclk40,
      reset     => ipb_rst,
      ipbus_in  => ipbw(15),
      ipbus_out => ipbr(15),
      d         => FirmwareDate
      );


----------------------------------------------------------------


  slave16_FirmwareRevision : entity work.ipbus_slave_status
    generic map(addr_width => 0)
    port map(
      clk       => sysclk40,
      reset     => ipb_rst,
      ipbus_in  => ipbw(16),
      ipbus_out => ipbr(16),
      d         => FirmwareRevision
      );


----------------------------------------------------------------


  slave8_Usr_Access : entity work.ipbus_slave_status
    generic map(addr_width => 0)
    port map(
      clk       => sysclk40,
      reset     => ipb_rst,
      ipbus_in  => ipbw(8),
      ipbus_out => ipbr(8),
      d         => usr_access
      );


----------------------------------------------------------------

  --process(sysclk40, sysclk_pll_locked) begin
  --    if sysclk_pll_locked='0' then
  --      GeneralStatus_32bit(0) <= '1';
  --    elsif rising_edge(sysclk40) then
  --      if GeneralPulse_32bit(0)='1' then
  --        GeneralStatus_32bit(0) <= '0';
  --      end if;
  --    end if; 
  --end process;
  GeneralStatus_32bit(0) <= sysclk_pll_locked;

  slave2_GeneralStatus : entity work.ipbus_slave_status
    generic map(addr_width => 0)
    port map(
      clk       => sysclk40,
      reset     => ipb_rst,
      ipbus_in  => ipbw(2),
      ipbus_out => ipbr(2),
      d         => GeneralStatus_32bit
      );


----------------------------------------------------------------    


  slave3_GeneralControl : entity work.ipbus_slave_control
    generic map(addr_width => 0)
    port map(
      clk       => sysclk40,
      reset     => ipb_rst,
      ipbus_in  => ipbw(3),
      ipbus_out => ipbr(3),
      q         => GeneralControl_32bit
      );

  GeneralControl <= GeneralControl_32bit;

----------------------------------------------------------------    


  slave4_GeneralPulse : entity work.ipbus_slave_control
    generic map(addr_width => 0)
    port map(
      clk       => sysclk40,
      reset     => ipb_rst,
      ipbus_in  => ipbw(4),
      ipbus_out => ipbr(4),
      q         => GeneralPulse_32bit
      );


  pll_reset <= GeneralPulse_32bit(0);  --not a pulse ! see infrastructure for details

----------------------------------------------------------------


  ipbBridge_idelayStatus_32bit <= (31 downto 25 => '0') & ipbBridge_idelayStatus;

  slave5_ipbBridgeIDelayStatus : entity work.ipbus_slave_status
    generic map(addr_width => 0)
    port map(
      clk       => sysclk40,
      reset     => ipb_rst,
      ipbus_in  => ipbw(5),
      ipbus_out => ipbr(5),
      d         => ipbBridge_idelayStatus_32bit
      );

----------------------------------------------------------------


  ipbBridge_alignmentStatus_32bit <= (31 downto 20 => '0') & ipbBridge_alignmentStatus;

  slave6_ipbBridgeAlignmentStatus : entity work.ipbus_slave_status
    generic map(addr_width => 0)
    port map(
      clk       => sysclk40,
      reset     => ipb_rst,
      ipbus_in  => ipbw(6),
      ipbus_out => ipbr(6),
      d         => ipbBridge_alignmentStatus_32bit
      );


----------------------------------------------------------------    


  ipbBridgeErrorCounter_32bit <= (31 downto 30 => '0') & ipbBridgeErrorCounter(29 downto 0);

  slave7_ipbBridgeErrorCounter : entity work.ipbus_slave_status
    generic map(addr_width => 0)
    port map(
      clk       => sysclk40,
      reset     => ipb_rst,
      ipbus_in  => ipbw(7),
      ipbus_out => ipbr(7),
      d         => ipbBridgeErrorCounter_32bit
      );


----------------------------------------------------------------


--slave8_ipbBridgeErrorCounterReset: entity work.ipbus_slave_reg_pulse
--        generic map(addr_width => 0)
--        port map(
--            clk => sysclk40,
--            reset => ipb_rst,
--            ipbus_in => ipbw(8),
--            ipbus_out => ipbr(8),
--            q => ipbBridgeErrorCounterReset_32bit
--        );

--    ipbBridgeErrorCounterReset(4 downto 0) <= ipbBridgeErrorCounterReset_32bit(4 downto 0); 
--    ipbBridgeErrorCounterReset(5) <= ipbBridgeErrorCounterReset_32bit(5) or GeneralPulse_32bit(1); 


  ipbBridgeErrorCounterReset <= GeneralPulse_32bit(1);

----------------------------------------------------------------    


  ttcBridge_status_32bit <= (31 downto 15 => '0') & ttcBridge_status(14 downto 0);

  slave9_ttcBridgeStatus : entity work.ipbus_slave_status
    generic map(addr_width => 0)
    port map(
      clk       => sysclk40,
      reset     => ipb_rst,
      ipbus_in  => ipbw(9),
      ipbus_out => ipbr(9),
      d         => ttcBridge_status_32bit
      );


----------------------------------------------------------------    


  slave10_ttcBridgeControl : entity work.ipbus_slave_control
    generic map(addr_width => 0)
    port map(
      clk       => sysclk40,
      reset     => ipb_rst,
      ipbus_in  => ipbw(10),
      ipbus_out => ipbr(10),
      q         => ttcBridge_control_32bit
      );

  ttcBridge_control(4 downto 0) <= ttcBridge_control_32bit(4 downto 0);


----------------------------------------------------------------


  slave11_ttcBridgeErrorCounterReset : entity work.ipbus_slave_reg_pulse
    generic map(addr_width => 0)
    port map(
      clk       => sysclk40,
      reset     => ipb_rst,
      ipbus_in  => ipbw(11),
      ipbus_out => ipbr(11),
      q         => ttcErrorCounterReset_32bit
      );

  ttcBridge_control(5) <= ttcErrorCounterReset_32bit(0);


----------------------------------------------------------------



  slave12_playbackSpyControl : entity work.ipbus_slave_control
    generic map(addr_width => 0)
    port map(
      clk       => sysclk40,
      reset     => ipb_rst,
      ipbus_in  => ipbw(12),
      ipbus_out => ipbr(12),
      q         => PlaybackSpyControl
      );

  topoInput_enableSpy            <= PlaybackSpyControl(1);
  algoResult_enableSpy           <= PlaybackSpyControl(2);
  topoInput_enablePlayback       <= PlaybackSpyControl(4);
  enablePlaybackOfAlgos          <= PlaybackSpyControl(5);
  enableAddressCounterSaturation <= PlaybackSpyControl(6);
  disableAutoAddressIncrement    <= PlaybackSpyControl(7);
  enable1024Events               <= PlaybackSpyControl(8);

  topoInput_enablePlayback_out <= topoInput_enablePlayback;
  enablePlaybackOfAlgos_out    <= enablePlaybackOfAlgos;

----------------------------------------------------------------    


  slave13_xadc_drpInterface : entity work.ipbus_slave_xadc
    port map(
      clk          => sysclk40,
      reset        => ipb_rst,
      ipbus_in     => ipbw(13),
      ipbus_out    => ipbr(13),
      xadc_control => xadc_control,
      xadc_status  => xadc_status
      );


----------------------------------------------------------------


  slave14_TestRAM : entity work.ipbus_slave_testram
    generic map(addr_width => 10)
    port map(
      clk       => sysclk40,
      reset     => ipb_rst,
      ipbus_in  => ipbw(14),
      ipbus_out => ipbr(14)
      );


----------------------------------------------------------------


  slave17_ChannelControl : entity work.ipbus_slave_control_N
    generic map(numberOfRegisters => 80)
    port map(
      clk       => sysclk40,
      reset     => ipb_rst,
      ipbus_in  => ipbw(17),
      ipbus_out => ipbr(17),
      q         => ChannelControl_vector
      );


  ChannelControl_GEN : for i in 79 downto 0 generate
  begin
    ChannelControl(i) <= ChannelControl_vector(i*32+31 downto i*32) when (active_channels(i) = '1' or i = 77) else (others => '0');
  end generate;


----------------------------------------------------------------


  ChannelStatus_GEN : for i in 79 downto 0 generate
  begin
    ChannelStatus_vector(i*32+31 downto i*32) <= ChannelStatus(i) when (active_channels(i) = '1' or i = 77) else (others => '0');
  end generate;



  slave18_ChannelStatus : entity work.ipbus_slave_status_N
    generic map(numberOfRegisters => 80)
    port map(
      clk       => sysclk40,
      reset     => ipb_rst,
      ipbus_in  => ipbw(18),
      ipbus_out => ipbr(18),
      d         => ChannelStatus_vector
      );


----------------------------------------------------------------


--slave19_DataDelay: entity work.ipbus_slave_control_N
--        generic map(numberOfRegisters => (MGTHigh-MGTLow+1) )
--        port map(
--            clk => sysclk40,
--            reset => ipb_rst,
--            ipbus_in => ipbw(19),
--            ipbus_out => ipbr(19),
--            q => DataShift40MHz_vector
--        );


--    DataShift40MHz_GEN: for i in (MGTHigh-MGTLow) downto 0 generate begin   
--        DataShift40MHz(MGTLow+i) <= DataShift40MHz_vector(i*32+2 downto i*32);
--    end generate;


----------------------------------------------------------------

----------------------------------------------------------------


--    process(sysclk40) begin
--        if rising_edge(sysclk40) then
--            fakeDataCounter <= std_logic_vector(unsigned(fakeDataCounter)+1);
--        end if;
--    end process;

--FAKEDATA_GEN: for i in MGTHigh downto MGTLow generate begin
--    fakeData(i) <= fakeDataCounter;
--end generate;

  rewindPlaybackSpy <= '1' when ttcBroadcast = '1' or GeneralPulse_32bit(2) = '1' else '0';


--slave21_playbackSpyDataOfDeserialisers: entity work.ipbus_slave_playbackspy128
--        port map(
--            sysclk40 => sysclk40,
--            sysclk160 => sysclk160,
--            ipbus_in => ipbw(21),
--            ipbus_out => ipbr(21),

--            spyData128Bit => spyData128BitFromDeserialisers(MGTHigh downto MGTLow),
--            playbackData128Bit => playbackData128BitFakingDeserialisers_int,

--            enableSpy => PlaybackspyControl(1),

--            enableAddressIncrement => '1',
--            enableAddressCounterSaturation => PlaybackSpyControl(6),
--            rewindPlaybackSpy => rewindPlaybackSpy,
--            fineDelay => PlaybackSpyControl(8 downto 7)
----            debugPlaybackSpy128 => debugPlaybackSpy128
--        );


--    playbackData128BitFakingDeserialisers <= playbackData128BitFakingDeserialisers_int;


  slave21_topoInput_PlaybackSpy : entity work.ipbus_slave_topoInput_playbackspy
    port map(
      sysclk40                       => sysclk40,
      ipbus_in                       => ipbw(21),
      ipbus_out                      => ipbr(21),
      topoInput_enablePlayback       => topoInput_enablePlayback,
      topoInput_enableSpy            => topoInput_enableSpy,
      topoInput_spyData128           => topoInput_spyData128,
      topoInput_playbackData128      => topoInput_playbackData128,
      enableAddressCounterSaturation => enableAddressCounterSaturation,
      disableAutoAddressIncrement    => disableAutoAddressIncrement,
      enable1024Events               => '0',  --enable1024Events,
      incrementAddress               => '0',
      rewindPlaybackSpy              => rewindPlaybackSpy
      );



----------------------------------------------------------------

  slave22_PlaybackSpyDataOfAlgoResults : entity work.ipbus_slave_playbackspy32
    port map(
      sysclk40  => sysclk40,
      ipbus_in  => ipbw(22),
      ipbus_out => ipbr(22),

      spyData32Bit                   => spyData64BitFromAlgos(31 downto 0),
      playbackData32Bit              => playbackData32BitFakingAlgos,
      enableSpy                      => algoResult_enableSpy,
      rewindPlaybackSpy              => rewindPlaybackSpy,
      enableAddressCounterSaturation => enableAddressCounterSaturation,
      enable1024Events               => '0'  -- enable1024Events
      );

------------------------------------------------------------------

--slave22_ROD_OvBusy: entity work.ipbus_slave_control
--        generic map(addr_width => 0)
--        port map(
--            clk => sysclk40,
--            reset => ipb_rst,
--            ipbus_in => ipbw(22),
--            ipbus_out => ipbr(22),
--            q => ROD_OverwriteBusy
--        );

----------------------------------------------------------------

  slave23_ROD_FirmwareVersion : entity work.ipbus_slave_status
    generic map(addr_width => 0)
    port map(
      clk       => sysclk40,
      reset     => ipb_rst,
      ipbus_in  => ipbw(23),
      ipbus_out => ipbr(23),
      d         => rod_status_register(0)
      );

----------------------------------------------------------------

  slave24_ROD_Debug : entity work.ipbus_slave_status
    generic map(addr_width => 0)
    port map(
      clk       => sysclk40,
      reset     => ipb_rst,
      ipbus_in  => ipbw(24),
      ipbus_out => ipbr(24),
      d         => rod_status_register(2)
      );


----------------------------------------------------------------

  slave25_ROD_Reset : entity work.ipbus_slave_reg_pulse
    generic map(addr_width => 0)
    port map(
      clk       => sysclk40,
      reset     => ipb_rst,
      ipbus_in  => ipbw(25),
      ipbus_out => ipbr(25),
      q         => rod_control_register(0)
      );

----------------------------------------------------------------

  slave26_ROD_L1A_Offset : entity work.ipbus_slave_control
    generic map(addr_width => 0)
    port map(
      clk       => sysclk40,
      reset     => ipb_rst,
      ipbus_in  => ipbw(26),
      ipbus_out => ipbr(26),
      q         => rod_control_register(1)
      );


----------------------------------------------------------------

  slave27_ROD_L1A_Fill : entity work.ipbus_slave_control
    generic map(addr_width => 0)
    port map(
      clk       => sysclk40,
      reset     => ipb_rst,
      ipbus_in  => ipbw(27),
      ipbus_out => ipbr(27),
      q         => rod_control_register(2)
      );


----------------------------------------------------------------

  slave28_ROD_Slices : entity work.ipbus_slave_control_N
    generic map(numberOfRegisters => 11)
    port map(
      clk       => sysclk40,
      reset     => ipb_rst,
      ipbus_in  => ipbw(28),
      ipbus_out => ipbr(28),
      q         => ROD_Slices_vector
      );

  ROD_Slices_GEN : for i in 13 downto 3 generate
    rod_control_register(i) <= ROD_Slices_vector((i-3)*32+31 downto (i-3)*32);
  end generate;

----------------------------------------------------------------

  slave29_ROD_Offsets : entity work.ipbus_slave_control_N
    generic map(numberOfRegisters => 11)
    port map(
      clk       => sysclk40,
      reset     => ipb_rst,
      ipbus_in  => ipbw(29),
      ipbus_out => ipbr(29),
      q         => ROD_Offsets_vector
      );

  ROD_Offsets_GEN : for i in 24 downto 14 generate
    rod_control_register(i) <= ROD_Offsets_vector((i-14)*32+31 downto (i-14)*32);
  end generate;


----------------------------------------------------------------


  slave30_Delay_CMX_Data : entity work.ipbus_slave_control
    generic map(addr_width => 0)
    port map(
      clk       => sysclk40,
      reset     => ipb_rst,
      ipbus_in  => ipbw(30),
      ipbus_out => ipbr(30),
      q         => rod_control_register(25)
      );


----------------------------------------------------------------


  slave31_AlgoParameters : entity work.ipbus_slave_parameters
    port map(
      clk            => sysclk40,
      ipbus_in       => ipbw(31),
      ipbus_out      => ipbr(31),
      SortParameters => SortParameters,
      AlgoParameters => AlgoParameters
--            debug => debugParameters
      );


----------------------------------------------------------------


  slave32_ROD_Max_Offset : entity work.ipbus_slave_control
    generic map(addr_width => 0)
    port map(
      clk       => sysclk40,
      reset     => ipb_rst,
      ipbus_in  => ipbw(32),
      ipbus_out => ipbr(32),
      q         => rod_control_register(26)
      );

----------------------------------------------------------------

  slave33_ROD_Send_On_CRC : entity work.ipbus_slave_control_N
    generic map(numberOfRegisters => 3)
    port map(
      clk       => sysclk40,
      reset     => ipb_rst,
      ipbus_in  => ipbw(33),
      ipbus_out => ipbr(33),
      q         => ROD_Send_On_CRC_vector
      );

  ROD_Send_On_CRC_GEN : for i in 29 downto 27 generate
    rod_control_register(i) <= ROD_Send_On_CRC_vector((i-27)*32+31 downto (i-27)*32);
  end generate;






----------------------------------------------------------------


  slave52_ctpOutputControl : entity work.ipbus_slave_control
    generic map(addr_width => 0)
    port map(
      clk       => sysclk40,
      reset     => ipb_rst,
      ipbus_in  => ipbw(52),
      ipbus_out => ipbr(52),
      q         => ctpOutputControl_32bit

      );

  ctpOutputControl <= ctpOutputControl_32bit(1 downto 0);


----------------------------------------------------------------


  slave53_ctpOutputMask : entity work.ipbus_slave_control
    generic map(addr_width => 0)
    port map(
      clk       => sysclk40,
      reset     => ipb_rst,
      ipbus_in  => ipbw(53),
      ipbus_out => ipbr(53),
      q         => ctpOutputMask

      );

----------------------------------------------------------------



  process(sysclk40)
  begin
    if rising_edge(sysclk40) then
      if rateCounter_valid = '1' then
        for i in numberOfResultBits-1 downto 0 loop
          rateCounter_reg(i*32+31 downto i*32)   <= rateCounter(i);
          rateCounterOv_reg(i*32+31 downto i*32) <= rateCounterOv(i);
        end loop;
      end if;
    end if;
  end process;


----------------------------------------------------------------


  slave54_RateCounter : entity work.ipbus_slave_status
    generic map(addr_width => 5)
    port map(
      clk       => sysclk40,
      reset     => ipb_rst,
      ipbus_in  => ipbw(54),
      ipbus_out => ipbr(54),
      d         => rateCounter_reg
      );

----------------------------------------------------------------

  slave60_RateCounterOv : entity work.ipbus_slave_status
    generic map(addr_width => 5)
    port map(
      clk       => sysclk40,
      reset     => ipb_rst,
      ipbus_in  => ipbw(60),
      ipbus_out => ipbr(60),
      d         => rateCounterOv_reg
      );


----------------------------------------------------------------


  slave55_ctp_odelay_control : entity work.ipbus_slave_control
    generic map(addr_width => 0)
    port map(
      clk       => sysclk40,
      reset     => ipb_rst,
      ipbus_in  => ipbw(55),
      ipbus_out => ipbr(55),
      q         => ctp_odelay_control_32bit
      );

  ctp_odelay_control <= ctp_odelay_control_32bit(5 downto 0);


----------------------------------------------------------------


  slave56_ctp_odelay_pulse : entity work.ipbus_slave_reg_pulse
    generic map(addr_width => 0)
    port map(
      clk       => sysclk40,
      reset     => ipb_rst,
      ipbus_in  => ipbw(56),
      ipbus_out => ipbr(56),
      q         => ctp_odelay_pulse_32bit
      );

  ctp_odelay_pulse <= ctp_odelay_pulse_32bit(1 downto 0);


----------------------------------------------------------------

  CTP_ODELAY_STATUS_GEN : for l in 15 downto 0 generate
    ctp_odelay_status_512bit(l*32+4 downto l*32) <= ctp_odelay_status(l);
  end generate;


  slave57_ctp_odelay_status : entity work.ipbus_slave_status
    generic map(addr_width => 4)
    port map(
      clk       => sysclk40,
      reset     => ipb_rst,
      ipbus_in  => ipbw(57),
      ipbus_out => ipbr(57),
      d         => ctp_odelay_status_512bit
      );


----------------------------------------------------------------


  slave58_clk_control : entity work.ipbus_slave_control
    generic map(addr_width => 0)
    port map(
      clk       => sysclk40,
      reset     => ipb_rst,
      ipbus_in  => ipbw(58),
      ipbus_out => ipbr(58),
      q         => clk_control_32bit
      );

  clk_control <= clk_control_32bit(0);


----------------------------------------------------------------


  clk_status_32bit(3 downto 0) <= clk_status;

  slave59_clk_status : entity work.ipbus_slave_status
    generic map(addr_width => 0)
    port map(
      clk       => sysclk40,
      reset     => ipb_rst,
      ipbus_in  => ipbw(59),
      ipbus_out => ipbr(59),
      d         => clk_status_32bit
      );

----------------------------------------------------------------

-----------------------------------------------------------------------------
-- Histogramming
-----------------------------------------------------------------------------

  gen_histogramming : if PROCESSOR_NUMBER = 1 and DISABLE_HISTOGRAMS = 0 generate

    ClockBus(0) <= sysclk40;
    ClockBus(1) <= sysclk80;
    ClockBus(2) <= sysclk160;

    slave61_hist_timestamp : entity work.ipbus_slave_status
      generic map(addr_width => 0)
      port map(
        clk       => sysclk40,
        reset     => ipb_rst,
        ipbus_in  => ipbw(61),
        ipbus_out => ipbr(61),
        d         => HistTimestamp
        );

    slave62_ECRoverflow : entity work.ipbus_slave_status
      generic map(addr_width => 0)
      port map(
        clk       => sysclk40,
        reset     => ipb_rst,
        ipbus_in  => ipbw(62),
        ipbus_out => ipbr(62),
        d         => ECRoverflow
        );

    slave63_histStatus : entity work.ipbus_slave_status
      generic map(addr_width => 0)
      port map(
        clk       => sysclk40,
        reset     => ipb_rst,
        ipbus_in  => ipbw(63),
        ipbus_out => ipbr(63),
        d         => histStatus
        );

    slave64_histogramNumber : entity work.ipbus_slave_status
      generic map(addr_width => 0)
      port map(
        clk       => sysclk40,
        reset     => ipb_rst,
        ipbus_in  => ipbw(64),
        ipbus_out => ipbr(64),
        d         => std_logic_vector(HistogramNumber)
        );

    slave65_HistControl :  entity work.ipbus_slave_control
      generic map(addr_width => 0)
      port map(
        clk       => sysclk40,
        reset     => HistControlReset,
        ipbus_in  => ipbw(65),
        ipbus_out => ipbr(65),
        q         => HistControl
        );

    slave66_pulseHist : entity work.ipbus_slave_reg_pulse
      generic map(addr_width => 0)
      port map(
        clk       => sysclk40,
        reset     => ipb_rst,
        ipbus_in  => ipbw(66),
        ipbus_out => ipbr(66),
        q         => HistogramPulse
        );

    slave67_HistResetCtr : entity work.ipbus_slave_status
      generic map(addr_width => 0)
      port map(
        clk       => sysclk40,
        reset     => ipb_rst,
        ipbus_in  => ipbw(67),
        ipbus_out => ipbr(67),
        d         => HistResetCtr
        );

    slave68_NumLhcTurns :  entity work.ipbus_slave_control
        generic map(addr_width => 0)
        port map(
          clk       => sysclk40,
          reset     => ipb_rst,
          ipbus_in  => ipbw(68),
          ipbus_out => ipbr(68),
          q         => NumLhcTurns
          );

    slave69_HistControl2 :  entity work.ipbus_slave_control
      generic map(addr_width => 0)
      port map(
        clk       => sysclk40,
        reset     => ipb_rst,
        ipbus_in  => ipbw(69),
        ipbus_out => ipbr(69),
        q         => HistControl2
        );


    slave70_BCIDveto : entity work.ipbus_slave_status
      generic map(addr_width => 7)
      port map(
        clk       => sysclk40,
        reset     => ipb_rst,
        ipbus_in  => ipbw(70),
        ipbus_out => ipbr(70),
        d         => BcidVeto
        );

    slave71_dijetHistograms : entity work.HistogrammingTop
      port map(
        ClockBus          => ClockBus,
        JetTobArray       => JetTOBs,
        BcidVeto          => BcidVeto(3584 - 1 downto 0),
        Met               => Met,
        HistControl       => HistControl,
        HistControl2       => HistControl2,
        EventCounterReset => HistogramPulse(2) or EventCounterReset,
        L1Accept          => HistogramPulse(1) or L1Accept,
        ResetCounters     => HistogramPulse(0),
        BunchCounterReset => HistogramPulse(3), -- TODO: add BunchCounterReset
        DoBcidSync        => HistogramPulse(4),
        NumLhcTurns       => NumLhcTurns,
        BCIDin            => BCID,
        ipbus_in          => ipbw(71),
        ipbus_out         => ipbr(71),
        Timestamp         => HistTimestamp,
        ECRoverflow       => ECRoverflow,
        HistResetCtrOut   => HistResetCtr,
--        HistogramReady    => histStatus(0),
        PrevHistRead      => histStatus(0),
        ControlReset      => HistControlReset,
        HistogramNumber   => HistogramNumber
        );

  end generate;

end rtl;
