library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.l1topo_package.all;
use work.L1TopoDataTypes.all;
use work.L1TopoLUTvalues.all;
use work.muon_types.all;

Package L1TopoFunctions is

    --ctp lines
    function ctpOutputLinesOffset(processorNumber: natural) return natural;

    --fiber mapping
    function quad(q: natural) return natural;
	function channel(ch: natural) return natural;
	function em_channel(processorNumber: natural; em_ch: natural) return natural;
	function tau_channel(processorNumber: natural; tau_ch: natural) return natural;
	function jet_channel(jet_ch: natural) return natural;
	function sumEt_channel(sumEt_ch: natural) return natural;	
	function muon_channel(muon_ch: natural) return natural;
	constant muonCRC_channel: natural := 77;
	
	--tob decoding
    function decodeJetWord(arg: std_logic_vector(127 downto 0); crateNum: std_logic) return JetWord;
    function decodeClusterWord(arg: std_logic_vector(127 downto 0); crateNum: std_logic_vector(1 downto 0)) return ClusterWord;
    function decodeJetTOB(arg: std_logic_vector(27 downto 0); JEPCrate: std_logic) return JetTOB;
    function decodeSumEtTOB(arg: std_logic_vector(127 downto 0)) return SumEtTOB;
    function decodeClusterTOB(arg: std_logic_vector(22 downto 0); CPCrate: std_logic_vector(1 downto 0)) return ClusterTOB;
    function decodeMuonTOB(pt: std_logic_vector(7 downto 0); eta: std_logic_vector(7 downto 0); phi: std_logic_vector(7 downto 0); isNoCandidate : std_logic) return MuonTOB;
--    function decodeMuonTOB(arg: std_logic_vector(7 downto 0); MuonChannel: std_logic; MuonTOBIndex: std_logic_vector(3 downto 0)) return MuonTOB;
    function remapPresortedJetTOBs(arg: JetArray) return JetArray;
    function remapPresortedClusterTOBs(arg: ClusterArray) return ClusterArray;    
    function calculateMissingEt(sumEtTOB_0: SumEtTOB; sumEtTOB_1: SumEtTOB) return MetTOB;
    
--	function ld(m : positive) return natural;        --moved to l1topo_package
	function sqrt(d : UNSIGNED) return UNSIGNED;
	function reg_ctrl(ctrl_reg_flags : std_logic_vector) return integer;
	function find_msb_1(input : std_logic_vector) return integer;
	function calc_crc(data : std_logic_vector(115 downto 0)) return std_logic_vector;
--	function Jet_TOB_to_std_logic_vector(arg : JetTOB) return std_logic_vector;
--	function std_logic_vector_to_Jet_TOB(arg : std_logic_vector(27 downto 0)) return JetTOB;
	function std_logic_vector_to_TOB(arg : std_logic_vector(22 downto 0); phi_bit4 : std_logic; phi_bit5 : std_logic) return ClusterTOB;
	function std_logic_vector_to_TOB(arg : std_logic_vector(21 downto 0)) return GenericTOB;
	function std_logic_vector_to_TOB(arg : std_logic_vector(27 downto 0); phi_bit1 : std_logic) return JetTOB;
	function std_logic_vector_to_TOB(arg : std_logic_vector(7 downto 0)) return MuonTOB;
--	function TOB_to_std_logic_vector(arg : ClusterTOB) return std_logic_vector;
--	function TOB_to_std_logic_vector(arg : JetTOB) return std_logic_vector;
--	function TOB_to_std_logic_vector(arg : GenericTOB) return std_logic_vector;
--	function TOB_to_std_logic_vector(arg : MuonTOB) return std_logic_vector;
	function to_GenericTOB(arg : ClusterTOB) return GenericTOB;
	function to_GenericTOB(arg : JetTOB; jet_size : integer) return GenericTOB;
	function to_GenericTOB(arg : MuonTOB) return GenericTOB;
	function to_GenericTOB(arg : MuonTOB; EtLUT: arraySLVgenericEt(MuonPtBins - 1 downto 0); EtaLUT: arraySLVgenericEta(MuonEtaBins - 1 downto 0); PhiLUT: arraySLVgenericPhi(MuonPhiBins - 1 downto 0)) return GenericTOB;
	function to_GenericTOB(arg : MetTOB; phi : std_logic_vector(GenericPhiBitWidth - 1 downto 0)) return GenericTOB;
	function is_in_eta_range_tob(Tob : GenericTOB;
		                      MinEta : std_logic_vector;
		                      MaxEta : std_logic_vector) return boolean;
	function getMaxCount(InputWidth : integer;
						 MaxTob : integer) return integer;
	function GenericTOB_toBitArray(input : GenericTOB) return std_logic_vector;
--	function to_128bit_array(input : std_logic_vector) return BitArray128;
	function count_ones_twoBits(vectorWithOnes : std_logic_vector) return natural;
	function count_ones(vectorWithOnes : std_logic_vector) return natural;
	function TobArray_to128BitArray5(input : TobArray(4 downto 0)) return std_logic_vector;
	function TobArray_to128BitArray4(input : TobArray(3 downto 0)) return std_logic_vector;
	function JetArray_to128BitArray4(input : JetArray(3 downto 0)) return std_logic_vector;
	function TobArray_to128BitArray3(input : TobArray(2 downto 0)) return std_logic_vector;
	function TobArray_to128BitArray2(input : TobArray(1 downto 0)) return std_logic_vector;
	function TobArray_to128BitArray1(input : TobArray(0 downto 0)) return std_logic_vector;
	function Maximum(OutputWidth: integer; InputWidthPerBlock: integer) return integer;
--	function JetTOBtoIlaSpy(input : JetArray(3 downto 0)) return std_logic_vector;
--  	function ClusterTOBtoIlaSpy(input : ClusterArray(3 downto 0)) return std_logic_vector;
	function GenericTOBtoVector(inTob : GenericTOB) return std_logic_vector;
	function MuonTOBtoVector(inTob : MuonTOB) return std_logic_vector;
	function alignToLeftStdLogicVector(target : std_logic_vector; input : std_logic_vector; cutPos : integer) return std_logic_vector;
	function alignToLeftTobArray(target : TobArray; input : TobArray; cutPos : integer) return TobArray;
	function alignToLeftMuonArray(target : MuonArray; input : MuonArray; cutPos : integer) return MuonArray;


end package L1TopoFunctions;

Package body L1TopoFunctions is


    --ctp lines
    function ctpOutputLinesOffset(processorNumber: natural) return natural is
    begin
        if processorNumber=0 or processorNumber=1 or processorNumber=3 then return 2;
        else return 0;
        end if; 
    end function ctpOutputLinesOffset;
    
     

    --fiber mapping

    function quad(q: natural) return natural is
    begin
        if(q<9) then return q;
        elsif(q<17) then return (q+1);
        else return 19;
        end if;
    end function quad;
    
    
    function channel(ch: natural) return natural is
    begin
        if(ch<36) then return ch;
--        elsif(ch<68) then return (ch+4);
        elsif(ch<49) then return (ch+4); --n
        elsif(ch=49) then return (54); --n
        elsif(ch<66) then return (ch+6); --n
        elsif(ch=66) then return (76);--n
        elsif(ch=67) then return (79);--n
        elsif(ch=68) then return (77);--n delayed_muons
        elsif(ch=79) then return (78);--n delayed_muons
                          
--        else return (ch+8);
        else return (ch+12);
        end if;
    end function channel;
    
    
    function em_channel(processorNumber: natural; em_ch: natural) return natural is
        variable offset: natural;
    begin
        if processorNumber=2 or processorNumber=4 then offset := 0;
        else offset := 6;
        end if;
                                                                --                U1 (offset=6)           ||  U2 (offset=0)
        if   (em_ch< 6) then return channel(em_ch+offset   );   --em_ch =  0- 5: channel( 6-11) =  6-11   ||  channel( 0- 5) =  0- 5
        elsif(em_ch<12) then return channel(em_ch+offset+ 6);   --em_ch =  6-11: channel(18-23) = 18-23   ||  channel(12-17) = 12-17
        elsif(em_ch<18) then return channel(em_ch+offset+12);   --em_ch = 12-17: channel(30-35) = 30-35   ||  channel(24-29) = 24-29
        else                 return channel(em_ch+offset+18);   --em_ch = 18-23: channel(42-47) = 46-51   ||  channel(36-41) = 40-45
        end if;
    end function em_channel;
    
    
    function tau_channel(processorNumber: natural; tau_ch: natural) return natural is
        variable offset: natural;
    begin
        if processorNumber=2 or processorNumber=4 then offset := 6;
        else offset := 0;
        end if;
                                                                    --                U1 (offset=0)            ||  U2 (offset=6)
        if   (tau_ch<6)  then return channel(tau_ch+offset   );     --tau_ch =  0- 5: channel( 0- 5) =  0- 5   ||  channel( 6-11) =  6-11
        elsif(tau_ch<12) then return channel(tau_ch+offset+ 6);     --tau_ch =  6-11: channel(12-17) = 12-17   ||  channel(12-23) = 18-23
        elsif(tau_ch<18) then return channel(tau_ch+offset+12);     --tau_ch = 12-17: channel(24-29) = 24-29   ||  channel(30-35) = 30-35
        else                  return channel(tau_ch+offset+18);     --tau_ch = 18-23: channel(36-41) = 40-45   ||  channel(42-47) = 46-51
        end if;
    end function tau_channel;
    
    
    function jet_channel(jet_ch: natural) return natural is
    begin
        return jet_ch+56;                                    --jet_ch  0-15,    channel 56-71
    end function jet_channel;
    
    
    function sumEt_channel(sumEt_ch: natural) return natural is
    begin
        if(sumEt_ch=0) then return 52;
        else return 54;
        end if;
    end function sumEt_channel;
    
    
    function muon_channel(muon_ch: natural) return natural is
    begin
        if(muon_ch=0) then return 79;
        else return 76;
        end if;
    end function muon_channel;





    -- Functions for converting std_logic_vector to TOB

    function decodeJetWord(arg: std_logic_vector(127 downto 0); crateNum: std_logic) return JetWord is
        variable result: JetWord;
    begin
        result.tobs(0) := decodeJetTOB(arg(127 downto 100), crateNum);
        result.tobs(1) := decodeJetTOB(arg( 99 downto  72), crateNum);
        result.tobs(2) := decodeJetTOB(arg( 71 downto  44), crateNum);
        result.tobs(3) := decodeJetTOB(arg( 43 downto  16), crateNum);
        result.overflow := arg(15);
        return result;
    end function decodeJetWord;


	
	function decodeClusterWord(arg: std_logic_vector(127 downto 0); crateNum: std_logic_vector(1 downto 0)) return ClusterWord is
	   variable result: ClusterWord;
	begin
	   result.tobs(0) := decodeClusterTOB(arg(127 downto 105), crateNum);
	   result.tobs(1) := decodeClusterTOB(arg(104 downto  82), crateNum);
	   result.tobs(2) := decodeClusterTOB(arg( 81 downto  59), crateNum);
	   result.tobs(3) := decodeClusterTOB(arg( 58 downto  36), crateNum);
	   result.tobs(4) := decodeClusterTOB(arg( 35 downto  13), crateNum);
	   result.overflow := arg(12);
	   return result;
	end function decodeClusterWord;


    function decodeJetTOB(arg: std_logic_vector(27 downto 0); JEPCrate: std_logic) return JetTOB is 
    variable result: JetTOB;
    begin
        result.Et1 := arg(18 downto 10);
        result.Et2 := arg(9 downto 0);
        result.Eta := arg(26 downto 24) & arg(23) & arg(19);
        result.Phi := arg(27) & JEPCrate & arg(22 downto 21) & arg(20);
        return result;
    end decodeJetTOB;
    
    
    function decodeSumEtTOB(arg: std_logic_vector(127 downto 0)) return SumEtTOB is
        variable result: SumEtTOB;
    begin
        result.rEt  := '0' & arg(126 downto 112);
        result.Et   := '0' & arg(110 downto  96);
        result.rEy  := arg(94) & arg( 94 downto  80);
        result.Ey   := arg(78) & arg( 78 downto  72) & arg( 51 downto 44);
        result.rEx  := arg(42) & arg( 42 downto  28);
        result.Ex   := arg(26) & arg( 26 downto  12);
        result.rEt_Overflow := arg(127);
        result.Et_Overflow  := arg(111);
        result.rEy_Overflow := arg(95);
        result.Ey_Overflow  := arg(79);
        result.rEx_Overflow := arg(43);
        result.Ex_Overflow  := arg(27);
        return result;
    end function decodeSumEtTOB;
    
    
    function decodeClusterTOB(arg: std_logic_vector(22 downto 0); CPCrate: std_logic_vector(1 downto 0)) return ClusterTOB is 
        variable result: ClusterTOB;
    begin
        result.Et := arg(7 downto 0);
        result.Isol := arg(12 downto 8);
        result.Eta := arg(22 downto 19) & arg(15) & arg(13);
        result.Phi := CPCrate & arg(18 downto 16) & arg(14);
        return result;
    end decodeClusterTOB;

    function decodeMuonTOB(pt: std_logic_vector(7 downto 0); eta: std_logic_vector(7 downto 0); phi: std_logic_vector(7 downto 0); isNoCandidate : std_logic) return MuonTOB is 
        variable result: MuonTOB;
    begin
        result.Pt   := pt(1 downto 0);
        result.Eta  := eta(5 downto 0);
        result.Phi  := phi(5 downto 0);
        result.Overflow := '0';
        result.no_candidate := isNoCandidate;
        return result;
    end decodeMuonTOB;
	
--    function decodeMuonTOB(arg: std_logic_vector(7 downto 0); MuonChannel: std_logic; MuonTOBIndex: std_logic_vector(3 downto 0)) return MuonTOB is 
--        variable result: MuonTOB;
--    begin
--        result.Pt   := arg(1 downto 0);
--        result.Eta  := MuonChannel & arg(4 downto 2);
--        result.Phi  := MuonTOBIndex & arg(7 downto 5);
--        return result;
--    end decodeMuonTOB;



    function remapPresortedJetTOBs(arg: JetArray) return JetArray is
        variable result: JetArray(arg'RANGE);
    begin
        for crate in numberOfJetCrates-1 downto 0 loop
            for ch in (numberOfChannelsPerJetCrate/2)-1 downto 0 loop
                for tob in numberOfTOBsPerJetChannel-1 downto 0 loop
                
                    result(    crate*numberOfChannelsPerJetCrate*numberOfTOBsPerJetChannel + tob*numberOfChannelsPerJetCrate + (numberOfChannelsPerJetCrate/2)-1-ch) 
                        := arg(crate*numberOfChannelsPerJetCrate*numberOfTOBsPerJetChannel + ch*(numberOfChannelsPerJetCrate/2) + tob);
                        
                    result(    crate*numberOfChannelsPerJetCrate*numberOfTOBsPerJetChannel + tob*numberOfChannelsPerJetCrate + numberOfChannelsPerJetCrate-1-ch)
                        := arg(crate*numberOfChannelsPerJetCrate*numberOfTOBsPerJetChannel + (ch+4)*(numberOfChannelsPerJetCrate/2) + tob);

                end loop;
            end loop;
        end loop;
        return result;
    end remapPresortedJetTOBs;



    function remapPresortedClusterTOBs(arg: ClusterArray) return ClusterArray is
        variable result: ClusterArray(arg'RANGE);
    begin
        for crate in numberOfClusterCrates-1 downto 0 loop
            for ch in numberOfChannelsPerClusterCrate-1 downto 0 loop
                for tob in numberOfTOBsPerClusterChannel-1 downto 0 loop
                    
                    result(    crate*numberOfChannelsPerClusterCrate*numberOfTOBsPerClusterChannel + tob*numberOfChannelsPerClusterCrate + numberOfTOBsPerClusterChannel-ch)
                        := arg(crate*numberOfChannelsPerClusterCrate*numberOfTOBsPerClusterChannel +  ch*numberOfTOBsPerClusterChannel   + tob);
                        
                end loop;
            end loop;
        end loop;
        return result;
    end remapPresortedClusterTOBs;



    function calculateMissingEt(sumEtTOB_0: SumEtTOB; sumEtTOB_1: SumEtTOB) return MetTOB is
    	variable result: MetTOB;
    begin
        result.Et   := std_logic_vector(unsigned(sumEtTOB_0.Et) + unsigned(sumEtTOB_1.Et));
        result.Ex   := std_logic_vector((-signed(sumEtTOB_0.Ex) + signed(sumEtTOB_1.Ex)));
        result.Ey   := std_logic_vector((-signed(sumEtTOB_0.Ey) - signed(sumEtTOB_1.Ey)));
        result.Overflow := sumEtTOB_0.Ex_Overflow or sumEtTOB_0.Ey_Overflow or sumEtTOB_1.Ex_Overflow or sumEtTOB_1.Ey_Overflow;
--        result.Overflow := sumEtTOB_0.Et_Overflow or sumEtTOB_0.rEt_Overflow or sumEtTOB_0.Ex_Overflow or sumEtTOB_0.rEx_Overflow or sumEtTOB_0.Ey_Overflow or sumEtTOB_0.rEy_Overflow;
        return result;
    end calculateMissingEt;




--	-- log_2 function
--	function ld(m : positive) return natural is      --moved to l1topo_package
--	begin
--		for n in 0 to integer'high loop
--			if (2 ** n >= m) then
--				return n;
--			end if;
--		end loop;
--	end function ld;

	--sqrt function
	function sqrt(d : UNSIGNED) return UNSIGNED is
		variable a              : unsigned(31 downto 0) := d; --original input.
		variable q              : unsigned(15 downto 0) := (others => '0'); --result.
		variable left, right, r : unsigned(17 downto 0) := (others => '0'); --input to adder/sub.r-remainder.
	begin
		for i in 0 to 15 loop
			right(0)           := '1';
			right(1)           := r(17);
			right(17 downto 2) := q;
			left(1 downto 0)   := a(31 downto 30);
			left(17 downto 2)  := r(15 downto 0);
			a(31 downto 2)     := a(29 downto 0); --shifting by 2 bit.
			if (r(17) = '1') then
				r := left + right;
			else
				r := left - right;
			end if;
			q(15 downto 1) := q(14 downto 0);
			q(0)           := not r(17);
		end loop;
		return q;
	end sqrt;

	function reg_ctrl(ctrl_reg_flags : std_logic_vector) return integer is
		variable ctrl_flags : std_logic_vector(ctrl_reg_flags'length - 1 downto 0);
		variable result     : integer;
		variable ctrl_reg_flags_resize : std_logic_vector(9 downto 0);
	begin
		
		ctrl_reg_flags_resize := std_logic_vector(resize(unsigned(ctrl_reg_flags), 10));
		
		case ctrl_reg_flags_resize is
			when "0000000000" => result := 0;
			when "0000000001" => result := 1;
			when "0000000011" => result := 2;
			when "0000000111" => result := 3;
			when "0000001111" => result := 4;
			when "0000011111" => result := 5;
			when "0000111111" => result := 6;
			when "0001111111" => result := 7;
			when "0011111111" => result := 8;
			when "0111111111" => result := 9;
			when "1111111111" => result := 10;
--			when "11111111111" => result := 11;
			when others => result := 0;
		end case;
				
	return result;
--		ctrl_flags := ctrl_reg_flags;
--		result     := 0;
--		for i in 0 to ctrl_reg_flags'length - 1 loop
--			if ctrl_flags(i) = '0' then
--				result := i;
--				return result;
--			end if;
--		end loop;
--		return ctrl_flags'length;
	end;
	
	-- find most significant bit set to "1"
	function find_msb_1(input : std_logic_vector) return integer is
		variable temp       : std_logic_vector(input'length - 1 downto 0);
		variable result     : integer;
	begin
		temp   := input;
		result := 0;
		for i in input'length - 1 downto 0 loop
			if temp(i) = '1' then
				result := i;
				return result;
			end if;
		end loop;
		return 0;
	end;

	-- calculate crc data
	function calc_crc(data : std_logic_vector(115 downto 0)) return std_logic_vector is
		variable d       : std_logic_vector(115 downto 0);
		variable new_crc : std_logic_vector(11 downto 0);
	begin
		d := data;

		new_crc(0) := d(0) xor d(8) xor d(9) xor d(10) xor d(11) xor d(12) xor d(16) xor d(18) xor d(20) xor d(22) xor d(25) xor d(28) xor d(30) xor d(35) xor d(41) xor d(42) xor d(43) xor d(44) xor d(45) xor d(46) xor d(47) xor d(49) xor d(51) xor d(53)
			xor d(54) xor d(55) xor d(58) xor d(59) xor d(61) xor d(62) xor d(64) xor d(65) xor d(66) xor d(67) xor d(69) xor d(71) xor d(72) xor d(78) xor d(79) xor d(80) xor d(81) xor d(84) xor d(86) xor d(88) xor d(91) xor d(92) xor d(96) xor d(100) xor d(
				105) xor d(106) xor d(107) xor d(109) xor d(110) xor d(111) xor d(112) xor d(113) xor d(115);
		new_crc(1) := d(0) xor d(1) xor d(8) xor d(13) xor d(16) xor d(17) xor d(18) xor d(19) xor d(20) xor d(21) xor d(22) xor d(23) xor d(25) xor d(26) xor d(28) xor d(29) xor d(30) xor d(31) xor d(35) xor d(36) xor d(41) xor d(48) xor d(49) xor d(50)
			xor d(51) xor d(52) xor d(53) xor d(56) xor d(58) xor d(60) xor d(61) xor d(63) xor d(64) xor d(68) xor d(69) xor d(70) xor d(71) xor d(73) xor d(78) xor d(82) xor d(84) xor d(85) xor d(86) xor d(87) xor d(88) xor d(89) xor d(91) xor d(93) xor d(
				96) xor d(97) xor d(100) xor d(101) xor d(105) xor d(108) xor d(109) xor d(114) xor d(115);
		new_crc(2) := d(0) xor d(1) xor d(2) xor d(8) xor d(10) xor d(11) xor d(12) xor d(14) xor d(16) xor d(17) xor d(19) xor d(21) xor d(23) xor d(24) xor d(25) xor d(26) xor d(27) xor d(28) xor d(29) xor d(31) xor d(32) xor d(35) xor d(36) xor d(37)
			xor d(41) xor d(43) xor d(44) xor d(45) xor d(46) xor d(47) xor d(50) xor d(52) xor d(55) xor d(57) xor d(58) xor d(66) xor d(67) xor d(70) xor d(74) xor d(78) xor d(80) xor d(81) xor d(83) xor d(84) xor d(85) xor d(87) xor d(89) xor d(90) xor d(
				91) xor d(94) xor d(96) xor d(97) xor d(98) xor d(100) xor d(101) xor d(102) xor d(105) xor d(107) xor d(111) xor d(112) xor d(113);
		new_crc(3) := d(0) xor d(1) xor d(2) xor d(3) xor d(8) xor d(10) xor d(13) xor d(15) xor d(16) xor d(17) xor d(24) xor d(26) xor d(27) xor d(29) xor d(32) xor d(33) xor d(35) xor d(36) xor d(37) xor d(38) xor d(41) xor d(43) xor d(48) xor d(49)
			xor d(54) xor d(55) xor d(56) xor d(61) xor d(62) xor d(64) xor d(65) xor d(66) xor d(68) xor d(69) xor d(72) xor d(75) xor d(78) xor d(80) xor d(82) xor d(85) xor d(90) xor d(95) xor d(96) xor d(97) xor d(98) xor d(99) xor d(100) xor d(101) xor d(
				102) xor d(103) xor d(105) xor d(107) xor d(108) xor d(109) xor d(110) xor d(111) xor d(114) xor d(115);
		new_crc(4) := d(0) xor d(1) xor d(2) xor d(3) xor d(4) xor d(8) xor d(10) xor d(12) xor d(14) xor d(17) xor d(20) xor d(22) xor d(27) xor d(33) xor d(34) xor d(35) xor d(36) xor d(37) xor d(38) xor d(39) xor d(41) xor d(43) xor d(45) xor d(46) xor
			d(47) xor d(50) xor d(51) xor d(53) xor d(54) xor d(56) xor d(57) xor d(58) xor d(59) xor d(61) xor d(63) xor d(64) xor d(70) xor d(71) xor d(72) xor d(73) xor d(76) xor d(78) xor d(80) xor d(83) xor d(84) xor d(88) xor d(92) xor d(97) xor d(98)
			xor d(99) xor d(101) xor d(102) xor d(103) xor d(104) xor d(105) xor d(107) xor d(108) xor d(113);
		new_crc(5) := d(1) xor d(2) xor d(3) xor d(4) xor d(5) xor d(9) xor d(11) xor d(13) xor d(15) xor d(18) xor d(21) xor d(23) xor d(28) xor d(34) xor d(35) xor d(36) xor d(37) xor d(38) xor d(39) xor d(40) xor d(42) xor d(44) xor d(46) xor d(47) xor
			d(48) xor d(51) xor d(52) xor d(54) xor d(55) xor d(57) xor d(58) xor d(59) xor d(60) xor d(62) xor d(64) xor d(65) xor d(71) xor d(72) xor d(73) xor d(74) xor d(77) xor d(79) xor d(81) xor d(84) xor d(85) xor d(89) xor d(93) xor d(98) xor d(99)
			xor d(100) xor d(102) xor d(103) xor d(104) xor d(105) xor d(106) xor d(108) xor d(109) xor d(114);
		new_crc(6) := d(2) xor d(3) xor d(4) xor d(5) xor d(6) xor d(10) xor d(12) xor d(14) xor d(16) xor d(19) xor d(22) xor d(24) xor d(29) xor d(35) xor d(36) xor d(37) xor d(38) xor d(39) xor d(40) xor d(41) xor d(43) xor d(45) xor d(47) xor d(48)
			xor d(49) xor d(52) xor d(53) xor d(55) xor d(56) xor d(58) xor d(59) xor d(60) xor d(61) xor d(63) xor d(65) xor d(66) xor d(72) xor d(73) xor d(74) xor d(75) xor d(78) xor d(80) xor d(82) xor d(85) xor d(86) xor d(90) xor d(94) xor d(99) xor d(
				100) xor d(101) xor d(103) xor d(104) xor d(105) xor d(106) xor d(107) xor d(109) xor d(110) xor d(115);
		new_crc(7) := d(3) xor d(4) xor d(5) xor d(6) xor d(7) xor d(11) xor d(13) xor d(15) xor d(17) xor d(20) xor d(23) xor d(25) xor d(30) xor d(36) xor d(37) xor d(38) xor d(39) xor d(40) xor d(41) xor d(42) xor d(44) xor d(46) xor d(48) xor d(49)
			xor d(50) xor d(53) xor d(54) xor d(56) xor d(57) xor d(59) xor d(60) xor d(61) xor d(62) xor d(64) xor d(66) xor d(67) xor d(73) xor d(74) xor d(75) xor d(76) xor d(79) xor d(81) xor d(83) xor d(86) xor d(87) xor d(91) xor d(95) xor d(100) xor d(
				101) xor d(102) xor d(104) xor d(105) xor d(106) xor d(107) xor d(108) xor d(110) xor d(111);
		new_crc(8) := d(4) xor d(5) xor d(6) xor d(7) xor d(8) xor d(12) xor d(14) xor d(16) xor d(18) xor d(21) xor d(24) xor d(26) xor d(31) xor d(37) xor d(38) xor d(39) xor d(40) xor d(41) xor d(42) xor d(43) xor d(45) xor d(47) xor d(49) xor d(50)
			xor d(51) xor d(54) xor d(55) xor d(57) xor d(58) xor d(60) xor d(61) xor d(62) xor d(63) xor d(65) xor d(67) xor d(68) xor d(74) xor d(75) xor d(76) xor d(77) xor d(80) xor d(82) xor d(84) xor d(87) xor d(88) xor d(92) xor d(96) xor d(101) xor d(
				102) xor d(103) xor d(105) xor d(106) xor d(107) xor d(108) xor d(109) xor d(111) xor d(112);
		new_crc(9) := d(5) xor d(6) xor d(7) xor d(8) xor d(9) xor d(13) xor d(15) xor d(17) xor d(19) xor d(22) xor d(25) xor d(27) xor d(32) xor d(38) xor d(39) xor d(40) xor d(41) xor d(42) xor d(43) xor d(44) xor d(46) xor d(48) xor d(50) xor d(51)
			xor d(52) xor d(55) xor d(56) xor d(58) xor d(59) xor d(61) xor d(62) xor d(63) xor d(64) xor d(66) xor d(68) xor d(69) xor d(75) xor d(76) xor d(77) xor d(78) xor d(81) xor d(83) xor d(85) xor d(88) xor d(89) xor d(93) xor d(97) xor d(102) xor d(
				103) xor d(104) xor d(106) xor d(107) xor d(108) xor d(109) xor d(110) xor d(112) xor d(113);
		new_crc(10) := d(6) xor d(7) xor d(8) xor d(9) xor d(10) xor d(14) xor d(16) xor d(18) xor d(20) xor d(23) xor d(26) xor d(28) xor d(33) xor d(39) xor d(40) xor d(41) xor d(42) xor d(43) xor d(44) xor d(45) xor d(47) xor d(49) xor d(51) xor d(52)
			xor d(53) xor d(56) xor d(57) xor d(59) xor d(60) xor d(62) xor d(63) xor d(64) xor d(65) xor d(67) xor d(69) xor d(70) xor d(76) xor d(77) xor d(78) xor d(79) xor d(82) xor d(84) xor d(86) xor d(89) xor d(90) xor d(94) xor d(98) xor d(103) xor d(
				104) xor d(105) xor d(107) xor d(108) xor d(109) xor d(110) xor d(111) xor d(113) xor d(114);
		new_crc(11) := d(7) xor d(8) xor d(9) xor d(10) xor d(11) xor d(15) xor d(17) xor d(19) xor d(21) xor d(24) xor d(27) xor d(29) xor d(34) xor d(40) xor d(41) xor d(42) xor d(43) xor d(44) xor d(45) xor d(46) xor d(48) xor d(50) xor d(52) xor d(53)
			xor d(54) xor d(57) xor d(58) xor d(60) xor d(61) xor d(63) xor d(64) xor d(65) xor d(66) xor d(68) xor d(70) xor d(71) xor d(77) xor d(78) xor d(79) xor d(80) xor d(83) xor d(85) xor d(87) xor d(90) xor d(91) xor d(95) xor d(99) xor d(104) xor d(
				105) xor d(106) xor d(108) xor d(109) xor d(110) xor d(111) xor d(112) xor d(114) xor d(115);
		return new_crc;
	end calc_crc;

	--------------------------------------------------------
	-- Functions to convert TOB from/to std_logic_vector --
	--------------------------------------------------------
	
--	function Jet_TOB_to_std_logic_vector(arg : JetTOB) return std_logic_vector is
--		variable result : std_logic_vector(28 downto 0);
--	begin
--		result(18 downto 10) := arg.Et1;
--		result(9 downto 0)   := arg.Et2;
--		result(23 downto 19) := arg.Eta;
--		result(28 downto 24) := arg.Phi;
--		return result;
--	end function Jet_TOB_to_std_logic_vector;

--	function std_logic_vector_to_Jet_TOB(arg : std_logic_vector(28 downto 0)) return JetTOB is
--		variable result : JetTOB;
--	begin
--		result.Et1 := arg(18 downto 10);
--		result.Et2 := arg(9 downto 0);
--		result.Eta := arg(23 downto 19);
--		result.Phi := arg(28 downto 24);
--		return result;
--	end function std_logic_vector_to_Jet_TOB;

	function std_logic_vector_to_TOB(arg : std_logic_vector(22 downto 0); phi_bit4 : std_logic; phi_bit5 : std_logic) return ClusterTOB is
		variable result : ClusterTOB;
	begin
		result.Et              := arg(7 downto 0);
		result.Isol            := arg(12 downto 8);
		result.Eta             := arg(18 downto 13);
		result.Phi(3 downto 0) := arg(22 downto 19);
		result.Phi(4)          := phi_bit4; --!
		result.Phi(5)          := phi_bit5; --!
		return result;
	end function std_logic_vector_to_TOB;

	function std_logic_vector_to_TOB(arg : std_logic_vector(21 downto 0)) return GenericTOB is
		variable result : GenericTOB;
	begin
		result.Et  := arg(9 downto 0);
		result.eta := arg(15 downto 10);
		result.phi := arg(21 downto 16);
		return result;
	end function std_logic_vector_to_TOB;

	function std_logic_vector_to_TOB(arg : std_logic_vector(27 downto 0); phi_bit1 : std_logic) return JetTOB is
		variable result : JetTOB;
	begin
		result.Et1 := arg(18 downto 10);
		result.Et2 := arg(9 downto 0);
		result.Eta := arg(23 downto 19);
		result.Phi(0)          := arg(24);
		result.Phi(1)          := phi_bit1; --!
		result.Phi(4 downto 2) := arg(27 downto 25);
		return result;
	end function std_logic_vector_to_TOB;

	function std_logic_vector_to_TOB(arg : std_logic_vector(7 downto 0)) return MuonTOB is
		variable result : MuonTOB;
	begin
		result.Eta := arg(7 downto 5);
		result.Phi := arg(4 downto 2);
		result.Pt  := arg(1 downto 0);
		return result; -- TODO: Check if this is correct!
	end function std_logic_vector_to_TOB;

--	function TOB_to_std_logic_vector(arg : ClusterTOB) return std_logic_vector is
--		variable result : std_logic_vector(24 downto 0);
--	begin
--		result(7 downto 0)   := arg.Et;
--		result(12 downto 8)  := arg.Isol;
--		result(18 downto 13) := arg.Eta;
--		result(24 downto 19) := arg.Phi;
--		return result;
--	end function TOB_to_std_logic_vector;

--	function TOB_to_std_logic_vector(arg : JetTOB) return std_logic_vector is
--		variable result : std_logic_vector(19 downto 0);
--	begin
--		--result(18 downto 10) := arg.Et1;
--		result(9 downto 0)   := arg.Et2;
--		result(14 downto 10) := arg.Eta;
--		result(19 downto 15) := arg.Phi;
--		return result;
--	end function TOB_to_std_logic_vector;

--	function TOB_to_std_logic_vector(arg : GenericTOB) return std_logic_vector is
--		variable result : std_logic_vector(21 downto 0);
--	begin
--		result(9 downto 0)   := arg.Et;
--		result(15 downto 10) := arg.eta;
--		result(21 downto 16) := arg.phi;
--		return result;
--	end function TOB_to_std_logic_vector;
	
--	function TOB_to_std_logic_vector(arg : MuonTOB) return std_logic_vector is
--		variable result : std_logic_vector(7 downto 0);
--	begin
--		result(7 downto 5) := arg.Eta;
--		result(4 downto 2) := arg.Phi;
--		result(1 downto 0) := arg.Pt;
--		return result;
--	end function TOB_to_std_logic_vector;

	---------------------------------------------
	-- Functions to convert TOB to GenericTOB --
	---------------------------------------------
	
	function to_GenericTOB(arg : ClusterTOB) return GenericTOB is
		variable EtaBin : Integer;
		variable result : GenericTOB;
	begin
		result.Et  := "00" & arg.Et;
		EtaBin     := to_integer(unsigned(arg.Eta));
		result.Eta := std_logic_vector(to_signed(ClusterEtaTab(EtaBin),GenericEtaBitWidth));
		result.Phi := std_logic_vector(unsigned(arg.Phi)+1);
		result.Overflow := '0';
		return result;
	end function to_GenericTOB;
	
	function to_GenericTOB(arg : JetTOB; jet_size : integer) return GenericTOB is
		variable EtaBin : Integer;
		variable result : GenericTOB;
	begin
		if jet_size = 1 then
			result.Et := '0' & arg.Et1;
		else
			result.Et := arg.Et2;
		end if;
		EtaBin     := to_integer(unsigned(arg.Eta));
		result.Eta := std_logic_vector(to_signed(JetEtaTab(EtaBin),GenericEtaBitWidth));
		result.Phi := std_logic_vector(unsigned(arg.Phi & '0') + 2);
		result.Overflow := '0';
		return result;
	end function to_GenericTOB;
	
	function to_GenericTOB(arg : MuonTOB) return GenericTOB is
		variable result : GenericTOB;
		variable etaSign : std_logic;
	begin
		if (arg.no_candidate = '0') then
			etaSign := arg.Eta(arg.Eta'length - 1);
			
			result.Et    := std_logic_vector(to_unsigned(MuonPtTab(to_integer(unsigned(arg.Pt))), 10));
	--		result.Et    := "00000000" & arg.Pt;
			result.Eta   := etaSign & arg.Eta;
			result.Phi   := arg.Phi;
			result.Overflow := '0';
		else
			result := empty_tob;
		end if;

		return result;
	end function to_GenericTOB;

        --function to_GenericTOB(arg : MuonTOB; octant: integer) return GenericTOB is
        --  variable result : GenericTOB;
	--begin
        --  result.Et    := std_logic_vector(to_unsigned(MuonPtTab(to_integer(unsigned(arg.Pt))),GenericEtBitWidth));
        --  result.Eta   := std_logic_vector(to_signed(MuonEtaTab(octant)(to_integer(unsigned(arg.Eta)))(to_integer(unsigned(arg.Phi))), GenericEtaBitWidth));
        --  result.Phi   := std_logic_vector(to_signed(MuonPhiTab(octant)(to_integer(unsigned(arg.Eta)))(to_integer(unsigned(arg.Phi))),GenericPhiBitWidth));
        --  return result;
	--end function to_GenericTOB;

	
	function to_GenericTOB(arg : MuonTOB; EtLUT: arraySLVgenericEt(MuonPtBins - 1 downto 0); EtaLUT: arraySLVgenericEta(MuonEtaBins - 1 downto 0); PhiLUT: arraySLVgenericPhi(MuonPhiBins - 1 downto 0)) return GenericTOB is
		variable PtSel:   natural;
		variable EtaSel:  natural;
		variable PhiSel:  natural;
		variable result : GenericTOB;
	begin
	    PtSel        := to_integer(unsigned(arg.Pt));
		result.Et    := EtLUT(PtSel);
		EtaSel       := to_integer(unsigned(arg.Eta));
        result.Eta   := EtaLUT(EtaSel);
        PhiSel       := to_integer(unsigned(arg.Phi));
        result.Phi   := PhiLUT(PhiSel);
		result.Overflow := '0';
		return result;
	end function to_GenericTOB;
	
	function to_GenericTOB(arg : MetTOB; phi : std_logic_vector(GenericPhiBitWidth - 1 downto 0)) return GenericTOB is
		variable METSqr     : std_logic_vector(METSqrEnergyBitWidth - 1 downto 0);
		variable MET        : std_logic_vector(METEnergyBitWidth - 1 downto 0);
		variable saturation : integer;  --std_logic_vector(METEnergyBitWidth - GenericEtBitWidth - 1 downto 0);
		variable result     : GenericTOB;
	begin
		METSqr     := std_logic_vector(unsigned(signed(arg.Ex) * signed(arg.Ex) + signed(arg.Ey) * signed(arg.Ey)));
		MET        := std_logic_vector(sqrt(unsigned(METSqr)));
		-- put MET into Et record entry. If saturated, set all bits of Et to '1'.
		saturation := to_integer(unsigned(MET(METEnergyBitWidth - 1 downto GenericEtBitWidth))); --bits that don't fit in result.Et
		if saturation = 0 then --no saturation
			result.Et := MET(GenericEtBitWidth - 1 downto 0);
		else --saturated -> set all bits to '1' 
			result.Et := (others => '1');
		end if;
--		result.EtSqr := EtSqr;
		result.Eta := (others => '0');
		result.Phi := phi;
		result.Overflow := '0';
		return result;
	end function to_GenericTOB;
	
--	function is_in_eta_range(eta_bin : std_logic_vector;
--		                     min_bin : std_logic_vector;
--		                     max_bin : std_logic_vector) return boolean is
--		-- calculate symmetric bin ranges (only needed for forward regions - they are split: low bins and high bins)
--		variable eta_max_val : unsigned(eta_bin'length -1 downto 0) := to_unsigned(2**(eta_bin'length) - 1, eta_bin'length);
--		variable sym_min_bin : std_logic_vector(eta_bin'length - 1 downto 0) := std_logic_vector(eta_max_val - unsigned(max_bin));
--		variable sym_max_bin : std_logic_vector(eta_bin'length - 1 downto 0) := std_logic_vector(eta_max_val - unsigned(min_bin));
--		variable result : boolean := false;
--	begin
--		if ( ((unsigned(eta_bin) >= unsigned(min_bin)) and      -- >= min bin  5bit (32 bins) examples: (0), (14)
--		      (unsigned(eta_bin) <= unsigned(max_bin))) or      -- <= max bin                           (5), (17)
--		     ((unsigned(eta_bin) >= unsigned(sym_min_bin)) and  -- >= symmetric min bin                (26), (17)
--		   	  (unsigned(eta_bin) <= unsigned(sym_max_bin)))     -- <= symmetric max bin                (31), (14)
--		   ) then
--			result := true;
--		else
--			result := false;
--		end if;
--		return result;
--	end function is_in_eta_range;
	
	function is_in_eta_range_tob(Tob : GenericTOB;
		                      MinEta : std_logic_vector;
		                      MaxEta : std_logic_vector) return boolean is
		variable result : boolean := false;
	begin
		if ( (to_integer(abs(signed(Tob.Eta))) >= to_integer(unsigned(MinEta))) and
			 (to_integer(abs(signed(Tob.Eta))) <= to_integer(unsigned(MaxEta)))
		   ) then
			result := true;
		else
			result := false;
		end if;
		return result;
	end function is_in_eta_range_tob;
	
	function getMaxCount(InputWidth : integer; MaxTob : integer) return integer is
	begin
		if (MaxTob > InputWidth) then
			return InputWidth;
		end if;
		if (MaxTob = 0) then
			return InputWidth;
		else
			return MaxTob;
		end if;
	end function;

	function GenericTOB_toBitArray(input : GenericTOB) return std_logic_vector is
		variable output : std_logic_vector(22 downto 0);
	begin
		output(9 downto 0) := input.Et;
		output(16 downto 10) := input.Eta;
		output(22 downto 17) := input.Phi;
		return output;
	end function GenericTOB_toBitArray;

--	function to_128bit_array(input : std_logic_vector) return BitArray128 is 
--	variable vectorSize : integer := (((input'length-1) / 128) + 1);
--	variable remainingBits : integer := input'length - (vectorSize - 1)*128;
--	variable res_array : BitArray128(vectorSize - 1 downto 0);
--	begin
----		if (not input'ascending) then
--		for i in vectorSize - 2 downto 0 loop
--			res_array(i)(127 downto 0) := input((i+1)*128 - 1 downto i*128);
--		end loop;
--		res_array(vectorSize - 1)(remainingBits - 1 downto 0) := input(input'length - 1 downto (input'length - remainingBits));
--		res_array(vectorSize - 1)(127 downto remainingBits) := (others => '0');
----		end if;
--		return res_array;
--	end function to_128bit_array;
	
	--this function counts ones in a vector

	
	function count_ones_twoBits(vectorWithOnes : std_logic_vector) return natural is
	variable n_ones : natural := 0;
	begin
		for i in vectorWithOnes'range loop
			if vectorWithOnes(i) = '1' and n_ones /= 3 then
				n_ones := n_ones + 1;
			end if;
		end loop;

		return n_ones;
	end function count_ones_twoBits;
	
	function count_ones(vectorWithOnes : std_logic_vector) return natural is
	variable n_ones : natural := 0;
	begin
		for i in vectorWithOnes'range loop
			if vectorWithOnes(i) = '1' then
				n_ones := n_ones + 1;
			end if;
		end loop;

		return n_ones;
	end function count_ones;
	
--	function count_ones(vectorWithOnes : std_logic_vector) return natural is
--	variable n_ones : natural := 0;
--	begin
--		for i in vectorWithOnes'range loop
--			if vectorWithOnes(i) = '1' then
--				n_ones := n_ones + 1;
--			end if;
--		end loop;
--
--		return n_ones;
--	end function count_ones;

	function TobArray_to128BitArray5(input : TobArray(4 downto 0)) return std_logic_vector is
        variable output : std_logic_vector(127 downto 0);
        variable offset : integer := 0;
    begin
        for i in 0 to 5 - 1 loop
            output(offset + 9 downto offset + 0) := input(i).Et;
            output(offset + 16 downto offset + 10) := input(i).Eta;
            output(offset + 22 downto offset + 17) := input(i).Phi;
            output(offset + 23) := input(i).Overflow;
            offset := offset + 24;
        end loop;
        --output(127 downto 120) := (others => '0');
        return output;
    end function TobArray_to128BitArray5;

	function TobArray_to128BitArray4(input : TobArray(3 downto 0)) return std_logic_vector is
        variable output : std_logic_vector(127 downto 0);
        variable offset : integer := 0;
    begin
        for i in 0 to 4 - 1 loop
            output(offset + 9 downto offset + 0) := input(i).Et;
            output(offset + 16 downto offset + 10) := input(i).Eta;
            output(offset + 22 downto offset + 17) := input(i).Phi;
            output(offset + 23) := input(i).Overflow;
            offset := offset + 24;
        end loop;
        --output(127 downto 120) := (others => '0');
        return output;
    end function TobArray_to128BitArray4;
    
	function JetArray_to128BitArray4(input : JetArray(3 downto 0)) return std_logic_vector is
        variable output : std_logic_vector(127 downto 0);
        variable offset : integer := 0;
    begin
        for i in 0 to 4 - 1 loop
            output(offset + 8 downto offset + 0) := input(i).Et1;
            output(offset + 18 downto offset + 9) := input(i).Et2;
            output(offset + 23 downto offset + 19) := input(i).Eta;
            output(offset + 28 downto offset + 24) := input(i).Phi;
            offset := offset + 29;
        end loop;
        output(127 downto 116) := (others => '0');
        return output;
    end function JetArray_to128BitArray4;

	function TobArray_to128BitArray3(input : TobArray(2 downto 0)) return std_logic_vector is
        variable output : std_logic_vector(127 downto 0);
        variable offset : integer := 0;
    begin
        for i in 0 to 3 - 1 loop
            output(offset + 9 downto offset + 0) := input(i).Et;
            output(offset + 16 downto offset + 10) := input(i).Eta;
            output(offset + 22 downto offset + 17) := input(i).Phi;
            output(offset + 23) := input(i).Overflow;
            offset := offset + 24;
        end loop;
        --output(127 downto 120) := (others => '0');
        return output;
    end function TobArray_to128BitArray3;

	function TobArray_to128BitArray2(input : TobArray(1 downto 0)) return std_logic_vector is
        variable output : std_logic_vector(127 downto 0);
        variable offset : integer := 0;
    begin
        for i in 0 to 2 - 1 loop
            output(offset + 9 downto offset + 0) := input(i).Et;
            output(offset + 16 downto offset + 10) := input(i).Eta;
            output(offset + 22 downto offset + 17) := input(i).Phi;
            output(offset + 23) := input(i).Overflow;
            offset := offset + 24;
        end loop;
        --output(127 downto 120) := (others => '0');
        return output;
    end function TobArray_to128BitArray2;
    
	function TobArray_to128BitArray1(input : TobArray(0 downto 0)) return std_logic_vector is
        variable output : std_logic_vector(127 downto 0);
        variable offset : integer := 0;
    begin
        for i in 0 to 1 - 1 loop
            output(offset + 9 downto offset + 0) := input(i).Et;
            output(offset + 16 downto offset + 10) := input(i).Eta;
            output(offset + 22 downto offset + 17) := input(i).Phi;
            output(offset + 23) := input(i).Overflow;
            offset := offset + 24;
        end loop;
        --output(127 downto 120) := (others => '0');
        return output;
    end function TobArray_to128BitArray1;
    
--	function JetTOBtoIlaSpy(input : JetArray(3 downto 0)) return std_logic_vector is
--		variable result : std_logic_vector(127 downto 0);
--		variable tmpGeneric : TOBArray(3 downto 0);
--	begin
--		for i in 3 downto 0 loop
--			tmpGeneric(i) := to_GenericTOB(input(i), 1);
--		end loop;
--		
--		for i in 3 downto 0 loop
--			 result(32*(i+1) - 1 downto 32*i) := "00000000" & tmpGeneric(i).Et & tmpGeneric(i).Eta & tmpGeneric(i).Phi & tmpGeneric(i).Overflow;
--		end loop;
--		
--		return result;
--	end;
--	
--	function ClusterTOBtoIlaSpy(input : ClusterArray(3 downto 0)) return std_logic_vector is
--		variable result : std_logic_vector(127 downto 0);
--		variable tmpGeneric : TOBArray(3 downto 0);
--	begin
--		for i in 3 downto 0 loop
--			tmpGeneric(i) := to_GenericTOB(input(i));
--		end loop;
--		
--		for i in 3 downto 0 loop
--			 result(32*(i+1) - 1 downto 32*i) := "00000000" & tmpGeneric(i).Et & tmpGeneric(i).Eta & tmpGeneric(i).Phi & tmpGeneric(i).Overflow;
--		end loop;
--		
--		return result;
--    end;
    
    function Maximum(OutputWidth: integer; InputWidthPerBlock: integer) return integer is
    	--variable MaximumOfTwo : integer;
    begin
        if OutputWidth > InputWidthPerBlock then
            return OutputWidth;
        
        else
            return InputWidthPerBlock;
        end if;

    end function Maximum;
    
    function GenericTOBtoVector(inTob : GenericTOB) return std_logic_vector is
    begin
    	return inTob.Overflow & inTob.Phi & inTob.Eta & inTob.Et;
    end function GenericTOBtoVector;
    
    -- 16 bits output
    function MuonTOBtoVector(inTob : MuonTOB) return std_logic_vector is
    begin
    	return inTob.no_candidate & inTob.Overflow & inTob.Phi & inTob.Eta & inTob.Pt;
    end function MuonTOBtoVector;
    
    function alignToLeftStdLogicVector(target : std_logic_vector; input : std_logic_vector; cutPos : integer) return std_logic_vector is
    	variable output : std_logic_vector(input'length - 1 downto 0);
    	variable vectorLength : integer;
    begin
    	vectorLength := input'length;
    	output := target;
    	
		case cutPos is
		when 0 =>
			output := input;
		when 1 =>
			output(vectorLength - 1 downto 1) 	:= input(vectorLength - 1 - 1 downto 0);
		when 2 =>
			output(vectorLength - 1 downto 2) 	:= input(vectorLength - 2 - 1 downto 0);
		when 3 =>
			output(vectorLength - 1 downto 3) 	:= input(vectorLength - 3 - 1 downto 0);
		when 4 =>
			output(vectorLength - 1 downto 4) 	:= input(vectorLength - 4 - 1 downto 0);
		when 5 =>
			output(vectorLength - 1 downto 5) 	:= input(vectorLength - 5 - 1 downto 0);
		when 6 =>
			output(vectorLength - 1 downto 6) 	:= input(vectorLength - 6 - 1 downto 0);
		when 7 =>
			output(vectorLength - 1 downto 7) 	:= input(vectorLength - 7 - 1 downto 0);
		when 8 =>
			output(vectorLength - 1 downto 8) 	:= input(vectorLength - 8 - 1 downto 0);
		when 9 =>
			output(vectorLength - 1 downto 9) 	:= input(vectorLength - 9 - 1 downto 0);
		when others =>
			output := target;
		end case;

		return output;
	end function;
	
	function alignToLeftTobArray(target : TobArray; input : TobArray; cutPos : integer) return TobArray is
		variable output : TobArray(input'length - 1 downto 0);
    	variable vectorLength : integer;
    begin
    	vectorLength := input'length;
    	output := target;
    	    	
		case cutPos is
		when 0 =>
			output := input;
		when 1 =>
			output(vectorLength - 1 downto 1) 	:= input(vectorLength - 1 - 1 downto 0);
		when 2 =>
			output(vectorLength - 1 downto 2) 	:= input(vectorLength - 2 - 1 downto 0);
		when 3 =>
			output(vectorLength - 1 downto 3) 	:= input(vectorLength - 3 - 1 downto 0);
		when 4 =>
			output(vectorLength - 1 downto 4) 	:= input(vectorLength - 4 - 1 downto 0);
		when 5 =>
			output(vectorLength - 1 downto 5) 	:= input(vectorLength - 5 - 1 downto 0);
		when 6 =>
			output(vectorLength - 1 downto 6) 	:= input(vectorLength - 6 - 1 downto 0);
		when 7 =>
			output(vectorLength - 1 downto 7) 	:= input(vectorLength - 7 - 1 downto 0);
		when 8 =>
			output(vectorLength - 1 downto 8) 	:= input(vectorLength - 8 - 1 downto 0);
		when 9 =>
			output(vectorLength - 1 downto 9) 	:= input(vectorLength - 9 - 1 downto 0);
		when others =>
			output := target;
		end case;

		return output;
	end function;
	
	function alignToLeftMuonArray(target : MuonArray; input : MuonArray; cutPos : integer) return MuonArray is
		variable output : MuonArray(input'length - 1 downto 0);
    	variable vectorLength : integer;
    begin
    	vectorLength := input'length;
    	output := target;
    	    	
		case cutPos is
		when 0 =>
			output := input;
		when 1 =>
			output(vectorLength - 1 downto 1) 	:= input(vectorLength - 1 - 1 downto 0);
		when 2 =>
			output(vectorLength - 1 downto 2) 	:= input(vectorLength - 2 - 1 downto 0);
		when 3 =>
			output(vectorLength - 1 downto 3) 	:= input(vectorLength - 3 - 1 downto 0);
		when 4 =>
			output(vectorLength - 1 downto 4) 	:= input(vectorLength - 4 - 1 downto 0);
		when 5 =>
			output(vectorLength - 1 downto 5) 	:= input(vectorLength - 5 - 1 downto 0);
		when 6 =>
			output(vectorLength - 1 downto 6) 	:= input(vectorLength - 6 - 1 downto 0);
		when 7 =>
			output(vectorLength - 1 downto 7) 	:= input(vectorLength - 7 - 1 downto 0);
		when 8 =>
			output(vectorLength - 1 downto 8) 	:= input(vectorLength - 8 - 1 downto 0);
		when 9 =>
			output(vectorLength - 1 downto 9) 	:= input(vectorLength - 9 - 1 downto 0);
		when others =>
			output := target;
		end case;

		return output;
	end function;
	
end package body L1TopoFunctions;
