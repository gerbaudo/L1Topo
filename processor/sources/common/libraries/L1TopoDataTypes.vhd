library ieee;
use ieee.std_logic_1164.all;

Package L1TopoDataTypes is

    -- constants for the realtime data path
    
    constant numberOfQuads:                     natural := 20;
    constant numberOfChannels:                  natural := 80;
    constant numberOfEMChannels:                natural := 24;
    constant numberOfTauChannels:               natural := 24;
    constant numberOfJetChannels:               natural := 16;
    constant numberOfSumEtChannels:             natural := 2;
    constant numberOfMuonChannels:              natural := 2;
    
    constant numberOfL1CaloChannels:            natural := numberOfEMChannels + numberOfTauChannels + numberOfJetChannels + numberOfSumEtChannels + numberOfMuonChannels; --  68


    constant numberOfClusterCrates:             natural := 4;
    constant numberOfJetCrates:                 natural := 2;
    constant numberOfChannelsPerClusterCrate:   natural := 6;
    constant numberOfChannelsPerJetCrate:       natural := 8;
    constant numberOfTOBsPerClusterChannel:     natural := 5;
    constant numberOfTOBsPerJetChannel:         natural := 4;
    

	-- generics for the algorithm modules

	constant InputWidthEM                : integer := 120;--120;
	constant InputwidthTAU               : integer := 120;--120;
	constant InputWidthJET               : integer := 64; -- CMX: neuer stand nur 48 ??
	constant InputWidthMU                : integer := 32;
	
	constant InputWidth1stStageSortEM    : integer := 20;--20;
	constant InputWidth1stStageSelectEM  : integer := 30; --previous value: 15
	constant InputWidth1stStageSortTAU   : integer := 20;--20;
	constant InputWidth1stStageSelectTAU : integer := 30; --previous value: 15
	constant InputWidth1stStageSortJET   : integer := 16;
	constant InputWidth1stStageSelectJET : integer := 16;
	constant InputWidth1stStageSortMU    : integer := 16;
	constant InputWidth1stStageSelectMU  : integer := 16;
	
	constant OutputWidthSortEM           : integer := 6; --6
	constant OutputWidthSelectEM         : integer := 10; --10
	constant OutputWidthSortTAU          : integer := 6; --6
	constant OutputWidthSelectTAU        : integer := 10; --10
	constant OutputWidthSortJET          : integer := 6; --6
	constant OutputWidthSelectJET        : integer := 10; --10
	constant OutputWidthSortMU           : integer := 6; --6
	constant NumberOfDelayedMuons        : integer := 1; -- to save timing resources: output only leading delayed muon
	constant OutputWidthSelectMU         : integer := 10; --8 or 9
	
	constant NumberOfAlgorithms          : integer := 120; -- maximum decision algoId from menu XML
	constant NumberOfSortAlgorithms      : integer := 35; -- maximum sort type algoId from menu XML

	constant NumberOfResultBits          : integer := 32;  --ratio of results to overflow is adjustable (needs some more thoughts, which overflows are useful)
--	constant NumberOfOverflowBits        : integer := 31;  -- = NumberOfResultBits !!!
	constant NumberOfOutputBits          : integer := 32;  --there are only 32 lines from a L1Topo-Module to CTP => 16 lines per processor
	
	constant NumberOfMGTs_485            : integer := 56;
	constant NumberOfMGTs_690            : integer := 80;
	constant NumberOfQuads_485           : integer := 14;
	constant NumberOfQuads_690           : integer := 20;
	
	constant KalmanMETCorrection_numberOfEtaBins    : integer := 16;  -- needs to be multiple of 2
	constant KalmanMETCorrection_numberOfEtBins     : integer := 10; -- needs to be multiple of 2
	constant KalmanMETCorrection_correctionBitWidth : integer := 9;  -- assuming format signed [+0.0000000]
	
	constant InputWidthMET               : integer := 1;   -- The MET values Ex, Ey in one MetTOB
	constant OutputWidthMET              : integer := 1;   -- MET (and MET_corrected? TODO: Set to "2"!)
	constant METEnergyBitWidth           : integer := 16;  --
	constant METSqrEnergyBitWidth        : integer := 2 * METEnergyBitWidth;
	constant Arctan2InputEnergyBitWidth  : integer := 15;  -- Fixed! Do not change!
	
	constant CosSinBitWidth              : integer := 9;
	constant CoshBitWidth                : integer := 11 + 7;
	constant SechTanhBitWidth            : integer := 8;
	
	constant DefaultJetSize              : integer := 2;  --jet Et bit width (1 = 9 bit, 2 = 10 bit)
	
	constant ClusterEtBitWidth           : integer := 8;
	constant ClusterIsolBitWidth         : integer := 5;
	constant ClusterEtaBitWidth          : integer := 6;
	constant ClusterPhiBitWidth          : integer := 6;
	
	constant JetEt1BitWidth              : integer := 9;
	constant JetEt2BitWidth              : integer := 10;
	constant JetEtaBitWidth              : integer := 5;
	constant JetPhiBitWidth              : integer := 5;
	
    constant SumEt_Et_bitWidth           : natural := 16;
    constant SumEt_rEt_bitWidth          : natural := 16;
    constant SumEt_Ex_bitWidth           : natural := 16;
    constant SumEt_rEx_bitWidth          : natural := 16;
    constant SumEt_Ey_bitWidth           : natural := 16;
    constant SumEt_rEy_bitWidth          : natural := 16;
	
	constant MuonPtBitWidth              : integer := 2; --2;
	constant MuonEtaBitWidth             : integer := 6; --3;
	constant MuonPhiBitWidth             : integer := 6; --3;
	
	constant MuonPtBins                  : integer := 2 ** MuonPtBitWidth; --                  --> 4
	constant MuonEtaBins                 : integer := 2 * (2 ** MuonEtaBitWidth); -- 2 sides   --> 16
	constant MuonPhiBins                 : integer := 8 * (2 ** MuonPhiBitWidth); -- 8 octants --> 64
	
	constant GenericEtBitWidth           : integer := 10;
--	constant GenericEtSqrBitWidth        : integer := 2 * GenericEtBitWidth;
	constant GenericEtaBitWidth          : integer := 7; -- LSB: 0.1,	2's complement,	range: [-64;+63] or as deltaEta: [0;127]
	constant GenericAbsoluteEtaBitWidth  : integer := GenericEtaBitWidth - 1; -- LSB: 0.1, unsigned, range: [0;+63]
	constant GenericPhiBitWidth          : integer := 6; -- LSB: pi/32,	unsigned,		range: [0;2*pi)
	constant GenericRSqrBitWidth         : integer := 2*GenericEtaBitWidth+1; --! GenericEtaBitWidth needs to be >= GenericPhiBitWidth
	
	-- compound constants
	
	constant MassSqrBitWidth             : integer := GenericEtBitWidth +  METEnergyBitWidth + 1 + CosSinBitWidth + 1;
	constant RatioBitWidth				 : integer := 8; -- unsigned 1 decimal digit shift
	constant InvariantMassSqrBitWidth	 : integer := 2 * GenericEtBitWidth + CoshBitWidth + 1 - 7;
	constant TransverseMassSqrBitWidth	 : integer := 2 * GenericEtBitWidth + CosSinBitWidth + 2 - 7;
	constant ContratransverseMassSqrBitWidth	 : integer := TransverseMassSqrBitWidth;
	constant MultiplicityCustomBitWidth : integer := 7;
	constant JetHtBitWidth : integer := 14;				  -- bitwith until saturation
--	constant JetHTMaxSumParameterBitwith : integer := 16; -- maximum sum value; log2(64 jets * 1024 maxEt)
	
	constant KalmanMETBitWidth : integer := 14;

	-- Maximum number of components that require parameters

	constant max_components : integer := 70; --TODO: unused!
	constant max_jets       : integer := 48; --TODO: unused!
	constant max_clusters   : integer := 80; --TODO: unused!

	-- Max number of TOBs to be analysed by inclusive algorithms:

	constant max_incl_jets     : integer := 8; --TODO: unused!
	constant max_incl_clusters : integer := 8; --TODO: unused!

        constant active_channels   : std_logic_vector(80 downto 0) :=
          (80 => '1', 79 => '1', 76 => '1', 71 downto 56 => '1', 54 => '1', 52 downto 40 => '1', 35 downto 0 => '1', others => '0');
          
	--Internal Data width
	constant ParameterBitWidth : integer := 32;
	--Internal Data from MGTs 
	type InternalData is array (natural range <>) of std_logic_vector(ParameterBitWidth - 1 downto 0);
	type Deserialized_Data_Array is array (natural range <>) of std_logic_vector(128 - 1 downto 0);
	type ChannelData is array (natural range <>) of InternalData(3 downto 0);
	
	--36bit data type for playback/spy data(32bit) + charisk(4bit)
	type ArrayOf36bitVectors is array (natural range <>) of std_logic_vector(35 downto 0);

   type arraySLVgenericEt is array (natural range <>) of std_logic_vector(GenericEtBitWidth - 1 downto 0);
   type arraySLVgenericEta is array (natural range <>) of std_logic_vector(GenericEtaBitWidth - 1 downto 0);
   type arraySLVgenericPhi is array (natural range <>) of std_logic_vector(GenericPhiBitWidth - 1 downto 0);

	-- TOB data are arranged as arrays of records. Use these for unsorted/presorted TOBs:

	type ClusterTOB is record
		Et   : std_logic_vector(ClusterEtBitWidth-1 downto 0);
		Isol : std_logic_vector(ClusterIsolBitWidth-1 downto 0);
		Eta  : std_logic_vector(ClusterEtaBitWidth-1 downto 0);
		Phi  : std_logic_vector(ClusterPhiBitWidth-1 downto 0);
		Overflow : std_logic;
	end record;

    constant emptyClusterTOB : ClusterTOB := (
    	Et   => (others => '0'),
    	Isol => (others => '0'),
    	Eta  => (others => '0'),
    	Phi  => (others => '0'),
    	Overflow => '0'
    );

	type JetTOB is record
		Et1 : std_logic_vector(JetEt1BitWidth-1 downto 0);
		Et2 : std_logic_vector(JetEt2BitWidth-1 downto 0);
		Eta : std_logic_vector(JetEtaBitWidth-1 downto 0);
		Phi : std_logic_vector(JetPhiBitWidth-1 downto 0);
		Overflow : std_logic;
	end record;

    constant emptyJetTOB : JetTOB := (
    	Et1 => (others => '0'),
    	Et2 => (others => '0'),
    	Eta => (others => '0'),
    	Phi => (others => '0'),
    	Overflow => '0'
    );


    type SumEtTOB is record
        Et:             std_logic_vector(SumEt_Et_bitWidth-1 downto 0);
        Et_Overflow:    std_logic;
        rEt:            std_logic_vector(SumEt_rEt_bitWidth-1 downto 0);
        rEt_Overflow:   std_logic;
        Ex:             std_logic_vector(SumEt_Ex_bitWidth-1 downto 0);
        Ex_Overflow:    std_logic;
        rEx:            std_logic_vector(SumEt_rEx_bitWidth-1 downto 0);
        rEx_Overflow:   std_logic;
        Ey:             std_logic_vector(SumEt_Ey_bitWidth-1 downto 0);
        Ey_Overflow:    std_logic;
        rEy:            std_logic_vector(SumEt_rEy_bitWidth-1 downto 0);
        rEy_Overflow:   std_logic;
    end record;
    
    constant emptySumEtTOB: SumEtTob := (
        Et  => (others => '0'),
        Et_Overflow => '0',
        rEt  => (others => '0'),
        rEt_Overflow => '0',
        Ex  => (others => '0'),
        Ex_Overflow => '0',
        rEx  => (others => '0'),
        rEx_Overflow => '0',
        Ey  => (others => '0'),
        Ey_Overflow => '0',
        rEy  => (others => '0'),
        rEy_Overflow => '0'
    ); 


	type MuonTOB is record --TODO: How translate MuonTOB binning to GenericTOB??? Where? In Selection/Sort algos? Other Types converted correctly?
		Pt  : std_logic_vector(MuonPtBitWidth-1 downto 0);
		Eta : std_logic_vector(MuonEtaBitWidth-1 downto 0);
		Phi : std_logic_vector(MuonPhiBitWidth-1 downto 0);
		Overflow : std_logic;
		no_candidate: std_logic;
	end record;
	
	constant emptyMuonTOB : MuonTOB := (
		Pt  => (others => '0'),
		Eta => (others => '0'),
		Phi => (others => '0'),
		Overflow => '0',
		no_candidate => '1'
	);
	
	type MetTOB is record
		Ex : std_logic_vector(METEnergyBitWidth-1 downto 0);
		Ey : std_logic_vector(METEnergyBitWidth-1 downto 0);
		Et : std_logic_vector(METEnergyBitWidth-1 downto 0);
		Overflow : std_logic;
	end record;
	
	constant emptyMetTOB : MetTOB := (
		Ex => (others => '0'),
		Ey => (others => '0'),
		Et => (others => '0'),
		Overflow => '0'
	);

	type ClusterArray is array (natural range <>) of ClusterTOB;
	type JetArray is array (natural range <>) of JetTOB;
	type SumEtArray is array(natural range <>) of SumEtTOB;
	type MuonArray is array (natural range <>) of MuonTOB;
	type MetArray is array (natural range <>) of MetTOB;



    type ClusterWord is record
        tobs:       ClusterArray(4 downto 0);
        overflow:   std_logic;
    end record;
    
    constant emptyClusterWord : ClusterWord :=(
    	tobs => (others => emptyClusterTOB),
    	overflow => '0'
    );
    
    type JetWord is record
        tobs:       JetArray(3 downto 0);
        overflow:   std_logic;
    end record;
    
    constant emptyJetWord : JetWord :=(
    	tobs => (others => emptyJetTOB),
    	overflow => '0'
    );
    
    type ClusterWordArray is array(natural range <>) of ClusterWord;
    type JetWordArray is array(natural range <>) of JetWord;




	-- After selection and sorting, TOB data are all of same type.
	-- Use a generic TOB record for sorted TOBs:

	type GenericTOB is record
		Et       : std_logic_vector(GenericEtBitWidth - 1 downto 0);    -- Pad unused bits with zeros, they will be optimised away
--		EtSqr    : std_logic_vector(GenericEtSqrBitWidth - 1 downto 0); -- Pad unused bits with zeros, they will be optimised away
		Eta      : std_logic_vector(GenericEtaBitWidth - 1 downto 0);   -- Again, pad bottom bit with zero for muons and jets
		Phi      : std_logic_vector(GenericPhiBitWidth - 1 downto 0);
		Overflow : std_logic; -- Overflow bit is only set for element (0) of TOBArray
	end record;
	
	constant empty_tob : GenericTOB := (
		Et    => (others => '0'),
--		EtSqr => (others => '0'),
		Eta   => (others => '0'),
		Phi   => (others => '0'),
		Overflow => '0'
	);

	type TOBArray is array (natural range <>) of GenericTOB;

	-- Parameters for trigger algorithms configuration

	constant MaxParams : integer := 32;  -- Maximum number of parameters

	type ParameterArray is array (MaxParams - 1 downto 0) of std_logic_vector(ParameterBitWidth - 1 downto 0);
	type ParameterSpace is array (natural range <>) of ParameterArray;

	-- Debug outputs for IPBus

	type BitArray128 is array (natural range <> ) of std_logic_vector(128 - 1 downto 0);
--	type DebugArray is BitArray128(256 - 33 - 1);

end L1TopoDataTypes;

package body L1TopoDataTypes is
end L1TopoDataTypes;  
