set_clock_groups -asynchronous -group [get_clocks "ROD_GTH_CLK"]

set_property IOSTANDARD LVDS [get_ports GCK1_P]
set_property DIFF_TERM TRUE [get_ports GCK1_P]
set_property PACKAGE_PIN H29 [get_ports GCK1_N]
set_property IOSTANDARD LVDS [get_ports GCK1_N]
set_property DIFF_TERM TRUE [get_ports GCK1_N]
create_clock -period 24.950 -name GCK1_CLK -waveform {0.000 12.475} [get_ports GCK1_P]

set_property IOSTANDARD LVDS [get_ports GCK2_P]
set_property DIFF_TERM TRUE [get_ports GCK2_P]
set_property PACKAGE_PIN J27 [get_ports GCK2_N]
set_property IOSTANDARD LVDS [get_ports GCK2_N]
set_property DIFF_TERM TRUE [get_ports GCK2_N]
create_clock -period 24.950 -name GCK2_CLK -waveform {0.000 12.475} [get_ports GCK2_P]

set_property PACKAGE_PIN BB8 [get_ports {MGT2_CLK_P[1]}]
set_property PACKAGE_PIN AN10 [get_ports {MGT2_CLK_P[2]}]
set_property PACKAGE_PIN AB8 [get_ports {MGT2_CLK_P[3]}]
set_property PACKAGE_PIN R10 [get_ports {MGT2_CLK_P[4]}]
set_property PACKAGE_PIN G10 [get_ports {MGT2_CLK_P[5]}]
set_property PACKAGE_PIN BB37 [get_ports {MGT2_CLK_P[7]}]
set_property PACKAGE_PIN AN35 [get_ports {MGT2_CLK_P[8]}]
set_property PACKAGE_PIN AB37 [get_ports {MGT2_CLK_P[9]}]
set_property PACKAGE_PIN R35 [get_ports {MGT2_CLK_P[10]}]
set_property PACKAGE_PIN G35 [get_ports {MGT2_CLK_P[11]}]

set_property PACKAGE_PIN AT8 [get_ports {MGT4_CLK_P[1]}]
set_property PACKAGE_PIN AH8 [get_ports {MGT4_CLK_P[2]}]
set_property PACKAGE_PIN W10 [get_ports {MGT4_CLK_P[3]}]
set_property PACKAGE_PIN L10 [get_ports {MGT4_CLK_P[4]}]
set_property PACKAGE_PIN C10 [get_ports {MGT4_CLK_P[5]}]
set_property PACKAGE_PIN AT37 [get_ports {MGT4_CLK_P[7]}]
set_property PACKAGE_PIN AH37 [get_ports {MGT4_CLK_P[8]}]
set_property PACKAGE_PIN W35 [get_ports {MGT4_CLK_P[9]}]
set_property PACKAGE_PIN L35 [get_ports {MGT4_CLK_P[10]}]
set_property PACKAGE_PIN C35 [get_ports {MGT4_CLK_P[11]}]


create_clock -period 3.118 -name ROD_GTH_CLK [get_pins {*/RTDP/QUAD_GEN[1].GTH16_ROD.QUAD/gtwizard_gth16Quad_init/gtwizard_gth16QuadROD_i/gt0_gtwizard_gth16QuadROD_i/gthe2_i/TXOUTCLK}]
create_clock -period 6.237 -name MUON_GTH_CLK -waveform {0.000 3.118} [get_pins {*/RTDP/QUAD_GEN[19].GTH32MUON_GEN.QUAD_MUON/MUON_GTH_INST/muon_gth32_i/gt0_muon_gth32_i/gthe2_i/RXOUTCLK}]


create_clock -period 6.237 -name MGT2_CLK -waveform {0.000 3.118} [get_ports {MGT2_CLK_P[1] MGT2_CLK_P[2] MGT2_CLK_P[3] MGT2_CLK_P[4] MGT2_CLK_P[5] MGT2_CLK_P[7] MGT2_CLK_P[8] MGT2_CLK_P[9] MGT2_CLK_P[10] MGT2_CLK_P[11]}]
create_clock -period 6.237 -name MGT4_CLK -waveform {0.000 3.118} [get_ports {MGT4_CLK_P[1] MGT4_CLK_P[2] MGT4_CLK_P[3] MGT4_CLK_P[4] MGT4_CLK_P[5] MGT4_CLK_P[7] MGT4_CLK_P[8] MGT4_CLK_P[9] MGT4_CLK_P[10] MGT4_CLK_P[11]}]


##MGT RxP Pin Constraints (RxN,TxP,TxN are then constrained automatically)
##AV1
##MGT[X/H]RXP[0..4]_110
set_property PACKAGE_PIN BD8 [get_ports {RxP[0]}]
set_property PACKAGE_PIN BC6 [get_ports {RxP[1]}]
set_property PACKAGE_PIN BA6 [get_ports {RxP[2]}]
set_property PACKAGE_PIN AW6 [get_ports {RxP[3]}]
# #MGT[X/H]RXP[0..4]_111
set_property PACKAGE_PIN AV8 [get_ports {RxP[4]}]
set_property PACKAGE_PIN AU6 [get_ports {RxP[5]}]
set_property PACKAGE_PIN AR6 [get_ports {RxP[6]}]
set_property PACKAGE_PIN AP8 [get_ports {RxP[7]}]
# #MGT[X/H]RXP[0..4]_112
set_property PACKAGE_PIN AN6 [get_ports {RxP[8]}]
set_property PACKAGE_PIN AM4 [get_ports {RxP[9]}]
set_property PACKAGE_PIN AM8 [get_ports {RxP[10]}]
set_property PACKAGE_PIN AL6 [get_ports {RxP[11]}]
#AV2
#MGT[X/H]RXP[0..4]_113
set_property PACKAGE_PIN AK8 [get_ports {RxP[12]}]
set_property PACKAGE_PIN AJ6 [get_ports {RxP[13]}]
set_property PACKAGE_PIN AG6 [get_ports {RxP[14]}]
set_property PACKAGE_PIN AE6 [get_ports {RxP[15]}]
#MGT[X/H]RXP[0..4]_114
set_property PACKAGE_PIN AD8 [get_ports {RxP[16]}]
set_property PACKAGE_PIN AC6 [get_ports {RxP[17]}]
set_property PACKAGE_PIN AA6 [get_ports {RxP[18]}]
set_property PACKAGE_PIN Y8 [get_ports {RxP[19]}]
#MGT[X/H]RXP[0..4]_115
set_property PACKAGE_PIN W6 [get_ports {RxP[20]}]
set_property PACKAGE_PIN V8 [get_ports {RxP[21]}]
set_property PACKAGE_PIN U6 [get_ports {RxP[22]}]
set_property PACKAGE_PIN T8 [get_ports {RxP[23]}]
#AV3
#MGT[X/H]RXP[0..4]_116
set_property PACKAGE_PIN R6 [get_ports {RxP[24]}]
set_property PACKAGE_PIN P8 [get_ports {RxP[25]}]
set_property PACKAGE_PIN N6 [get_ports {RxP[26]}]
set_property PACKAGE_PIN M8 [get_ports {RxP[27]}]
# #MGT[X/H]RXP[0..4]_117
set_property PACKAGE_PIN L6 [get_ports {RxP[28]}]
set_property PACKAGE_PIN K8 [get_ports {RxP[29]}]
set_property PACKAGE_PIN J6 [get_ports {RxP[30]}]
set_property PACKAGE_PIN H8 [get_ports {RxP[31]}]
# #MGT[X/H]RXP[0..4]_118
set_property PACKAGE_PIN G6 [get_ports {RxP[32]}]
set_property PACKAGE_PIN F8 [get_ports {RxP[33]}]
set_property PACKAGE_PIN E6 [get_ports {RxP[34]}]
set_property PACKAGE_PIN D8 [get_ports {RxP[35]}]
# #AV4
# #MGT[X/H]RXP[0..4]_119
set_property PACKAGE_PIN D4 [get_ports {RxP[36]}]
set_property PACKAGE_PIN C6 [get_ports {RxP[37]}]
set_property PACKAGE_PIN B8 [get_ports {RxP[38]}]
set_property PACKAGE_PIN A6 [get_ports {RxP[39]}]
##second block after AV7 (see below)
##no third block
##AV5
#MGT[X/H]RXP[0..4]_210
set_property PACKAGE_PIN BD37 [get_ports {RxP[40]}]
set_property PACKAGE_PIN BC39 [get_ports {RxP[41]}]
set_property PACKAGE_PIN BA39 [get_ports {RxP[42]}]
set_property PACKAGE_PIN AW39 [get_ports {RxP[43]}]
# #MGT[X/H]RXP[0..4]_211
set_property PACKAGE_PIN AV37 [get_ports {RxP[44]}]
set_property PACKAGE_PIN AU39 [get_ports {RxP[45]}]
set_property PACKAGE_PIN AR39 [get_ports {RxP[46]}]
set_property PACKAGE_PIN AP37 [get_ports {RxP[47]}]
# #MGT[X/H]RXP[0..4]_212
set_property PACKAGE_PIN AN39 [get_ports {RxP[48]}]
set_property PACKAGE_PIN AM41 [get_ports {RxP[49]}]
set_property PACKAGE_PIN AM37 [get_ports {RxP[50]}]
set_property PACKAGE_PIN AL39 [get_ports {RxP[51]}]
# #AV6
# #MGT[X/H]RXP[0..4]_213
set_property PACKAGE_PIN AK37 [get_ports {RxP[52]}]
set_property PACKAGE_PIN AJ39 [get_ports {RxP[53]}]
set_property PACKAGE_PIN AG39 [get_ports {RxP[54]}]
set_property PACKAGE_PIN AE39 [get_ports {RxP[55]}]
# #MGT[X/H]RXP[0..4]_214
set_property PACKAGE_PIN AD37 [get_ports {RxP[56]}]
set_property PACKAGE_PIN AC39 [get_ports {RxP[57]}]
set_property PACKAGE_PIN AA39 [get_ports {RxP[58]}]
set_property PACKAGE_PIN Y37 [get_ports {RxP[59]}]
#MGT[X/H]RXP[0..4]_215
set_property PACKAGE_PIN W39 [get_ports {RxP[60]}]
set_property PACKAGE_PIN V37 [get_ports {RxP[61]}]
set_property PACKAGE_PIN U39 [get_ports {RxP[62]}]
set_property PACKAGE_PIN T37 [get_ports {RxP[63]}]
# #AV7
# #MGT[X/H]RxP[0..4]_216
set_property PACKAGE_PIN R39 [get_ports {RxP[64]}]
set_property PACKAGE_PIN P37 [get_ports {RxP[65]}]
set_property PACKAGE_PIN N39 [get_ports {RxP[66]}]
set_property PACKAGE_PIN M37 [get_ports {RxP[67]}]
# #MGT[X/H]RxP[0..4]_217
set_property PACKAGE_PIN L39 [get_ports {RxP[68]}]
set_property PACKAGE_PIN K37 [get_ports {RxP[69]}]
set_property PACKAGE_PIN J39 [get_ports {RxP[70]}]
set_property PACKAGE_PIN H37 [get_ports {RxP[71]}]
# #MGT[X/H]RxP[0..4]_218
set_property PACKAGE_PIN G39 [get_ports {RxP[72]}]
set_property PACKAGE_PIN F37 [get_ports {RxP[73]}]
set_property PACKAGE_PIN E39 [get_ports {RxP[74]}]
set_property PACKAGE_PIN D37 [get_ports {RxP[75]}]
# #AV4 (again)
# #second block of AV4:
# #MGT[X/H]RxP[0..4]_219
set_property PACKAGE_PIN D41 [get_ports {RxP[76]}]
set_property PACKAGE_PIN C39 [get_ports {RxP[77]}]
set_property PACKAGE_PIN B37 [get_ports {RxP[78]}]
set_property PACKAGE_PIN A39 [get_ports {RxP[79]}]



## #MGT Reference Clock Pin Constraints (MGTREFCLK0N, MGTREFCLK1P, MGTREFCLK1N are then partly constrained automatically)
## Now also constrain the negative pair since either TTC or Crystal MGT Reference Clock is constrained automatically
## #AV1
# #MGTREFCLK0P_110 TTC JC
#set_property PACKAGE_PIN AY7 [get_ports {MgtRefClk0N[0]}]
## #MGTREFCLK0P_110 Crystal
#set_property PACKAGE_PIN BB7 [get_ports {MgtRefClk1N[0]}]
## #MGTREFCLK0P_111
#set_property PACKAGE_PIN AR9 [get_ports {MgtRefClk0N[1]}]
#set_property PACKAGE_PIN AT7 [get_ports {MgtRefClk1N[1]}]
## #MGTREFCLK0P_112
#set_property PACKAGE_PIN AL9 [get_ports {MgtRefClk0N[2]}]
#set_property PACKAGE_PIN AN9 [get_ports {MgtRefClk1N[2]}]
##AV2
##MGTREFCLK0P_113 TTC JC
#set_property PACKAGE_PIN AF7 [get_ports {MgtRefClk0N[3]}]
##MGTREFCLK0P_113 Crystal
#set_property PACKAGE_PIN AH7 [get_ports {MgtRefClk1N[3]}]
##MGTREFCLK0P_114
#set_property PACKAGE_PIN AA9 [get_ports {MgtRefClk0N[4]}]
#set_property PACKAGE_PIN AB7 [get_ports {MgtRefClk1N[4]}]
##MGTREFCLK0P_115
#set_property PACKAGE_PIN U9 [get_ports {MgtRefClk0N[5]}]
#set_property PACKAGE_PIN W9 [get_ports {MgtRefClk1N[5]}]
##AV3
## #MGTREFCLK0P_116 TTC JC
#set_property PACKAGE_PIN N9 [get_ports {MgtRefClk0N[6]}]
## #MGTREFCLK0P_116 Crystal
#set_property PACKAGE_PIN R9 [get_ports {MgtRefClk1N[6]}]
## #MGTREFCLK0P_117
#set_property PACKAGE_PIN J9 [get_ports {MgtRefClk0N[7]}]
#set_property PACKAGE_PIN L9 [get_ports {MgtRefClk1N[7]}]
## #MGTREFCLK0P_118 TTC JC
#set_property PACKAGE_PIN E9 [get_ports {MgtRefClk0N[8]}]
## #MGTREFCLK0P_118 Crystal
#set_property PACKAGE_PIN G9 [get_ports {MgtRefClk1N[8]}]
## #AV4
## #MGTREFCLK0P_119
#set_property PACKAGE_PIN A9 [get_ports {MgtRefClk0N[9]}]
#set_property PACKAGE_PIN C9 [get_ports {MgtRefClk1N[9]}]
### #second block after AV7 (see below)
## #no third block
## #AV5
# #MGTREFCLK0P_210 TTC JC
#set_property PACKAGE_PIN AY38 [get_ports {MgtRefClk0N[10]}]
## #MGTREFCLK0P_210 Crystal
#set_property PACKAGE_PIN BB38 [get_ports {MgtRefClk1N[10]}]
## #MGTREFCLK0P_211
#set_property PACKAGE_PIN AR36 [get_ports {MgtRefClk0N[11]}]
#set_property PACKAGE_PIN AT38 [get_ports {MgtRefClk1N[11]}]
## #MGTREFCLK0P_212
#set_property PACKAGE_PIN AL36 [get_ports {MgtRefClk0N[12]}]
#set_property PACKAGE_PIN AN36 [get_ports {MgtRefClk1N[12]}]
## #AV6
## #MGTREFCLK0P_213 TTC JC
#set_property PACKAGE_PIN AF38 [get_ports {MgtRefClk0N[13]}]
## #MGTREFCLK0P_213 Crystal
#set_property PACKAGE_PIN AH38 [get_ports {MgtRefClk1N[13]}]
## #MGTREFCLK0P_214
#set_property PACKAGE_PIN AA36 [get_ports {MgtRefClk0N[14]}]
#set_property PACKAGE_PIN AB38 [get_ports {MgtRefClk1N[14]}]
## #MGTREFCLK0P_215
#set_property PACKAGE_PIN U36 [get_ports {MgtRefClk0N[15]}]
#set_property PACKAGE_PIN W36 [get_ports {MgtRefClk1N[15]}]
## #AV7
## #MGTREFCLK0P_216 TTC JC
#set_property PACKAGE_PIN N36 [get_ports {MgtRefClk0N[16]}]
## #MGTREFCLK0P_216 Crystal
#set_property PACKAGE_PIN R36 [get_ports {MgtRefClk1N[16]}]
## #MGTREFCLK0P_217
#set_property PACKAGE_PIN J36 [get_ports {MgtRefClk0N[17]}]
#set_property PACKAGE_PIN L36 [get_ports {MgtRefClk1N[17]}]
## #MGTREFCLK0P_218 TTC JC
#set_property PACKAGE_PIN E36 [get_ports {MgtRefClk0N[18]}]
## #MGTREFCLK0P_218 Crystal
#set_property PACKAGE_PIN G36 [get_ports {MgtRefClk1N[18]}]
## #AV4 (again)
## #second block of AV4:
## #MGTREFCLK0P_219
#set_property PACKAGE_PIN A36 [get_ports {MgtRefClk0N[19]}]
#set_property PACKAGE_PIN C36 [get_ports {MgtRefClk1N[19]}]





##Clock Constraints
#create_clock -period 6.237 -name MGT_REF_CLK0 -waveform {0.000 3.118} [get_ports {MgtRefClk0P[0] MgtRefClk0P[1] MgtRefClk0P[2] MgtRefClk0P[3] MgtRefClk0P[4] MgtRefClk0P[5] MgtRefClk0P[6] MgtRefClk0P[7] MgtRefClk0P[8] MgtRefClk0P[9] MgtRefClk0P[10] MgtRefClk0P[11] MgtRefClk0P[12] MgtRefClk0P[13] MgtRefClk0P[14] MgtRefClk0P[15] MgtRefClk0P[16] MgtRefClk0P[17] MgtRefClk0P[18] MgtRefClk0P[19]}]
##create_clock -period 6.237 -name MGT_REF_CLK0 -waveform {0.000 3.118} [get_ports {MgtRefClk0P[3] MgtRefClk0P[4] MgtRefClk0P[5] MgtRefClk0P[6] MgtRefClk0P[7] MgtRefClk0P[8]}]
#create_clock -period 6.237 -name MGT_REF_CLK1 -waveform {0.000 3.118} [get_ports {MgtRefClk1P[0] MgtRefClk1P[1] MgtRefClk1P[2] MgtRefClk1P[3] MgtRefClk1P[4] MgtRefClk1P[5] MgtRefClk1P[6] MgtRefClk1P[7] MgtRefClk1P[8] MgtRefClk1P[9] MgtRefClk1P[10] MgtRefClk1P[11] MgtRefClk1P[12] MgtRefClk1P[13] MgtRefClk1P[14] MgtRefClk1P[15] MgtRefClk1P[16] MgtRefClk1P[17] MgtRefClk1P[18] MgtRefClk1P[19]}]
##create_clock -period 6.237 -name MGT_REF_CLK1 -waveform {0.000 3.118} [get_ports {MgtRefClk1P[3] MgtRefClk1P[4] MgtRefClk1P[5] MgtRefClk1P[6] MgtRefClk1P[7] MgtRefClk1P[8]}]








#set_property PACKAGE_PIN BD29 [get_ports {CTRLBUS_IN_P[0]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_IN_P[0]}]
#set_property DIFF_TERM TRUE [get_ports {CTRLBUS_IN_P[0]}]
#set_property DIFF_TERM TRUE [get_ports {CTRLBUS_IN_N[0]}]

#set_property PACKAGE_PIN BB30 [get_ports {CTRLBUS_IN_P[1]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_IN_P[1]}]
#set_property DIFF_TERM TRUE [get_ports {CTRLBUS_IN_P[1]}]
#set_property DIFF_TERM TRUE [get_ports {CTRLBUS_IN_N[1]}]

#set_property PACKAGE_PIN BC28 [get_ports {CTRLBUS_IN_P[2]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_IN_P[2]}]
#set_property DIFF_TERM TRUE [get_ports {CTRLBUS_IN_P[2]}]
#set_property DIFF_TERM TRUE [get_ports {CTRLBUS_IN_N[2]}]

#set_property PACKAGE_PIN BA29 [get_ports {CTRLBUS_IN_P[3]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_IN_P[3]}]
#set_property DIFF_TERM TRUE [get_ports {CTRLBUS_IN_P[3]}]
#set_property DIFF_TERM TRUE [get_ports {CTRLBUS_IN_N[3]}]

#set_property PACKAGE_PIN AW29 [get_ports {CTRLBUS_IN_P[4]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_IN_P[4]}]
#set_property DIFF_TERM TRUE [get_ports {CTRLBUS_IN_P[4]}]
#set_property DIFF_TERM TRUE [get_ports {CTRLBUS_IN_N[4]}]

#set_property PACKAGE_PIN AW27 [get_ports {CTRLBUS_IN_P[5]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_IN_P[5]}]
#set_property DIFF_TERM TRUE [get_ports {CTRLBUS_IN_P[5]}]
#set_property DIFF_TERM TRUE [get_ports {CTRLBUS_IN_N[5]}]

#set_property PACKAGE_PIN AY31 [get_ports {CTRLBUS_IN_P[6]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_IN_P[6]}]
#set_property DIFF_TERM TRUE [get_ports {CTRLBUS_IN_P[6]}]

##set_property PACKAGE_PIN	AY28	[get_ports {CTRLBUS_IN_P[7]}]
##set_property IOSTANDARD		LVDS	[get_ports {CTRLBUS_IN_P[7]}]
##set_property DIFF_TERM		TRUE	[get_ports {CTRLBUS_IN_P[7]}]

#set_property PACKAGE_PIN AV27 [get_ports {CTRLBUS_OUT_P[9]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_OUT_P[9]}]
#set_property DIFF_TERM FALSE [get_ports {CTRLBUS_OUT_P[9]}]

#set_property PACKAGE_PIN AU30 [get_ports {CTRLBUS_OUT_P[10]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_OUT_P[10]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_OUT_N[10]}]
#set_property DIFF_TERM FALSE [get_ports {CTRLBUS_OUT_P[10]}]

#set_property PACKAGE_PIN AU28 [get_ports {CTRLBUS_OUT_P[11]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_OUT_P[11]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_OUT_N[11]}]
#set_property DIFF_TERM FALSE [get_ports {CTRLBUS_OUT_P[11]}]

#set_property PACKAGE_PIN AT28 [get_ports {CTRLBUS_OUT_P[12]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_OUT_P[12]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_OUT_N[12]}]
#set_property DIFF_TERM FALSE [get_ports {CTRLBUS_OUT_P[12]}]

#set_property PACKAGE_PIN AP29 [get_ports {CTRLBUS_OUT_P[13]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_OUT_P[13]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_OUT_N[13]}]
#set_property DIFF_TERM FALSE [get_ports {CTRLBUS_OUT_P[13]}]

#set_property PACKAGE_PIN AR28 [get_ports {CTRLBUS_OUT_P[14]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_OUT_P[14]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_OUT_N[14]}]
#set_property DIFF_TERM FALSE [get_ports {CTRLBUS_OUT_P[14]}]

#set_property PACKAGE_PIN AN28 [get_ports {CTRLBUS_OUT_P[15]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_OUT_P[15]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_OUT_N[15]}]
#set_property DIFF_TERM FALSE [get_ports {CTRLBUS_OUT_P[15]}]

#set_property PACKAGE_PIN AM30 [get_ports {CTRLBUS_OUT_P[16]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_OUT_P[16]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_OUT_N[16]}]
#set_property DIFF_TERM FALSE [get_ports {CTRLBUS_OUT_P[16]}]

#set_property PACKAGE_PIN AK27 [get_ports {CTRLBUS_OUT_P[17]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_OUT_P[17]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_OUT_N[17]}]
#set_property DIFF_TERM FALSE [get_ports {CTRLBUS_OUT_P[17]}]

#set_property PACKAGE_PIN AM27 [get_ports {CTRLBUS_OUT_P[18]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_OUT_P[18]}]
#set_property DIFF_TERM FALSE [get_ports {CTRLBUS_OUT_P[18]}]

#set_property PACKAGE_PIN AJ29 [get_ports {CTRLBUS_OUT_P[19]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_OUT_P[19]}]
#set_property DIFF_TERM FALSE [get_ports {CTRLBUS_OUT_P[19]}]

#set_property PACKAGE_PIN AL28 [get_ports {CTRLBUS_OUT_P[20]}]
#set_property IOSTANDARD LVDS [get_ports {CTRLBUS_OUT_P[20]}]
#set_property DIFF_TERM FALSE [get_ports {CTRLBUS_OUT_P[20]}]


set_property PACKAGE_PIN BD29 [get_ports {CTRLBUS_0_P}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_0_P}]
set_property DIFF_TERM TRUE [get_ports {CTRLBUS_0_P}]
set_property DIFF_TERM TRUE [get_ports {CTRLBUS_0_N}]

set_property PACKAGE_PIN BB30 [get_ports {CTRLBUS_1_P}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_1_P}]
set_property DIFF_TERM TRUE [get_ports {CTRLBUS_1_P}]
set_property DIFF_TERM TRUE [get_ports {CTRLBUS_1_N}]

set_property PACKAGE_PIN BC28 [get_ports {CTRLBUS_2_P}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_2_P}]
set_property DIFF_TERM TRUE [get_ports {CTRLBUS_2_P}]
set_property DIFF_TERM TRUE [get_ports {CTRLBUS_2_N}]

set_property PACKAGE_PIN BA29 [get_ports {CTRLBUS_3_P}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_3_P}]
set_property DIFF_TERM TRUE [get_ports {CTRLBUS_3_P}]
set_property DIFF_TERM TRUE [get_ports {CTRLBUS_3_N}]

set_property PACKAGE_PIN AW29 [get_ports {CTRLBUS_4_P}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_4_P}]
set_property DIFF_TERM TRUE [get_ports {CTRLBUS_4_P}]
set_property DIFF_TERM TRUE [get_ports {CTRLBUS_4_N}]

set_property PACKAGE_PIN AW27 [get_ports {CTRLBUS_5_P}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_5_P}]
set_property DIFF_TERM TRUE [get_ports {CTRLBUS_5_P}]
set_property DIFF_TERM TRUE [get_ports {CTRLBUS_5_N}]

set_property PACKAGE_PIN AY31 [get_ports {CTRLBUS_6_P}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_6_P}]
set_property DIFF_TERM FALSE [get_ports {CTRLBUS_6_P}]

set_property PACKAGE_PIN	AY28	[get_ports {CTRLBUS_7_P}]
set_property IOSTANDARD		LVDS	[get_ports {CTRLBUS_7_P}]
set_property DIFF_TERM		FALSE	[get_ports {CTRLBUS_7_P}]

set_property PACKAGE_PIN	AW30	[get_ports {CTRLBUS_8_P}]
set_property IOSTANDARD		LVDS	[get_ports {CTRLBUS_8_P}]
set_property DIFF_TERM		TRUE	[get_ports {CTRLBUS_8_P}]

set_property PACKAGE_PIN AV27 [get_ports {CTRLBUS_9_P}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_9_P}]
set_property DIFF_TERM FALSE [get_ports {CTRLBUS_9_P}]

set_property PACKAGE_PIN AU30 [get_ports {CTRLBUS_10_P}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_10_P}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_10_N}]
set_property DIFF_TERM FALSE [get_ports {CTRLBUS_10_P}]

set_property PACKAGE_PIN AU28 [get_ports {CTRLBUS_11_P}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_11_P}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_11_N}]
set_property DIFF_TERM FALSE [get_ports {CTRLBUS_11_P}]

set_property PACKAGE_PIN AT28 [get_ports {CTRLBUS_12_P}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_12_P}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_12_N}]
set_property DIFF_TERM FALSE [get_ports {CTRLBUS_12_P}]

set_property PACKAGE_PIN AP29 [get_ports {CTRLBUS_13_P}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_13_P}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_13_N}]
set_property DIFF_TERM FALSE [get_ports {CTRLBUS_13_P}]

set_property PACKAGE_PIN AR28 [get_ports {CTRLBUS_14_P}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_14_P}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_14_N}]
set_property DIFF_TERM FALSE [get_ports {CTRLBUS_14_P}]

set_property PACKAGE_PIN AN28 [get_ports {CTRLBUS_15_P}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_15_P}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_15_N}]
set_property DIFF_TERM FALSE [get_ports {CTRLBUS_15_P}]

set_property PACKAGE_PIN AM30 [get_ports {CTRLBUS_16_P}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_16_P}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_16_N}]
set_property DIFF_TERM FALSE [get_ports {CTRLBUS_16_P}]

set_property PACKAGE_PIN AK27 [get_ports {CTRLBUS_17_P}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_17_P}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_17_N}]
set_property DIFF_TERM FALSE [get_ports {CTRLBUS_17_P}]

set_property PACKAGE_PIN AM27 [get_ports {CTRLBUS_18_P}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_18_P}]
set_property DIFF_TERM FALSE [get_ports {CTRLBUS_18_P}]

set_property PACKAGE_PIN AJ29 [get_ports {CTRLBUS_19_P}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_19_P}]
set_property DIFF_TERM FALSE [get_ports {CTRLBUS_19_P}]

set_property PACKAGE_PIN AL28 [get_ports {CTRLBUS_20_P}]
set_property IOSTANDARD LVDS [get_ports {CTRLBUS_20_P}]
set_property DIFF_TERM FALSE [get_ports {CTRLBUS_20_P}]


set_property PACKAGE_PIN A28 [get_ports {EXT_V7_P[0]}]
set_property IOSTANDARD LVDS [get_ports {EXT_V7_P[0]}]
set_property DIFF_TERM FALSE [get_ports {EXT_V7_P[0]}]

set_property PACKAGE_PIN D29 [get_ports {EXT_V7_P[1]}]
set_property IOSTANDARD LVDS [get_ports {EXT_V7_P[1]}]
set_property DIFF_TERM FALSE [get_ports {EXT_V7_P[1]}]

set_property PACKAGE_PIN C27 [get_ports {EXT_V7_P[2]}]
set_property IOSTANDARD LVDS [get_ports {EXT_V7_P[2]}]
set_property DIFF_TERM FALSE [get_ports {EXT_V7_P[2]}]

set_property PACKAGE_PIN C28 [get_ports {EXT_V7_P[3]}]
set_property IOSTANDARD LVDS [get_ports {EXT_V7_P[3]}]
set_property DIFF_TERM FALSE [get_ports {EXT_V7_P[3]}]

set_property PACKAGE_PIN B26 [get_ports {EXT_V7_P[4]}]
set_property IOSTANDARD LVDS [get_ports {EXT_V7_P[4]}]
set_property DIFF_TERM FALSE [get_ports {EXT_V7_P[4]}]

set_property PACKAGE_PIN D26 [get_ports {EXT_V7_P[5]}]
set_property IOSTANDARD LVDS [get_ports {EXT_V7_P[5]}]
set_property DIFF_TERM FALSE [get_ports {EXT_V7_P[5]}]

set_property PACKAGE_PIN G28 [get_ports {EXT_V7_P[6]}]
set_property IOSTANDARD LVDS [get_ports {EXT_V7_P[6]}]
set_property DIFF_TERM FALSE [get_ports {EXT_V7_P[6]}]

set_property PACKAGE_PIN F26 [get_ports {EXT_V7_P[7]}]
set_property IOSTANDARD LVDS [get_ports {EXT_V7_P[7]}]
set_property DIFF_TERM FALSE [get_ports {EXT_V7_P[7]}]

set_property PACKAGE_PIN F29 [get_ports {EXT_V7_P[8]}]
set_property IOSTANDARD LVDS [get_ports {EXT_V7_P[8]}]
set_property DIFF_TERM FALSE [get_ports {EXT_V7_P[8]}]

set_property PACKAGE_PIN E27 [get_ports {EXT_V7_P[9]}]
set_property IOSTANDARD LVDS [get_ports {EXT_V7_P[9]}]
set_property DIFF_TERM FALSE [get_ports {EXT_V7_P[9]}]

set_property PACKAGE_PIN H27 [get_ports {EXT_V7_P[10]}]
set_property IOSTANDARD LVDS [get_ports {EXT_V7_P[10]}]
set_property DIFF_TERM FALSE [get_ports {EXT_V7_P[10]}]

set_property PACKAGE_PIN K28 [get_ports {EXT_V7_P[11]}]
set_property IOSTANDARD LVDS [get_ports {EXT_V7_P[11]}]
set_property DIFF_TERM FALSE [get_ports {EXT_V7_P[11]}]

set_property PACKAGE_PIN N30 [get_ports {EXT_V7_P[12]}]
set_property IOSTANDARD LVDS [get_ports {EXT_V7_P[12]}]
set_property DIFF_TERM FALSE [get_ports {EXT_V7_P[12]}]

set_property PACKAGE_PIN L28 [get_ports {EXT_V7_P[13]}]
set_property IOSTANDARD LVDS [get_ports {EXT_V7_P[13]}]
set_property DIFF_TERM FALSE [get_ports {EXT_V7_P[13]}]

set_property PACKAGE_PIN N28 [get_ports {EXT_V7_P[14]}]
set_property IOSTANDARD LVDS [get_ports {EXT_V7_P[14]}]
set_property DIFF_TERM FALSE [get_ports {EXT_V7_P[14]}]

set_property PACKAGE_PIN M28 [get_ports {EXT_V7_P[15]}]
set_property IOSTANDARD LVDS [get_ports {EXT_V7_P[15]}]
set_property DIFF_TERM FALSE [get_ports {EXT_V7_P[15]}]

set_property PACKAGE_PIN P29 [get_ports {EXT_V7_P[16]}]
set_property IOSTANDARD LVDS [get_ports {EXT_V7_P[16]}]
set_property DIFF_TERM FALSE [get_ports {EXT_V7_P[16]}]

set_property PACKAGE_PIN T28 [get_ports {EXT_V7_P[17]}]
set_property IOSTANDARD LVDS [get_ports {EXT_V7_P[17]}]
set_property DIFF_TERM FALSE [get_ports {EXT_V7_P[17]}]

#set_property PACKAGE_PIN N27 [get_ports MMCX_U56]
#set_property IOSTANDARD LVCMOS18 [get_ports MMCX_U56]
#set_property PACKAGE_PIN M27 [get_ports MMCX_U57]
#set_property IOSTANDARD LVCMOS18 [get_ports MMCX_U57]




#create_pblock ddr_wrapper_area
#add_cells_to_pblock [get_pblocks ddr_wrapper_area] [get_cells -quiet [list L1TopoProcessor1/TRANSMITTERS_WRAPPER_INST]]
#resize_pblock [get_pblocks ddr_wrapper_area] -add {SLICE_X44Y195:SLICE_X59Y229}


#create_pblock rod_pblock
#resize_pblock rod_pblock -add {SLICE_X110Y209:SLICE_X177Y314 DSP48_X8Y84:DSP48_X14Y125 RAMB18_X7Y84:RAMB18_X11Y125 RAMB36_X7Y42:RAMB36_X11Y62} 
#add_cells_to_pblock rod_pblock [get_cells [list {L1TopoProcessor3/L1TOPO_TO_DDR_INST/*}]]
##############################################################################################
## timing contraints
##############################################################################################
#relax timing to and from/to muon interface to/from 40/80 MHz 
set_false_path -from [get_clocks sysclk40_clk_wiz_sysclk] -to [get_clocks MGT2_CLK]
set_false_path -from [get_clocks sysclk40_clk_wiz_sysclk] -to [get_clocks MGT4_CLK]
set_false_path -from [get_clocks sysclk80_clk_wiz_sysclk] -to [get_clocks MGT2_CLK]
set_false_path -from [get_clocks sysclk80_clk_wiz_sysclk] -to [get_clocks MGT4_CLK]
set_false_path -from [get_clocks MGT2_CLK] -to [get_clocks sysclk40_clk_wiz_sysclk]
set_false_path -from [get_clocks MGT4_CLK] -to [get_clocks sysclk40_clk_wiz_sysclk]
set_false_path -from [get_clocks MGT2_CLK] -to [get_clocks sysclk80_clk_wiz_sysclk]
set_false_path -from [get_clocks MGT4_CLK] -to [get_clocks sysclk80_clk_wiz_sysclk]

#relax timing to and from/to muon interface to/from 40/80 MHz 
set_false_path -from [get_clocks MUON_GTH_CLK] -to [get_clocks sysclk40_clk_wiz_sysclk]
set_false_path -from [get_clocks MUON_GTH_CLK] -to [get_clocks sysclk80_clk_wiz_sysclk]
set_false_path -from [get_clocks {sysclk40_clk_wiz_sysclk}] -to [get_clocks {MUON_GTH_CLK}] 
set_false_path -from [get_clocks {sysclk80_clk_wiz_sysclk}] -to [get_clocks {MUON_GTH_CLK}] 

set_false_path  -from [get_clocks ROD_GTH_CLK] -to [get_clocks sysclk80_clk_wiz_sysclk] 
set_false_path  -from [get_clocks sysclk80_clk_wiz_sysclk] -to [get_clocks ROD_GTH_CLK] 

set_false_path -from {L1TopoProcessor*/CTRL/slaves/slave*/*/*}
set_false_path -to {L1TopoProcessor*/CTRL/slaves/slave*_ROD_Debug/*/*}

set_false_path -from {L1TopoProcessor*/RTDP/cpllPowerUp_reg*/*}      
#set_false_path -from {L1TopoProcessor*/RTDP/softReset_reg*/*}        
set_false_path -from {L1TopoProcessor*/RTDP/rxbufreset_reg*/*}       
set_false_path -from {L1TopoProcessor*/RTDP/errorCounterReset_reg*/*}
set_false_path -from {L1TopoProcessor*/RTDP/manualFineDelay_reg*/*}  
set_false_path -from {L1TopoProcessor*/RTDP/coarseDelay_reg*/*}      
set_false_path -from {L1TopoProcessor*/RTDP/rxpolarity_reg*/*}       
set_false_path -from {L1TopoProcessor*/RTDP/crateNumber_reg*/*}      
set_false_path -from {L1TopoProcessor*/RTDP/showChannel_reg*/*}      
#set_false_path -from {L1TopoProcessor*/RTDP/showComma_reg*/*}        

set_false_path -from {L1TopoProcessor*/RTDP/topoInput_enablePlayback_reg_reg*/*}
set_false_path -from {L1TopoProcessor*/RTDP/disableCRCdataZeroing*/*}

set_multicycle_path -from [get_pins {L1TopoProcessor*/CTRL/slaves/*/*/*}] -to [get_pins {L1TopoProcessor*/*}] 2 -hold
set_multicycle_path -from [get_pins {L1TopoProcessor*/CTRL/slaves/*/*/*}] -to [get_pins {L1TopoProcessor*/*/*}] 2 -hold
set_multicycle_path -from [get_pins {L1TopoProcessor*/CTRL/slaves/*/*/*}] -to [get_pins {L1TopoProcessor*/*/*/*}] 2 -hold
set_multicycle_path -from [get_pins {L1TopoProcessor*/CTRL/slaves/*/*/*}] -to [get_pins {L1TopoProcessor*/*/*/*/*}] 2 -hold
set_multicycle_path -from [get_pins {L1TopoProcessor*/CTRL/slaves/*/*/*}] -to [get_pins {L1TopoProcessor*/*/*/*/*/*}] 2 -hold
set_multicycle_path -from [get_pins {L1TopoProcessor*/CTRL/slaves/*/*/*}] -to [get_pins {L1TopoProcessor*/*/*/*/*/*/*}] 2 -hold
set_multicycle_path -from [get_pins {L1TopoProcessor*/CTRL/slaves/*/*/*}] -to [get_pins {L1TopoProcessor*/*/*/*/*/*/*/*}] 2 -hold

set_multicycle_path -from [get_pins {L1TopoProcessor*/L1TOPO_TO_DDR_INST/*}] -to [get_pins {L1TopoProcessor*/CTRL/slaves/*/*/*}] 2 -hold 
set_multicycle_path -from [get_pins {L1TopoProcessor*/L1TOPO_TO_DDR_INST/*/*}] -to [get_pins {L1TopoProcessor*/CTRL/slaves/*/*/*}] 2 -hold
set_multicycle_path -from [get_pins {L1TopoProcessor*/L1TOPO_TO_DDR_INST/*/*/*}] -to [get_pins {L1TopoProcessor*/CTRL/slaves/*/*/*}] 2 -hold
set_multicycle_path -from [get_pins {L1TopoProcessor*/L1TOPO_TO_DDR_INST/*/*/*/*}] -to [get_pins {L1TopoProcessor*/CTRL/slaves/*/*/*}] 2 -hold


set_multicycle_path -from [get_pins {L1TopoProcessor*/RTDP/ChannelStatus_reg*}] -to [get_pins {L1TopoProcessor*/CTRL/slaves/*/*/*}] 2 -hold 

###$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$29.03.2016
#set_multicycle_path -from [get_pins {L1TopoProcessor*/RTDP/rxdata128_deserialized_reg*/*}] -to [get_pins {L1TopoProcessor*/SHIFT_DATA_FROM_CMX*/*}] 2 -setup -start
#set_multicycle_path -from [get_pins {L1TopoProcessor*/RTDP/rxdata128_deserialized_reg*/*}] -to [get_pins {L1TopoProcessor*/SHIFT_DATA_FROM_CMX*/*}] 1 -hold 

#set_multicycle_path -to [get_pins {L1TopoProcessor*/*err_in*}] 2 -hold 


#playback
#this only for time being to relax timing !!!!!!!!!!!!!!
set_multicycle_path -from [get_nets {L1TopoProcessor*/CTRL/slaves/slave*/BRAM_GEN*.EN_ACTIVE_ONLY.BRAM/douta*}] -to [get_pins {L1TopoProcessor*/RTDP/rxdata128_deserialized_reg*/*}] 2 -setup 
set_multicycle_path -from [get_nets {L1TopoProcessor*/CTRL/slaves/slave*/BRAM_GEN*.EN_ACTIVE_ONLY.BRAM/douta*}] -to [get_pins {L1TopoProcessor*/RTDP/rxdata128_deserialized_reg*/*}] 1 -hold -end 

#set_multicycle_path  -from [get_cells {L1TopoProcessor*/L1TOPO_TO_DDR_INST/SAVE_INPUT_ROS_ROI_DATA*.SELECT_ACTIVE_LINKS.RING_BUFF_128B_512W_ROS_INST/U0/inst_blk_mem_gen/gnativebmg.native_blk_mem_gen/valid.cstr/ramloop[0].ram.r/prim_noinit.ram/DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram}] -to [get_pins {L1TopoProcessor*/L1TOPO_TO_DDR_INST/data_to_fifo*/*}] 2 -setup
#set_multicycle_path  -from [get_cells {L1TopoProcessor*/L1TOPO_TO_DDR_INST/SAVE_INPUT_ROS_ROI_DATA*.SELECT_ACTIVE_LINKS.RING_BUFF_128B_512W_ROS_INST/U0/inst_blk_mem_gen/gnativebmg.native_blk_mem_gen/valid.cstr/ramloop[0].ram.r/prim_noinit.ram/DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM36_NO_ECC.ram}] -to [get_pins {L1TopoProcessor*/L1TOPO_TO_DDR_INST/data_to_fifo*/*}] 1 -hold
###$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$03.04.2016





