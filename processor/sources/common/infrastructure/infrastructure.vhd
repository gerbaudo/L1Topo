----------------------------------------------------------------------------------
-- Company:     Johannes Gutenberg - Universitaet Mainz, Jagiellonian University
-- Engineer:    Christian Kahra, Marek Palka
-- 
-- Create Date: 02/08/2015 09:28:41 PM
-- Design Name: 
-- Module Name: infrastructure - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity infrastructure is port(
    pll_reset:              in  std_logic;    
    gck1:                   in  std_logic;     -- using the PLL of the same bank as the input pins
    gck2_bufg:              in  std_logic;     -- routed on BUFG to other PLL
    sysclk40:               out std_logic;
    sysclk80:               out std_logic;
    sysclk160:              out std_logic;
    sysclk320:              out std_logic;
    sysclk400:              out std_logic;
    rodclk400_io:           out std_logic;
    rodclk400_r:            out std_logic;
    rodclk80:               out std_logic;
    sysclk_pll_locked:      out std_logic;
    crystalclk40:           out std_logic;
    crystalclk62_5:         out std_logic;
    crystalclk200:          out std_logic;
    crystalclk_pll_locked:  out std_logic;
    idelayctrlReset:        out std_logic;
    xadc_control:           in  std_logic_vector(25 downto 0);
    xadc_status:            out std_logic_vector(22 downto 0);
    ttcBridge:              in  std_logic;
    ttcBridge_control:      in  std_logic_vector( 5 downto 0);
    ttcBridge_status:       out std_logic_vector(14 downto 0);
    ttcL1Accept:            out std_logic;
    ttcBroadcast:           out std_logic;
    ttcBunchCounterReset:   out std_logic;
    usr_access:             out std_logic_vector(31 downto 0);
    clk_control:            in  std_logic;
    clk_status:             out std_logic_vector( 2 downto 0)
    
    
    
);
end infrastructure;

architecture Behavioral of infrastructure is
	
	component clk_wiz_sysclk
		port(
                        reset             : in  STD_LOGIC;
			gck1              : in  STD_LOGIC;
			sysclk40          : out STD_LOGIC;
			sysclk80          : out STD_LOGIC;
			sysclk160         : out STD_LOGIC;
			sysclk320         : out STD_LOGIC;
			sysclk400         : out STD_LOGIC;
			sysclk62_5        : out STD_LOGIC;
			sysclk_pll_locked : out STD_LOGIC
		);
	end component;

	component clk_wiz_crystalclk
		port(
			gck2                  : in  STD_LOGIC;
			crystalclk40          : out STD_LOGIC;
			crystalclk62_5        : out STD_LOGIC;
			crystalclk200         : out STD_LOGIC;
			crystalclk_pll_locked : out STD_LOGIC
		);
	end component;

	component rod_clk
		port(
 			-- Clock in ports
			clk40     : in  std_logic;
			clkfb_in  : in  std_logic;
			-- Clock out ports
			clk400_io : out std_logic;
			clk400_r  : out std_logic;
			clk80     : out std_logic;
			clkfb_out : out std_logic;
			-- Status and control signals
			reset     : in  std_logic;
			locked    : out std_logic
		);
	end component;
        
	
    signal sysclk40_i, sysclk80_i, sysclk400_i: std_logic; 
    signal sysclk_pll_locked_i, sysclk_pll_locked_sync: std_logic;
    signal crystalclk_pll_locked_i: std_logic;
    signal idelayctrlResetCounter: std_logic_vector(1 downto 0);
    signal rodclkfb_out, rodclk_locked : std_logic;
    signal rodclk400_io_i:   std_logic;
    signal rodclk400_r_i:    std_logic;
    signal rodclk80_i:       std_logic;
    signal internal_pll_reset, internal_pll_reset_sync: std_logic;
    signal internal_pll_reset_sync_a, internal_pll_reset_sync_b: std_logic;
    signal pll_reset_delayed : std_logic:='0';    
begin

   DELAY_PLL_RESET: entity work.delay
    generic map (
      VECTOR_WIDTH => 1,
      DELAY_INT    => 50)
    port map (
      CLK              => sysclk40_i,
      DELAY_VECTOR_IN(0)  => pll_reset,
      DELAY_VECTOR_OUT(0) => pll_reset_delayed);
  
MakeRstPulse: process(sysclk40_i)
begin
  if sysclk_pll_locked_i = '0' then
    internal_pll_reset_sync_a <= '1';
    internal_pll_reset_sync_b <= '1';
    internal_pll_reset <= '0';
  elsif rising_edge(sysclk40_i) then
    if internal_pll_reset_sync_a = '1' and internal_pll_reset_sync_b = '0' then
      internal_pll_reset_sync_a <= pll_reset_delayed;
      internal_pll_reset_sync_b <= internal_pll_reset_sync_a;
      internal_pll_reset <= '1';
    else
      sysclk_pll_locked_sync <= sysclk_pll_locked_i;
      internal_pll_reset_sync_a <= pll_reset_delayed;
      internal_pll_reset_sync_b <= internal_pll_reset_sync_a;
      internal_pll_reset <= '0';
    end if;
  end if;
end process MakeRstPulse;



sysclk: clk_wiz_sysclk
port map(
   reset                => internal_pll_reset,
   gck1                 => gck1,
   sysclk40             => sysclk40_i,
   sysclk80             => sysclk80_i,
   sysclk160            => sysclk160,
   sysclk320            => sysclk320,
   sysclk400            => sysclk400_i,
   sysclk62_5           => open,
   sysclk_pll_locked    => sysclk_pll_locked_i      
);


    sysclk40            <= sysclk40_i;
    sysclk80            <= sysclk80_i;
    sysclk400           <= sysclk400_i;
    sysclk_pll_locked   <= sysclk_pll_locked_i and rodclk_locked;
    
    
    

crystalclk: clk_wiz_crystalclk
port map(
    gck2                    => gck2_bufg,
    crystalclk40            => crystalclk40,
    crystalclk62_5          => crystalclk62_5,
    crystalclk200           => crystalclk200,
    crystalclk_pll_locked   => crystalclk_pll_locked_i
);

    crystalclk_pll_locked <= crystalclk_pll_locked_i;



-- 26th January 2016, Christian Kahra: Who has commented out the xadc??? Why??? 
-- Without xadc we don't have anymore temperature and voltage monitoring!!
-- The L1Calo software should run amok!!
-- Or was the monitoring in the L1Calo software also disabled? Why? 

xadc: entity work.read_out_xadc
port map(
    drpclkin    => sysclk40_i,
    drpadress   => xadc_control( 6 downto  0),
    drp_control => xadc_control( 9 downto  7),
    drp_input   => xadc_control(25 downto 10),
    drp_output  => xadc_status(15 downto 0),
    drp_ready   => xadc_status(16),
    drp_alarms  => xadc_status(22 downto 17)
);




-- 26th January 2016, Christian Kahra: Change in the forwarding of ttc signals
-- Up to now, the ttcL1Accept, the ttcBunchCounterReset and the reception of the ttcBroadcast "01" were serialized on the ttcBridge-line
-- From now on, only the reception of the ttcBroadcast "01" will be signaled as 40MHz pulsed on the ttcBridge-line
-- Therefore the ttcBridge deserialization module was commented out and the TTC_BROADCAST_PROCESS process samples this line with sysclk40
-- The unused ttcL1Accept and ttcBunchCounterReset signals are tied to '0'


--ttc: entity work.ttcBridge
--port map(
--    sysclk40              => sysclk40_i,
--    sysclk80              => sysclk80_i,
--    sysclk_pll_locked     => sysclk_pll_locked_i,
--    serialData            => ttcBridge,
--    control               => ttcBridge_control,
--    status                => ttcBridge_status,
--    ttcL1Accept           => ttcL1Accept,
--    ttcBroadcast          => ttcBroadcast,
--    ttcBunchCounterReset  => ttcBunchCounterReset 
--);


-- TTC short broadcast to synchronize the playback-/spy-memories
-- ttcBridge was sent by controller fpga on the falling-edge of its sysclk40.
-- Sampling it on the rising-edge defines a 12.5 ns delay to stay away from the data eye edge
--  (should be more than enough to take care of the phase between controller and processor clocks and the transmission period) 

TTC_BROADCAST_PROCESS: process(sysclk40_i) begin
if rising_edge(sysclk40_i) then
    ttcBroadcast <= ttcBridge;      
end if;
end process TTC_BROADCAST_PROCESS;


    ttcL1Accept <= '0';
    ttcBunchCounterReset <= '0';

-- end of "Change in the forwarding of ttc signals"



process(sysclk40_i) begin
    if rising_edge(sysclk40_i) then
        if sysclk_pll_locked_i='0' then
            idelayctrlReset <= '1';
            idelayctrlResetCounter <= (others => '0');
        else
            if idelayctrlResetCounter = "11" then
                idelayctrlReset <= '0';
                idelayctrlResetCounter <= idelayctrlResetCounter;
            else
                idelayctrlReset <= '1';
                idelayctrlResetCounter <= std_logic_vector( unsigned(idelayctrlResetCounter) + 1);
            end if;
        end if;
    end if;
end process;

------------------------------------
--rod ddr clocks
------------------------------------
--rod_clk_1: rod_clk
--  port map (
--    clk40     => gck1,
--    clkfb_in  => rodclkfb_out,
--    clk400_io => rodclk400_io_i,
--    clk400_r  => rodclk400_r_i,
--    clk80     => rodclk80_i,    
--    clkfb_out => rodclkfb_out,
--    reset     => '0',
--    locked    => rodclk_locked);

--ROD_BUFIO : BUFIO
--  port map
--  (
--    I => rodclk400_io_i,
--    O => rodclk400_io
--  );

--ROD_BUFR400 : BUFR
--  port map
--  (
--    CE =>  '1',
--    CLR => '0',
--    I => rodclk400_r_i,
--    O => rodclk400_r
--  );

--ROD_BUFR80 : BUFR
--  port map
--  (
--    CE => '1',
--    CLR => '0',
--    I => rodclk80_i,
--    O => rodclk80
--  );


    rodclk80        <= sysclk80_i;
    rodclk400_r     <= sysclk400_i;
    rodclk400_io    <= sysclk400_i;
    rodclk_locked   <= sysclk_pll_locked_i;


USR_ACCESSE2_inst : USR_ACCESSE2
   port map (
      CFGCLK => open,
      DATA => usr_access,
      DATAVALID => open
   );




process(sysclk40_i, sysclk_pll_locked_i, crystalclk_pll_locked_i, rodclk_locked) begin

if sysclk_pll_locked_i='0' then clk_status(0) <= '1';
elsif rising_edge(sysclk40_i) and clk_control='1' then clk_status(0) <= '0';
end if;

if crystalclk_pll_locked_i='0' then clk_status(1) <= '1';
elsif rising_edge(sysclk40_i) and clk_control='1' then clk_status(1) <= '0';
end if;

if rodclk_locked='0' then clk_status(2) <= '1';
elsif rising_edge(sysclk40_i) and clk_control='1' then clk_status(2) <= '0';
end if;
 
end process;


end Behavioral;


