------------------------------------------------------------------------------------------
-- Author/Modified by : manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de
-- Description      : applies eta cut to JetTOB list, sorts TOBs,
--              converts them to GenericTOBs and returns short sorted list
-- Number of registers  : 2
------------------------------------------------------------------------------------------

--------------
-- includes --
--------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;
use work.L1TopoDataTypes.all;
use work.L1TopoFunctions.all;
use work.HistogrammingConstants.all;

------------------------
-- entity declaration --
------------------------

entity JetSortEtaDijet is
  generic(InputWidth         : integer := InputWidthJET;  --number of input TOBs
          InputWidth1stStage : integer := InputWidth1stStageSelectJET;  --number of TOBs in 1st stage
          OutputWidth        : integer := OutputWidthSelectJET;  --number of selected output TOBs
          JetSize            : integer := DefaultJetSize  --jet Et bit width (1 = 9 bit, 2 = 10 bit)
          );
  port(ClockBus    : in  std_logic_vector(2 downto 0);
       Parameters  : in  ParameterArray;
       JetTobArray : in  JetArray(InputWidth - 1 downto 0);
       TobArrayOut : out ExtendedTobArray(OutputWidth - 1 downto 0)
       );
end JetSortEtaDijet;

-----------------------------------------
-- behavioral description of algorithm --
-----------------------------------------

architecture Behavioral of JetSortEtaDijet is

  ---------------
  -- constants --
  ---------------
  constant JetPresortedLength                   : integer := 30;
  constant OutputWidth1stStagePresorted         : integer := 10;
  constant JetPresortedLengthExtended           : integer := 33;
  constant OutputWidth1stStagePresortedExtended : integer := 11;
  constant InputWidthExtended                   : integer := InputWidth + 2;

  -----------
  -- types --
  -----------
  type vector_output_width is array (integer range <>) of std_logic_vector(OutputWidth1stStagePresortedExtended - 1 downto 0);

  -------------
  -- signals --
  -------------
  -- parameters:tob_in
  signal MinEt                           : std_logic_vector(GenericEtBitWidth - 1 downto 0);
  signal MinEta                          : std_logic_vector(GenericAbsoluteEtaBitWidth - 1 downto 0);
  signal MaxEta                          : std_logic_vector(GenericAbsoluteEtaBitWidth - 1 downto 0);
  -- selected input TOBs (Eta):
  signal tob_in                          : JetArray(InputWidth - 1 downto 0);
  signal generic_in                      : JetArray(InputWidth - 1 downto 0);
  signal select_pattern                  : std_logic_vector(InputWidth - 1 downto 0);
  signal select_pattern_extended         : std_logic_vector(InputWidthExtended - 1 downto 0);
  signal generic_in_cut                  : JetArray(InputWidth - 1 downto 0);
  signal generic_in_cut_extended         : JetArray(InputWidthExtended - 1 downto 0);
  -- selected TOBs after first stage (Et threshold):
  signal out_1stStage                    : JetArray((OutputWidth * ((InputWidthExtended) / JetPresortedLengthExtended)) - 1 downto 0);
  signal out_1stStage_short              : JetArray((OutputWidth * ((InputWidthExtended) / JetPresortedLengthExtended)) - 1 downto 0);
  signal reg_out_1stStage                : JetArray((OutputWidth * ((InputWidthExtended) / JetPresortedLengthExtended)) - 1 downto 0);
  -- intermediate results from first stage (used for second stage):
  signal cmp_res_1stStage                : std_logic_vector((OutputWidth * ((InputWidthExtended) / JetPresortedLengthExtended)) - 1 downto 0);
  signal cmp_res_1stStage_reg            : std_logic_vector((OutputWidth * ((InputWidthExtended) / JetPresortedLengthExtended)) - 1 downto 0);
  -- dummy (helper) signals:
  signal cmp_res_1stStage_reg_dmy        : std_logic_vector((OutputWidth * ((InputWidthExtended) / JetPresortedLengthExtended)) - 1 + OutputWidth downto 0);
  signal cmp_res_1stStage_reg_dmy_zeroes : std_logic_vector(OutputWidth - 1 downto 0) := (others => '0');
  signal reg_out_1stStage_dmy            : JetArray((OutputWidth * (InputWidthExtended / JetPresortedLengthExtended)) - 1 + OutputWidth downto 0);
  signal reg_out_1stStage_dmy_empty      : JetArray(OutputWidth - 1 downto 0);
  -- selected TOBs after second stage
  signal reg_out_2ndStage                : JetArray(OutputWidth - 1 downto 0);
  signal reg_out_3                       : JetArray(OutputWidth - 1 downto 0);
  -- overflow bit after second stage
  signal reg_over_2ndStage               : std_logic;
  signal reg_over_final                  : std_logic;
  signal overflow_reg1, overflow_reg2    : std_logic;
  signal overflow_all_cmx                : std_logic;  -- TODO: unused
  signal overflow1stStage                : std_logic_vector(((InputWidthExtended) / JetPresortedLengthExtended) - 1 downto 0);
  signal select_pattern_out              : vector_output_width(((InputWidthExtended) / JetPresortedLengthExtended) - 1 downto 0);
  signal ParametersRemap                 : ParameterArray;
  signal TobArrayOut_tmp                 : ExtendedTobArray(OutputWidth - 1 downto 0);

begin
  tob_in <= JetTobArray;

  ParametersRemap(0) <= (others => '0');
  ParametersRemap(1) <= Parameters(0);
  ParametersRemap(2) <= Parameters(1);

  gen_extend_1 : for i in 31 downto 0 generate
    generic_in_cut_extended(i) <= tob_in(i);
  --    select_pattern_extended(i) <= select_pattern(i);
  end generate;

  gen_extend_2 : for i in 31 downto 0 generate
    generic_in_cut_extended(33 + i) <= tob_in(32 + i);
  --    select_pattern_extended(33 + i) <= select_pattern(32 + i);
  end generate;

  generic_in_cut_extended(32) <= emptyJetTOB;
  generic_in_cut_extended(65) <= emptyJetTOB;

  ------------------------------------------------------------------------
  -- first stage: select the input TOBs in groups of InputWidth1stStage --
  ------------------------------------------------------------------------

  gen1stStage : for i in 0 to InputWidthExtended / JetPresortedLengthExtended - 1 generate
    selection_1stStage : entity work.SelectMergePresortedJetDijet
      generic map(
        InputWidthBlocks   => 4,
        InputWidthPerBlock => 8,
        OutputWidth        => OutputWidth,
        JetSize            => JetSize,
        DoEtaCut           => 1,
        UseOutClk40        => 1
        )
      port map(
        clk40              => ClockBus(0),
        clkFast            => ClockBus(2),
        Parameters         => ParametersRemap,
        inp                => tob_in((i + 1) * 32 - 1 downto i * 32),
        select_pattern_in  => select_pattern((i + 1) * 32 - 1 downto i * 32),
        oup                => out_1stStage((i + 1) * (OutputWidth) - 1 downto i * (OutputWidth)),
        select_pattern_out => open,
        overflow           => open
        );
  end generate;

  gen_short_1 : for j in InputWidthExtended / JetPresortedLengthExtended - 1 downto 0 generate
    gen_short_2 : for i in OutputWidth - 1 downto 0 generate
      out_1stStage_short(((InputWidthExtended / JetPresortedLengthExtended - 1) - j + 1) * OutputWidth - i - 1) <= out_1stStage(j * OutputWidth + i);
    end generate;
  end generate;

  reg_out_1stStage <= out_1stStage_short;

  -------------------------------------------------
  -- second stage: merge the first-stage outputs --
  -------------------------------------------------

  sort2ndStage : entity work.TobSortDijet
    generic map(OutputWidth => OutputWidth,
                InputWidth  => (InputWidth / 32) * OutputWidth,
                DoPresort   => 0,
                JetSize     => JetSize
                )
    port map(Tob         => reg_out_1stStage,
             TobArrayOut => reg_out_2ndStage
             );

  ---------------------------------------------
  -- register the output of the second stage --
  ---------------------------------------------

  reg_out_3 <= reg_out_2ndStage when rising_edge(ClockBus(0)) else reg_out_3;

  --reg_2nd_stage : entity work.TobRegister
  --  generic map(InputWidth => OutputWidth
  --  )
  --  Port map(clk     => ClockBus(0),
  --         enable  => '1',
  --         reg_in  => reg_out_2ndStage,
  --         reg_out => reg_out_3
  --  );

  ----------------------------------------------------------
  -- register the output and overflow of the second stage --
  ----------------------------------------------------------

  -- workaround to set overflow bit
  gen_set_overflow : for i in 0 to OutputWidth - 1 generate
    TobArrayOut_tmp(i)    <= to_ExtendedgenericTOB(reg_out_3(i), JetSize);
    TobArrayOut(i).Et     <= TobArrayOut_tmp(OutputWidth - 1 - i).Et;
    TobArrayOut(i).Eta    <= TobArrayOut_tmp(OutputWidth - 1 - i).Eta;
    TobArrayOut(i).Phi    <= TobArrayOut_tmp(OutputWidth - 1 - i).Phi;
    TobArrayOut(i).Et1    <= TobArrayOut_tmp(OutputWidth - 1 - i).Et1;
    TobArrayOut(i).Et2    <= TobArrayOut_tmp(OutputWidth - 1 - i).Et2;
    TobArrayOut(i).EtaBin <= TobArrayOut_tmp(OutputWidth - 1 - i).EtaBin;
    TobArrayOut(i).PhiBin <= TobArrayOut_tmp(OutputWidth - 1 - i).PhiBin;
  end generate;

  --set Overflow
  gen_overflow_reg1 : entity work.StdLogicRegister
    port map(clk     => ClockBus(0),
             enable  => '1',
             reg_in  => JetTobArray(0).Overflow,
             reg_out => overflow_reg1
             );

  gen_overflow_reg2 : entity work.StdLogicRegister
    port map(clk     => ClockBus(0),
             enable  => '1',
             reg_in  => overflow_reg1,
             reg_out => overflow_reg2
             );

  TobArrayOut(0).Overflow <= overflow_reg2;

  -- only needed for simulation
  gen_set_dummy_overflow : for i in 1 to OutputWidth - 1 generate
    TobArrayOut(i).Overflow <= '0';
  end generate;

end Behavioral;
