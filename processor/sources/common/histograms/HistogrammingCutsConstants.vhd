------------------------------------------------------------------------------------------
-- Author/Modified by : Johannes Damp (johannes.frederic.damp@cern.ch)
-- Description      : constants for the dijet analysis cuts module
-- created        : 08.03.2017
------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.L1TopoDataTypes.all;

package HistogrammingCutsConstants is
  constant yStarBitWidth : integer := 7;  -- TODO Should be equal to GenericEtaBitWidth, maybe import L1DataTypes?

  constant NumberOfJetEtaCategories   : integer := 3;  -- categories per jet
  constant NumberOfTotalEtaCategories : integer := 6;  -- total possible categories for an event
  constant NumberOfYStarCategories    : integer := 3;

  type etaBitMatrix is array(NumberOfJetEtaCategories - 1 downto 0) of std_logic_vector(NumberOfJetEtaCategories - 1 downto 0);
  type resultBitMatrix is array(NumberOfYStarCategories - 1 downto 0) of std_logic_vector(NumberOfTotalEtaCategories - 1 downto 0);
  type cutValuesYStar is array (integer range <>) of unsigned(yStarBitWidth - 1 downto 0);
  type cutValuesEta is array (integer range <>) of unsigned(GenericEtaBitWidth - 1 downto 0);


  constant yStarSmallIndex  : integer := 0;  -- y* < 0.3
  constant yStarMediumIndex : integer := 1;  -- 0.3 < y* < 0.6
  constant yStarHighIndex   : integer := 2;  -- 0.6 < y*

  constant yStarSmallLowerCut  : unsigned(yStarBitWidth - 1 downto 0) := to_unsigned(0, yStarBitWidth);
  constant yStarSmallUpperCut  : unsigned(yStarBitWidth - 1 downto 0) := to_unsigned(3, yStarBitWidth);
  constant yStarMediumLowerCut : unsigned(yStarBitWidth - 1 downto 0) := to_unsigned(3, yStarBitWidth);
  constant yStarMediumUpperCut : unsigned(yStarBitWidth - 1 downto 0) := to_unsigned(6, yStarBitWidth);
  constant yStarHighLowerCut   : unsigned(yStarBitWidth - 1 downto 0) := to_unsigned(6, yStarBitWidth);
  constant yStarHighUpperCut   : unsigned(yStarBitWidth - 1 downto 0) := to_unsigned(99, yStarBitWidth);


  constant CentralDetectorIndex  : integer := 0;
  constant ExtendedDetectorIndex : integer := 1;
  constant ForwardDetectorIndex  : integer := 2;

  constant JetsBothCentralDetectorIndex   : integer := 0;
  constant JetsBothExtendedDetectorIndex  : integer := 1;
  constant JetsBothForwardDetectorIndex   : integer := 2;
  constant JetsOneCentralOneExtendedIndex : integer := 3;
  constant JetsOneCentralOneForwardIndex  : integer := 4;
  constant JetsOneExtendedOneForwardIndex : integer := 5;

  constant EtaCentralLowerCut  : unsigned(GenericEtaBitWidth - 1 downto 0) := to_unsigned(0, GenericEtaBitWidth);
  constant EtaCentralUpperCut  : unsigned(GenericEtaBitWidth - 1 downto 0) := to_unsigned(20, GenericEtaBitWidth);
  constant EtaExtendedLowerCut : unsigned(GenericEtaBitWidth - 1 downto 0) := to_unsigned(20, GenericEtaBitWidth);
  constant EtaExtendedUpperCut : unsigned(GenericEtaBitWidth - 1 downto 0) := to_unsigned(30, GenericEtaBitWidth);
  constant EtaForwardLowerCut  : unsigned(GenericEtaBitWidth - 1 downto 0) := to_unsigned(30, GenericEtaBitWidth);
  constant EtaForwardUpperCut  : unsigned(GenericEtaBitWidth - 1 downto 0) := to_unsigned(99, GenericEtaBitWidth);



  constant MaxJetEt : std_logic_vector(GenericEtBitWidth - 1 downto 0) := (others => '1');


end package HistogrammingCutsConstants;

package body HistogrammingCutsConstants is
end package body HistogrammingCutsConstants;
