------------------------------------------------------------------------------------------
-- Author/Modified by : Sebastian Artz (sebastian.artz@cern.ch)
-- Description        : Store InputData in histogram.
--                      Once histogram ins recorded it is buffered while a new one is recorded
-- created            : 17.01.2017
-- 
------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.L1TopoDataTypes.all;
use work.L1TopoFunctions.all;
use work.ipbus.all;
use work.l1topo_package.all;
use work.HistogrammingConstants.all;

entity HistogramBram is
  generic(
    BramAddrWidth : integer := 9;
    NumBins       : integer := 0;
    BinBitWidth   : integer := DijetAnaBinBitWidth
    );
  port(
    ClockBus         : in  std_logic_vector(2 downto 0);
    InputAddress     : in  std_logic_vector(BramAddrWidth - 1 downto 0);
    EventVeto        : in  std_logic;
    Accept           : in  std_logic;
    SwapHistograms   : in  std_logic;
    ipbus_in         : in  ipb_wbus;
    enableWriteIPbus : in  std_logic;
    ipbus_out        : out ipb_rbus
    );
end entity HistogramBram;

architecture Behavioral of HistogramBram is

  -- constants

  constant ControlSignalDelay : integer              := 3;
--  constant BramAddrWidth      : integer              := ld(NumBins);
  constant cntStart           : unsigned(1 downto 0) := "11";

  -- types

  type BinSLV is array (natural range <>) of std_logic_vector(BinBitWidth - 1 downto 0);
  type AddrSLV is array (natural range <>) of std_logic_vector(BramAddrWidth - 1 downto 0);
  type arraySLV1 is array (natural range <>) of std_logic_vector(0 downto 0);

  -- signals

  signal currentEntry : unsigned(BinBitWidth - 1 downto 0);

  signal binRead, binWrite                                          : std_logic_vector(BinBitWidth - 1 downto 0);
  signal addrRead, addrWrite                                        : std_logic_vector(BramAddrWidth - 1 downto 0);
  signal binReadIPbus, binReadIPbus_reg, binWriteIPbus              : std_logic_vector(BinBitWidth - 1 downto 0);
  signal addrReadIPbus, addrWriteIPbus                              : std_logic_vector(BramAddrWidth - 1 downto 0);
  signal binReadSLV, binWriteSLV                                    : BinSLV(2 - 1 downto 0);
  signal addrReadSLV, addrWriteSLV                                  : AddrSLV(2 - 1 downto 0);
  signal activeHistIndex                                            : integer range 0 to 1                         := 1;
  signal DoRecording                                                : std_logic                                    := '0';
  signal bufferHistogram, bufferHistogram_reg, bufferHistogram_reg2 : std_logic                                    := '0';
  signal subtickCtr                                                 : unsigned(1 downto 0)                         := "11";
  signal toggle40, toggle160, toggle160synch, resetClk              : std_logic;
  signal addrCtr                                                    : std_logic_vector(BramAddrWidth - 1 downto 0) := (others => '0');
  signal recordCtr                                                  : unsigned(3 - 1 downto 0)                     := "101";
  signal enableBram                                                 : std_logic                                    := '1';
  signal writeBram                                                  : std_logic                                    := '0';
  signal readBram                                                   : std_logic                                    := '0';
  signal resetBram                                                  : std_logic                                    := '0';
  signal enableBramIPbus                                            : std_logic                                    := '1';
  signal writeBramIPbus                                             : std_logic                                    := '0';
  signal readBramIPbus                                              : std_logic                                    := '0';
  signal resetBramIPbus                                             : std_logic                                    := '0';
  signal enableBramSLV                                              : std_logic_vector(1 downto 0)                 := "00";
  signal readBramSLV                                                : std_logic_vector(1 downto 0)                 := "00";
--  signal resetBramSLV                                               : std_logic_vector(1 downto 0)                 := "00";
  signal writeEnableSLV                                             : arraySLV1(1 downto 0)                        := (others => (others => '0'));
  signal ctrl_sel                                                   : integer;
  signal ack                                                        : std_logic;
  signal strobe_reg, addr0_reg                                      : std_logic;
  signal strobeWriteEnable                                          : std_logic                                    := '0';

begin

  addrRead <= InputAddress;--std_logic_vector(to_unsigned(300, addrRead'length));

  -- update bin in histogram
  increase_entry : process(ClockBus(0))
  begin
    if (rising_edge(ClockBus(0))) then
      if SwapHistograms = '1' then
        activeHistIndex <= 1 - activeHistIndex;
      end if;
    end if;
  end process;

  clk_160 : process(ClockBus)
  begin
    if rising_edge(ClockBus(2)) then
      if (resetClk = '1') then
        subtickCtr <= cntStart;
      else
        subtickCtr <= subtickCtr + "01";
      end if;

      if subtickCtr = "10" then
        addrWrite <= addrRead;
        binWrite  <= std_logic_vector(unsigned(binRead) + to_unsigned(1, binRead'length));
      else
        addrWrite <= addrWrite;
        binWrite  <= binWrite;
      end if;

    end if;
  end process;

  readBram       <= '1' when subtickCtr = "01"                                                        else '0';
  writeBram      <= '1' when (subtickCtr = "11" and Accept = '1')                                     else '0';
  writeBramIPbus <= '1' when subtickCtr = "11" and strobeWriteEnable = '1' and enableWriteIPbus = '1' else '0';

  process(ClockBus(2))
  begin
    if rising_edge(ClockBus(2)) then
      toggle160      <= toggle40;
      toggle160synch <= toggle160;
    end if;
  end process;

  resetClk <= '1' when (toggle160synch /= toggle160) else '0';

  enableBram <= '1';

  readBramSLV(0)       <= readBram;
  enableBramSLV(0)     <= enableBram when activeHistIndex = 0 else enableBramIPbus;
--  resetBramSLV(0)      <= bufferHistogram_reg2 when activeHistIndex = 1 else '0';
  addrWriteSLV(0)      <= addrWrite  when activeHistIndex = 0 else addrWriteIPbus;
  binWriteSLV(0)       <= binWrite   when activeHistIndex = 0 else binWriteIPbus;
  addrReadSLV(0)       <= addrRead   when activeHistIndex = 0 else addrReadIPbus;
  writeEnableSLV(0)(0) <= writeBram  when activeHistIndex = 0 else writeBramIPbus;

  readBramSLV(1)       <= readBram;
  enableBramSLV(1)     <= enableBram when activeHistIndex = 1 else enableBramIPbus;
--  resetBramSLV(1)      <= bufferHistogram_reg2 when activeHistIndex = 0 else '0';
  addrWriteSLV(1)      <= addrWrite  when activeHistIndex = 1 else addrWriteIPbus;
  binWriteSLV(1)       <= binWrite   when activeHistIndex = 1 else binWriteIPbus;
  addrReadSLV(1)       <= addrRead   when activeHistIndex = 1 else addrReadIPbus;
  writeEnableSLV(1)(0) <= writeBram  when activeHistIndex = 1 else writeBramIPbus;

  readBramIPbus   <= '1';
  enableBramIPbus <= '1';
  resetBramIPbus  <= '0';
  addrWriteIPbus  <= ipbus_in.ipb_addr(BramAddrWidth - 1 downto 0);  --addrCtr;
  binWriteIPbus   <= (others => '0');
  --addrReadIPbus   <= addrCtr;
  addrReadIPbus   <= ipbus_in.ipb_addr(BramAddrWidth - 1 downto 0);

  binRead          <= binReadSLV(0)    when activeHistIndex = 0 else binReadSLV(1);
  binReadIPbus_reg <= binReadSLV(1)    when activeHistIndex = 0 else binReadSLV(0);
  binReadIPbus     <= binReadIPbus_reg when rising_edge(ClockBus(0));
--  HistEntryOut <= binReadIPbus  when rising_edge(ClockBus(0));

----- IPbus -----

  process(ClockBus(0))
  begin
    if rising_edge(ClockBus(0)) then

      strobe_reg <= ipbus_in.ipb_strobe;
      addr0_reg  <= ipbus_in.ipb_addr(0);

      --if reset='1' then
      --  reg <= (others=>(others=>'0'));
      --elsif ipbus_in.ipb_strobe='1' and ipbus_in.ipb_write='1' then
      --  reg(ctrl_sel) <= ipbus_in.ipb_wdata;
      --end if;

      if ipbus_in.ipb_strobe = '1' and (strobe_reg = '0' or (addr0_reg /= ipbus_in.ipb_addr(0))) then
        ack <= not ack;
      else ack <= '0';
      end if;

    end if;
  end process;

  strobeWriteEnable <= '1' when ipbus_in.ipb_strobe = '1' and (strobe_reg = '0' or (addr0_reg /= ipbus_in.ipb_addr(0))) else '0';

  ipbus_out.ipb_rdata <= binReadIPbus;
  ipbus_out.ipb_ack   <= ack;
  ipbus_out.ipb_err   <= '0';

  -- BRAM

  gen_hist102 : if NumBins = 102 generate
    bram_mjj : entity work.hist102
      port map(
        clka  => ClockBus(2),
        ena   => enableBramSLV(0),
        wea   => writeEnableSLV(0),
        addra => addrWriteSLV(0),
        dina  => binWriteSLV(0),
        clkb  => ClockBus(2),
        enb   => readBramSLV(0),
        addrb => addrReadSLV(0),
        doutb => binReadSLV(0)
        );

    bram2_mjj : entity work.hist102
      port map(
        clka  => ClockBus(2),
        ena   => enableBramSLV(1),
        wea   => writeEnableSLV(1),
        addra => addrWriteSLV(1),
        dina  => binWriteSLV(1),
        clkb  => ClockBus(2),
        enb   => readBramSLV(1),
        addrb => addrReadSLV(1),
        doutb => binReadSLV(1)
        );
  end generate;

  gen_hist120 : if NumBins = 120 generate
    bram_mjj : entity work.hist120
      port map(
        clka  => ClockBus(2),
        ena   => enableBramSLV(0),
        wea   => writeEnableSLV(0),
        addra => addrWriteSLV(0),
        dina  => binWriteSLV(0),
        clkb  => ClockBus(2),
        enb   => readBramSLV(0),
        addrb => addrReadSLV(0),
        doutb => binReadSLV(0)
        );

    bram2_mjj : entity work.hist120
      port map(
        clka  => ClockBus(2),
        ena   => enableBramSLV(1),
        wea   => writeEnableSLV(1),
        addra => addrWriteSLV(1),
        dina  => binWriteSLV(1),
        clkb  => ClockBus(2),
        enb   => readBramSLV(1),
        addrb => addrReadSLV(1),
        doutb => binReadSLV(1)
        );
  end generate;

  gen_hist128 : if NumBins = 128 generate
    bram_mjj : entity work.hist128
      port map(
        clka  => ClockBus(2),
        ena   => enableBramSLV(0),
        wea   => writeEnableSLV(0),
        addra => addrWriteSLV(0),
        dina  => binWriteSLV(0),
        clkb  => ClockBus(2),
        enb   => readBramSLV(0),
        addrb => addrReadSLV(0),
        doutb => binReadSLV(0)
        );

    bram2_mjj : entity work.hist128
      port map(
        clka  => ClockBus(2),
        ena   => enableBramSLV(1),
        wea   => writeEnableSLV(1),
        addra => addrWriteSLV(1),
        dina  => binWriteSLV(1),
        clkb  => ClockBus(2),
        enb   => readBramSLV(1),
        addrb => addrReadSLV(1),
        doutb => binReadSLV(1)
        );
  end generate;

  gen_hist294 : if NumBins = 294 generate
    bram_mjj : entity work.hist294
      port map(
        clka  => ClockBus(2),
        ena   => enableBramSLV(0),
        wea   => writeEnableSLV(0),
        addra => addrWriteSLV(0),
        dina  => binWriteSLV(0),
        clkb  => ClockBus(2),
        enb   => readBramSLV(0),
        addrb => addrReadSLV(0),
        doutb => binReadSLV(0)
        );

    bram2_mjj : entity work.hist294
      port map(
        clka  => ClockBus(2),
        ena   => enableBramSLV(1),
        wea   => writeEnableSLV(1),
        addra => addrWriteSLV(1),
        dina  => binWriteSLV(1),
        clkb  => ClockBus(2),
        enb   => readBramSLV(1),
        addrb => addrReadSLV(1),
        doutb => binReadSLV(1)
        );
  end generate;


end architecture Behavioral;
