library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.L1TopoDataTypes.all;
use work.L1TopoFunctions.all;
use work.HistogrammingConstants.all;
use work.HistogrammingCutsConstants.all;

entity HistogrammingCuts is
  port(
    ClockBus      : in  std_logic_vector(2 downto 0);
    Js            : in  ExtendedTOBArray(NumSortedJetsHist - 1 downto 0);  -- Jets are already sorted by the JetSort module
    invariantMass : out unsigned(DijetAnaInvmBitWidth - 1 downto 0);
    ResultMatrix  : out ResultBitMatrix;
    EventVeto     : out std_logic
    );
end HistogrammingCuts;


architecture Behavioral of HistogrammingCuts is

  -------------
  -- signals --
  -------------

  signal Js_in    : ExtendedTOBArray ((NumSortedJetsHist - 1) downto 0);
  signal Js_tmp   : ExtendedTOBArray ((NumSortedJetsHist - 1) downto 0);
  signal Jlead    : ExtendedGenericTOB;
  signal Jsublead : ExtendedGenericTOB;
--  signal yStarCut : std_logic_vector(yStarBitWidth - 1 downto 0);
  signal deltaEta : std_logic_vector(yStarBitWidth - 1 downto 0);

  signal yStarBit_tmp : std_logic;
  signal EtaBit_tmp   : std_logic;

  signal tmp_etaResultBits     : etaBitMatrix                                            := (others => (others => '0'));
  signal tmp_flatEtaResultBits : std_logic_vector(NumberOfTotalEtaCategories-1 downto 0) := (others => '0');
  signal veto                  : std_logic;

  signal ResultBits : ResultBitMatrix := (others => (others => '0'));

  signal InvMSqr, InvMSqr_reg : std_logic_vector(InvariantMassSqrBitWidth - 1 downto 0);
  signal InvM    : unsigned((InvariantMassSqrBitWidth / 2) - 1 downto 0);

  signal yStarLowerCuts : cutValuesYStar(NumberOfYStarCategories - 1 downto 0);
  signal yStarUpperCuts : cutValuesYStar(NumberOfYStarCategories - 1 downto 0);

  signal EtaLowerCuts : cutValuesEta(NumberOfJetEtaCategories - 1 downto 0);
  signal EtaUpperCuts : cutValuesEta(NumberOfJetEtaCategories - 1 downto 0);

  signal leadJetAccept    : std_logic;
  signal subleadjetAccept : std_logic;
  signal yStarAccept      : std_logic;
begin

  Js_in    <= Js;
  Jlead    <= Js_in(0);
  Jsublead <= Js_in(1);

  yStarLowerCuts(yStarSmallIndex) <= yStarSmallLowerCut;
  yStarUpperCuts(yStarSmallIndex) <= yStarSmallUpperCut;

  yStarLowerCuts(yStarMediumIndex) <= yStarMediumLowerCut;
  yStarUpperCuts(yStarMediumIndex) <= yStarMediumUpperCut;

  yStarLowerCuts(yStarHighIndex) <= yStarHighLowerCut;
  yStarUpperCuts(yStarHighIndex) <= yStarHighUpperCut;

  EtaLowerCuts(CentralDetectorIndex) <= EtaCentralLowerCut;
  EtaUpperCuts(CentralDetectorIndex) <= EtaCentralUpperCut;

  EtaLowerCuts(ExtendedDetectorIndex) <= EtaExtendedLowerCut;
  EtaUpperCuts(ExtendedDetectorIndex) <= EtaExtendedUpperCut;

  EtaLowerCuts(ForwardDetectorIndex) <= EtaForwardLowerCut;
  EtaUpperCuts(ForwardDetectorIndex) <= EtaForwardUpperCut;

  Inv_mass_inst : entity work.InvMassCalc
    generic map(
      NumRegisters => 0
      )
    port map (
      ClockBus => ClockBus,
      eta1     => Jlead.Eta,
      eta2     => Jsublead.Eta,
      phi1     => Jlead.Phi,
      phi2     => Jsublead.Phi,
      energy1  => Jlead.Et,
      energy2  => Jsublead.Et,
      mass_sqr => InvMSqr_reg
      );

  InvMSqr <= InvMSqr_reg when rising_edge(ClockBus(0));

  InvM <= sqrt(unsigned(InvMSqr));

  dEtaCalc_inst : entity work.DeltaEtaCalc
    port map(
      eta1In      => Jlead.eta,
      eta2In      => Jsublead.eta,
      deltaEtaOut => deltaEta
      );


  gen_eta_lead : for i in 0 to NumberOfJetEtaCategories - 1 generate
    gen_eta_sublead : for j in 0 to NumberOfJetEtaCategories - 1 generate
   --   leadJetAccept           <= '1' when is_in_eta_range_tob(Jlead, std_logic_vector(EtaLowerCuts(i)), std_logic_vector(EtaUpperCuts(i))) else '0'; -- ugly casts
   --   subleadjetAccept        <= '1' when is_in_eta_range_tob(Jsublead, std_logic_vector(EtaLowerCuts(j)), std_logic_vector(EtaUpperCuts(j))) else '0';
      tmp_etaResultBits(i)(j) <= '1' when is_in_eta_range_tob(Jlead, std_logic_vector(EtaLowerCuts(i)), std_logic_vector(EtaUpperCuts(i))) and is_in_eta_range_tob(Jsublead, std_logic_vector(EtaLowerCuts(j)), std_logic_vector(EtaUpperCuts(j))) else '0';
    end generate;
  end generate;

  -- Flatten the array
  tmp_flatEtaResultBits(JetsBothCentralDetectorIndex)  <= tmp_etaResultBits(CentralDetectorIndex)(CentralDetectorIndex);
  tmp_flatEtaResultBits(JetsBothExtendedDetectorIndex) <= tmp_etaResultBits(ExtendedDetectorIndex)(ExtendedDetectorIndex);
  tmp_flatEtaResultBits(JetsBothForwardDetectorIndex)  <= tmp_etaResultBits(ForwardDetectorIndex)(ForwardDetectorIndex);

  tmp_flatEtaResultBits(JetsOneCentralOneExtendedIndex) <= tmp_etaResultBits(CentralDetectorIndex)(ExtendedDetectorIndex)
                                                           or tmp_etaResultBits(ExtendedDetectorIndex)(CentralDetectorIndex);

  tmp_flatEtaResultBits(JetsOneCentralOneForwardIndex) <= tmp_etaResultBits(CentralDetectorIndex)(ForwardDetectorIndex)
                                                          or tmp_etaResultBits(ForwardDetectorIndex)(CentralDetectorIndex);

  tmp_flatEtaResultBits(JetsOneExtendedOneForwardIndex) <= tmp_etaResultBits(ForwardDetectorIndex)(ExtendedDetectorIndex)
                                                           or tmp_etaResultBits(ExtendedDetectorIndex)(ForwardDetectorIndex);


  gen_ystar : for i in 0 to NumberOfTotalEtaCategories - 1 generate
    gen_eta : for j in 0 to NumberOfYStarCategories - 1 generate
      ResultBits(j)(i) <= '1' when tmp_flatEtaResultBits(i) = '1' and
                          unsigned(deltaEta) < 2*yStarUpperCuts(j) and unsigned(deltaEta) >= 2*yStarLowerCuts(j) else '0';
    end generate;
  end generate;



  veto <= '1' when Jlead.Et = MaxJetEt or Jsublead.Et = MaxJetEt or Jlead.Overflow = '1' else '0';



  invariantMass <= InvM(DijetAnaInvmBitWidth - 1 downto 0);
  ResultMatrix  <= ResultBits;
  EventVeto     <= veto;

end Behavioral;
