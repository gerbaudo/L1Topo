------------------------------------------------------------------------------------------
-- Author/Modified by : Sebastian Artz (sebastian.artz@cern.ch)
-- Description      : constants for the dijet analysis
-- created        : 14.12.2016
------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;
--use work.L1TopoFunctions.all;
use work.l1topo_package.all;
use work.L1TopoDataTypes.all;
use work.L1TopoLUTvalues.all;
use work.muon_types.all;

package HistogrammingConstants is

  constant BCIDmax                  : natural := 3564;
  constant MaxLHCturns              : natural := 11246;
  constant DijetAnaBinBitWidth      : natural := 32;  -- 32
  constant DijetAnaInvmBitWidth     : natural := 14;  -- 14
  constant InvmAdressBitWidth       : natural := 14;  --DijetAnaInvmBitWidth;
  constant DijetAnaNumBinsInvm      : natural := 2**InvmAdressBitWidth;
  constant NumSortedJetsHist        : integer := 2;
  constant MaxInvM                  : integer := 14000;
  constant NumHistograms            : integer := 24;
  constant HistResetTicks           : integer := BCIDmax * MaxLHCturns;  --294;
  constant NumBinsInvm              : integer := 294;
  constant WriteOutputDelaySort     : integer := 2;
  constant WriteOutputDelayAnalysis : integer := 3;
  constant TotalDelayDijetAnalysis  : integer := WriteOutputDelaySort + WriteOutputDelayAnalysis;
  constant MaxHistAddrWidth         : integer := 11;
  constant MaxNumHistograms         : integer := 32;

  type Histogram is array (natural range <>) of unsigned(DijetAnaBinBitWidth - 1 downto 0);
  type HistogramArray is array (natural range <>) of Histogram(DijetAnaNumBinsInvm - 1 downto 0);


  function mapInvmBinBase2(m : unsigned; LogBinWidth : integer) return unsigned;

  type ExtendedGenericTOB is record
    Et       : std_logic_vector(GenericEtBitWidth - 1 downto 0);  -- Pad unused bits with zeros, they will be optimised away
--    EtSqr    : std_logic_vector(GenericEtSqrBitWidth - 1 downto 0); -- Pad unused bits with zeros, they will be optimised away
    Eta      : std_logic_vector(GenericEtaBitWidth - 1 downto 0);  -- Again, pad bottom bit with zero for muons and jets
    Phi      : std_logic_vector(GenericPhiBitWidth - 1 downto 0);
    Et1      : std_logic_vector(JetEt1BitWidth-1 downto 0);
    Et2      : std_logic_vector(JetEt2BitWidth-1 downto 0);
    EtaBin   : std_logic_vector(JetEtaBitWidth-1 downto 0);
    PhiBin   : std_logic_vector(JetPhiBitWidth-1 downto 0);
    Overflow : std_logic;  -- Overflow bit is only set for element (0) of TOBArray
  end record;

  constant empty_extended_tob : ExtendedGenericTOB := (
    Et       => (others => '0'),
    Et1      => (others => '0'),
    Et2      => (others => '0'),
--    EtSqr => (others => '0'),
    Eta      => (others => '0'),
    Phi      => (others => '0'),
    EtaBin   => (others => '0'),
    PhiBin   => (others => '0'),
    Overflow => '0'
    );

  type ExtendedTOBArray is array (natural range <>) of ExtendedGenericTOB;

  function mapInvmBin(m                       : unsigned(DijetAnaInvmBitWidth - 1 downto 0); BinWidth : integer) return unsigned;
  function to_ExtendedGenericTOB(arg          : JetTOB; jet_size : integer) return ExtendedGenericTOB;
  function is_in_eta_range_tob(Tob            : ExtendedGenericTOB; MinEta : std_logic_vector; MaxEta : std_logic_vector) return boolean;
  function alignToLeftJetArray(target         : JetArray; input : JetArray; cutPos : integer) return JetArray;
  function alignToLeftExtendedTobArray(target : ExtendedTobArray; input : ExtendedTobArray; cutPos : integer) return ExtendedTobArray;
  function mapDijetInvmBin(m                  : unsigned(DijetAnaInvmBitWidth - 1 downto 0); AddrWidth : integer; EventVeto : std_logic) return unsigned;
  function mapDijetEtBin(Et                   : unsigned(GenericEtBitWidth - 1 downto 0); AddrWidth : integer) return unsigned;
  function mapDijetEtaPhiBin(tob              : ExtendedGenericTob; AddrWidth : integer) return unsigned;
  function mapDijetEtMissBin(Met              : unsigned(GenericEtBitWidth - 1 downto 0); AddrWidth : integer) return unsigned;
  function mapDijetDeltaRBin(dR               : unsigned(GenericEtaBitWidth downto 0); AddrWidth : integer) return unsigned;
  function hex2bin (hex                       : character) return std_logic_vector;
  function mapLineToFiber(lineNumber          : integer) return integer;

end package HistogrammingConstants;

package body HistogrammingConstants is

  function mapInvmBinBase2(m : unsigned; LogBinWidth : integer) return unsigned is
  begin
    return unsigned(m(m'length - 1 downto LogBinWidth));
  end function mapInvmBinBase2;

  function mapInvmBin(m : unsigned(DijetAnaInvmBitWidth - 1 downto 0); BinWidth : integer) return unsigned is
  begin
    for i in 1 to (MaxInvM / BinWidth) + 1 loop
      if m < i*BinWidth then
        return to_unsigned(i-1, InvmAdressBitWidth);
      end if;
    end loop;
    return to_unsigned(0, InvmAdressBitWidth);
  end function mapInvmBin;

  function mapDijetInvmBin(m : unsigned(DijetAnaInvmBitWidth - 1 downto 0); AddrWidth : integer; EventVeto : std_logic) return unsigned is
--    constant AddrWidth : integer                          := MjjAddrWidth;
    variable mInt    : integer := 0;
    variable baseBin : integer := 0;
    variable addBin  : integer := 0;
  begin
    mInt := to_integer(m);
    if EventVeto = '0' then
      if mInt >= 0 and mInt < 200 then
        baseBin := 0;
        addBin  := to_integer(m(AddrWidth -1 downto 1));
      elsif mInt >= 200 and mInt < 600 then
        baseBin := 100;
        addBin  := to_integer(to_unsigned(mInt - 200, m'length)(m'length -1 downto 3));
      elsif mInt >= 600 and mInt < 1000 then
        baseBin := 150;
        addBin  := to_integer(to_unsigned(mInt - 600, m'length)(m'length -1 downto 4));
      elsif mInt >= 1000 and mInt < 2984 then
        baseBin := 175;
        addBin  := to_integer(to_unsigned(mInt - 1000, m'length)(m'length -1 downto 5));
      elsif mInt >= 2984 and mInt < 4968 then
        baseBin := 237;
        addBin  := to_integer(to_unsigned(mInt - 2984, m'length)(m'length -1 downto 6));
      elsif mInt >= 4968 and mInt < 8040 then
        baseBin := 268;
        addBin  := to_integer(to_unsigned(mInt - 4968, m'length)(m'length -1 downto 7));
      else
        baseBin := 292;
        addBin  := to_integer(to_unsigned(0, AddrWidth));
      end if;
    else
      baseBin := 293;
      addBin  := to_integer(to_unsigned(0, AddrWidth));
    end if;
    return to_unsigned(baseBin, AddrWidth) + addBin;
  end function;

  function mapDijetEtBin(Et : unsigned(GenericEtBitWidth - 1 downto 0); AddrWidth : integer) return unsigned is
    variable Et_int  : integer := 0;
    variable baseBin : integer := 0;
    variable addBin  : integer := 0;
  begin
    Et_int := to_integer(Et);
    if Et_int >= 0 and Et_int < 200 then
      baseBin := 0;
      addBin  := to_integer(Et(Et'length -1 downto 1));
    elsif Et_int >= 200 and Et_int < 1023 then
      baseBin := 100;
      addBin  := to_integer(to_unsigned(Et_int - 200, Et'length)(Et'length -1 downto 5));
    else
      baseBin := 127;
      addBin  := to_integer(to_unsigned(0, AddrWidth));
    end if;
    return to_unsigned(baseBin, AddrWidth) + addBin;
  end function;

  function mapDijetEtaPhiBin(tob : ExtendedGenericTob; AddrWidth : integer) return unsigned is
  begin
    return to_unsigned((to_integer(unsigned(tob.PhiBin)) * (2**JetEtaBitWidth)) + to_integer(unsigned(tob.EtaBin)), AddrWidth);
  end function;

  function mapDijetEtMissBin(Met : unsigned(GenericEtBitWidth - 1 downto 0); AddrWidth : integer) return unsigned is
    variable met_int : integer := 0;
    variable baseBin : integer := 0;
    variable addBin  : integer := 0;
  begin
    met_int := to_integer(Met);
    if met_int >= 0 and met_int < 200 then
      baseBin := 0;
      addBin  := to_integer(Met(AddrWidth -1 downto 1));
    elsif met_int >= 200 and met_int < 808 then
      baseBin := 100;
      addBin  := to_integer(to_unsigned(Met_int - 200, Met'length)(Met'length -1 downto 5));
    else
      baseBin := 119;
      addBin  := to_integer(to_unsigned(0, AddrWidth));
    end if;
    return to_unsigned(baseBin, AddrWidth) + addBin;
  end function;

  function mapDijetDeltaRBin(dR : unsigned(GenericEtaBitWidth downto 0); AddrWidth : integer) return unsigned is
    variable dR_int  : integer := 0;
    variable baseBin : integer := 0;
    variable addBin  : integer := 0;
  begin
    dR_int := to_integer(dR);
    if dR_int >= 0 and dR_int < 102 then
      baseBin := 0;
      addBin  := to_integer(dR(AddrWidth -1 downto 0));
    else
      baseBin := 102;
      addBin  := to_integer(to_unsigned(0, AddrWidth));
    end if;
    return to_unsigned(baseBin, AddrWidth) + addBin;
  end function;

  function to_ExtendedGenericTOB(arg : JetTOB; jet_size : integer) return ExtendedGenericTOB is
    variable EtaBin : integer;
    variable result : ExtendedGenericTOB;
  begin
    if jet_size = 1 then
      result.Et := '0' & arg.Et1;
    else
      result.Et := arg.Et2;
    end if;
    EtaBin          := to_integer(unsigned(arg.Eta));
    result.Eta      := std_logic_vector(to_signed(JetEtaTab(EtaBin), GenericEtaBitWidth));
    result.Phi      := std_logic_vector(unsigned(arg.Phi & '0') + 2);
    result.Et1      := arg.Et1;
    result.Et2      := arg.Et2;
    result.EtaBin   := arg.Eta;
    result.PhiBin   := arg.Phi;
    result.Overflow := '0';
    return result;
  end function to_ExtendedGenericTOB;

  function is_in_eta_range_tob(Tob    : ExtendedGenericTOB;
                               MinEta : std_logic_vector;
                               MaxEta : std_logic_vector) return boolean is
    variable result : boolean := false;
  begin
    if ((to_integer(abs(signed(Tob.Eta))) >= to_integer(unsigned(MinEta))) and
        (to_integer(abs(signed(Tob.Eta))) <= to_integer(unsigned(MaxEta)))
        ) then
      result := true;
    else
      result := false;
    end if;
    return result;
  end function is_in_eta_range_tob;

  function alignToLeftJetArray(target : JetArray; input : JetArray; cutPos : integer) return JetArray is
    variable output       : JetArray(input'length - 1 downto 0);
    variable vectorLength : integer;
  begin
    vectorLength := input'length;
    output       := target;

    case cutPos is
      when 0 =>
        output := input;
      when 1 =>
        output(vectorLength - 1 downto 1) := input(vectorLength - 1 - 1 downto 0);
      when 2 =>
        output(vectorLength - 1 downto 2) := input(vectorLength - 2 - 1 downto 0);
      when 3 =>
        output(vectorLength - 1 downto 3) := input(vectorLength - 3 - 1 downto 0);
      when 4 =>
        output(vectorLength - 1 downto 4) := input(vectorLength - 4 - 1 downto 0);
      when 5 =>
        output(vectorLength - 1 downto 5) := input(vectorLength - 5 - 1 downto 0);
      when 6 =>
        output(vectorLength - 1 downto 6) := input(vectorLength - 6 - 1 downto 0);
      when 7 =>
        output(vectorLength - 1 downto 7) := input(vectorLength - 7 - 1 downto 0);
      when 8 =>
        output(vectorLength - 1 downto 8) := input(vectorLength - 8 - 1 downto 0);
      when 9 =>
        output(vectorLength - 1 downto 9) := input(vectorLength - 9 - 1 downto 0);
      when others =>
        output := target;
    end case;

    return output;
  end function;


  function alignToLeftExtendedTobArray(target : ExtendedTobArray; input : ExtendedTobArray; cutPos : integer) return ExtendedTobArray is
    variable output       : ExtendedTobArray(input'length - 1 downto 0);
    variable vectorLength : integer;
  begin
    vectorLength := input'length;
    output       := target;

    case cutPos is
      when 0 =>
        output := input;
      when 1 =>
        output(vectorLength - 1 downto 1) := input(vectorLength - 1 - 1 downto 0);
      when 2 =>
        output(vectorLength - 1 downto 2) := input(vectorLength - 2 - 1 downto 0);
      when 3 =>
        output(vectorLength - 1 downto 3) := input(vectorLength - 3 - 1 downto 0);
      when 4 =>
        output(vectorLength - 1 downto 4) := input(vectorLength - 4 - 1 downto 0);
      when 5 =>
        output(vectorLength - 1 downto 5) := input(vectorLength - 5 - 1 downto 0);
      when 6 =>
        output(vectorLength - 1 downto 6) := input(vectorLength - 6 - 1 downto 0);
      when 7 =>
        output(vectorLength - 1 downto 7) := input(vectorLength - 7 - 1 downto 0);
      when 8 =>
        output(vectorLength - 1 downto 8) := input(vectorLength - 8 - 1 downto 0);
      when 9 =>
        output(vectorLength - 1 downto 9) := input(vectorLength - 9 - 1 downto 0);
      when others =>
        output := target;
    end case;

    return output;
  end function;

  function hex2bin (hex : character) return std_logic_vector is
    variable result : std_logic_vector (3 downto 0);
  begin
    case hex is
      when '0'     => result := "0000";
      when '1'     => result := "0001";
      when '2'     => result := "0010";
      when '3'     => result := "0011";
      when '4'     => result := "0100";
      when '5'     => result := "0101";
      when '6'     => result := "0110";
      when '7'     => result := "0111";
      when '8'     => result := "1000";
      when '9'     => result := "1001";
      when 'A'|'a' => result := "1010";
      when 'B'|'b' => result := "1011";
      when 'C'|'c' => result := "1100";
      when 'D'|'d' => result := "1101";
      when 'E'|'e' => result := "1110";
      when 'F'|'f' => result := "1111";
      when 'X'|'x' => result := "XXXX";
      when others  => null;
    end case;
    return result;
  end;

  function mapLineToFiber(lineNumber : integer) return integer is
    variable result : integer := 81;
  begin
    if (lineNumber >= 32) then
      result := (lineNumber - 32) / 4 + 56;
    else
      result := lineNumber / 4 + 64;
    end if;

    return result;
  end function;

end package body HistogrammingConstants;

