------------------------------------------------------------------------------------------
-- Author/Modified by : Sebastian Artz (sebastian.artz@cern.ch)
-- Description        : Store InputData1 and InputData2 in histogram.
--                      Once histogram ins recorded it is buffered while a new one is recorded
-- created            : 13.04.2017
-- 
------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.L1TopoDataTypes.all;
use work.L1TopoFunctions.all;
use work.ipbus.all;
use work.l1topo_package.all;
use work.HistogrammingConstants.all;

entity HistogramBram2D is
  generic(
    BramAddrWidth : integer := 9;
    NumBins       : integer := 0;
    BinBitWidth   : integer := DijetAnaBinBitWidth
    );
  port(
    ClockBus         : in  std_logic_vector(2 downto 0);
    InputAddress1    : in  std_logic_vector(BramAddrWidth - 1 downto 0);
    InputAddress2    : in  std_logic_vector(BramAddrWidth - 1 downto 0);
    EventVeto        : in  std_logic;
    Accept           : in  std_logic;
    SwapHistograms   : in  std_logic;
    ipbus_in         : in  ipb_wbus;
    enableWriteIPbus : in  std_logic;
    ipbus_out        : out ipb_rbus
    );
end entity HistogramBram2D;

architecture Behavioral of HistogramBram2D is

  -- constants

  constant ControlSignalDelay : integer              := 3;
--  constant BramAddrWidth      : integer              := ld(NumBins);
  constant cntStart           : unsigned(1 downto 0) := "11";

  -- types

  type BinSLV is array (natural range <>) of std_logic_vector(BinBitWidth - 1 downto 0);
  type AddrSLV is array (natural range <>) of std_logic_vector(BramAddrWidth - 1 downto 0);
  type arraySLV1 is array (natural range <>) of std_logic_vector(0 downto 0);

  -- signals

  signal currentEntry : unsigned(BinBitWidth - 1 downto 0);

  signal binRead, binWrite                                          : std_logic_vector(BinBitWidth - 1 downto 0);
  signal addrRead, addrWrite                                        : std_logic_vector(BramAddrWidth - 1 downto 0);
  signal binReadIPbus, binReadIPbus_reg, binWriteIPbus              : std_logic_vector(BinBitWidth - 1 downto 0);
  signal addrReadIPbus, addrWriteIPbus                              : std_logic_vector(BramAddrWidth - 1 downto 0);
--  signal binReadSLV, binWriteSLV                                    : BinSLV(2 - 1 downto 0);
--  signal addrReadSLV, addrWriteSLV                                  : AddrSLV(2 - 1 downto 0);
  signal activeHistIndex                                            : integer range 0 to 1                         := 1;
  signal DoRecording                                                : std_logic                                    := '0';
  signal bufferHistogram, bufferHistogram_reg, bufferHistogram_reg2 : std_logic                                    := '0';
  signal subtickCtr                                                 : unsigned(1 downto 0)                         := "11";
  signal toggle40, toggle160, toggle160synch, resetClk              : std_logic;
  signal addrCtr                                                    : std_logic_vector(BramAddrWidth - 1 downto 0) := (others => '0');
  signal recordCtr                                                  : unsigned(3 - 1 downto 0)                     := "101";
  signal enableBram                                                 : std_logic                                    := '1';
  signal writeBram                                                  : std_logic                                    := '0';
  signal readBram                                                   : std_logic                                    := '0';
  signal resetBram                                                  : std_logic                                    := '0';
  signal enableBramIPbus                                            : std_logic                                    := '1';
  signal writeBramIPbus                                             : std_logic                                    := '0';
  signal readBramIPbus                                              : std_logic                                    := '0';
  signal resetBramIPbus                                             : std_logic                                    := '0';
--  signal enableBramSLV                                              : std_logic_vector(1 downto 0)                 := "00";
--  signal readBramSLV                                                : std_logic_vector(1 downto 0)                 := "00";
--  signal resetBramSLV                                               : std_logic_vector(1 downto 0)                 := "00";
--  signal writeEnableSLV                                             : arraySLV1(1 downto 0)                        := (others => (others => '0'));
  signal ctrl_sel                                                   : integer;
  signal ack                                                        : std_logic;
  signal strobe_reg, addr0_reg                                      : std_logic;
  signal strobeWriteEnable                                          : std_logic                                    := '0';

  -- 2d histogram
  signal entryReadSLVa, entryReadSLVb, entryWriteSLVa, entryWriteSLVb : BinSLV(2 - 1 downto 0);
  signal addrBramSLVa, addrBramSLVb                                   : AddrSLV(2 - 1 downto 0);
  signal enableBramSLVa, enableBramSLVb                               : std_logic_vector(1 downto 0) := "00";
  signal writeEnableSLVa, writeEnableSLVb                             : arraySLV1(1 downto 0)        := (others => (others => '0'));
  signal entryReadA, entryWriteA, entryReadB, entryWriteB             : std_logic_vector(BinBitWidth - 1 downto 0);
  signal addrBramA, addrBramB, addrBramAtmp, addrBramBtmp             : std_logic_vector(BramAddrWidth - 1 downto 0);
  signal addressCollision                                             : std_logic                    := '0';
  signal addToHistEntryA, addToHistEntryB                             : unsigned(BinBitWidth - 1 downto 0);

begin

--  addrRead <= InputAddress;
--  addrBramSLVa <= InputAddress1;
--  addrBramSLVb <= InputAddress2;

  addrBramA <= InputAddress1;
  addrBramB <= InputAddress2;

  addressCollision <= '1'                                    when InputAddress1 = InputAddress2 else '0';
  addToHistEntryA  <= to_unsigned(2, addToHistEntryA'length) when InputAddress1 = InputAddress2 else to_unsigned(1, addToHistEntryA'length);
  addToHistEntryB  <= to_unsigned(0, addToHistEntryB'length) when InputAddress1 = InputAddress2 else to_unsigned(1, addToHistEntryB'length);

  -- update bin in histogram
  increase_entry : process(ClockBus(0))
  begin
    if (rising_edge(ClockBus(0))) then
      if SwapHistograms = '1' then
        activeHistIndex <= 1 - activeHistIndex;
      end if;
    end if;
  end process;

  clk_160 : process(ClockBus)
  begin
    if rising_edge(ClockBus(2)) then
      if (resetClk = '1') then
        subtickCtr <= cntStart;
      else
        subtickCtr <= subtickCtr + "01";
      end if;

      if subtickCtr = "10" then
--        addrWrite <= addrRead;

        --if InputAddress1 = InputAddress2 then
        --  addressCollision <= '1';
        --  addToHistEntryA  <= to_unsigned(2, addToHistEntryA'length);
        --  addToHistEntryB  <= to_unsigned(0, addToHistEntryB'length);
        --else
        --  addressCollision <= '0';
        --  addToHistEntryA  <= to_unsigned(1, addToHistEntryA'length);
        --  addToHistEntryB  <= to_unsigned(1, addToHistEntryB'length);
        --end if;

--        binWrite  <= std_logic_vector(unsigned(binRead) + to_unsigned(1, binRead'length));
        entryWriteA <= std_logic_vector(unsigned(entryReadA) + addToHistEntryA);
        entryWriteB <= std_logic_vector(unsigned(entryReadB) + addToHistEntryB);
      else
--        addrBramA   <= addrBramA;
--        addrBramB   <= addrBramB;
        entryWriteA <= entryWriteA;
        entryWriteB <= entryWriteB;
      end if;

    end if;
  end process;

  readBram       <= '1' when subtickCtr = "01"                                                        else '0';
  writeBram      <= '1' when (subtickCtr = "11" and Accept = '1')                                     else '0';
  writeBramIPbus <= '1' when subtickCtr = "11" and strobeWriteEnable = '1' and enableWriteIPbus = '1' else '0';

  process(ClockBus(2))
  begin
    if rising_edge(ClockBus(2)) then
      toggle160      <= toggle40;
      toggle160synch <= toggle160;
    end if;
  end process;

  resetClk <= '1' when (toggle160synch /= toggle160) else '0';

  enableBram <= readBram or writeBram;

--  readBramSLV(0)        <= readBram;
  enableBramSLVa(0)     <= enableBram  when activeHistIndex = 0                              else enableBramIPbus;
  enableBramSLVb(0)     <= enableBram  when (activeHistIndex = 0 and addressCollision = '0') else '0';  --enableBramIPbus;
--  resetBramSLV(0)      <= bufferHistogram_reg2 when activeHistIndex = 1 else '0';
  addrBramSLVa(0)       <= addrBramA   when activeHistIndex = 0                              else addrWriteIPbus;
  addrBramSLVb(0)       <= addrBramB   when activeHistIndex = 0                              else addrWriteIPbus;
  entryWriteSLVa(0)     <= entryWriteA when activeHistIndex = 0                              else binWriteIPbus;
  entryWriteSLVb(0)     <= entryWriteB when activeHistIndex = 0                              else binWriteIPbus;
--  addrReadSLV(0)       <= addrRead   when activeHistIndex = 0 else addrReadIPbus;
  writeEnableSLVa(0)(0) <= writeBram   when activeHistIndex = 0                              else writeBramIPbus;
  writeEnableSLVb(0)(0) <= writeBram   when (activeHistIndex = 0 and addressCollision = '0') else '0';  --writeBramIPbus;

--  readBramSLV(1)        <= readBram;
  enableBramSLVa(1)     <= enableBram  when activeHistIndex = 1                              else enableBramIPbus;
  enableBramSLVb(1)     <= enableBram  when (activeHistIndex = 1 and addressCollision = '0') else '0';  --enableBramIPbus;
--  resetBramSLV(0)      <= bufferHistogram_reg2 when activeHistIndex = 1 else '0';
  addrBramSLVa(1)       <= addrBramA   when activeHistIndex = 1                              else addrWriteIPbus;
  addrBramSLVb(1)       <= addrBramB   when activeHistIndex = 1                              else addrWriteIPbus;
  entryWriteSLVa(1)     <= entryWriteA when activeHistIndex = 1                              else binWriteIPbus;
  entryWriteSLVb(1)     <= entryWriteB when activeHistIndex = 1                              else binWriteIPbus;
--  addrReadSLV(0)       <= addrRead   when activeHistIndex = 0 else addrReadIPbus;
  writeEnableSLVa(1)(0) <= writeBram   when activeHistIndex = 1                              else writeBramIPbus;
  writeEnableSLVb(1)(0) <= writeBram   when (activeHistIndex = 1 and addressCollision = '0') else '0';  --writeBramIPbus;


--  readBramSLV(1)       <= readBram;
--  enableBramSLV(1)     <= enableBram when activeHistIndex = 1 else enableBramIPbus;
--  resetBramSLV(1)      <= bufferHistogram_reg2 when activeHistIndex = 0 else '0';
--  addrWriteSLV(1)      <= addrWrite  when activeHistIndex = 1 else addrWriteIPbus;
--  binWriteSLV(1)       <= binWrite   when activeHistIndex = 1 else binWriteIPbus;
--  addrReadSLV(1)       <= addrRead   when activeHistIndex = 1 else addrReadIPbus;
--  writeEnableSLV(1)(0) <= writeBram  when activeHistIndex = 1 else writeBramIPbus;

  readBramIPbus   <= '1';
  enableBramIPbus <= '1';
  resetBramIPbus  <= '0';
  addrWriteIPbus  <= ipbus_in.ipb_addr(BramAddrWidth - 1 downto 0);  --addrCtr;
  binWriteIPbus   <= (others => '0');
  --addrReadIPbus   <= addrCtr;
  addrReadIPbus   <= ipbus_in.ipb_addr(BramAddrWidth - 1 downto 0);

  entryReadA       <= entryReadSLVa(0) when activeHistIndex = 0 else entryReadSLVa(1);
  entryReadB       <= entryReadSLVb(0) when activeHistIndex = 0 else entryReadSLVb(1);
  binReadIPbus_reg <= entryReadSLVa(1) when activeHistIndex = 0 else entryReadSLVa(0);
  binReadIPbus     <= binReadIPbus_reg when rising_edge(ClockBus(0));
--  HistEntryOut <= binReadIPbus  when rising_edge(ClockBus(0));

----- IPbus -----

  process(ClockBus(0))
  begin
    if rising_edge(ClockBus(0)) then

      strobe_reg <= ipbus_in.ipb_strobe;
      addr0_reg  <= ipbus_in.ipb_addr(0);

      --if reset='1' then
      --  reg <= (others=>(others=>'0'));
      --elsif ipbus_in.ipb_strobe='1' and ipbus_in.ipb_write='1' then
      --  reg(ctrl_sel) <= ipbus_in.ipb_wdata;
      --end if;

      if ipbus_in.ipb_strobe = '1' and (strobe_reg = '0' or (addr0_reg /= ipbus_in.ipb_addr(0))) then
        ack <= not ack;
      else ack <= '0';
      end if;

    end if;
  end process;

  strobeWriteEnable <= '1' when ipbus_in.ipb_strobe = '1' and (strobe_reg = '0' or (addr0_reg /= ipbus_in.ipb_addr(0))) else '0';

  ipbus_out.ipb_rdata <= binReadIPbus;
  ipbus_out.ipb_ack   <= ack;
  ipbus_out.ipb_err   <= '0';

  -- BRAM

  gen_hist1024 : if NumBins = 1024 generate
    bram_hist : entity work.hist1024tdp
      port map(
        clka  => ClockBus(2),
        ena   => enableBramSLVa(0),
        wea   => writeEnableSLVa(0),
        addra => addrBramSLVa(0),
        dina  => entryWriteSLVa(0),
        douta => entryReadSLVa(0),
        clkb  => ClockBus(2),
        enb   => enableBramSLVb(0),
        web   => writeEnableSLVb(0),
        addrb => addrBramSLVb(0),
        dinb  => entryWriteSLVb(0),
        doutb => entryReadSLVb(0)
        );

    bram_hist2 : entity work.hist1024tdp
      port map(
        clka  => ClockBus(2),
        ena   => enableBramSLVa(1),
        wea   => writeEnableSLVa(1),
        addra => addrBramSLVa(1),
        dina  => entryWriteSLVa(1),
        douta => entryReadSLVa(1),
        clkb  => ClockBus(2),
        enb   => enableBramSLVb(1),
        web   => writeEnableSLVb(1),
        addrb => addrBramSLVb(1),
        dinb  => entryWriteSLVb(1),
        doutb => entryReadSLVb(1)
        );
  end generate;


end architecture Behavioral;
