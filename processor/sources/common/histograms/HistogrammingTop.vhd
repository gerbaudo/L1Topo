------------------------------------------------------------------------------------------
-- Author/Modified by : Sebastian Artz (sebastian.artz@cern.ch)
-- Description      : topo level module of the dijet analysis
-- created        : 14.12.2016
-- 
------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.L1TopoDataTypes.all;
use work.L1TopoFunctions.all;
use work.ipbus.all;
--use work.ipbus_addr_decode.ALL;
use work.l1topo_package.all;
use work.HistogrammingConstants.all;
use work.HistogrammingCutsConstants.all;

entity HistogrammingTop is
  generic (
    NumHistograms : integer := NumHistograms
    );
  port (
    ClockBus          : in  std_logic_vector(2 downto 0);
    JetTobArray       : in  JetArray ((InputWidthJET - 1) downto 0);
    Met               : in  MetArray (InputWidthMET - 1 downto 0);
    HistControl       : in  std_logic_vector(31 downto 0);
    HistControl2       : in  std_logic_vector(31 downto 0);
    BcidVeto          : in  std_logic_vector(3584 - 1 downto 0);
    EventCounterReset : in  std_logic;
    L1Accept          : in  std_logic;
    ResetCounters     : in  std_logic;
    BunchCounterReset : in  std_logic;
    DoBcidSync        : in  std_logic;
    NumLhcTurns       : in  std_logic_vector(31 downto 0);
    BCIDin            : in  std_logic_vector(11 downto 0);
    ipbus_in          : in  ipb_wbus;
    ipbus_out         : out ipb_rbus;
    Timestamp         : out std_logic_vector(31 downto 0);
    ControlReset      : out std_logic;
    HistResetCtrOut   : out std_logic_vector(32 -1 downto 0);
--    HistogramReady    : out std_logic;
    ECRoverflow       : out std_logic_vector(31 downto 0);
    PrevHistRead      : out std_logic;
    HistogramNumber   : out unsigned(31 downto 0)
    );
end entity HistogrammingTop;

architecture Behavioral of HistogrammingTop is

  constant EtCutEtaPhi   : std_logic_vector(10 - 1 downto 0) := (others => '0');
  constant BcidVetoDelay : integer                           := 2;

  signal JetSortParameters                                 : ParameterArray  := (others => (others => '0'));
  constant MetSortParameters                               : ParameterArray  := (others => (others => '0'));
  signal Js                                                : ExtendedTOBArray ((NumSortedJetsHist - 1) downto 0);
  signal XE, XE_reg                                        : TOBArray ((OutputWidthMET - 1) downto 0);
  signal InvM                                              : unsigned(DijetAnaInvmBitWidth - 1 downto 0);
  signal SwapHistograms                                    : std_logic       := '0';
  signal EventVeto, EventVeto_reg, EventVeto_reg2          : std_logic       := '0';
  signal ResultMatrix, ResultMatrix_reg, ResultMatrix_reg2 : ResultBitMatrix := (others => (others => '0'));
  signal histNumAddr, histNumAddr_reg                      : unsigned(ld(MaxNumHistograms) - 1 downto 0);
  signal ipbus_out_array                                   : ipb_rbus_array(NumHistograms - 1 downto 0);
  signal BCID                                              : std_logic_vector(11 downto 0);
  signal currentBcidVeto                                   : std_logic;
  signal currentBcidVeto_shreg                             : std_logic_vector(BcidVetoDelay - 1 downto 0);

  -- histogram addresses 
  constant InvmAddrBitWidth   : integer := ld(NumBinsInvm);
  constant EtAddrBitWidth     : integer := ld(127);
  constant EtaPhiAddrBitWidth : integer := ld(1024);
  constant MetAddrBitWidth    : integer := ld(119);
  constant DeltaRAddrBitWidth : integer := ld(102);

  signal InvmAddr, InvmAddr_reg, InvmAddr_reg2                            : std_logic_vector(InvmAddrBitWidth - 1 downto 0);
  signal EtLeadAddr, EtLeadAddr_reg, EtLeadAddr_reg2                      : std_logic_vector(EtAddrBitWidth - 1 downto 0);
  signal EtSubleadAddr, EtSubleadAddr_reg, EtSubleadAddr_reg2             : std_logic_vector(EtAddrBitWidth - 1 downto 0);
  signal EtaPhiLeadAddr, EtaPhiLeadAddr_reg, EtaPhiLeadAddr_reg2          : std_logic_vector(EtaPhiAddrBitWidth - 1 downto 0);
  signal EtaPhiSubleadAddr, EtaPhiSubleadAddr_reg, EtaPhiSubleadAddr_reg2 : std_logic_vector(EtaPhiAddrBitWidth - 1 downto 0);
  signal MetAddr, MetAddr_reg, MetAddr_reg2                               : std_logic_vector(MetAddrBitWidth - 1 downto 0);
  signal deltaRaddr, deltaRaddr_reg, deltaRaddr_reg2                      : std_logic_vector(DeltaRAddrBitWidth - 1 downto 0);

  signal deltaEta                      : std_logic_vector(GenericEtaBitWidth - 1 downto 0) := (others => '0');
  signal deltaPhi                      : std_logic_vector(GenericPhiBitWidth - 1 downto 0) := (others => '0');
  signal deltaRsqr                     : std_logic_vector(2 * GenericEtaBitWidth downto 0) := (others => '0');
  signal deltaR                        : std_logic_vector(GenericEtaBitWidth downto 0)     := (others => '0');
  signal deltaRsqrExt                  : std_logic_vector(31 downto 0)                     := (others => '0');
  signal deltaRext                     : std_logic_vector(15 downto 0)                     := (others => '0');
  signal invMint                       : integer                                           := 0;
  signal SwapHistograms_vec, IPBstrobe : std_logic_vector(0 downto 0);
--  signal ipb_sel                       : integer                                           := 0;
  signal ipb_addr_reg                  : std_logic_vector(31 downto 0)                     := (others => '0');
  signal enableWriteIPbus              : std_logic_vector(NumHistograms - 1 downto 0);
  signal useBcidVeto                   : std_logic := '0';

  --signal pulseIn  : ipb_wbus;
  --signal pulseOut : std_logic_vector(31 downto 0) := (others => '0');
  --signal ctr      : integer                       := 0;

begin

  --sim_inp : process(ClockBus)
  --begin
  --  if rising_edge(ClockBus(0)) then
  --    ctr <= ctr + 1;
  --  end if;

  --  if ctr < 2 then
  --    pulseIn.ipb_addr   <= (others => '0');
  --    pulseIn.ipb_wdata  <= (others => '0');
  --    pulseIn.ipb_strobe <= '0';
  --    pulseIn.ipb_write  <= '0';
  --  end if;

  --  if ctr = 2 then
  --    pulseIn.ipb_addr   <= (others => '0');
  --    pulseIn.ipb_wdata  <= x"00000001";
  --    pulseIn.ipb_strobe <= '1';
  --    pulseIn.ipb_write  <= '1';
  --  end if;

  --  if ctr = 3 then
  --    pulseIn.ipb_addr   <= (others => '0');
  --    pulseIn.ipb_wdata  <= (others => '0');
  --    pulseIn.ipb_strobe <= '0';
  --    pulseIn.ipb_write  <= '0';
  --  end if;

  --end process;

  --test_pulse : entity work.ipbus_slave_reg_pulse
  --  generic map(
  --    addr_width => 0
  --    )
  --  port map(
  --    clk       => ClockBus(0),
  --    reset     => '0',
  --    ipbus_in  => pulseIn,
  --    ipbus_out => ipbus_out,
  --    q         => pulseOut
  --    );

  JetSortParameters(0) <= x"00000000";
  JetSortParameters(1) <= x"000000FF";

-- jet sort
  jetSortHist : entity work.JetSortDijet
    generic map (
      InputWidth         => InputWidthJET,
      InputWidth1stStage => InputWidth1stStageSortJET,
      OutputWidth        => NumSortedJetsHist,
      JetSize            => 2)
    port map (
      JetTobArray => JetTobArray,
      TobArrayOut => Js,
      ClockBus    => ClockBus,
      Parameters  => JetSortParameters
      );

  met_sort : entity work.MetSort
    generic map (
      InputWidth  => InputWidthMET,
      OutputWidth => OutputWidthMET)
    port map (
      MetTobArray => met,
      TobArrayOut => XE_reg,
      ClockBus    => ClockBus,
      Parameters  => MetSortParameters
      );

  XE <= XE_reg when rising_edge(ClockBus(0));

  dEta_inst : entity work.DeltaEtaCalc
    port map(
      eta1In      => Js(0).Eta,
      eta2In      => Js(1).Eta,
      deltaEtaOut => deltaEta
      );
  dPhi_inst : entity work.DeltaPhiCalc
    port map(phi1In      => Js(0).Phi,
             phi2In      => Js(1).Phi,
             deltaPhiOut => deltaPhi
             );

  DeltaRSqrCalc_inst : entity work.DeltaRSqrCalc
    port map(deltaEta  => deltaEta,
             deltaPhi  => deltaPhi,
             deltaRSqr => deltaRSqr
             );

  deltaRSqrExt <= "0" & x"0000" & deltaRsqr;
  deltaRext    <= std_logic_vector(sqrt(unsigned(deltaRSqrExt))) when rising_edge(ClockBus(0));
  deltaR       <= deltaRext(deltaR'length - 1 downto 0);

  dijetCuts_inst : entity work.HistogrammingCuts
    port map(ClockBus      => ClockBus,
             Js            => Js,
             invariantMass => InvM,
             ResultMatrix  => ResultMatrix_reg2,
             EventVeto     => EventVeto_reg
             );

  EventVeto        <= EventVeto_reg or currentBcidVeto when rising_edge(ClockBus(0));
--  EventVeto_reg    <= EventVeto_reg2                   when rising_edge(ClockBus(0));
  ResultMatrix     <= ResultMatrix_reg                 when rising_edge(ClockBus(0));
  ResultMatrix_reg <= ResultMatrix_reg2                when rising_edge(ClockBus(0));

  EVENT_CTR : entity work.EventInfo
    generic map (
      OutputDelay => TotalDelayDijetAnalysis
      )
    port map(
      ClockBus          => ClockBus,
      EventCounterReset => EventCounterReset,
      L1Accept          => L1Accept,
      BunchCounterReset => BunchCounterReset,
      Reset             => ResetCounters,
      HistControl       => HistControl,
      HistControl2       => HistControl2,
      DoBcidSync        => DoBcidSync,
      Timestamp         => Timestamp,
      BCIDin            => BCIDin,
--      BCIDout           => BCID,
--      HistogramReady    => HistogramReady,
      NumLhcTurns       => NumLhcTurns,
      ControlReset      => ControlReset,
      HistResetCtrOut   => HistResetCtrOut,
      ECRoverflow       => ECRoverflow,
      PrevHistRead      => PrevHistRead,
      SwapHistograms    => SwapHistograms,
      HistogramNumber   => HistogramNumber
      );

  useBcidVeto <= HistControl2(1);

  currentBcidVeto_shreg(BcidVetoDelay - 1) <= BcidVeto(to_integer(unsigned(BCID))) when rising_edge(ClockBus(0));
  gen_bcidVeto_reg : for i in BcidVetoDelay - 2 downto 0 generate
    currentBcidVeto_shreg(i) <= currentBcidVeto_shreg(i+1) when rising_edge(ClockBus(0));
  end generate;
  currentBcidVeto <= currentBcidVeto_shreg(0) when useBcidVeto = '1' else '0';

  -- mapping to bins
  InvmAddr_reg           <= std_logic_vector(mapDijetInvmBin(InvM, InvmAddrBitWidth, EventVeto))      when rising_edge(ClockBus(0));
  EtLeadAddr_reg2        <= std_logic_vector(mapDijetEtBin(unsigned(Js(0).Et), EtAddrBitWidth))       when rising_edge(ClockBus(0));
  EtSubleadAddr_reg2     <= std_logic_vector(mapDijetEtBin(unsigned(Js(1).Et), EtAddrBitWidth))       when rising_edge(ClockBus(0));
  MetAddr_reg2           <= std_logic_vector(mapDijetEtMissBin(unsigned(Xe(0).Et), MetAddrBitWidth))  when rising_edge(ClockBus(0));
  EtaPhiLeadAddr_reg2    <= std_logic_vector(mapDijetEtaPhiBin(Js(0), EtaPhiAddrBitWidth))            when rising_edge(ClockBus(0));
  EtaPhiSubleadAddr_reg2 <= std_logic_vector(mapDijetEtaPhiBin(Js(1), EtaPhiAddrBitWidth))            when rising_edge(ClockBus(0));
  deltaRaddr_reg2        <= std_logic_vector(mapDijetDeltaRBin(unsigned(deltaR), DeltaRAddrBitWidth)) when rising_edge(ClockBus(0));

  EtLeadAddr_reg        <= EtLeadAddr_reg2        when rising_edge(ClockBus(0));
  EtSubleadAddr_reg     <= EtSubleadAddr_reg2     when rising_edge(ClockBus(0));
  MetAddr_reg           <= MetAddr_reg2           when rising_edge(ClockBus(0));
  EtaPhiLeadAddr_reg    <= EtaPhiLeadAddr_reg2    when rising_edge(ClockBus(0));
  EtaPhiSubleadAddr_reg <= EtaPhiSubleadAddr_reg2 when rising_edge(ClockBus(0));
  deltaRaddr_reg        <= deltaRaddr_reg2        when rising_edge(ClockBus(0));


  InvmAddr          <= InvmAddr_reg          when rising_edge(ClockBus(0));
  EtLeadAddr        <= EtLeadAddr_reg        when rising_edge(ClockBus(0));
  EtSubleadAddr     <= EtSubleadAddr_reg     when rising_edge(ClockBus(0));
  MetAddr           <= MetAddr_reg           when rising_edge(ClockBus(0));
  EtaPhiLeadAddr    <= EtaPhiLeadAddr_reg    when rising_edge(ClockBus(0));
  EtaPhiSubleadAddr <= EtaPhiSubleadAddr_reg when rising_edge(ClockBus(0));
  deltaRaddr        <= deltaRaddr_reg        when rising_edge(ClockBus(0));


--    SwapHistograms_vec(0) <= SwapHistograms;
--    IPBstrobe(0) <= ipbus_in.ipb_strobe;
--ILA_HISTOGRAMMING : entity work.ila_hist
--PORT MAP (
--  clk => ClockBus(0),
--    probe0 => SwapHistograms_vec, 
--  probe1 => InvmAddr, 
--  probe2 => Timestamp, 
--  probe3 => ipbus_in.ipb_addr,
--  probe4 => ipbus_out.ipb_rdata,
--  probe5 => IPBstrobe
--);

  gen_yStar : for yStar in NumberOfYStarCategories - 1 downto 0 generate
    gen_eta : for eta in NumberOfTotalEtaCategories - 1 downto 0 generate
      HIST_INVM : entity work.HistogramBram
        generic map(
          BramAddrWidth => InvmAddrBitWidth,
          NumBins       => 294
          )
        port map(
          ClockBus         => ClockBus,
          InputAddress     => InvmAddr,
          EventVeto        => EventVeto,
          Accept           => ResultMatrix(yStar)(eta),
          SwapHistograms   => SwapHistograms,
          ipbus_in         => ipbus_in,
          enableWriteIPbus => enableWriteIPbus(yStar * NumberOfTotalEtaCategories + eta),
          ipbus_out        => ipbus_out_array(yStar * NumberOfTotalEtaCategories + eta)
          );
    end generate;
  end generate;

  HIST_ET1 : entity work.HistogramBram
    generic map(
      BramAddrWidth => EtAddrBitWidth,
      NumBins       => 128
      )
    port map(
      ClockBus         => ClockBus,
      InputAddress     => EtLeadAddr,
      EventVeto        => EventVeto,
      Accept           => '1',
      SwapHistograms   => SwapHistograms,
      ipbus_in         => ipbus_in,
      enableWriteIPbus => enableWriteIPbus(18),
      ipbus_out        => ipbus_out_array(18)
      );

  HIST_ET2 : entity work.HistogramBram
    generic map(
      BramAddrWidth => EtAddrBitWidth,
      NumBins       => 128
      )
    port map(
      ClockBus         => ClockBus,
      InputAddress     => EtSubleadAddr,
      EventVeto        => EventVeto,
      Accept           => '1',
      SwapHistograms   => SwapHistograms,
      ipbus_in         => ipbus_in,
      enableWriteIPbus => enableWriteIPbus(19),
      ipbus_out        => ipbus_out_array(19)
      );

  HIST_MET : entity work.HistogramBram
    generic map(
      BramAddrWidth => MetAddrBitWidth,
      NumBins       => 120
      )
    port map(
      ClockBus         => ClockBus,
      InputAddress     => MetAddr,
      EventVeto        => EventVeto,
      Accept           => '1',
      SwapHistograms   => SwapHistograms,
      ipbus_in         => ipbus_in,
      enableWriteIPbus => enableWriteIPbus(20),
      ipbus_out        => ipbus_out_array(20)
      );

  HIST_DR : entity work.HistogramBram
    generic map(
      BramAddrWidth => DeltaRAddrBitWidth,
      NumBins       => 102
      )
    port map(
      ClockBus         => ClockBus,
      InputAddress     => deltaRaddr,
      EventVeto        => EventVeto,
      Accept           => '1',
      SwapHistograms   => SwapHistograms,
      ipbus_in         => ipbus_in,
      enableWriteIPbus => enableWriteIPbus(21),
      ipbus_out        => ipbus_out_array(21)
      );

  HIST_ETA_PHI : entity work.HistogramBram2D
    generic map(
      BramAddrWidth => EtaPhiAddrBitWidth,
      NumBins       => 1024
      )
    port map(
      ClockBus         => ClockBus,
      InputAddress1    => EtaPhiLeadAddr,
      InputAddress2    => EtaPhiSubleadAddr,
      EventVeto        => EventVeto,
      Accept           => '1',
      SwapHistograms   => SwapHistograms,
      ipbus_in         => ipbus_in,
      enableWriteIPbus => enableWriteIPbus(22),
      ipbus_out        => ipbus_out_array(22)
      );


  DEFAULT_HIST : entity work.HistogramBram
    generic map(
      BramAddrWidth => InvmAddrBitWidth,
      NumBins       => 294
      )
    port map(
      ClockBus         => ClockBus,
      InputAddress     => InvmAddr,
      EventVeto        => EventVeto,
      Accept           => '1',
      SwapHistograms   => SwapHistograms,
      ipbus_in         => ipbus_in,
      enableWriteIPbus => enableWriteIPbus(23),
      ipbus_out        => ipbus_out_array(23)
      );


  histNumAddr     <= unsigned(ipbus_in.ipb_addr(MaxHistAddrWidth + ld(MaxNumHistograms) - 1 downto MaxHistAddrWidth));
  histNumAddr_reg <= histNumAddr when rising_edge(ClockBus(0));
  ipbus_out       <= ipbus_out_array(to_integer(histNumAddr_reg));

  GEN_WRITE_ENABLE_IPBUS : for i in 0 to NumHistograms - 1 generate
    enableWriteIPbus(i) <= '1' when histNumAddr = i else '0';
  end generate;

end architecture Behavioral;
