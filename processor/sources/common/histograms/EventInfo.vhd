------------------------------------------------------------------------------------------
-- Author/Modified by : Sebastian Artz (sebastian.artz@cern.ch)
-- Description        : creates L1 event ID from BCID counter and ECR
-- created            : 29.03.2017
------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.L1TopoDataTypes.all;
use work.L1TopoFunctions.all;
use work.HistogrammingConstants.all;

entity EventInfo is
  generic (
    OutputDelay : integer := TotalDelayDijetAnalysis
    );
  port (
    ClockBus          : in  std_logic_vector(2 downto 0);
    EventCounterReset : in  std_logic;
    L1Accept          : in  std_logic;
    BunchCounterReset : in  std_logic;
    Reset             : in  std_logic;
    HistControl       : in  std_logic_vector(31 downto 0);
    HistControl2       : in  std_logic_vector(31 downto 0);
    DoBcidSync        : in  std_logic;
    ControlReset      : out std_logic;
    Timestamp         : out std_logic_vector(31 downto 0);
--    HistNumber        : out std_logic_vector(9 downto 0);
    ECRoverflow       : out std_logic_vector(31 downto 0);
    BCIDin            : in  std_logic_vector(11 downto 0);
--    BCIDout           : out std_logic_vector(11 downto 0);
--    HistogramReady    : out std_logic;
    NumLhcTurns       : in  std_logic_vector(31 downto 0);
    PrevHistRead      : out std_logic;
    SwapHistograms    : out std_logic;
--    BcidResetCtrOut   : out std_logic_vector(32 -1 downto 0);
    HistResetCtrOut   : out std_logic_vector(32 -1 downto 0);
    HistogramNumber   : out unsigned(31 downto 0)
    );
end entity EventInfo;

architecture Behavioral of EventInfo is

  constant HistCtrBitWidth : integer                                := 32;
  constant DeriveBcid      : integer                                := 1;
--  constant OutputDelay     : integer                                := 3;
  signal HistReset         : unsigned(HistCtrBitWidth - 1 downto 0) := to_unsigned(HistResetTicks - 1, HistCtrBitWidth);

  type TimestampArray is array (natural range <>) of std_logic_vector(32 - 1 downto 0);

  signal histResetCtr : unsigned(HistCtrBitWidth - 1 downto 0) := to_unsigned(HistResetTicks - 1, HistCtrBitWidth);

  signal ECRctr                                  : unsigned(7 downto 0)                  := (others => '0');
  signal eventCtr                                : unsigned(23 downto 0)                 := (others => '0');
  signal bcidCtr                                 : unsigned(11 downto 0)                 := (others => '0');
--  signal histNumCtr                      : unsigned(9 downto 0)          := (others => '0');
  signal SwapHistograms_reg                      : std_logic_vector(OutputDelay - 1 downto 0);
  signal SwapHistograms_int                      : std_logic                             := '0';
  signal timestamp_int                           : std_logic_vector(31 downto 0)         := (others => '0');
  signal timestamp_reg                           : TimestampArray(OutputDelay - 1 downto 0);
  signal ECRoverflow_int                         : unsigned(31 downto 0)                 := (others => '0');
  signal PrevHistRead_int                        : std_logic;
  signal HistogramNumber_int                     : unsigned(31 downto 0)                 := (others => '0');
  signal controlReset_int                        : std_logic;
  signal doBcidReset, doBcidReset1, doBcidReset2 : std_logic                             := '0';
  signal BCIDresetDone                           : std_logic                             := '0';
  signal bcidResetCtr                            : unsigned(HistCtrBitWidth -1 downto 0) := (others => '0');
  signal histResetCtr_buf                        : unsigned(HistCtrBitWidth -1 downto 0) := (others => '0');
  signal HistResetLong                           : unsigned(43 downto 0)                 := (others => '0');
  signal bcrGenerated, BunchCounterReset_int     : std_logic                             := '0';
  signal useSimpleCounter                        : std_logic := '0';

begin

  useSimpleCounter <= HistControl2(0);

  HistResetLong <= unsigned(NumLhcTurns) * to_unsigned(BCIDmax, 12);
  HistReset     <= to_unsigned(HistResetTicks - 1, HistCtrBitWidth) when unsigned(NumLhcTurns) = 0 else HistResetLong(31 downto 0);

  proc_hist_ctr : process(ClockBus, BCIDin, bcrGenerated)
  begin
    if rising_edge(ClockBus(0)) then
      if (unsigned(BCIDin) >= BCIDMax - 1) then
        bcrGenerated <= '1';
      else
        bcrGenerated <= '0';
      end if;

      if (useSimpleCounter = '1') then
        if (histResetCtr >= HistReset - 1) then
          histResetCtr <= (others => '0');
          SwapHistograms_reg(OutputDelay - 1) <= '1';
        else
          histResetCtr <= histResetCtr + to_unsigned(1, histResetCtr'length);
          SwapHistograms_reg(OutputDelay - 1) <= '0';
        end if;
      else
        if (bcrGenerated = '1') then
          if (BCIDctr >= unsigned(NumLhcTurns) - 1) then
            BCIDctr <= (others => '0');
            SwapHistograms_reg(OutputDelay - 1) <= '1';
          else
            BCIDctr <= BCIDctr + to_unsigned(1, BCIDctr'length);
            SwapHistograms_reg(OutputDelay - 1) <= '0';
          end if;
        else
          BCIDctr <= BCIDctr;
          SwapHistograms_reg(OutputDelay - 1) <= '0';--SwapHistograms_reg(OutputDelay - 1);
        end if;
      end if;
    end if;
  end process;

  gen_output_delay : for i in OutputDelay - 2 downto 0 generate
    SwapHistograms_reg(i) <= SwapHistograms_reg(i + 1) when rising_edge(ClockBus(0));
    timestamp_reg(i)      <= timestamp_reg(i + 1)      when rising_edge(ClockBus(0));
  end generate;

  SwapHistograms_int <= SwapHistograms_reg(0);
  SwapHistograms     <= SwapHistograms_int;

  SAVE_TIMESTAMP : process(ClockBus(0))
  begin
    if rising_edge(ClockBus(0)) then
      if SwapHistograms_int = '1' then
        Timestamp_int       <= timestamp_reg(0);
--        HistogramReady_int  <= '1';
        HistogramNumber_int <= HistogramNumber_int + to_unsigned(1, HistogramNumber_int'length);

        if HistControl(0) = '1' then
          PrevHistRead_int <= '1';
          controlReset_int <= '1';
        else
          PrevHistRead_int <= '0';
          controlReset_int <= '0';
        end if;
      else
        Timestamp_int       <= Timestamp_int;
--        HistogramReady_int  <= HistogramReady_int;
        PrevHistRead_int    <= PrevHistRead_int;
        controlReset_int    <= '0';
        HistogramNumber_int <= HistogramNumber_int;
      end if;
    end if;
  end process;

  PrevHistRead    <= PrevHistRead_int;
--  HistogramReady  <= HistogramReady_int;
  HistogramNumber <= HistogramNumber_int;
  ControlReset    <= controlReset_int;

  timestamp_reg(OutputDelay - 1) <= std_logic_vector(ECRctr) & std_logic_vector(eventCtr);
  Timestamp                      <= timestamp_int;

end architecture Behavioral;
