------------------------------------------------------------------------------------------
-- Author/Modified by : manuel.simon@uni-mainz.de, sebastian.artz@uni-mainz.de
-- Description      : merges selected lists using a faster clock
-- Number of registers  : 1
------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
use IEEE.math_real.all;
use work.L1TopoDataTypes.all;
use work.L1TopoFunctions.all;
use work.L1TopoLUTvalues.all;
use work.l1topo_package.all;
use work.HistogrammingConstants.all;

entity SelectMergePresortedJetDijet is
  generic(InputWidthBlocks   : integer := 4;
          InputWidthPerBlock : integer := 10;
          OutputWidth        : integer := 10;
          JetSize            : integer := 0;
          DoEtaCut           : integer := 1;
          UseOutClk40        : integer := 0
          );
  port(clk40              : in  std_logic;
       clkFast            : in  std_logic;
       Parameters         : in  ParameterArray;
       inp                : in  JetArray(InputWidthPerBlock * (InputWidthBlocks) - 1 downto 0);
       oup                : out JetArray(OutputWidth - 1 downto 0);
       select_pattern_in  : in  std_logic_vector(InputWidthPerBlock * (InputWidthBlocks) - 1 downto 0);
       select_pattern_out : out std_logic_vector(OutputWidth - 1 downto 0);
       overflow           : out std_logic
       );
end SelectMergePresortedJetDijet;

architecture Behavioral of SelectMergePresortedJetDijet is
  type PresortedArrayGeneric is array (integer range <>) of TobArray(InputWidthPerBlock - 1 downto 0);
  type PresortedArrayGenericOut is array (integer range <>) of JetArray(OutputWidth - 1 downto 0);
  type PresortedArrayJet is array (integer range <>) of JetArray(InputWidthPerBlock - 1 downto 0);
  type SelectionArray is array (integer range <>) of std_logic_vector(InputWidthPerBlock - 1 downto 0);
  type SelectionArrayOut is array (integer range <>) of std_logic_vector(OutputWidth - 1 downto 0);
  type CounterArray is array (integer range <>) of unsigned(integer(ceil(log2(real(OutputWidth)))) + 1 - 1 downto 0);
  type NumTOBsArray is array (integer range <>) of integer range 0 to 2 ** OutputWidth - 1;

  signal oup_int, oup_int_reg                               : JetArray(OutputWidth - 1 downto 0);
  signal clk40_int, clkFast_int                             : std_logic;
  signal inp_int                                            : JetArray(InputWidthPerBlock * (InputWidthBlocks) - 1 downto 0);
  signal select_pattern_out_int, select_pattern_out_int_reg : std_logic_vector(OutputWidth - 1 downto 0);

  signal tobSelectIn                                      : PresortedArrayJet(InputWidthBlocks - 1 downto 0);
  signal tobSelectInGenericCut, tobSelectInGenericCut_reg : PresortedArrayJet(InputWidthBlocks - 1 downto 0);
  signal tobSelectInGeneric                               : PresortedArrayGeneric(InputWidthBlocks - 1 downto 0);
  signal tobSelectOut                                     : PresortedArrayGenericOut(InputWidthBlocks - 1 downto 0);
  signal tobSelectOut_reg                                 : PresortedArrayGenericOut(InputWidthBlocks - 1 downto 0);
  signal selectBitsIn, selectBitsIn_reg                   : SelectionArray(InputWidthBlocks - 1 downto 0);
  signal selectBitsOut                                    : SelectionArrayOut(InputWidthBlocks - 1 downto 0);
  signal selectBitsOut_reg                                : SelectionArrayOut(InputWidthBlocks - 1 downto 0);
  signal select_pattern_int                               : std_logic_vector(InputWidthPerBlock * (InputWidthBlocks) - 1 downto 0);

  signal currentTobCount : CounterArray(InputWidthBlocks - 1 downto 0);
  signal numTOBs         : NumTOBsArray(InputWidthBlocks - 1 downto 0);

  signal MinEt  : std_logic_vector(GenericEtBitWidth - 1 downto 0);
  signal MinEta : std_logic_vector(GenericAbsoluteEtaBitWidth - 1 downto 0);
  signal MaxEta : std_logic_vector(GenericAbsoluteEtaBitWidth - 1 downto 0);
  signal tmpInt : NumTOBsArray(InputWidthBlocks - 1 downto 0);

begin
  MinEt  <= Parameters(0)(GenericEtBitWidth - 1 downto 0);
  MinEta <= Parameters(1)(GenericAbsoluteEtaBitWidth - 1 downto 0);
  MaxEta <= Parameters(2)(GenericAbsoluteEtaBitWidth - 1 downto 0);

  clk40_int   <= clk40;
  clkFast_int <= clkFast;

  inp_int            <= inp;
  select_pattern_int <= select_pattern_in;

  GEN_OUT_40 : if (UseOutClk40 = 1) generate
    oup                <= oup_int_reg                when rising_edge(clk40_int);
    select_pattern_out <= select_pattern_out_int_reg when rising_edge(clk40_int);
  end generate;

  GEN_OUT_fast : if (UseOutClk40 /= 1) generate
    oup                <= oup_int_reg                when rising_edge(clkFast_int);
    select_pattern_out <= select_pattern_out_int_reg when rising_edge(clkFast_int);
  end generate;

  PROC_DELAY_OUTPUT : process(clkFast_int, oup_int, select_pattern_out_int, select_pattern_out_int_reg, oup_int_reg)
    variable tmpSum : integer range 0 to 2 ** (OutputWidth + InputWidthBlocks + 1) - 1;
  begin
    if rising_edge(clkFast_int) then
      oup_int_reg                <= oup_int;
      select_pattern_out_int_reg <= select_pattern_out_int;

      tmpSum := to_integer(unsigned(currentTobCount(0)));
      for blockNum in 1 to InputWidthBlocks - 1 loop
        tmpSum := tmpSum + to_integer(unsigned(currentTobCount(blockNum)));
      end loop;

      if (tmpSum > OutputWidth) then
        Overflow <= '1';
      else
        Overflow <= '0';
      end if;
    end if;
  end process;

  LOOP_ETA_CUT_GEN : for blockNum in InputWidthBlocks - 1 downto 0 generate
    tobSelectIn(blockNum) <= inp_int((blockNum + 1) * InputWidthPerBlock - 1 downto (blockNum + 0) * InputWidthPerBlock);

    ETA_CUT_GEN : if (DoEtaCut /= 0) generate
      LOOP_CUT_ETA : for i in 0 to tobSelectIn(blockNum)'length - 1 generate
        tobSelectInGeneric(blockNum)(i)    <= to_genericTOB(tobSelectIn(blockNum)(i), JetSize);
        tobSelectInGenericCut(blockNum)(i) <= tobSelectIn(blockNum)(i);
        selectBitsIn(blockNum)(i)          <= '1' when (tobSelectInGeneric(blockNum)(i).Et > MinEt and is_in_eta_range_tob(tobSelectInGeneric(blockNum)(i), MinEta, MaxEta)) else '0';
      end generate;
    end generate;

    ETONLY_CUT_GEN : if (DoEtaCut = 0) generate
      LOOP_CUT_NOETA : for i in 0 to tobSelectIn(blockNum)'length - 1 generate
        tobSelectInGeneric(blockNum)(i)    <= to_GenericTOB(tobSelectIn(blockNum)(i), JetSize);
        tobSelectInGenericCut(blockNum)(i) <= tobSelectIn(blockNum)(i);
        selectBitsIn(blockNum)(i)          <= '1' when (tobSelectInGeneric(blockNum)(i).Et > MinEt) else '0';
      end generate;
    end generate;

    gen_tob_select : entity work.TobSelect_presortedDijet
      generic map(
        InputWidth  => InputWidthPerBlock,
        OutputWidth => OutputWidth
        )
      port map(
        Tob         => tobSelectInGenericCut_reg(blockNum)(InputWidthPerBlock - 1 downto 0),
        cmp_res     => selectBitsIn_reg(blockNum)(InputWidthPerBlock - 1 downto 0),
        selTOBs     => tobSelectOut(blockNum)(OutputWidth - 1 downto 0),
        end_cmp_res => selectBitsOut(blockNum)(OutputWidth - 1 downto 0)
        );

    numTOBs(blockNum) <= to_integer(currentTobCount(blockNum));

  end generate;

  REG_2 : process(clkFast_int, tobSelectOut, selectBitsOut, currentTobCount, tobSelectOut_reg, selectBitsOut_reg)
  begin
    if rising_edge(clkFast_int) then
      for blockNum in InputWidthBlocks - 1 downto 0 loop
        currentTobCount(blockNum) <= to_unsigned(reg_ctrl(selectBitsOut(blockNum)), currentTobCount(blockNum)'length);
        tmpInt(blockNum)          <= reg_ctrl(selectBitsOut(blockNum));
      end loop;

      tobSelectOut_reg  <= tobSelectOut;
      selectBitsOut_reg <= selectBitsOut;
    end if;
  end process;

  REG_INPUT2 : process(clkFast_int)
  begin
    if (rising_edge(clkFast_int)) then
      tobSelectInGenericCut_reg <= tobSelectInGenericCut;
      selectBitsIn_reg          <= selectBitsIn;
    end if;
  end process;

  ADD_LIST1 : process(numTOBs, selectBitsOut_reg, tobSelectOut_reg)
    variable select_pattern_out_tmp : std_logic_vector(OutputWidth - 1 downto 0);
    variable oup_int_tmp            : JetArray(OutputWidth - 1 downto 0);
    variable tobSum                 : integer range 0 to 2 ** OutputWidth - 1;
  begin
    tobSum                                           := numTOBs(0);
    select_pattern_out_tmp(OutputWidth - 1 downto 0) := selectBitsOut_reg(0)(OutputWidth - 1 downto 0);
    oup_int_tmp(OutputWidth - 1 downto 0)            := tobSelectOut_reg(0)(OutputWidth - 1 downto 0);

    for blockNum in 1 to InputWidthBlocks - 1 loop
      select_pattern_out_tmp := alignToLeftStdLogicVector(select_pattern_out_tmp, selectBitsOut_reg(blockNum), tobSum);
      oup_int_tmp            := alignToLeftJetArray(oup_int_tmp, tobSelectOut_reg(blockNum), tobSum);
      tobSum                 := tobSum + numTOBs(blockNum);
    end loop;

    select_pattern_out_int <= select_pattern_out_tmp;
    oup_int                <= oup_int_tmp;
  end process;

end Behavioral;
