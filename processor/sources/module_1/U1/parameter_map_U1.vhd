library IEEE;
use IEEE.STD_LOGIC_1164.all;
use work.L1TopoDataTypes.all;
package parameter_map is
		constant NumberOfParameters : natural := 125;
		type ParameterRegisters is array (NumberOfParameters - 1 downto 0) of std_logic_vector(31 downto 0);
		function SortParameterMapper(reg : ParameterRegisters) return ParameterSpace;
		function AlgoParameterMapper(reg : ParameterRegisters) return ParameterSpace;
end parameter_map;
package body parameter_map is
		function SortParameterMapper(reg : ParameterRegisters) return ParameterSpace is
				variable result : ParameterSpace(NumberOfSortAlgorithms - 1 downto 0);
		begin
				--EMall
				result(0)(0) := reg(0);		--IsoMask_0

				--EMs
				result(5)(0) := reg(1);		--IsoMask_0
				result(5)(1) := reg(2);		--MinEta_1
				result(5)(2) := reg(3);		--MaxEta_2

				--TAUsi
				result(7)(0) := reg(4);		--IsoMask_0
				result(7)(1) := reg(5);		--MinEta_1
				result(7)(2) := reg(6);		--MaxEta_2

				--AJjall

				--AJs
				result(14)(0) := reg(7);		--MinEta_0
				result(14)(1) := reg(8);		--MaxEta_1

				--Js
				result(15)(0) := reg(9);		--MinEta_0
				result(15)(1) := reg(10);		--MaxEta_1

				--XE

				return result;
		end function;
		function AlgoParameterMapper(reg : ParameterRegisters) return ParameterSpace is
				variable result : ParameterSpace(NumberOfAlgorithms - 1 downto 0);
		begin
				--INVM_AJ_HighMass
				result(0)(0) := reg(11);		--MinET1_0
				result(0)(1) := reg(12);		--MinET2_1
				result(0)(2) := reg(13);		--MinMSqr_2
				result(0)(3) := reg(14);		--MaxMSqr_3
				result(0)(4) := reg(15);		--MinET1_4
				result(0)(5) := reg(16);		--MinET2_5
				result(0)(6) := reg(17);		--MinMSqr_6
				result(0)(7) := reg(18);		--MaxMSqr_7
				result(0)(8) := reg(19);		--MinET1_8
				result(0)(9) := reg(20);		--MinET2_9
				result(0)(10) := reg(21);		--MinMSqr_10
				result(0)(11) := reg(22);		--MaxMSqr_11
				result(0)(12) := reg(23);		--MinET1_12
				result(0)(13) := reg(24);		--MinET2_13
				result(0)(14) := reg(25);		--MinMSqr_14
				result(0)(15) := reg(26);		--MaxMSqr_15

				--INVM_AJ_LowMass
				result(1)(0) := reg(27);		--MinET1_0
				result(1)(1) := reg(28);		--MinET2_1
				result(1)(2) := reg(29);		--MinMSqr_2
				result(1)(3) := reg(30);		--MaxMSqr_3
				result(1)(4) := reg(31);		--MinET1_4
				result(1)(5) := reg(32);		--MinET2_5
				result(1)(6) := reg(33);		--MinMSqr_6
				result(1)(7) := reg(34);		--MaxMSqr_7
				result(1)(8) := reg(35);		--MinET1_8
				result(1)(9) := reg(36);		--MinET2_9
				result(1)(10) := reg(37);		--MinMSqr_10
				result(1)(11) := reg(38);		--MaxMSqr_11
				result(1)(12) := reg(39);		--MinET1_12
				result(1)(13) := reg(40);		--MinET2_13
				result(1)(14) := reg(41);		--MinMSqr_14
				result(1)(15) := reg(42);		--MaxMSqr_15

				--HT150-J20s5.ETA31
				result(25)(0) := reg(43);		--MinET_0
				result(25)(1) := reg(44);		--MinEta_1
				result(25)(2) := reg(45);		--MaxEta_2
				result(25)(3) := reg(46);		--MinHt_3

				--HT190-J15s5.ETA21
				result(26)(0) := reg(47);		--MinET_0
				result(26)(1) := reg(48);		--MinEta_1
				result(26)(2) := reg(49);		--MaxEta_2
				result(26)(3) := reg(50);		--MinHt_3

				--05MINDPHI-EM12s6-XE0
				result(35)(0) := reg(51);		--MinET1_0
				result(35)(1) := reg(52);		--MinET2_1
				result(35)(2) := reg(53);		--DeltaPhiMin_2

				--05MINDPHI-EM15s6-XE0
				result(36)(0) := reg(54);		--MinET1_0
				result(36)(1) := reg(55);		--MinET2_1
				result(36)(2) := reg(56);		--DeltaPhiMin_2

				--25MT-EM12s6-XE0
				result(37)(0) := reg(57);		--MinET1_0
				result(37)(1) := reg(58);		--MinET2_1
				result(37)(2) := reg(59);		--MinMTSqr_2

				--35MT-EM15s6-XE0
				result(38)(0) := reg(60);		--MinET1_0
				result(38)(1) := reg(61);		--MinET2_1
				result(38)(2) := reg(62);		--MinMTSqr_2

				--10MINDPHI-J20s2-XE50
				result(39)(0) := reg(63);		--MinET1_0
				result(39)(1) := reg(64);		--MinET2_1
				result(39)(2) := reg(65);		--DeltaPhiMin_2

				--100RATIO-0MATCH-TAU30si2-EMall
				result(62)(0) := reg(66);		--MinET1_0
				result(62)(1) := reg(67);		--MinET2_1
				result(62)(2) := reg(68);		--Ratio_2

				--NOT-0MATCH-TAU30si2-EMall
				result(63)(0) := reg(69);		--MinET1_0
				result(63)(1) := reg(70);		--MinET2_1
				result(63)(2) := reg(71);		--EtaMin1_2
				result(63)(3) := reg(72);		--EtaMax1_3
				result(63)(4) := reg(73);		--EtaMin2_4
				result(63)(5) := reg(74);		--EtaMax2_5
				result(63)(6) := reg(75);		--DRCut_6

				--LAR-EM50s1
				result(72)(0) := reg(76);		--MinET_0
				result(72)(1) := reg(77);		--EtaMin_1
				result(72)(2) := reg(78);		--EtaMax_2
				result(72)(3) := reg(79);		--PhiMin_3
				result(72)(4) := reg(80);		--PhiMax_4

				--LAR-J100s1
				result(73)(0) := reg(81);		--MinET_0
				result(73)(1) := reg(82);		--EtaMin_1
				result(73)(2) := reg(83);		--EtaMax_2
				result(73)(3) := reg(84);		--PhiMin_3
				result(73)(4) := reg(85);		--PhiMax_4

				--NOT-02MATCH-EM10s1-AJj15all.ETA49
				result(58)(0) := reg(86);		--MinET1_0
				result(58)(1) := reg(87);		--MinET2_1
				result(58)(2) := reg(88);		--EtaMin1_2
				result(58)(3) := reg(89);		--EtaMax1_3
				result(58)(4) := reg(90);		--EtaMin2_4
				result(58)(5) := reg(91);		--EtaMax2_5
				result(58)(6) := reg(92);		--DRCut_6

				--15MINDPHI-EM12s6-XE0
				result(75)(0) := reg(93);		--MinET1_0
				result(75)(1) := reg(94);		--MinET2_1
				result(75)(2) := reg(95);		--DeltaPhiMin_2

				--15MINDPHI-EM15s6-XE0
				result(76)(0) := reg(96);		--MinET1_0
				result(76)(1) := reg(97);		--MinET2_1
				result(76)(2) := reg(98);		--DeltaPhiMin_2

				--35MT-EM12s6-XE0
				result(77)(0) := reg(99);		--MinET1_0
				result(77)(1) := reg(100);		--MinET2_1
				result(77)(2) := reg(101);		--MinMTSqr_2

				--400INVM9999-AJ30s6.ETA31-AJ20s6.31ETA49
				result(84)(0) := reg(102);		--MinET1_0
				result(84)(1) := reg(103);		--MinET2_1
				result(84)(2) := reg(104);		--MinMSqr_2
				result(84)(3) := reg(105);		--MaxMSqr_3
				result(84)(4) := reg(106);		--MinEta1_4
				result(84)(5) := reg(107);		--MaxEta1_5
				result(84)(6) := reg(108);		--MinEta2_6
				result(84)(7) := reg(109);		--MaxEta2_7

				--INVM_EMs6
				result(31)(0) := reg(110);		--MinET1_0
				result(31)(1) := reg(111);		--MinET2_1
				result(31)(2) := reg(112);		--MinMSqr_2
				result(31)(3) := reg(113);		--MaxMSqr_3
				result(31)(4) := reg(114);		--MinET1_4
				result(31)(5) := reg(115);		--MinET2_5
				result(31)(6) := reg(116);		--MinMSqr_6
				result(31)(7) := reg(117);		--MaxMSqr_7
				result(31)(8) := reg(118);		--MinET1_8
				result(31)(9) := reg(119);		--MinET2_9
				result(31)(10) := reg(120);		--MinMSqr_10
				result(31)(11) := reg(121);		--MaxMSqr_11

				--10MINDPHI-J20s2-XE30
				result(42)(0) := reg(122);		--MinET1_0
				result(42)(1) := reg(123);		--MinET2_1
				result(42)(2) := reg(124);		--DeltaPhiMin_2

				return result;
		end function;
end package body;
