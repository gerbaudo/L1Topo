library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use work.L1TopoDataTypes.all;

entity L1TopoAlgorithms_U1 is
    Port(ClockBus     : in  std_logic_vector(2 downto 0);

         EmTobArray   : in  ClusterArray(InputWidthEM - 1 downto 0);
         TauTobArray  : in  ClusterArray(InputWidthTAU - 1 downto 0);
         JetTobArray  : in  JetArray(InputWidthJET - 1 downto 0);
         MuonTobArray : in  MuonArray(InputWidthMU - 1 downto 0);
         MetTobArray  : in  MetArray(0 downto 0);
         
         Parameters   : in  ParameterSpace(NumberOfAlgorithms - 1 downto 0);
         SortParameters     : in  ParameterSpace(NumberOfSortAlgorithms - 1 downto 0);
         
         Results      : out std_logic_vector(NumberOfResultBits - 1 downto 0);
         Overflow     : out std_logic_vector(NumberOfResultBits - 1 downto 0)
    );
end L1TopoAlgorithms_U1;

architecture Behavioral of L1TopoAlgorithms_U1 is-- Menu Version: 1

-- Module/FPGA : 0/0
-- General configuration (<TopoConfig>):

--  constant AlgoOffset39 : integer := 0;   --   INVM_AJ_HighMass
--  constant AlgoOffset40 : integer := 4;   --   INVM_AJ_LowMass
--  constant AlgoOffset41 : integer := 8;   --   INVM_AJ_VLowMass
--  constant AlgoOffset32 : integer := 9;   --   HT150-J20s5.ETA30
--  constant AlgoOffset31 : integer := 10;   --   HT190-J15s5.ETA20
  constant AlgoOffset37 : integer := 11;   --   INVM_EMall
--  constant AlgoOffset65 : integer := 14;   --   10MINDPHI-EM10s6-XE0
--  constant AlgoOffset66 : integer := 15;   --   15MINDPHI-EM10s6-XE0
--  constant AlgoOffset67 : integer := 16;   --   05MINDPHI-EM15s6-XE0
--  constant AlgoOffset68 : integer := 17;   --   25MT-EM10s6-XE0
--  constant AlgoOffset69 : integer := 18;   --   30MT-EM10s6-XE0
--  constant AlgoOffset70 : integer := 19;   --   35MT-EM15s6-XE0
--  constant AlgoOffset5 : integer := 20;   --   0DETA10-Js1-Js2
--  constant AlgoOffset33 : integer := 21;   --   10MINDPHI-Js2-XE50
--  constant AlgoOffset34 : integer := 22;   --   10MINDPHI-J20s2-XE50
--  constant AlgoOffset78 : integer := 23;   --   210RATIO-0MATCH-TAU30si2-EMall
--  constant AlgoOffset79 : integer := 24;   --   NOT-0MATCH-TAU30si2-EMall
--  constant AlgoOffset90 : integer := 25;   --   LAR-EM50s1
--  constant AlgoOffset91 : integer := 26;   --   LAR-J100s1
--  constant AlgoOffset72 : integer := 27;   --   NOT-02MATCH-EM10s1-AJj15all.ETA49
--  constant AlgoOffset73 : integer := 28;   --   05RATIO-XE0-SUM0-EM10s1-HT0-AJj15all.ETA49

 -- Ordered list of sorted TOBArrays:
  signal EMall  :    TOBArray ((InputWidthEM - 1)  downto 0);
--  signal TAUabi  :    TOBArray ((OutputWidthSelectTAU - 1)  downto 0);
--  signal EMabi  :    TOBArray ((OutputWidthSelectEM - 1)  downto 0);
--  signal EMabhi  :    TOBArray ((OutputWidthSelectEM - 1)  downto 0);
--  signal TAUab  :    TOBArray ((OutputWidthSelectTAU - 1)  downto 0);
--  signal TAUsi  :    TOBArray ((OutputWidthSortTAU - 1)  downto 0);
  signal EMs  :    TOBArray ((OutputWidthSortEM - 1)  downto 0);
--  signal EMshi  :    TOBArray ((OutputWidthSortEM - 1)  downto 0);
--  signal AJall  :    TOBArray ((InputWidthJET - 1)  downto 0);
--  signal AJjall  :    TOBArray ((InputWidthJET - 1)  downto 0);
--  signal AJMatchall  :    TOBArray ((InputWidthJET - 1)  downto 0);
--  signal CJab  :    TOBArray ((OutputWidthSelectJET - 1)  downto 0);
--  signal Jab  :    TOBArray ((OutputWidthSelectJET - 1)  downto 0);
--  signal AJjs  :    TOBArray ((OutputWidthSortJET - 1)  downto 0);
--  signal AJs  :    TOBArray ((OutputWidthSortJET - 1)  downto 0);
--  signal Js  :    TOBArray ((OutputWidthSortJET - 1)  downto 0);
--  signal XENoSort  :    TOBArray ((OutputWidthMET - 1)  downto 0);
--  signal XE  :    TOBArray ((OutputWidthMET - 1)  downto 0);
--  signal CMUab  :    TOBArray ((OutputWidthSelectMU - 1)  downto 0);
--  signal MUab  :    TOBArray ((OutputWidthSelectMU - 1)  downto 0);

begin

sortalgo11 : entity work.ClusterNoSort --  Algorithm name: EMall
  generic map (
     InputWidth => InputWidthEM,
     OutputWidth => InputWidthEM )
  port map (
     ClusterTobArray => EmTobArray,
     TobArrayOut =>  EMall,
     ClockBus => ClockBus,
     Parameters => SortParameters(11)
 );
--
--sortalgo13 : entity work.ClusterSort --  Algorithm name: TAUsi
--  generic map (
--     InputWidth => InputWidthTAU,
--     InputWidth1stStage => InputWidth1stStageSortTAU,
--     OutputWidth => OutputWidthSortTAU,
--     DoIsoCut => 1 )
--  port map (
--     ClusterTobArray => TauTobArray,
--     TobArrayOut =>  TAUsi,
--     ClockBus => ClockBus,
--     Parameters => SortParameters(13)
-- );
--
sortalgo1 : entity work.ClusterSort --  Algorithm name: EMs
  generic map (
     InputWidth => InputWidthEM,
     InputWidth1stStage => InputWidth1stStageSortEM,
     OutputWidth => OutputWidthSortEM,
     DoIsoCut => 0 )
  port map (
     ClusterTobArray => EmTobArray,
     TobArrayOut =>  EMs,
     ClockBus => ClockBus,
     Parameters => SortParameters(1)
 );
--
--sortalgo24 : entity work.JetNoSort --  Algorithm name: AJjall
--  generic map (
--     InputWidth => InputWidthJET,
--     OutputWidth => InputWidthJET,
--     JetSize => 1 )
--  port map (
--     JetTobArray => JetTobArray,
--     TobArrayOut =>  AJjall,
--     ClockBus => ClockBus,
--     Parameters => SortParameters(24)
-- );
--
--sortalgo4 : entity work.JetSort --  Algorithm name: AJs
--  generic map (
--     InputWidth => InputWidthJET,
--     InputWidth1stStage => InputWidth1stStageSortJET,
--     OutputWidth => OutputWidthSortJET,
--     JetSize => 2,
--     DoEtaCut => 0 )
--  port map (
--     JetTobArray => JetTobArray,
--     TobArrayOut =>  AJs,
--     ClockBus => ClockBus,
--     Parameters => SortParameters(4)
-- );
--
--sortalgo5 : entity work.JetSort --  Algorithm name: Js
--  generic map (
--     InputWidth => InputWidthJET,
--     InputWidth1stStage => InputWidth1stStageSortJET,
--     OutputWidth => OutputWidthSortJET,
--     JetSize => 2,
--     DoEtaCut => 1 )
--  port map (
--     JetTobArray => JetTobArray,
--     TobArrayOut =>  Js,
--     ClockBus => ClockBus,
--     Parameters => SortParameters(5)
-- );
--
--sortalgo15 : entity work.MetSort --  Algorithm name: XE
--  generic map (
--     InputWidth => InputWidthMET,
--     OutputWidth => OutputWidthMET )
--  port map (
--     MetTobArray => MetTobArray,
--     TobArrayOut =>  XE,
--     ClockBus => ClockBus,
--     Parameters => SortParameters(15)
-- );
--
--decisionalgo39 : entity work.InvariantMassInclusive1 --  Algorithm name: INVM_AJ_HighMass
--  generic map (
--     InputWidth => OutputWidthSortJET,
--     MaxTob => 6,
--     NumResultBits => 4 )
--  port map (
--     Tob => AJs,
--     Results => Results(AlgoOffset39+3 downto AlgoOffset39), 
--     Overflow => Overflow(AlgoOffset39+3 downto AlgoOffset39), 
--     ClockBus => ClockBus,
--     Parameters => Parameters(39)
-- );
--
--decisionalgo40 : entity work.InvariantMassInclusive1 --  Algorithm name: INVM_AJ_LowMass
--  generic map (
--     InputWidth => OutputWidthSortJET,
--     MaxTob => 6,
--     NumResultBits => 4 )
--  port map (
--     Tob => AJs,
--     Results => Results(AlgoOffset40+3 downto AlgoOffset40), 
--     Overflow => Overflow(AlgoOffset40+3 downto AlgoOffset40), 
--     ClockBus => ClockBus,
--     Parameters => Parameters(40)
-- );
--
--decisionalgo32 : entity work.JetHT --  Algorithm name: HT150-J20s5.ETA30
--  generic map (
--     InputWidth => OutputWidthSortJET,
--     MaxTob => 5,
--     NumRegisters => 0,
--     NumResultBits => 1 )
--  port map (
--     Tob => Js,
--     Results => Results(AlgoOffset32+0 downto AlgoOffset32), 
--     Overflow => Overflow(AlgoOffset32+0 downto AlgoOffset32), 
--     ClockBus => ClockBus,
--     Parameters => Parameters(32)
-- );
--
--decisionalgo31 : entity work.JetHT --  Algorithm name: HT190-J15s5.ETA20
--  generic map (
--     InputWidth => OutputWidthSortJET,
--     MaxTob => 5,
--     NumRegisters => 0,
--     NumResultBits => 1 )
--  port map (
--     Tob => Js,
--     Results => Results(AlgoOffset31+0 downto AlgoOffset31), 
--     Overflow => Overflow(AlgoOffset31+0 downto AlgoOffset31), 
--     ClockBus => ClockBus,
--     Parameters => Parameters(31)
-- );
--
decisionalgo37 : entity work.InvariantMassInclusive2 --  Algorithm name: INVM_EMall
  generic map (
     InputWidth1 => OutputWidthSortEM,
     InputWidth2 => InputWidthEM,
     MaxTob1 => 2,
     MaxTob2 => InputWidthEM,
     NumResultBits => 3,
     NumRegisters => 0 )
  port map (
     Tob1 => EMs,
     Tob2 => EMall,
     Results => Results(AlgoOffset37+2 downto AlgoOffset37), 
     Overflow => Overflow(AlgoOffset37+2 downto AlgoOffset37), 
     ClockBus => ClockBus,
     Parameters => Parameters(37)
 );
--
--decisionalgo65 : entity work.MinDeltaPhiIncl2 --  Algorithm name: 10MINDPHI-EM10s6-XE0
--  generic map (
--     InputWidth1 => OutputWidthSortEM,
--     InputWidth2 => 1,
--     MaxTob1 => 6,
--     MaxTob2 => 1,
--     NumResultBits => 1 )
--  port map (
--     Tob1 => EMs,
--     Tob2 => XE,
--     Results => Results(AlgoOffset65+0 downto AlgoOffset65), 
--     Overflow => Overflow(AlgoOffset65+0 downto AlgoOffset65), 
--     ClockBus => ClockBus,
--     Parameters => Parameters(65)
-- );
--
--decisionalgo66 : entity work.MinDeltaPhiIncl2 --  Algorithm name: 15MINDPHI-EM10s6-XE0
--  generic map (
--     InputWidth1 => OutputWidthSortEM,
--     InputWidth2 => 1,
--     MaxTob1 => 6,
--     MaxTob2 => 1,
--     NumResultBits => 1 )
--  port map (
--     Tob1 => EMs,
--     Tob2 => XE,
--     Results => Results(AlgoOffset66+0 downto AlgoOffset66), 
--     Overflow => Overflow(AlgoOffset66+0 downto AlgoOffset66), 
--     ClockBus => ClockBus,
--     Parameters => Parameters(66)
-- );
--
--decisionalgo67 : entity work.MinDeltaPhiIncl2 --  Algorithm name: 05MINDPHI-EM15s6-XE0
--  generic map (
--     InputWidth1 => OutputWidthSortEM,
--     InputWidth2 => 1,
--     MaxTob1 => 6,
--     MaxTob2 => 1,
--     NumResultBits => 1 )
--  port map (
--     Tob1 => EMs,
--     Tob2 => XE,
--     Results => Results(AlgoOffset67+0 downto AlgoOffset67), 
--     Overflow => Overflow(AlgoOffset67+0 downto AlgoOffset67), 
--     ClockBus => ClockBus,
--     Parameters => Parameters(67)
-- );
--
--decisionalgo68 : entity work.TransverseMassInclusive1 --  Algorithm name: 25MT-EM10s6-XE0
--  generic map (
--     InputWidth => OutputWidthSortEM,
--     MaxTob => 6,
--     NumResultBits => 1 )
--  port map (
--     Tob1 => EMs,
--     Tob2 => XE,
--     Results => Results(AlgoOffset68+0 downto AlgoOffset68), 
--     Overflow => Overflow(AlgoOffset68+0 downto AlgoOffset68), 
--     ClockBus => ClockBus,
--     Parameters => Parameters(68)
-- );
--
--decisionalgo69 : entity work.TransverseMassInclusive1 --  Algorithm name: 30MT-EM10s6-XE0
--  generic map (
--     InputWidth => OutputWidthSortEM,
--     MaxTob => 6,
--     NumResultBits => 1 )
--  port map (
--     Tob1 => EMs,
--     Tob2 => XE,
--     Results => Results(AlgoOffset69+0 downto AlgoOffset69), 
--     Overflow => Overflow(AlgoOffset69+0 downto AlgoOffset69), 
--     ClockBus => ClockBus,
--     Parameters => Parameters(69)
-- );
--
--decisionalgo70 : entity work.TransverseMassInclusive1 --  Algorithm name: 35MT-EM15s6-XE0
--  generic map (
--     InputWidth => OutputWidthSortEM,
--     MaxTob => 6,
--     NumResultBits => 1 )
--  port map (
--     Tob1 => EMs,
--     Tob2 => XE,
--     Results => Results(AlgoOffset70+0 downto AlgoOffset70), 
--     Overflow => Overflow(AlgoOffset70+0 downto AlgoOffset70), 
--     ClockBus => ClockBus,
--     Parameters => Parameters(70)
-- );
--
--decisionalgo5 : entity work.DeltaEtaIncl1 --  Algorithm name: 0DETA10-Js1-Js2
--  generic map (
--     InputWidth => OutputWidthSortJET,
--     MaxTob => 2,
--     NumResultBits => 1 )
--  port map (
--     Tob => Js,
--     Results => Results(AlgoOffset5+0 downto AlgoOffset5), 
--     Overflow => Overflow(AlgoOffset5+0 downto AlgoOffset5), 
--     ClockBus => ClockBus,
--     Parameters => Parameters(5)
-- );
--
--decisionalgo33 : entity work.MinDeltaPhiIncl2 --  Algorithm name: 10MINDPHI-Js2-XE50
--  generic map (
--     InputWidth1 => OutputWidthSortJET,
--     InputWidth2 => 1,
--     MaxTob1 => 2,
--     MaxTob2 => 1,
--     NumResultBits => 1 )
--  port map (
--     Tob1 => Js,
--     Tob2 => XE,
--     Results => Results(AlgoOffset33+0 downto AlgoOffset33), 
--     Overflow => Overflow(AlgoOffset33+0 downto AlgoOffset33), 
--     ClockBus => ClockBus,
--     Parameters => Parameters(33)
-- );
--
--decisionalgo34 : entity work.MinDeltaPhiIncl2 --  Algorithm name: 10MINDPHI-J20s2-XE50
--  generic map (
--     InputWidth1 => OutputWidthSortJET,
--     InputWidth2 => 1,
--     MaxTob1 => 2,
--     MaxTob2 => 1,
--     NumResultBits => 1 )
--  port map (
--     Tob1 => Js,
--     Tob2 => XE,
--     Results => Results(AlgoOffset34+0 downto AlgoOffset34), 
--     Overflow => Overflow(AlgoOffset34+0 downto AlgoOffset34), 
--     ClockBus => ClockBus,
--     Parameters => Parameters(34)
-- );
--
--decisionalgo78 : entity work.RatioMatch --  Algorithm name: 210RATIO-0MATCH-TAU30si2-EMall
--  generic map (
--     InputWidth1 => OutputWidthSortTAU,
--     InputWidth2 => InputWidthEM,
--     MaxTob1 => 2,
--     MaxTob2 => InputWidthEM,
--     NumResultBits => 1 )
--  port map (
--     Tob1 => TAUsi,
--     Tob2 => EMall,
--     Results => Results(AlgoOffset78+0 downto AlgoOffset78), 
--     Overflow => Overflow(AlgoOffset78+0 downto AlgoOffset78), 
--     ClockBus => ClockBus,
--     Parameters => Parameters(78)
-- );
--
--decisionalgo79 : entity work.NotMatch --  Algorithm name: NOT-0MATCH-TAU30si2-EMall
--  generic map (
--     InputWidth1 => OutputWidthSortTAU,
--     InputWidth2 => InputWidthEM,
--     MaxTob1 => 2,
--     MaxTob2 => InputWidthEM,
--     NumResultBits => 1 )
--  port map (
--     Tob1 => TAUsi,
--     Tob2 => EMall,
--     Results => Results(AlgoOffset79+0 downto AlgoOffset79), 
--     Overflow => Overflow(AlgoOffset79+0 downto AlgoOffset79), 
--     ClockBus => ClockBus,
--     Parameters => Parameters(79)
-- );
--
--decisionalgo90 : entity work.LAr --  Algorithm name: LAR-EM50s1
--  generic map (
--     InputWidth => OutputWidthSortEM,
--     MaxTob => 1,
--     NumResultBits => 1 )
--  port map (
--     Tob => EMs,
--     Results => Results(AlgoOffset90+0 downto AlgoOffset90), 
--     Overflow => Overflow(AlgoOffset90+0 downto AlgoOffset90), 
--     ClockBus => ClockBus,
--     Parameters => Parameters(90)
-- );
--
--decisionalgo91 : entity work.LAr --  Algorithm name: LAR-J100s1
--  generic map (
--     InputWidth => OutputWidthSortJET,
--     MaxTob => 1,
--     NumResultBits => 1 )
--  port map (
--     Tob => Js,
--     Results => Results(AlgoOffset91+0 downto AlgoOffset91), 
--     Overflow => Overflow(AlgoOffset91+0 downto AlgoOffset91), 
--     ClockBus => ClockBus,
--     Parameters => Parameters(91)
-- );
--
--decisionalgo72 : entity work.NotMatch --  Algorithm name: NOT-02MATCH-EM10s1-AJj15all.ETA49
--  generic map (
--     InputWidth1 => OutputWidthSortEM,
--     InputWidth2 => InputWidthJET,
--     MaxTob1 => 1,
--     MaxTob2 => InputWidthJET,
--     NumResultBits => 1 )
--  port map (
--     Tob1 => EMs,
--     Tob2 => AJjall,
--     Results => Results(AlgoOffset72+0 downto AlgoOffset72), 
--     Overflow => Overflow(AlgoOffset72+0 downto AlgoOffset72), 
--     ClockBus => ClockBus,
--     Parameters => Parameters(72)
-- );
--
--decisionalgo73 : entity work.RatioSum --  Algorithm name: 05RATIO-XE0-SUM0-EM10s1-HT0-AJj15all.ETA49
--  generic map (
--     InputWidth1 => 1,
--     InputWidth2 => InputWidthJET,
--     InputWidth3 => OutputWidthSortEM,
--     MaxTob1 => 1,
--     MaxTob2 => InputWidthJET,
--     MaxTob3 => 1,
--     NumResultBits => 1,
--     UseCluster05Granularity => 1 )
--  port map (
--     Tob1 => XE,
--     Tob2 => AJjall,
--     Tob3 => EMs,
--     Results => Results(AlgoOffset73+0 downto AlgoOffset73), 
--     Overflow => Overflow(AlgoOffset73+0 downto AlgoOffset73), 
--     ClockBus => ClockBus,
--     Parameters => Parameters(73)
-- );

end Behavioral;