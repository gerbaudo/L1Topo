library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use work.L1TopoDataTypes.all;

entity L1TopoAlgorithms_U1 is
    Port(ClockBus     : in  std_logic_vector(2 downto 0);

         EmTobArray   : in  ClusterArray(InputWidthEM - 1 downto 0);
         TauTobArray  : in  ClusterArray(InputWidthTAU - 1 downto 0);
         JetTobArray  : in  JetArray(InputWidthJET - 1 downto 0);
         MuonTobArray : in  MuonArray(InputWidthMU - 1 downto 0);
         MetTobArray  : in  MetArray(0 downto 0);
         
         Parameters   : in  ParameterSpace(NumberOfAlgorithms - 1 downto 0);
         SortParameters     : in  ParameterSpace(NumberOfSortAlgorithms - 1 downto 0);
         
         Results      : out std_logic_vector(NumberOfResultBits - 1 downto 0);
         Overflow     : out std_logic_vector(NumberOfResultBits - 1 downto 0)
    );
end L1TopoAlgorithms_U1;

architecture Behavioral of L1TopoAlgorithms_U1 is-- Menu Version: 1

-- Module/FPGA : 0/0
-- General configuration (<TopoConfig>):

  constant AlgoOffset0 : integer := 0;   --   INVM_AJ_HighMass Firstbit=0 clock=0
  constant AlgoOffset1 : integer := 4;   --   INVM_AJ_LowMass Firstbit=4 clock=0
  constant AlgoOffset25 : integer := 9;   --   HT150-J20s5.ETA31 Firstbit=9 clock=0
  constant AlgoOffset26 : integer := 10;   --   HT190-J15s5.ETA21 Firstbit=10 clock=0
  constant AlgoOffset35 : integer := 14;   --   05MINDPHI-EM12s6-XE0 Firstbit=14 clock=0
  constant AlgoOffset36 : integer := 16;   --   05MINDPHI-EM15s6-XE0 Firstbit=0 clock=1
  constant AlgoOffset37 : integer := 17;   --   25MT-EM12s6-XE0 Firstbit=1 clock=1
  constant AlgoOffset38 : integer := 19;   --   35MT-EM15s6-XE0 Firstbit=3 clock=1
  constant AlgoOffset39 : integer := 22;   --   10MINDPHI-J20s2-XE50 Firstbit=6 clock=1
  constant AlgoOffset62 : integer := 23;   --   100RATIO-0MATCH-TAU30si2-EMall Firstbit=7 clock=1
  constant AlgoOffset63 : integer := 24;   --   NOT-0MATCH-TAU30si2-EMall Firstbit=8 clock=1
  constant AlgoOffset72 : integer := 25;   --   LAR-EM50s1 Firstbit=9 clock=1
  constant AlgoOffset73 : integer := 26;   --   LAR-J100s1 Firstbit=10 clock=1
  constant AlgoOffset58 : integer := 27;   --   NOT-02MATCH-EM10s1-AJj15all.ETA49 Firstbit=11 clock=1
  constant AlgoOffset75 : integer := 30;   --   15MINDPHI-EM12s6-XE0 Firstbit=14 clock=1
  constant AlgoOffset76 : integer := 31;   --   15MINDPHI-EM15s6-XE0 Firstbit=15 clock=1
  constant AlgoOffset77 : integer := 29;   --   35MT-EM12s6-XE0 Firstbit=13 clock=1
  constant AlgoOffset84 : integer := 15;   --   400INVM9999-AJ30s6.ETA31-AJ20s6.31ETA49 Firstbit=15 clock=0
  constant AlgoOffset31 : integer := 11;   --   INVM_EMs6 Firstbit=11 clock=0
  constant AlgoOffset42 : integer := 21;   --   10MINDPHI-J20s2-XE30 Firstbit=5 clock=1
 -- Ordered list of sorted TOBArrays:
  signal EMall  :    TOBArray ((InputWidthEM - 1)  downto 0);
  signal TAUabi  :    TOBArray ((OutputWidthSelectTAU - 1)  downto 0);
  signal EMabi  :    TOBArray ((OutputWidthSelectEM - 1)  downto 0);
  signal TAUab  :    TOBArray ((OutputWidthSelectTAU - 1)  downto 0);
  signal EMs  :    TOBArray ((OutputWidthSortEM - 1)  downto 0);
  signal EMshi  :    TOBArray ((OutputWidthSortEM - 1)  downto 0);
  signal TAUsi  :    TOBArray ((OutputWidthSortTAU - 1)  downto 0);
  signal AJall  :    TOBArray ((InputWidthJET - 1)  downto 0);
  signal AJjall  :    TOBArray ((InputWidthJET - 1)  downto 0);
  signal AJMatchall  :    TOBArray ((InputWidthJET - 1)  downto 0);
  signal Jab  :    TOBArray ((OutputWidthSelectJET - 1)  downto 0);
  signal CJab  :    TOBArray ((OutputWidthSelectJET - 1)  downto 0);
  signal AJjs  :    TOBArray ((OutputWidthSortJET - 1)  downto 0);
  signal AJs  :    TOBArray ((OutputWidthSortJET - 1)  downto 0);
  signal Js  :    TOBArray ((OutputWidthSortJET - 1)  downto 0);
  signal XENoSort  :    TOBArray ((OutputWidthMET - 1)  downto 0);
  signal XE  :    TOBArray ((OutputWidthMET - 1)  downto 0);
  signal MUab  :    TOBArray ((OutputWidthSelectMU - 1)  downto 0);
  signal CMUab  :    TOBArray ((OutputWidthSelectMU - 1)  downto 0);
  signal LMUs  :    TOBArray ((OutputWidthSortMU - 1)  downto 0);

begin

sortalgo0 : entity work.ClusterNoSort --  Algorithm name: EMall
  generic map (
     InputWidth => InputWidthEM,
     OutputWidth => InputWidthEM )
  port map (
     ClusterTobArray => EmTobArray,
     TobArrayOut =>  EMall,
     ClockBus => ClockBus,
     Parameters => SortParameters(0)
 );

sortalgo5 : entity work.ClusterSort --  Algorithm name: EMs
  generic map (
     InputWidth => InputWidthEM,
     InputWidth1stStage => InputWidth1stStageSortEM,
     OutputWidth => OutputWidthSortEM,
     DoIsoCut => 0 )
  port map (
     ClusterTobArray => EmTobArray,
     TobArrayOut =>  EMs,
     ClockBus => ClockBus,
     Parameters => SortParameters(5)
 );

sortalgo7 : entity work.ClusterSort --  Algorithm name: TAUsi
  generic map (
     InputWidth => InputWidthTAU,
     InputWidth1stStage => InputWidth1stStageSortTAU,
     OutputWidth => OutputWidthSortTAU,
     DoIsoCut => 1 )
  port map (
     ClusterTobArray => TauTobArray,
     TobArrayOut =>  TAUsi,
     ClockBus => ClockBus,
     Parameters => SortParameters(7)
 );

sortalgo9 : entity work.JetNoSort --  Algorithm name: AJjall
  generic map (
     InputWidth => InputWidthJET,
     OutputWidth => InputWidthJET,
     JetSize => 1 )
  port map (
     JetTobArray => JetTobArray,
     TobArrayOut =>  AJjall,
     ClockBus => ClockBus,
     Parameters => SortParameters(9)
 );

sortalgo14 : entity work.JetSort --  Algorithm name: AJs
  generic map (
     InputWidth => InputWidthJET,
     InputWidth1stStage => InputWidth1stStageSortJET,
     OutputWidth => OutputWidthSortJET,
     JetSize => 2,
     DoEtaCut => 0 )
  port map (
     JetTobArray => JetTobArray,
     TobArrayOut =>  AJs,
     ClockBus => ClockBus,
     Parameters => SortParameters(14)
 );

sortalgo15 : entity work.JetSort --  Algorithm name: Js
  generic map (
     InputWidth => InputWidthJET,
     InputWidth1stStage => InputWidth1stStageSortJET,
     OutputWidth => OutputWidthSortJET,
     JetSize => 2,
     DoEtaCut => 1 )
  port map (
     JetTobArray => JetTobArray,
     TobArrayOut =>  Js,
     ClockBus => ClockBus,
     Parameters => SortParameters(15)
 );

sortalgo17 : entity work.MetSort --  Algorithm name: XE
  generic map (
     InputWidth => InputWidthMET,
     OutputWidth => OutputWidthMET )
  port map (
     MetTobArray => MetTobArray,
     TobArrayOut =>  XE,
     ClockBus => ClockBus,
     Parameters => SortParameters(17)
 );

decisionalgo0 : entity work.InvariantMassInclusive1 --  Algorithm name: INVM_AJ_HighMass
  generic map (
     InputWidth => OutputWidthSortJET,
     MaxTob => 6,
     NumResultBits => 4 )
  port map (
     Tob => AJs,
     Results => Results(AlgoOffset0+3 downto AlgoOffset0), 
     Overflow => Overflow(AlgoOffset0+3 downto AlgoOffset0), 
     ClockBus => ClockBus,
     Parameters => Parameters(0)
 );

decisionalgo1 : entity work.InvariantMassInclusive1 --  Algorithm name: INVM_AJ_LowMass
  generic map (
     InputWidth => OutputWidthSortJET,
     MaxTob => 6,
     NumResultBits => 4 )
  port map (
     Tob => AJs,
     Results => Results(AlgoOffset1+3 downto AlgoOffset1), 
     Overflow => Overflow(AlgoOffset1+3 downto AlgoOffset1), 
     ClockBus => ClockBus,
     Parameters => Parameters(1)
 );

decisionalgo25 : entity work.JetHT --  Algorithm name: HT150-J20s5.ETA31
  generic map (
     InputWidth => OutputWidthSortJET,
     MaxTob => 5,
     NumRegisters => 0,
     NumResultBits => 1 )
  port map (
     Tob => Js,
     Results => Results(AlgoOffset25+0 downto AlgoOffset25), 
     Overflow => Overflow(AlgoOffset25+0 downto AlgoOffset25), 
     ClockBus => ClockBus,
     Parameters => Parameters(25)
 );

decisionalgo26 : entity work.JetHT --  Algorithm name: HT190-J15s5.ETA21
  generic map (
     InputWidth => OutputWidthSortJET,
     MaxTob => 5,
     NumRegisters => 0,
     NumResultBits => 1 )
  port map (
     Tob => Js,
     Results => Results(AlgoOffset26+0 downto AlgoOffset26), 
     Overflow => Overflow(AlgoOffset26+0 downto AlgoOffset26), 
     ClockBus => ClockBus,
     Parameters => Parameters(26)
 );

decisionalgo35 : entity work.MinDeltaPhiIncl2 --  Algorithm name: 05MINDPHI-EM12s6-XE0
  generic map (
     InputWidth1 => OutputWidthSortEM,
     InputWidth2 => 1,
     MaxTob1 => 6,
     MaxTob2 => 1,
     NumResultBits => 1 )
  port map (
     Tob1 => EMs,
     Tob2 => XE,
     Results => Results(AlgoOffset35+0 downto AlgoOffset35), 
     Overflow => Overflow(AlgoOffset35+0 downto AlgoOffset35), 
     ClockBus => ClockBus,
     Parameters => Parameters(35)
 );

decisionalgo36 : entity work.MinDeltaPhiIncl2 --  Algorithm name: 05MINDPHI-EM15s6-XE0
  generic map (
     InputWidth1 => OutputWidthSortEM,
     InputWidth2 => 1,
     MaxTob1 => 6,
     MaxTob2 => 1,
     NumResultBits => 1 )
  port map (
     Tob1 => EMs,
     Tob2 => XE,
     Results => Results(AlgoOffset36+0 downto AlgoOffset36), 
     Overflow => Overflow(AlgoOffset36+0 downto AlgoOffset36), 
     ClockBus => ClockBus,
     Parameters => Parameters(36)
 );

decisionalgo37 : entity work.TransverseMassInclusive1 --  Algorithm name: 25MT-EM12s6-XE0
  generic map (
     InputWidth => OutputWidthSortEM,
     MaxTob => 6,
     NumResultBits => 1 )
  port map (
     Tob1 => EMs,
     Tob2 => XE,
     Results => Results(AlgoOffset37+0 downto AlgoOffset37), 
     Overflow => Overflow(AlgoOffset37+0 downto AlgoOffset37), 
     ClockBus => ClockBus,
     Parameters => Parameters(37)
 );

decisionalgo38 : entity work.TransverseMassInclusive1 --  Algorithm name: 35MT-EM15s6-XE0
  generic map (
     InputWidth => OutputWidthSortEM,
     MaxTob => 6,
     NumResultBits => 1 )
  port map (
     Tob1 => EMs,
     Tob2 => XE,
     Results => Results(AlgoOffset38+0 downto AlgoOffset38), 
     Overflow => Overflow(AlgoOffset38+0 downto AlgoOffset38), 
     ClockBus => ClockBus,
     Parameters => Parameters(38)
 );

decisionalgo39 : entity work.MinDeltaPhiIncl2 --  Algorithm name: 10MINDPHI-J20s2-XE50
  generic map (
     InputWidth1 => OutputWidthSortJET,
     InputWidth2 => 1,
     MaxTob1 => 2,
     MaxTob2 => 1,
     NumResultBits => 1 )
  port map (
     Tob1 => Js,
     Tob2 => XE,
     Results => Results(AlgoOffset39+0 downto AlgoOffset39), 
     Overflow => Overflow(AlgoOffset39+0 downto AlgoOffset39), 
     ClockBus => ClockBus,
     Parameters => Parameters(39)
 );

decisionalgo62 : entity work.RatioMatch --  Algorithm name: 100RATIO-0MATCH-TAU30si2-EMall
  generic map (
     InputWidth1 => OutputWidthSortTAU,
     InputWidth2 => InputWidthEM,
     MaxTob1 => 2,
     MaxTob2 => InputWidthEM,
     NumResultBits => 1 )
  port map (
     Tob1 => TAUsi,
     Tob2 => EMall,
     Results => Results(AlgoOffset62+0 downto AlgoOffset62), 
     Overflow => Overflow(AlgoOffset62+0 downto AlgoOffset62), 
     ClockBus => ClockBus,
     Parameters => Parameters(62)
 );

decisionalgo63 : entity work.NotMatch --  Algorithm name: NOT-0MATCH-TAU30si2-EMall
  generic map (
     InputWidth1 => OutputWidthSortTAU,
     InputWidth2 => InputWidthEM,
     MaxTob1 => 2,
     MaxTob2 => InputWidthEM,
     NumResultBits => 1 )
  port map (
     Tob1 => TAUsi,
     Tob2 => EMall,
     Results => Results(AlgoOffset63+0 downto AlgoOffset63), 
     Overflow => Overflow(AlgoOffset63+0 downto AlgoOffset63), 
     ClockBus => ClockBus,
     Parameters => Parameters(63)
 );

decisionalgo72 : entity work.LAr --  Algorithm name: LAR-EM50s1
  generic map (
     InputWidth => OutputWidthSortEM,
     MaxTob => 1,
     NumResultBits => 1 )
  port map (
     Tob => EMs,
     Results => Results(AlgoOffset72+0 downto AlgoOffset72), 
     Overflow => Overflow(AlgoOffset72+0 downto AlgoOffset72), 
     ClockBus => ClockBus,
     Parameters => Parameters(72)
 );

decisionalgo73 : entity work.LAr --  Algorithm name: LAR-J100s1
  generic map (
     InputWidth => OutputWidthSortJET,
     MaxTob => 1,
     NumResultBits => 1 )
  port map (
     Tob => Js,
     Results => Results(AlgoOffset73+0 downto AlgoOffset73), 
     Overflow => Overflow(AlgoOffset73+0 downto AlgoOffset73), 
     ClockBus => ClockBus,
     Parameters => Parameters(73)
 );

decisionalgo58 : entity work.NotMatch --  Algorithm name: NOT-02MATCH-EM10s1-AJj15all.ETA49
  generic map (
     InputWidth1 => OutputWidthSortEM,
     InputWidth2 => InputWidthJET,
     MaxTob1 => 1,
     MaxTob2 => InputWidthJET,
     NumResultBits => 1 )
  port map (
     Tob1 => EMs,
     Tob2 => AJjall,
     Results => Results(AlgoOffset58+0 downto AlgoOffset58), 
     Overflow => Overflow(AlgoOffset58+0 downto AlgoOffset58), 
     ClockBus => ClockBus,
     Parameters => Parameters(58)
 );

decisionalgo75 : entity work.MinDeltaPhiIncl2 --  Algorithm name: 15MINDPHI-EM12s6-XE0
  generic map (
     InputWidth1 => OutputWidthSortEM,
     InputWidth2 => 1,
     MaxTob1 => 6,
     MaxTob2 => 1,
     NumResultBits => 1 )
  port map (
     Tob1 => EMs,
     Tob2 => XE,
     Results => Results(AlgoOffset75+0 downto AlgoOffset75), 
     Overflow => Overflow(AlgoOffset75+0 downto AlgoOffset75), 
     ClockBus => ClockBus,
     Parameters => Parameters(75)
 );

decisionalgo76 : entity work.MinDeltaPhiIncl2 --  Algorithm name: 15MINDPHI-EM15s6-XE0
  generic map (
     InputWidth1 => OutputWidthSortEM,
     InputWidth2 => 1,
     MaxTob1 => 6,
     MaxTob2 => 1,
     NumResultBits => 1 )
  port map (
     Tob1 => EMs,
     Tob2 => XE,
     Results => Results(AlgoOffset76+0 downto AlgoOffset76), 
     Overflow => Overflow(AlgoOffset76+0 downto AlgoOffset76), 
     ClockBus => ClockBus,
     Parameters => Parameters(76)
 );

decisionalgo77 : entity work.TransverseMassInclusive1 --  Algorithm name: 35MT-EM12s6-XE0
  generic map (
     InputWidth => OutputWidthSortEM,
     MaxTob => 6,
     NumResultBits => 1 )
  port map (
     Tob1 => EMs,
     Tob2 => XE,
     Results => Results(AlgoOffset77+0 downto AlgoOffset77), 
     Overflow => Overflow(AlgoOffset77+0 downto AlgoOffset77), 
     ClockBus => ClockBus,
     Parameters => Parameters(77)
 );

decisionalgo84 : entity work.InvariantMassInclusive2 --  Algorithm name: 400INVM9999-AJ30s6.ETA31-AJ20s6.31ETA49
  generic map (
     InputWidth1 => OutputWidthSortJET,
     InputWidth2 => OutputWidthSortJET,
     MaxTob1 => 6,
     MaxTob2 => 6,
     NumResultBits => 1,
     ApplyEtaCut => 1 )
  port map (
     Tob1 => AJs,
     Tob2 => AJs,
     Results => Results(AlgoOffset84+0 downto AlgoOffset84), 
     Overflow => Overflow(AlgoOffset84+0 downto AlgoOffset84), 
     ClockBus => ClockBus,
     Parameters => Parameters(84)
 );

decisionalgo31 : entity work.InvariantMassInclusive2 --  Algorithm name: INVM_EMs6
  generic map (
     InputWidth1 => OutputWidthSortEM,
     InputWidth2 => OutputWidthSortEM,
     MaxTob1 => 1,
     MaxTob2 => OutputWidthSortEM,
     NumResultBits => 3 )
  port map (
     Tob1 => EMs,
     Tob2 => EMs,
     Results => Results(AlgoOffset31+2 downto AlgoOffset31), 
     Overflow => Overflow(AlgoOffset31+2 downto AlgoOffset31), 
     ClockBus => ClockBus,
     Parameters => Parameters(31)
 );

decisionalgo42 : entity work.MinDeltaPhiIncl2 --  Algorithm name: 10MINDPHI-J20s2-XE30
  generic map (
     InputWidth1 => OutputWidthSortJET,
     InputWidth2 => 1,
     MaxTob1 => 2,
     MaxTob2 => 1,
     NumResultBits => 1 )
  port map (
     Tob1 => Js,
     Tob2 => XE,
     Results => Results(AlgoOffset42+0 downto AlgoOffset42), 
     Overflow => Overflow(AlgoOffset42+0 downto AlgoOffset42), 
     ClockBus => ClockBus,
     Parameters => Parameters(42)
 );

end Behavioral;