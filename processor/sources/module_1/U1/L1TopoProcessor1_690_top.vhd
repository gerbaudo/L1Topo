----------------------------------------------------------------------------------
-- Company: Johannes Gutenberg-Universitaet Mainz
-- Engineers: Christian Kahra, ...
-- 
-- Create Date: 14.07.2014 16:06:32
-- Design Name: L1TopoProcessor
-- Module Name: top_L1TopoProcessor - Behavioral
-- Project Name: Atlas Level-1 Topological Processor
-- Target Devices: 
-- Tool Versions: Vivado 14.2
-- Description: top module
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.L1TopoDataTypes.all;
use work.L1TopoFunctions.all;

entity top_L1TopoProcessor_1 is
    generic(
        PROCESSOR_NUMBER: natural := 1
    );
	port(
		GCK1_P, GCK1_N               : in  std_logic;
		GCK2_P, GCK2_N               : in  std_logic;

		MGT2_CLK_P, MGT2_CLK_N       : in  std_logic_vector(11 downto 1);
		MGT4_CLK_P, MGT4_CLK_N       : in  std_logic_vector(11 downto 1);

		RxP                          : in  std_logic_vector(numberOfChannels-1 downto 0);
		RxN                          : in  std_logic_vector(numberOfChannels-1 downto 0);
                TxP                          : out std_logic_vector(3 downto 0);
                TxN                          : out std_logic_vector(3 downto 0);
--		CTRLBUS_IN_P, CTRLBUS_IN_N   : in  std_logic_vector(6 downto 0);
--		CTRLBUS_OUT_P, CTRLBUS_OUT_N : out std_logic_vector(20 downto 9);

        CTRLBUS_0_P,  CTRLBUS_0_N:    in  std_logic;
		CTRLBUS_1_P,  CTRLBUS_1_N:    in  std_logic;
		CTRLBUS_2_P,  CTRLBUS_2_N:    in  std_logic;
		CTRLBUS_3_P,  CTRLBUS_3_N:    in  std_logic;
		CTRLBUS_4_P,  CTRLBUS_4_N:    in  std_logic;
		CTRLBUS_5_P,  CTRLBUS_5_N:    in  std_logic;
		CTRLBUS_6_P,  CTRLBUS_6_N:    out std_logic;
		CTRLBUS_7_P,  CTRLBUS_7_N:    out std_logic;
		CTRLBUS_8_P,  CTRLBUS_8_N:    in  std_logic;
		CTRLBUS_9_P,  CTRLBUS_9_N:    out std_logic;
		CTRLBUS_10_P, CTRLBUS_10_N:   out std_logic;
		CTRLBUS_11_P, CTRLBUS_11_N:   out std_logic;
		CTRLBUS_12_P, CTRLBUS_12_N:   out std_logic;
		CTRLBUS_13_P, CTRLBUS_13_N:   out std_logic;
		CTRLBUS_14_P, CTRLBUS_14_N:   out std_logic;
		CTRLBUS_15_P, CTRLBUS_15_N:   out std_logic;
		CTRLBUS_16_P, CTRLBUS_16_N:   out std_logic;
		CTRLBUS_17_P, CTRLBUS_17_N:   out std_logic;
		CTRLBUS_18_P, CTRLBUS_18_N:   out std_logic;
		CTRLBUS_19_P, CTRLBUS_19_N:   out std_logic;
		CTRLBUS_20_P, CTRLBUS_20_N:   out std_logic;

		EXT_V7_P, EXT_V7_N           : out std_logic_vector(16+ctpOutputLinesOffset(PROCESSOR_NUMBER)-1 downto ctpOutputLinesOffset(PROCESSOR_NUMBER))
	);
end top_L1TopoProcessor_1;


architecture Behavioral of top_L1TopoProcessor_1 is

    signal ipbBridgeRequest_p, ipbBridgeRequest_n:      std_logic_vector(4 downto 0);
    signal ipbBridgeResponse_p, ipbBridgeResponse_n:    std_logic_vector(2 downto 0);
    signal ttcBridge_p, ttcBridge_n:                    std_logic;
    signal rodBridge_p, rodBridge_n:                    std_logic_vector(7 downto 0);
    signal l1_accepted_in_p, l1_accepted_in_n:          std_logic;
    signal l0_busy_p, l0_busy_n:                        std_logic;
    signal sysclk40_p, sysclk40_n:                      std_logic;
    signal crystalclk40_p, crystalclk40_n:              std_logic;

begin


    ipbBridgeRequest_p(0) <= CTRLBUS_0_P;
    ipbBridgeRequest_n(0) <= CTRLBUS_0_N;
    ipbBridgeRequest_p(1) <= CTRLBUS_1_P;
    ipbBridgeRequest_n(1) <= CTRLBUS_1_N;
    ipbBridgeRequest_p(2) <= CTRLBUS_2_P;
    ipbBridgeRequest_n(2) <= CTRLBUS_2_N;
    ipbBridgeRequest_p(3) <= CTRLBUS_3_P;
    ipbBridgeRequest_n(3) <= CTRLBUS_3_N;
    ipbBridgeRequest_p(4) <= CTRLBUS_4_P;
    ipbBridgeRequest_n(4) <= CTRLBUS_4_N;    

    ttcBridge_p <= CTRLBUS_5_P;
    ttcBridge_n <= CTRLBUS_5_N;

    CTRLBUS_6_P <= l0_busy_p;
    CTRLBUS_6_N <= l0_busy_n;

    CTRLBUS_7_P <= crystalclk40_p;
    CTRLBUS_7_N <= crystalclk40_n;
    
    l1_accepted_in_p <= CTRLBUS_8_P;
    l1_accepted_in_n <= CTRLBUS_8_N;
    
    CTRLBUS_9_P  <= rodBridge_p(0);
    CTRLBUS_9_N  <= rodBridge_n(0);
    CTRLBUS_10_P <= rodBridge_p(1);
    CTRLBUS_10_N <= rodBridge_n(1);
    CTRLBUS_11_P <= rodBridge_p(2);
    CTRLBUS_11_N <= rodBridge_n(2);
    
    CTRLBUS_12_P <= sysclk40_p;
    CTRLBUS_12_N <= sysclk40_n;
    
    CTRLBUS_13_P <= rodBridge_p(3);
    CTRLBUS_13_N <= rodBridge_n(3);
    CTRLBUS_14_P <= rodBridge_p(4);
    CTRLBUS_14_N <= rodBridge_n(4);
    CTRLBUS_15_P <= rodBridge_p(5);
    CTRLBUS_15_N <= rodBridge_n(5);
    CTRLBUS_16_P <= rodBridge_p(6);
    CTRLBUS_16_N <= rodBridge_n(6);
    CTRLBUS_17_P <= rodBridge_p(7);
    CTRLBUS_17_N <= rodBridge_n(7);
    
    CTRLBUS_18_P <= ipbBridgeResponse_p(0);
    CTRLBUS_18_N <= ipbBridgeResponse_n(0);
    CTRLBUS_19_P <= ipbBridgeResponse_p(1);
    CTRLBUS_19_N <= ipbBridgeResponse_n(1);
    CTRLBUS_20_P <= ipbBridgeResponse_p(2);
    CTRLBUS_20_N <= ipbBridgeResponse_n(2);



	L1TopoProcessor1 : entity work.top_L1TopoProcessor
		generic map(
			PROCESSOR_NUMBER => PROCESSOR_NUMBER,
			SIMULATION => false
		)
		port map(
			GCK1_P => GCK1_P,
			GCK1_N => GCK1_N,
			GCK2_P => GCK2_P,
			GCK2_N => GCK2_N,
			MGT2_CLK_P => MGT2_CLK_P,
			MGT2_CLK_N => MGT2_CLK_N,
			MGT4_CLK_P => MGT4_CLK_P,
			MGT4_CLK_N => MGT4_CLK_N,
			RxP => RxP,
			RxN => RxN,
                        TxP => TxP,
			TxN => TxN,
--			CTRLBUS_IN_P => CTRLBUS_IN_P,
--			CTRLBUS_IN_N => CTRLBUS_IN_N,
--			CTRLBUS_OUT_P => CTRLBUS_OUT_P,
--			CTRLBUS_OUT_N => CTRLBUS_OUT_N,

            ipbBridgeRequest_p    => ipbBridgeRequest_p,
            ipbBridgeRequest_n    => ipbBridgeRequest_n,
            ipbBridgeResponse_p   => ipbBridgeResponse_p,
            ipbBridgeResponse_n   => ipbBridgeResponse_n,
            ttcBridge_p           => ttcBridge_p,
            ttcBridge_n           => ttcBridge_n,    
            rodBridge_p           => rodBridge_p,
            rodBridge_n           => rodBridge_n,    
            l1_accepted_in_p      => l1_accepted_in_p,
            l1_accepted_in_n      => l1_accepted_in_n,
            l0_busy_p             => l0_busy_p,
            l0_busy_n             => l0_busy_n,      
            sysclk40_p            => sysclk40_p,
            sysclk40_n            => sysclk40_n,     
            crystalclk40_p        => crystalclk40_p,
            crystalclk40_n        => crystalclk40_n, 

			EXT_V7_P => EXT_V7_P,
			EXT_V7_N => EXT_V7_N
		);

end Behavioral;
