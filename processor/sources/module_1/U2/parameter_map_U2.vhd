library IEEE;
use IEEE.STD_LOGIC_1164.all;
use work.L1TopoDataTypes.all;
package parameter_map is
		constant NumberOfParameters : natural := 101;
		type ParameterRegisters is array (NumberOfParameters - 1 downto 0) of std_logic_vector(31 downto 0);
		function SortParameterMapper(reg : ParameterRegisters) return ParameterSpace;
		function AlgoParameterMapper(reg : ParameterRegisters) return ParameterSpace;
end parameter_map;
package body parameter_map is
		function SortParameterMapper(reg : ParameterRegisters) return ParameterSpace is
				variable result : ParameterSpace(NumberOfSortAlgorithms - 1 downto 0);
		begin
				--TAUabi
				result(1)(0) := reg(0);		--MinET_0
				result(1)(1) := reg(1);		--IsoMask_1
				result(1)(2) := reg(2);		--MinEta_2
				result(1)(3) := reg(3);		--MaxEta_3

				--TAUab
				result(4)(0) := reg(4);		--MinET_0
				result(4)(1) := reg(5);		--IsoMask_1
				result(4)(2) := reg(6);		--MinEta_2
				result(4)(3) := reg(7);		--MaxEta_3

				--EMshi
				result(6)(0) := reg(8);		--IsoMask_0
				result(6)(1) := reg(9);		--MinEta_1
				result(6)(2) := reg(10);		--MaxEta_2

				--Jab
				result(11)(0) := reg(11);		--MinET_0
				result(11)(1) := reg(12);		--MinEta_1
				result(11)(2) := reg(13);		--MaxEta_2

				--MUab
				result(18)(0) := reg(14);		--MinET_0
				result(18)(1) := reg(15);		--MinEta_1
				result(18)(2) := reg(16);		--MaxEta_2

				return result;
		end function;
		function AlgoParameterMapper(reg : ParameterRegisters) return ParameterSpace is
				variable result : ParameterSpace(NumberOfAlgorithms - 1 downto 0);
		begin
				--1DISAMB-EM15his2-TAU12abi-J25ab
				result(67)(0) := reg(17);		--MinET1_0
				result(67)(1) := reg(18);		--MinET2_1
				result(67)(2) := reg(19);		--MinET3_2
				result(67)(3) := reg(20);		--DisambDRSqr_3

				--1DISAMB-J25ab-0DR28-EM15his2-TAU12abi
				result(69)(0) := reg(21);		--MinET1_0
				result(69)(1) := reg(22);		--MinET2_1
				result(69)(2) := reg(23);		--MinET3_2
				result(69)(3) := reg(24);		--DisambDRSqrMin_3
				result(69)(4) := reg(25);		--DisambDRSqrMax_4
				result(69)(5) := reg(26);		--DisambDRSqr_5

				--2INVM9-2MU6ab
				result(8)(0) := reg(27);		--MinET1_0
				result(8)(1) := reg(28);		--MinET2_1
				result(8)(2) := reg(29);		--MinMSqr_2
				result(8)(3) := reg(30);		--MaxMSqr_3

				--2INVM8-ONEBARREL-MU6ab-MU4ab
				result(49)(0) := reg(31);		--MinET1_0
				result(49)(1) := reg(32);		--MinET2_1
				result(49)(2) := reg(33);		--MinMSqr_2
				result(49)(3) := reg(34);		--MaxMSqr_3

				--5DETA99-5DPHI99-MU6ab-MU4ab
				result(20)(0) := reg(35);		--MinET1_0
				result(20)(1) := reg(36);		--MinET2_1
				result(20)(2) := reg(37);		--MinDeltaEta_2
				result(20)(3) := reg(38);		--MaxDeltaEta_3
				result(20)(4) := reg(39);		--MinDeltaPhi_4
				result(20)(5) := reg(40);		--MaxDeltaPhi_5

				--5DETA99-5DPHI99-2MU6ab
				result(21)(0) := reg(41);		--MinET1_0
				result(21)(1) := reg(42);		--MinET2_1
				result(21)(2) := reg(43);		--MinDeltaEta_2
				result(21)(3) := reg(44);		--MaxDeltaEta_3
				result(21)(4) := reg(45);		--MinDeltaPhi_4
				result(21)(5) := reg(46);		--MaxDeltaPhi_5

				--1DISAMB-TAU20abi-TAU12abi-J25ab
				result(68)(0) := reg(47);		--MinET1_0
				result(68)(1) := reg(48);		--MinET2_1
				result(68)(2) := reg(49);		--MinET3_2
				result(68)(3) := reg(50);		--DisambDRSqr_3

				--0DR28-MU10ab-TAU12abi
				result(22)(0) := reg(51);		--MinET1_0
				result(22)(1) := reg(52);		--MinET2_1
				result(22)(2) := reg(53);		--DeltaRMin_2
				result(22)(3) := reg(54);		--DeltaRMax_3

				--0DETA20-0DPHI20-TAU20abi-TAU12abi
				result(52)(0) := reg(55);		--MinET1_0
				result(52)(1) := reg(56);		--MinET2_1
				result(52)(2) := reg(57);		--MinDeltaEta_2
				result(52)(3) := reg(58);		--MaxDeltaEta_3
				result(52)(4) := reg(59);		--MinDeltaPhi_4
				result(52)(5) := reg(60);		--MaxDeltaPhi_5

				--DISAMB-0DR28-EM15his2-TAU12abi
				result(78)(0) := reg(61);		--MinET1_0
				result(78)(1) := reg(62);		--MinET2_1
				result(78)(2) := reg(63);		--DisambDRSqrMin_2
				result(78)(3) := reg(64);		--DisambDRSqrMax_3

				--DISAMB-30INVM-EM20his2-TAU12ab
				result(83)(0) := reg(65);		--MinET1_0
				result(83)(1) := reg(66);		--MinET2_1
				result(83)(2) := reg(67);		--MinMSqr_2
				result(83)(3) := reg(68);		--MaxMSqr_3

				--0DR22-2MU6ab
				result(17)(0) := reg(69);		--MinET1_0
				result(17)(1) := reg(70);		--MinET2_1
				result(17)(2) := reg(71);		--DeltaRMin_2
				result(17)(3) := reg(72);		--DeltaRMax_3

				--7INVM15-2MU4ab
				result(9)(0) := reg(73);		--MinET1_0
				result(9)(1) := reg(74);		--MinET2_1
				result(9)(2) := reg(75);		--MinMSqr_2
				result(9)(3) := reg(76);		--MaxMSqr_3

				--0DR22-MU6ab-MU4ab
				result(18)(0) := reg(77);		--MinET1_0
				result(18)(1) := reg(78);		--MinET2_1
				result(18)(2) := reg(79);		--DeltaRMin_2
				result(18)(3) := reg(80);		--DeltaRMax_3

				--0DR15-2MU4ab
				result(13)(0) := reg(81);		--MinET1_0
				result(13)(1) := reg(82);		--MinET2_1
				result(13)(2) := reg(83);		--DeltaRMin_2
				result(13)(3) := reg(84);		--DeltaRMax_3

				--0DR24-2MU4ab
				result(16)(0) := reg(85);		--MinET1_0
				result(16)(1) := reg(86);		--MinET2_1
				result(16)(2) := reg(87);		--DeltaRMin_2
				result(16)(3) := reg(88);		--DeltaRMax_3

				--0DR15-2MU6ab
				result(19)(0) := reg(89);		--MinET1_0
				result(19)(1) := reg(90);		--MinET2_1
				result(19)(2) := reg(91);		--DeltaRMin_2
				result(19)(3) := reg(92);		--DeltaRMax_3

				--2INVM9-2MU4ab
				result(3)(0) := reg(93);		--MinET1_0
				result(3)(1) := reg(94);		--MinET2_1
				result(3)(2) := reg(95);		--MinMSqr_2
				result(3)(3) := reg(96);		--MaxMSqr_3

				--2INVM9-MU6ab-MU4ab
				result(6)(0) := reg(97);		--MinET1_0
				result(6)(1) := reg(98);		--MinET2_1
				result(6)(2) := reg(99);		--MinMSqr_2
				result(6)(3) := reg(100);		--MaxMSqr_3

				return result;
		end function;
end package body;
