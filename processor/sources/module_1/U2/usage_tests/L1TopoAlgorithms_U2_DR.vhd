library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use work.L1TopoDataTypes.all;

entity L1TopoAlgorithms_U2 is
    Port(ClockBus     : in  std_logic_vector(2 downto 0);

         EmTobArray   : in  ClusterArray(InputWidthEM - 1 downto 0);
         TauTobArray  : in  ClusterArray(InputWidthTAU - 1 downto 0);
         JetTobArray  : in  JetArray(InputWidthJET - 1 downto 0);
         MuonTobArray : in  MuonArray(InputWidthMU - 1 downto 0);
         MetTobArray  : in  MetArray(0 downto 0);
         
         Parameters   : in  ParameterSpace(NumberOfAlgorithms - 1 downto 0);
         SortParameters     : in  ParameterSpace(NumberOfSortAlgorithms - 1 downto 0);
         
         Results      : out std_logic_vector(NumberOfResultBits - 1 downto 0);
         Overflow     : out std_logic_vector(NumberOfResultBits - 1 downto 0)
    );
end L1TopoAlgorithms_U2;

architecture Behavioral of L1TopoAlgorithms_U2 is-- Menu Version: 1

-- Module/FPGA : 0/1
-- General configuration (<TopoConfig>):

--  constant AlgoOffset84 : integer := 0;   --   1DISAMB-TAU20ab-J20ab
--  constant AlgoOffset85 : integer := 1;   --   DISAMB-EM15abhi-TAU12abi
--  constant AlgoOffset86 : integer := 2;   --   1DISAMB-EM15his2-TAU12abi-J25ab
--  constant AlgoOffset88 : integer := 3;   --   1DISAMB-J25ab-0DR28-EM15his2-TAU12abi
--  constant AlgoOffset11 : integer := 4;   --   4INVM8-2MU6ab
--  constant AlgoOffset49 : integer := 5;   --   2INVM999-ONEBARREL-MU6ab-MU4ab
--  constant AlgoOffset53 : integer := 6;   --   4INVM8-ONEBARREL-MU6ab-MU4ab
--  constant AlgoOffset17 : integer := 7;   --   5DETA99-5DPHI99-2MU4ab
--  constant AlgoOffset18 : integer := 8;   --   5DETA99-5DPHI99-MU6ab-MU4ab
--  constant AlgoOffset19 : integer := 9;   --   5DETA99-5DPHI99-2MU6ab
--  constant AlgoOffset87 : integer := 10;   --   1DISAMB-TAU20abi-TAU12abi-J25ab
  constant AlgoOffset20 : integer := 11;   --   0DR28-MU10ab-TAU12abi
--  constant AlgoOffset61 : integer := 12;   --   0DETA20-0DPHI20-TAU20abi-TAU12abi
 -- Ordered list of sorted TOBArrays:
--  signal EMall  :    TOBArray ((InputWidthEM - 1)  downto 0);
  signal TAUabi  :    TOBArray ((OutputWidthSelectTAU - 1)  downto 0);
--  signal EMabi  :    TOBArray ((OutputWidthSelectEM - 1)  downto 0);
--  signal EMabhi  :    TOBArray ((OutputWidthSelectEM - 1)  downto 0);
--  signal TAUab  :    TOBArray ((OutputWidthSelectTAU - 1)  downto 0);
--  signal TAUsi  :    TOBArray ((OutputWidthSortTAU - 1)  downto 0);
--  signal EMs  :    TOBArray ((OutputWidthSortEM - 1)  downto 0);
--  signal EMshi  :    TOBArray ((OutputWidthSortEM - 1)  downto 0);
--  signal AJall  :    TOBArray ((InputWidthJET - 1)  downto 0);
--  signal AJjall  :    TOBArray ((InputWidthJET - 1)  downto 0);
--  signal AJMatchall  :    TOBArray ((InputWidthJET - 1)  downto 0);
--  signal CJab  :    TOBArray ((OutputWidthSelectJET - 1)  downto 0);
--  signal Jab  :    TOBArray ((OutputWidthSelectJET - 1)  downto 0);
--  signal AJjs  :    TOBArray ((OutputWidthSortJET - 1)  downto 0);
--  signal AJs  :    TOBArray ((OutputWidthSortJET - 1)  downto 0);
--  signal Js  :    TOBArray ((OutputWidthSortJET - 1)  downto 0);
--  signal XENoSort  :    TOBArray ((OutputWidthMET - 1)  downto 0);
--  signal XE  :    TOBArray ((OutputWidthMET - 1)  downto 0);
--  signal CMUab  :    TOBArray ((OutputWidthSelectMU - 1)  downto 0);
  signal MUab  :    TOBArray ((OutputWidthSelectMU - 1)  downto 0);

begin

--sortalgo0 : entity work.ClusterSelect --  Algorithm name: TAUabi
--  generic map (
--     InputWidth => InputWidthTAU,
--     InputWidth1stStage => InputWidth1stStageSelectTAU,
--     OutputWidth => OutputWidthSelectTAU,
--     DoIsoCut => 1 )
--  port map (
--     ClusterTobArray => TauTobArray,
--     TobArrayOut =>  TAUabi,
--     ClockBus => ClockBus,
--     Parameters => SortParameters(0)
-- );
 
--TODO: replaced by JetSelect 
sortalgo0 : entity work.JetSelect --  Algorithm name: TAUabi
  generic map (
     InputWidth => InputWidthJET,
     InputWidth1stStage => InputWidth1stStageSelectJET,
     OutputWidth => OutputWidthSelectJET,
     JetSize => 2)
  port map (
     JetTobArray => JetTobArray,
     TobArrayOut =>  TAUabi,
     ClockBus => ClockBus,
     Parameters => SortParameters(0)
 );

--sortalgo19 : entity work.ClusterSelect --  Algorithm name: EMabhi
--  generic map (
--     InputWidth => InputWidthEM,
--     InputWidth1stStage => InputWidth1stStageSelectEM,
--     OutputWidth => OutputWidthSelectEM,
--     DoIsoCut => 1 )
--  port map (
--     ClusterTobArray => EmTobArray,
--     TobArrayOut =>  EMabhi,
--     ClockBus => ClockBus,
--     Parameters => SortParameters(19)
-- );
--
--sortalgo20 : entity work.ClusterSelect --  Algorithm name: TAUab
--  generic map (
--     InputWidth => InputWidthTAU,
--     InputWidth1stStage => InputWidth1stStageSelectTAU,
--     OutputWidth => OutputWidthSelectTAU,
--     DoIsoCut => 0 )
--  port map (
--     ClusterTobArray => TauTobArray,
--     TobArrayOut =>  TAUab,
--     ClockBus => ClockBus,
--     Parameters => SortParameters(20)
-- );
--
--sortalgo12 : entity work.ClusterSort --  Algorithm name: EMshi
--  generic map (
--     InputWidth => InputWidthEM,
--     InputWidth1stStage => InputWidth1stStageSortEM,
--     OutputWidth => OutputWidthSortEM,
--     DoIsoCut => 1 )
--  port map (
--     ClusterTobArray => EmTobArray,
--     TobArrayOut =>  EMshi,
--     ClockBus => ClockBus,
--     Parameters => SortParameters(12)
-- );
--
--sortalgo8 : entity work.JetSelect --  Algorithm name: Jab
--  generic map (
--     InputWidth => InputWidthJET,
--     InputWidth1stStage => InputWidth1stStageSelectJET,
--     OutputWidth => OutputWidthSelectJET,
--     JetSize => 2)
--  port map (
--     JetTobArray => JetTobArray,
--     TobArrayOut =>  Jab,
--     ClockBus => ClockBus,
--     Parameters => SortParameters(8)
-- );
--
sortalgo2 : entity work.MuonSelect --  Algorithm name: MUab
  generic map (
     InputWidth => InputWidthMU,
     InputWidth1stStage => InputWidth1stStageSelectMU,
     OutputWidth => OutputWidthSelectMU )
  port map (
     MuonTobArray => MuonTobArray,
     TobArrayOut =>  MUab,
     ClockBus => ClockBus,
     Parameters => SortParameters(2)
 );
--
--decisionalgo84 : entity work.DisambiguationIncl2 --  Algorithm name: 1DISAMB-TAU20ab-J20ab
--  generic map (
--     InputWidth1 => OutputWidthSelectTAU,
--     InputWidth2 => OutputWidthSelectJET,
--     MaxTob1 => OutputWidthSelectTAU,
--     MaxTob2 => OutputWidthSelectJET,
--     NumResultBits => 1,
--     ClusterOnly => 0,
--     ApplyDR => 0 )
--  port map (
--     Tob1 => TAUab,
--     Tob2 => Jab,
--     Results => Results(AlgoOffset84+0 downto AlgoOffset84), 
--     Overflow => Overflow(AlgoOffset84+0 downto AlgoOffset84), 
--     ClockBus => ClockBus,
--     Parameters => Parameters(84)
-- );
--
--decisionalgo85 : entity work.DisambiguationIncl2 --  Algorithm name: DISAMB-EM15abhi-TAU12abi
--  generic map (
--     InputWidth1 => OutputWidthSelectEM,
--     InputWidth2 => OutputWidthSelectTAU,
--     MaxTob1 => OutputWidthSelectEM,
--     MaxTob2 => OutputWidthSelectTAU,
--     NumResultBits => 1,
--     ClusterOnly => 1,
--     ApplyDR => 0 )
--  port map (
--     Tob1 => EMabhi,
--     Tob2 => TAUabi,
--     Results => Results(AlgoOffset85+0 downto AlgoOffset85), 
--     Overflow => Overflow(AlgoOffset85+0 downto AlgoOffset85), 
--     ClockBus => ClockBus,
--     Parameters => Parameters(85)
-- );
--
--decisionalgo86 : entity work.DisambiguationIncl3 --  Algorithm name: 1DISAMB-EM15his2-TAU12abi-J25ab
--  generic map (
--     InputWidth1 => OutputWidthSortEM,
--     InputWidth2 => OutputWidthSelectTAU,
--     InputWidth3 => OutputWidthSelectJET,
--     MaxTob1 => 2,
--     MaxTob2 => OutputWidthSelectTAU,
--     MaxTob3 => OutputWidthSelectJET,
--     NumResultBits => 1,
--     ApplyDR => 0 )
--  port map (
--     Tob1 => EMshi,
--     Tob2 => TAUabi,
--     Tob3 => Jab,
--     Results => Results(AlgoOffset86+0 downto AlgoOffset86), 
--     Overflow => Overflow(AlgoOffset86+0 downto AlgoOffset86), 
--     ClockBus => ClockBus,
--     Parameters => Parameters(86)
-- );
--
--decisionalgo88 : entity work.DisambiguationDRIncl3 --  Algorithm name: 1DISAMB-J25ab-0DR28-EM15his2-TAU12abi
--  generic map (
--     InputWidth1 => OutputWidthSortEM,
--     InputWidth2 => OutputWidthSelectTAU,
--     InputWidth3 => OutputWidthSelectJET,
--     MaxTob1 => 2,
--     MaxTob2 => OutputWidthSelectTAU,
--     MaxTob3 => OutputWidthSelectJET,
--     NumResultBits => 1 )
--  port map (
--     Tob1 => EMshi,
--     Tob2 => TAUabi,
--     Tob3 => Jab,
--     Results => Results(AlgoOffset88+0 downto AlgoOffset88), 
--     Overflow => Overflow(AlgoOffset88+0 downto AlgoOffset88), 
--     ClockBus => ClockBus,
--     Parameters => Parameters(88)
-- );
--
--decisionalgo11 : entity work.InvariantMassInclusive1 --  Algorithm name: 4INVM8-2MU6ab
--  generic map (
--     InputWidth => OutputWidthSelectMU,
--     MaxTob => OutputWidthSelectMU,
--     RequireOneBarrel => 0,
--     NumResultBits => 1 )
--  port map (
--     Tob => MUab,
--     Results => Results(AlgoOffset11+0 downto AlgoOffset11), 
--     Overflow => Overflow(AlgoOffset11+0 downto AlgoOffset11), 
--     ClockBus => ClockBus,
--     Parameters => Parameters(11)
-- );
--
--decisionalgo49 : entity work.InvariantMassInclusive1 --  Algorithm name: 2INVM999-ONEBARREL-MU6ab-MU4ab
--  generic map (
--     InputWidth => OutputWidthSelectMU,
--     MaxTob => OutputWidthSelectMU,
--     RequireOneBarrel => 1,
--     NumResultBits => 1 )
--  port map (
--     Tob => MUab,
--     Results => Results(AlgoOffset49+0 downto AlgoOffset49), 
--     Overflow => Overflow(AlgoOffset49+0 downto AlgoOffset49), 
--     ClockBus => ClockBus,
--     Parameters => Parameters(49)
-- );
--
--decisionalgo53 : entity work.InvariantMassInclusive1 --  Algorithm name: 4INVM8-ONEBARREL-MU6ab-MU4ab
--  generic map (
--     InputWidth => OutputWidthSelectMU,
--     MaxTob => OutputWidthSelectMU,
--     RequireOneBarrel => 1,
--     NumResultBits => 1 )
--  port map (
--     Tob => MUab,
--     Results => Results(AlgoOffset53+0 downto AlgoOffset53), 
--     Overflow => Overflow(AlgoOffset53+0 downto AlgoOffset53), 
--     ClockBus => ClockBus,
--     Parameters => Parameters(53)
-- );
--
--decisionalgo17 : entity work.DeltaEtaPhiIncl1 --  Algorithm name: 5DETA99-5DPHI99-2MU4ab
--  generic map (
--     NumResultBits => 1,
--     InputWidth => OutputWidthSelectMU,
--     MaxTob => OutputWidthSelectMU )
--  port map (
--     Tob => MUab,
--     Results => Results(AlgoOffset17+0 downto AlgoOffset17), 
--     Overflow => Overflow(AlgoOffset17+0 downto AlgoOffset17), 
--     ClockBus => ClockBus,
--     Parameters => Parameters(17)
-- );
--
--decisionalgo18 : entity work.DeltaEtaPhiIncl1 --  Algorithm name: 5DETA99-5DPHI99-MU6ab-MU4ab
--  generic map (
--     NumResultBits => 1,
--     InputWidth => OutputWidthSelectMU,
--     MaxTob => OutputWidthSelectMU )
--  port map (
--     Tob => MUab,
--     Results => Results(AlgoOffset18+0 downto AlgoOffset18), 
--     Overflow => Overflow(AlgoOffset18+0 downto AlgoOffset18), 
--     ClockBus => ClockBus,
--     Parameters => Parameters(18)
-- );
--
--decisionalgo19 : entity work.DeltaEtaPhiIncl1 --  Algorithm name: 5DETA99-5DPHI99-2MU6ab
--  generic map (
--     NumResultBits => 1,
--     InputWidth => OutputWidthSelectMU,
--     MaxTob => OutputWidthSelectMU )
--  port map (
--     Tob => MUab,
--     Results => Results(AlgoOffset19+0 downto AlgoOffset19), 
--     Overflow => Overflow(AlgoOffset19+0 downto AlgoOffset19), 
--     ClockBus => ClockBus,
--     Parameters => Parameters(19)
-- );
--
--decisionalgo87 : entity work.DisambiguationIncl3 --  Algorithm name: 1DISAMB-TAU20abi-TAU12abi-J25ab
--  generic map (
--     InputWidth1 => OutputWidthSelectTAU,
--     InputWidth2 => OutputWidthSelectTAU,
--     InputWidth3 => OutputWidthSelectJET,
--     MaxTob1 => OutputWidthSelectTAU,
--     MaxTob2 => OutputWidthSelectTAU,
--     MaxTob3 => OutputWidthSelectTAU,
--     NumResultBits => 1,
--     ApplyDR => 0 )
--  port map (
--     Tob1 => TAUabi,
--     Tob2 => TAUabi,
--     Tob3 => Jab,
--     Results => Results(AlgoOffset87+0 downto AlgoOffset87), 
--     Overflow => Overflow(AlgoOffset87+0 downto AlgoOffset87), 
--     ClockBus => ClockBus,
--     Parameters => Parameters(87)
-- );
--
decisionalgo20 : entity work.DeltaRSqrIncl2 --  Algorithm name: 0DR28-MU10ab-TAU12abi
  generic map (
     InputWidth1 => OutputWidthSelectMU,
     InputWidth2 => OutputWidthSelectTAU,
     MaxTob1 => OutputWidthSelectMU,
     MaxTob2 => OutputWidthSelectTAU,
     NumResultBits => 1 )
  port map (
     Tob1 => MUab,
     Tob2 => TAUabi,
     Results => Results(AlgoOffset20+0 downto AlgoOffset20), 
     Overflow => Overflow(AlgoOffset20+0 downto AlgoOffset20), 
     ClockBus => ClockBus,
     Parameters => Parameters(20)
 );
--
--decisionalgo61 : entity work.DeltaEtaPhiIncl1 --  Algorithm name: 0DETA20-0DPHI20-TAU20abi-TAU12abi
--  generic map (
--     NumResultBits => 1,
--     InputWidth => OutputWidthSelectTAU,
--     MaxTob => OutputWidthSelectTAU )
--  port map (
--     Tob => TAUabi,
--     Results => Results(AlgoOffset61+0 downto AlgoOffset61), 
--     Overflow => Overflow(AlgoOffset61+0 downto AlgoOffset61), 
--     ClockBus => ClockBus,
--     Parameters => Parameters(61)
-- );

end Behavioral;