library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use work.L1TopoDataTypes.all;

entity L1TopoAlgorithms_U2 is
    Port(ClockBus     : in  std_logic_vector(2 downto 0);

         EmTobArray   : in  ClusterArray(InputWidthEM - 1 downto 0);
         TauTobArray  : in  ClusterArray(InputWidthTAU - 1 downto 0);
         JetTobArray  : in  JetArray(InputWidthJET - 1 downto 0);
         MuonTobArray : in  MuonArray(InputWidthMU - 1 downto 0);
         MetTobArray  : in  MetArray(0 downto 0);
         
         Parameters   : in  ParameterSpace(NumberOfAlgorithms - 1 downto 0);
         SortParameters     : in  ParameterSpace(NumberOfSortAlgorithms - 1 downto 0);
         
         Results      : out std_logic_vector(NumberOfResultBits - 1 downto 0);
         Overflow     : out std_logic_vector(NumberOfResultBits - 1 downto 0)
    );
end L1TopoAlgorithms_U2;

architecture Behavioral of L1TopoAlgorithms_U2 is-- Menu Version: 1

-- Module/FPGA : 0/1
-- General configuration (<TopoConfig>):

  constant AlgoOffset67 : integer := 2;   --   1DISAMB-EM15his2-TAU12abi-J25ab Firstbit=2 clock=0
  constant AlgoOffset69 : integer := 3;   --   1DISAMB-J25ab-0DR28-EM15his2-TAU12abi Firstbit=3 clock=0
  constant AlgoOffset8 : integer := 4;   --   2INVM9-2MU6ab Firstbit=4 clock=0
  constant AlgoOffset49 : integer := 6;   --   2INVM8-ONEBARREL-MU6ab-MU4ab Firstbit=6 clock=0
  constant AlgoOffset20 : integer := 8;   --   5DETA99-5DPHI99-MU6ab-MU4ab Firstbit=8 clock=0
  constant AlgoOffset21 : integer := 9;   --   5DETA99-5DPHI99-2MU6ab Firstbit=9 clock=0
  constant AlgoOffset68 : integer := 10;   --   1DISAMB-TAU20abi-TAU12abi-J25ab Firstbit=10 clock=0
  constant AlgoOffset22 : integer := 11;   --   0DR28-MU10ab-TAU12abi Firstbit=11 clock=0
  constant AlgoOffset52 : integer := 12;   --   0DETA20-0DPHI20-TAU20abi-TAU12abi Firstbit=12 clock=0
  constant AlgoOffset78 : integer := 15;   --   DISAMB-0DR28-EM15his2-TAU12abi Firstbit=15 clock=0
  constant AlgoOffset83 : integer := 16;   --   DISAMB-30INVM-EM20his2-TAU12ab Firstbit=0 clock=1
  constant AlgoOffset17 : integer := 20;   --   0DR22-2MU6ab Firstbit=4 clock=1
  constant AlgoOffset9 : integer := 21;   --   7INVM15-2MU4ab Firstbit=5 clock=1
  constant AlgoOffset18 : integer := 22;   --   0DR22-MU6ab-MU4ab Firstbit=6 clock=1
  constant AlgoOffset13 : integer := 23;   --   0DR15-2MU4ab Firstbit=7 clock=1
  constant AlgoOffset16 : integer := 24;   --   0DR24-2MU4ab Firstbit=8 clock=1
  constant AlgoOffset19 : integer := 25;   --   0DR15-2MU6ab Firstbit=9 clock=1
  constant AlgoOffset3 : integer := 26;   --   2INVM9-2MU4ab Firstbit=10 clock=1
  constant AlgoOffset6 : integer := 27;   --   2INVM9-MU6ab-MU4ab Firstbit=11 clock=1
 -- Ordered list of sorted TOBArrays:
  signal EMall  :    TOBArray ((InputWidthEM - 1)  downto 0);
  signal TAUabi  :    TOBArray ((OutputWidthSelectTAU - 1)  downto 0);
  signal EMabi  :    TOBArray ((OutputWidthSelectEM - 1)  downto 0);
  signal TAUab  :    TOBArray ((OutputWidthSelectTAU - 1)  downto 0);
  signal EMs  :    TOBArray ((OutputWidthSortEM - 1)  downto 0);
  signal EMshi  :    TOBArray ((OutputWidthSortEM - 1)  downto 0);
  signal TAUsi  :    TOBArray ((OutputWidthSortTAU - 1)  downto 0);
  signal AJall  :    TOBArray ((InputWidthJET - 1)  downto 0);
  signal AJjall  :    TOBArray ((InputWidthJET - 1)  downto 0);
  signal AJMatchall  :    TOBArray ((InputWidthJET - 1)  downto 0);
  signal Jab  :    TOBArray ((OutputWidthSelectJET - 1)  downto 0);
  signal CJab  :    TOBArray ((OutputWidthSelectJET - 1)  downto 0);
  signal AJjs  :    TOBArray ((OutputWidthSortJET - 1)  downto 0);
  signal AJs  :    TOBArray ((OutputWidthSortJET - 1)  downto 0);
  signal Js  :    TOBArray ((OutputWidthSortJET - 1)  downto 0);
  signal XENoSort  :    TOBArray ((OutputWidthMET - 1)  downto 0);
  signal XE  :    TOBArray ((OutputWidthMET - 1)  downto 0);
  signal MUab  :    TOBArray ((OutputWidthSelectMU - 1)  downto 0);
  signal CMUab  :    TOBArray ((OutputWidthSelectMU - 1)  downto 0);
  signal LMUs  :    TOBArray ((OutputWidthSortMU - 1)  downto 0);

begin

sortalgo1 : entity work.ClusterSelect --  Algorithm name: TAUabi
  generic map (
     InputWidth => InputWidthTAU,
     InputWidth1stStage => InputWidth1stStageSelectTAU,
     OutputWidth => OutputWidthSelectTAU,
     DoIsoCut => 1 )
  port map (
     ClusterTobArray => TauTobArray,
     TobArrayOut =>  TAUabi,
     ClockBus => ClockBus,
     Parameters => SortParameters(1)
 );

sortalgo4 : entity work.ClusterSelect --  Algorithm name: TAUab
  generic map (
     InputWidth => InputWidthTAU,
     InputWidth1stStage => InputWidth1stStageSelectTAU,
     OutputWidth => OutputWidthSelectTAU,
     DoIsoCut => 0 )
  port map (
     ClusterTobArray => TauTobArray,
     TobArrayOut =>  TAUab,
     ClockBus => ClockBus,
     Parameters => SortParameters(4)
 );

sortalgo6 : entity work.ClusterSort --  Algorithm name: EMshi
  generic map (
     InputWidth => InputWidthEM,
     InputWidth1stStage => InputWidth1stStageSortEM,
     OutputWidth => OutputWidthSortEM,
     DoIsoCut => 1 )
  port map (
     ClusterTobArray => EmTobArray,
     TobArrayOut =>  EMshi,
     ClockBus => ClockBus,
     Parameters => SortParameters(6)
 );

sortalgo11 : entity work.JetSelect --  Algorithm name: Jab
  generic map (
     InputWidth => InputWidthJET,
     InputWidth1stStage => InputWidth1stStageSelectJET,
     OutputWidth => OutputWidthSelectJET,
     JetSize => 2,
     DoEtaCut => 1 )
  port map (
     JetTobArray => JetTobArray,
     TobArrayOut =>  Jab,
     ClockBus => ClockBus,
     Parameters => SortParameters(11)
 );

sortalgo18 : entity work.MuonSelect --  Algorithm name: MUab
  generic map (
     InputWidth => InputWidthMU,
     InputWidth1stStage => InputWidth1stStageSelectMU,
     OutputWidth => OutputWidthSelectMU )
  port map (
     MuonTobArray => MuonTobArray,
     TobArrayOut =>  MUab,
     ClockBus => ClockBus,
     Parameters => SortParameters(18)
 );

decisionalgo67 : entity work.DisambiguationIncl3 --  Algorithm name: 1DISAMB-EM15his2-TAU12abi-J25ab
  generic map (
     InputWidth1 => OutputWidthSortEM,
     InputWidth2 => OutputWidthSelectTAU,
     InputWidth3 => OutputWidthSelectJET,
     MaxTob1 => 2,
     MaxTob2 => OutputWidthSelectTAU,
     MaxTob3 => OutputWidthSelectJET,
     NumResultBits => 1,
     ApplyDR => 0 )
  port map (
     Tob1 => EMshi,
     Tob2 => TAUabi,
     Tob3 => Jab,
     Results => Results(AlgoOffset67+0 downto AlgoOffset67), 
     Overflow => Overflow(AlgoOffset67+0 downto AlgoOffset67), 
     ClockBus => ClockBus,
     Parameters => Parameters(67)
 );

decisionalgo69 : entity work.DisambiguationDRIncl3 --  Algorithm name: 1DISAMB-J25ab-0DR28-EM15his2-TAU12abi
  generic map (
     InputWidth1 => OutputWidthSortEM,
     InputWidth2 => OutputWidthSelectTAU,
     InputWidth3 => OutputWidthSelectJET,
     MaxTob1 => 2,
     MaxTob2 => OutputWidthSelectTAU,
     MaxTob3 => OutputWidthSelectJET,
     NumResultBits => 1 )
  port map (
     Tob1 => EMshi,
     Tob2 => TAUabi,
     Tob3 => Jab,
     Results => Results(AlgoOffset69+0 downto AlgoOffset69), 
     Overflow => Overflow(AlgoOffset69+0 downto AlgoOffset69), 
     ClockBus => ClockBus,
     Parameters => Parameters(69)
 );

decisionalgo8 : entity work.InvariantMassInclusive1 --  Algorithm name: 2INVM9-2MU6ab
  generic map (
     InputWidth => OutputWidthSelectMU,
     MaxTob => OutputWidthSelectMU,
     RequireOneBarrel => 0,
     NumResultBits => 1 )
  port map (
     Tob => MUab,
     Results => Results(AlgoOffset8+0 downto AlgoOffset8), 
     Overflow => Overflow(AlgoOffset8+0 downto AlgoOffset8), 
     ClockBus => ClockBus,
     Parameters => Parameters(8)
 );

decisionalgo49 : entity work.InvariantMassInclusive1 --  Algorithm name: 2INVM8-ONEBARREL-MU6ab-MU4ab
  generic map (
     InputWidth => OutputWidthSelectMU,
     MaxTob => OutputWidthSelectMU,
     RequireOneBarrel => 1,
     NumResultBits => 1 )
  port map (
     Tob => MUab,
     Results => Results(AlgoOffset49+0 downto AlgoOffset49), 
     Overflow => Overflow(AlgoOffset49+0 downto AlgoOffset49), 
     ClockBus => ClockBus,
     Parameters => Parameters(49)
 );

decisionalgo20 : entity work.DeltaEtaPhiIncl1 --  Algorithm name: 5DETA99-5DPHI99-MU6ab-MU4ab
  generic map (
     NumResultBits => 1,
     InputWidth => OutputWidthSelectMU,
     MaxTob => OutputWidthSelectMU )
  port map (
     Tob => MUab,
     Results => Results(AlgoOffset20+0 downto AlgoOffset20), 
     Overflow => Overflow(AlgoOffset20+0 downto AlgoOffset20), 
     ClockBus => ClockBus,
     Parameters => Parameters(20)
 );

decisionalgo21 : entity work.DeltaEtaPhiIncl1 --  Algorithm name: 5DETA99-5DPHI99-2MU6ab
  generic map (
     NumResultBits => 1,
     InputWidth => OutputWidthSelectMU,
     MaxTob => OutputWidthSelectMU )
  port map (
     Tob => MUab,
     Results => Results(AlgoOffset21+0 downto AlgoOffset21), 
     Overflow => Overflow(AlgoOffset21+0 downto AlgoOffset21), 
     ClockBus => ClockBus,
     Parameters => Parameters(21)
 );

decisionalgo68 : entity work.DisambiguationIncl3 --  Algorithm name: 1DISAMB-TAU20abi-TAU12abi-J25ab
  generic map (
     InputWidth1 => OutputWidthSelectTAU,
     InputWidth2 => OutputWidthSelectTAU,
     InputWidth3 => OutputWidthSelectJET,
     MaxTob1 => OutputWidthSelectTAU,
     MaxTob2 => OutputWidthSelectTAU,
     MaxTob3 => OutputWidthSelectTAU,
     NumResultBits => 1,
     ApplyDR => 0 )
  port map (
     Tob1 => TAUabi,
     Tob2 => TAUabi,
     Tob3 => Jab,
     Results => Results(AlgoOffset68+0 downto AlgoOffset68), 
     Overflow => Overflow(AlgoOffset68+0 downto AlgoOffset68), 
     ClockBus => ClockBus,
     Parameters => Parameters(68)
 );

decisionalgo22 : entity work.DeltaRSqrIncl2 --  Algorithm name: 0DR28-MU10ab-TAU12abi
  generic map (
     InputWidth1 => OutputWidthSelectMU,
     InputWidth2 => OutputWidthSelectTAU,
     MaxTob1 => OutputWidthSelectMU,
     MaxTob2 => OutputWidthSelectTAU,
     NumResultBits => 1 )
  port map (
     Tob1 => MUab,
     Tob2 => TAUabi,
     Results => Results(AlgoOffset22+0 downto AlgoOffset22), 
     Overflow => Overflow(AlgoOffset22+0 downto AlgoOffset22), 
     ClockBus => ClockBus,
     Parameters => Parameters(22)
 );

decisionalgo52 : entity work.DeltaEtaPhiIncl1 --  Algorithm name: 0DETA20-0DPHI20-TAU20abi-TAU12abi
  generic map (
     NumResultBits => 1,
     InputWidth => OutputWidthSelectTAU,
     MaxTob => OutputWidthSelectTAU )
  port map (
     Tob => TAUabi,
     Results => Results(AlgoOffset52+0 downto AlgoOffset52), 
     Overflow => Overflow(AlgoOffset52+0 downto AlgoOffset52), 
     ClockBus => ClockBus,
     Parameters => Parameters(52)
 );

decisionalgo78 : entity work.DisambiguationDRIncl2 --  Algorithm name: DISAMB-0DR28-EM15his2-TAU12abi
  generic map (
     InputWidth1 => OutputWidthSortEM,
     InputWidth2 => OutputWidthSelectTAU,
     MaxTob1 => 2,
     MaxTob2 => OutputWidthSelectTAU,
     NumResultBits => 1 )
  port map (
     Tob1 => EMshi,
     Tob2 => TAUabi,
     Results => Results(AlgoOffset78+0 downto AlgoOffset78), 
     Overflow => Overflow(AlgoOffset78+0 downto AlgoOffset78), 
     ClockBus => ClockBus,
     Parameters => Parameters(78)
 );

decisionalgo83 : entity work.DisambiguationInvmIncl2 --  Algorithm name: DISAMB-30INVM-EM20his2-TAU12ab
  generic map (
     InputWidth1 => OutputWidthSortEM,
     InputWidth2 => OutputWidthSelectTAU,
     MaxTob1 => 2,
     MaxTob2 => OutputWidthSelectTAU,
     NumResultBits => 1 )
  port map (
     Tob1 => EMshi,
     Tob2 => TAUab,
     Results => Results(AlgoOffset83+0 downto AlgoOffset83), 
     Overflow => Overflow(AlgoOffset83+0 downto AlgoOffset83), 
     ClockBus => ClockBus,
     Parameters => Parameters(83)
 );

decisionalgo17 : entity work.DeltaRSqrIncl1 --  Algorithm name: 0DR22-2MU6ab
  generic map (
     InputWidth => OutputWidthSelectMU,
     MaxTob => OutputWidthSelectMU,
     RequireOneBarrel => 0,
     NumResultBits => 1 )
  port map (
     Tob => MUab,
     Results => Results(AlgoOffset17+0 downto AlgoOffset17), 
     Overflow => Overflow(AlgoOffset17+0 downto AlgoOffset17), 
     ClockBus => ClockBus,
     Parameters => Parameters(17)
 );

decisionalgo9 : entity work.InvariantMassInclusive1 --  Algorithm name: 7INVM15-2MU4ab
  generic map (
     InputWidth => OutputWidthSelectMU,
     MaxTob => OutputWidthSelectMU,
     RequireOneBarrel => 0,
     NumResultBits => 1 )
  port map (
     Tob => MUab,
     Results => Results(AlgoOffset9+0 downto AlgoOffset9), 
     Overflow => Overflow(AlgoOffset9+0 downto AlgoOffset9), 
     ClockBus => ClockBus,
     Parameters => Parameters(9)
 );

decisionalgo18 : entity work.DeltaRSqrIncl1 --  Algorithm name: 0DR22-MU6ab-MU4ab
  generic map (
     InputWidth => OutputWidthSelectMU,
     MaxTob => OutputWidthSelectMU,
     RequireOneBarrel => 0,
     NumResultBits => 1 )
  port map (
     Tob => MUab,
     Results => Results(AlgoOffset18+0 downto AlgoOffset18), 
     Overflow => Overflow(AlgoOffset18+0 downto AlgoOffset18), 
     ClockBus => ClockBus,
     Parameters => Parameters(18)
 );

decisionalgo13 : entity work.DeltaRSqrIncl1 --  Algorithm name: 0DR15-2MU4ab
  generic map (
     InputWidth => OutputWidthSelectMU,
     MaxTob => OutputWidthSelectMU,
     RequireOneBarrel => 0,
     NumResultBits => 1 )
  port map (
     Tob => MUab,
     Results => Results(AlgoOffset13+0 downto AlgoOffset13), 
     Overflow => Overflow(AlgoOffset13+0 downto AlgoOffset13), 
     ClockBus => ClockBus,
     Parameters => Parameters(13)
 );

decisionalgo16 : entity work.DeltaRSqrIncl1 --  Algorithm name: 0DR24-2MU4ab
  generic map (
     InputWidth => OutputWidthSelectMU,
     MaxTob => OutputWidthSelectMU,
     RequireOneBarrel => 0,
     NumResultBits => 1 )
  port map (
     Tob => MUab,
     Results => Results(AlgoOffset16+0 downto AlgoOffset16), 
     Overflow => Overflow(AlgoOffset16+0 downto AlgoOffset16), 
     ClockBus => ClockBus,
     Parameters => Parameters(16)
 );

decisionalgo19 : entity work.DeltaRSqrIncl1 --  Algorithm name: 0DR15-2MU6ab
  generic map (
     InputWidth => OutputWidthSelectMU,
     MaxTob => OutputWidthSelectMU,
     RequireOneBarrel => 0,
     NumResultBits => 1 )
  port map (
     Tob => MUab,
     Results => Results(AlgoOffset19+0 downto AlgoOffset19), 
     Overflow => Overflow(AlgoOffset19+0 downto AlgoOffset19), 
     ClockBus => ClockBus,
     Parameters => Parameters(19)
 );

decisionalgo3 : entity work.InvariantMassInclusive1 --  Algorithm name: 2INVM9-2MU4ab
  generic map (
     InputWidth => OutputWidthSelectMU,
     MaxTob => OutputWidthSelectMU,
     RequireOneBarrel => 0,
     NumResultBits => 1 )
  port map (
     Tob => MUab,
     Results => Results(AlgoOffset3+0 downto AlgoOffset3), 
     Overflow => Overflow(AlgoOffset3+0 downto AlgoOffset3), 
     ClockBus => ClockBus,
     Parameters => Parameters(3)
 );

decisionalgo6 : entity work.InvariantMassInclusive1 --  Algorithm name: 2INVM9-MU6ab-MU4ab
  generic map (
     InputWidth => OutputWidthSelectMU,
     MaxTob => OutputWidthSelectMU,
     RequireOneBarrel => 0,
     NumResultBits => 1 )
  port map (
     Tob => MUab,
     Results => Results(AlgoOffset6+0 downto AlgoOffset6), 
     Overflow => Overflow(AlgoOffset6+0 downto AlgoOffset6), 
     ClockBus => ClockBus,
     Parameters => Parameters(6)
 );

end Behavioral;