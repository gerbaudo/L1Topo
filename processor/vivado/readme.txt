* Configuring the project files after downloading from svn
	- the fresh downloaded project files only contain the processor specific source files
	- open the project file in Vivado and execute the "add_common_source_files.tcl" in the "Tcl Console" (see command below) to add the other source files and the ip-cores:  
		source <PROJECT_DIRECTORY>/../../sources/common/tcl_scripts/add_common_source_files.tcl
		(Where <PROJECT_DIRECTORY> is the trunk/processor/vivado/L1TopoProcessor_Ux directory. If you are using a different project directory you have to change the relative paths of this command and of "common_dir" and "ip_dir" in add_common_source_files.tcl (line 1 and line 2))
	- (alternatively: menu bar -> Tools --> "Run Tcl Script...", select add_common_source_files.tcl in the dialog window)
	- the script adds all other source files and ip cores, which the four processor have in common
	- the ip cores are upgraded automatically (if necessary)
	- the algorithm files would have to be added by hand or by the processor specific tcl script in trunk/processor/sources/module_x/Ux/add_algorithm_source_files.tcl



* Vivado strategies
	- the settings of our L1Topo implementation strategy "Performance_AggressiveExplore" should be part of the project file
	- should it be not available in the "Change Run Settings..." menu, then copy the "Performance_AggressiveExplore.VivadoImplementation2015.psg" file in this directory to:
		-- Linux: $HOME/.Xilinx/Vivado/strategies
		-- Windows 7: C:\Users\<username>\AppData\Roaming\Xilinx\Vivado\strategies
	- see http://www.xilinx.com/support/documentation/sw_manuals/xilinx2015_2/ug904-vivado-implementation.pdf, page 39
