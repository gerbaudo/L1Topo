processors = [1, 2, 3, 4]
projectTrunkPath = "../../"

outFile = open("setupProjects.tcl", 'w')
for processorNum in processors:
    moduleNum = (processorNum+1) // 2
    fpgaNum = (processorNum+1) % 2 + 1
    openProjStr = "open_project {}/processor/vivado/L1TopoProcessor_U{}/L1TopoProcessor_U{}.xpr\n".format(
            projectTrunkPath, processorNum, processorNum)
    outFile.write(openProjStr)
    outFile.write("source {{{}/processor/sources/common/tcl_scripts/add_common_source_files.tcl}}\n".format(
            projectTrunkPath))
    outFile.write("source {{{}/processor/sources/module_{}/U{}/add_algorithm_source_files.tcl}}\n".format(
            projectTrunkPath, moduleNum, fpgaNum))
    outFile.write("close_project\n")
outFile.write('q\n')

outFile.close()

print ("run the generated script using 'vivado -mode tcl -source setupProjects.tcl'")
