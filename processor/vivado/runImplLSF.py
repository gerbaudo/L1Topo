
processors = [1, 2, 3, 4]
createNewRuns = True
##runName = "run161219-1200-r2902-lateMuonFix"
runName = "run1494624109-100-r3089-MOGON"
projectTrunkPath = "../../"

synthStrategy = "Flow_PerfOptimized_high"
implStrategy = ["Performance_AggressiveExplore", "PlaceDesignSpreadLogic_medium", "Spread_logic_medium_fanoutOpt", "Performance_ExploreSLLs"]
#implStrategy = ["{Vivado Implementation Defaults}", "Performance_AggressiveExplore", "PlaceDesignSpreadLogic_medium", "Performance_Explore",
#"Performance_ExplorePostRoutePhysOpt", "Performance_RefinePlacement", "Performance_WLBlockPlacement",
#"Performance_WLBlockPlacementFanoutOpt", "Performance_NetDelay_high", "Performance_NetDelay_medium",
#"Performance_NetDelay_low", "Performance_ExploreSLLs", "Performance_Retiming",
#"Area_Explore", "Power_DefaultOpt", "Flow_RunPhysOpt",
#"Flow_RunPostRoutePhysOpt", "Flow_RuntimeOptimized", "Congestion_SpreadLogic_high",
#"Congestion_SpreadLogic_medium", "Congestion_SpreadLogic_low", "Congestion_SpreadLogicSLLs",
#"Congestion_BalanceSLLs", "Congestion_BalanceSLRs", "Congestion_CompressSLRs"]

#launch_runs impl_1 -to_step write_bitstream -jobs 4 -lsf {bsub -R "rusage[atlasio=0]" -app Reserve20G -q atlaslong -W 24:00}


runNameSynth = runName + "_synth"
runNamesImpl = []
for i in range(len(implStrategy)):
    runNamesImpl.append(runName + "_impl_" + str(i))


# closeProjStr = "close_project\n"


outFile = open("runImplLSF.tcl", 'w')
for processorNum in processors:
	openProjStr = "open_project " + projectTrunkPath + "processor/vivado/L1TopoProcessor_U" + str(processorNum) + "/L1TopoProcessor_U" + str(processorNum) + ".xpr\n"
	outFile.write(openProjStr)
        outFile.write("set_param general.maxThreads 1\n")
        outFile.write("set_property top top_L1TopoProcessor_" + str(processorNum) + " [current_fileset]\n")
        outFile.write("update_compile_order -fileset sources_1\n")
	if createNewRuns:
	    outFile.write('create_run ' + runNameSynth + ' -flow {Vivado Synthesis 2015} -strategy Flow_PerfOptimized_high\n')
	    for i in range(len(runNamesImpl)):
		outFile.write('create_run ' + runNamesImpl[i] + ' -parent_run ' + runNameSynth + ' -flow {Vivado Implementation 2015}\n')

	outFile.write('set_property strategy ' + synthStrategy + ' [get_runs ' + runNameSynth + ']\n')
	for i in range(len(implStrategy)):
	    outFile.write('set_property strategy ' + implStrategy[i] + ' [get_runs ' + runNamesImpl[i] + ']\n')

	outFile.write('reset_run ' + runNameSynth + '\n')
	for i in range(len(runNamesImpl)):
	    outFile.write('reset_run ' + runNamesImpl[i] + '\n')
	#     outFile.write('launch_runs ' + runNamesImpl[i] + ' -jobs 4 -scripts_only\n')
	for i in range(len(runNamesImpl)):
	    outFile.write('launch_runs ' + runNamesImpl[i] + ' -to_step write_bitstream -lsf {bsub -n 64 -R "rusage[atlasio=0]" -R "span[ptile=64]" -app Reserve1800M -q atlasnodelong -W 24:00}\n')
	outFile.write("close_project\n")
outFile.write('q\n')

outFile.close()

print ("run the generated script using 'vivado -mode tcl -source runImplLSF.tcl'")
#vivado -mode tcl -source runImplLSF.tcl
