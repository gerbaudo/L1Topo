#!/usr/bin/python
import os
from runConfig import *
import argparse

parser = argparse.ArgumentParser(description='Creates scripts for submission of Vivado synthesis and implementation jobs to Slurm. The selected processor FPGAs, implementation strategies, number of threads and slurm job options can be set in "runConfig.py".\nAfter creating the script, enter the created folder with the run name and execute "submitVivadoJobs.sh". After implementation a summary file "' + summaryFilename + '" is created.')
parser.add_argument('--runName', '-n', action="store", dest="runName", type=str, default=defaultRunName, help="Name of the run. Will overwrite the generated scripts if run already exists")
parser.add_argument('--ip', '-i', action="store_true", dest="generateIP", help="(re)generate IP core files if flag is set")
#parser.add_argument('--outfile', '-o', action="store", dest="outfile", type=str, default="createSim.tcl", help="tcl file with commands to set up simulation")

args = parser.parse_args()
runName = args.runName
generateIP = args.generateIP

os.system("mkdir -p " + runName)
os.chdir(runName)
cwd = os.getcwd() + "/"
#projectPath = cwd + "../"
scriptsPath = cwd
#print(cwd)

#synthTclFileName = cwd + "runSynthesis.tcl"
#implTclFileName = cwd + "runImplementation.tcl"

#projectTrunkPath = "../../"

submitScriptName = cwd + "submitVivadoJobs.sh"
runNameSynth = runName  + "_synth" #"synth_0"
#synthesisTclFiles = []
synthImplFiles = []

fileNameIpTcl = cwd + "generateIp.tcl"
fileNameIpSh = cwd + "generateIp.sh"
if generateIP:
    with open(fileNameIpTcl, "w") as tcl, open(fileNameIpSh, "w") as sh:
        projectPath = cwd + "../../../processor/vivado/L1TopoProcessor_U1/"
        projectName = projectPath + "L1TopoProcessor_U1.xpr"
        tcl.write("open_project " + projectName + "\n")
        tcl.write("generate_target all [get_ips]\n")
        tcl.write("set runlist [get_runs *]\n")
        tcl.write("foreach i [get_ips] {\n")
        tcl.write("	if {[regexp -- $i $runlist]} {} else {create_ip_run $i}\n")
        tcl.write('	append i "_synth_1"\n')
        tcl.write("	reset_run $i\n")
        tcl.write("	launch_run -jobs 1 $i\n")
        tcl.write("}\n")
        tcl.write("foreach i [get_ips] {\n")
        tcl.write('\tappend i "_synth_1"\n')
        tcl.write("\twait_on_run $i\n")
        tcl.write("}\n")
        sh.write("#!/usr/bin/bash\n")
        sh.write("source " + vivadoSetup + "\n")
        sh.write("cd " + scriptsPath + "\n")
        sh.write("vivado -mode tcl -source " + fileNameIpTcl + "\n")
        print("wrote " + fileNameIpTcl.split("/")[-1])
        print("wrote " + fileNameIpSh.split("/")[-1])

for processorNumber in processors:
    projectPath = cwd + "../../../processor/vivado/L1TopoProcessor_U" + str(processorNumber) + "/"
    projectName = projectPath + "L1TopoProcessor_U" + str(processorNumber) + ".xpr"
    runNameSynthTcl = runNameSynth + "_" + str(processorNumber) + ".tcl"
    runNameSynthSh = runNameSynth + "_" + str(processorNumber) + ".sh"
    topModuleName = "top_L1TopoProcessor_" + str(processorNumber)

    with open(runNameSynthTcl, "w") as f, open(runNameSynthSh, "w") as sh:
        f.write("open_project " + projectName + "\n")
        f.write("set_property top " + topModuleName + " [current_fileset]\n")
        f.write("set runlist [get_runs *synth*]\n")
        f.write("if {[regexp -- " + runNameSynth + " $runlist]} {reset_run " + runNameSynth + "} else {create_run " + runNameSynth + " -flow {" + synthesisFlow + "}}\n")
        f.write('set_property strategy ' + synthStrategy + ' [get_runs ' + runNameSynth + ']\n')
        for impl in implStrategy:
            implName = runName  + "_impl_" + impl
            implNameTcl = implName + "_" + str(processorNumber) + ".tcl"
            implNameSh = implName + "_" + str(processorNumber) + ".sh"
            f.write("set runlist [get_runs *impl*]\n")
            f.write("if {[regexp -- " + implName + " $runlist]} {reset_run " + implName + "} else {create_run " + implName + " -parent_run " + runNameSynth + " -flow {" + implementationFlow + "}}\n")
            f.write('set_property strategy ' + impl + ' [get_runs ' + implName + ']\n')
        f.write("launch_runs " + runNameSynth + " -jobs " + str(nJobsSynth) + "\n")
        f.write("wait_on_run " + runNameSynth + "\n")
        f.write("q\n")
        sh.write("#!/usr/bin/bash\n")
        sh.write("source " + vivadoSetup + "\n")
        sh.write("cd " + scriptsPath + "\n")
        sh.write("vivado -mode tcl -source " + runNameSynthTcl + "\n")
        print("wrote " + runNameSynthTcl)
        print("wrote " + runNameSynthSh)
    
    implementationShFiles = []
    fileNameImplTcl = runName + "_impl_" + str(processorNumber) + ".tcl"
    fileNameImplSh = runName + "_impl_" + str(processorNumber) + ".sh"
    with open(fileNameImplTcl, "w") as f, open(fileNameImplSh, "w") as sh:
        f.write("open_project " + projectName + "\n")
        for impl in implStrategy:
            implName = runName  + "_impl_" + impl
            f.write("launch_runs " + implName + " -to_step write_bitstream -jobs " + str(nJobsImpl) + "\n")
        for impl in implStrategy:
            implName = runName  + "_impl_" + impl
            f.write("wait_on_run " + implName + "\n")
        f.write("q\n")
        sh.write("#!/usr/bin/bash\n")
        sh.write("source " + vivadoSetup + "\n")
        sh.write("cd " + scriptsPath + "\n")
        sh.write("vivado -mode tcl -source " + fileNameImplTcl + "\n")
        implementationShFiles.append(fileNameImplSh)
        print("wrote " + implNameTcl)
    
    synthImplFiles.append((runNameSynthSh, implementationShFiles))


with open (submitScriptName, "w") as f:
    f.write("#!/usr/bin/bash\n")
    f.write("ALL_JOB_IDS=''\n")
    dependencySynthseis = ""
    if generateIP:
        ipLogFileName = "ip.log"
        ipJobName = runName + "_ip"
        f.write("SUBMIT_COMMAND_IP='sbatch -A atlashpc -p " + ipQueue + " --nodes 1-1 -t " + wallTimeIp + " --job-name=" + ipJobName + " -o " + ipLogFileName + " " + fileNameIpSh + "'\n")
        f.write("JOBID_IP=`$SUBMIT_COMMAND_IP | cut -d ' ' -f4`\n")
        f.write("echo Submitted ip job $JOBID_IP\n\n")
        dependencySynthseis = " --dependency=afterok:'$JOBID_IP'"
    for tclFiles in synthImplFiles:
        synthName = tclFiles[0][:tclFiles[0].rfind(".")]
        synthLogName = synthName + ".log"
        synthShName = synthName + ".sh"
        synthJobName = synthName
        f.write("SUBMIT_COMMAND_SYNTH='sbatch -A atlashpc -p " + synthQueue + dependencySynthseis + " --ntasks=" + str(nTasksSynth) + " --cpus-per-task=" + str(nCpusPerTaskSyth) + " --mem-per-cpu=" + str(memPerCpuSynth) + " -t " + wallTimeSynth + " --job-name=" + synthJobName + " -o " + synthLogName + " " + synthShName + "'\n")
        #f.write("echo $SUBMIT_COMMAND_SYNTH\n")
        f.write("JOBID_SYNTH=`$SUBMIT_COMMAND_SYNTH | cut -d ' ' -f4`\n")
        f.write("ALL_JOB_IDS=$ALL_JOB_IDS:$JOBID_SYNTH\n")
        f.write("echo Submitted synthesis job $JOBID_SYNTH\n\n")
        for implFile in tclFiles[1]:
            implName = implFile[:implFile.rfind(".")]
            implLogName = implName + ".log"
            implShName = implName + ".sh"
            implJobName = implName
            f.write("SUBMIT_COMMAND_IMPL='sbatch -A atlashpc -p " + implQueue + " --dependency=afterok:'$JOBID_SYNTH' --nodes 1-1 -t " + wallTimeImpl + " --job-name=" + implJobName + " -o " + implLogName + " " + implShName + "'\n")
            f.write("JOBID_IMPL=`$SUBMIT_COMMAND_IMPL | cut -d ' ' -f4`\n")
            f.write("ALL_JOB_IDS=$ALL_JOB_IDS:$JOBID_IMPL\n")
            f.write("echo Submitted implementation job $JOBID_IMPL\n\n")
    f.write("ALL_JOB_IDS=${ALL_JOB_IDS:1}\n")
    f.write("sbatch -A atlashpc -p short --dependency=afterany:$ALL_JOB_IDS --ntasks=1 --cpus-per-task=1 --mem-per-cpu=2000 -t 0-01:00 --job-name=vivadoSummary -o summaryJob.log ../createSummary.py\n")
    #f.write("echo $ALL_JOB_IDS\n")
    f.write("echo Submitted summary job\n\n")
    f.write("echo Execute \'squeue -u $USER -l\' to check the job status.\n")
    print("write " + submitScriptName)
os.system("chmod +x " + submitScriptName)
