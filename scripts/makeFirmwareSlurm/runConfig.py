#!/usr/bin/python

processors = [1, 2, 3, 4]
defaultRunName = "default"
summaryFilename = "summary.txt"

synthStrategy = "Flow_PerfOptimized_high"
implStrategy = ["Performance_AggressiveExplore", "PlaceDesignSpreadLogic_medium", "Spread_logic_medium_fanoutOpt", "Performance_ExploreSLLs"]
#implStrategy = ["Performance_AggressiveExplore", "PlaceDesignSpreadLogic_medium"]

vivadoSetup = "/etapfs01/atlashpc/seartz/Xilinx/Vivado/2015.2/settings64.sh"

nJobsSynth = 8
nJobsImpl = 8

nTasksSynth = 1
nCpusPerTaskSyth = 1
memPerCpuSynth = 10000
wallTimeSynth = "0-10:00"
wallTimeImpl = "0-16:00"
wallTimeIp = "0-04:00"

synthQueue = "long"
implQueue = "nodelong"
ipQueue = "nodeshort"

# TODO: set dynamically
synthesisFlow = "Vivado Synthesis 2015"
implementationFlow = "Vivado Implementation 2015"
1
