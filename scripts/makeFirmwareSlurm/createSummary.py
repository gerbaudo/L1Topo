#!/usr/bin/python
#import matplotlib as mpl
#from matplotlib import pyplot as plt
#import numpy as np
#from matplotlib.ticker import AutoMinorLocator, MultipleLocator
#import math
import re
import os
import sys

cwd = os.getcwd() + "/"

sys.path.append(cwd + "../")
from runConfig import *

summaryFile = open(summaryFilename, "w")

runName = cwd.split("/")[-2]

summaryFile.write(("-"*50) + "\n\n")

for processorNum in processors:
    projectName = "L1TopoProcessor_U" + str(processorNum)
    topModuleName = "top_L1TopoProcessor_" + str(processorNum)
    projectPath = cwd + "../../../processor/vivado/L1TopoProcessor_U" + str(processorNum) + "/"
    summaryFile.write("Processor: " + str(processorNum) + "\n\n")
    for strategy in implStrategy:
        summaryFile.write("Strategy: " + strategy + "\n")
        runNameStrategy = runName + "_impl_" + strategy
        utilizationFileName = projectPath + projectName + ".runs/" + runNameStrategy + "/" + topModuleName + "_utilization_placed.rpt"
        if (os.path.isfile(utilizationFileName)):
            for line in open(utilizationFileName, "r"):
                regFound = re.search(r"Slice\sLUTs", line)
                if (regFound):
                    lineArr = line.split("|")
                    lutPercent = lineArr[5].strip().strip("<")
                    summaryFile.write("LUT: " + lutPercent + "%\n")
        else:
            summaryFile.write("file " + utilizationFileName + " not found.\n")

        bitfileName = projectPath + projectName + ".runs/" + runNameStrategy + "/" + topModuleName + ".bit"
        implOk = False
        if (os.path.isfile(bitfileName)):
            summaryFile.write("Bitfile was created.\n")
            implOk = True
        else:
            summaryFile.write("Bitfile was not created. \nLog files are located at " + projectPath + projectName + ".runs/" + runNameStrategy + "/\n")

        timingFileName = projectPath + projectName + ".runs/" + runNameStrategy + "/" + topModuleName + "_timing_summary_routed.rpt"
        if (os.path.isfile(timingFileName)):
            for line in open(timingFileName, "r"):
                timingMet = re.search(r"Timing constraints are not met", line)
                constraintsMet = re.search(r"All user specified timing constraints are met", line)
                if timingMet:
                    summaryFile.write(line+ "\n")
                if constraintsMet:
                    summaryFile.write(line+ "\n")
        else:
            summaryFile.write("File " + timingFileName + " not found.\n")

        summaryFile.write("\n")
    summaryFile.write(("-"*50) + "\n\n")

summaryFile.close()
