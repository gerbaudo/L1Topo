FNR==1{
    fileCounter += 1;
}


fileCounter==1 && $1~/^CTRLBUS/{
	name[substr($1, 9, 1),substr($1, match($1, "<") + 1, match($1, ">") - match($1, "<") - 1)] = $1;
	pins[substr($1, 9, 1),substr($1, match($1, "<") + 1, match($1, ">") - match($1, "<") - 1),substr($2, 2, 1)] = substr($2, 4);
	pins[substr($1, 9, 1),substr($1, match($1, "<") + 1, match($1, ">") - match($1, "<") - 1),substr($3, 2, 1)] = substr($3, 4);
}


fileCounter==2{
	for(l=0; l<42; ++l){
		
		if($1==pins["P",l,"3"]){
			packagePinName["3",l] = $2;
			polarity[l] = substr($2, match($2, "_T") - 1, 1);
			
			if($2~/SRCC/ || $2~/MRCC/){clkCapable["3",l] = substr($2, match($2, "CC")-2, 4);}
		
		}
	}
}

fileCounter==3{
	for(l=0; l<42; ++l){
		if($1==pins["P",l,"1"]){
			packagePinName["1",l] = $2;
			if($2~/SRCC/ || $2~/MRCC/){clkCapable["1",l] = substr($2, match($2, "CC")-2, 4);}
		}
		if($1==pins["P",l,"2"]){
			packagePinName["2",l] = $2;
			if($2~/SRCC/ || $2~/MRCC/){clkCapable["2",l] = substr($2, match($2, "CC")-2, 4);}
		}
	}
}

END{
	
	for(l=0; l<42; ++l){
		processor = (l<21)?"1":"2";
		l_ = (l<21)?l:l-21;
		printf("%u,%s,%s,%s,%s,%s,%s,%s,%s,%s\n", l, packagePinName["3",l], pins["P",l,"3"], pins["N",l,"3"], (polarity[l]=="N")?"inv":"", clkCapable["3",l], packagePinName[processor,l], pins["P",l,processor], pins["N",l,processor], clkCapable[processor,l]);
#		printf("%u\t%s:\t%s: %s %s,\t%s: %s\t%s\t%s\n", l_, name["P",l], processor, pins["P",l,processor], clkCapable[processor,l], "3", pins["P",l,"3"], clkCapable["3",l], polarity[l]);
		if(l==20){print "\n";}
	}
}