#/bin/sh !
#
# Source Vivado (USA15)
source /atlas/software/xilinx/Vivado/Vivado/2014.4/settings64.sh

usage()
{
    /bin/echo -e "\n WARNING: Usage `basename $0` ===> [U3] [U1] [U2]\n" 1>&2
    echo "Insert bit files with full path, type NO if no bit file"
    echo "Example (program only U3):"
    echo "./ProgramL1Topo.sh /atlas-home/0/esimioni/bitf/ControlFPGA.bit NO NO"
    echo "*************************"
#
}
#
case $# in
    3)
        bitU3=${1}
        bitU1=${2}
        bitU2=${3}
        ;;
    *)
        usage
        exit 1
esac


# Remove tcl temporary script if present
if [ -f tmp.tcl ]; then
	rm tmp.tcl
fi

# connection
echo "open_hw" >>  tmp.tcl
echo "connect_hw_server -url localhost:3121" >>  tmp.tcl
echo "current_hw_target [get_hw_targets */xilinx_tcf/Xilinx/00001319e81e01]" >>  tmp.tcl
echo "set_property PARAM.FREQUENCY 6000000 [get_hw_targets */xilinx_tcf/Xilinx/00001319e81e01]" >> tmp.tcl
echo "open_hw_target" >> tmp.tcl



# Generate tcl for U3
if [ "$bitU3" != "NO" ];
   then
	echo "current_hw_device [lindex [get_hw_devices] 1]" >>  tmp.tcl
	echo "refresh_hw_device -update_hw_probes false [lindex [get_hw_devices] 1]" >>  tmp.tcl
	echo "current_hw_device [lindex [get_hw_devices] 2]" >>  tmp.tcl
	echo "refresh_hw_device -update_hw_probes false [lindex [get_hw_devices] 2]" >> tmp.tcl
	echo "current_hw_device [lindex [get_hw_devices] 3]" >> tmp.tcl
	echo "refresh_hw_device -update_hw_probes false [lindex [get_hw_devices] 3]" >>  tmp.tcl
	echo "set_property PROGRAM.FILE {$bitU3} [lindex [get_hw_devices] 1]" >> tmp.tcl
	echo "set_property PROBES.FILE {} [lindex [get_hw_devices] 1]" >> tmp.tcl
	echo "set_property PROGRAM.FILE {$bitU3} [lindex [get_hw_devices] 1]" >> tmp.tcl
	echo "program_hw_devices [lindex [get_hw_devices] 1]" >> tmp.tcl
	echo "refresh_hw_device [lindex [get_hw_devices] 1]" >> tmp.tcl

   else
	echo "No bit file for U3"
fi


# Generate tcl for U1
if [ "$bitU1" != "NO" ];
   then
	echo "set_property PROBES.FILE {} [lindex [get_hw_devices] 2]" >> tmp.tcl
	echo "set_property PROGRAM.FILE {$bitU1} [lindex [get_hw_devices] 2]" >> tmp.tcl
	echo "program_hw_devices [lindex [get_hw_devices] 2]" >> tmp.tcl
	echo "refresh_hw_device [lindex [get_hw_devices] 2]" >> tmp.tcl
   else
        echo "No bit file for U1"
fi


# Generate tcl for U1
if [ "$bitU2" != "NO" ];
   then
	echo "set_property PROBES.FILE {} [lindex [get_hw_devices] 3]" >> tmp.tcl
	echo "set_property PROGRAM.FILE {$bitU2} [lindex [get_hw_devices] 3]" >> tmp.tcl
	echo "program_hw_devices [lindex [get_hw_devices] 3]" >> tmp.tcl
	echo "refresh_hw_device [lindex [get_hw_devices] 3]" >> tmp.tcl
   else
        echo "No bit file for U2"
fi


# Program the module 
vivado -mode batch -source tmp.tcl


# Remove tcl temporary script 
if [ -f tmp.tcl ]; then
        rm tmp.tcl
fi

