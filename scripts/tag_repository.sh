#!/bin/sh
#
usage()
{
    /bin/echo -e "\n WARNING: Usage `basename $0` ===>  Tag[]  Option[]\n" 1>&2
    echo "Option 1 = corresponding FW version"
    echo "Option 2 = description"
    echo "*************************"
#
}
#
case $# in
    2)
        FTag=${1}
        MTag=${2}
        ;;
    *)
        usage
        exit 1
esac

#Copy repository in /tag directory
svn copy svn+ssh://$USER@svn.cern.ch/reps/atl1calo/firmware/L1Topo/trunk svn+ssh://$USER@svn.cern.ch/reps/atl1calo/firmware/L1Topo/tags/release_${FTag} -m "${MTag}"

# Notification
mail -s "Tag $FTag created" l1topo-hardware@cern.ch <<< 'Automatic e-mail: L1Topo FW repository tag ${FTag} created. ${MTag}.'
echo "********************************"
echo "Mail sent to l1topo-hardware@cern.ch"
echo "********************************"

