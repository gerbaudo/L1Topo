#/bin/bash !
usage()
{
    echo "**********************************************************************"
    echo "Start making FW for processors"
    echo "One argument required: '0' (weekly build) or '1' (build after specific FW update)"
    echo ""
    echo "To execute in the same directory where the SVN design has been checked out"
    echo "with: svn co svn+ssh://"$USER"@svn.cern.ch/reps/atl1calo/firmware/L1Topo/trunk"
    echo ""
    echo "Make sure you are into a screen session"
    echo ""
    echo -e "\n WARNING: Usage `basename $0` ===>  flag \n" 1>&2
    echo ""
    echo "**********************************************************************"
}
#
case $# in
    1)
        built=${1}
        ;;
    *)
        usage
        exit 1
esac


 export local=`echo $PWD`

if [ $built -eq 1 ]; then

 #Export some variables, unique time tag (seconds from 1970) and revision from repository
 export time=`date +%s`
 cd trunk
 export revision=`svn info | grep Revision | awk '{print $2}'`
 cd ..
 export generation=`echo $time"-100-r"$revision"-MOGON"`
 export generation_post=`echo *"-100-r"$revision"-MOGON"`



 #Setup relevant FW parameters before new FW with newer version
 while : ; do
  echo "******************************************************"
  echo ""
  echo "1)"
  echo "Have you update FW version and revision? in this file (then commit)"
  echo "trunk/processor/sources/common/L1TopoProcessor_690_common_top.vhd"
  echo ""
  echo "Precisely those lines:"
  echo "constant FirmwareVersion:  std_logic_vector(31 downto 0) := X00040409;"
  echo "constant FirmwareDate:     std_logic_vector(31 downto 0) := X20160823;"
  echo "constant FirmwareRevision: std_logic_vector(31 downto 0) := X00002845;"
  echo ""
  echo ""
  echo "2)"
  echo "Have you updated the Generation Run name?"
  echo "trunk/processor/vivado/runImplLSF.py"
  echo "with run"$generation in runName
  echo ""
  #echo "Have you set which processor you want to make in the line: processors = [1, 2, 3, 4]"
  echo ""
  echo "If NOT, kill this session before countdown finish and do it"
  echo "******************************************************"
  sleep 5
  echo "***************"
  sleep 1
  echo "**************"
  sleep 1
  echo "*************"
  sleep 1
  echo "************"
  sleep 1
  echo "***********"
  sleep 1
  echo "**********"
  sleep 1
  echo "*********"
  sleep 1
  echo "********"
  sleep 1
  echo "*******"
  sleep 1
  echo "******"
  sleep 1
  echo "*****"
  sleep 1
  echo "****"
  sleep 1
  echo "***"
  sleep 1
  echo "**"
  sleep 1
  echo "*"
  sleep 1
  break
done
	else
 	cd trunk
	 export revision=`svn info | grep Revision | awk '{print $2}'`
	 cd ..
	echo "Weekly implementation"
 	export generation_post=`echo "161219-1200-r2902-lateMuonFix"`
 	export generation=`echo "161219-1200-r2902-lateMuonFix"`
fi



#Set Vivado
source /etapfs01/atlashpc/seartz/Xilinx/Vivado/2015.2/settings64.sh

# Remove TCL temporary file
file="tmp.tcl"
if [ -f $file ] ; then
    rm $file
fi


# Generate TCL

for processor in `echo "1 2 3 4"`;
do
        export module=`echo $processor | awk '{if ($1/2<=1) {print 1} else {print 2};}'`
        export U=`echo $processor | awk '{if ($1==1 || $1==3) {print 1} else {print 2};}'`
	#
	echo "Generate processor "$processor
	echo "open_project trunk/processor/vivado/L1TopoProcessor_U"$processor"/L1TopoProcessor_U"$processor".xpr" >> tmp.tcl
	echo "source trunk/processor/sources/common/tcl_scripts/add_common_source_files.tcl" >> tmp.tcl
	echo "update_compile_order -fileset sources_1" >> tmp.tcl
	echo "update_compile_order -fileset sim_1" >> tmp.tcl
	echo "source trunk/processor/sources/module_"$module"/U"$U"/add_algorithm_source_files.tcl" >> tmp.tcl
	echo "update_compile_order -fileset sources_1" >> tmp.tcl
	echo "set_property top top_L1TopoProcessor_"$processor" [current_fileset]" >> tmp.tcl
	echo "update_compile_order -fileset sources_1" >> tmp.tcl
	#echo "reset_run synth_1" >> tmp.tcl
	echo "launch_runs synth_1 -lsf {bsub -n 64 -R "rusage[atlasio=0]" -R "span[ptile=64]" -app Reserve1800M -q atlasnodeshort -W 4:00}" >> tmp.tcl
	echo "close_project" >> tmp.tcl
	# Execute
	vivado -mode batch -source tmp.tcl
	sleep 1
	rm tmp.tcl
done



#Monitor Jobs status
while : ; do
  sleep 60
  export active_jobs=`bjobs | awk '{if (match($2,"'$USER'")) {print $0}}' | wc | awk '{print $1}'`
  if [ $active_jobs -eq 0 ]; then
        echo "Jobs finished"
      sleep 600
      break
  fi
done


# Start implementation
cd trunk/processor/vivado/
python runImplLSF.py
sleep 10
vivado -mode tcl -source runImplLSF.tcl
cd ../../../




#Monitor Jobs status
while : ; do
  sleep 60
  export active_jobs=`bjobs | awk '{if (match($2,"'$USER'")) {print $0}}' | wc | awk '{print $1}'`
  if [ $active_jobs -eq 0 ]; then
        echo "Jobs finished"
      sleep 600
      break
  fi
done




# Check timing, logic usage
AWK='{ 
if (match($0,"Slice LUTs")){
        print $11"%"
        }
}'
# Remove log
file="log.txt"
if [ -f $file ] ; then
    rm $file
fi
#
date >> log.txt
echo "Topo Processors FW build, 4 independent implementations/strategies" >> log.txt
echo "Repository Rev"$revision >> log.txt
echo " " >> log.txt
for processor in `echo "1 2 3 4"`;
        do
        export path1=`echo trunk/processor/vivado/L1TopoProcessor_U${processor}/L1TopoProcessor_U${processor}.runs`
        export lut_usage=`less ${path1}/run${generation_post}_synth/top_L1TopoProcessor_${processor}_utilization_synth.rpt | awk "${AWK}"`
	echo " " >> log.txt
        echo "*************************************" >> log.txt
        echo "Check timing in logs for processor U"${processor}", LUT logic usage of "${lut_usage}"%"  >> log.txt
        echo ""  >> log.txt
        for implementation in `echo "0 1 2 3"`;
                do
                export path2=`echo run${generation_post}_impl_${implementation}`
                echo "Result from implementation "${implementation}":"  >> log.txt
                #export check_bit=`ls ${path1}/${path2}/top_L1TopoProcessor_${processor}.bit | wc | awk '{print $1}'`
                # Check if bit-file is there
                export bitfile="${path1}/${path2}/top_L1TopoProcessor_${processor}.bit"
                if [ -f $bitfile ] ; then
                    less ${path1}/${path2}/top_L1TopoProcessor_${processor}_timing_summary_routed.rpt | awk '{if (match($0,"Timing constraints are not met")) print $0}'  >> log.txt
                    less ${path1}/${path2}/top_L1TopoProcessor_${processor}_timing_summary_routed.rpt | awk '{if (match($0,"All user specified timing constraints are met")) print $0}'  >> log.txt
                    echo ""  >> log.txt
                else
                    echo "Bit file implementation FAILED"  >> log.txt
                    echo ""  >> log.txt
                fi
                done
        done
	cat log.txt
	mail -s "Topo FW build summary" l1topo-hardware@cern.ch < log.txt


	#Only for weekly build
	if [ $built -eq 0 ]; then
	# Send an e-mail for weekly build
	mail -s "L1Topo Weekly FW build summary: Rev"${revision} l1topo-hardware@cern.ch < log.txt
	fi



