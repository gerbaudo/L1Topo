import sys
if len(sys.argv) < 2:
    print("ERROR: target file needs to be specified!")
    sys.exit()

fileName = str(sys.argv[1])

def getDocumentStart(processorNumber):
    tmpString = '''library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use work.L1TopoDataTypes.all;

entity L1TopoAlgorithms_U'''+str(processorNumber)+''' is
    Port(ClockBus     : in  std_logic_vector(2 downto 0);

         EmTobArray   : in  ClusterArray(InputWidthEM - 1 downto 0);
         TauTobArray  : in  ClusterArray(InputWidthTAU - 1 downto 0);
         JetTobArray  : in  JetArray(InputWidthJET - 1 downto 0);
         MuonTobArray : in  MuonArray(InputWidthMU - 1 downto 0);
         MetTobArray  : in  MetArray(0 downto 0);
         
         Parameters   : in  ParameterSpace(NumberOfAlgorithms - 1 downto 0);
         SortParameters     : in  ParameterSpace(NumberOfSortAlgorithms - 1 downto 0);
         
         Results      : out std_logic_vector(NumberOfResultBits - 1 downto 0);
         Overflow     : out std_logic_vector(NumberOfResultBits - 1 downto 0)
    );
end L1TopoAlgorithms_U'''+str(processorNumber)+''';

architecture Behavioral of L1TopoAlgorithms_U'''+str(processorNumber)+''' is'''
    return tmpString

DOCUMENT_END = 'end Behavioral;'

MENU_NAME_STR = "-- Menu Name "
END_VHDL_STR = "// L1TopoXMLVHDLParser::PrintVHDL() //"

file = open(fileName, "r")

menus = []

menuNum = 0
for line in file:
    if MENU_NAME_STR in line:
        if (menuNum > 0):
            menus.append(currentMenuLines)
        menuNum += 1
        currentMenuLines = []
    else:
        if (menuNum > 0):
            if line.find(END_VHDL_STR) < 0:
                currentMenuLines.append(line)
menus.append(currentMenuLines)

if len(menus) < 5:
    currentNum = 0
    for menu in menus:
        currentNum += 1
        file = open("L1TopoAlgorithms_U"+str(currentNum)+".vhd", "w")
        for line in getDocumentStart(currentNum):
            file.write(line)        
        for line in menu:
            file.write(line)
        for line in DOCUMENT_END:
            file.write(line)  
        print("created file "+str(file.name)) 
        file. close()
