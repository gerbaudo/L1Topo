from ROOT import *
from numpy import binary_repr
import argparse

parser = argparse.ArgumentParser(description='')
parser.add_argument('--inFile', '-f', action="store", dest="inFile", type=str, default="default.root", help="kalman filter lookup table (root file)")

args = parser.parse_args()

inFileName = args.inFile

gROOT.Reset()

def floatToBinaryUnsigned(decVal, integerBitNum, decimalBitNum):
    bitNum = integerBitNum + decimalBitNum
    return binary_repr(int(round(decVal*2**(decimalBitNum), 0)), bitNum)

def floatToBinarySigned(decVal, integerBitNum, decimalBitNum):
    bitNum = integerBitNum + 1 + decimalBitNum
    return binary_repr(int(round(decVal*2**(decimalBitNum), 0)), bitNum)

def binaryToFloatSigned(binVal):
    tmp = 1. * strToIntSigned(binVal, len(binVal))
    tmp /= (2**(len(binVal) - 2))
    return tmp

def strToIntTwosComplement(s, bitNumber):
    if (s[0] == "1" and len(s) >= bitNumber):
        bitNumber = len(s)
        return -(2**bitNumber-int(s[0:], 2))
    else:
        return int(s, 2)

# alias function for consistency and compatibility reasons
def strToIntSigned(s, bitNumber):
    return strToIntTwosComplement(s, bitNumber)

def loadHistogram(fileName,histgramName):
    f = TFile(fileName)
    if (not f):
        print("error, can't open file")
    histo = f.Get(histgramName)
    histo.SetDirectory(gROOT)
    f.Close()
    return histo

NUM_INTEGER_BITS = 1
NUM_DECIMAL_BITS = 7


h_lut = loadHistogram(inFileName, "LUT")

binsX = h_lut.GetNbinsX()
binsY = h_lut.GetNbinsY()

etaBins = 16
etBins = 10

print("number of eta bins: " + str(binsX))
print("number of et bins: " + str(binsY))

NUM_MISSING_ET_BINS = 3
MAX_ET_BIN = 8

lutString = ""
lutPython = []#[[0]*16]*10
for x in range(etBins):
    tmp = []
    for y in range(etaBins):
        if (x <= NUM_MISSING_ET_BINS):
            value = h_lut.GetBinContent(y+1, 1)
        elif (x >= MAX_ET_BIN-1):
            value = h_lut.GetBinContent(y+1, MAX_ET_BIN-NUM_MISSING_ET_BINS)
        else:
            value = h_lut.GetBinContent(y+1, x+1 - NUM_MISSING_ET_BINS)
        binValue = floatToBinarySigned(value, NUM_INTEGER_BITS, NUM_DECIMAL_BITS)
        print("EtBin:" +str(x+1), "EtaBin:"+str(y+1), value, binValue, str(binaryToFloatSigned(binValue)))
        lutString = binValue + lutString
        tmp.append(binaryToFloatSigned(binValue))
    lutPython.append(tmp)

print("\nVHDL:")      
print('constant lookupTableEtEta_in : std_logic_vector(KalmanMETCorrection_numberOfEtBins * KalmanMETCorrection_numberOfEtaBins * KalmanMETCorrection_correctionBitWidth - 1 downto 0) := "'+lutString+'";')

print("\nPython:")
print("KALMAN_MET_LUT = "+str(lutPython))
